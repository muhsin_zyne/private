<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',

    'modules' => [
        'b2b' => [
            'class' => 'frontend\modules\b2b\Module',
        ],
        'suppliers' => [
            'class' => 'frontend\modules\suppliers\Module',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',

            'treeStructure' => [ 
                    'treeAttribute' => 'root',
                    'leftAttribute' => 'lft',
                    'rightAttribute' => 'rgt',
                    'depthAttribute' => 'lvl',
                 ],
            
            'dataStructure' => [
                    'keyAttribute' => 'id',
                    'nameAttribute' => 'title',
                    'iconAttribute' => 'icon',
                    'iconTypeAttribute' => 'icon_type'
                ],
        ],

      
    ],
    
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'eway'=>[
            'class' => 'frontend\components\eway\Eway',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
            ],
        ],    
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
         'view' => [
          'class' => 'frontend\components\InteView',
          'theme' => [
             'pathMap' => [
                '@app/views' => [
                    '@webroot/themes/default',
                 ]
             ],
             'baseUrl' => '@web/',
           ],
        ],
        'cart' => [
            'class' => 'frontend\components\InteCart',
            'cartId' => 'inte_cart',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<slug>.html' => 'site/slug',
                //'category/<slug>' => 'categories/products',
            ]
        ],
    ],
    'params' => $params,
];