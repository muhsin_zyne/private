<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * CheckoutForm is the model behind the checkout form.
 */
class CheckoutForm extends Model
{
    public $billing_firstname;
    public $billing_lastname;
    public $billing_company;
    public $email;
    public $billing_street;
    public $billing_city;
    public $billing_state;
    public $billing_postcode;
    public $billing_country;
    public $billing_phone;
    public $billing_fax;
    public $shipping_firstname;
    public $shipping_lastname;
    public $shipping_company;
    public $shipping_street;
    public $shipping_city;
    public $shipping_state;
    public $shipping_postcode;
    public $shipping_country;
    public $shipping_phone;
    public $shipping_fax;
    public $password;
    public $confirmPassword;
    public $shipping_method;
    public $voucherCode;
    public $creditcard_name;
    public $creditcard_no;
    public $creditcard_expiry_month;
    public $creditcard_expiry_year;
    public $creditcard_cvv;
    public $address;
    // public $email;
    // public $subject;
    // public $body;
    // public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['billing_firstname', 'billing_lastname', 'billing_company', 'email', 'billing_street', 'billing_city', 'billing_state', 'billing_country','creditcard_name','creditcard_no','creditcard_expiry_month','creditcard_cvv'], 'safe'],
            [['billing_firstname', 'billing_lastname', 'email', 'billing_street', 'billing_city', 'billing_state', 'billing_country', 'shipping_phone', 'billing_phone'], 'required', 
            'whenClient' => "function(attribute, value){
                if($('.stored-billing-address').length>0)
                    return ($('.stored-billing-address').val()=='new')
                else
                    return true;
            }"],
	    //[['billing_firstname', 'billing_lastname', 'billing_street', 'billing_city', 'billing_state', 'billing_country' ,'shipping_firstname', 'shipping_lastname', 'shipping_company', 'shipping_street', 'shipping_city', 'shipping_state', 'shipping_country'], 'match', 'pattern' => '/[^\x00-\x7F]/i', 'not' => true],
            [['creditcard_name', 'creditcard_no', 'creditcard_expiry_month', 'creditcard_expiry_year', 'creditcard_cvv'], 'required'],
            [['creditcard_no', 'creditcard_expiry_month', 'creditcard_expiry_year'], 'common\components\CreditCardValidator', 'when' => function(){ return false; }], //we need this only in the client side
            [['creditcard_cvv'], 'number', 'numberPattern' => '/^[0-9]{3,4}/', 'message' => 'CVV is invalid'],
            [['billing_postcode', 'billing_phone', 'billing_fax'], 'integer'],
            [['billing_postcode', 'shipping_postcode',], 'string', 'max' => 8],
	    [['shipping_firstname', 'shipping_lastname', 'shipping_company', 'shipping_street', 'shipping_city', 'shipping_state', 'shipping_country', 'shipping_method'], 'safe'],
            [['shipping_firstname', 'shipping_lastname', 'shipping_street', 'shipping_city', 'shipping_state', 'shipping_country'], 'required', 
            'whenClient' => "function(attribute, value){
                if($('.stored-shipping-address').length>0)
                    return ($('.stored-shipping-address').val()=='new')
                else
                    return true;
            }"],
            [['shipping_postcode', 'shipping_phone', 'shipping_fax'], 'integer'],
            [['shipping_method'], 'required' , 'message' => 'You must select a shipping method'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            ['password', 'required', 'whenClient' => "function(attribute, value){ return ($('input[type=\"radio\"]#login-register:checked').length > 0) }"],
            ['password', 'string', 'min' => 6],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }

    public function setShippingAddress($byod){ 
        $this->shipping_company = $byod->organisation_name;
        $this->shipping_street = $byod->address;
        $this->shipping_city = $byod->city;
        $this->shipping_state = $byod->state;
        $this->shipping_postcode = $byod->postcode;
        $this->shipping_phone = $byod->phone;
    }
}
