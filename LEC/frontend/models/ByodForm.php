<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ByodForm extends Model
{

    public $code;

    public function rules()
    {
        return [ 
            [['code'], 'required' , 'isEmpty' => function ($value) { return empty($value);}],
        ];

    }
}
?>