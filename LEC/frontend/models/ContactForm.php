<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Configuration;
use common\models\Stores;
use common\models\B2bAddresses;
class ContactForm extends \yii\base\Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;
    public $phone;
    public $enquiry;
    public $addressId;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body','verifyCode'], 'required'],
            // email has to be a valid email address
            ['email', 'email','message'=>'Please enter a valid email.'],
            
            [['phone','subject'], 'safe'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],

            //['verifyCode', 'captcha','captchaAction'=>'/b2b/site/captcha'],
        ];
    }

    public function attributeLabels(){
	return ['body' => 'Enquiry','subject'=>'Phone'];
    }

    public function sendB2bContactMail() // send to admin (b2b user)
    { 
        $name=$this->name;
        $email=$this->email;
        $enquiry=$this->body;
        if($this->phone!=NULL)
            {$phone=$this->phone;}
            else
            {$phone='Not set';}
        $template = \common\models\EmailTemplates::find()->where(['code' => 'b2bcontactus'])->one();
        //sent to master admin
        $user = \common\models\User::find()->where(['roleId' => '1'])->one();
        $useremail=$user->email;
        $queue = new \common\models\EmailQueue;
        $queue->models = ['user'=>$user]; 
        $queue->replacements =[ 'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'',
            'emailLinkedinImage'=>'','Logo'=>'','useremail'=>$useremail,'name'=>$name,'email'=>$email,'phone'=>$phone,'enquiry'=>$enquiry, 'footerText' => 'Thanks for using the Leading Edge B2B Platform'];
        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template); //var_dump($queue->message);die;
        $queue->save();
        //sent to sender himself
        /*$user= Yii::$app->user->identity;
        $useremail=$user->email;
        $queue = new \common\models\EmailQueue;
        $queue->models = ['user'=>$user]; 
        $queue->replacements =[ 'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'',
            'emailLinkedinImage'=>'','Logo'=>'','useremail'=>$useremail,'name'=>$name,'email'=>$email,'phone'=>$phone,'enquiry'=>$enquiry, 'footerText' => 'Thanks for using the Leading Edge B2B Platform'];
        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template); //var_dump($queue->message);die;
        $queue->save();*/
    }
    public function sendSuppliersContactMail() // send to admin (suppliers user)
    {
        $name=$this->name;
        $email=$this->email;
        $enquiry=$this->enquiry;
        if($this->subject!=NULL)
            {$phone=$this->subject;}
            else
            {$phone='Not set';}
        $template = \common\models\EmailTemplates::find()->where(['code' => 'supplierscontactus'])->one();
        $user= \common\models\User::find()->where(['roleId' => 1])->one();
        $useremail=$user->email;
        $queue = new \common\models\EmailQueue;
        $queue->models = ['user'=>$user]; 
        $queue->replacements =[ 'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'',
                        'emailLinkedinImage'=>'','Logo'=>'','useremail'=>$useremail,'name'=>$name,'email'=>$email,'phone'=>$phone,'enquiry'=>$enquiry,];
        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
        $queue->save();
    }

    public function sendEmail($storeId,$addressId=NULL) // send to admin (normal user)
    {
        $name=$this->name;
        $email=$this->email;
        //$subject=$this->subject;        
        $body=$this->body;        
        if($this->subject!='') {$phone=$this->subject;}  else {$phone='Not set';} 
        $address=isset($addressId) ? '<tr>
            <td style="padding-bottom:5px;font-weight:bold;width:175px;">Store:</td><td style="padding-bottom:5px;">'.B2bAddresses::find()->where(['id' => $addressId])->one()->addressForDropdown.'</td></tr>' : '';
        //var_dump($address) ;die; 
        $template = \common\models\EmailTemplates::find()->where(['code' => 'usercontactus'])->one();
        $store=\common\models\Stores::find()->where(['id' => $storeId])->one();
        $user= \common\models\User::find()->where(['id' => $store->adminId])->one();//var_dump($template);die();        
        $queue = new \common\models\EmailQueue;
        $queue->models = ['store'=>$store,'user'=>$user,'addressId'=>$addressId]; 
        $queue->recipients=$this->contactRecipients;
        $template->layout = "layout";
        $queue->recipients =isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $this->contactRecipients; 
        $queue->replacements =['emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage, 'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'logo'=>$store->logo,'useremail'=>$queue->recipients,'name'=>$name,'email'=>$email,'phone'=>$phone,'body'=>$body,'address'=>$address,'headerLogo'=>$this->mailBanner,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->billingAddress,];
        $queue->from = "no-reply";
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        return $queue->save();
    }
    public static function getContactRecipients() // websites only
    {
        $configContactRecipients=Configuration::findSetting('contact_form_recipients', yii::$app->params['storeId']);
        $configOrderConfirmationRecipients=Configuration::findSetting('order_confirmation_recipients', yii::$app->params['storeId']);
        $store=Stores::findOne(yii::$app->params['storeId']);
        if($configContactRecipients!=''){
            return $configContactRecipients;
        } else if($configOrderConfirmationRecipients!=''){
            return $configOrderConfirmationRecipients;
        } else{
            return $store->email;
        }

    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/contact-us.jpg></a>'; 
    }

}
