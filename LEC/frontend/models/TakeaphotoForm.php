<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Stores;
use frontend\components\Helper;

 class TakeaphotoForm extends \yii\db\ActiveRecord {
    public static function tableName()
    {
        return 'UploadPhoto';
    }
    
    public function rules(){
        return [
        [['name','email','phone','photo'],'required'],
		[['name','priceRange','email','phone','photo','comment'],'safe'],
        [['email'],'email'],
        [['photo'], 'file',  'extensions' => 'png,jpg,jpeg,gif,psd,doc,pdf,xls,docx','maxSize' => 3145728, 'tooBig' => 'Image should be less than 3 MB.'],
        [['name'],'string'],
        ];
    }
    public function getStore()
    {
        if($this->storeId!='')
        {
            return $this->hasOne(Stores::className(), ['id' => 'storeId']);
        }
        else
        {
            return $this->hasOne(Stores::className(), ['adminId' => 'customerId']);
        }
        
    }
	public function sendMail()
	{
        //$adminuser = \common\models\User::find()->where(['storeId' => $this->storeId,'roleId' => 3])->one();
        //$useremail=$adminuser->email;

        $store = Stores::findOne($this->storeId);

        //var_dump($store->admin->email);die();

        $useremail = $store->admin->email;
        $store_id=$this->storeId;
        $order_id=$this->id;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'takeaphoto'])->one();
        //$user = \common\models\User::find()->where(['id' => $user_id])->one();
        $store = \common\models\Stores::find()->where(['id' => $store_id])->one();
        $takeaphoto = $this;
        $queue = new \common\models\EmailQueue;
            $queue->models = ['store'=>$store,'takeaphoto'=>$takeaphoto]; 
			$queue->replacements =[ 'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
                'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,
                'emailLinkedinImage'=>$store->emailLinkedinImage,'siteUrl'=>'','Logo'=>$store->Logo,'useremail'=>$useremail];
            $queue->from = "no-reply";
            //$queue->recipients = $useremail;
            $queue->recipients = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $useremail;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);
			//var_dump($queue->message);
            $queue->save();
	}
	public function sendMailUser()
	{
        //$adminuser = \common\models\User::find()->where(['storeId' => $this->storeId,'roleId' => 3])->one();
        $useremail=$this->email;
        //$useremail = 
        $store_id=$this->storeId;
        $order_id=$this->id;    
        $template = \common\models\EmailTemplates::find()->where(['code' => 'takeaphotouser'])->one();
        //$user = \common\models\User::find()->where(['id' => $user_id])->one();
        $store = \common\models\Stores::find()->where(['id' => $store_id])->one();
        $takeaphoto = $this;
        $queue = new \common\models\EmailQueue;
            $queue->models = ['store'=>$store,'takeaphoto'=>$takeaphoto]; 
			$queue->replacements =['emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
                'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,
                'emailLinkedinImage'=>$store->emailLinkedinImage,'siteUrl'=>'','Logo'=>$store->Logo,'useremail'=>$useremail];
			
            $queue->from = "no-reply";
            $queue->recipients = $this->email;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);
			//var_dump($queue->message);die;
            $queue->save();
	}
	public function getPhotoUrl()
    {
        if($this->photo!='')
        {
		   return '<a href='.Yii::$app->params["rootUrl"].$this->photo.'>'.Yii::$app->params["rootUrl"].$this->photo.'</a>';
        }
        else
        {
            return '';
        }
    }
    
}