<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    //public $username;
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $confirm_password;
    public $storeId;
    public $verifyCode;
    public $e1;
    public $scene = "normal"; //or "checkout"

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            ['firstname', 'filter', 'filter' => 'trim'],
            ['firstname', 'required'],
            ['firstname', 'string', 'min' => 2, 'max' => 255],

            ['lastname', 'filter', 'filter' => 'trim'],
            ['lastname', 'required'],
            ['lastname', 'string', 'min' => 1, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [['email','storeId'],'unique', 'targetClass' => '\common\models\User','targetAttribute'=>['email','storeId'], 'message' => 'This email address has already been taken.'],
            
            [['password','confirm_password'],'safe'],
            [['password','confirm_password','verifyCode'], 'required', 'when' => function($model){ return $model->scene == "normal"; }],
            ['password', 'string', 'min' => 6],
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
            ['verifyCode', 'captcha', 'when' => function($model){ return $model->scene == "normal"; }],
            //['verifyCode', 'required'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function attributeLabels(){
        return [
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
        ];
    }
    public function signup()
    {
            if ($this->validate()) { //die('validate');
                $connection = Yii::$app->getDb();
                $user = new User;
                $user->email = $this->email;
                $user->status = "1";
                $user->firstname = $this->firstname;
                $user->lastname = $this->lastname;
                $user->roleId = "4";
                $user->storeId = Yii::$app->params['storeId'];
                $user->password_hash = Yii::$app->security->generatePasswordHash($this->password);
                $user->auth_key = Yii::$app->security->generateRandomString();
                $user->created_at = null;//new \yii\db\Expression('DEFAULT');
                //var_dump($user->attributes); die;
                if($user->save(false, ['email', 'status', 'firstname', 'lastname', 'roleId', 'storeId', 'password_hash', 'auth_key'])){
                    $user->sendActivationMail();
                    return $user;
                }
            }
            else{
        		
            }

        return false;
    }

}
