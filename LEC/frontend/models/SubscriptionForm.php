<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class SubscriptionForm extends \yii\base\Model
{
    public $firstname;
    public $lastname;
    public $email;
    public function rules()
    {
        return [
            // name, email are required
            [['email'], 'required'],
            [['firstname'], 'required', 'message' => "First Name cannot be blank."],          // email has to be a valid email address
            [['lastname'], 'required', 'message' => "Last Name cannot be blank."],
            ['email', 'email'],
        ];
    }

    public function attributeLables(){
        return [
            'firstname' => 'First Name',
            'lastname' => 'Last Name'
        ];
    }
    
    public function sendEmail($storeId, $email) { // send to admin (normal user)
        $template = \common\models\EmailTemplates::find()->where(['code' => 'talkboxmail'])->one();
        $store = \common\models\Stores::find()->where(['id' => $storeId])->one();
        $user = \common\models\User::find()->where(['id' => $store->adminId])->one(); //var_dump($template);die();        
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store' => $store, 'user' => $user, 'addressId' => ''];
        $queue->subject = "New Talkbox Mail from " . $store->title;
        //$queue->recipients =Yii::$app->params['adminEmail'];
        $queue->recipients = $user->email;
        //$queue->cc = 'mark@xtrastaff.com.au,' . Yii::$app->params['adminEmail'];
        //$queue->recipients=isset(Yii::$app->params['tempcontactusemail']) ? Yii::$app->params['tempcontactusemail'] : $queue->recipients;   
        $queue->replacements = ['emailFacebookImage' => '', 'emailTwitterImage' => '', 'emailInstagramImage' => '',
            'emailYoutubeImage' => '', 'emailPinterestImage' => '', 'emailGoogleplusImage' => '',
            'emailLinkedinImage' => '', 'logo' => $this->logo, 'useremail' => $queue->recipients, 'email' => $email, 'phone' => '', 'body' => '', 'store_title' => $store->title, 'store_email' => $store->email,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>'','headerLogo'=>$this->mailBanner];
        $queue->from = $email;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);
        //die;
        return $queue->save();
    }
    
    public function getLogo() {
        return '<img  src=' . Yii::$app->params["rootUrl"] . '/store/site/logo/logo.png>';
    }
    
    public function getMailBanner() {
        return '<a><img src=' . Yii::$app->params["rootUrl"] . '/store/site/email/welcome-emai-bnrl.jpg></a>';
    }

}
