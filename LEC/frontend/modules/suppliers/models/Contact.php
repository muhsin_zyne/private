<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ContactForm extends \yii\base\Model
{
    public $name;
    public $email;
    public $phone;
    public $enquiry;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'enquiry'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['phone', 'safe'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],

            //['verifyCode', 'captcha','captchaAction'=>'/b2b/site/captcha'],
        ];
    }
    // public function sendContactMail() // send to admin
    // {
    //     $name=$this->name;
    //     $email=$this->email;
    //     //var_dump($this);die;
    //     $enquiry=$this->enquiry;
    //     if($this->phone!=NULL)
    //         {$phone=$this->phone;}
    //         else
    //         {$phone='Not set';}
    //     $template = \common\models\EmailTemplates::find()->where(['code' => 'b2bcontactus'])->one();
    //     $user= \common\models\User::find()->where(['roleId' => 1])->one();
    //     //var_dump($user);die;
    //     $useremail=$user->email;
    //     $queue = new \common\models\EmailQueue;
    //     $queue->models = ['user'=>$user]; 
    //     $queue->replacements =['EmailFacebookImage'=>'','EmailTwitterImage'=>'','EmailInstagramImage'=>'','facebookUrl'=>'','twitterUrl'=>'',
    //         'instagramUrl'=>'','Logo'=>'','useremail'=>$useremail,'name'=>$name,'email'=>$email,'phone'=>$phone,'enquiry'=>$enquiry,];
    //     $queue->from = "no-reply";
    //     $queue->recipients = $useremail;
    //     $queue->creatorUid = Yii::$app->user->id;
    //     $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
    //     $queue->save();
    // }
}