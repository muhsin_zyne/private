<?php

namespace frontend\modules\suppliers;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\suppliers\controllers';

    public function init()
    {
    	$this->viewPath = $_SERVER['DOCUMENT_ROOT'].'/themes/suppliers';
        parent::init();

        // custom initialization code goes here
    }
}
