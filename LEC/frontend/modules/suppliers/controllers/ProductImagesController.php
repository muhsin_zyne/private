<?php

namespace frontend\modules\suppliers\controllers;

use Yii;
use yii\web\UploadedFile;
use common\models\ProductImages;
use common\models\Products;
use common\models\User;
//use app\components\AdminController;


class ProductImagesController extends \frontend\components\SuppliersController
{
    public $uploadThumbnailPath;
    public $uploadSmallPath;
    public $uploadBasePath;
	public function init(){
        $this->uploadThumbnailPath = \Yii::$app->basePath."/../store/products/images/thumbnail/";
        $this->uploadSmallPath = \Yii::$app->basePath."/../store/products/images/small/";
        $this->uploadBasePath = \Yii::$app->basePath."/../store/products/images/base/";
        parent::init();
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpload()
    {
        //die('hai');
     	$random = $_GET['rand'];		
       
      	if(isset($_FILES)){    //die('hai');
	      	//$file = $_FILES['Products']['tmp_name']['images'];
	      	$file = $_FILES['Products']['name']['images'];
	      	$ext = pathinfo($file, PATHINFO_EXTENSION);
	      	if($ext == "jpg")
	      		$source_image = imagecreatefromjpeg($_FILES['Products']['tmp_name']['images']);
	      	if($ext == "png")
	      		$source_image = imagecreatefrompng($_FILES['Products']['tmp_name']['images']);
			$width = imagesx($source_image);
			$height = imagesy($source_image);

			$fileName = $random.'_'. basename($_FILES['Products']['name']['images']);
			
			$uploadThumbFile = $this->uploadThumbnailPath . $fileName;
			$uploadSmallFile = $this->uploadSmallPath . $fileName;
			$uploadBaseFile = $this->uploadBasePath . $fileName;
			
			//var_dump($uploadThumbFile);die();

			/*Thubnail image resizing*/

			$uploadfile = $this->uploadThumbnailPath . basename($_FILES['Products']['name']['images']);
			$thumb_width = 150;
			$thumb_height = 150;
			$virtual_image = imagecreatetruecolor($thumb_width, $thumb_height);
			
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
			$im = imagejpeg($virtual_image, $uploadThumbFile);

			//var_dump($im);die();

			/* Small Image*/

			$small_width = 350;
			$small_height = 350;
			$virtual_image = imagecreatetruecolor($small_width, $small_height);
			
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $small_width, $small_height, $width, $height);
			$im = imagejpeg($virtual_image, $uploadSmallFile); 

			/* Base Image*/

			$base_width = 600;
			$base_height = 600;
			$virtual_image = imagecreatetruecolor($base_width, $base_height);
			
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $base_width, $base_height, $width, $height);
			$im = imagejpeg($virtual_image, $uploadBaseFile); 
      	
      }
        
    }

}
?>