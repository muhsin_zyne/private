<?php

namespace frontend\modules\suppliers\controllers;

use Yii;
use yii\filters\VerbFilter;
use frontend\components\SuppliersController;
use frontend\modules\suppliers\models\Contact;
use frontend\models\ContactForm;


/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ContactsController extends SuppliersController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    
    public function actionIndex()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {//&& $model->validate()
                $model->sendSuppliersContactMail();
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                return $this->refresh();
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
}
