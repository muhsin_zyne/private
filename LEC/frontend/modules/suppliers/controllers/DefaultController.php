<?php

namespace frontend\modules\suppliers\controllers;

use Yii;
use common\models\User;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use frontend\components\SuppliersController;
use frontend\modules\suppliers\models\LoginForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\FrontController;
use common\models\Suppliers;
use common\models\BrandSuppliers;
use common\models\search\StoreBrandsSearch;
use common\models\Brands;
//use yii\web\Controller;

class DefaultController extends SuppliersController
{
    public $storeId;

    public function init(){ 

        //var_dump(Yii::$app->user->Id);die();

        //$this->storeId = ; // Store Id of the logge user
        parent::init();
    }

    public function actionIndex()
    {
        //die('index');
        //return $this->render('index');
        //return $this->redirect('index');
        $this->layout = "main";
      //return $this->render('index');
        return $this->redirect('/suppliers/orders/index');
        
    }

    public function actionLogin()
    {
    	//var_dump('hai');die(); 

        $this->layout = "login";
        //return $this->render('login');

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
           	return $this->redirect('/suppliers/orders');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
		Yii::$app->user->logout();
		return $this->redirect('login');
	}
    public function actionRequestPasswordReset()
    {
        $this->layout = "login";
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { 
            if ($model->sendEmail()) { //die('validated');
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                //die('validated');
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        //var_dump($model->geterrors());die;
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);

        //die('end');
    }

    public function actionResetPassword($token)
    {
        $this->layout = "login";
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        //var_dump(Yii::$app->request->post());die;
        
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            
            Yii::$app->getSession()->setFlash('success', 'New Password has been Saved.');

            return $this->goHome();
        }
        

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    public function actionBrand()
    {
        return $this->render('brand');
    }
    public function actionBrandproduct($id)
    {
        $model=Brands::findOne($id);
        $searchModel = new StoreBrandsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['brandId' => $id,'type'=>'b2c','enabled'=>'1'])->orderBy(['id'=>SORT_DESC,])->distinct('storeId');
        //$dataProvider->query->andWhere(['brandId' => $id,'type'=>'b2c','enabled'=>'1'])->orderBy(['id'=>SORT_DESC,])->distinct('storeId');
       //var_dump(expression)
        return $this->render('brandproduct', [
            'model'=>$model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
    public function actionExport()
    {
        //var_dump('hiii');die;
        $searchModel = new \common\models\search\StoreBrandsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $columns = [['class' => 'yii\grid\SerialColumn'],
            ['label'=>'Id','value' => function($model){ return   $model->id; }],
            ['label'=>'Store Name','value' => function($model){ return   $model->storeName; }],
            ['label'=>'Store Name','value' => function($model){ return   $model->storeEmail; }],
            //['label'=>'Brand','value' => function($model){ return   $model->brandName; }],
            //['label'=>'Name','value' => function($model){ return   $model->name; }],
            //['label'=>'Cost','format' => 'html','value' => function($model){ return   \backend\components\Helper::money($model->cost_price); }],
            //['label'=>'Retail Price','format' => 'html','value' => function($model){ return   \backend\components\Helper::money($model->price); }],
            
            ];
        return \common\components\CSVExport::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]); 
    }
}

