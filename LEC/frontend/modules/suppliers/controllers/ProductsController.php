<?php

namespace frontend\modules\suppliers\controllers;

use Yii;
use common\models\Products;
use common\models\Suppliers;
use common\models\User;
use common\models\ProductImages;
use common\models\search\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends \frontend\components\SuppliersController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
       
        $supplier = Suppliers::findOne(Yii::$app->user->Id);

        //$products = $supplier->getProducts();
        $products = $supplier->getDashboardProducts();

        $searchModel = new \common\models\search\ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        
        //$dataProvider1=str_replace("(Products.status = 1 AND Products.visibility = 'everywhere')","",$dataProvider->query->where);

        $dataProvider->query->select = array_merge(!isset($dataProvider->query->select)? [] : $dataProvider->query->select, !isset($products->select)? [] : $products->select);
        $dataProvider->query->join = array_merge(!isset($dataProvider->query->join)? [] : $dataProvider->query->join, !isset($products->join)? [] : $products->join);
        // var_dump($products->where);var_dump($dataProvider->query->where);   
        // echo "------";
        $dataProvider->query->andWhere($products->where);
       // var_dump($products->where);var_dump($dataProvider->query->where);die;
       // $dataProvider->query->where = $dataProvider->query->where." AND ".$products->where;
       // $dataProvider->query->orderBy(['Products.sku' => SORT_ASC]);
        if(isset($_GET['sort']) && strpos($_GET['sort'], "ezcode")!==FALSE){
            $dataProvider->query->join ('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $dataProvider->query->join ('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $dataProvider->query->andWhere('Attributes.code = "ezcode"');
           // $dataProvider->query->groupBy("Products.id");
        }
        $dataProvider->setSort([
            'attributes' => [
                'sku',
                'brandName' => [
                    'asc' => ['b.title' => SORT_ASC],
                    'desc' => ['b.title' => SORT_DESC]
                ],
                'ezcode' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ]
            ]
        ]);

        if(Yii::$app->request->post())  // only for status update
        {
            $status=$_POST["enabledisable"];
            //var_dump($_POST);die;
            if (!empty($_POST["Products"])) {
                foreach ($_POST["Products"] as  $product) {
                   $product= Products::findOne($product);
                   //var_dump($status);die;
                   $product->status=$status;
                   $product->save(false);
                }
                Yii::$app->getSession()->setFlash('success', 'Status Changed  Successfully!.....');
            }
            
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        
        if(\Yii::$app->request->isAjax){
          
            return $this->renderPartial('_formview',compact('model'));
        }
        
        
    }
    public function actionUpdate($id)
    {  //die('hai');
        $model = $this->findModel($id);

        $productImages = $model->productImages;
        $thumbnailImage = $model->thumbnailImage;
        $smallImage = $model->smallImage;
        $baseImage = $model->baseImage;

        $thumbImageUrl = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail";
        $smallImageUrl = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."small";
        $baseImageUrl = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."base";

        if(Yii::$app->request->post())
        {
           //var_dump(Yii::$app->request->post());die;

           $model->saveCustomAttributes(['price'=>$_POST['Products']['sellingPrice'], 'cost_price'=>$_POST['Products']['cost_price'], 
            'short_description'=>$_POST['Products']['short_description'], 'description'=>$_POST['Products']['description']], 0);
            Yii::$app->getSession()->setFlash('success', 'Product data updated successfully!.....');

            if(isset($_POST['Products']['deletedImages'])) {  
                foreach ($_POST['Products']['deletedImages'] as $key => $value) {   
                    $model_sb=ProductImages::find()->where(['id' => $value])->one();
                    $model_sb->delete();
                    $uploadThumbnailPath = \Yii::$app->basePath."/../store/products/images/thumbnail/";
                    $uploadSmallPath = \Yii::$app->basePath."/../store/products/images/small/";
                    $uploadBasePath = \Yii::$app->basePath."/../store/products/images/base/";
                    if (file_exists($thumbImage =  $uploadThumbnailPath.$value['name'])) {
                        unlink($thumbImage);
                    }    
                    if (file_exists($smallImage =  $uploadSmallPath.$value['name'])) {
                        unlink($smallImage);
                    }
                    if (file_exists($baseImage =  $uploadBasePath.$value['name'])) {
                        unlink($baseImage);
                    }    
                }
            }

            if(isset($_POST['Products']['images'])){
                
                if(isset($_POST['Products']['images']['thumbnail'])){
                    $thumbnailImageIndex = (int) $_POST['Products']['images']['thumbnail'];
                } 
                if(isset($_POST['Products']['images']['small_image'])){
                    $smallImageIndex = (int) $_POST['Products']['images']['small_image'];
                } 
                if(isset($_POST['Products']['images']['base_image'])){
                    $baseImageIndex = (int) $_POST['Products']['images']['base_image'];
                } 

                foreach ($_POST['Products']['images'] as $key => $value) {
                    if ($key === "old"){
                        foreach ($value as $index => $image) { 
                            if($productImage = ProductImages::find()->where(['id' => $image['imageId'], 'productId' => $model->id])->one()){
                                $productImage->sortOrder = $image['sort_order'];
                                if(isset($_POST['Products']['images']['thumbnail'])) {
                                    if($index == $thumbnailImageIndex)
                                        $productImage->isThumbnail = 1;
                                    else
                                        $productImage->isThumbnail = 0;
                                }  
                                if(isset($_POST['Products']['images']['small_image'])) { 
                                    if($index == $smallImageIndex)
                                        $productImage->isSmall =1;
                                    else
                                        $productImage->isSmall = 0;
                                }
                                if(isset($_POST['Products']['images']['base_image'])) {    
                                    if($index == $baseImageIndex)
                                        $productImage->isBase = 1;
                                    else
                                        $productImage->isBase = 0;
                                }
                                if(isset($image['exclude']))
                                    $productImage->exclude = 1;
                                else
                                    $productImage->exclude = 0; 

                                $productImage->save(false);          
                            }
                        }        
                    }  

                    else{
                        if($key !== "thumbnail" && $key !== "small_image" && $key !== "base_image"){
                            $productImage = new ProductImages;
                            $productImage->productId = $model->id;
                            if(isset($value['title']))
                                $productImage->path = $value['title'];
                            if(isset($value['sort_order']))
                                $productImage->sortOrder = $value['sort_order'];
                            if(isset($value['exclude']))
                                $productImage->exclude = 1;
                            if(isset($value['thumbnail']))
                                $productImage->isThumbnail = 1;
                            if(isset($value['base']))
                                $productImage->base =1;
                            if(isset($value['small']))
                                $productImage->isSmall =1;

                            $productImage->save(false); 
                        }           
                    } 
                }    
            }

            return $this->redirect(['index']);
         
        }
        else {
            return $this->render('update', compact('productImages','model','thumbImageUrl'));
        }
   
   } 
   public function actionExport()
   {
        //var_dump('hiii');die;
        $searchModel = new \common\models\search\ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $columns = [['class' => 'yii\grid\SerialColumn'],
            ['label'=>'SKU','value' => function($model){ return   $model->sku; }],
            ['label'=>'EZ code','value' => function($model){ return   $model->ezcode; }],
            
            ['label'=>'Brand','value' => function($model){ return   $model->brandName; }],
            ['label'=>'Name','value' => function($model){ return   $model->name; }],
            ['label'=>'Cost','format' => 'html','value' => function($model){ return   \backend\components\Helper::money($model->cost_price); }],
            ['label'=>'Retail Price','format' => 'html','value' => function($model){ return   \backend\components\Helper::money($model->price); }],
            //['label'=>'EZ code','value' => function($model){ return   $model->stau; }],
           // ['label'=>'SUBSITE STORE NAME','value' => function($model){ return $model->store->title; }],
            //['attribute' => 'orderDate','value' => function($model){ return \backend\components\Helper::date($model->orderDate); }],
            //['label'=>'NUMBER OF ITEMS','value' => function($model){ return  $model->orderItemsCount; }],
            //['label'=>'GRAND TOTAL','format' => 'html','value' => function($model){ return  \backend\components\Helper::money($model->grandTotal); }],
            //['label'=>'STATUS','value' => function($model){ return   $model->suppliersOrderStatus; }],
            ];
        return \common\components\CSVExport::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]); 
   }


    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
