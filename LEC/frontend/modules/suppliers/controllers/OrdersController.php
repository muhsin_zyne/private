<?php

namespace frontend\modules\suppliers\controllers;

use Yii;
use common\models\Orders;
use common\models\OrderItems;
use common\models\Suppliers;
use common\models\Shipments;
use common\models\ShipmentItems;
use common\models\search\OrdersSearch;
use common\models\search\OrderItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\components\SuppliersController;
use common\models\SalesComments;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;
use yii\db\ActiveQuery;
/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends SuppliersController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {

        $supplier = Suppliers::findOne(Yii::$app->user->Id);
        //var_dump(Yii::$app->user->Id);die;
        $orders = $supplier->getOrders();
        //var_dump($orders->where);die;

        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->select = array_merge(!isset($dataProvider->query->select)? [] : $dataProvider->query->select, !isset($orders->select)? [] : $orders->select);
        $dataProvider->query->join = array_merge(!isset($dataProvider->query->join)? [] : $dataProvider->query->join, !isset($orders->join)? [] : $orders->join);

        //$dataProvider->query->where = array_merge(!isset($dataProvider->query->where)? [] : $dataProvider->query->where, !isset($orders->where)? [] : $orders->where);
        //(is_array($dataProvider->query->where)) ? $dataProvider->query->where = $dataProvider->query->where : $dataProvider->query->where = $dataProvider->query->where." ".$orders->where;
            $condition=""; // only for search 
            if(isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]) && isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"]) &&  Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]!='' && Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"]!='')
            {
               //var_dump('hiii');die;
                $start_date = date(date('Y-m-d 00:00:00', strtotime(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"])));
                $end_date = date(date('Y-m-d 23:59:59', strtotime(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"])));
                $condition=$condition."AND Orders.orderDate BETWEEN '$start_date' AND '$end_date'";
            }

            if(isset(Yii::$app->request->queryParams["OrdersSearch"]["storeId"]) && Yii::$app->request->queryParams["OrdersSearch"]["storeId"]!=''){   
                $storeId=Yii::$app->request->queryParams["OrdersSearch"]["storeId"];
                $condition=$condition."AND Orders.storeId='$storeId'";
            }
            if(isset(Yii::$app->request->queryParams["OrdersSearch"]["id"]) && Yii::$app->request->queryParams["OrdersSearch"]["id"]!=''){   
                $id=Yii::$app->request->queryParams["OrdersSearch"]["id"];
                $condition=$condition."AND Orders.id='$id'";
            }
            $searchCondition = $supplier->getOrdersSuppliers($condition);
        
        $dataProvider->query->where=(is_array($dataProvider->query->where) && is_array($orders->where)) ? array_merge($dataProvider->query->where,$orders->where) : is_array($dataProvider->query->where) ? $searchCondition->where : $dataProvider->query->where." ".$orders->where;
        //$dataProvider->query->where = $dataProvider->query->where." ".$orders->where;*/
        //var_dump($a);die;
        
       
        $dataProvider->query->groupBy('Orders.id');
        $dataProvider->query->orderBy(['Orders.id' => SORT_DESC]);

        $dataProvider->query->orderBy(['Orders.id'=>SORT_DESC,]);
       //var_dump($dataProvider);die;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       

        $supplier = Suppliers::findOne(Yii::$app->user->Id);
        //var_dump(Yii::$app->user->Id);die;
        $orderitems = $supplier->getOrderItems($id);
        $searchModel = new OrderItemsSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['supplierUid' => Yii::$app->user->Id, 'orderId' => $id]),
        ]);

            //var_dump($orderitems);die;
            //$order = Orders::findOne($id);
            //$suid=Yii::$app->user->Id;
            //$orders = $order->getSuppliersOrderItems($suid);
            //var_dump($order->getSuppliersOrderItems($suid));die;

            
            /*$searchModel = new OrderItemsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            
            $dataProvider->query->select = array_merge(!isset($dataProvider->query->select)? [] : $dataProvider->query->select, !isset($orderitems->select)? [] : $orderitems->select);
            $dataProvider->query->join = array_merge(!isset($dataProvider->query->join)? [] : $dataProvider->query->join, !isset($orderitems->join)? [] : $orderitems->join);
            $dataProvider->query->where = array_merge(!isset($dataProvider->query->where)? [] : $dataProvider->query->where, !isset($orderitems->where)? [] : $orderitems->where);
            //var_dump($dataProvider);die;*/

        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'order','orderId' => $id]),
        ]);

           return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider_salescomments' => $dataProvider_salescomments,

        ]);
    }
    // public function actionShip()
    // {
    //     $model = new Shipments();

    //     if(Yii::$app->request->post())
    //     {
    //         //var_dump(Yii::$app->request->post());die;
    //         $model->orderId=$_POST['Shipments']['orderId'];
    //         $model->title=$_POST['Shipments']['title'];
    //         $model->carrier=$_POST['Shipments']['carrier'];
    //         $model->trackingNumber=$_POST['Shipments']['trackingNumber'];
            
    //         $model->save();
    //         $shipeditems=explode(',', $_POST['shipitems']);
    //         //var_dump($shipeditems);die;
    //         foreach ($shipeditems as  $item) {
    //             $shipmentitems= new ShipmentItems();
    //             $shipmentitems->shipmentId=$model->id;
    //             $shipmentitems->orderItemId=$item;
    //             $orderItems = OrderItems::find()->where(['id' => $item])->one(); 
    //             $shipmentitems->quantityShipped=$orderItems->quantity;
    //             $shipmentitems->save();
               
    //             $orderItems->status='shipped';
    //             $orderItems->save();
    //         }
    //          //$model->sendSuppliersShipmentMail();
    //         return $this->redirect(['view', 'id' => $model->orderId]);
    //     }
        
    //     if(\Yii::$app->request->isAjax){
    //         return $this->renderPartial('_form',compact('model'));
    //     }
    //     else 
    //     {
    //         return $this->render('ship', [ 'model' => $model]);
           
    //     } 
    // }
    
    public function actionShip($id)
    {
        /*$model = new Shipments();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $shipeditems=explode(',', $_POST['shipitems']);
            //var_dump($shipeditems);die;
            foreach ($shipeditems as  $item) {
                $shipmentitems= new ShipmentItems();
                $shipmentitems->shipmentId=$model->id;
                $shipmentitems->orderItemId=$item;
                $orderItems = OrderItems::find()->where(['id' => $item])->one(); 
                $shipmentitems->quantityShipped=$orderItems->quantity;
                $shipmentitems->save();
               
                $orderItems->status='shipped';
                $orderItems->save();    
               
            }
            //return $this->redirect(['view', 'id' => $model->orderId]);
        }else
        if(\Yii::$app->request->isAjax){
            return $this->renderPartial('_form',compact('model','id'));
         }
         
          else {
            return $this->render('ship', [
                'model' => $model,
            ]);
        }

        else 
        {
            return $this->render('ship', [ 'model' => $model]);
           
        } */

    }

    
   public function actionCreate()
    {
        
        //Shipments::sendSuppliersShipmentMail();

        /*$model = new Shipments();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        if(\Yii::$app->request->isAjax){
            return $this->renderPartial('_form',compact('model'));
        }
        else 
        {
            return $this->render('ship', [ 'model' => $model]);
           
        } */
    }

    public function actionExportorderitems($id)
    {
        $searchModel = new OrderItemsSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId'=>$id,'supplierUid'=>Yii::$app->user->Id]),
        ]);
        
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination = false;
        $model= $this->findModel($id);
        $supplier = Suppliers::findOne(Yii::$app->user->Id);
        $orderitems = $supplier->getOrderItems($id);
        $command = $orderitems->createCommand();
        $orderItems = $command->queryAll();
        $mailstatus=($model->suppliersOrderStatus=='Acknowledged')? 1 : 0;
        foreach ($orderItems as $key => $orderItem) {
            $model_oi=OrderItems::findOne($orderItem['id']);
            $model_oi->status='acknowledged';
            $model_oi->save(false);
        }
        if($mailstatus==0)
        {
            $model->sendSupplierAcknowledgeMail();
            //Yii::$app->getSession()->setFlash('success', 'The Order has been marked as Acknowledged and customer has been notified.!...');
        }
       
        //var_dump($model);die;
        echo \common\components\CSVExport::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'columns' => [
                //'id',
                //'orderId',
                [
                    'label'=> 'Item Orderd',
                    'format' => 'html',
                    'value' => 'product.name'
                ],
                [
                    'label' => 'Option(s)',
                    'format' => 'html',
                    'value' => 'superAttributeValuesText',
                ],
               /* [
                    'label'=> 'SKU',
                    'value' => 'sku'
                ],*/
                [
                    'label'=> 'Billing Address',
                    'format' => 'html',
                    'value' => 'b2baddress.billingAddress'
                ],
                [
                    'label'=> 'Shipping Address',
                    'format' => 'html',
                    'value' => 'b2baddress.shippingAddress'
                ],
                [
                    'label' => 'Price',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money($model->price);
                    },
                ],
                [
                    'label' => 'Qty',
                    'value' => function ($model) {
                        return $model->quantity;
                    },
                ],
                [
                    'label' => 'Sub Total',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money($model->price * $model->quantity);
                    },
                ],
            ],
        ]);
        //die('hiii');
        return $this->redirect(['view','id'=>$model->id]);
    }
    public function actionExport()
    {
        $this->render('/orders/csvexport');
        /*$searchModel = new \common\models\search\OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->pagination = false;
        //$condition = (Yii::$app->user->identity->roleId == '3')? ['storeId' => Yii::$app->user->identity->store->id, 'roleId' => 4] : ['roleId' => 4];
        //$dataProvider->query->andWhere($condition);
        $columns = [['class' => 'yii\grid\SerialColumn'],
            ['label'=>'ORDER #','value' => function($model){ return   $model->id; }],
            ['label'=>'SUBSITE STORE NAME','value' => function($model){ return $model->store->title; }],
            ['attribute' => 'orderDate','value' => function($model){ return \backend\components\Helper::date($model->orderDate); }],
            ['label'=>'NUMBER OF ITEMS','value' => function($model){ return  $model->orderItemsCount; }],
            ['label'=>'GRAND TOTAL','format' => 'html','value' => function($model){ return  \backend\components\Helper::money($model->grandTotal); }],
            ['label'=>'STATUS','value' => function($model){ return   $model->suppliersOrderStatus; }],
            ];
        return \common\components\CSVExport::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]); */
        
    }

    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionComments(){
        $model = new SalesComments();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if($_POST['SalesComments']['notify']==1)
            {
                $model->sendSuppliersSalesCommentMail();
                Yii::$app->getSession()->setFlash('success', 'Your comment has been sent successfully!');
                return $this->redirect(['view','id'=>$model->orderId]);
            }
            Yii::$app->getSession()->setFlash('success', 'Your comment has been sent successfully!.');
            return $this->redirect(['view','id'=>$model->orderId]);  
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionAcknowledge($id){
        $model= $this->findModel($id);
        $supplier = Suppliers::findOne(Yii::$app->user->Id);
        $orderitems = $supplier->getOrderItems($id);
        $command = $orderitems->createCommand();
        $orderItems = $command->queryAll();
        foreach ($orderItems as $key => $orderItem) {
            $model_oi=OrderItems::findOne($orderItem['id']);
            $model_oi->status='acknowledged';
            $model_oi->save(false);
        }
        
        $model->sendSupplierAcknowledgeMail();
        Yii::$app->getSession()->setFlash('success', 'The Order has been marked as Acknowledged and customer has been notified.');
        return $this->redirect(['view','id'=>$model->id]);
    }
   
}

