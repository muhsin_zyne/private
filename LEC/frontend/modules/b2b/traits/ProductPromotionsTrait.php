<?php
namespace frontend\modules\b2b\traits;
use yii\helpers\ArrayHelper;
use common\models\ConsumerPromotionProducts;

trait ProductPromotionsTrait{
	public function getProductPromotions($dataProvider){
		$products = ArrayHelper::map($dataProvider->models, 'id' , 'id');
		$today = date("Y-m-d H:i:s");
		$promotionProducts = ArrayHelper::map(ConsumerPromotionProducts::find()->where(['productId' => $products])->groupBy('ConsumerPromotionProducts.id')->all(), 'id', 'self');

		//$promotionProducts = ArrayHelper::map(ConsumerPromotionProducts::find()->join('JOIN','ConsumerPromotions as cp','cp.id = ConsumerPromotionProducts.promotionId')->where('productId in ('.implode(",", $products).')')->groupBy('ConsumerPromotionProducts.id')->all(), 'id', 'self');

		//var_dump($promotionProducts);die();

		$productsPromoton = Array();

		foreach ($promotionProducts as $cpp => $details) 
		{  
			if(isset($productsPromoton[$details->productId])){
				//array_push($productsPromoton[$details->productId], $details->promotionDetails);
				$productsPromoton[$details->productId][] = $details->promotionDetails;
			}
			else{
				$productsPromoton[$details->productId] = [$details->promotionDetails];
			}
		}

		return $productsPromoton;

			
	}
}	
?>