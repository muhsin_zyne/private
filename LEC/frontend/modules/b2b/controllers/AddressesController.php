<?php

namespace frontend\modules\b2b\controllers;

use Yii;
use frontend\components\B2bController;
use common\models\B2bAddresses;
use common\models\User;

class AddressesController extends B2bController
{
    public function actionIndex()
    {
        $address = new B2bAddresses();
        return $this->render('index',compact('address'));
    }

    public function actionCreate(){     
    	$address = new B2bAddresses();
    	$id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
       	if ($address->load(Yii::$app->request->post())) { 
            $tradingHoursArray = array(
                array(
                    'monday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayTo']
                    ),
                    'tuesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayTo']
                    ),
                    'wednesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayTo']
                    ),
                    'thursday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayTo']
                    ),
                    'friday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayTo']
                    ),
                    'saturday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayTo']
                    ),
                    'sunday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayTo']
                    ),
                ),
            );
            $address->tradingHours=json_encode(array_values($tradingHoursArray));           
            if(Yii::$app->request->post()["B2bAddresses"]["defaultAddress"] == "1"){ 
                if($default = B2bAddresses::find()->where(['ownerId'=>$user->id,'defaultAddress'=>'1'])->one()){               
                    $default->defaultAddress = "0";
                    $default->save(false);
                }
            } 
        	$address->ownerId = $user->id;
            $address->save();
            return $this->redirect(['index']);
        }
		return $this->render('index', [
            'address' => $address,
        ]);
    }

    public function actionUpdate($id){   
        $address = $this->findModel($id);
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
        if ($address->load(Yii::$app->request->post())) {
            $tradingHoursArray = array(
                array(
                    'monday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayTo']
                    ),
                    'tuesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayTo']
                    ),
                    'wednesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayTo']
                    ),
                    'thursday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayTo']
                    ),
                    'friday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayTo']
                    ),
                    'saturday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayTo']
                    ),
                    'sunday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayTo']
                    ),
                ),
            );
            $address->tradingHours=json_encode(array_values($tradingHoursArray)); 
            if(Yii::$app->request->post()["B2bAddresses"]["defaultAddress"] == "1"){ 
                if($default = B2bAddresses::find()->where(['ownerId'=>$user->id,'defaultAddress'=>'1'])->one()){               
                    $default->defaultAddress = "0";
                    $default->save(false);
                }
            }
            $address->save(false);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'address' => $address,
            ]);
        }
    }

    public function actionDelete($id){ 
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($address = B2bAddresses::findOne($id)) !== null) {
            return $address;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

?>