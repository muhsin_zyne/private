<?php

namespace frontend\modules\b2b\controllers;
use Yii;
use common\models\Products;
use common\models\ProductsSearch;
use common\models\AttributeOptionPrices;
use common\models\AttributeValues;
use common\models\Stores;
use frontend\components\B2bController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;
use yii\db\ActiveQuery;

class ProductsController extends B2bController
{

	 public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionView($id)
    {

        $product = $this->findModel($id);

        //var_dump(explode('.',$product->getThumbnailImage()));die();

        $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$product->getThumbnailImage();
        $baseImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."base/".$product->getBaseImage();
        $smallImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."small/".$product->getSmallImage();

        $dataProvider = new ActiveDataProvider([
        'query' => Products::find()->orderBy('rand()')->limit(4),
        'pagination' => false,
        ]);

        if(isset($_GET['options']) && $product->typeId == "configurable"){
            $_REQUEST['Products']['attributeSetId'] = $product->attributeSetId;
            if(is_array($_GET['options'])){
                foreach($_GET['options'] as $attr => $option){
                    $product->$attr = $option;
                }
            }
        }
        return $this->render('view', compact('product', 'dataProvider','thumbImagePath','baseImagePath','smallImagePath'));

    }

    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetprice()
    {
        $tot_price = 0;
        if(isset($_REQUEST['price'])) 
            $price = $_REQUEST['price']; 
        if($tot_price == 0)  
            $tot_price = $_REQUEST['price']; 
        if(isset($_REQUEST['selected']) && !empty($_REQUEST['selected'])) { 
            foreach ($_REQUEST['selected'] as $productId) { var_dump(Products::findOne($productId));die();
                $prodPrice = Products::findOne($productId)->price; //var_dump($prodPrice);die();
                    if(isset($_REQUEST['selectedqty'])){ 
                        foreach ($_REQUEST['selectedqty'] as $qty) {
                            if(key($qty) == (int)$productId) 
                                $quantity = $qty[(int)$productId];
                            else    
                                $quantity = 1;
                        }
                        $tot_price = $tot_price+($quantity*$prodPrice);
                     }
                    else 
                        $tot_price = $tot_price+$prodPrice; 
            }
            return Helper::money($tot_price);
            $tot_price = 0;
        }
        else  
            return Helper::money($price);
    }    

     public function actionGetconfigprice()
    {
        $tot_price = 0;    
        if(isset($_REQUEST['selected_attr'])&& !empty($_REQUEST['selected_attr'])) {
            $price = $_REQUEST['base_price'];
            foreach ($_REQUEST['selected_attr'] as $productId) {
                var_dump($productId);die();
             //$attrPrice = AttributeOptionPrices::findOne()
            }    

        }
    }

    public function actionGetproducts($limit=null){
        $keyword = $_REQUEST['keyword'];
        $store = Stores::findOne(Yii::$app->params['storeId']);
        if(Yii::$app->module->id!="b2b")
            $query = $store->getProducts();
        else
            $query = new ActiveQuery('\common\models\Products');
        if(isset($keyword))
            $query->where("Products.sku like '%".$keyword."%'");
        if(isset($limit))
            $query->limit($limit);
        $products = $query->all();  
        $data = [];
        foreach($products as $product)  
            $data[$product->id] = ['id'=>$product->id,'name'=>$product->name, 'sku'=>$product->sku, 'price'=>Helper::money($product->price), 'cost_price'=>Helper::money($product->cost_price), 'type'=>$product->typeId];
        return json_encode($data); 
    }   
    

    public function actionConfig($pid){
        $product = Products::findOne($pid);
        //var_dump($product);die();
        return $this->renderPartial('_config', compact('product'));
    }
}
?>
