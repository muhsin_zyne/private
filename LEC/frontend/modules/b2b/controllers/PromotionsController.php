<?php

namespace frontend\modules\b2b\controllers;

use Yii;
use frontend\components\B2bController;
use common\models\B2bAddresses;
use common\models\User;
use frontend\models\ResetPasswordForm;
use common\models\StorePromotions;
use common\models\ConsumerPromotions;
use common\models\ConsumerPromotionProducts;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use frontend\components\Helper;

class PromotionsController extends B2bController
{
    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;

        $search = new ConsumerPromotions;
        $dataProvider = new ActiveDataProvider([
            'query' => ConsumerPromotions::find()->where('id not in (SELECT promotionId from StorePromotions where storeId = "'.$storeId.'") and status="published"')
            ]);

        return $this->render('index',compact('dataProvider'));
    }

    public function actionView($id)
    {
        
        //var_dump($id);die();

        $promotion = ConsumerPromotions::findOne($id);
    	$user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;

        $search = new ConsumerPromotionProducts;
        /*$dataProvider = new ActiveDataProvider([
            'query' => ConsumerPromotionProducts::find()->join('join', 'StoreProducts as sp', 'sp.productId = ConsumerPromotionProducts.productId')->where(['promotionId'=>$id,'sp.storeid'=>$storeId]),
            'pagination' => [
                'pageSize' => 10,
            ],    
            ]);*/

        $dataProvider = new ActiveDataProvider([
            'query' => ConsumerPromotionProducts::find()->where(['promotionId'=>$id]),
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 20,
            ],    
            ]);

        return $this->render('view',compact('dataProvider','promotion'));
    }

    public function actionProducts($id)
    {
        $promotion = $this->findModel($id);
        $query = $promotion->getProducts(0);
        if(isset($_GET['sort']) && (strpos($_GET['sort'], "name")!==FALSE || strpos($_GET['sort'], "ezcode")!==FALSE)){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->groupBy("Products.id");
            if(strpos($_GET['sort'], "name")!==FALSE){
                $query->andWhere('Attributes.code = "name"');
            }
            elseif(strpos($_GET['sort'], "ezcode")!==FALSE){
                $query->andWhere('Attributes.code = "ezcode"');
            } 
        }
        $products = new ActiveDataProvider([
            'query' => $query, 
            'pagination' => [
            'pageSize' => 10,
            ],
            /*'sort' => [
                'defaultOrder' => [
                    'cpp.pageId' => SORT_DESC, 
                ]
            ],*/
        ]);
        $products->setSort([
            'attributes' => [

                'dateAdded',
                'sku',
                'price' => [
                    'asc' => ['cpp.offerCostPrice' => SORT_ASC],
                    'desc' => ['cpp.offerCostPrice' => SORT_DESC]
                ],
                'name' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'ezcode' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ]
            ]
        ]);
        return $this->render('products', compact('products','promotion'));
    }

    public function actionAccept($id)
    {
    	
        if (isset($_POST['StorePromotion']))  {
    		
    		$user = User::findOne(Yii::$app->user->Id);
        	$storeid = $user->store->id;

            $storePromotions = new StorePromotions();
            $storePromotions->storeId =  $storeid;
            $storePromotions->promotionId = $id;
            $storePromotions->startDate = Helper::convertdate($_POST['StorePromotion']['startDate']);
            $storePromotions->endDate = Helper::convertdate($_POST['StorePromotion']['endDate']);
            $storePromotions->active = "1";
            $storePromotions->save(false);
            return $this->redirect(\yii\helpers\Url::to(['promotions/index']));
            //--------------------------------mail send to admin -----------------
            
        }
    }	
    public function actionTest()
    {
            $c_p_id = 1;
            $consumerpromotion = ConsumerPromotions::findOne($c_p_id);
             $consumerpromotion->endPromotion();
            $consumerpromotion->adminAcceptPromotionMail();
            $consumerpromotion->b2buserAcceptPromotionMail();
            
          
    }

    protected function findModel($id)
    {
        if (($model = ConsumerPromotions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
