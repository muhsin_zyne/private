<?php   

namespace frontend\modules\b2b\controllers;

use Yii;
use common\models\User;
use common\models\Banners;
use frontend\components\B2bController;
use frontend\modules\b2b\models\LoginForm; 
use common\models\Products;
use common\models\search\ProductsSearch;
use common\models\Categories;
use yii\data\ActiveDataProvider;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use frontend\models\ContactForm;
use common\models\AttributeValues;
use common\models\EmailQueue;
use common\models\Brands;
use yii\db\Query;
class DefaultController extends B2bController
{
    public $storeId;

    public function init(){ 

        //var_dump(Yii::$app->user->Id);die();

        //$this->storeId = ; // Store Id of the logge user
        parent::init();
    }

    public function actionIndex()
    {
        
        $query = new Query;  // main banner
        $query->select(['StoreBanners.bannerId AS sbid'])->orderBy(['StoreBanners.position' => SORT_ASC])->where(['storeId' => -1])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2b-main' and StoreBanners.storeId = -1");
        $command = $query->createCommand();
        $mainbanner = $command->queryAll();   
        
        $query = new Query; // mini banner
        $query  ->select(['StoreBanners.bannerId AS sbid' ] )->orderBy(['rand()' => SORT_DESC])->where(['storeId' => -1])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2b-mini' and StoreBanners.storeId = -1")->LIMIT(2);;
        $command = $query->createCommand();
        $minibanner = $command->queryAll();  
		//var_dump($minibanner);die;
        $query = new Query; //side banner
        
        $brands =new ActiveDataProvider([
            'query' => Brands::find()->where('path!=""'),
            'pagination' => false,
        ]);
        return $this->render('index',compact('mainbanner','minibanner','brands'));     
    }

    public function actionLogin()
    {
    	//var_dump('hai');die(); 

        $this->layout = "login";
        //return $this->render('login');

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
           	return $this->redirect('?r=b2b');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
		Yii::$app->user->logout();
		return $this->goHome();
	}
    public function actionRequestPasswordReset()
    {   //die('hii');
        $this->layout = "login";
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { 
            if ($model->sendEmail()) { //die('validated');
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                //die('validated');
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        //var_dump($model->geterrors());die;
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);

        //die('end');
    }

    public function actionResetPassword($token)
    {
        //var_dump($token);die;
        $this->layout = "login";
	$model = new \common\models\ResetPasswordForm($token);
        if(!$user = User::findByPasswordResetToken($token)){
		Yii::$app->session->setFlash('error', 'Wrong password reset token.');
		return $this->redirect(\yii\helpers\Url::to(['default/login']));
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            
            Yii::$app->getSession()->setFlash('success', ' Your new password was sucessfully saved.');

            return $this->goHome();
        }
        

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionSlug($slug){ //var_dump($slug); die;
        if($category = \common\models\Categories::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('b2b/categories/products', ['categoryId' => $category->id]);
        }elseif($product = \common\models\Products::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('b2b/products/view', ['id' => $product->id]);
        }elseif($page = \common\models\CmsPages::find()->where(['slug' => $slug])->one()){
            return Yii::$app->runAction('b2b/cms/view', ['id' => $page->id]);
        }else{
            throw new \yii\web\NotFoundHttpException('Page Not Found');
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [ 
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionContact()
    {
        $model = new ContactForm();
        //
        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {//&& $model->validate()
                $model->sendB2bContactMail();
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                return $this->refresh();
        } else {
        	//var_dump($model->geterrors());die;

            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSearch($q) 
    {
        $_REQUEST = yii\helpers\ArrayHelper::merge(['keyword' => $q, 'Products' => ['priceRange' => ['min' => '0', 'max' => '99999999']]], $_REQUEST);
        $options = [];
        $searchModel = new \common\models\search\ProductsSearch();
        $childProductQuery = new \yii\db\ActiveQuery('\common\models\Products');
        //$childProductQuery->join('LEFT JOIN', 'ProductLinkages pl', 'Products.id = pl.productId');
        $childProductQuery->where("sku LIKE '%$q%' AND visibility = 'not-visible-individually'");
        $childProducts = yii\helpers\ArrayHelper::map($childProductQuery->all(),'parent.id', 'self');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(count($childProducts) > 0){
            $dataProvider->query->orWhere("Products.id in ('".implode(',', array_keys($childProducts))."')");
            foreach($childProducts as $parentId => $product){
                $superAttrs = \common\models\ProductSuperAttributes::find()->where(['productId' => $parentId])->all();
                foreach($superAttrs as $attr){
                    $aop = \common\models\AttributeOptionPrices::find()->where(['attributeId' => $attr->attributeId, 'productId' => $parentId])->one();
                    $options[$parentId] = ['options' => [$attr->attr->code => $aop->id]];
                }
            }
        } 
        return $this->render('search', compact('searchModel', 'dataProvider', 'options'));

    }

    public function actionTestmail()
    {
        $EmailQueue = EmailQueue::find()->where(['id' => '36'])->one();
        echo $EmailQueue->message;exit;
    }
    
    public function actionLoginAndForward($uid, $pid, $authString=null){
        $user = User::findOne($uid);
        if($authString == md5($user->email.':'.$user->password_hash) && Yii::$app->user->login(User::findOne($uid), 0)){
                $this->redirect(\yii\helpers\Url::to(['/b2b/products/view', 'id' => $pid]));
        }
        else {
            $this->redirect(\yii\helpers\Url::to(['/b2b/products/view', 'id' => $pid]));
        }
    }
}
?>
