<?php 

namespace frontend\modules\b2b\controllers;

use Yii;
use frontend\components\B2bController;
use common\models\Categories;
use common\models\Products;
use common\models\User;
use common\models\Suppliers;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;

class SuppliersController extends B2bController
{
    use \frontend\modules\b2b\traits\ProductPromotionsTrait;    

    public function actionIndex()
    {
        return $this->render('products');
    }

    /*public function actionProducts($id)
    {
    	$supplier = Suppliers::findOne($id);
    	$user = User::findOne(Yii::$app->user->Id);
    	$storeId = $user->store->id;

    	$products = new ActiveDataProvider([
    		'query' => $supplier->getProducts(),
            'pagination' => [
            'pageSize' => 10,
            ],
        ]);

       // var_dump($products);

    	return $this->render('products', compact('products','category'));
	}*/

    public function actionProducts($id)
    {
        $supplier = Suppliers::findOne($id);
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = isset($user->store->id)? $user->store->id : null;
        $searchModel = new \common\models\search\ProductsSearch();
        $searchModel->filterByCategories = false;
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        if(\Yii::$app->request->isAjax){
            $productsQuery = $supplier->getProducts($storeId);
            $totalCount = $productsQuery->count();
        }else{
            $productsQuery = new \yii\db\ActiveQuery('common\models\Products');
        }
        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        if(isset($_GET['sort']) && (strpos($_GET['sort'], "name")!==FALSE || strpos($_GET['sort'], "ezcode")!==FALSE || strpos($_GET['sort'], "price")!==FALSE)){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->groupBy("Products.id");
            if(strpos($_GET['sort'], "name")!==FALSE){
                $query->andWhere('Attributes.code = "name"');
            }
            elseif(strpos($_GET['sort'], "ezcode")!==FALSE){
                $query->andWhere('Attributes.code = "ezcode"');
            }
            elseif(strpos($_GET['sort'], "price")!==FALSE){
                $query->andWhere('Attributes.code = "cost_price"');
            }
        }
        $products = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 20,
            ],
        ]);

        $products->setSort([
            'attributes' => [

                'dateAdded',
                'sku',
                'price' => [
                    'asc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_ASC],
                    'desc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_DESC]
                ],
                'name' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'ezcode' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ]
            ]
        ]);
        if(\Yii::$app->request->isAjax){
            $promotionProductsArray = $this->getProductPromotions($products);
            return $this->renderAjax('products', compact('products','category','supplier','promotionProductsArray'));
        }
        else
            return $this->render('products', compact('products','category','supplier'));

    }    	
}    

?>