<?php
namespace frontend\modules\b2b\controllers;
use Yii;
use common\models\Conferences;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;


class ConferencesController extends \frontend\components\B2bController
{
	public function actionView($id){
		$conference = $this->findModel($id);

		//var_dump($conference->getTrayProducts());die();

        $conferencetrays = $conference->getConferenceTrays()->all();
        
        if(Yii::$app->request->isAjax){
            $searchModel = new \common\models\search\ProductsSearch();
            $searchModel->filterByCategories = false;
            $query = $searchModel->search(Yii::$app->request->queryParams)->query;
            $productsQuery = $conference->getTrayProducts();
            $query->join = $productsQuery->join;
            $query->andWhere($productsQuery->where);
            if(isset($_GET['sort']) && (strpos($_GET['sort'], "name")!==FALSE || strpos($_GET['sort'], "ezcode")!==FALSE || strpos($_GET['sort'], "price")!==FALSE)){
                $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
                $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
                $query->groupBy("Products.id");
                if(strpos($_GET['sort'], "name")!==FALSE){
                    $query->andWhere('Attributes.code = "name"');
                }
                elseif(strpos($_GET['sort'], "ezcode")!==FALSE){
                    $query->andWhere('Attributes.code = "ezcode"');
                }
                elseif(strpos($_GET['sort'], "price")!==FALSE){
                    $query->andWhere('Attributes.code = "cost_price"');
                }
            }
            $products = new ActiveDataProvider([
                'query' => $query, 
                'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 20,
                ],
            ]);
            $products->setSort([
            'attributes' => [

                'dateAdded',
                'sku',
                'price' => [
                    'asc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_ASC],
                    'desc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_DESC]
                ],
                'name' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'ezcode' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ]
            ]
        ]);
            return $this->renderPartial('_list', compact('products','conferencetrays','conference'));
        }
        return $this->render('view', compact('conferencetrays','conference'));
	}

	public function actionIndex($id){ 
		$conference = $this->findModel($id); 
        $conferencetrays = $conference->getConferenceTrays()->all();
        $trays = new ActiveDataProvider([
            'query' => $conference->getConferenceTrays(), 
        ]);
        return $this->render('conferencetraylist', compact('conferencetrays','conference','trays'));
	}	 

    public function actionTrayproducts($id){
        //var_dump($_POST['trayId']);die;

        $tray = $_REQUEST['trayId'];

        $conference = $this->findModel($id);
        $conferencetrays = $conference->getConferenceTrays()->all();
        $searchModel = new \common\models\search\ProductsSearch();
        $searchModel->filterByCategories = false;
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        $productsQuery = $conference->getTrayProducts($tray);
        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        if(isset($_GET['sort']) && (strpos($_GET['sort'], "name")!==FALSE || strpos($_GET['sort'], "ezcode")!==FALSE || strpos($_GET['sort'], "price")!==FALSE)){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->groupBy("Products.id");
            if(strpos($_GET['sort'], "name")!==FALSE){
                $query->andWhere('Attributes.code = "name"');
            }
            elseif(strpos($_GET['sort'], "ezcode")!==FALSE){
                $query->andWhere('Attributes.code = "ezcode"');
            }
            elseif(strpos($_GET['sort'], "price")!==FALSE){
                $query->andWhere('Attributes.code = "cost_price"');
            }
        }
        $products = new ActiveDataProvider([
            'query' => $query, 
            'pagination' => [
            'pageSize' => 10,
            ],
        ]);
        $products->setSort([
            'attributes' => [

                'dateAdded',
                'sku',
                'price' => [
                    'asc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_ASC],
                    'desc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_DESC]
                ],
                'name' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'ezcode' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ]
            ]
        ]);
        return $this->render('view', compact('products','conferencetrays','conference','tray'));

        return $this->redirect(['Conferences/view','id'=>$conference->id]);

    }

	protected function findModel($id)
    {
        if (($model = Conferences::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
?>