<?php 

namespace frontend\modules\b2b\controllers;

use Yii;
use frontend\components\B2bController;
use common\models\B2bAddresses;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\User;
use common\models\StorePromotions;
use common\models\ConsumerPromotions;
use common\models\Products;
use yii\data\ActiveDataProvider;
use common\models\Orders;
use common\models\OrderItems;
use common\models\ChangePasswordForm;

class AccountController extends B2bController
{
    public function actionIndex() 
    {
        $userid=Yii::$app->user->Id;
        $query=OrderItems::find();
        $query->join('JOIN', 'Orders as o', 'o.id = OrderItems.orderId');
        $query->where("(o.type = 'b2b' AND o.customerId =$userid )");
        $query->orderBy(['id'=>SORT_DESC,]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        return $this->render('index',[
            'dataProvider'=>$dataProvider,
            ]);
    }

    public function actionChangepassword(){
        $model = new ChangePasswordForm;
        $user = User::findOne(Yii::$app->user->id);
      
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                    $user->password_hash = $user->setPassword($_POST['ChangePasswordForm']['newpass']);
                    if($user->save(false)){
                        Yii::$app->getSession()->setFlash(
                            'success','Password changed'
                        );
                        return $this->redirect(['index']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                        return $this->redirect(['index']);
                    }
            }else{
                return $this->render('changepassword',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('changepassword',[
                'model'=>$model
            ]);
        }
    }


    public function actionPromotions()
    {
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $promotions = ConsumerPromotions::find()->where('id not in (SELECT promotionId from StorePromotions where storeId != "'.$storeId.'")')->all();
        return $this->render('promotions',compact('promotions'));
    } 
   public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { //die('validated');
            if ($model->sendEmail()) { //die('validated');
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                //die('validated');
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        //die('hai');
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);

        //die('end');
    } 

    public function actionOrder(){
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $products = Products::find()->all();
        //var_dump($products);die();
        $addresses = $user->b2bAddresses;
        //var_dump($addresses);die();
        return $this->render('order',compact('products','addresses'));
    }

    public function actionOrders(){
        $userid=Yii::$app->user->Id;
        $query=OrderItems::find();
        $query->join('JOIN', 'Orders as o', 'o.id = OrderItems.orderId');
        $query->where("(o.type = 'b2b' AND o.customerId =$userid )");
        $query->orderBy(['id'=>SORT_DESC,]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('orders',[
            'dataProvider'=>$dataProvider,
            ]);
    }

}    

?>