<?php  
namespace frontend\modules\b2b\controllers;

use Yii;
use frontend\components\B2bController;
use common\models\Categories;
use common\models\Products;
use common\models\ConsumerPromotions;
use common\models\User;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;



//var_dump($storeId);die();

class CategoriesController extends B2bController
{
    use \frontend\modules\b2b\traits\ProductPromotionsTrait;
    public function actionIndex()
    {
        return $this->render('products');
    }

    public function actionProducts($categoryId)
    { 
        /*if(!$category = Categories::findOne($categoryId))
            throw new \yii\web\NotFoundHttpException('Page Not Found');
        $searchModel = new \common\models\search\ProductsSearch(); 
        
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        
        $productsQuery = $category->getProducts(123);
        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);

        if(isset($_GET['sort']) && (strpos($_GET['sort'], "dateAdded")===FALSE && strpos($_GET['sort'], "sku")===FALSE)){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->andWhere('Attributes.code = "'.str_replace("-","",$_GET['sort']).'"');
            $query->groupBy("Products.id");
        }

        $products = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 50,
            ],
        ]);
        
        $products->setSort([
            'attributes' => [
                'name'=> [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'ezcode'=> [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'sku',
                'dateAdded',
                'price' => [
                    'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                ]
            ]
        ]);
        if(\Yii::$app->request->isAjax) { //die('skdjfhhhhh');
            
            return $this->renderAjax('products', compact('products','category'));
            //die('hai');
            
        }    
        else
            return $this->render('products', compact('products','category'));
        

		/*$user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;

   		/*$products = new ActiveDataProvider([
    		'query' => $category->getProducts($storeId)->andWhere('typeId="configurable"'),
            'pagination' => [
            'pageSize' => 10,
            ],
        ]);*/

        //var_dump($products);die();
        
        //return $this->render('products', compact('products','category'));

        $category = (is_null($categoryId) && is_null($slug))? false : (!is_null($categoryId)? Categories::findOne($categoryId) : Categories::find()->where(['urlKey' => $slug])->one());
        if(!$category)
            throw new \yii\web\NotFoundHttpException('Page Not Found');
        //var_dump($category); die;
        $store = \common\models\Stores::findOne(Yii::$app->params['storeId']);
        $this->metaTitle = $store->title.": ".$category->title;//var_dump($this->metaTitle);die;
        $this->metaKeywords = $store->title.": ".$category->title;
        $this->metaDescription = $store->title.": ".$category->title;
        $searchModel = new \common\models\search\ProductsSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        if(\Yii::$app->request->isAjax){
            $productsQuery = $category->getProducts(Yii::$app->params['storeId']);
            $totalCount = $productsQuery->count();
        }else{
            $productsQuery = new \yii\db\ActiveQuery('common\models\Products');
        }
        //$productsQuery = $category->getProducts(Yii::$app->params['storeId']);
        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        /*if($category->urlKey == "gift-vouchers"){
            $_GET['sort'] = "price";
        }*/
        if(isset($_GET['sort']) && (strpos($_GET['sort'], "name")!==FALSE || strpos($_GET['sort'], "ezcode")!==FALSE || strpos($_GET['sort'], "price")!==FALSE)){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->groupBy("Products.id");
            if(strpos($_GET['sort'], "name")!==FALSE){
                $query->andWhere('Attributes.code = "name"');
            }
            elseif(strpos($_GET['sort'], "ezcode")!==FALSE){
                $query->andWhere('Attributes.code = "ezcode"');
            }
            elseif(strpos($_GET['sort'], "price")!==FALSE){
                $query->andWhere('Attributes.code = "cost_price"');
            }
            
        }
        $products = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 20,
            ],
        ]);

        //$consumerPromotions = ConsumerPromotions::find()->all();
        //var_dump($promotionProductsArray);die();

        // if(\Yii::$app->request->isAjax)
        //     {var_dump($products->pagination->pageSize); die;}
        $products->setSort([
            'attributes' => [

                'dateAdded',
                'sku',
                'price' => [
                    'asc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_ASC],
                    'desc' => ['CAST(`AttributeValues`.`value` as DECIMAL(10,2))' => SORT_DESC]
                ],
                'name' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ],
                'ezcode' => [
                    'asc' => ['`AttributeValues`.`value`' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`' => SORT_DESC]
                ]
            ]
        ]);
        if(\Yii::$app->request->isAjax){
            $promotionProductsArray = $this->getProductPromotions($products);
            return $this->renderAjax('products', compact('products','category','totalCount','promotionProductsArray')); 
        }    
        else
            return $this->render('products', compact('products','category','totalCount','promotionProductsArray'));


    }

    public function actionTest()
    {
        die('hello');
    }

    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*public function getPromotionProducts(){

    }*/
}

?>

