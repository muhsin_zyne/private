<?php
namespace frontend\modules\b2b\controllers;
use common\models\Products;
use common\models\BundleItems;
use common\models\AttributeOptionPrices;
use common\models\User;
use common\models\B2bAddresses;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Attributes;
use common\models\Carts;
use common\models\Orders;
use common\models\OrderItems;
use yii\data\ActiveDataProvider;
class CartController extends \frontend\components\B2bController
{
    public $customAttributes;
    public function init(){
        $this->customAttributes = ArrayHelper::map(Attributes::find()->all(), 'id', 'code');
        parent::init();
    }
    public function actionAdd(){ //var_dump($_POST); die;
        //var_dump($_POST['']);die;
        if(Yii::$app->session->has('loadedCart')){
            $savedCart = Carts::findOne(Yii::$app->session->get('loadedCart'));
            Yii::$app->cart->positions = array_merge(unserialize($savedCart->positionsData), Yii::$app->cart->positions);
        }
        if(!isset($_POST['Products']['id'])){ //bulk order
            foreach($_POST['Products'] as $id => $product){
                if(isset($product['config'])){
                    parse_str($product['config'], $config);
                    if(isset($config['Products']['BundleItems'])) //it's a bundle product
                        $product['BundleItems'] = $config['Products']['BundleItems'];
                    else{ //configurable product
                        $product = array_merge($product, $config['Products']);
                    }
                }
                if(isset($product['id'])) //only if that product is ticked in the bulk order form
                    $this->processItem($product);
            } //die;
        }else{ //single order
            //var_dump("sdfsdf"); die;

            $this->processItem($_POST['Products']);
        }
        
        Yii::$app->getSession()->setFlash('success', "Product(s) successfully added to cart!");
        if(isset($_POST['tourl']))
            return $this->redirect(Yii::$app->params['b2bUrl'].$_POST['tourl']);
        else
            return $this->redirect(Yii::$app->request->referrer);
        //return $this->goBack();
    }

    public function processItem($product){ //var_dump($product); die;
        
        $user = User::findOne(Yii::$app->user->id);
        $productModel = Products::findOne($product['id']);
        $position = $productModel->getCartPosition(); 
        $position->config = []; //var_dump($product->typeId); die;
        if($productModel->typeId=="bundle"){
            foreach($product['BundleItems'] as $item){
                if(!is_array($item)){
                    $bundleItem = BundleItems::findOne($item);
                    if(isset($product['BundleItems']['qty'][$item])){
                        $position->config[$bundleItem->productId] = ['qty' => $_POST['Products']['BundleItems']['qty'][$item], 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                    }else{
                        $position->config[$bundleItem->productId] = ['qty' => $bundleItem->defaultQty, 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                    }
                }
            }
        }elseif($productModel->typeId=="configurable"){
            $superAttributes = $this->filterSuperAttributes($product);
            foreach($superAttributes as $attrCode => $optionPriceId){ // looping through the super attributes
                if($attrOptionPrice = AttributeOptionPrices::findOne($optionPriceId)){ if(!isset($attrOptionPrice->option)){ var_dump($attrOptionPrice->optionId); die;}
                    $position->config[$attrOptionPrice->id] = ['attr' => $attrOptionPrice->attr->toArray(), 'value' => $attrOptionPrice->option->value, 'optionId' =>$attrOptionPrice->optionId];
                }
            }
        }
        if(isset($product['B2bAddresses'])){
            foreach($product['B2bAddresses'] as $addressId => $qty){
                if(is_numeric($qty)){
                    $position = clone $position;
                    $position->b2bAddress = B2bAddresses::findOne($addressId);
                    Yii::$app->cart->put($position, $qty);
                }
            }
        }else{

            $position->b2bAddress = $user->defaultB2bAddress;
            //var_dump($user->defaultB2bAddress); die;
            Yii::$app->cart->put($position, $product['quantity']);
        }
    }


    public function actionCheckout($comments=null){
        //var_dump(Yii::$app->cart->positions); die;

        if($order = $this->placeOrder(Yii::$app->cart->positions)){
            $orderId = $order->id;
            Yii::$app->cart->removeAll();
            return $this->render('_success',compact('orderId'));
            //$this->redirect(Url::to(['cart/view']));
         }   
    }

    public function actionIndex(){
        die('Saved Cart List');
    }

    public function actionView($id = null)
    {   //var_dump(Yii::$app->session->get('loadedCart')); die;
        $cart = Yii::$app->cart;
        $savedCart = new Carts;
        if(!is_null($id)){
            Yii::$app->session->set('loadedCart', $id);
            $savedCart = Carts::findOne(isset($id)? $id : Yii::$app->session->get('loadedCart'));
            $cart->positions = unserialize($savedCart->positionsData);
            $positions = [];
            foreach($cart->positions as $position){
                if(!is_null($position->product))
                    $positions[$position->getId()] = $position;
            }
            $cart->positions = $positions;
            Yii::$app->getSession()->setFlash('success', "If you are adding/editing the cart, please ensure that you save it if you want to re-use the list in future.");
            $this->redirect(Url::to(['cart/view'])); 
        }
        //var_dump($cart->positions);die(); 

        $addresses = ArrayHelper::map($cart->positions, 'b2bAddress.id', 'b2bAddress');
        // foreach($cart->positions as $pos){
        //     var_dump($pos->getId());
        // }
        if(empty($cart->positions))
            return $this->render('empty');
        return $this->render('view', compact('cart', 'addresses', 'savedCart'));
    }

    public function actionDelete($positionId)
    {
        Yii::$app->cart->removeById($positionId);
        $this->redirect(Url::to(['cart/view'])); 
    }
  
    public function actionUpdate($checkout = false)
    {
        $app = Yii::$app;
        if(isset($_POST['Positions'])){
            $positions = $app->cart->positions;
            $newPositions = [];
            foreach($_POST['Positions'] as $positionId => $pos){
                $positions[$positionId]->quantity = $pos['qty'];
                $positions[$positionId]->supplierUid = $pos['supplierUid'];
                $positions[$positionId]->comments = $pos['comments'];
                $positions[$positionId]->b2bAddress = B2bAddresses::findOne($pos['b2bAddressId']);
                $newPositions[$positions[$positionId]->getId()] = $positions[$positionId];
            }
            $app->cart->setPositions($newPositions);
            if($checkout=="true"){ //var_dump($checkout); die;
                $this->redirect(Url::to(['cart/checkout', 'comments'=>$_POST['cart']['comments']]));
            }
            else{
                Yii::$app->getSession()->setFlash('success', "Cart updated successfully");
                $this->redirect(Url::to(['cart/view']));
            }
        }
        //$this->redirect(Url::to(['cart/view']));
    }

    public function actionSave(){  //die('save'); 
        $app = Yii::$app;
        $positions = array_values($app->cart->positions);
        if(isset($_POST['Positions'])){
            $positions = $app->cart->positions;
            $newPositions = [];
            foreach($_POST['Positions'] as $positionId => $pos){ //var_dump($pos['qty']); die;
                $positions[$positionId]->quantity = $pos['qty'];
                $positions[$positionId]->supplierUid = $pos['supplierUid'];
                $positions[$positionId]->comments = $pos['comments'];
                $positions[$positionId]->b2bAddress = B2bAddresses::findOne($pos['b2bAddressId']);
                $newPositions[$positions[$positionId]->getId()] = $positions[$positionId];
            }
            //var_dump($positions); die;
            $app->cart->setPositions($newPositions); //var_dump(Yii::$app->session->get('loadedCart')); die;
            if(!$cart = Carts::findOne($_POST['Cart']['id'])){
                $cart = new Carts;
                if(Yii::$app->session->has('loadedCart')){
                    if(!$cart = Carts::findOne(Yii::$app->session->get('loadedCart'))){
                        $cart = new Carts;
                    }
                }
            }
            //var_dump($cart->isNewRecord); die;
            $cart->positionsData = serialize($positions);
            $cart->userId = Yii::$app->user->id;
            if($cart->isNewRecord){
                $cart->title = $_POST['Cart']['title'];
            }
            $cart->type = "saved";
            $cart->save(false);
            Yii::$app->session->set('loadedCart', $cart->id);
            Yii::$app->getSession()->setFlash('success', "Cart saved successfully");
            $this->redirect(Url::to(['cart/view']));
        }
    }

    public function actionSaved(){

        $dataProvider = new ActiveDataProvider([
            'query' => Carts::find()->where(['userId' => Yii::$app->user->id, 'type' => 'saved'])->orderBy(['updatedDate'=>SORT_DESC,])
        ]);
        return $this->render('saved',[
            'dataProvider'=>$dataProvider,
            ]);
        
    }
    public function actionDeletesavedcart($id)
    {
        $model=$this->findModel($id);
        //var_dump($model);die;
        $model->delete();
        $this->redirect(Url::to(['cart/saved']));
    }
    protected function findModel($id)
    {
        if (($model = Carts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEmpty(){
        Yii::$app->cart->removeAll();
        Yii::$app->session->set('loadedCart', null);
        $this->redirect(Url::to(['cart/view']));
    }

    public function filterSuperAttributes($product){
        foreach($product as $attr => $value) if(!in_array($attr, $this->customAttributes)) unset($product[$attr]);
        return $product;
    }

    public function placeOrder($orderDetails){

        //var_dump($orderDetails);die();

    	// foreach ($orderDetails as $order) {
    	// 	if(!is_null($order))
    	// 		var_dump($order->product->id);
    	// }
    	// die();

    	//die();
        $cart = Yii::$app->cart;
        $order = new Orders;
        $order->type = 'b2b';
        $order->storeId = Yii::$app->params['storeId'];
        $order->customerId = Yii::$app->user->id;
        //$order->shippingMethodId = $_POST['CheckoutForm']['shipping_method'];
        //$order->shippingAmount = $shippingMethod->getCalculatedPrice($cart->cost);
        $order->subTotal = $cart->cost;
        $order->grandTotal = $cart->getCost();
        $order->customerNotes = isset($_GET['comments'])? $_GET['comments'] : null;
        $order->status = "complete";
        if($order->save(false)){ 
        	foreach($cart->positions as $position){
        		$item = new OrderItems;
        		$item->orderId = $order->id;
    			$item->productId = $position->product->id;
    			$item->quantity = $position->quantity;
    			$item->config = json_encode($position->config);
    			$item->price = $position->price;
                $item->conferenceId = ($position->product->isInConference())? $position->product->isInConference() : null;
    			$item->storeAddressId = $position->b2bAddress->id;
    			$item->supplierUid = isset($position->product->brand->supplier)? $position->product->brand->supplier->id : "";
    			$item->comment = $position->comments;
    			$item->status = "pending";
    			if($item->save(false)){
                    if($position->product->typeId=="configurable"){
                        $childProduct = $position->childProduct;
                        // $linkages = \common\models\ProductLinkages::find()->where(['parent' => $position->product->id])->all();
                        // foreach($linkages as $link){ //logic for finding the child product with given config
                        //     foreach($position->config as $config){ 
                        //         $linkedProductQuery = new \yii\db\ActiveQuery('common\models\Products');
                        //         $linkedProduct = $linkedProductQuery->from('Products')->where('id = '.$link->productId)->one();
                        //         if($linkedProduct->{$config['attr']['code']} == $config['value']){
                        //             $childProduct = $linkedProduct;
                        //             break;
                        //         }else
                        //             $childProduct = false;
                        //     }
                        //     if($childProduct) break;
                        // }
                        $child = new \common\models\OrderItemChildren;
                        $child->parentProductId = $position->id;
                        $child->orderItemId = $item->id;
                        $child->productId = $childProduct->id;
                        $childProduct = '100';
                        $child->quantity = 1;
                        $child->type = "configurable";
                        $child->save(false);
                    }
                }

    		}
	    $order->subTotal = $order->getOrderTotal();
            $order->grandTotal = $order->getOrderTotal();
            $order->save(false);
    	}
        $order->sendB2bOrderConfirmation();
        $order->sendSupplierNotification();
    	return $order;	
    }	

}
