<?php

namespace frontend\modules\b2b;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\b2b\controllers';
    //public $viewPath = 'frontend\web\themes\b2b';
    public function init()
    {
    	$this->viewPath = $_SERVER['DOCUMENT_ROOT'].'/themes/b2b';
    	$this->controllerMap = [
    		'account' => 'frontend\modules\b2b\controllers\AccountController'
    	];
        parent::init();

        // custom initialization code goes here
    }

    public function getBestSellers($limit = 10){
        $query = new \yii\db\ActiveQuery('\common\models\Products');
        return Yii::$app->db->createCommand("select p.id, count(p.id) as pCount from Products p join OrderItems oi on oi.productId = p.id join Orders o on oi.orderId = o.id where o.type='b2b' group by p.id order by pCount desc limit $limit")
        ->queryAll();
    }    

}