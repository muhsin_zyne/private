<?php
namespace frontend\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\bootstrap\NavBar;
 
class FrontNavBar extends NavBar
{
	public $userName;
	public function init()
    {
        echo '<div class="header">
<div class="headtop">
<div class="wrapper_content">
<div class="head_phone">02 9497 4077</div>
<div class="freeshipping_header"><span>Free Delivery</span> on orders over $100</div>
<div class="cartmenu"><ul >';
                       if (!\Yii::$app->user->isGuest) {                        
                      echo  '
                                <li ><a href="'.\yii\helpers\Url::to(['site/logout']).'" title="Log In" >Log Out</a></li>
                        <li>/</li>
                                <li ><a href="'.\yii\helpers\Url::to(['site/account']).'" title="My Account" >My Account</a></li>
                        <li>/</li>'; }
                        else {
                            echo  '
                            <li class="first" ><a href="'.\yii\helpers\Url::to(['site/signup']).'" title="Register" >Register</a></li>
                        <li>/</li>
                                <li ><a href="'.\yii\helpers\Url::to(['site/login']).'" title="Log In" >Log In</a></li>
                        <li>/</li>
                                ';
                        }
                               echo  ' <li ><a href="" title="View Cart" class="top-link-cart">View Cart</a></li>
                        <li>/</li>
                                <li class=" last" ><a href="" title="Checkout" class="top-link-checkout">Checkout</a></li>
                    <li class="clear"></li>
</ul>
</div><!--cartmenu-->
<div class="clear"></div>
</div>
</div><!--headtop-->
<div class="welcome_search">
<div class="wrapper_content">
<div class="searchbox">
<form name="form" action="" method="get" onsubmit="return searchform()">
<input type="text" name="q" id="searchname" value="" onkeypress="jQuery(this).removeClass("err");" />  
<input type="submit" name="" value="Search" />
<div class="clear"></div>
</form>
</div><!--search-->
<div class="clear"></div>
</div>
</div><!--welcome_search-->
<div class="logo_sec">
<div class="wrapper_content">
<div class="logo">
<a href="http://www.legj.com.au/site1/" title="BJZ Jewellers" ><img src="images/logo.png" alt="BJZ Jewellers" /></a>
</div>
<div class="finduson_top">
<span>Find us on</span>
<a href="" target="_blank"><img src="images/facebook_top.png" /></a>
<a href="" target="_blank"><img src="images/youtube_top.png" /></a>
<a href="" target="_blank"><img src="images/pininterest_top.png" /></a>
</div>
<div class="clear"></div>
</div>
</div><!--logo_sec-->';
    } 
}