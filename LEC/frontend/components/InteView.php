<?php
/**
 * @author: Max
 * Checks if a view file exists in the set theme's folder and if yes, uses it. Else uses the view file in the default theme's folder.
 */

namespace frontend\components;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\View;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\db\ActiveQuery;
use yii\db\Query;

class InteView extends View
{
    private $_viewFiles = [];
    private $_assetManager;
    public $pathMap;

    public function init(){
        parent::init();
    }

    public function renderFile($viewFile, $params = [], $context = null){
        $viewFile = Yii::getAlias($viewFile);
        if ($this->theme !== null) {
            $testTheme = clone $this->theme; 
            // if(count($this->theme->pathMap['@app/views']) > 1)
            //  $testTheme->pathMap["@app/views"] = [end($this->theme->pathMap["@app/views"])];
            
            foreach(array_reverse($this->theme->pathMap["@app/views"]) as $view){
                $testTheme->pathMap["@app/views"] = $view;
                $viewFileCheck = $testTheme->applyTo($viewFile);
                if(file_exists($viewFileCheck) && strpos($viewFileCheck, "themes")){ //Max: check if the file exists in the set theme's folder other than the default one
                    $viewFile = $viewFileCheck;
                    break;
                }
                // else{ //else use the file in the default theme's folder
                //     //var_dump($viewFile);die();
                //  $viewFile = $this->theme->applyTo($viewFile);
                // }
            }
        }
        //var_dump($viewFile); die;
        if (is_file($viewFile)) {
            $viewFile = FileHelper::localize($viewFile);
        } else {
            throw new InvalidParamException("The view file does not exist: $viewFile");
        }
        $oldContext = $this->context;
        if ($context !== null) {
            $this->context = $context;
        }
        $output = '';
        $this->_viewFiles[] = $viewFile;
        if ($this->beforeRender($viewFile, $params)) {
            Yii::trace("Rendering view file: $viewFile", __METHOD__);
            $ext = pathinfo($viewFile, PATHINFO_EXTENSION);
            if (isset($this->renderers[$ext])) {
                if (is_array($this->renderers[$ext]) || is_string($this->renderers[$ext])) {
                    $this->renderers[$ext] = Yii::createObject($this->renderers[$ext]);
                }
                /* @var $renderer ViewRenderer */
                $renderer = $this->renderers[$ext];
                $output = $renderer->render($this, $viewFile, $params);
            } else {
                $output = $this->renderPhpFile($viewFile, $params);
            }
            $this->afterRender($viewFile, $params, $output);
        }
        array_pop($this->_viewFiles);
        $this->context = $oldContext;
        return $output;
    }

    public function renderBlock($slug){
        $query = new Query;                             
        $query->select(['CmsBlocks.content AS content'])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreCmsBlocks')
        ->join('LEFT OUTER JOIN', 'CmsBlocks','CmsBlocks.id=StoreCmsBlocks.blockId')
        ->where("StoreCmsBlocks.storeId = ".Yii::$app->params['storeId']." and CmsBlocks.slug='".$slug."'");
        $command = $query->createCommand();
        $cmsblock = $command->queryOne();   
        if(isset($cmsblock['content']))   
            $result= $cmsblock['content'];
            else
            $result= '';
        return $result;
    }
    public function renderCategoryBlock($categoryId){      
        $query = new Query;                             
        $query->select(['CmsBlocks.content AS content'])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreCmsBlocks')
        ->join('LEFT OUTER JOIN', 'CmsBlocks','CmsBlocks.id=StoreCmsBlocks.blockId')
        ->where("StoreCmsBlocks.storeId = ".Yii::$app->params['storeId']." and CmsBlocks.categoryId='".$categoryId."' and CmsBlocks.type='category_content' and CmsBlocks.status='1'");
        $command = $query->createCommand();
        $cmsblock = $command->queryOne();
        if(isset($cmsblock['content']))   
            $result= $cmsblock['content'];
            else
            $result= '';
        return $result;
    }
       
}
