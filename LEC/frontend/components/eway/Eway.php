<?php

namespace frontend\components\eway;
use Yii;
use yii\base\Component;
/*** 
*/
class Eway extends Component{			
	public $apiKey;		
	public $apiPassword;		
	public $apiEndpoint;     	
	public $transaction;    	
	public $encryptionKey;    	
	public $model;		
	
	public function init() {    			
		require_once 'lib/eway-rapid-php-master/include_eway.php';    			
		return parent::init();    	
	}	    	

	public function renderForm($form){    			
		echo \Yii::$app->view->renderFile('@app/components/eway/_form.php',['model'=>$this->model, 'form' => $form]);    	
	}    	

	public function processTransaction(){    	        		
		$client = \Eway\Rapid::createClient($this->apiKey, $this->apiPassword, $this->apiEndpoint);        		
		$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $this->transaction);        		
		return $response;    	
	}    	

	public function convertAmount($amount){        		
		return $amount*100;     	
	}
        
        public function refundAmount($transacionID, $amount) {
        $payment = [
            'TotalAmount' => $amount,
        ];

        $refundDetails = $payment;
        $refundDetails['TransactionID'] = $transacionID;
        $refund = [
            'Refund' => $refundDetails,
        ];



        $client = \Eway\Rapid::createClient($this->apiKey, $this->apiPassword, $this->apiEndpoint);
        $response = $client->refund($refund);
        return $response;
    }

}?>