<?php
 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\Helper;

?>
<div class="eway-container">
		<div id="payment" class="row">
	        <div id="payment" class="col-xs-12">
	            <div id="creditcard-form-wrapper">
	    			<div class="card-details">
					    <h4 class="credit-card-details"> Credit Card Details </h4>
					    <span class="cardimage" id="cardportal-visa" title="Visa">&nbsp;</span>
					    <span class="cardimage" id="cardportal-mastercard" title="Mastercard">&nbsp;</span>
					</div>
					<!-- <form action="response.php" class="form-horizontal" id="form1" method="post">  --> 
						<hr class="modal-wide">
						<?=$form->field($model, 'creditcard_no')->textInput(['data-eway-encrypt-name'=>'EWAY_CARDNUMBER', 'placeholder' => 'CARD NUMBER', 'name' => 'EWAY_CARDNUMBER'])->label('');?>
						<!-- <div id="cc-number" class="form-group">
                            <div class="row">
						    <div class="col-xs-12">
						        <div class="input-group">
						            <span class="input-group-addon glyphicon glyphicon-credit-card"></span>
						            <input id="EWAY_CARDNUMBER" name="EWAY_CARDNUMBER" type="tel" placeholder="Card Number" data-unsupportedcardmessage="Card type is not accepted by merchant" data-validationmessage="Invalid Card Number" class="form-control" autocomplete="off" data-eway-encrypt-name="EWAY_CARDNUMBER">
						        </div>
						    </div>
                            </div>
						</div> -->
						<?=$form->field($model, 'creditcard_name')->textInput(['name'=>'EWAY_CARDNAME', 'placeholder' => 'NAME'])->label('');?>
						<!-- <div id="cc-name" class="form-group">
                          <div class="row">
						    <div class="col-xs-12">
						        <div class="input-group">
						            <span class="input-group-addon glyphicon glyphicon-user"></span>
						            <input id="EWAY_CARDNAME" name="EWAY_CARDNAME" type="text" placeholder="Name on Card" data-validationmessage="Card Holder Name Required" class="form-control text">
						        </div>
						    </div>
                            </div>
						</div> -->
						
						<div id="cc-expiry" class="form-group">
							<div class="row">
								<div class="col-xs-4">
								<?=$form->field($model, 'creditcard_cvv')->textInput(['type' => 'text', 'data-eway-encrypt-name' => 'EWAY_CARDCVN', 'maxlength' => '4', 'autocomplete' => 'off', 'placeholder' => 'CVV', 'name'=>'EWAY_CARDCVN'])->label('');?>
						    		<!-- <div id="cc-cvn" class="credit-card-ccv form-group">
						    			<label class="control-label col-xs-5 card-title">CVV</label>
						    			<div class="input-group">
								            <span class="input-group-addon glyphicon glyphicon-lock"></span>
								            <input id="EWAY_CARDCVN" name="EWAY_CARDCVN" type="text" maxlength="4" data-validationmessage="Invalid CCV Number" class="form-control text" autocomplete="off" placeholder="CVV" data-eway-encrypt-name="EWAY_CARDCVN">
						        		</div>
						 			</div> -->
								</div>
							
							    <div class="col-xs-4">
							    <?=$form->field($model, 'creditcard_expiry_month')->dropDownList(['01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'], ['prompt'=>'--Expiry Month--', 'name' => 'EWAY_CARDEXPIRYMONTH'])->label('');?>
							    	<!-- <div class="form-group">
								        <select id="EWAY_CARDEXPIRYMONTH" name="EWAY_CARDEXPIRYMONTH" class="form-control">
								            <option value="01">January</option>
								            <option value="02">February</option>
								            <option value="03">March</option>
								            <option value="04">April</option>
								            <option value="05">May</option>
								            <option value="06">June</option>
								            <option value="07">July</option>
								            <option value="08">August</option>
								            <option value="09">September</option>
								            <option value="10">October</option>
								            <option value="11">November</option>
								            <option value="12">December</option>
								        </select>
	                                </div> -->
							    </div>
						        <div class="col-xs-4">
						        <?php
						        	$currYear = (int)date("Y");
									for($x = $currYear; $x <= $currYear+20; $x++){
										//echo "<option value=\"", $x, "\"", ($x==$currYear) ? " selected=\"selected\"" : "", ">", $x, "</option>";
										$expiryYears[$x] = $x;
									}
						        ?>
						        	<?=$form->field($model, 'creditcard_expiry_year')->dropDownList($expiryYears, ['prompt'=>'--Expiry Year--', 'name' => 'EWAY_CARDEXPIRYYEAR'])->label('');?>
									<!-- <div class="form-group">
								        <select class="form-control" id="EWAY_CARDEXPIRYYEAR" name="EWAY_CARDEXPIRYYEAR">
											<?php
												$currYear = (int)date("Y");
												for($x = $currYear; $x <= $currYear+20; $x++){
													echo "<option value=\"", $x, "\"", ($x==$currYear) ? " selected=\"selected\"" : "", ">", $x, "</option>";
												}
											?>		
										</select>
                                	</div> -->
						    	</div>
						    </div>

                            	<div class="col-xs-6">
                              		<button type="button" title="Pay Now" class="button continue pull-right eway-pay">Pay Now</button>
                             	</div>
                             	<div class="col-xs-6">
                              		<div class="v_m_logo pull-right"> <img border="0" width="125px" alt="visa and mastercard" src="/images/visaandmastercard.jpg"> </div>
                             	</div>
                            
						</div>
						<hr>
						<div class="credit-card-buttons row">
						</div>
					<!-- </form> --> 
				</div>
	        </div>
	    </div>
	</div>


    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script> 
	<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script>
    <script>
		
		$(document).ready(function() {
			// $.validator.addMethod('CCExp', function(value, element, params) { //console.log(params);
			//       var minMonth = new Date().getMonth() + 1;
			//       var minYear = new Date().getFullYear();
			//       var month = parseInt($(params.month).val(), 10);
			//       var year = parseInt($(params.year).val(), 10);
			//       //var year = $(params.year).val();
			//       return (year > minYear || (year === minYear && month >= minMonth));
			//       //return ((year >= minYear && month > minMonth) || (year === minYear && month >= minMonth));
			// }, 'Your Credit Card Expiration date is invalid.');
			
			// $( "#checkout-form" ).validate({
			//   rules: {
			//     EWAY_CARDNUMBER: {
			//       required: true,
			//       creditcard: true
			//     },
			//     EWAY_CARDEXPIRYYEAR: {
	  //                 CCExp: {
	  //                       month: '#EWAY_CARDEXPIRYMONTH',
	  //                       year: '#EWAY_CARDEXPIRYYEAR'
	  //                 }
	  //           },
	  //           EWAY_CARDEXPIRYMONTH: {
	  //                 CCExp: {
	  //                       month: '#EWAY_CARDEXPIRYMONTH',
	  //                       year: '#EWAY_CARDEXPIRYYEAR'
	  //                 }
	  //           },
	  //           EWAY_CARDCVN: {
	  //               required: true,
	  //               minlength: 3,
	  //               maxlength: 4
	  //           },
			//   }
			// });

			// $('.eway-pay').on('click', function () {
   //      		$('#checkout-form').valid();
   //  		});

		//});*/

		$(document).ready(function () {
    			$("form").submit(function () {
        			$(".eway-pay").attr("disabled", true);
        			//return true;  
    			});
		});

		/*$(document).ready(function(){
			$('.eway-pay').click(function(){

			})*/
		});

	</script> 
