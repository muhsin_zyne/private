<?php
namespace frontend\components;
use yii;
use app\assets\AppAsset;
use yii\bootstrap\Nav;
use common\models\Categories;
use common\models\Gallery;
use common\models\StorePages;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Menus;
use common\models\StorePromotions;
use common\models\ConsumerPromotions;
use common\models\Stores;

class HeaderMenu extends Nav
{

    public $is_Mobile=false;
    public $storeMenuExclusions = [ '136' => ['brands','advice'], '63'=>['brands'], '112'=>['advice']];
    //public $commonMenus = ['brands','gallery','advice'];

    public function init() {

        $store=Stores::findOne(Yii::$app->params['storeId']);
        $activeCategory=$store->activeCategoryIds;
        array_push($activeCategory,"0");
        $items = Menus::find()   //orderby position
            ->where(['storeId' => Yii::$app->params['storeId'],'unAssigned'=>0,'type'=>'header',])
            ->orderBy('position')
            ->asArray()
            ->all();

        $elements = [];

        foreach ($items as $item) {
            if($item['parent']=='0'){
                if(preg_match("/http/", $item['path'])) {
                    $elements[] =
                        [   'label' => $item['title'],
                            'url' => $item['path'],
                            'id' => $item['id'],
                            'target'=>'_blank',
                            'categoriesId'=>$item['categoriesId'],
                            'htmlAttributes'=>isset($item['htmlAttributes'])?$item['htmlAttributes']:'',
                            'items' => $this->getItems($items,$item['id']),
                        ];
                }
                else {

                    $elements[] =
                        [   'label' => $item['title'],
                            'url' => Yii::$app->urlManager->baseUrl."/".$item['path'],
                            'id' => $item['id'],
                            'target'=>'_self',
                            'categoriesId'=>$item['categoriesId'],
                            'htmlAttributes'=>isset($item['htmlAttributes'])?$item['htmlAttributes']:'',
                            'items' => $this->getItems($items,$item['id']),
                        ];  

                }
            } 
        }

        $role_id = 1;
        if($this->is_Mobile==true)//It's a mobile browswe
            $result = $this->getMenuRecrusiveMobile($elements,$activeCategory,FALSE);
        else
            $result = $this->getMenuRecrusive($elements,$activeCategory,FALSE);
        
        echo $result;
    }

    public function getItems($items_new,$parent){
        
        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {
                if(preg_match("/http/", $newitem['path'])) {   

                    $newelements[] =

                    [   'label' => $newitem['title'],
                        'url' => $newitem['path'],
                        'target'=>'_blank',
                        'id' => $newitem['id'],
                        'categoriesId'=>$newitem['categoriesId'],
                        'htmlAttributes'=>isset($newitem['htmlAttributes'])?$newitem['htmlAttributes']:'',
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];
                }
                else {

                   $newelements[] =

                    [   'label' => $newitem['title'],
                        'url' => Yii::$app->urlManager->baseUrl."/".$newitem['path'],
                        'id' => $newitem['id'],
                        'target'=>'_self',
                        'categoriesId'=>$newitem['categoriesId'],
                        'htmlAttributes'=>isset($newitem['htmlAttributes'])?$newitem['htmlAttributes']:'',
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ]; 
                }
            }
        }

        return $newelements;  

    }

    private function getMenuRecrusive($data,$activeCategory,$child=FALSE){
        
        $html = '<ul id="shown-sub" class="">';
        foreach($data as $item){ 
            if(isset($item['items'])) { //var_dump($item);die;
                if (in_array($item['categoriesId'], $activeCategory)) { 
                    $html .= '<li class="sub"><a href="'.$item['url'].'" target="'.$item['target'].'" '.$item['htmlAttributes'].'><span>'.$item['label'].'</span></a>';
                    $html.= $this->getMenuRecrusive($item['items'],$activeCategory,TRUE); 
                    $html.= '</li>';  
                }                
            }  

            else { 

                $html .= '<li class="child_sub"><a href="'.\yii\helpers\Url::to(['/categories/products', 'categoryId'=>$item['id']]).'">'.$item['label'].'</a>';
                $html.= '</li>';    
            } 
        }

        if($child==FALSE) {
            
            if(!in_array('advice', isset($this->storeMenuExclusions[Yii::$app->params['storeId']]) ? $this->storeMenuExclusions[Yii::$app->params['storeId']] : [] )) {       
                    
                    $html.= '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Advice</a>';
                    $html.= '<ul class="dropdown-menu" id="dropdown-menu">';
                    $storePages = StorePages::find()->where(['storeId' => Yii::$app->params['storeId']])->orderBy('id')->all(); 
                    foreach($storePages as $index => $storepage){  
                        if(isset($storepage->cmsPage->status)){
                            if($storepage->cmsPage->status==1) {                 

                                $html.= '<li title="'.$storepage->cmsPage->title.'" ><a href="'.\yii\helpers\Url::base().'/'.$storepage->cmsPage->slug.'.html'.'" class="">'.\frontend\components\Helper::stripText($storepage->cmsPage->title,25).'</a></li>';
                            }
                        }    
                    }
                $html.= '</ul></li>';
            } 
            $html .= '<li class="sub"><a href="'.\yii\helpers\Url::to(['/site/contact']).'"><span>Contact Us</span></a></li>';                 
        }
        $html .='</ul>'; 
        return $html;

    }
}   