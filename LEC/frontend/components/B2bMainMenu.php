<?php
namespace frontend\components;

use Yii;
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\models\Categories;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use commmon\models\Stores;
use common\models\User;
use common\models\Conferences;
use common\models\ConsumerPromotions;
use common\models\Brands;
use common\models\Suppliers;

class B2bMainMenu extends Nav
{

    public $exclustions = [
        'categories' => ['PRINTERS AND SCANNERS', 'COMPUTER COMPONENTS'],
        //'categories' => [],
        'brands' => ['Unbranded', ],
        'suppliers' => [],
    ];
    public function init()
    {
        /*$items = Categories::find()
                ->where(['active' => '1'])
                ->where("EXISTS (SELECT 1 FROM ProductCategories pc join Products p on pc.productid = p.id WHERE  status = 1 and createdBy = 0 and pc.categoryId = Categories.id)")
                ->andWhere("title NOT IN ('".implode("','", $this->exclustions['categories'])."')")
                //->orderBy('position asc')
                ->asArray()
                ->all();*/
        
        //var_dump($items);die();

        $items = Categories::find()->where(['active' => '1'])->andWhere("title NOT IN ('".implode("','", $this->exclustions['categories'])."')")->asArray()->all(); 
        
        $elements = [];       

        foreach ($items as $item) { 
            if($item['parent']=='0'){ 
                $elements[] =
                    ['label' => $item['title'], 
                     'url' => '#',
                     'id' => $item['id'],
                     'icon' => $item['icon'],
                     'items' => $this->getItems($items,$item['id']),
                    ];
            } 
        }
        
        $result = $this->getMenuRecursive($elements,FALSE);
        echo $result;

       

        echo " <script>
        (function($){
            $(window).load(function(){
                $('#content-1').mCustomScrollbar({
                    theme:'minimal'
                });
        $('#content-2').mCustomScrollbar({
                    theme:'minimal'
                });
                
            });
        })(jQuery);
    </script> " ;

    }

    public function getItems($items_new,$parent){
        
        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {
                $newelements[] =
                    ['label' => $newitem['title'],
                     'url' => '#',
                     'id' => $newitem['id'],
                     'icon' => $newitem['icon'],
                     'items' => $this->getItems($items_new,$newitem['id']),
                    ];
            }
        }

        return $newelements;  
    }
        
    private function getMenuRecursive($data,$child=FALSE){
        $html =''; 
        /*if($child==FALSE) {
            $html = '<ul><li><a href="'.Yii::$app->params['b2bUrl'].'"><span>Home</span></a></li>';

            $brands = Brands::find()->select(['id', 'title', 'enabled'])->andWhere("title NOT IN ('".implode("','", $this->exclustions['brands'])."') AND isVirtual=0")->orderBy("title asc")->all();
            
            $suppliers = Suppliers::find()->andWhere("firstname NOT IN ('".implode("','", $this->exclustions['suppliers'])."')")->orderBy("firstname asc")->all();

            $html .= '  <li class="parent">
                        <a href="javascript:void(0)"><span>Supplier</span></a><ul class=""><div id="content-2" class="scoller-head">';
                        foreach ($suppliers as $supplier) { 
                            if($supplier->hasProducts(true))
                                $html .= '<li class=""><a href="'.\yii\helpers\Url::to(['suppliers/products', 'id'=>$supplier['id']]).'" class=""><span>'.$supplier['firstname'].' '.$supplier['lastname'].'</span></a></li>';
                        }
            $html .= '  </div></ul> </li>';

            $html .= '  <li class="parent">
                        <a href="javascript:void(0)"><span>Brands</span></a><ul class=""><div id="content-1" class="scoller-head">';
                        foreach ($brands as $brand) { 
                            if($brand->hasProducts())
                                $html .= '<li class=""><a href="'.\yii\helpers\Url::to(['brands/products', 'id'=>$brand['id']]).'" class=""><span>'.$brand['title'].'</span></a></li>';
                        }
            $html .= '</div></ul> </li>';

        }    
        else*/
            $html = '<ul id="nav">';

        foreach($data as $item){ 

            if($item['label'] != "Gift Vouchers") {
                if(isset($item['items'])) { 
                    $html .= '<li class="parent"><a href="'.\yii\helpers\Url::to(['categories/products', 'categoryId'=>$item['id']]).'">
                              <i class="sm '.$item['icon'].'"></i><span>'.$item['label'].'</span></a>';
                    $html .= $this->getMenuRecursive($item['items'],TRUE);
                    $html .= '</li>';
                }  
                else {
            
                    $html .= '<li><a href="'.\yii\helpers\Url::to(['categories/products', 'categoryId'=>$item['id']]).'"><span>'.$item['label'].'</span></a>';
                    $html .= '</li>'; 
                } 
            }    
        }


        /*if ($child==FALSE) {

            //$conference = Conferences::find()->where("status = '1'")->one();
            //if(!$conference = Conferences::find()->where("status = '1'")->one())

            $todays = date("Y-m-d H:i:s");
            $promotions = ConsumerPromotions::find()->where("status = 'published' and fromDate < '$todays' and toDate > '$todays'")->all();
            if(!empty($promotions)){
                $html .= '  <li class="parent">
                        <a href="javascript:void(0)"><span>Catalogues</span></a><ul class="">';
                foreach ($promotions as $promotion) {
                    $html .='<li class=""><a href="'.\yii\helpers\Url::to(['promotions/products', 'id'=>$promotion->id]).'" class=""><span>'.$promotion->title.'</span></a></li>'; 
                }
            	
            	$html .= '</ul></li>';
            }            
                        
            //$html .= '  </ul> </li>'; 

            /*if($conference = Conferences::find()->where("status = '1' and fromDate < '$todays' and toDate > '$todays'")->one()){
                $html .= '  <li class="conference_name"><a href="'.\yii\helpers\Url::to(['conferences/index', 'id'=>$conference->id]).'"><span>'.$conference->title.'</span></a></li>';
            }
            elseif($conference = Conferences::find()->where("status = '0' and fromDate < '$todays' order by toDate DESC")->one()){
                $html .= '  <li class="conference_name"><a href="'.\yii\helpers\Url::to(['conferences/index', 'id'=>$conference->id]).'"><span>core ranges</span></a></li>';
            }*/
            /*elseif($conference = Conferences::find()->where("status = '0' order by toDate desc")->one()) { 
                 $html .= '  <li class="conference_name"><a href="'.\yii\helpers\Url::to(['conferences/index', 'id'=>$conference->id]).'"><span>core ranges</span></a></li>';
            }
        }*/
        $html .='</ul>'; 
        return $html;
    }
}       
