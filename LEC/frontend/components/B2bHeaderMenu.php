<?php
namespace frontend\components;

use Yii;
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use commmon\models\Stores;
use common\models\User;
use common\models\Conferences;
use common\models\ConsumerPromotions;
use common\models\Brands;
use common\models\Suppliers;


class B2bHeaderMenu extends Nav
{

	public $exclustions = [
	        'categories' => ['PRINTERS AND SCANNERS', 'COMPUTER COMPONENTS'],
	        //'categories' => [],
	        'brands' => ['Unbranded', ],
	        'suppliers' => [],
    	];

	public function init()
    {
    		
    	$html = '<ul><li><a href="'.Yii::$app->params['b2bUrl'].'"><span>Home</span></a></li>';

    	$brands = Brands::find()->select(['id', 'title', 'enabled'])->andWhere("title NOT IN ('".implode("','", $this->exclustions['brands'])."') AND isVirtual=0")->orderBy("title asc")->all();
        $suppliers = Suppliers::find()->andWhere("firstname NOT IN ('".implode("','", $this->exclustions['suppliers'])."')")->orderBy("firstname asc")->all();
    	
    	$todays = date("Y-m-d H:i:s");
        //$promotions = ConsumerPromotions::find()->where("status = 'published' and fromDate < '$todays' and toDate > '$todays'")->all();

        /*if($conference = Conferences::find()->where("status = '1' and fromDate < '$todays' and toDate > '$todays'")->one()){
            $html .= '  <li class="conference_name"><a href="'.\yii\helpers\Url::to(['conferences/index', 'id'=>$conference->id]).'"><span>'.$conference->title.'</span></a></li>';
        }
        elseif($conference = Conferences::find()->where("status = '0' and fromDate < '$todays' order by toDate DESC")->one()){
            $html .= '  <li class="conference_name"><a href="'.\yii\helpers\Url::to(['conferences/index', 'id'=>$conference->id]).'"><span>core ranges</span></a></li>';
        }*/

        $html .= '  <li class="conference_name"><a href="'.\yii\helpers\Url::to(['cart/saved']).'"><span>QUICK LIST</span></a></li>';

        $html .= '  <li class="parent">
                        <a href="javascript:void(0)"><span>Supplier</span></a><ul class=""><div id="content-2" class="scoller-head">';
                        foreach ($suppliers as $supplier) { 
                            if($supplier->hasProducts(true))
                                $html .= '<li class=""><a href="'.\yii\helpers\Url::to(['suppliers/products', 'id'=>$supplier['id']]).'" class=""><span>'.$supplier['firstname'].' '.$supplier['lastname'].'</span></a></li>';
                        }
        $html .= '  </div></ul> </li>';

        $html .= '  <li class="parent">
                        <a href="javascript:void(0)"><span>Brands</span></a><ul class=""><div id="content-1" class="scoller-head">';
                        foreach ($brands as $brand) { 
                            if($brand->hasProducts())
                                $html .= '<li class=""><a href="'.\yii\helpers\Url::to(['brands/products', 'id'=>$brand['id']]).'" class=""><span>'.$brand['title'].'</span></a></li>';
                        }
        $html .= '</div></ul> </li>';

        /*if(!empty($promotions)){
            $html .= '  <li class="parent"><a href="javascript:void(0)"><span>Catalogues</span></a><ul class="">';
            foreach ($promotions as $promotion) {
                $html .='<li class=""><a href="'.\yii\helpers\Url::to(['promotions/products', 'id'=>$promotion->id]).'" class=""><span>'.$promotion->title.'</span></a></li>'; 
            }
            $html .= '</ul></li>';
        }*/

    	echo " <script>
        (function($){
            $(window).load(function(){
                $('#content-1').mCustomScrollbar({
                    theme:'minimal'
                });
        $('#content-2').mCustomScrollbar({
                    theme:'minimal'
                });
                
            });
        })(jQuery);
    	</script> " ;

    	echo $html;
    }	

}
?>