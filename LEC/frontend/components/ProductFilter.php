<?php
namespace frontend\components;
use yii\bootstrap\Widget;
use common\models\Categories;
use common\models\Brands;
use common\models\Stores;
use common\models\Attributes;
use common\models\AttributeOptions;
use yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax; 

?>

<link href="/css/tree_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">

<?php
class ProductFilter extends Widget
{
	public $basedModel = null;
	public $attributes = [];
	public $attributeExceptions = ['f_size', 'country_of_manufacture', 'length','brand_name'];
	public $query;
	public $attributeOrder = ['gender', 'producttype', 'style', 'metaltype', 'metalcolour', 'gemstonetype', 'diamondshape', 'material'];
	public function init()
    {
    	$store = Stores::findOne(Yii::$app->params['storeId']);  	
		$form = ActiveForm::begin(['id' => 'filter-form']); 
       	$codeIdMap = $codeTitleMap = [];
		if(!empty($this->attributes)){	
			$codeIdMap = ArrayHelper::map($this->attributes, 'code', 'id');
			$codeTitleMapRaw =ArrayHelper::map($this->attributes, 'code', 'title');
			foreach($this->attributeOrder as $code){
				if(isset($codeTitleMapRaw[$code])){
					$codeTitleMap[$code] = $codeTitleMapRaw[$code];
					unset($codeTitleMapRaw[$code]);
				}
			}
			if(!empty($codeTitleMapRaw)){
				foreach($codeTitleMapRaw as $remainingCode => $remainingTitle)
					$codeTitleMap[$remainingCode] = $remainingTitle;
			}
		}

		//var_dump($this->basedModel->productsBrands);die();

		/*foreach ($this->basedModel->productsBrands as $brand) {
			var_dump($brand->title);
		}
		die();*/

		echo '<p class="block-subtitle">Shopping Options</p>
	          <div class="s_category">
	          <div><b>Price</b></div>
	            <!-- Price range -->
	                <div class="range-slider">
	                  <div>
	                    <input type="text" id="range" value="" name="range" />
	                  </div>
	                </div>
	            <!-- End -->
	          </div>
	        
	          ';

	    if(count($this->basedModel->productsBrands) > 1){
		    echo '<div class="s_category"><div><b>Brands</b></div><div class="cat_name">';
		    foreach ($this->basedModel->productsBrands as $brand) {
				echo '<label><input type="checkbox" name="Products[Brands][]" value="'.$brand->id.'"> '.$brand->title.'</label>';
			}	
			echo '</div></div>';   
		}	   
	          	
	        	/*$categories = Categories::find()
            			->where(['active' => '1'])
            			->andWhere('id != 0')
            			->asArray()
            			->all();*/
            	
            	$elements = [];		

            	/*if(!$this->basedModel instanceof \common\models\Categories){
            		
            		$elements[] = 
            					[
            						'label' => 'Categories',
            						'parent' => 'nil',
            						'items' => $this->getItems($categories,null),
            					];

            		//var_dump($elements);die();			
            	}else{
	        		if(isset($this->basedModel)){
							$elements[] =   
									[
										'label' => $this->basedModel->title,
			                     		'id' => $this->basedModel->id,
			                     		'parent' => $this->basedModel->parent,
			                     		'items' => $this->getItems($categories,$this->basedModel->id),
		                    		];
		        	}
	        	}
				if(count($elements[0]['items']) > 1){
					echo '<div class="s_category product_cats" style="display:none">';
        			echo '<div><b>Category</b></div>';
        			if(!$this->basedModel instanceof \common\models\Categories)
	        			$result = $this->getBrandCategoriesRecrusive($elements);
	        		else	
	        			$result = $this->getCategoriesRecrusive($elements); 
	       			
					echo "<div id='left' class='span3'><ul id='menu-group-1' class='nav-sm'>";
					echo $result;
					
	       			echo '</ul></div></div>';
				}*/

       			if(isset($this->attributes)){  //var_dump($this->attributes);die();
				
					foreach($codeTitleMap as $attributeCode => $attributeTitle){  //var_dump($attributeCode);die();
						if(!in_array($attributeCode, $this->attributeExceptions)){
							$options = $this->basedModel->getFilterAttributeValues($attributeCode);
							 $opts = [];
							 foreach($options as $option){
							 	if(!\frontend\components\Helper::isJson($option->value)) 
							 		$opts[] = $option;
							 	else{
							 		$jsonOptions = json_decode($option->value, true);
							 		if(is_array($jsonOptions)){
								 		foreach($jsonOptions as $jo){
								 			if(!in_array($jo, ArrayHelper::map($opts, 'value', 'value'))){
								 				$option = new \stdClass;
								 				$option->value = $jo;
								 				$option->formattedValue = $jo;
								 				$opts[] = $option;
								 			}
								 		}
								 	}else
								 		$opts[] = $option;
							 	}
							 }
							if(count($opts) > 1){
				        	  	echo '<div class="s_category"><div><b>'.$attributeTitle.'</b></div>';
				                echo Html::checkBoxList('Products[Attributes]['.$codeIdMap[$attributeCode].']', [], ArrayHelper::map($opts, 'value', 'value'), ['class'=>'cat_name']);
								echo '</div>';
							}
						}
		        	}
	        	}	
	        	echo "<input type='hidden' class='price-min' name='Products[priceRange][min]' value=''>";
        		echo "<input type='hidden' class='price-max' name='Products[priceRange][max]' value=''>";
        		echo "<input type='hidden' class='page-size' name='per-page' value='".((Yii::$app->id=="app-b2b")? "20" : "12")."'>";
        		if(get_class($this->basedModel) == "common\models\ConsumerPromotions" && Yii::$app->id == "app-b2b")
			    echo "<input type='hidden' class='sort' name='sort' value='pageId'>";
                        else
                            echo "<input type='hidden' class='sort' name='sort' value='price'>";
        		echo "<input type='hidden' class='keyword' name='keyword'>";
        	  ActiveForm::end();
        	  echo '<div class="s_category"><div class="sum"></div></div>';
        	  $loaderGif = (Yii::$app->id == "app-b2b")? "$('.product_lists').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>');" : "$('.Product_Featured').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>');";
        	  $stars = "$('.rating-star').rating({disabled: true, showClear: false, showCaption: false});$('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');" ;
        	  echo "
        	  	<script type=\"text/javascript\">
					$(document).ready(function() {
						$('body').on('click', '.sort-direction', function(){
							var direction = $(this).hasClass('asc')? '+' : '-';
					        $('#filter-form .sort').val(direction + $('.sort-by select').val());
					        filterproducts();
					    })
				  		$(\".s_category input[type='checkbox']\").change(function(e){
				  			filterproducts();
						}); 
						$('.ui-slider-handle').on('mouseleave', function(){ 
							filterproducts();
						});
						
						$('#range').ionRangeSlider({
							hide_min_max: true,
							keyboard: true,
							min: 0,
							max: ".(method_exists(get_class($this->basedModel), 'getProductsMaximumPrice')? $this->basedModel->productsMaximumPrice : 14000).",
							// from: 1000,
							// to: 4000,
							type: 'double',
							step: 1,
							prefix: '$',
							grid: true,
							onStart: function (data) {
        						$('.price-min').val(data.from);
								$('.price-max').val(data.to);
						        filterproducts();
    						},
							onFinish: function (data) {
								$('.price-min').val(data.from);
								$('.price-max').val(data.to);
						        filterproducts();
						    }
						}); 

						//$('#menu-group-1 li.parent:first').addClass('active');
					});	 
				</script>  

				<script type=\"text/javascript\"> 
					function filterproducts(){
						".$loaderGif."
						$.ajax({
				           	url: \"\",
				           	type: 'GET',
				           	data: $('#filter-form').serialize(),
				                 success: function(data)
				                 { //console.log($(data.trim()).find(\".Product_Featured\").html());
				                   $(\".Product_Featured\").html($(data.trim()).find(\".Product_Featured\").html());
						   //$('body').prepend('<div style=\'display: none;\' class=\'filterproducts-output\'>'+data+'</div>');
                                   totopslow();
                                   ".$stars."
				     		}
				   		});
					}
				</script>

				<script type=\"text/javascript\">
    			$(document).ready(function ($) {
    			$('body').on('click','.s_category .sign', function(){   
			        $(this).find('i:first').toggleClass('icon-minus');      
			    }); 
			    
			    //$('.s_category ul#menu-group-1 li.parent > a > span.sign').find('i:first').addClass('icon-minus');
			    $('.s_category ul#menu-group-1 li.current').parents('ul.children').addClass('in');

			});

			</script>";
    }

    public function getItems($categories,$parent=null){  //var_dump($parent);die();
        $newelements = array();
        foreach ($categories as $category) {
            if($category['parent'] == $parent) {
                $newelements[] =
                    ['label' => $category['title'], 
                     'id' => $category['id'],
                     'parent' => $category['parent'],
                     'items' => $this->getItems($categories,$category['id']),
                    ];
            }

            if(is_null($parent)){
        		$newelements[] =
                    ['label' => $category['title'], 
                     'id' => $category['id'],
                     'parent' => $category['parent'],
                     'items' => $this->getItems($categories,$category['id']),
                    ];
        	}
        }
		
		return $newelements;  
    }

    private function getCategoriesRecrusive($data,$child=FALSE){
       
        $html = '';
        $key=2;
        if(!empty($data)){
	        foreach($data as $item){  
	            if(isset($item['items'])) {  
	            	if(!empty($item['items']) && $item['parent'] != 0){  //var_dump($item['parent']);die();
	            		$html .= '<li class="item-'.$key.' deeper parent">
		                			<a>
			                			<span data-toggle="collapse" data-parent="#menu-group-1" href="#sub-item-'.$key.'" class="sign">
			                				<i class="icon-plus icon-white"></i>
			                				<!--<input type="hidden" name="Categories[]" value="'.$item['id'].'"/>-->
			                			</span>
		                				<span class="lbl">'.$item['label'].'</span>
		                			</a>
		                			<ul class="children nav-child unstyled collapse" id="sub-item-'.$key.'">';
		                $html.= $this->getCategoriesRecrusive($item['items'],TRUE);
		                $html.= '</ul></li>';    
	            	}
	            	elseif (!empty($item['items']) && $item['parent'] == 0) {
	            	  	$html .= '<li class="item-1 deeper parent">
		                			<a>
			                			<span data-toggle="collapse" data-parent="#menu-group-1" href="#sub-item-1" class="sign">
			                				<i class="icon-plus icon-white"></i>
			                				<!--<input type="hidden" name="Categories[]" value="'.$item['id'].'"/>-->
			                			</span>
		                				<span class="lbl">'.$item['label'].'</span>
		                			</a>
		                			<ul class="children nav-child unstyled collapse" id="sub-item-1">';
		                $html.= $this->getCategoriesRecrusive($item['items'],TRUE);
		                $html.= '</ul></li>';
	            	}  
		            else {  
		            	$html .= '<li class="child_sub"><input type="checkbox" value="'.$item['id'].'" name="Categories[]"><span class="lbl">'.$item['label'].'</span>';
		                $html.= '</li>';    
		            } 
		        }    

	            $key = $key+1;
	        }
	    }    
      	return $html;
    }

    private function getBrandCategoriesRecrusive($data,$child=FALSE){
       
        $html = '';
        $key=2;	
        if(!empty($data)){
	        foreach($data as $item){
	        	if(!empty($item['items']) && $item['parent'] != 0){
	        		$html .= '<li class="item-'.$key.' deeper parent">
		                			<a>
			                			<span data-toggle="collapse" data-parent="#menu-group-1" href="#sub-item-'.$key.'" class="sign">
			                				<i class="icon-plus icon-white"></i>
			                				<!--<input type="hidden" name="Categories[]" value="'.$item['id'].'"/>-->
			                			</span>
		                				<span class="lbl">'.$item['label'].'</span>
		                			</a>
		                			<ul class="children nav-child unstyled collapse" id="sub-item-'.$key.'">';
		                $html.= $this->getCategoriesRecrusive($item['items'],TRUE);
		                $html.= '</ul></li>';
	        	}

	        	elseif (!empty($item['items']) && $item['parent'] == 0) {
	            	  	$html .= '<li class="item-1 deeper parent">
		                			<a>
			                			<span data-toggle="collapse" data-parent="#menu-group-1" href="#sub-item-1" class="sign">
			                				<i class="icon-plus icon-white"></i>
			                				
			                			</span>
		                				<span class="lbl">'.$item['label'].'</span>
		                			</a>
		                			<ul class="children nav-child unstyled collapse" id="sub-item-1">';
		                $html.= $this->getCategoriesRecrusive($item['items'],TRUE);
		                $html.= '</ul></li>';
	            	} 
	        }

	        $key = $key+1;
	    }

	   return $html;    	
    }


}
?>
