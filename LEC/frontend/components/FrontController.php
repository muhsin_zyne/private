<?php
namespace app\components;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Products;


class FrontController extends \common\components\MainController
{
    public $title;
    public $mainMenu;
    public $metaTitle;
    public $metaKeywords;
    public $metaDescription;
    public $byodOverride = false;
    public $loginRequiredaccess = ['dashboard','account','myorders','wishlist'];
    public $byodOverrideActions = ['checkout/placeOrder','checkout/process','products/view','cart/add','cart/update','cart/delete','cart/view','byod/view'];
    public function init(){     
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-suppliers"){
            $this->metaTitle = \common\models\Configuration::findSetting('meta_title',Yii::$app->params['storeId']);//var_dump($this->metaTitle);die;
            $this->metaKeywords = \common\models\Configuration::findSetting('meta_keywords',Yii::$app->params['storeId']);
            $this->metaDescription = \common\models\Configuration::findSetting('meta_description',Yii::$app->params['storeId']);
        }else{

        }
        return parent::init();
    }
    public function beforeAction($action){  //var_dump($action->id);die();
	    if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-suppliers")
	       $this->hasSpecialProducts();
        if (\Yii::$app->getUser()->isGuest && \Yii::$app->getRequest()->url !== Url::to(\Yii::$app->getUser()->loginUrl) && in_array($action->id, $this->loginRequiredaccess)){
            \Yii::$app->getResponse()->redirect(\Yii::$app->getUser()->loginUrl);
        }
        /*if(Yii::$app->id == "app-b2b"){
            $route = explode("/", substr(Yii::$app->controller->route, 1));
            if(strpos(Yii::$app->controller->route, "b2b") === FALSE){ //var_dump(Yii::$app->controller->route); die;
                //var_dump(Url::to(["b2b/".Yii::$app->controller->route])); die;
                //return $this->redirect(Yii::$app->params['b2bUrl']."/b2b/".Yii::$app->controller->route); //die;
            }else{
                //var_dump(Yii::$app->controller->route); die;
            }
        }*/
        //var_dump(Yii::$app->controller->route); die;x`
        Yii::$app->cart->on(\yz\shoppingcart\ShoppingCart::EVENT_CART_CHANGE, function ($event){ // re-check if the coupons are still valid on the current cart
            Yii::$app->cart->refreshCoupons();
            Yii::$app->cart->refreshVouchers();
        });
        /*if(isset(Yii::$app->session['appliedCoupons'])){
            Yii::$app->cart->on(\yz\shoppingcart\ShoppingCart::EVENT_COST_CALCULATION, function ($event){ 
                $event->discountValue = (isset(Yii::$app->session['appliedCoupons'])? array_sum(array_values(Yii::$app->session['appliedCoupons'])) : 0);
                //var_dump($event->discountValue); die;
            });
        }*/

        if(isset(Yii::$app->session['appliedCoupons'])){
            Yii::$app->cart->on(\yz\shoppingcart\ShoppingCart::EVENT_COST_CALCULATION, function ($event){ 
                $event->discountValue = 0;
                foreach (Yii::$app->session['appliedCoupons'] as $details) {
                    if(is_array($details)){ 
                        $event->discountValue = array_sum(array_values($details)) + $event->discountValue;
                    }
                    else
                        $event->discountValue = $details + $event->discountValue;    
                }       
            });
        }     

        //var_dump(Yii::$app->cart->getCost(true)); die;
        //var_dump(Yii::$app->request->resolve()[0]);die();
        if(in_array(Yii::$app->request->resolve()[0], $this->byodOverrideActions)){  //var_dump(Yii::$app->request->resolve()[0]);die();
            $this->byodOverride = true;
        }
        return parent::beforeAction($action);
    }

    public function forward($route, $params = []){
        Yii::$app->runAction($route, $params);
    }

    public function hasSpecialProducts(){

        $todays = date("d-m-Y");

        $validFromProducts = ArrayHelper::map(Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->where('a.code = "special_from_date" and STR_TO_DATE(value,"%d-%m-%Y") < STR_TO_DATE("'.$todays.'","%d-%m-%Y")')->andWhere(['storeId' => (!is_null(Yii::$app->params['storeId'])? Yii::$app->params['storeId'] : 0)])->all(), 'id', 'self');

        $validToProducts = ArrayHelper::map(Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->where('a.code = "special_to_date" and STR_TO_DATE(value,"%d-%m-%Y") > STR_TO_DATE("'.$todays.'","%d-%m-%Y")')->andWhere(['storeId' => (!is_null(Yii::$app->params['storeId'])? Yii::$app->params['storeId'] : 0)])->all(), 'id', 'self');

        if(array_intersect(array_keys($validFromProducts), array_keys($validToProducts))){
            Yii::$app->session['saleMenuVisible'] = true;
        }
    }

    public function sendPopupCookie(){
        $cookie = new yii\web\Cookie(['name' => 'popup_shown', 'httpOnly' => true]);
        $cookie->value = "1";
        $cookie->expire = strtotime("+365 days");
        Yii::$app->getResponse()->getCookies()->add($cookie);
    }
    
    public function subscribeToTalkbox($email,$tag_id) {
        error_reporting(0);
        if(!empty(Yii::$app->params['talkboxConfig'])) {
            $username = Yii::$app->params['talkboxConfig']['username'];
            $password = Yii::$app->params['talkboxConfig']['password'];

            //endpoint for get request
            //refer to talkbox.impactapp.com.au/developers/api
            $endpoint = Yii::$app->params['talkboxConfig']['endpoint'];

            $postdata = http_build_query(
                    array(
                        'email' => $email
                    )
            );

            $opts = array(
                'http' => array(
                    'method' => "POST",
                    'header' => "Connection: close\r\n" .
                    "Content-type: application/x-www-form-urlencoded\r\n" . "Authorization: Basic " . base64_encode("$username:$password") . "\r\n" .
                    "Content-Length: " . strlen($postdata) . "\r\n",
                    'content' => $postdata
                )
            );


            $context = stream_context_create($opts);

            //execute request and get result into $file
            try {
                $file = file_get_contents($endpoint, false, $context);

                $json = json_decode($file, true);

                $id = $json['id'];
            } catch (Exception $exc) {
                // echo $exc->getTraceAsString();
            }

            if (!empty($id)) {
                $endpoint = "https://talkbox.impactapp.com.au/service/v1/tags/".$tag_id."/contacts/" . $id;

                $opts = array(
                    'http' => array(
                        'method' => "POST",
                        'header' => "Connection: close\r\n" .
                        "Content-type: application/x-www-form-urlencoded\r\n" . "Authorization: Basic " . base64_encode("$username:$password") . "\r\n"
                    ,
                    )
                );

                $context = stream_context_create($opts);
                //execute request and get result into $file
                $file = file_get_contents($endpoint, false, $context);
                
                $model = new \frontend\models\SubscriptionForm();
                $model->sendEmail(Yii::$app->params['storeId'], $email);
                
                Yii::$app->session->setFlash('success', 'Thank you! You will be the first to get updates from us!');
            } else {
                Yii::$app->session->setFlash('error', 'Sorry!. you are already subscribed!..');
            }
        }
        else {
            Yii::$app->session->setFlash('error', 'Sorry!. Please configure the talkbox configuration!..');
        }
    }

    /*public function setByodOverride()
    {
        $_REQUEST['setRequestOverride'] = true;
    }

    public function unsetByodOverride()
    {
        unset($_REQUEST['setRequestOverride']);
    }*/
}        
