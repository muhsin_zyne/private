<?php

/**
 * @author: Max
 */

namespace frontend\components;
use yii\base\Component;

class Helper extends Component{
	public static function slugify($text){
	    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
	    $text = trim($text, '-');
	    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	    $text = strtolower($text);
	    $text = preg_replace('~[^-\w]+~', '', $text);
	    if (empty($text))
	      	return 'n-a';
	  	return $text;
	}

	public static function money($string){
		setlocale(LC_MONETARY, 'en_AU');
		//$symbol = 'AU';
		$symbol = 'AU$';
		$color = (preg_match("/-/",$string)) ? 'red' : '';
		$string = ($string == null) ? 0 : $string;
		return "<span class='$color'>$symbol".money_format('%!.2n', trim($string))."</span>";
	}

	public static function date($string, $format = "d/m/Y"){
		return date($format, strtotime($string));
	}

	public static function convertDate($string){
		$recieved_date = explode('/', $string);
        $date = $recieved_date[1] . '/' . $recieved_date[0] . '/' . $recieved_date[2];
        $new_date = date('Y-m-d H:i:s', strtotime($date));
		return $new_date;
	}

	public static function getVoucherCode($id = NULL){
		if(is_null($id))
			$id = rand(1, 1000);
		$code = "";
		$codelen = 12;
		for( $i=strlen($code); $i<$codelen; $i++) { 
		    $code .= dechex(rand(0,15));
		    if($i%4==0 && $i!=0)
				$code = $code."-";
		}
		return strtoupper($code);
	}

	public static function isJson($string) {
	    json_decode($string);
	    return (json_last_error() === JSON_ERROR_NONE);
	}

	public static function stripText($title, $limit = 40){
		$stripped = $title;
		if(strlen($title)>$limit){ //var_dump($title); die;
		   	$stripped = substr($title,0,$limit-3)."..";
		} 
		return $stripped;
	}

	public static function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public static function escape($value) {
	    $return = '';
	    for($i = 0; $i < strlen($value); ++$i) {
	        $char = $value[$i];
	        $ord = ord($char);
	        if($char !== "'" && $char !== "\"" && $char !== '\\' && $ord >= 32 && $ord <= 126)
	            $return .= $char;
	        else
	            $return .= '\\x' . dechex($ord);
	    }
	    return $return;
	}

	public static function getPriceExclGST($price,$gstPercentage){
		$exclPrice = ($price*100)/(100+$gstPercentage);
		return round($exclPrice,2);
	}

	public static function localDate($date, $format = 'Y-m-d H:i:s', $defaultTimezone = 'UTC', $toTimezone = 'Australia/Sydney'){
		date_default_timezone_set($defaultTimezone);
	    $new_date = new \DateTime($date);
	    $new_date->setTimeZone(new \DateTimeZone($toTimezone));
	    return $new_date->format($format);
	}
}
