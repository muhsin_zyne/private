<?php

namespace frontend\components;
use yii;
use app\assets\AppAsset;
use yii\bootstrap\Nav;
use common\models\Categories;
use common\models\Gallery;
use common\models\StorePages;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Menus;
use common\models\StorePromotions;
use common\models\ConsumerPromotions;
use common\models\Stores;

class MainMenu extends Nav {

    public $is_Mobile=false;
    public $storeMenuExclusions = [ '136' => ['brands','advice'], '63'=>['brands'], '112'=>['advice']];
    public $commonMenus = ['brands','gallery','advice'];

    public function init() {
        $store=Stores::findOne(Yii::$app->params['storeId']);
        $activeCategory=$store->activeCategoryIds;
        //var_dump($activeCategory);var_dump('------------------------------');
        //$categories = Yii::$app->db->createCommand("select id from Categories")->queryAll();        
        //$activeCategory=ArrayHelper::map($categories, 'id', 'id');
        
        array_push($activeCategory,"0");
        //var_dump($activeCategory);die;
        $items = Menus::find()   //orderby position
            ->where(['storeId' => Yii::$app->params['storeId'],'unAssigned'=>0,'type'=>'main'])
            ->orderBy('position')
            //->asArray()
            ->all();
            //var_dump($items);die;
        $items_title = Menus::find() //orderby title
            ->where(['storeId' => Yii::$app->params['storeId'],'unAssigned'=>0,'type'=>'main'])
            ->orderBy('title')
            //->asArray()
            ->all();
        $elements = [];
        foreach ($items as $item) {
            if($item['parent']=='0'){
                if(preg_match("/http/", $item['path']))
                {
                    $elements[] =
                    [   'label' => $item['title'],
                        'url' => $item['path'],
                        'icon'=>($item['categoriesId']!=0 )?$item->categories->icon:'',
                        'id' => $item['id'],
                        'categoriesId'=>$item['categoriesId'],
                        'htmlAttributes'=>isset($item['htmlAttributes'])?$item['htmlAttributes']:'',
                        'items' => $this->getItems($items_title,$item['id']),
                    ];
                }
                else {
                  $elements[] =
                    [   'label' => $item['title'],
                        'url' => Yii::$app->urlManager->baseUrl."/".$item['path'],
                        'id' => $item['id'],
                        'icon'=>($item['categoriesId']!=0 && isset($item->categories->icon))?$item->categories->icon:'',
                        'categoriesId'=>$item['categoriesId'],
                        'htmlAttributes'=>isset($item['htmlAttributes'])?$item['htmlAttributes']:'',
                        'items' => $this->getItems($items_title,$item['id']),
                    ];  
                }
            } 
        }


        $role_id = 1;
        if($this->is_Mobile==true)//It's a mobile browswe
            $result = $this->getMenuRecrusiveMobile($elements,$activeCategory,FALSE);
        else
            $result = $this->getMenuRecrusive($elements,$activeCategory,FALSE);
        echo $result;
    }

    public function getItems($items_new,$parent){


        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {
                if(preg_match("/http:/", $newitem['path']))
                {   
                    $newelements[] =
                    [   'label' => $newitem['title'],
                        'url' => $newitem['path'],
                        'id' => $newitem['id'],
                       
                        'icon'=>($newitem['categoriesId']!=0)?$newitem->categories->icon:'',
                        'categoriesId'=>$newitem['categoriesId'],
                        'htmlAttributes'=>isset($newitem['htmlAttributes'])?$newitem['htmlAttributes']:'',
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];
                }
                else
                {
                   $newelements[] =
                    [   'label' => $newitem['title'],
                        'url' => Yii::$app->urlManager->baseUrl."/".$newitem['path'],
                        'id' => $newitem['id'],
                        'icon'=>($newitem['categoriesId']!=0)?$newitem->categories->icon:'',
                        'categoriesId'=>$newitem['categoriesId'],
                        'htmlAttributes'=>isset($newitem['htmlAttributes'])?$newitem['htmlAttributes']:'',
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ]; 
                }
            }
        }
        return $newelements;  
    }

    private function getMenuRecrusive($data,$activeCategory,$child=FALSE){

        $html='<ul class="nav">';
            foreach($data as $item){ 
                if(isset($item['items'])) { //var_dump($item['categoriesId']);
                    if (in_array($item['categoriesId'], $activeCategory)) {
                        $html .= '<li><a href="'.$item['url'].'" class="'.$item['htmlAttributes'].'"><i class="sm '.$item['icon'].'"></i><span>'.$item['label'].'</span></a>';
                        $html.= $this->getMenuRecrusive($item['items'],$activeCategory,TRUE); 
                        $html.= '</li>';  
                    }                
                }  
                else { 
                    $html .= '<li ><a href="'.\yii\helpers\Url::to(['/categories/products', 'categoryId'=>$item['id']]).'">'.$item['label'].'</a>';
                    $html.= '</li>';    


                } 
            }
                    $html.='
                </ul>';
        return $html;

    }

/** 
** Creating Menu for the Mobile we site
** Added by Sreedev on 6 July 2016 @ 12:30 PM  
**/

    private function getSubMenuRecrusiveMobile($data,$activeCategory,$child=FALSE){ //var_dump($activeCategory);die();
        $html = '';
        foreach ($data as $item) {
            if(empty($item['items'])){
                if (in_array($item['categoriesId'], $activeCategory)) {
                    $html .='<li class="parent"><a href="'.$item['url'].'"><span>'.$item['label'].'</span></a>';
                    $html.= $this->getMenuRecrusiveMobile($item['items'],$activeCategory,TRUE);
                    $html.= '</li>';
                }    
            }
            else{
                if (in_array($item['categoriesId'], $activeCategory)) {
                    $html .='<li class="parent"><a class="main_menu" href="javascript:;"><span>'.$item['label'].'</span></a><ul class="sub-menu">';
                    $html .='<li><a href="'.$item['url'].'"><span>All</span></a></li>';
                    $html.= $this->getSubMenuRecrusiveMobile($item['items'],$activeCategory,TRUE);
                    $html.= '</ul></li>';
                }    
            }    
        }       
        return $html;
    }        
    
    private function getMenuRecrusiveMobile($data,$activeCategory,$child=FALSE){

        $store=Stores::findOne(Yii::$app->params['storeId']);
        //$html = '<h2 class="icon icon-world">All Categories</h2>';
        if($child==FALSE)
        {
            $html= '<ul><li><a href="'.yii\helpers\Url::to('@web/').'" class="icon icon-photo"><span>Home</span></a></li>';
            $html.= '<li><a href='.\yii\helpers\Url::to(['/about-us.html']).' class="icon icon-photo"><span>About Us</span></a></li>'; 
        }
        else{
            $html= '<ul class="shown-sub">';
        }
        foreach($data as $item){
            if(!empty($item['items'])) {
                ///var_dump($activeCategory);die;
                if (in_array($item['categoriesId'], $activeCategory)) {
                    $html .='<li class="icon icon-arrow-left"><a class="icon icon-news" href="javascript:;">'.$item['label'].'</a><div class="mp-level"><h2 class="icon icon-news">'.$item['label'].'</h2><a class="mp-back" href="#">back</a><ul>';
                    $html .='<li><a href="'.$item['url'].'"><span>All</span></a></li>';
                    $html.= $this->getSubMenuRecrusiveMobile($item['items'],$activeCategory,TRUE);
                    $html.= '</ul></div></li>';
                }    
            }
            else{
                $html .= '<li><a href="'.$item['url'].'">'.$item['label'].'</a>';
                $html .= '</li>';    
            }    
        }
        if($child==FALSE) {

            $todays = date("Y-m-d H:i:s");
            $storeId = Yii::$app->params['storeId'];
            $storePromotions = StorePromotions::find()->where("storeId = $storeId and endDate > '$todays' and active = 1")->all(); 
            //var_dump($storePromotions);die();
            if(!empty($storePromotions)){


                $html.= '<li class="icon icon-arrow-left"><a href="javascript:;" class="icon icon-news">Promotions</a><div class="mp-level"><h2 class="icon icon-news">Promotions</h2><a class="mp-back" href="#">back</a><ul>'; 
                foreach ($storePromotions as $storePromotion) {


                    $promotion = ConsumerPromotions::find()->where("status = 'published' and id = $storePromotion->promotionId")->one();
                    $html.= '<li><a href="'.\yii\helpers\Url::to(['/promotions/view', 'id'=>$promotion->id]).'" class="">'.$promotion->title.'</a></li>';

                } 
                $html .='</ul></div></li>';
            }   	

            $html.= '<li class="icon icon-photo"><a href='.\yii\helpers\Url::to(['/brands']).'><span>Brands</span></a></li>';   
            //$html.= '<li><a href='.\yii\helpers\Url::to(['site/contact']).'><span>Contact Us</span></a></li>'; 
            $html.= '<li class="icon icon-arrow-left"><a class="icon icon-news" href="javascript:;">Advice</a><div class="mp-level"><h2 class="icon icon-news">Advice</h2><a class="mp-back" href="#">back</a><ul>';


                $storePages = StorePages::find()->where(['storeId' => Yii::$app->params['storeId']])->orderBy('id')->all();


                foreach($storePages as $index => $storepage){  
                    if(isset($storepage->cmsPage->status)){
                        if($storepage->cmsPage->status==1)
                            {                 
                                $html.= '<li>
                                <a href="'.\yii\helpers\Url::base().'/'.$storepage->cmsPage->slug.'.html'.'" class="">'.\frontend\components\Helper::stripText($storepage->cmsPage->title,25).'</a>
                                </li>';
                            }
                    }    
                }
            $html.= '</ul></div></li>';
           // $html.= '<li class="parent"><a class="main_menu" href="javascript:;"><span>Gallery</span></a>';
            $html.= '<ul class="sub-menu" id="shown-sub">';
            $gallerys = Gallery::find()->where(['storeId' => Yii::$app->params['storeId']])->orderBy('position')->all();
                foreach($gallerys as $index => $gallery){
                    if(count($gallery->galleryImages)!=0)
                        {
                            $html.= '<li class="child_sub"><a href="'.\yii\helpers\Url::to(['/gallery/view', 'id'=>$gallery['id']]).'" class="">'.$gallery['title'].'</a></li>';
                        }
                }
            $html.= '</ul></li>'; 
            //$html.= '<li><a href="'.\yii\helpers\Url::to(['mobile/takeaphoto']).'"><span>Send a Photo</span></a>';
            $html.= '<li><a href="tel:'.$store->phone.'"><span>Call Us</span></a>';
        }
        $html .= '</ul>';
        return $html;
    }    
}   