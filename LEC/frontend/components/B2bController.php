<?php

namespace frontend\components;

use Yii;

use app\components\FrontController;

use yii\helpers\Url;

use common\models\User;





class B2bController extends FrontController

{	

	public function beforeAction($action){ //var_dump(Yii::$app->getRequest()->url); die;

		if (\Yii::$app->getUser()->isGuest) { 

			if(\Yii::$app->getRequest()->url !== Url::to(\Yii::$app->getUser()->loginUrl) && \Yii::$app->getRequest()->url !== Url::to(["/b2b/default/request-password-reset"])

                && preg_match("/password/", Yii::$app->getRequest()->url) !==1 && preg_match("/login-and-forward/", Yii::$app->getRequest()->url) !==1)

            {

            	\Yii::$app->getResponse()->redirect(Url::to(\Yii::$app->getUser()->loginUrl));

        	}



        }

		else{ 

            if(!isset(Yii::$app->params['storeId']))

                Yii::$app->params['storeId'] = ($store = \common\models\Stores::find()->where(['adminId' => Yii::$app->user->id])->one())? $store->id : null;

        }

        $this->view->registerJs(

           '$(document).on("pjax:start", function() {

                $("#category-products").html($(".loading_pjax").html());

            });'

        );

    	return parent::beforeAction($action);

    }    



}       