<?php
namespace frontend\components;
use Yii;
use app\components\FrontController;
use yii\helpers\Url;
use common\models\User;


class SuppliersController extends FrontController
{   
    
    public function beforeAction($action){ 
        if (\Yii::$app->getUser()->isGuest) { 
            if(\Yii::$app->getRequest()->url !== Url::to(\Yii::$app->getUser()->loginUrl) 
                && \Yii::$app->getRequest()->url !== Url::to(["/suppliers/default/request-password-reset"])
                && preg_match("/password/", Yii::$app->getRequest()->url) !==1
		&& preg_match("/loginas/", Yii::$app->getRequest()->url) !==1
		)
            {
                return \Yii::$app->getResponse()->redirect(Url::to(\Yii::$app->getUser()->loginUrl))->send();
            }
        }
		else{ 
        	Yii::$app->params['storeId'] = NULL;
        }
    	return true;    
    }    

}        