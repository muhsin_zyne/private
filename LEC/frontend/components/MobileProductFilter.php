<?php
	namespace frontend\components;
	use yii\bootstrap\Widget;
	use common\models\Categories;
	use common\models\Brands;
	use common\models\Stores;
	use common\models\Attributes;
	use common\models\AttributeOptions;
	use yii;
	use yii\bootstrap\ActiveForm;
	use yii\helpers\ArrayHelper;
	use yii\helpers\Html;
	use yii\web\View;
	use yii\widgets\Pjax; 
?>

<?php
class MobileProductFilter extends Widget
{
	public $basedModel = null;
	public $attributes = [];
	public $attributeExceptions = [];
	public $query;
	public $attributeOrder = [];




	public function init()
	{

		$store = Stores::findOne(Yii::$app->params['storeId']);  	
		$form = ActiveForm::begin(['id' => 'filter-form']); 
		$codeIdMap = $codeTitleMap = [];
		if(!empty($this->attributes)){	
			$codeIdMap = ArrayHelper::map($this->attributes, 'code', 'id');
			$codeTitleMapRaw =ArrayHelper::map($this->attributes, 'code', 'title');
			foreach($this->attributeOrder as $code){
				if(isset($codeTitleMapRaw[$code])){
					$codeTitleMap[$code] = $codeTitleMapRaw[$code];
					unset($codeTitleMapRaw[$code]);
				}
			}
			if(!empty($codeTitleMapRaw)){
				foreach($codeTitleMapRaw as $remainingCode => $remainingTitle)
					$codeTitleMap[$remainingCode] = $remainingTitle;
			}
		}

		echo '<div class="modal-body"><div class="sidebar">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#price" aria-controls="price" role="tab" data-toggle="tab">Price</a></li>
                    ';

        $elements = [];	            

        if(isset($this->attributes)){  
			foreach($codeTitleMap as $attributeCode => $attributeTitle){ 
				if(!in_array($attributeCode, $this->attributeExceptions)){
					$options = ArrayHelper::map($this->basedModel->getFilterAttributeValues($attributeCode), 'value', 'value');
					if(count($options) > 1){
						 echo '<li role="presentation"><a href="#'.str_replace(' ', '', $attributeTitle).'" aria-controls="'.str_replace(' ', '', $attributeTitle).'" role="tab" data-toggle="tab">'.$attributeTitle.'</a></li>';
					}
				}		
			}
			            
		echo '</ul></div>';    

        echo '<div class="content-bg">
                <!-- tab-content --> 
                <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="price">
                	<!-- Price range -->
					<div class="range-slider">
					<div>
					<input type="text" id="range" value="" name="range" />
					</div>
					</div>
					<!-- End -->
                </div>';

        foreach($codeTitleMap as $attributeCode => $attributeTitle){ 
			if(!in_array($attributeCode, $this->attributeExceptions)){
				$options = ArrayHelper::map($this->basedModel->getFilterAttributeValues($attributeCode), 'value', 'value');
				if(count($options) > 1){
						echo '<div role="tabpanel" class="tab-pane" id="'.str_replace(' ', '', $attributeTitle).'"><div class="check-group-bg"><div class="checkbox">';
                       	echo Html::checkBoxList('Products[Attributes]['.$codeIdMap[$attributeCode].']', [], ArrayHelper::map($this->basedModel->getFilterAttributeValues($attributeCode), 'value', 'value'),['itemOptions'=>['class'=>'check-type']]);
                        echo  '</div></div></div>';
				}
			}		
		}        


        echo '</div>
                </div></div>';

        }   

        echo "<input type='hidden' class='price-min' name='Products[priceRange][min]' value=''>";
		echo "<input type='hidden' class='price-max' name='Products[priceRange][max]' value=''>";
		echo "<input type='hidden' class='page-size' name='per-page' value='12'>";
		echo "<input type='hidden' class='sort' name='sort' value='dateAdded'>";
		echo "<input type='hidden' class='keyword' name='keyword'>";

		echo '<div class="modal-footer">
            <div class="col-xs-6 btns-bg"><button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button></div>
            <div class="col-xs-6"><input type="submit" class="btn btn-primary apply-filter" value="APPLY"></div>
          </div>';

        ActiveForm::end();

        $loaderGif = "$('.Product_Featured').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>');";

        $stars = "$('.rating-loading').rating({disabled: true, showClear: false, showCaption: false});$('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');" ;         

        echo "<script type=\"text/javascript\">
					$(document).ready(function() {

					/*$('body').on('click', '.sort-direction', function(){
						var direction = $(this).hasClass('asc')? '+' : '-';
						$('#filter-form .sort').val(direction + $('.sort-by select').val());
						filterproducts();
					})

				  	$(\".checkbox input[type='checkbox']\").change(function(e){
						filterproducts();
					}); 

					$('.ui-slider-handle').on('mouseleave', function(){ 
						filterproducts();
					});	*/

					

					$('#range').ionRangeSlider({
							hide_min_max: true,
							keyboard: true,
							min: 0,
							max: ".(method_exists(get_class($this->basedModel), 'getProductsMaximumPrice')? $this->basedModel->productsMaximumPrice : 14000).",
							type: 'double',
							step: 1,
							prefix: '$',
							grid: true,
							onStart: function (data) {
								$('.price-min').val(data.from);
								$('.price-max').val(data.to);
								/*filterproducts();*/
							},

							onFinish: function (data) {
								$('.price-min').val(data.from);
								$('.price-max').val(data.to);
								 /*filterproducts();*/
							}
						}); 

						//$('#menu-group-1 li.parent:first').addClass('active');

					});	 

				</script>

				<script type=\"text/javascript\"> 
					function filterproducts(){ 
						".$loaderGif."
						$.ajax({
						url: \"\",
						type: 'GET',
						data: $('#filter-form').serialize(),
								success: function(data)
								{ //console.log($(data.trim()).find(\".Product_Featured\").html());
									$(\"#category-products\").html($(data.trim()).find(\"#category-products\").html());
									/*totopslow();*/
									".$stars."
								}
						});
					}

					$( '#filter-form' ).submit(function(e) {
						e.preventDefault();
  						filterproducts();
  						$('#filterModal').modal('hide');
  					});
				</script>";    

		$elements = [];		
		
	}
}

?>			