<?php
namespace app\components;
use Yii;
use yii\grid;

class AdminGridView extends \yii\grid\GridView
{
    public $layout = "{summary}{buttons}\n{items}\n{pager}";
    public $filterRowOptions = ["id" => "products-filters", 'class' => 'filters'];
    public function init(){
        $this->view->registerJs("$(document).ready(function(){ $('.smreset').click(function(){ $('.grid-view .filters input').val(''); $('.grid-view .filters input').change(); }); });");
        //$this->view->registerJs("$(document).ready(function(){ $('.grid-view').yiiGridView({'afterFilter' : 'alert(\"test\");'}); });");
        //$this->clientOptions = $this->clientOptions + ['afterFilter' => 'alert("sdfkk"");'];
        return parent::init();
    }

    // public function run()
    // {
    //     $id = $this->options['id'];
    //     $options = ($this->getClientOptions());
    //     $view = $this->getView();
    //     \yii\grid\GridViewAsset::register($view);
    //     var_dump($options); die;
    //     $view->registerJs("jQuery('#$id').yiiGridView($options);");
    //     parent::run();
    // }

    public function renderSection($name)
    {   switch ($name) {
            case '{summary}':
                return $this->renderSummary();
            case '{items}':
                return $this->renderItems();
            case '{pager}':
                return $this->renderPager();
            case '{sorter}':
                return $this->renderSorter();
            case '{buttons}':
                return $this->renderButtons();    
            default:
                return false;
        }
    }

    public function renderButtons(){ 
        return '<div class="buttons-smrow"><button class="btn btn-primary smsrch">Filter</button><button class="btn btn-primary smreset">Reset</button></div>';
    }
}