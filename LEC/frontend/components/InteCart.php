<?php
/**
 * Author: Max
 */
namespace frontend\components;
use Yii;
use yz\shoppingcart\ShoppingCart;
use yz\shoppingcart\CartActionEvent;
use common\models\Stores;
use common\models\ClientPortal;
use yii\helpers\ArrayHelper;
use yz\shoppingcart\CostCalculationEvent;

class InteCart extends ShoppingCart{

    public function getPositions(){
        $positions = $this->_positions;
        foreach($positions as $position){
            if(!$position->product){ //var_dump($position->getId()); die; //
                $this->remove($position);
            }
        }
        return $this->_positions;
    }

    public function refreshCoupons(){
        if(!isset(Yii::$app->session['appliedCoupons']))
            return false;
        $coupons = Yii::$app->session['appliedCoupons'];  

        foreach($coupons as $ruleId => $details){
            if($rule = \common\models\PriceRules::findOne($ruleId)){
                if($rule->type=="fixed-product" || $rule->type=="percent-product"){
                    foreach($details as $productId => $discount){
                        $found = false;
                        foreach($this->positions as $position){
                            if($position->product->id == $productId){
                                if(!$rule->isValidWith($position)){
                                    unset($coupons[$ruleId][$productId]);
                                }
                                $found = true;
                                break;
                            }
                        }
                        if(!$found)
                            unset($coupons[$ruleId][$productId]);
                    }
                    if(empty($coupons[$ruleId]))
                        unset($coupons[$ruleId]);
                }else{
                    if(!$rule->isValidWith($this)){
                        unset($coupons[$ruleId]);  
                    }else{
                        $cartCost = Yii::$app->cart->cost;
                        if($rule->type=="fixed-cart"){
                            $discount = $rule->discount;
                            $withDiscount = $cartCost - (($rule->discount<=$cartCost)? $rule->discount : $cartCost);
                        }elseif($rule->type=="percent-cart"){ //var_dump($rule->type);die();
                            //$discount = (($rule->discount/$cartCost)/100);
                            $discount = $cartCost*($rule->discount/100);
                            //var_dump($discount);die();
                            $withDiscount = $cartCost - (($rule->discount<=$cartCost)? ($cartCost*($rule->discount/100)) : $cartCost);
                            //$withDiscount = $cartCost - $rule->discount;
                        }
                        elseif($rule->type=="free-shipping"){
                            $discount = 0;
                            $withDiscount = $cartCost;
                        }
                        $coupons[$ruleId] = $discount;
                    }
                }
            }
        }
        Yii::$app->session['appliedCoupons'] = $coupons;
    }

    public function refreshVouchers(){
        if(!isset(Yii::$app->session['appliedVouchers']))
            return false;
        $vouchers = Yii::$app->session['appliedVouchers'];
        $discount = $this->getVoucherDiscount();
        if($discount > $this->getCost(true)){
            foreach($vouchers as $id => $amount){
                if($amount > ($discount - $this->getCost(true))){
                    $vouchers[$id] = $amount - ($discount - $this->getCost(true));
                    $discount = $discount - ($discount - $this->getCost(true));
                    break;
                }else{
                    unset($vouchers[$id]);
                    $discount = $discount - $amount;
                }
            }
        }
        Yii::$app->session['appliedVouchers'] = $vouchers;
    }

    public function removeById($id)
    {
        $this->trigger(self::EVENT_BEFORE_POSITION_REMOVE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
            'position' => $this->_positions[$id],
        ]));
        $position = $this->_positions[$id];
        unset($this->_positions[$id]);
        $this->trigger(self::EVENT_CART_CHANGE, new CartActionEvent([
            'action' => CartActionEvent::ACTION_BEFORE_REMOVE,
            'position' => $position,
        ]));
        if ($this->storeInSession){
            $this->saveToSession();
        }
    }

    public function getVoucherDiscount(){
        $discount = 0;
        if(isset(Yii::$app->session['appliedVouchers'])){
            foreach(Yii::$app->session['appliedVouchers'] as $disc){
                $discount += $disc;
            }
        }
        return $discount;
    }

    public function getCouponDiscount(){  //die();
        $discount = 0;
        if(isset(Yii::$app->session['appliedCoupons'])){
            foreach(Yii::$app->session['appliedCoupons'] as $details){
                //$discount += $disc;
                 if(is_array($details)){ // it's a per product coupon

                   // var_dump(array_values($details)[0]);die();

                    $disc = array_sum(array_values($details));
                    $discount += $disc;
                }
                else //per cart coupon
                    $discount += $details;
            }
        }
        return $discount;
    }

    public function hasOnlyGiftVouchers(){
        foreach($this->positions as $position)
            if($position->product->typeId != "gift-voucher")
                return false;
        return true;
    }

    public function hasOnlySchoolByodProducts(){
        if(isset(Yii::$app->session['schoolByod'])){
            $store = Stores::findOne(Yii::$app->params['storeId']);
            $byodProducts = ArrayHelper::map($store->getPortalProducts('schoolByod')->all(), 'productId', 'portalProduct');
            foreach($this->positions as $position)
                if(!in_array($position->product->id, array_keys($byodProducts)))
                    return false;
            return true;
        }    
    }

    public function hasOnlyByodProducts(){
        return $this->hasOnlySchoolByodProducts() || $this->hasOnlyStudentByodProducts();
    }

    public function hasOnlyStudentByodProducts(){
        if(isset(Yii::$app->session['studentByod'])){
            $store = Stores::findOne(Yii::$app->params['storeId']);
            $byodProducts = ArrayHelper::map($store->getPortalProducts('studentByod')->all(), 'productId', 'portalProduct');
            foreach($this->positions as $position)
                if(!in_array($position->product->id, array_keys($byodProducts)))
                    return false;
            return true;
        }    
    }

    public function hasOnlyClientPortalProducts(){
        if(isset(Yii::$app->session['clientPortal'])){  
            $store = Stores::findOne(Yii::$app->params['storeId']);
            $clientPortalProducts = ArrayHelper::map($store->getPortalProducts('clientPortal')->all(), 'productId', 'portalProduct');
            foreach($this->positions as $position)
                if(!in_array($position->product->id, array_keys($clientPortalProducts)))
                    return false;
            return true;
        }    
    }

    /*public function beforeAdd(){  
        
    }*/

    public function check($position,$sessionvar){ //var_dump($sessionvar);die();
        if(isset(Yii::$app->session[$sessionvar])){ 
            $cart = Yii::$app->cart;
            if(empty($cart->positions)){ 
                return true;
            }
            else{  
                $store = Stores::findOne(Yii::$app->params['storeId']);
                $portalProducts = ArrayHelper::map($store->getPortalProducts($sessionvar)->all(), 'productId', 'portalProduct');
                if($this->{"hasOnly".ucfirst($sessionvar)."Products"}() || $this->hasOnlyClientPortalProducts()){
                    if(!in_array($position->product->id, array_keys($portalProducts)))
                        return false;
                }
                else{
                    if(in_array($position->product->id, array_keys($portalProducts)))
                        return false;
                }
                return true;
            }
        }
        else{
            return true;
        }
    }

    public function isPaymentRequired($sessionVar){  
        if($this->{"hasOnly".ucfirst($sessionVar)."Products"}()){ //var_dump(ucfirst($sessionVar));die();
            if($sessionVar == "studentByod")
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            elseif($sessionVar == "schoolByod")
                $portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            else
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one(); 

            if($portal->payment_type == "no-online-payment")
                return false;
            return true;
        }
        return true;
    }

    public function isClientPortalDeliveryProgram($sessionVar){ 
        if($this->{"hasOnly".ucfirst($sessionVar)."Products"}()){
            if($sessionVar == "studentByod")
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            elseif($sessionVar == "schoolByod")
                $portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            else
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            
            if($portal->shipment_type == "delivery-program")
                return true;
            return false;
        }
        return false;    
    }

    public function isClientPortalInstorePickup($sessionVar){
        if($this->{"hasOnly".ucfirst($sessionVar)."Products"}()){
            if($sessionVar == "studentByod")
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            elseif($sessionVar == "schoolByod")
                $portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            else
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
            if($portal->shipment_type == "instore-pickup")
                return true;
            return false;
        }
        return false;    
    }
    
      public function getGiftWrapCost(){
        $cost=0;
        $giftWrap= \common\models\Configuration::findSetting('gift_wrapping', Yii::$app->params['storeId']);
        foreach($this->positions as $position) {
            if($position->giftWrap) 
                $cost+=($giftWrap*$position->quantity);
        }
        return $cost;
           
    }
    
    
    /**
     * Return full cart cost as a sum of the individual positions costs
     * @param $withDiscount
     * @return int
     */
    public function getCost($withDiscount = false,$withGiftWrap=false) {
        $cost = 0;
        foreach ($this->_positions as $position) {
            $cost += $position->getCost($withDiscount,$withGiftWrap);
        }
        
        $costEvent = new CostCalculationEvent([
            'baseCost' => $cost,
        ]);
        $this->trigger(self::EVENT_COST_CALCULATION, $costEvent);
        if ($withDiscount)
            $cost = max(0, $cost - $costEvent->discountValue);
        
        if($withGiftWrap)
            $cost+=Yii::$app->cart->giftWrapCost;
        return $cost;
    }
}
