<?php



namespace frontend\components;

use yii;
use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Menus;
use common\models\Stores;



class FooterMenu extends Nav
{
    public $is_Mobile=false;
    public function init()
    {
        //$store=Stores::findOne(Yii::$app->params['storeId']);        
        $items = Menus::find()   //orderby position
            ->where(['storeId' => Yii::$app->params['storeId'],'unAssigned'=>0,'type'=>'footer',])
            ->orderBy('position')
            ->asArray()
            ->all();//var_dump($items);die;
        $elements = [];
        foreach ($items as $item) { 
            if($item['parent']=='0'){
                if(preg_match("/http/", $item['path']))
                {       
                    $elements[] =
                    [   'label' => $item['title'],
                        'url' => $item['path'],
                        'id' => $item['id'],
                        'categoriesId'=>$item['categoriesId'],
                        'target'=>'_blank',
                        'parent'=>$item['parent'],
                        'htmlAttributes'=>isset($item['htmlAttributes'])?$item['htmlAttributes']:'',
                        'items' => $this->getItems($items,$item['id']),
                    ];
                }
                else
                {
                  $elements[] =
                    [   'label' => $item['title'],
                        'url' => Yii::$app->urlManager->baseUrl."/".$item['path'],
                        'id' => $item['id'],
                        'categoriesId'=>$item['categoriesId'],
                        'parent'=>$item['parent'],
                        'target'=>'_self',
                        'htmlAttributes'=>isset($item['htmlAttributes'])?$item['htmlAttributes']:'',
                        'items' => $this->getItems($items,$item['id']),
                    ];
                }   
            } 

        }
        $role_id = 1;
        if($this->is_Mobile==true)//It's a mobile browswe
            $result = $this->getMenuRecrusiveMobile($elements,FALSE);
        else
            $result = $this->getMenuRecrusive($elements,FALSE);

        echo $result;
    }

    public function getItems($items_new,$parent){
        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {
                if(preg_match("/http/", $newitem['path']))
                { 
                    $newelements[] =
                    [   'label' => $newitem['title'],
                        'url' => $newitem['path'],
                        'id' => $newitem['id'],
                        'categoriesId'=>$newitem['categoriesId'],
                        'target'=>'_blank',
                        'parent'=>$newitem['parent'],
                        'htmlAttributes'=>isset($newitem['htmlAttributes'])?$newitem['htmlAttributes']:'',
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];
                }
                else
                {
                   $newelements[] =
                    [   'label' => $newitem['title'],
                        'url' => Yii::$app->urlManager->baseUrl."/".$newitem['path'],
                        'id' => $newitem['id'],
                        'categoriesId'=>$newitem['categoriesId'],
                        'target'=>'_self',
                        'parent'=>$newitem['parent'],
                        'htmlAttributes'=>isset($newitem['htmlAttributes'])?$newitem['htmlAttributes']:'',
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ]; 
                }
            }
        }
        return $newelements; 
    }
        

    private function getMenuRecrusive($data,$child=FALSE){ 
        $html = '';
            foreach($data as $item){
                $temp=0;
                if(isset($item['items'])) { 
                    if ($item['parent']==0) {
                        $html .= '<div class="col-xs-4 ftr-one"><h3>'.$item['label'].'</h3><ul><li>';
                        $html.= $this->getMenuRecrusive($item['items'],TRUE);
                        $html.= '</li></ul></div>';
                        
                    }
                    else { 
                        
                        $html .= '<li ><a href="'.$item['url'].'" target="'.$item['target'].'"  class="'.$item['htmlAttributes'].'"><span>'.$item['label'].'</span></a>';
                        $html.= $this->getMenuRecrusive($item['items'],TRUE);
                        $html.= '</li>';
                    } 
                }                
            }
        return $html;
    }

}   

