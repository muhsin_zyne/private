<?php
namespace frontend\components;
use yii;
use app\assets\AppAsset;
use yii\bootstrap\Nav;
use common\models\Categories;
use common\models\Gallery;
use common\models\StorePages;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Menus;
use common\models\StorePromotions;
use common\models\ConsumerPromotions;
use common\models\Stores;
class MainCategory extends Nav
{
    public function init()
    {
        $store=Stores::findOne(Yii::$app->params['storeId']);      
        $activeCategory=$store->activeCategoryIds;  
        //$categories = Yii::$app->db->createCommand("select id from Categories")->queryAll();        
        //$activeCategory=ArrayHelper::map($categories, 'id', 'id');
        $items = Categories::find()   //orderby parent
            ->orderBy('parent')
            ->asArray()
            ->all();
        $elements = [];
        foreach ($items as $item) {
            if($item['parent']=='0'){ 
                $elements[] =  
                [   'label' => $item['title'],
                    'url' => Yii::$app->urlManager->baseUrl."/".$item['urlKey'].'.html',
                    'id' => $item['id'],
                    'icon'=>$item['icon'],
                    'categoriesId'=>$item['id'],
                    'items' => $this->getItems($items,$item['id']),
                ]; 
            } 
        }
        $result = $this->getMenuRecrusive($elements,$activeCategory,FALSE);// only show  activeCategory  in frontend
        echo $result;
    }

    public function getItems($items_new,$parent){ //Recrusive call
        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {               
                $newelements[] =
                [   'label' => $newitem['title'],
                    'url' => Yii::$app->urlManager->baseUrl."/".$newitem['urlKey'].'.html',
                    'id' => $newitem['id'],
                    'icon'=>$newitem['icon'],
                    'categoriesId'=>$newitem['id'],
                    'items' => $this->getItems($items_new,$newitem['id']),
                ];
            }
        }
        return $newelements; 
    }      

    private function getMenuRecrusive($data,$activeCategory,$child=FALSE){ 
        $html='<ul class="cat-nav">';
            foreach($data as $item){ 
                if(isset($item['items'])) { 
                    if (in_array($item['categoriesId'], $activeCategory)) {
                        $html .= '<li><a href="'.$item['url'].'"> <i class="sm '.$item['icon'].'"></i> <span>'.$item['label'].'</span></a>';
                        $html.= $this->getMenuRecrusive($item['items'],$activeCategory,TRUE); 
                        $html.= '</li>'; 
                    }  
                }  
                else { 
                    $html .= '<li ><a href="'.\yii\helpers\Url::to(['/categories/products', 'categoryId'=>$item['id']]).'">'.$item['label'].'</a>';
                    $html.= '</li>'; 
                } 
            }
        $html.='</ul>';
        return $html;
    }
}   

