<?php
namespace frontend\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\bootstrap\NavBar;
//use frontend\components\Helper;
use yii\helpers\Url;
 
class SuppliersNavBar extends NavBar
{
	public $userName;
	public function init()
    {
        echo '<header class="header">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>'.$this->userName.'<i class="caret"></i></span>
                            </a>
                            
                        </li>
                        <li class="signout">
                            <a href="'.\yii\helpers\Url::to(['/site/logout']).'" class="signout_button">
                                <span>Sign out</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>';
    } 
}