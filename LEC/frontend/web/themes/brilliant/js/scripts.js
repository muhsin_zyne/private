$(document).ready(function(e) {

 $(function() {

  $('.cat-nav li').mouseenter(function() {

   $(this).parents('li').addClass('hover');

   var len = $(this).find('ul').siblings().length;

  if (len > 15) {
   $(this).find('ul').addClass("smallmenu");
  } else {
   $(this).find('ul').removeClass("smallmenu");
  }
  
  
	
  });

  $('.cat-nav li').mouseleave(function() {

   $(this).parents('li').removeClass('hover');

  });

 });
 
 
 
 
 $('select').each(function () {

    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    // Hides the select element
    $this.addClass('s-hidden');

    // Wrap the select element in a div
    $this.wrap('<div class="select"></div>');

    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
    $styledSelect.text($this.children('option').eq(0).text());

    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);

    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
    $styledSelect.click(function (e) {
        e.stopPropagation();
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('ul.options').hide();
        });
        $(this).toggleClass('active').next('ul.options').toggle();
    });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
    $listItems.click(function (e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        /* alert($this.val()); Uncomment this for demonstration! */
    });

    // Hides the unordered list when clicking outside of it
    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});
 
});

$(document).ready(function() {

	/*var scroll, wresize, mobile;
	var headerPos = $('.header-bottom').offset().top;
	var once = true;
	var init = false;
	var show, go;
	
	(scroll = function() {
		
		if(mobile != true && $('.header-bottom').css('position') != 'fixed') {
			var scrollPos = $(document).scrollTop();
			
			if(scrollPos > headerPos) {
				clearTimeout(show);
				init = true;
				if(once === true) {
					once = false;
					$('.header .cover').hide();
					go = setTimeout(function() {
						$('.header .cover').show();
					}, 400);
				}
				
				$('.header-bottom').addClass('attached').css({'top' : (scrollPos-headerPos)+'px'});
				
			} else if(init === true) {
				
				clearTimeout(go);
				
				$('.header-bottom').removeClass('attached').css({'top' : '0px'});
				once = true;
				$('.header .cover').hide();
				show = setTimeout(function() {
					$('.header .cover').show();
				}, 400);
				
				init = false;
			}
		}
		
	})();*/
	
	window.addEventListener('touchstart', function() {
		mobile = true;
	});
	
	(wresize = function() {
		msize = $('.header').width();
		$('.attached').width(msize);
	});
	
	$(document).scroll(scroll);
	$(window).resize(wresize);
	
});

