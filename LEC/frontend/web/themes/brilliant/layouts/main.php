<?php  //test
    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use frontend\assets\AppAsset;
    use frontend\widgets\Alert;
    use frontend\components\FrontNavBar;
    use frontend\components\MainMenu;
    use frontend\components\HeaderMenu;
    use frontend\components\FooterMenu;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use common\models\Stores;
    use common\models\Categories;
    use yii\db\Query;   
    AppAsset::register($this);
        
?>
<?php
    $userId=Yii::$app->user->id;
    $storeId=Yii::$app->params['storeId'];
    $store = Stores::findOne(Yii::$app->params['storeId']);
    $facebookUrl = $store->getfacebookUrl();
    $youtubeUrl = $store->getyoutubeUrl();
    $linkedinUrl = $store->getlinkedinUrl();
    $googleplusUrl = $store->getgoogleplusUrl();
    $twitterUrl = $store->gettwitterUrl();
    $instagramUrl = $store->getinstagramUrl();
    $pinterestUrl = $store->getpinterestUrl();
        
?>
<?php $this->beginPage() ?>
<!doctype html>
<html>
<head>
<meta charset="<?= Yii::$app->charset ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="title" content="<?= Yii::$app->controller->metaTitle?>" />
<meta name="keywords" content="<?= Yii::$app->controller->metaKeywords?>" />
<meta name="description" content="<?= Yii::$app->controller->metaDescription?>" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=1200">
<link rel="icon" href="/themes/brilliant/images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/themes/brilliant/images/favicon.ico" type="image/x-icon" />

      <?= Html::csrfMetaTags() ?>
      <title>
      <?= Yii::$app->controller->metaTitle?>
      </title>
      <?php $this->head() ?>
<?php include "scripts.php" 
//Yii::$app->view->renderPartial('scripts'); ?>

<!--<link href="css/theme.css" rel="stylesheet">-->


<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86738421-7', 'auto');
  ga('send', 'pageview');

</script>


    
</head>
    <body>
<?php $this->beginBody() ?>

<?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>
                <a href="<?=\yii\helpers\Url::to(['byod/logout'])?>" class="logout-fix" title="Logout from BYOD"><i class="fa fa-sign-out"></i><span>Logout <br/>from BYOD</span></a>
        <?php } ?>
    <div class="header">
      <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="col-xs-6 top-social">
               <?php if($facebookUrl!=''){ ?>
              <a href="<?=$facebookUrl?>" target="_blank" class="fa fa-facebook"></a>
              <?php } ?>
              <?php if($youtubeUrl!=''){ ?>
              <a href="<?=$youtubeUrl?>" target="_blank" class="fa fa-youtube"></i></a>
              <?php } ?>
              <?php if($linkedinUrl!=''){ ?>
              <a href="<?=$linkedinUrl?>" target="_blank" class="fa fa-linkedin"></i></a>
              <?php } ?>
              <?php if($googleplusUrl!=''){ ?>
              <a href="<?=$googleplusUrl?>" target="_blank" class="fa fa-google"></i></a>
              <?php } ?>
              <?php if($twitterUrl!=''){ ?>
              <a href="<?=$twitterUrl?>" target="_blank" class="fa fa-twitter"></i></a>
              <?php } ?>
              <?php if($instagramUrl!=''){ ?>
              <a href="<?=$instagramUrl?>" target="_blank" class="fa fa-instagram"></i></a>
              <?php } ?>
              <?php if($pinterestUrl!=''){ ?>
              <a href="<?=$pinterestUrl?>" target="_blank" class="fa fa-pinterest"></i></a>
              <?php } ?>
            </div>
            <div class="col-xs-6 top-menu">

            

            <?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>  
              <a href="<?=\yii\helpers\Url::to(['byod/view'])?>" style="margin-right:20px;"  title="BYOD" class="top-link-byod"> BYOD</i></a>
            <?php } else {  ?>
              <a href="<?=\yii\helpers\Url::to(['byod/apply'])?>" title="BYOD" style="margin-right:20px;" class="top-link-byod bootstrap-modal" data-complete="window.location = '/byod/view';" data-parameters='{"noFlash":"true"}'> BYOD</i></a>
            <?php } ?>

            <?php if (!\Yii::$app->user->isGuest) { ?> 
            <a href="<?=\yii\helpers\Url::to(['site/account'])?>" title="My Account" >My Account</a>
            <a href="<?=Yii::$app->urlManager->createUrl(['site/logout']) ?>" title="Log In" >Log Out</a>
            <a href="<?=\yii\helpers\Url::to(['/site/wishlist'])?>" title="View Cart" class="top-link-cart">Wish List <span>(<?php $n=count(common\models\WishlistItems::find()->where(['storeId'=>$storeId,'userId'=>$userId])->all()); echo $n;?>)</span></a>
            <?php } else { ?>
            <a href="<?=\yii\helpers\Url::to(['site/signup']) ?>" title="Register" ></i>Register</a> 
            <a href="<?=\yii\helpers\Url::to(['site/login'])?>" title="Log In" ></i>Login</a>
            <?php } ?>
            <a href="<?=\yii\helpers\Url::to(['checkout/index'])?>" title="Checkout" class="top-link-checkout">Checkout</i></a>
            <!-- <div class="col-xs-6 top-menu"> <a href="#">My Account</a>
             <a href="#">Checkout</a> 
             <a href="#">Wish List (0)</a> 
             <a href="#">Login</a> </div> -->
          </div>
        </div>
      </div>
     </div>

      <div class="header-middle">
        <div class="container">
          <div class="logo"> <a href="<?=\yii\helpers\Url::to('@web/')?>"><img src="<?=Yii::$app->params["rootUrl"]?>/store/site/logo/brilliant-logo.jpg"  alt="logo"></a> </div>
          <div class="search-outer">
            <div class="search-wrapper">
              <form id="search_mini_form" action="/site/search" method="get">
              <select class="cat-select" name="category">
                <option value="" disabled selected>All Categories</option>
                  <?php  $activeCategory=$store->activeCategoryIds;
                    $parentCategories=Categories::find()->where(['parent'=>0,'id'=>$activeCategory,'disabled'=>0])->all();
                  foreach ($parentCategories as $cat) { ?>
                      <option value="<?=$cat->id ?>" name="c">  <?=$cat->title ?></option>
                  <?php } ?>
              </select>
              <input type="text" value="" id="searchname" name="q" required placeholder="Search here...">
              <input type="submit" class="fa-search" value=""/> </form></div>
          </div>

          <div class="phn-wrapper navbar-brand">
            

              <a class="navbar-brand" href="/"><img src="<?=Yii::$app->params["rootUrl"]?>/store/site/logo/logo.png"></a>
              
        </div>
      </div>
      <div class="header-bottom" data-spy="affix" data-offset-top="170">
        <div class="container">
          <div class="nav-inner">
          <div class="dropdown"> <!--catr-menus-->
          <a data-toggle="dropdown" class="cat-wrapper dropdown-toggle"> CATEGORIES <i class="fa fa-bars"></i></a>
          <div id="main-menu" class="sub-navigation dropdown-menu">
              <?php echo MainMenu::widget([ ]);?>
           </div>
          </div>
            <div class="navigation ">
              <?php echo HeaderMenu::widget([ ]);?>
            </div>
            <div class="cart-container dropdown"> <a class="dropdown-toggle" type="button" data-toggle="dropdown"><?php $n=count(Yii::$app->cart->positions); echo $n.' Item(s) - AU$'.Yii::$app->cart->cost?><span class="fa fa-angle-down "></span> </a>
              <div class="dropdown-menu cart-drop"> <a href="<?=\yii\helpers\Url::to(['cart/view'])?>">View Cart</a> <a href="<?=\yii\helpers\Url::to(['checkout/index'])?>">Checkout</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php if(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='site/index' && Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='categories/products') {?>
        <div class="cart-heading"><!-- breadcrumb -->
          <div class="container">
              <!-- <h4><?= Html::encode($this->title) ?></h4> -->
              <div class="pull-right">                          
                  <?= Breadcrumbs::widget([
                      'tag' => 'ol',
                      //'options' => ['class' => 'breadcrumb'],
                      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                  ]) ?>
              </div>
          </div>
        </div>
      <?php } ?>
      <div class="clearfix"></div>
    <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
     <div class="clearfix"> </div> 
         <?= $content ?>
    <div class="subscribe-wrapper">
      <div class="container">
        <form id="sub-form"  action="/ajax/subscribe" method="post" class="pull-left" onsubmit="return isValidEmailAddress()" >
        <div class="sub-wrapper">
          <span>Subscribe</span>
          <input type="email"  name="email" id="sub-email"   placeholder="Enter your email" class="form-control" required >
          <div class="input-group-btn">
                <button class="btn btn-default" type="submit" value=""><i class="fa fa-angle-right"></i></button>
              </div>
        </div>
        </form>
        <div class="ftr-social">
          <span>stay connected</span>
          <?php if($facebookUrl!=''){ ?>
          <a href="<?=$facebookUrl?>" target="_blank" class="fa  fa-facebook"></i></a>
          <?php } ?>
          <?php if($youtubeUrl!=''){ ?>
          <a href="<?=$youtubeUrl?>" target="_blank" class="fa  fa-youtube"></i></a>
          <?php } ?>
          <?php if($linkedinUrl!=''){ ?>
          <a href="<?=$linkedinUrl?>" target="_blank" class="fa  fa-linkedin"></i></a>
          <?php } ?>
          <?php if($googleplusUrl!=''){ ?>
          <a href="<?=$googleplusUrl?>" target="_blank" class="fa  fa-google"></i></a>
          <?php } ?>
          <?php if($twitterUrl!=''){ ?>
          <a href="<?=$twitterUrl?>" target="_blank" class="fa  fa-twitter"></i></a>
          <?php } ?>
          <?php if($instagramUrl!=''){ ?>
          <a href="<?=$instagramUrl?>" target="_blank" class="fa  fa-instagram"></i></a>
          <?php } ?>
          <?php if($pinterestUrl!=''){ ?>
          <a href="<?=$pinterestUrl?>" target="_blank" class="fa  fa-pinterest-p"></i></a>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="footer">
       <div class="container">
         <div class="footer-top">
            <?=$this->renderBlock('home-page'); // cms block page slug ?>
         </div>
         <div class="footer-menu">
           <div class="row">
             <div class="col-xs-12">
               <div class="ftr-menu-inner">

                  
                 
                   <?php echo FooterMenu::widget([ ]);?>

                   <div class="clientportal-div" style="display:none;">
                    <?php if(isset(Yii::$app->session['clientportal'])) { ?> 
                      <a href="<?=\yii\helpers\Url::to(['client-portal/view'])?>" title="Client Portal" class="top-link-portal"> Client Portal</i></a>
                    <?php } else {  ?>
                      <a href="<?=\yii\helpers\Url::to(['client-portal/apply'])?>" title="Client Portal" class="top-link-portal bootstrap-modal" data-complete="window.location = '/client-portal/view';" data-parameters='{"noFlash":"true"}'> Client Portal</i></a>
                    <?php } ?>
                  </div> 
                 
               </div>
             </div>
           </div>
         </div>
         <div class="footer-card">
           <img src="/themes/brilliant/images/cards.png" alt=""/>
           <span>  Copyright  &copy; <?=$store->title;?> <?= date('Y') ?>. All rights reserved</span>
         </div>
       </div>
    </div>
    
    <script>

/*$(".triger-wrap").click(function(event){  alert('clicked');
	event.preventDefault();
	if($(this).parent().hasClass('active'))
	{
		 $(this).parent().find('.sub-wrap').toggle();
		 $(this).toggleClass('rotate');
		 
	}
	else
	{
	$('.sub-wrap').css('display','none');	
    $(this).parent().find('.sub-wrap').toggle();	
	}
	$(".triger-wrap").parent().removeClass('active');
	$(this).parent().addClass('active');
});*/

/*$(".triger-wrap").click(function(){
    $(this).toggleClass("main");
}); */


    
    $(document).ready(function(){
		$(".dropdown").hover(
		    function() { $('.dropdown-menu', this).stop().fadeIn("fast")},
		   	function() { $('.dropdown-menu', this).stop().fadeOut("fast")}
		);
	});
   </script>
    
    <script>
	
	
	$(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                $(this).toggleClass('open');
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                $(this).toggleClass('open');
            });
    });
	
	
      </script> 
    <script>
      
        $(document).ready(function() {
          $("#owl-demo").owlCarousel({
    
          navigation : true,
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem : true,
          loop:true,
          items:1,
          animateOut: 'fadeOut',
          autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true
    
          });
        });
    
        $('.owl-carousel2').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:6
            }
        }
    
    
    })
  
  
        </script>

    <script type="text/javascript">
        $(document).ready(function () {
            /*$('.category-menu ul').first().addClass('navbar-nav category-class');
            $('.category-class li').addClass('dropdown');
            $('li.dropdown a').addClass('dropdown-toggle');
            $('li.dropdown a').attr('data-toggle','dropdown');
            $('li.dropdown a').append('<i class="fa fa-angle-down"></i>');
            $('li.dropdown ul').addClass('dropdown-menu');
            $('li.dropdown ul').removeClass('nav');
            $('ul.dropdown-menu').attr('id','menu1');
            $('#menu1 li').removeClass('dropdown');*/

           /* $('.category-menu ul').first().addClass('navbar-nav category-class');
            $('.category-class').children().addClass('dropdown cat-menus');
            $('.cat-menus ul').removeClass('nav');
            $('.cat-menus ul').addClass('dropdown-menu');
            $('.cat-menus ul.dropdown-menu').attr('id','menu1');
            $('.cat-menus li').find('ul').addClass('sub-menu');
            $('.cat-menus li').find('ul').removeAttr('id');
            $('.category-class > li > a').addClass('dropdown-toggle');
            $('.category-class > li > a').attr('data-toggle','dropdown');
            $('.category-class > li > a').append('<i class="fa fa-angle-down"></i>');*/
$('.cat-select').each(function () {    
            // Cache the number of options
            var $this = $(this),
            numberOfOptions = $(this).children('option').length;
    
            // Hides the select element
            $this.addClass('s-hidden');
        
            // Wrap the select element in a div
            $this.wrap('<div class="select"></div>');
        
            // Insert a styled div to sit over the top of the hidden select element
            $this.after('<div class="styledSelect"></div>');
        
            // Cache the styled div
            var $styledSelect = $this.next('div.styledSelect');
        
            // Show the first select option in the styled div
            $styledSelect.text($this.children('option').eq(0).text());
        
            // Insert an unordered list after the styled div and also cache the list
            var $list = $('<ul />', {
                'class': 'options'
            }).insertAfter($styledSelect);
    
            // Insert a list item into the unordered list for each select option
            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }
        
            // Cache the list items
            var $listItems = $list.children('li');
        
            // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
            $styledSelect.click(function (e) {
                e.stopPropagation();
                $('div.styledSelect.active').each(function () {
                    $(this).removeClass('active').next('ul.options').hide();
                });
                $(this).toggleClass('active').next('ul.options').toggle();
            });
        
            // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
            // Updates the select element to have the value of the equivalent option
            $listItems.click(function (e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                /* alert($this.val()); Uncomment this for demonstration! */
            });
    
            // Hides the unordered list when clicking outside of it
            $(document).click(function () {
                $styledSelect.removeClass('active');
                $list.hide();
            });
    
        });
    });

    </script>    

    <script type="text/javascript">
        $(document).ready(function() {
          $("#main-menu > ul").addClass("megamenu");
		      $("#main-menu > ul").addClass("dropdown-menu");
            //$( "ul" ).addClass( "cat-nav" );
            $('ul').not(':has(li)').remove();
            setTimeout(function(){ $("ul.pagination").wrap("<div class='col-xs-12 text-center'></div>")}, 1000);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $( "#main-menu > ul.dropdown-menu > li > ul.nav > li" ).each(function() {
              	if($(this).find('ul').length > 0){ 
                  $(this).find('a:first').addClass('triger-wrap');
                  $(this).find('ul:first').addClass('sub-wrap'); 
                }
            });

            $(".triger-wrap").click(function(event){  
                event.preventDefault();
                if($(this).parent().hasClass('active')){
                   $(this).parent().find('.sub-wrap').toggle();
                   $(this).toggleClass('rotate');
                }
                else {
                  $('.sub-wrap').css('display','none'); 
                  $(this).parent().find('.sub-wrap').toggle();  
                }
                $(".triger-wrap").parent().removeClass('active');
                $(this).parent().addClass('active');
            });  
         });
    </script>      
    

    <?php
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody()
?>
</body>
</html>
<?php $this->endPage() ?>
<script>
/*function validateForm() {
    var x = document.forms["sub-form"]["sub-email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Please enter a valid email.");
        return false;
    }
}*/

function isValidEmailAddress() {
    var emailAddress = document.forms["sub-form"]["sub-email"].value;
    if(emailAddress.length != 0){
      var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
      if(pattern.test(emailAddress) == false ){
        alert("Please enter a valid email.");
          return false;
      }
    }
    else{
      alert('Please enter a valid email.');
      return false;
    }  
};
</script>
<script type="text/javascript">

    $(document).ready(function() {

        $('ul').not(':has(li)').remove();

    });

</script>
