
<?php 
    use frontend\components\Helper;
    use yii\helpers\Html;
    use kartik\rating\StarRating;
    use kartik\widgets\ActiveForm;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();
?>



    <div class="product">
        <div class="pdt-img"> 
            <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax=0><img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>" ></a> 
        </div>
        <div class="pdt-cnt">
            <div class="pdt-head"> <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax=0><?=Helper::stripText($model->name,42) ?></a> </div>
            <div class="pr-left">
                <div class="price">
                    <?php if(!$model->isInPromotion()) {
                        if($model->hasSpecialPrice()) {
                    ?>    
                    <span class="strike-price"><?=Helper::money($model->originalPrice)?></span>
                    <?php } } ?>
                    <?php 
                        if($model->typeId == "gift-voucher"){
                            echo "&nbsp";
                        }
                        else{  
                            if (isset(Yii::$app->session['schoolByod']) && $model->isInByod('schoolByod')) { 
                                $gstPercentage = \common\models\Configuration::findSetting('gst_percentage',0);
                                echo Helper::money(Helper::getPriceExclGST($model->price,$gstPercentage)); 
                                echo "<div class='gst_text'>exc GST</div>";
                            }
                            elseif(isset(Yii::$app->session['studentByod']) && $model->isInByod('studentByod')){
                                echo Helper::money($model->price);
                                echo "<div class='gst_text'>incl. GST</div>";
                            }
                            else{
                                echo Helper::money($model->price);
                            }    
                        }
                    ?>
                </div>
                <div class="star-small">
                            <input id="input-id" type="text" class="rating rating-star" value="<?=$model->rating?>">
                </div>
            </div>
            <div class="pr-right">
                <?php $form = ActiveForm::begin(['action' => ['wishlist-items/create'],'options' => ['method' => 'post']]) ?>
                <?=Html::a('<i class="fa fa-eye"></i>', ['products/view', 'id' => $model->id],['class'=>'bootstrap-modal sm-dialog','data-title' =>'',]); ?> 
                    <?php if (!\Yii::$app->getUser()->isGuest) {?>                   
                        <input type="hidden" name="user_id" value="<?=Yii::$app->user->id?>">
                        <input type="hidden" name="store_id" value="<?=Yii::$app->params['storeId']?>">
                        <input type="hidden" name="product_id" value="<?=$model->id?>">
                        <?= Html::submitButton('<i class="fa fa-heart"></i>') ?>  
                <?php } else {?>
                    <a href="/site/login" class="fa fa-heart"></a>
                <?php }?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="pdt-btm"> 
            <!-- <a class="pdt-cart" href="<?php //\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax=0><i class=" fa fa-shopping-cart" ></i> Add to cart</a>  -->
            <form method="POST" id="product_cart_form_<?=$model->id?>" action="<?=\yii\helpers\Url::to(['cart/add'])?>">
              <input type="hidden" id="products-id" name="Products[id]" value="<?=$model->id?>">
              <input type="hidden" name="qty" value="1"/>
            </form>  
            <a class="add-to-cart pdt-cart" id="<?=$model->id?>" href="javascript:;"><i class=" fa fa-shopping-cart" ></i>Add to Cart</a>
        </div>
        
    </div>
