<?php 
use common\models\Storebanners;
use common\models\Configuration;
use common\models\Banners;
use yii\widgets\ListView;
use yii\db\Query;
?>
<div class="container">
    <div class="banner-wrapper">
        <div class="row">
            <!--mini banner-->
            <div class="col-xs-3 min-banner-side">
                <?php foreach ($mini_banner as $index => $banner) { 
                    $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one();?>
                    <?php if($banner_images['url']!='') { 
                        if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url']; }                                                 
                            else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                            else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                        ?>
                        <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">
                            <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                        </a>
                    <?php } else { ?>
                        <a><img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' ></a>
                    <?php } ?> 
                <?php } ?>
            </div>
            <!--mini banner end-->
            <!-- main Banner -->
            <div class="col-xs-9">
                <div id="owl" class="owl-carousel owl-theme">
                    <?php foreach ($main_banner as $index => $banner){ 
                        $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one();?>
                        <div class="item">
                            <?php if($banner_images['html']!=''){ //var_dump($banner_images->youtubeUrl);die;?>
                                <a class="modalButton" data-toggle="modal" data-src="<?=$banner_images->youtubeUrl?>?rel=0&wmode=transparent&fs=0" data-target="#myModal"> 
                                	<img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" >
                                </a>
                            <?php 
                            } 
                            elseif ($banner_images['url']!='') {   
                                if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url']; }                                                 
                                else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                                else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                                ?> 
                                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                                            
                                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>'  >
                                    </a>
                                <?php //echo "</div>"; 
                            } 
                            else{ ?>
                                 <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" > 
                            <?php }  ?>
                           
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!---       youtube popup         -->
            <!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
		        <div class="modal-dialog">
		            <div class="modal-content">
                        <button type="button" class="close model-close-sm" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <div class="modal-body frame-sm">
		                      <iframe frameborder="0"></iframe>
		                </div>         
		            </div> /.modal-content -->
		        <!--</div> /.modal-dialog -->
		    <!--</div> /.modal -->
            <!-- main banner end-->
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class=" cat-container">
                    <div class="cat-head">
                        Best Sellers <i></i>
                    </div>
                    <div class="cat-cnt">
                        <?php                         
                            $bestSellers->pagination = [
                                'defaultPageSize' => 3,
                            ];
                            echo ListView::widget([
                            'id' => 'homepage-products',
                            'summary'=>'', 
                            'dataProvider' => $bestSellers,
                            'itemView' => '/products/_bestsellers',
                            'summary'=>'',
                            'layout' => "{items}",                                                    
                            'itemOptions' => ['class' => 'items_featured bst-row']
                        ]);

                        echo "<script> $(document).on('pjax:complete', function() { $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});  })</script>";
                        ?> 

                    </div>
                </div>

                <div class="fb-container">

                    <?php $store_id=Yii::$app->params['storeId'];
                        $conf=Configuration::findSetting('is_sidebanner_facebook', $store_id);
                        if($conf=='1')
                        {
                            $banner_facebook = Banners::find()->where(['storeId' => $store_id,'isFacebook' => 1])->one();?>
                            <div class="fb-page" data-href="<?=$banner_facebook->path?>" data-tabs="timeline" data-width="325" data-height="393" data-small-        header="    false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <div class="fb-xfbml-parse-ignore"><blockquote cite="<?=$banner_facebook->path?>"><a href="<?=$banner_facebook->path?>"></a></blockquote></div>
                            </div>
                    <?php } 
                    else
                    { ?>
                        <div class="fb-page">
                            <?php foreach ($side_banner as $index => $banner){
                                $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one(); ?>
                                <div class="small_banner"> 
                                    <?php if($banner_images['url']!='') { 
                                        if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url'];}                                                  
                                            else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                                            else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                                        ?>
                                        <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                            
                                            <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                                        </a>
                                    <?php } else { ?>
                                    <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                                <?php } ?>  
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-9">
                <div class="pdt-tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="tab-head">FEATURED PRODUCTS</li>                        
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">                        
                        <div role="tabpanel" class="tab-pane active"  activeid="featuredproducts">
                            <div class="products-container">
                                <div class="row">
                                    <?php   
                                        $featuredProducts->pagination = [
                                            'defaultPageSize' => 6,
                                        ];
                                        echo ListView::widget([
                                            'id' => 'homepage-products',
                                            'summary'=>'', 
                                            'dataProvider' => $featuredProducts,
                                            'itemView' => '/products/_productlist',
                                            'summary'=>'',
                                            'layout' => "{items}", 
                                            'itemOptions' => ['class' => 'items_featured col-xs-3']
                                        ]);
                                         echo "<script> $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});</script>";
                                    ?> 
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>

    <div class="advt"> <!--    strip banner section          -->
        <?php foreach ($strip_banner as $index => $banner){
            $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one(); ?>           
                <?php if($banner_images['url']!='') { 
                    if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url'];}                                                  
                        else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                        else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                    ?>
                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                            
                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                    </a>
                <?php } else { ?>
                <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
            <?php } ?>
        <?php } ?>
    </div> 
</div> 
<div class="brands">
	<div class="container">
	   <div class="row">
			<div class="col-xs-12">
				<?php 
                    echo ListView::widget([
                    //'id' => 'homepage-products',
                    'summary'=>'', 
                    'options' => [
                        'tag' => 'div',
                        'class' => 'owl-carousel',
                        'id' => 'footer_slider',
                    ],
                    'dataProvider' => $brands,
                    'itemView' => '/products/_brandlist',
                    'summary'=>'',
                    'layout' => "{items}",                                                    
                    'itemOptions' => ['class' => 'item']
                ]);?>
			</div>
		</div>
	</div>
</div> 
<div id="fb-root"></div>        
<script type="text/javascript">
    $('a.modalButton').on('click', function(e) {   //youtube popup
      var src = $(this).attr('data-src');      
      var height = $(this).attr('data-height') || 320;
      var width = $(this).attr('data-width') || 450;      
      $("#myModal iframe").attr({'src':src,'height': height,'width': width}); 
  });

</script>
<script>(function(d, s, id) {
        
          var js, fjs = d.getElementsByTagName(s)[0];
        
          if (d.getElementById(id)) return;
        
          js = d.createElement(s); js.id = id;
        
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
        
          fjs.parentNode.insertBefore(js, fjs);
        
        }(document, 'script', 'facebook-jssdk'));
</script>
