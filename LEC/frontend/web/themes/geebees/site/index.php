<?php 
use common\models\Storebanners;
use common\models\Configuration;
use common\models\Banners;
use common\models\Products;
use yii\widgets\ListView;
use yii\db\Query;
use yii\data\ActiveDataProvider;
?>
<div class="clearfix"></div>
<div class="banner">
    <div class="row">
        <div class="col-xs-12">
            <div class="banner-wrapper">
                <div id="owl" class="owl-carousel owl-theme">
                      <?php foreach ($main_banner as $index => $banner){ 
                        $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one();?>
                        <div class="item">
                            <?php if($banner_images['html']!=''){ //var_dump($banner_images->youtubeUrl);die;?>
                                <a class="modalButton" data-toggle="modal" data-src="<?=$banner_images->youtubeUrl?>?rel=0&wmode=transparent&fs=0" data-target="#myModal"> 
                                    <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" >
                                </a>
                            <?php 
                            } 
                            elseif ($banner_images['url']!='') {   
                                if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url']; }                                                 
                                else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                                else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                                ?> 
                                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                                            
                                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>'  >
                                    </a>
                                <?php //echo "</div>"; 
                            } 
                            else{ ?>
                                 <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" > 
                            <?php }  ?>
                           
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="options-banner">
    <div class="container">
        <div class="row">
        <div class="clearfix"></div>
            <div class="col-xs-12">
                <?=$this->renderBlock('home-page'); // cms block page slug ?>
            </div>
        </div>
    </div>
</div>
<div class="products">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>featured products</h1> 
                   

                 <?php                    
                    $featuredProducts->pagination = [
                        'defaultPageSize' => 25,
                    ];
                    echo ListView::widget([
                    //'id' => 'homepage-products',
                    'summary'=>'', 
                    'options' => [
                        'tag' => 'div',
                        'class' => 'owl-carousel owl-theme',
                        'id' => 'featuredslider',
                    ],
                    'dataProvider' => $featuredProducts,
                    'itemView' => '/products/_productlist',
                    'summary'=>'',
                    'layout' => "{items}",                                                    
                    'itemOptions' => ['class' => 'item']
                ]);
                 echo "<script> $(document).on('pjax:complete', function() { $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});  })</script>";   
                ?> 
                    
                
            </div>
        </div>
    </div>
</div>
<div class="newsletter">
  <div class="container">
      <div class="row">
          <div class="col-xs-12">
                <h2>JOIN OUR COMMUNITY</h2>
                <p class="line">_______</p>
                <p>To receive updates on new arrivals, specials, offers and other discounts</p>
                <div class="news_register">
                    <form id="sub-form"  action="/ajax/subscribe" method="post" onsubmit="return isValidEmailAddress()">
                         <input type="email"  name="email" id="sub-email"   placeholder="Enter your email" autocomplete="off" class="form-control" required >
                        <input type="submit" class="subscribe" value="SUBSCRIBE" />

                       
                    </form> 
                <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product_list">
  <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Best Sellers</h1>
            </div>
             
                <?php                         
                    $bestSellers->pagination = [
                        'defaultPageSize' => 10,
                    ];
                    echo ListView::widget([
                    'id' => 'homepage-products',
                    'summary'=>'', 
                    'dataProvider' => $bestSellers,
                    'itemView' => '/products/_productlist',
                    'summary'=>'',
                    'layout' => "{items}",                                                    
                    'itemOptions' => ['class' => 'col-xs-3']
                ]);
                    echo "<script> $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});</script>";
                ?> 
                <div class="col-xs-12">
                    <div class="ad-full"> <!--    strip banner section          -->                  
                        <?php foreach ($strip_banner as $index => $banner){
                            $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one(); ?>           
                                <?php if($banner_images['url']!='') { 
                                    if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url'];}                                                  
                                        else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                                        else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                                    ?>
                                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                            
                                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                                    </a>
                                <?php } else { ?>
                                <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                            <?php } ?>
                        <?php } ?>
                    
                    </div>
                </div>
        </div>
    </div>
   
</div>

<div class="brands">
  <div class="container">
      <div class="row">
          <div class="col-xs-12">
              <h2>Brands</h2>
              <p class="line">_______</p>
                <p>Choose best with our favourite brands</p>
                 <?php 
                    echo ListView::widget([
                    //'id' => 'homepage-products',
                    'summary'=>'', 
                    'options' => [
                        'tag' => 'div',
                        'class' => 'owl-carousel',
                        'id' => 'footer_slider',
                    ],
                    'dataProvider' => $brands,
                    'itemView' => '/products/_brandlist',
                    'summary'=>'',
                    'layout' => "{items}",                                                    
                    'itemOptions' => ['class' => 'item']
                ]);?>
               
            </div>
        </div>
    </div>
</div>
<script>
/*function validateForm() {
    var x = document.forms["sub-form"]["sub-email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Please enter a valid email.");
        return false;
    }
}*/
function isValidEmailAddress() {
    var emailAddress = document.forms["sub-form"]["sub-email"].value;
    if(emailAddress.length != 0){
      var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
      if(pattern.test(emailAddress) == false ){
        alert("Please enter a valid email.");
          return false;
      }
    }
    else{
      alert('Please enter a valid email.');
      return false;
    }  
};
</script>