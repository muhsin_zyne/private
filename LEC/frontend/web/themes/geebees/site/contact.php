<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */
$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="inner-container">
        <div class="site-contact">  
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="cntct-head">Get in touch</h1>
                    <p> If you have business enquiries or other questions, please fill out the following form to contact us. Thank you. </p>
                </div>
                <div class="col-xs-9">
                    <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'name') ?>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'email') ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'subject') ?>
                            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                'template' => '<div class="row"><div class="col-xs-3">{image}<a href="#" id="fox"><i class="fa fa-refresh"></i></a></div><div class="col-xs-9">{input}</div></div>',]) ?>                        
                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                            </div>
                        </div> 
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <div class="col-xs-3">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="trading_hours">
                            <span><i class="fa fa-clock-o"></i>TRADING HOURS:</span>
                            MON     8.30am  &ndash; 5.30pm<br>
                            TUE     8.30am  &ndash; 5.30pm<br>
                            WED     8.30am  &ndash; 5.30pm<br>
                            THU     8.30am  &ndash; 5.30pm<br>
                            FRI     8.30am  &ndash; 5.30pm<br>
                            SAT     9.00am &ndash;  12.00pm<br>
                            SUN     Closed </div>
                        </div> 
                        <div class="col-xs-12">
                            <div class="contact-bottom ">
                                <div class="mapaddress">
                                    <span><i class="fa fa-map-marker"></i> Address:</span>
                                    188 Manifold Street,<br>
                                    Camperdown,<br>
                                    VIC 3260,<br>
                                    Australia.<br>
                                    P: 03 5593 2489<br>
                                </div>
                            </div>  
                        </div>         
                    </div>
                </div> 
                <div class="col-xs-12 cnct-form">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3134.0453029115897!2d143.14476741497074!3d-38.23205607967877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad2ee0c7aaae74d%3A0xe8f6750a026dac91!2sGee+Bees+Media+-+Leading+Edge+Computers+-+Telstra+Dealer!5e0!3m2!1sen!2sin!4v1481801200102" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("#fox").click(function(event){ // captcha refresh button 
    event.preventDefault();
    $("img[id$='-verifycode-image']").click();
})
</script>