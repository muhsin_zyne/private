<?php 
    use frontend\components\Helper;
    use yii\helpers\Url;
?>

<?php $conferenceId = $_REQUEST['id']; ?>

<li class="traylist" id="item_6842">
    <div>
        <div class="floatinfos"> 
            <div class="product-name">
                <div class="tray-name">
                <a title="<?=$model->title?>" href="<?=\yii\helpers\Url::to(['conferences/trayproducts','id'=>$conferenceId ,'trayId'=>$model->id])?>"><?=$model->trayId?>-<?=$model->title?></a>
                </div>
            </div>
        </div>    
    </div>
</li>    