<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use frontend\components\Helper;

?>

<?php
$user = User::findOne(Yii::$app->user->Id);

?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="conferencetrays">
			<h2><?=$conference->title?></h2>
			<div class="date_offer">
				<div class="offer">
					<b><span>Start Date:</span></b> <span><?=Helper::date($conference->fromDate);?></span> 
				</div>
				<div class="offer">
					<b><span>End Date:</span></b> <span><?=Helper::date($conference->toDate);?></span> 
				</div>
				<div class="clear"></div>
			</div>
			<p class="filter-tray">Search <input type="search" placeholder="Search by Tray ID or Tray name or SKU" name=""></p>

			<?= GridView::widget([
    			'dataProvider' => $conferencetrays,
    			'class'=>'data-table',
        		'columns' => [
        				[
        				'attribute'=>'trayId',
        				'label'=> 'Tray Id'
        				],
        				[
        				'attribute'=>'title',
        				'label'=>'Tray Name'
        				],
        		['class' => 'yii\grid\ActionColumn','template' => '{view}']]
    		]); ?>

		</div>
	</div>	
</div>





