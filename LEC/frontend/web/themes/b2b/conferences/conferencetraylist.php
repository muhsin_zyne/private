<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
?>

<?php //var_dump($conferencetrays);die(); ?>

<div class="mainarea">
    <div class="wrapper_inner">
        <div class="col-main Product_Featured">
            <h2 class="conference_head">Conference Trays</h2>
                <div class="area_filter tray_drop">
                    <!-- <form name="filter" id="filter_form" method="post" action=""> -->
                    <div id="type_view"></div>
                    <!-- <div class="flit_search">
                    <input type="text" name="query" value="" placeholder="Search by SKU, EzCode, Title" />
                    <input type="submit" name="fil_search" value="Search" class="button" />
                    </div> -->
                    <!-- <div class="filt_select">
                    <select name="filter_value" onchange="#">
                    <option value="">Sort by</option>
                    <option  value="price_low">Price (Low to High)</option>
                    <option  value="price_high">Price (High to Low)</option> 
                    <!--<option  value="name">Name</option>-->
                   <!--<option  value="sku">SKU</option>
                    <option  value="ezcode">EzCode</option>
                    </select>
                    </div> -->

                    <div class="filt_select">
                        <?php  $form = ActiveForm::begin(['id' => 'products-form','method' => 'GET', 'action' => \yii\helpers\Url::to(['conferences/trayproducts','id'=>$conference->id])]); ?>
                            <select class="trayIds" name="trayId" onchange='this.form.submit()'>
                                <option value="0">Select Tray</option>
                                    <?php
                                        foreach ($conferencetrays as $conferencetray) {
                                    ?>
                                        <option value="<?=$conferencetray->id?>" <?php echo (isset($tray)&&$tray==$conferencetray->id)  ? "selected" : ""?>><?=$conferencetray->trayId." ".$conferencetray->title?></option>
                                    <?php
                                        }
                                    ?>
                            </select>
                        <?php ActiveForm::end(); ?>
                    </div>

                    <div class="clear"></div>
                    <!-- </form> -->
                </div>
               <!--  <div class="view">
                    <span>View</span>
                    <a class="grid " href="javascript:void(0);" onclick="formsubmit('grid')">Grid</a>
                    <a class="list active" href="javascript:void(0);" onclick="formsubmit('list')">List</a>
                    <div class="clear"></div>
                </div> -->
                <div class="clear"></div>
                <form id="list_form_pro" name="pro_order" action="" method="post" onsubmit="return chk_validaion()">
                    <input type="hidden" name="form_key" value="ItAThARoVgdWrAGq" />
                    <div class="product_list">
                        <div id="checkall">
                            <a href="javascript:void(0);" onclick="selectvisible()">Select visible</a>
                            <a href="javascript:void(0);" onclick="unselectvisible()">Unselect visible</a>
                        </div>

                        <ol>
                    		<?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                    		<?php 
		                        echo ListView::widget( [
		                        'id' => 'testclass',
		                        'dataProvider' => $trays,
		                        'itemView' => '/conferences/_tray',
		                        'summary' => '',
		                        'itemOptions' => ['class' => 'items col-md-4']
		                        ] );
                    		?>
                    		<?php  \yii\widgets\Pjax::end();  ?> 
                		</ol>

               
                <div id="noresults"></div>
                <div class="animation_image" style="display:none" align="center">
                    <img src="http://www.orders.legj.com.au/skin/frontend/default/b2b/images/ajax-loader.gif"/>
                </div>
                </div>
                <!-- <div id="floatdiv">
                    <input type="submit" class="button list_bttn" name="order" value="Order" />
                    <div class="clear"></div>
                </div> -->
                </form>


<!-- GRID LOAD HERE -->

</div>
<div class="clear"></div>
</div><!-- end .wrapper-->
</div><!--mainarea-->

