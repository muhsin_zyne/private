<?php

use frontend\components\Helper;
use yii\helpers\Url;
?>

<?php 
   $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$model->getThumbnailImage(); 
    
    //var_dump($thumbImagePath); die();
?>

<li class="" id="item_6842">
    <div>
        <div class="floatchk">

        <?php //var_dump($model->product->id); die(); ?>

             <input type="checkbox" name="Products[<?=$model->id?>][id]" value="<?=$model->id?>" class="chkbx ajax-update chkbx-<?=$model->id?>" href="<?=Url::to(['products/config','pid' => $model->id])?>" data-before-submit="setConfig(<?=$model->id?>);" data-title="Please select configuration" data-configurable="<?=($model->typeId=="configurable" || $model->typeId=="bundle")? 'true' : 'false'?>">
        </div>
        <div class="floatimg">
            <input type="hidden" value="<?=$model->id?>" name="product_id[<?=$model->id?>]">
            <a href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>" <?php echo ($model->isInConference()) ? 'class="ribbon"' : '' ?>><img alt="<?=$model->name?>" src="<?=$thumbImagePath?>"></a>
            <div class="clear"></div>        </div> 
        <div class="floatinfo"> 
            <div class="product-name">
                <a title="<?=$model->name?>" href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>"><?=$model->name ?></a>
            </div>
            <div class="desclist"><?=$model->name?></div>
            <div class="sku"><b>SKU: </b><span><?=$model->sku?></span></div>
            <?php if($model->ezcode!='' ){ ?>
            <div class="sku"><b>EZ Code: </b><span><?=$model->ezcode?></span></div>
            <?php } ?>
            <div class="sku"><b>Markup: </b><span><?=$model->getMarkup()?> %</span></div>
            <div class="sku"><b>Retail price: </b><span><?=Helper::money($model->sellingPrice)?></span></div><br/>
            <?php if(array_key_exists($model->id, $promotionProductsArray)) {    ?>
                <div class="sku promotion-list"><b>Promotions: </b>
                    <?php
                        $count = 0;
                        foreach (array_reverse($promotionProductsArray[$model->id]) as $promotion) {
                            if($promotion){
                                if($count<4){
                    ?>
                        <span><?=$promotion->shortTag?></span>
                    <?php  } }  $count = $count+1;  } ?>
                </div>
            <?php } ?> 
        </div>
       
        <div class="floatright">
             <div class="addto">
                <span class="qty">Qty</span>
                <input type="text" class="vsamll" value="" name="Products[<?=$model->id?>][quantity]" onkeyup="chknumber(this)"> 
                <div class="clear"></div>
            </div>
            <div class="floatleft price_disp">
                <div class="pricebox"><span class="price"><?=Helper::money($model->price)?></span></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</li>
