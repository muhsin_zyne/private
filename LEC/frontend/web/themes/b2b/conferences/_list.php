<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div>
        <div class="col-main Product_Featured">
            <h2 class=""></h2>
                <div class="area_filter">
                    
                    <div id="type_view"></div>
                    <div class="flit_search">
                    <input type="text" class="search" name="query" value="<?=isset($_REQUEST['keyword'])? $_REQUEST['keyword'] : ''?>" placeholder="Search by SKU, EzCode, Title" />
                    <input type="button" name="fil_search" value="Search" class="button" onclick="validsearch()"/>
                    </div>

                    <div class="filt_select">
                       <?php  $form = ActiveForm::begin(['id' => 'products-form','method' => 'GET', 'action' => \yii\helpers\Url::to(['conferences/trayproducts','id'=>$conference->id])]); ?>
                    <select class="trayIds" name="trayId" onchange='this.form.submit()'>
                        <option value="0">Select Tray</option>
                        <?php
                            foreach ($conferencetrays as $conferencetray) {
                        ?>
                            <option value="<?=$conferencetray->id?>" <?php echo (isset($tray)&&$tray==$conferencetray->id)  ? "selected" : ""?>><?=$conferencetray->trayId." ".$conferencetray->title?></option>
                        <?php
                            }
                        ?>
                    </select>
                     <?php ActiveForm::end(); ?>
                </div>

                <?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => \yii\helpers\Url::to(['cart/add'])]); ?>
                    <input type="hidden" class="productsids" name="Products[id]"/>

                    <div class="filt_select">
                    <select name="filter_value" onchange="#">
                    <option value="">Sort by</option>
                    <option  value="price_low">Price (Low to High)</option>
                    <option  value="price_high">Price (High to Low)</option> 
                    <!--<option  value="name">Name</option>-->
                   <option  value="sku">SKU</option>
                    <option  value="ezcode">EzCode</option>
                    </select>
                    </div>
                    <div class="clear"></div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="view">
                    <span>View</span>
                    <a class="grid " href="javascript:void(0);" onclick="formsubmit('grid')">Grid</a>
                    <a class="list active" href="javascript:void(0);" onclick="formsubmit('list')">List</a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <?php $form = ActiveForm::begin(['method' => 'POST', 'action' => Url::to(['cart/add']), 'id' => 'list_form_pro']); ?>
                    <input type="hidden" name="form_key" value="ItAThARoVgdWrAGq" />
                    <?php if(Yii::$app->request->isAjax){ ?>
                    <div class="product_list">
                        <div id="checkall">
                            <a href="javascript:void(0);" onclick="selectvisible()">Select visible</a>
                            <a href="javascript:void(0);" onclick="unselectvisible()">Unselect visible</a>
                        </div>

                        <ol>
                    		<?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                    		<?php 
		                        echo ListView::widget( [
		                        'id' => 'testclass',
		                        'dataProvider' => $products,
		                        'itemView' => '/conferences/_product',
		                        'summary' => '',
		                        'itemOptions' => ['class' => 'items col-md-4']
		                        ] );
                    		?>
                    		<?php  \yii\widgets\Pjax::end();  ?> 
                		</ol>

               
                <div id="noresults"></div>
                <div class="animation_image" style="display:none" align="center">
                    <img src="http://www.orders.legj.com.au/skin/frontend/default/b2b/images/ajax-loader.gif"/>
                </div>
                </div>
                <div id="floatdiv">
                    <input type="submit" class="button list_bttn order_now" name="order" value="Order" />
                    <div class="clear"></div>
                </div>
                <?php }else{ ?>
                    <div class="product_list">
                        <div style="text-align: center;">
                            <img src="/themes/b2b/images/loading.gif"/>
                        </div>
                    </div>
                <?php } ?>

                 <?php ActiveForm::end(); ?>


<!-- GRID LOAD HERE -->

</div>
</div>