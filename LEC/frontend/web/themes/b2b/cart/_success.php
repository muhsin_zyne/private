<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\components\Helper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = "Order Placed Successfully";
?>
<div class="mainarea">
	<div class="wrapper_inner">
		<div class="col-main">
			<div class="shopping_cart success_checkout">
				<h1><?= Html::encode($this->title) ?></h1>
    			<p>Thank you for your purchase!</p>
        		<p>Your order # is: <?=$orderId?></p>
        		<p>You will receive an order confirmation email with details of your order.</p>
        		<div class="continue_button">
            		<a class="continue-shopping" href="javascript:;">
                		<button class="cont"><span>Continue Shopping</span></button>
            		</a>    
        		</div>
			</div>
		</div>
	</div>
</div>			
<script type="text/javascript">
	$(document).ready(function(){
		$('a.continue-shopping').click(function(){
			location.href = "<?=Url::to(['default/index'])?>";
		});
	});
</script>
