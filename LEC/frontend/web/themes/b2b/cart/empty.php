<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\components\Helper;
use yii\helpers\Html;
?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="col-main">
			<div class="shopping_cart">
				<h1><h1>Shopping Cart is Empty</h1></h1>
				<div class="cart-empty">
		        	<p>You have no items in your shopping cart.</p>
		    		<p>Click <a href="<?=\yii\helpers\Url::to(['default/index'])?>">here</a> to continue shopping.</p>
				</div>
			</div>
		</div>
	</div>
</div>			