<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\components\Helper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$this->title = "Shopping Cart";
$positions = $cart->positions;
?>
<div class="mainarea">
<div class="wrapper_inner">
<div class="col-main">
<div class="shopping_cart">
<form method="POST" class="checkout-form" action="<?=Url::to(['cart/update'])?>">
<h2 style="float: left;">Shopping Cart</h2>
<a class="button clearcart" href="<?=Url::to(['default/index'])?>" title="Add More Products">Add More Products</a>
<a class="button clearcart" href="<?=Url::to(['cart/empty'])?>" onclick="return confirm('Are you sure you want to clear the cart?')" title="Clear cart">Clear Cart</a>
<div class="clear"></div>

<?php foreach($addresses as $address){ ?>
        <?php if(!is_null($address)){ ?>
        <div class="multi_cart_head">
          
          <div class="multi_cart_head_left">
          <h2>Shipping Address</h2>
             <p><?=$address->shippingAddress?></p>
          </div>
          <div class="multi_cart_head_right"><?=$address->storeName?></div>
          <div class="clear"></div>
        </div>
     <?php } ?>
     
<table>
    <thead>
    <tr>
        <th>Image</th>
        <th style="text-align: left;">Product Name</th>
        <th>SKU</th>
        <th>Store</th>
        <th width="100px">Supplier</th>
        <th width="80px">Unit Price</th>
        <th>Qty</th>
        <th>Subtotal</th>
        <th>Comments</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        <?php 
        $cost = 0;
        foreach($positions as $position){
            $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$position->product->getThumbnailImage();
            if($position->b2bAddress->id == $address->id){ //var_dump($position->product->brand->supplier->title); die;
        ?>
            <tr>
                <td>  
                    <a <?php echo ($position->product->isInConference()) ? 'class="ribbon cart_img"' : 'class="cart_img"' ?>>
                        <img src="<?=$thumbImagePath?>" width="150" height="150">
                    </a>    
                </td>
                
                <td style="text-align: left;"><?=$position->product->name?>
                    <dl class="item-options">
                        <?=$position->superAttributeValuesText?>
                    </dl>
                </td>
                <td><?=$position->sku?></td>
                <td><?=Html::dropDownlist("Positions[".$position->getId()."][b2bAddressId]", $position->b2bAddress->id, ArrayHelper::map(Yii::$app->user->identity->b2bAddresses, 'id', 'storeName'), ['class' => 'store-dropdown'])?></td>
                <!-- <td><?php /*Html::dropDownlist("Positions[".$position->getId()."][supplierUid]", "", ArrayHelper::map($position->product->brand->supplier, 'id', 'title'));*/?></td> -->
                <td>
                    <?=isset($position->product->brand->supplier)? $position->product->brand->supplier->title : ""?>
                    <input type="hidden" value="<?=isset($position->product->brand->supplier)? $position->product->brand->supplier->title : ""?>" name="Positions[<?=$position->getId()?>][supplierUid]">
                </td>
                <td><?=Helper::money($position->price)?></td>
                <td id="qty_<?=$position->getId()?>"><?=Html::textInput("Positions[".$position->getId()."][qty]", $position->quantity, ['class' => 'item-quantity only-numeric'])?></td>

                <td id="subtotal_<?=$position->getId()?>"><?=Helper::money($position->cost)?></td>
                
                <td id="comments_25199">
                    <textarea name="Positions[<?=$position->getId()?>][comments]" id="comment_<?=$position->getId()?>"><?=$position->comments?></textarea>
                </td>
                <td class="action" id="edit_25199">
                        <!-- <a href="javascript:edit(25199,'1');" id="edit_a_25199" title="Edit"> <img src="http://www.orders.legj.com.au/skin/frontend/default/b2b/images/editor.png"> </a> -->
                        <a class="fa fa-trash" style="font-size:18px; color: #4f4f4f;" href="<?=Url::to(['cart/delete','positionId'=>$position->getId()])?> " onclick="return confirm('Are you sure you want to remove this product from cart?')" title="Delete"></a>
                </td>
            </tr>
        <?php
                $cost = $cost + $position->cost;
            }
        }
        ?>
        </tbody>
</table>
     
<div class="store_total">
<?=$address->storeName?> Total: <?=Helper::money($cost);?></div>
<?php } ?>

<div class="short_comment">
<label for="shortarea">Your order no/comments:</label>
<textarea name="cart[comments]" onchange="saveshortcomment(this)" id="shortarea"></textarea>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label>Order Type:</label>
<select name="cart[order_type]" class="order_type_drop"> 
    <option value="" selected="selected">-Select-</option>
    <option value="Replenishment">Replenishment</option>
    <option value="Special Order">Special Order</option>   
    <option value="Promotional">Promotional</option>  
    <option value="Indent">Indent</option>   
</select>

<div class="clear"></div>
</div>
<div class="total">
    <span id="total">Grand Total:   <?=Helper::money(Yii::$app->cart->cost)?></span>
</div>
<div class="clear"></div>
<div class="continue_shopping"><a href="<?=Url::to(['default/index'])?>">Continue shopping</a></div>
<div class="proceed_order">
<div style="display:none"><input id="alternateSubmit" type="submit" /></div>
<input class="update-cart" value="Update Shopping Cart" type="button"/>
<a class="save-cart" href="javascript:;">Save this cart</a>
<!-- <a class="checkout" href="<?php /*Url::to(['cart/checkout'])*/?>">Proceed to Checkout</a> -->
<input type="button" class="checkout" value="Proceed to Checkout" name="submit">
</div>
<div class="clear"></div>
<div class="popupsave" style="display: none; position: fixed; top: 135.5px; left: 523.5px;">
<div class="pophed">
<label>Save Cart</label>
<a class="close" onclick="closepop();" href="javascript:void(0);">close</a>
<div class="clear"></div>
</div>
</div>
<input type="hidden" name="Cart[id]" value="<?=$savedCart->id?>"/>
</form>
</div>
</div>
</div><!-- end .wrapper-->
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.store-dropdown').change(function(){
        $('.update-cart').click();
    })
    $('.checkout').click(function(){
        $('.item-quantity').each(function(){
            var intRegex = /^[1-9]+$/;
            if($(this).val() == "" || $(this).val() == 0){
                alert('Sorry, could not proceed to checkout as your order contains items with zero quantity. Please amend quantity or delete zero lines and try again.');
                throw new Error('Not an error');
            }
        })

        if($('.order_type_drop').val() == ""){
            alert('Please select the Order Type');
            throw new Error('Not an error');
        }

        $('.checkout-form').attr("action", "<?=Url::to(['cart/update', 'checkout' => 'true'])?>");
        $('#alternateSubmit').click();
    })

    $('.update-cart').click(function(){
        $('.item-quantity').each(function(){
            var intRegex = /^[1-9]+$/;
            if($(this).val() == "" || $(this).val() == 0){
                alert('Quantity cannot be zero!');
                throw new Error('Not an error');
            }
        })
    })    

    $('.update-cart').click(function(){
        $('.checkout-form').attr("action", "<?=Url::to(['cart/update'])?>");
        $('#alternateSubmit').click();
    })
    $('.save-cart').click(function(){
        if($('input[name="Cart[id]"]').attr("value")!=""){ "";
            $('.checkout-form').append('<input type="hidden" name="Cart[title]" value="'+title+'"/>');
            $('.checkout-form').attr("action", "<?=Url::to(['cart/save'])?>");
            $('#alternateSubmit').click();
        }else{
            <?php if(!Yii::$app->session->has('loadedCart')){ ?>
            var title = prompt('Please enter a title for this cart');
            <?php }else{ ?>
                title = "already-saved";
                <?php } ?>
            if(title){
                $('.checkout-form').append('<input type="hidden" name="Cart[title]" value="'+title+'"/>');
                $('.checkout-form').attr("action", "<?=Url::to(['cart/save'])?>");
                $('#alternateSubmit').click();
            }
        }
    })
})
</script>
