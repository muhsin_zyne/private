<?php

use frontend\components\Helper;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

?>

<script src="js/jquery-1.9.1.min.js"></script>

<div class="mainarea">
    <div class="wrapper">
        <div class="col-main">
            <div class="product_detail">
            	<?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => \yii\helpers\Url::to(['cart/add'])]); ?>
            	<h1><?=$product->name ?></h1>
                <input type="hidden" name="Products[id]" value="<?=$product->id?>"/>
                    <div class="product-img-box">
                        <div class="large">
                            <a data-toggle="modal" data-target="#myModal" class="group <?php echo ($product->isInConference()) ? 'ribbon' : '' ?>" rel="group" title="18ct Half Carat Diamond Engagement Ring">
                                <img  src="<?=$smallImagePath?>" width="350" height="350" alt="<?=$product->name ?>" />
                            </a>
                        </div><!--large-->

                        <div class="modal fade" id="myModal" role="dialog"><!-- image popup -->
                            <div class="modal-dialog">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-content">
                                    
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">x</button>
                                        </div>
                                        <img src="<?=$baseImagePath?>"  alt="<?=$product->name ?>" />
                                   
                                </div>
                            </div>
                        </div><!-- image popup end -->

                        <div class="description">
                            <h3>Description</h3>
                            <div class="descarea"><?=$product->description?></div>
                        </div>
                    </div><!--product-img-box-->
                    <div class="product-info">
                        
                            <div class="pricebox">
                                <span class="label">Price: </span>
                                <span class="price"><?=Helper::money($product->price);?></span>
                            </div>  
                            <div class="addtobox">
                                <span class="qty">Qty</span>

                                <input type="text" onkeyup="chknumber(this)" name="Products[quantity]" value="1" class="vsamll" />

                                <input type="submit" class="button" name="order" value="ADD TO CART" />
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                            <?php if($product->typeId == "bundle"){ ?>
                                <div class="bundle_item">
                                   <?=$product->renderBundleFields($form);?>
                                </div>     
                            <?php } 
                                  else if($product->typeId == "configurable") { 
                                ?>
                                <div class="configurable_item">
                                   <?=$product->renderConfigurableFields($form);?>
                                </div>     
                            <?php } ?>
                        <div class="clear"></div>
                        <div class="product_attributes">
                        	<h3>Product Details</h3>
                        	<?= DetailView::widget([
                            'model' => $product,
                            'attributes' => $product->detailViewAttributes,
                            ]);
                            ?>
                        </div>	
                        
                        
                    </div>
                    <div class="clear"></div>
                <?php ActiveForm::end(); ?>
            	<div class="related_products product_grid">
					<h3>OTHER PRODUCTS YOU MAY LIKE</h3>
					<?php  echo ListView::widget([
		 				'class' => 'product_grid',
		 				'dataProvider' => $dataProvider,
		 				'itemView' => '/products/_featured',
                        'summary' => '',
		 				'itemOptions' => ['class' => 'items_featured']
					]);	?>
				</div>
				<div class="clear"></div>	
			</div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.bundle_dropdown').on("change", function(){
                var isdefined = $(this).find('option:selected').attr('data-isuserdefined');
                var defaultQty = $(this).find('option:selected').attr('data-defaultQty');
                var id = $(this).val();
                if(isdefined==1){  
                    $(this).parent().find('.qtytext').remove();
                    $(this).after('<div class="qtytext"><label class="control-label">Qty</label> <input type="text" class="item_qty" onchange="findvalues()" name="Products[BundleItems]['+id+'][qty]" value="'+defaultQty+'"></div>');
                }
                else {
                        if(id != ""){ 
                          $(this).parent().find('.qtytext').remove();
                            $(this).after('<div class="qtytext"><label class="control-label">Qty:</label> '+defaultQty+'<input type="hidden" class="item_qty" name="Products[BundleItems]['+id+'][qty]" value="'+defaultQty+'"></div>');
                        }
                        else { 
                            $(this).parent().find('.qtytext').hide(); 
                        }
                }

                findvalues();
        });
        $('.bundle_radio').on("click", function(){   
            values = [];
            var params = $(this).attr('data-params');
            var values =  $.parseJSON(params);
            var id = $(this).val();

            //console.log(values);

            $.each( values, function( key, value ) {
                if(value['data-isUserDefined']==1){ 
                    $('.qtytextradio').remove();
                    // $('.'+id).after('<div class="qtytextradio"><input type="text" class="item_qtyradio" onchange="findvalues()" name="Products[BundleItems]['+id+'][radioqty]" value="'+value['data-defaultQty']+'"></div>');

                    $('.bundle_radio:checked').parent().next().append('<div class="qtytextradio"><label class="control-label">Qty</label> <input type="text" class="item_qtyradio" onchange="findvalues()" name="Products[BundleItems]['+id+'][radioqty]" value="'+value['data-defaultQty']+'"></div>');

                    
                }
                else {
                        if(id != ""){
                            $('.qtytextradio').remove();
                            $('.products-bundleitems').after('<div class="qtytextradio"><label class="control-label">Qty:</label> '+defaultQty+'<input type="hidden" class="item_qtyradio" name="Products[BundleItems]['+key+'][radioqty]" value="'+value['data-defaultQty']+'"></div>');
                        }
                        else { 
                            $('.qtytextradio').hide(); 
                        }
                }
            });
           findvalues();
        }); 

        $('.bundle_checkbox').on("click", function(){ 
            findvalues();
        });
        $('.bundle_multisel').on("click", function(){
            findvalues();
        }); 
    }); 
</script>  

<script type="text/javascript">

function findvalues() {

    selected = [];
    selectedqty = [];
    var prices =  $(".price").text();
    var cur_price = prices.replace("AU$", "");
    var price = <?=$product->price?>;

    $('.bundle_checkbox:checked').each(function() { 
        selected.push(this.value);
    });
    $('.bundle_radio:checked').each(function() { 
        selected.push(this.value); 
    });
    if($('.bundle_dropdown').val() != "") {
        selected.push($('.bundle_dropdown').val());
        qty = $('.item_qty').val(); 
    }
    if($('.bundle_multisel').val() != null) {
        for( var i = 0, xlength = $('.bundle_multisel').val().length; i < xlength; i++)
            {
                selected.push($('.bundle_multisel').val()[i]);  
            }   
    }
    if($('.item_qty').val() != "") { 
        id = $('.bundle_dropdown').val();
        qty = $('.item_qty').val(); 
        var object = {};
        object[id] = qty;
        selectedqty.push(object);
    }
    if($('.item_qtyradio').val() != ""){
        id = $('.bundle_radio').val();
        qty = $('.item_qtyradio').val(); 
        var object = {};
        object[id] = qty;
        selectedqty.push(object);
    }
    $.ajax({
        url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=b2b/products/getprice', 
        type: 'POST',
        data: {selected: selected, price: price, cur_price: cur_price, selectedqty: selectedqty},
               success: function(data){
                    $(".price").html(data);
                }
    });

}

</script>



<script type="text/javascript"> 

  $(document).ready(function(){

        $('.attribute-select').attr('disabled', 'disabled');
        $('.configurable_item .form-group:first-child select').attr('disabled', false);

        selected_attr = [];
        var base_price = <?=$product->price?>;

        $('.attribute-select').on("change", function(){ //alert('changed');
            $('.configurable_item .form-group select').attr('disabled', false);
            selected_attr = [];
           // alert($('.attribute-select option:selected').val());
           // selected_attr.push($('.attribute-select:selected').val());
            
        
            $.ajax({
                url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=b2b/products/getconfigprice', 
                type: 'POST',
                data: {selected_attr: selected_attr, base_price: base_price},
                    success: function(data){
                  //  $(".price").html(data);
                }
            });
        });    

    });   

</script>

 