<?php 
use frontend\components\Helper;
use yii\widgets\ActiveForm;
?>

<?php //var_dump($model->getThumbnailImage()); die;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."base/".$model->getThumbnailImage();    
?>

<div id="" class="featured_item">
    <div>
        <?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => \yii\helpers\Url::to(['cart/add'])]); ?>
                    <input type="hidden" class="productsids" name="Products[id]" value="<?=$model->id?>"/>
            	<a href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>">
                    <img src="<?=$thumbImagePath?>" width="150" height="150"/>
                </a>
                <div class="product-name">
                    <a href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>" title="<?=$model->name ?>"><?=Helper::stripText($model->name) ?></a>
                </div>
                <div class="desc"><?=$model->short_description ?></div>
                <div class="attcon">
                    <div class="sku"><b>SKU: </b><span><?=$model->sku ?></span></div>
                    <div class="sku"><b>Markup: </b><span><?=$model->getMarkup()?>%</span></div>
                    <div class="sku"><b>Retail price: </b><span><?=Helper::money($model->sellingPrice)?></span></div>
                </div>
                <div class="pricebox"><span class="price"><?=Helper::money($model->cost_price) ?></span></div>
                <div class="addto">
                    <input type="hidden" name="product_id" value="7681" />
                    <span class="qty">Qty</span>
                    <input type="text" onkeyup="chknumber(this)" name="Products[quantity]" value="1" class="vsamll" />
                    <input type="submit" class="button" name="order" value="Order" /><div class="clear"></div>
                </div>
        <?php ActiveForm::end(); ?> 
    </div>    
</div>
