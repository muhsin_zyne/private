<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<div class="product-settings-form">

    <?php $form = ActiveForm::begin(['method' => 'POST', 'options' => [/*'class' => 'nobind'*/]]); ?>
    <?php 
    	
        if($product->typeId=="configurable"){
    		echo $product->renderConfigurableFields($form);
    	}elseif($product->typeId=="bundle"){   
    		echo $product->renderBundleFields($form);
    	}
    ?>
    <div class="form-group">
        <?= Html::submitButton('Set', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
