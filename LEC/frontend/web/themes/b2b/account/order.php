<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\components\Helper;
use kartik\date\DatePicker;
use common\models\StorePromotions;
use yii\grid\GridView;
use yii\widgets\DetailView;

?>
<?php
$user = User::findOne(Yii::$app->user->Id);
//var_dump(Url::base());die();
?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="orders">
			<h2>Online Order form</h2>	
			<div class="createorder">
				<p>
					Please enter the SKU or Ezcode of the product you would like to order. Once you type in the SKU/Ezcode, a drop down shall appear if the SKU/Ezcode you entered matches with the SKUs in our Database. Then click on the SKU/Ezcode in the dropdown to see the product title. You may then add the quantity. If you would like to add another product to your order, please click the '+' button.
				</p>
				<form id="multiloc_order" onsubmit="return validation();" class="multiloc-order" name="multiloc_order" method="POST" action="<?=Url::to(['cart/add'])?>">
				<table id="table_create_order">
					<thead>
						<tr>
							<th><label>Product Ezcode, SKU</label></th>
							<th><label>Title</label></th>
							<th></th>
							<th><label>Unit Price</label></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr class="item" id="0">
							<td>
								<div class="droplist">
									<input type="text" autocomplete="off" name="sku[]" class="product_sku">
									<div class="listitem">
										<ul></ul>
									</div>
								</div>
							</td>
							<td>
								 <div class="product_title"></div>
							</td>
							<td width="300" class="sub_address">
								<?php foreach ($addresses as $address) {  ?>
									<span>
										<label><?=$address["storeName"]?></label>
										<input type="text" onkeyup="chknumber(this)" value="" data-addressid="<?=$address["id"]?>" name="" id="" class="vsmall quantity">
									</span>
								<?php } ?>
								<input type="hidden" name="Products[id]" class="product-id"/>
								<div class="clear"></div>
							</td>
							<td id="price" class="unitcost">
								<span class="price"></span>
							</td>
							<td>
								<div class="add_icon">
									<img border="0" class="add_more" style="cursor:pointer" alt="Add more products" src="/themes/b2b/images/add_icon.jpg">
								</div>
							</td>
						</tr>			
					</tbody>
				</table>
				<div class="proceed_order"><a class="save-cart">Order</a></div>
				</form>	
			</div>
		</div>
	</div>
</div>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
	$('.save-cart').click(function(){
		$('#multiloc_order').submit();		
	});
    	$(".createorder table").on('keyup','.product_sku',function(e){ 
    		var keyword = $(this).val();
    		$(this).next('.listitem').css('display','none');
    		var thisText = this;
    		$.ajax({
                url:'<?php echo \yii\helpers\Url::to(["products/getproducts", "limit" => "10"])?>',    
                type: 'POST',
                data: {keyword: keyword},
               	success: function(data)
                   	{
                        $(thisText).next('.listitem ul').empty("");
                        $('.listitem ul').html('');
						if($.trim($(thisText)).length > 0) 
                        {   
                            $(thisText).next('.listitem').css('display','block');
							var baseurl = "<?php echo Url::base() ?>";	
                            $.each($.parseJSON(data), function(key, item) { //console.log($(thisText).find('.listitem ul').length)

                            			if(item['type'] != "simple") {	
									href= baseurl+'/b2b/products/config?pid='+item['id']; 	

								$(thisText).next('.listitem').find('ul').append('<li onclick="productdetails(this)" href="'+href+'" data-productId="'+item['id']+'" data-before-submit="setConfig('+item['id']+');" data-name="'+item['name']+'" data-costprice="'+item['cost_price']+'" data-sku="'+item['sku']+'" class="ajax-update">'+item['sku']+'</li>');

								}
								else { 
										href = "#"; 
										$(thisText).next('.listitem').find('ul').append('<li onclick="productdetails(this)" href="'+href+'" data-productId="'+item['id']+'" data-name="'+item['name']+'" data-costprice="'+item['cost_price']+'" data-sku="'+item['sku']+'">'+item['sku']+'</li>');
								}
							}); 
                        }
					}
            });	
    	});	
    });
</script>

<script type="text/javascript">
	function productdetails(e){

		tr =  $(e).closest('tr');
		trid = tr.attr('id');
		id = $(e).attr('data-productid');
		name = $(e).attr('data-name'); 
		costprice = $(e).attr('data-costprice');
		sku = $(e).attr('data-sku');
		$('tr#'+trid).find('.product_sku').val(sku);
		$('tr#'+trid).find('.product-id').attr("name", "Products["+id+"][id]").attr("value", id);
		$('tr#'+trid).find('.product_title').html(name);
		$('tr#'+trid).find('.price').html(costprice);
		$('tr#'+trid).find('.vsmall').each(function(index, value){ 
			addressid = $(this).attr('data-addressid');
            $(this).attr('name','Products['+id+'][B2bAddresses]['+addressid+']');
        });   
		$('tr#'+trid).find('.listitem').css('display','none');
	}
</script>    

<script type="text/javascript">
	$(document).ready(function() {
		var i=1;
		$(".add_more").on('click',function(){ 
			$('.createorder table tbody').append('<tr class="item" id="'+i+'"><td><div class="droplist"><input type="text" autocomplete="off" name="sku[]" class="product_sku"><div class="listitem"><ul></ul></div></div></td><td><div class="product_title"></div></td><td width="300" class="sub_address"><?php foreach ($addresses as $address) {  ?><span><label><?=$address["storeName"]?></label><input type="text" value="" data-addressid="<?=$address["id"]?>" name="" id="" class="vsmall"></span><?php } ?><input type="hidden" name="Products[id]" class="product-id"/><div class="clear"></div></td><td id="price" class="unitcost"><span class="price"></span></td><td><div class="add_icon"></div></td></tr>');

			i = i+1;
		});	
	});	
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".createorder table").on('click','.ajax-update',function(e){
			//alert('hai');
		}); 
	});
</script>		

<script type="text/javascript">
	$(document).ready(function() {
		$.validator.addMethod("uniqueName", function(value, element) {
        	var parentForm = $(element).closest('form');
        	if ($(parentForm.find('.vsmall[value=' + value + ']')).size() > 1) {
            	return false;
        	}
	        else {
	            return true;
	        }
    	}, "Name exists already");
    
    	$.validator.addClassRules({
	        vsmall: {
	        	required: true,
	        	minlength: 2,
	        	//uniqueName: true
        	}
    	});
    
    $(".multiloc_order").validate();
	});	
</script>

<script type="text/javascript">
function chknumber()
{
    var item = jQuery(id).val();
    //alert(item);
    jQuery(id).onkeyup = function() {
        var key = event.keyCode || event.charCode;
        //alert(key);
        if( key == 8 || key == 46 )
            return false;
    };
    intRegex = /^[0-9]+$/;
    if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
    {
        if(item!='')
        {
            alert('Please enter a valid quantity');
            jQuery(id).val('1');
        }
        
        return false;
    }
}
</script>

<script type="text/javascript">
	function validation()
	{
		var error1 = 0;
		var error2 = 0;

		$.each($('.product_sku'), function(index, value) { 
    		if ($(this).val() == "") {
    			alert('Please select a product');
    			error1 = error1+1;
    		};
    	});

    	if(error1!="0")
    		return false;
    	else{
    		$.each($('.quantity'), function(index, value) { 
	    		if ($(this).val() == "") {
	    			alert('Please add quantity required');
	    			error2 = error2+1;
	    			return false;
	    		};
    		});

    		if(error2!="0")
    			return false;
    	}

    	return true;

    }	
</script>

