<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
?>

<?php
$user = User::findOne(Yii::$app->user->Id);
?>


<div class="mainarea">
<div class="wrapper_inner">
<div class="col-main leftside">
        <h2>MY DASHBOARD</h2>
        
        <div class="myaccount">
            <b>Hello, <?=$user->Fullname?></b>
            <p>Here you can view the snapshot of your recent activities and you can update your Billing and Shipping addresses.</p>
            <div class="box-account box-recent">
                <div class="box-head">
                    <h3>Recent Orders</h3>
                    <div class="clear"></div>
                </div>
                <?php \yii\widgets\Pjax::begin(['id' => 'recent-products',]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'id'=>'recent-products',
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        //'order.orderDate',
                        [
                            'label' => 'Order #',
                            'attribute' => 'id',
                            'value' => 'orderId',
                        ],
                        [
                            'label' => 'Product Name',
                            'attribute' => 'id',
                            'value' => 'product.name',
                        ],
                        [
                            'label' => 'Order Date',
                            'attribute' => 'id',
                            'value' => 'order.orderPlacedDate',
                        ],
                        [
                            'label' => 'Ship To',
                            'attribute' => 'id',
                            'value' => 'b2baddress.storeName',
                        ],
                        [
                            'label' => 'Qty',
                            'attribute' => 'id',
                            'value' => 'quantity',
                        ],
                        [
                            'label' => 'Total',
                            'attribute' => 'id',
                            'format' => 'html',
                           'value' => function ($model) {
                                return Helper::money($model->price*$model->quantity);
                            },
                        ],
                    ],
                    
                ]); ?>
                <?php  \yii\widgets\Pjax::end();  ?> 
            </div>

            <!-- Updating Address-->
            <!-- <div class="acc-address">
                <h3>Address book</h3>
                <p>Please enter your new Billing and Shipping Addresses to be updated.</p>
                <form action="" method="post">
                    <input type="hidden" name="form_key" value="fZ9inTDFROGgOVvx" />
                    <div class="box-address">
                        <label>Billing Address</label>
                        <textarea name="billing_address"><?php //isset($user->defaultB2bAddress)? $user->defaultB2bAddress->billingAddress : ""?></textarea>
                    </div>
                    <div class="box-address">
                        <label>Shipping Address</label>
                        <textarea name="shipping_address"><?php //isset($user->defaultB2bAddress)? $user->defaultB2bAddress->shippingAddress : ""?></textarea>
                    </div>
                    <div class="clear"></div>
                    <input type="hidden" name="user_id" value="33" />
                    <input type="submit" class="button" name="update" value="Update Address" />
                </form>
            </div> -->
    </div>
</div>

<div class="rightside">
    <div class="block block-account">
        <div class="block-title">
            <span>My Account</span>
        </div>
        <div class="block-content">
            <ul>
                <li><a href="<?=\yii\helpers\Url::to(['account/index']) ?>">Account Dashboard</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>">Address Book</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['account/orders']) ?>">My Orders</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['cart/saved']) ?>">Saved Cart Lists</a></li>
                <li><a href="<?=Yii::$app->params['b2csignupurl']?>" target="_blank">Sign Up for Website</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['account/changepassword']) ?>">Change Password</a></li>
                <li class="last"><a href="<?=\yii\helpers\Url::to(['default/logout']) ?>">Logout</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="clear"></div>
</div><!-- end .wrapper-->
</div><!--mainarea-->
