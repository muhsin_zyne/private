<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */



$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrapper_login frgt-pswrd"> 
    <div class="col-main">
       
        <b>Please fill out your email. A link to reset password will be sent there.</b>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']);  ?>
          
        <div class="textfeild">

            <?= $form->field($model, 'email') ?>
            
        </div>
        <?= Html::submitButton('Send', ['class' => 'button']) ?>
          
        </form>
      </div>
    </div><!-- end .wrapper-->
</div>

    