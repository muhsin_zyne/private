<?php
use frontend\components\Helper;
use yii\helpers\Url;
?>

<?php //var_dump($model->getThumbnailImage()); die;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$model->getThumbnailImage();    
?>

<?php //var_dump("Price:".$model->sellingPrice);?>

<?php //var_dump("Cost Price:".$model->cost_price);die(); ?>

<li class="" id="item_6842">
    <div>
        <div class="floatchk">
            <input type="checkbox" name="Products[<?=$model->id?>][id]" value="<?=$model->id?>" class="chkbx ajax-update" href="<?=Url::to(['products/config','pid' => $model->id])?>" data-before-submit="setConfig(<?=$model->id?>);" data-title="Configure the product" data-configurable="<?=($model->typeId=="configurable" || $model->typeId=="bundle")? 'true' : 'false'?>">
        </div>
        <div class="floatimg">
        <a href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>" <?php echo ($model->isInConference()) ? 'class="ribbon"' : '' ?>><img alt="<?=$model->name?>" src="<?=$thumbImagePath?>"></a>
            <div class="clear"></div>
        </div>
        <div class="floatinfo">
            <div class="product-name">
                <a title="18ct .25ct Diamond Shoulder Solitaire" href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>"><?=Helper::stripText($model->name) ?></a>
            </div>
            <div class="desclist">18ct .25ct Diamond Shoulder Solitaire</div>
            <div class="sku"><b>SKU: </b><span><?=$model->sku?></span></div>
            
        </div>
        <div class="floatleft">

            <div class="pricebox"><span class="price"><b><?=Helper::money($model->price)?></b></span></div>

        </div>
        <div class="floatright">
             <div class="addto">
                <span class="qty">Qty</span>
                <input type="text" class="vsamll" value="" name="Products[<?=$model->id?>][quantity]" onkeyup="chknumber(this)">
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</li>
