<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="wrapper_login"> 
      <div class="col-main">
        <div id="myOverlay">Loading products for a super-fast experience. Please wait....</div>
        <?php $form = ActiveForm::begin(['id' => 'loginform']); ?>
          <input type="hidden" name="form_key" value="EuSTNKx0DsGG2KSf" />
          <div class="textfeild">
            <?= $form->field($model, 'email') ?>
          </div>
          <div class="textfeild">
            <?= $form->field($model, 'password')->passwordInput() ?>
          </div>
          <?= Html::submitButton('Login', ['class' => 'button', 'name' => 'login']) ?>
         <a ><?= Html::a('Forgot password?', ['default/request-password-reset']) ?></a>
        </form>
      </div>
    </div><!-- end .wrapper-->
    </div>

    <script type="text/javascript">
  function validate_login_form()
    {  
      var x=document.forms["loginForm"]["username"].value;
      if (x==null || x=="")
        {
          alert("Please enter Username");
          return false;
        }  
      var x=document.forms["loginForm"]["password"].value;
      if (x==null || x=="")
        {
          alert("Please enter Password");
          return false;
        } 
      jQuery("#myOverlay").show();     
    }
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
     jQuery('span:contains("Catalogues")').parent().attr("href", "javascript:;");
  })
</script>