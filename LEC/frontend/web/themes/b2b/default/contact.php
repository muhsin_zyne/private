<?php



use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use yii\captcha\Captcha;



$this->title = 'Contact Us';

$this->params['breadcrumbs'][] = $this->title;

?>



<div class="container">

    <div class="contact_area">

    <h1><?= Html::encode($this->title) ?></h1>

        <div class="site-contact">

            <p>

                Please use this form to contact the site administrator and provide feedback on your user experience or the platform more generally. We will respond as soon as possible.

            </p>

            <div class="row">

             <div class="contact_right"><img src="/images/contact.jpg"></div>

                <div class="col-lg-6">

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>



                        <?= $form->field($model, 'name') ?>



                        <?= $form->field($model, 'email') ?>



                        <?= $form->field($model, 'phone') ?>

                       

                        <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>



                        <?php echo $form->field($model, 'verifyCode')->widget(Captcha::className(), array(

                            'captchaAction' => '/site/captcha',

                            //'options' => array('class' => 'input-medium'),

                        ));  ?>

                        <div class="form-group">

                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>

                        </div>

                    <?php ActiveForm::end(); ?>



                </div>

            </div>

        </div>

    </div>

</div>



