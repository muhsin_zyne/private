<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="mainarea">
    <div class="wrapper_inner">
        <div class="col-main Product_Featured">
            <h2 class="">Search Results for -"<?=$_REQUEST['q']?>"</h2>
              
                <div class="clear"></div>
                <?php $form = ActiveForm::begin(['method' => 'POST', 'action' => Url::to(['cart/add']), 'id' => 'list_form_pro']); ?>
                    <input type="hidden" name="form_key" value="ItAThARoVgdWrAGq" />
                    <div class="product_list">
                        <div id="checkall">
                            <a href="javascript:void(0);" onclick="selectvisible()">Select visible</a>
                            <a href="javascript:void(0);" onclick="unselectvisible()">Unselect visible</a>
                        </div>

                <ol>
                    <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                    <?php 
                        echo ListView::widget( [
                        //'id' => 'testclass',
                        'dataProvider' => $dataProvider,
                        //'filter' => $searchModel,
                        'itemView' => '/default/_productlist',
                        //'summary' => '',
                        'itemOptions' => ['class' => 'items col-md-4']
                        ] );
                    ?>
                    <?php  \yii\widgets\Pjax::end();  ?> 
                </ol>
                <div id="noresults"></div>
                <div class="animation_image" style="display:none" align="center">
                    <img src="http://www.orders.legj.com.au/skin/frontend/default/b2b/images/ajax-loader.gif"/>
                </div>
                </div>
                <div id="floatdiv">
                    <input type="submit" class="button list_bttn order_now" name="order" value="ADD TO CART" />
                    <div class="clear"></div>
                </div>
            <?php ActiveForm::end(); ?>


<!-- GRID LOAD HERE -->

</div>
        <div class="clear"></div>
</div><!-- end .wrapper-->
</div><!--mainarea-->
<script type="text/javascript">
function validsearch()
{
    var sename = document.getElementById('sename').value;
    if(sename=="")
    {
       alert('search field cannot be empty !!')
       return false;
    }
}
</script>

<script type="text/javascript">
$(".grid").click(function() {
    $("#category-products #testclass").removeClass('list-view');
    $("#category-products #testclass").addClass('grid-product');
});
</script>

<script type="text/javascript">
$(".list").click(function() {
    $("#category-products #testclass").addClass('list-view');
    $("#category-products #testclass").removeClass('grid-product');
});
</script>

<script type="text/javascript">
 /*           jQuery(document).ready(function() {
       var arrayFromPHP = {"vendor":"996","name":"Bond Street Jewellers"};
        var track_load = 0; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var total_groups = 65; //total record group(s)
    var type = 'list';
    var product_per_load=8;
    
    arrayFromPHP['group_no'] = track_load;
    arrayFromPHP['type'] = type;
    jQuery('.animation_image').show(); //show loading image
    
    jQuery.post("http://www.orders.legj.com.au/dashboard/products/ajaxproductload/",arrayFromPHP, function(data) {
       if(data)
       {
            jQuery("#results").append(data);
            jQuery('#noresults').html('');
            track_load++;
            jQuery('.animation_image').hide();
       }
       else
        {
            jQuery('.animation_image').hide();
            //jQuery('#noresults').html('<p style="color:red">There is no results to show</p>');                        
        }
    }); //load first group
    
    
    var K;
    jQuery(window).scroll(function() { //detect page scroll
    
        if((K!=(jQuery(document).height()-100)) && (jQuery(window).scrollTop() + jQuery(window).height() >= (jQuery(document).height()-100)))  //user scrolled to bottom of the page?
        {
          K=jQuery(document).height()-100;
          //    alert(jQuery(window).scrollTop() + jQuery(window).height());
        //alert('cdohe'+(jQuery(document).height()-100));
            
            if((track_load * product_per_load) <= total_groups && loading==false) //there's more data to load
            {
                loading = true; //prevent further ajax loading
                jQuery('.animation_image').show(); //show loading image
                
                //load data from the server using a HTTP POST request
                arrayFromPHP['group_no'] = track_load;
                jQuery.post('http://www.orders.legj.com.au/dashboard/products/ajaxproductload/',arrayFromPHP, function(data){
                    
                    if(data){
                        //alert(data);
                        jQuery("#results").append(data); //append received data into the element
                        //hide loading image
                        jQuery('#noresults').html('');
                        jQuery('.animation_image').hide(); //hide loading image once data is received                   
                        track_load++; //loaded group increment
                        loading = false;
                    }
                    else
                    {
                        jQuery('.animation_image').hide();
                        //jQuery('#noresults').html('<p style="color:red">There is no results to show</p>');                        
                    }
                
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    
                    alert(thrownError); //alert with HTTP error
                    jQuery('.animation_image').hide(); //hide loading image
                    loading = false;
                
                });
                
            }
        }
    });
});

function formsubmit(type)
{
    jQuery('#type_view').html('<input type="hidden" name="type" value="'+type+'" />');
    jQuery('#filter_form').submit();
}*/
</script>

<script>
function chk_validaion()
{
    if(jQuery("#list_form_pro .chkbx:checked").length==0)
    {
       alert("Please select atleast one product to continue");
       return false;
    }
}
function selectvisible()
{
    jQuery('#results .chkbx').attr('checked', 'checked');
}

function unselectvisible()
{
    jQuery('#results .chkbx').removeAttr('checked');
}
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.chkbx').change(function(){
        $('#product_ids').val()
    })
})
</script>

<script type="text/javascript">
jQuery("#nav li").each(function() {
    var href = jQuery(this).find('a').attr('href');
    var cur = 'http://www.orders.legj.com.au/dashboard/';
    if(href==cur)
    {
        //alert(href);
        jQuery(this).addClass('active');
    }
});


function minprice(id,max,min)
{
    var num = jQuery(id).val();
    chknumber(id);
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}
                    
function maxprice(id,max,min)  
{
    var num = jQuery(id).val(); 
    chknumber(id);
    
    if(num>max)
    {
        if(num!='')
        {
            alert("Please order below "+max+" quantity.");
            jQuery(id).val(max);
        }
    }
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}

function chknumber(id)
{
    var item = jQuery(id).val();
    //alert(item);
    jQuery(id).onkeyup = function() {
        var key = event.keyCode || event.charCode;
        //alert(key);
        if( key == 8 || key == 46 )
            return false;
    };
    intRegex = /^[0-9]+$/;
    if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
    {
        if(item!='')
        {
            alert('Please enter a valid quantity');
            jQuery(id).val('1');
        }
        
        return false;  
    }
}
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('change', '.addto input', function(){
        $(".vsamll").each(function() {
            if($(this).val() > 0){
                console.log($(this).val());
                $(this).parents('.items').find('.chkbx').prop('checked',true);
                //$(this).prev().find('floatchk').addClass('hai');
            }else{
                $(this).parents('.items').find('.chkbx').prop('checked',false);
            }
        });
    })
    $('.product_slider .item:first-child').addClass('active');
    $(".order_now").click(function() {
        //$(this).prev().addClass('hai');
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required');
          return false;
        }
        if($('.chkbx:checked').length < 1){
            alert('Please tick atleast one product to order');
            return false;
        }  
    }); 
  });  
</script>
