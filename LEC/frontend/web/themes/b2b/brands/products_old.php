<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
?>

<div class="mainarea">
    <div class="wrapper_inner">
        <div class="col-main">
            <h2 class="">Products</h2>
                <div class="area_filter">
                    <form name="filter" id="filter_form" method="post" action="">
                    <div id="type_view"></div>
                    <div class="flit_search">
                    <input type="text" name="query" value="" placeholder="Search by SKU, EzCode, Title" />
                    <input type="submit" name="fil_search" value="Search" class="button" />
                    </div>
                    <div class="filt_select">
                    <select name="filter_value" onchange="jQuery('#filter_form').submit();">
                    <option value="">Sort by</option>
                    <option  value="price_low">Price (Low to High)</option>
                    <option  value="price_high">Price (High to Low)</option> -->
                    <!--<option  value="name">Name</option>-->
                   <option  value="sku">SKU</option>
                    <option  value="ezcode">EzCode</option>
                    </select>
                    </div>
                    <div class="clear"></div>
                    </form>
                </div>
                <div class="view">
                    <span>View</span>
                    <a class="grid " href="javascript:void(0);" onclick="formsubmit('grid')">Grid</a>
                    <a class="list active" href="javascript:void(0);" onclick="formsubmit('list')">List</a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <form id="list_form_pro" name="pro_order" action="" method="post" onsubmit="return chk_validaion()">
                    <input type="hidden" name="form_key" value="ItAThARoVgdWrAGq" />
                    <div class="product_list">
                        <div id="checkall">
                            <a href="javascript:void(0);" onclick="selectvisible()">Select visible</a>
                            <a href="javascript:void(0);" onclick="unselectvisible()">Unselect visible</a>
                        </div>

                <ol>
                    <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                    <?php 
                        echo ListView::widget( [
                        'id' => 'testclass',
                        'dataProvider' => $products,
                        'itemView' => '/categories/_product',
                        'summary' => '',
                        'itemOptions' => ['class' => 'items']
                        ] );
                    ?>
                    <?php  \yii\widgets\Pjax::end();  ?> 
                </ol>
                <div id="noresults"></div>
                <div class="animation_image" style="display:none" align="center">
                    <img src="http://www.orders.legj.com.au/skin/frontend/default/b2b/images/ajax-loader.gif"/>
                </div>
                </div>
                <div id="floatdiv" style="position:absolute;bottom:25px;right:20px;z-index:9000;">
                    <input type="submit" class="button list_bttn" name="order" value="Order" />
                    <div class="clear"></div>
                </div>
                </form>


<!-- GRID LOAD HERE -->

</div>
</div><!-- end .wrapper-->
</div><!--mainarea-->

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#slider').nivoSlider({
        effect:'fade' // Specify sets like: 'fold,fade,sliceDown'
    });
}); 
</script>

<script type="text/javascript">
function validsearch()
{
    var sename = document.getElementById('sename').value;
    if(sename=="")
    {
       alert('search field cannot be empty !!')
       return false;
    }
}
</script>

<script type="text/javascript">
            jQuery(document).ready(function() {
       var arrayFromPHP = {"vendor":"996","name":"Bond Street Jewellers"};
        var track_load = 0; //total loaded record group(s)
    var loading  = false; //to prevents multipal ajax loads
    var total_groups = 65; //total record group(s)
    var type = 'list';
    var product_per_load=8;
    
    arrayFromPHP['group_no'] = track_load;
    arrayFromPHP['type'] = type;
    jQuery('.animation_image').show(); //show loading image
    
    jQuery.post("http://www.orders.legj.com.au/dashboard/products/ajaxproductload/",arrayFromPHP, function(data) {
       if(data)
       {
            jQuery("#results").append(data);
            jQuery('#noresults').html('');
            track_load++;
            jQuery('.animation_image').hide();
       }
       else
        {
            jQuery('.animation_image').hide();
            //jQuery('#noresults').html('<p style="color:red">There is no results to show</p>');                        
        }
    }); //load first group
    
    
    var K;
    jQuery(window).scroll(function() { //detect page scroll
    
        if((K!=(jQuery(document).height()-100)) && (jQuery(window).scrollTop() + jQuery(window).height() >= (jQuery(document).height()-100)))  //user scrolled to bottom of the page?
        {
          K=jQuery(document).height()-100;
          //    alert(jQuery(window).scrollTop() + jQuery(window).height());
        //alert('cdohe'+(jQuery(document).height()-100));
            
            if((track_load * product_per_load) <= total_groups && loading==false) //there's more data to load
            {
                loading = true; //prevent further ajax loading
                jQuery('.animation_image').show(); //show loading image
                
                //load data from the server using a HTTP POST request
                arrayFromPHP['group_no'] = track_load;
                jQuery.post('http://www.orders.legj.com.au/dashboard/products/ajaxproductload/',arrayFromPHP, function(data){
                    
                    if(data){
                        //alert(data);
                        jQuery("#results").append(data); //append received data into the element
                        //hide loading image
                        jQuery('#noresults').html('');
                        jQuery('.animation_image').hide(); //hide loading image once data is received                   
                        track_load++; //loaded group increment
                        loading = false;
                    }
                    else
                    {
                        jQuery('.animation_image').hide();
                        //jQuery('#noresults').html('<p style="color:red">There is no results to show</p>');                        
                    }
                
                }).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
                    
                    alert(thrownError); //alert with HTTP error
                    jQuery('.animation_image').hide(); //hide loading image
                    loading = false;
                
                });
                
            }
        }
    });
});

function formsubmit(type)
{
    jQuery('#type_view').html('<input type="hidden" name="type" value="'+type+'" />');
    jQuery('#filter_form').submit();
}
</script>

<script>
function chk_validaion()
{
    if(jQuery("#results .chkbx:checked").length==0)
    {
       alert("Please select atleast one product to continue");
       return false;
    }
}
function selectvisible()
{
    jQuery('#results .chkbx').attr('checked', 'checked');
}

function unselectvisible()
{
    jQuery('#results .chkbx').removeAttr('checked');
}
</script>

<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('span:contains("Catalogues")').parent().attr("href", "javascript:;");
})
</script>

<script type="text/javascript">
jQuery("#nav li").each(function() {
    var href = jQuery(this).find('a').attr('href');
    var cur = 'http://www.orders.legj.com.au/dashboard/';
    if(href==cur)
    {
        //alert(href);
        jQuery(this).addClass('active');
    }
});


function minprice(id,max,min)
{
    var num = jQuery(id).val();
    chknumber(id);
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}
                    
function maxprice(id,max,min)
{
    var num = jQuery(id).val();
    chknumber(id);
    
    if(num>max)
    {
        if(num!='')
        {
            alert("Please order below "+max+" quantity.");
            jQuery(id).val(max);
        }
    }
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}

function chknumber(id)
{
    var item = jQuery(id).val();
    //alert(item);
    jQuery(id).onkeyup = function() {
        var key = event.keyCode || event.charCode;
        //alert(key);
        if( key == 8 || key == 46 )
            return false;
    };
    intRegex = /^[0-9]+$/;
    if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
    {
        if(item!='')
        {
            alert('Please enter a valid quantity');
            jQuery(id).val('1');
        }
        
        return false;
    }
}
</script>

<script type="text/javascript">  
floatingMenu.add('floatdiv',  
{  
    // Represents distance from left or right browser window  
    // border depending upon property used. Only one should be  
    // specified.  
    targetLeft: 1040,  
    //targetRight: 20,  

    // Represents distance from top or bottom browser window  
    // border depending upon property used. Only one should be  
    // specified.  
    //targetTop: 20,  
     targetBottom: 25,  

    // Uncomment one of those if you need centering on  
    // X- or Y- axis.  
    // centerX: true,  
    // centerY: true,  

    // Remove this one if you don't want snap effect  
    snap: false  
});  
</script>