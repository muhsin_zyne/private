<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<?php
    /*$date = date_create();
    echo date_format($date, 'Y-m-d H:i:s') . "\n";*/
?>

<!--<style type="text/css">
    .col-left{ display: none; }
    .col-right{ width: 100%; float:none;}
    .topsearch > form { margin: 0; }
    body {  margin-top: -18px !important; }
</style>-->

<div class="mainarea">
    <div class="wrapper_inner">
    	<div class="col-left">
        	<?php
                if(!Yii::$app->request->isAjax){
                    ProductFilter::begin([
                        'basedModel' => $brand,
                        'attributes' => $brand->filterAttributes,
                    ]);
                    ProductFilter::end();
                }    
            ?>
        </div>
        <div class="col-main col-right Product_Featured">
            <h2 class="">Products</h2>
                <div class="area_filter">
                    <?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => \yii\helpers\Url::to(['cart/add'])]); ?>
                    <input type="hidden" class="productsids" name="Products[id]"/>
                    <div id="type_view"></div>
                    <div class="flit_search">
                        <input type="text" class="search" id="pi_search" name="query" value="<?=isset($_REQUEST['keyword'])? $_REQUEST['keyword'] : ''?>" placeholder="Search by SKU, EzCode, Title" />
                        <input type="button" name="filter_search" id="p_search"  value="Search" class="button search-button" onclick="validsearch()">
                        <div class="view">
                            <span>View</span>
                            <a class="grid" href="javascript:void(0);" onclick="">Grid</a>
                            <a class="list active" href="javascript:void(0);" onclick="">List</a>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                   
                    <div class="sort-by">
                        <span>Sort by</span>
                        <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['dateAdded' => 'New', 'price' => 'Price', 'name' => 'Name', 'sku' => 'SKU', 'ezcode' => 'EZ Code'])?>
                        <?php if(isset($_GET['sort']) && strpos($_GET['sort'], "-") !== FALSE){ ?>
                        <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"><span class="custom_sort">ASC</span> </a>
                        <?php }else{ ?>
                        <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                        <?php } ?>
                    </div>
                    <div class="pager">
                        <p class="amount">
                          <strong><?=(\Yii::$app->request->isAjax)? $products->totalCount." Item(s)" : ""?></strong>
                        </p>
                        <div class="limiter">
                            <label>Show</label>
                            <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['20' => '20', '30' => '30', '40' => '40', '50' => '50'])?>
                            per page
                        </div>
                    </div>
                    
                    <div class="clear"></div>
                    <?php ActiveForm::end(); ?>
                </div>    

                <div class="clear"></div>
                    
                <!-- <div class="clear"></div> -->
                <?php $form = ActiveForm::begin(['method' => 'POST', 'action' => Url::to(['cart/add']), 'id' => 'list_form_pro']); ?>
                    <div class="product_lists">
                        <ol>
                            <?php \yii\widgets\Pjax::begin(['id' => 'category-products']); ?>
                            <?php if(Yii::$app->request->isAjax){ ?>
                            <?php 
                                echo ListView::widget( [
                                'id' => 'testclass',
                                'dataProvider' => $products,
                                'itemView' => '/categories/_product',
                                'summary' => '',
                                'itemOptions' => ['class' => 'items col-md-4'],
                                'viewParams'=>['promotionProductsArray'=>$promotionProductsArray]
                                ] );
                            ?>
                            <?php } ?>
                            <?php  \yii\widgets\Pjax::end();  ?> 
                            <?php 
                                            if(!Yii::$app->request->isAjax){
                                        ?> 
                                            <div style="text-align: center;" class="loading">
                                                <img src="/themes/b2b/images/loading.gif"/>
                                            </div>

                                        <?php } ?>
                        </ol>

                        <div class=" loading_pjax" style="display:none;">
                            <div style="text-align: center;" class="loading">
                                <img src="/themes/b2b/images/loading.gif"/>
                            </div>
                        </div>   

                        <div id="noresults"></div>
                        <div id="floatdiv">
                            <input type="submit" class="button list_bttn order_now" name="order" value="ADD TO CART" />
                            <div class="clear"></div>
                        </div>
                    <!-- GRID LOAD HERE -->
                    <div class="clear"></div>
                    </div>
                <?php ActiveForm::end(); ?>
                <div class="clear"></div>
        </div>	
    <div class="clearfix"></div>
    </div><!-- end .wrapper-->
    
</div><!--mainarea-->

<script type="text/javascript">
function validsearch(){
    var sename = $('.search').val();
    // if(sename=="")
    // {
    //    alert('search field cannot be empty !!')
    //    return false;
    // }
    // else{
        //$('.col-left').append('<input type="text" name="Products[keyword]" value='+sename+'>');
        $('.keyword').val(sename);
        filterproducts();
    // }    
}
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.product_slider .item:first-child').addClass('active');
    $(".order_now").click(function() {
        //$(this).prev().addClass('hai');
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required')
          return false;
        }  
    }); 
  });  
</script>

<script type="text/javascript">
$('body').on('click', '.grid', function(){
     $("#category-products #testclass").removeClass('list-view');
    $("#category-products #testclass").addClass('grid-product');       
});   

</script>

<script type="text/javascript">
$('body').on('click', '.list', function(){
    $("#category-products #testclass").addClass('list-view');
    $("#category-products #testclass").removeClass('grid-product');       
});

</script>

<script>
function chk_validaion()
{
    if(jQuery("#list_form_pro .chkbx:checked").length==0)
    {
       alert("Please select atleast one product to continue");
       return false;
    }
}
function selectvisible()
{
    jQuery('#results .chkbx').attr('checked', 'checked');
}

function unselectvisible()
{
    jQuery('#results .chkbx').removeAttr('checked');
}
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.chkbx').change(function(){
        $('#product_ids').val()
    })
})
</script>

<script type="text/javascript">
jQuery("#nav li").each(function() {
    var href = jQuery(this).find('a').attr('href');
    var cur = 'http://www.orders.legj.com.au/dashboard/';
    if(href==cur)
    {
        //alert(href);
        jQuery(this).addClass('active');
    }
});


function minprice(id,max,min)
{
    var num = jQuery(id).val();
    chknumber(id);
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}
                    
function maxprice(id,max,min)  
{
    var num = jQuery(id).val(); 
    chknumber(id);
    
    if(num>max)
    {
        if(num!='')
        {
            alert("Please order below "+max+" quantity.");
            jQuery(id).val(max);
        }
    }
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}

function chknumber(id)
{
    var item = jQuery(id).val();
    //alert(item);
    jQuery(id).onkeyup = function() {
        var key = event.keyCode || event.charCode;
        //alert(key);
        if( key == 8 || key == 46 )
            return false;
    };
    intRegex = /^[0-9]+$/;
    if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
    {
        if(item!='')
        {
            alert('Please enter a valid quantity');
            jQuery(id).val('1');
        }
        
        return false;  
    }
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        // $(window).keydown(function(event){
        //     if(event.keyCode == 13) {
        //       event.preventDefault();
        //       return false;
        //     }
        // }); 
        $(".chkbx").change(function() {
            if(this.checked) {
                 $('.productsids').val($(this).val());
            }
        });
    });    
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('body').on('change', '.addto input', function(){
        $(".vsamll").each(function() {
            if($(this).val() > 0){
                //console.log($(this).val());
                $(this).parents('.items').find('.chkbx').prop('checked',true);
                //$(this).prev().find('floatchk').addClass('hai');
            }else{
                $(this).parents('.items').find('.chkbx').prop('checked',false);
            }
        });
    })
    $('.product_slider .item:first-child').addClass('active');
    //$(".order_now").click(function() {  alert('hai');
    $('body').on('click', '.order_now', function(){    
        //$(this).prev().addClass('hai');
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required');
          return false;
        }
        if($('.chkbx:checked').length < 1){
            alert('Please tick atleast one product to order');
            return false;
        }  

    });

    //$(".chkbx").change(function() {
    $('body').on('change', '.chkbx', function(){     
            if(this.checked) {
                $('.productsids').val($(this).val());
                //$('.vsamll').val('1');
                $(this).parents('.items').find('.vsamll').val('1');
            }
        }); 

   

  });  
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('change', '.limiter select', function(){
            $('#filter-form .page-size').val($(this).val());
            filterproducts();
        });
        $('body').on('change', '.sort-by select', function(){
            $('#filter-form .sort').val($(this).val());
            filterproducts();
        })
    });  
</script>
