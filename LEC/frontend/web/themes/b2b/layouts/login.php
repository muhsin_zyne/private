<?php  

use yii\helpers\Html;

?>

<?php $this->beginPage() ?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <title>LEC B2B Login</title>

  <meta name="description" content="Leading Edge Computers" />

  <meta name="keywords" content="Leading Edge Computers" />

  <meta name="robots" content="INDEX,FOLLOW" />

  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

  <link rel="icon" href="/images/favicon.ico" type="image/x-icon" />

  <link rel="shortcut icon" href="/i/mages/favicon.ico" type="image/x-icon" />

  <link rel="stylesheet" type="text/css" href="/themes/b2b/css/style.css" media="all" />

  <script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script> 

  

</head>



<body class=" dashboard-login-index">

  <div class="mainarea loginmainarea">

    <div class="header-log">

      <noscript>

        <div class="noscript">

            <div class="noscript-inner">

              <p><strong>JavaScript seem to be disabled in your browser.</strong></p>

              <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>

            </div>

        </div>

      </noscript>

      <div class="logo_login">

        <a href="#" title="Leading Edge Computers"><img src="/themes/b2b/images/logo.png" alt="Leading Edge Group Jewellers" /></a>

      </div>

    </div><!--header-->

    <div style="margin:0 auto 15px auto; width:450px;">

    <?php if(Yii::$app->session->getFlash('error'))

              { ?>

              <div class="alert alert-danger alert-dismissable">

                <i class="icon fa fa-ban"></i>

                <button type="button" class="close" style="float:right;" data-dismiss="alert" aria-hidden="true">×</button>                         

                <span style="color:red; font-size:14px;"><?= Yii::$app->session->getFlash('error'); ?></span>       

              </div> 

            <?php } ?>

            <?php if(Yii::$app->session->getFlash('success'))

              { ?>

              <div class="alert alert-success alert-dismissable">

                <i class="icon fa fa-check"></i>

                <button type="button" class="close" style="float:right;" data-dismiss="alert" aria-hidden="true">×</button>

                <span style="color:red; font-size:14px;"><?= Yii::$app->session->getFlash('success'); ?></span>       

              </div> 

            <?php } ?>

            </div>

    <?= $content ?>

	</div>

    <div class="footerarea">

    <div class="footer-container">

      <div class="copyright_login">Leading Edge Computers. All Rights Reserved.</div>

      <div class="clear"></div>

    </div>

  </div><!--footerarea-->

<script type="text/javascript">

  function validate_login_form()

    {  

      var x=document.forms["loginForm"]["username"].value;

      if (x==null || x=="")

        {

          alert("Please enter Username");

          return false;

        }  

      var x=document.forms["loginForm"]["password"].value;

      if (x==null || x=="")

        {

          alert("Please enter Password");

          return false;

        } 

      jQuery("#myOverlay").show();     

    }

</script>





</body>

</html>