﻿<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use commmon\models\Stores;
use common\models\User;
use frontend\components\B2bMainMenu;
use frontend\components\B2bHeaderMenu;
use frontend\assets\AppAsset;
use frontend\components\MainCategory;
use frontend\components\MainMenu;
//AppAsset::register($this);


// $user = User::findOne(Yii::$app->user->Id);
// $brands = $user->store->brands;
// $suppliers = $user->store->b2bSuppliers;

AppAsset::register($this);
?>
<?php $this->beginPage(); 
	$user = User::findOne(Yii::$app->user->Id);
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Leading Edge Computers - Business to Business Portal</title>
	<meta name="description" content="Leading Edge Computers" />
	<meta name="keywords" content="Leading Edge Computers" />
	<meta name="robots" content="INDEX,FOLLOW" />
	<meta name="viewport" content="width=1030, user-scalable=yes" />
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<link rel="icon" href="/themes/b2b/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/themes/b2b/images/favicon.ico" type="image/x-icon" />
	<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>  
	<!--<script type="text/javascript" src="/js/jquery.gritter.min.js"></script>-->
  	<script type="text/javascript" src="/js/owl.carousel.min.js"></script>
	
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="/themes/b2b/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />  -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/themes/b2b/css/style.css?ic" media="all" />
	<link rel="stylesheet" type="text/css" href="/themes/b2b/css/style1.css?ic" media="all" />
  	<link rel="stylesheet" type="text/css" href="/themes/b2b/css/jquery.mCustomScrollbar.css" media="all" />
	<link rel="stylesheet" type="text/css" href="/themes/b2b/css/jquery-ui-1.8.21.custom.css" media="all" />
	<link href="/css/ion.rangeSlider.css" rel="stylesheet" type="text/css">
    <link href="/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css">
    <link href="/css/normalize.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/css/jquery.gritter.css" />

	<!--<script type="text/javascript" src="themes/b2b/js/script.js"></script>-->  
	<script type="text/javascript" src="/js/ajax-update.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script src="/js/ion.rangeSlider.js"></script>
	<script type="text/javascript" src="/js/wl_Form.js"></script>  
	 <script type="text/javascript" src="/js/jquery.gritter.min.js"></script>
	<script type="text/javascript" src="/js/scripts.js"></script>


	<!--<script src="js/jquery-1.9.1.min.js"></script> -->

	
	<script type="text/javascript">
		function validheadersearch()
		{
			var senames = document.getElementById('mainserach').value;
			if(senames=="")
			{
			   alert('search field cannot be empty !!');
		       return false;
			}
		}
	</script>
	
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('span:contains("Catalogues")').parent().attr("href", "javascript:;");
		})
	</script>
	  <?php $this->head() ?>
</head>

<body class=" dashboard-index-index">

<?php $this->beginBody() ?>

<div class="header">
    <noscript>
        <div class="noscript">
            <div class="noscript-inner">
                <p><strong>JavaScript seem to be disabled in your browser.</strong></p>
                <p>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
	<div class="header-container">
	    <div class="logo">
	        <a href="<?=Yii::$app->params['b2bUrl']?>" title="Leading Edge Computers"><img src="/themes/b2b/images/logo.png" alt="Leading Edge Computers" /></a>
	    </div>
		<div class="topsec">
			<div class="shopcart"><a href="<?=\yii\helpers\Url::to(['cart/view'])?>"><?=\frontend\components\Helper::money(Yii::$app->cart->cost)?></a><div class="clear"></div></div>
			<div class="acc-area">
				<div class="links">
					<ul>
						<li>Welcome <?=$user->Fullname?></li>
						<li>|</li>
						<li><a href="<?=\yii\helpers\Url::to(['account/index'])?>">My Account</li>
						<li>|</li>
						<li><a href="<?=\yii\helpers\Url::to(['default/logout'])?>">Logout</a></li>
					</ul>
				</div>
				<div class="topsearch">
					<form id="search_mini_form" onsubmit="return validheadersearch();" action="/b2b/default/search" method="get">
						<input type="text" placeholder="Search here..." value="" id="mainserach" name="q"/>
						<input style="cursor: pointer;" type="submit" value="" class="mainsearchbutton"/>
						<div class="clear"></div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<!-- <div id="navigation">
			 	<!-- <ul id="nav"> -->
					<?php //B2bMainMenu::widget([]);
	            
	        ?>	
				<!-- </ul>  
			<div class="clear"></div>
		</div> -->
		<div id="navigation">
			<div class="dropdown"> <!--catr-menus-->
	          <a data-toggle="dropdown" class="cat-wrapper dropdown-toggle"> CATEGORIES <i class="fa fa-bars"></i></a>
	          <div id="main-menu" class="sub-navigation">
	              <?php echo B2bMainMenu::widget([])?>
	           </div>
	        </div>
			<div class="navigation ">
	             <?=B2bHeaderMenu::widget([ ]);?>
	        </div>
	    </div>    
	</div><!--header-container-->
</div><!--header-->

<div class="containers"> 
<div class="bfh-slider" data-name="slider3" data-min="5" data-max="25">
</div>
	<?= skinka\widgets\gritter\AlertGritterWidget::widget() ?> 

    <?= $content ?>
    <div class="modal-dialog" id="dialog" style="display:none;">
       	<b>Warning - Next Page</b><br/>
           <span>The selected items have not been added to cart. Click ADD TO CART to order or CONTINUE to discard selections and proceed to next page.</span><br/>
            
            	<div class="dialog-buttons">  
					<button type="button" class="btn btn-default" id="add_tocart">Add To Cart</button>
					<a href="">
						<button type="button" class="btn btn-default" id="continue">Continue</button>
					</a>
                	<button type="button" class="btn btn-default" id="cancel" >Cancel</button>
				</div> 	
        
    </div>
</div> 

<div class="footerarea">
	<div class="footer-container">
		<div class="footer">
			<ul>
				<li><a href="<?=\yii\helpers\Url::to(['account/index'])?>">My Account</a></li>
				<li><a href="<?=\yii\helpers\Url::to(['account/orders'])?>">Order History</a></li>
				<li><a href="<?=\yii\helpers\Url::to(['default/contact'])?>">Contact Us</a></li>
				<li><a href="<?=\yii\helpers\Url::to(['cart/saved'])?>">QUICK LIST</a></li>
			</ul>      
		</div>
		<div class="copyright">&copy; <?=date('Y')?> Leading Edge Computers. All Rights Reserved.</div>
		<div class="clear"></div>
	</div>

<script type="text/javascript">
	/*jQuery("#nav li").each(function() {
		var href = jQuery(this).find('a').attr('href');
		var cur = 'http://www.orders.legj.com.au/dashboard/';
		if(href==cur)
		{
			//alert(href);
			jQuery(this).addClass('active');
		}
	});*/


	function minprice(id,max,min)
	{
	    var num = jQuery(id).val();
	    chknumber(id);
	    if(num<min)
	    {
	        if(num!='')
	        {
	            alert("Minimum order quantity should be "+min);
	            jQuery(id).val(min);
	        }
	    }
	}
                    
	function maxprice(id,max,min)
	{
	    var num = jQuery(id).val();
	    chknumber(id);
	    
	    if(num>max)
	    {
	        if(num!='')
	        {
	            alert("Please order below "+max+" quantity.");
	            jQuery(id).val(max);
	        }
	    }
	    if(num<min)
	    {
	        if(num!='')
	        {
	            alert("Minimum order quantity should be "+min);
	            jQuery(id).val(min);
	        }
	    }
	}

	function chknumber(id)
	{
	    var item = jQuery(id).val();
	    //alert(item);
	    jQuery(id).onkeyup = function() {
	        var key = event.keyCode || event.charCode;
	        //alert(key);
	        if( key == 8 || key == 46 )
	            return false;
	    };
	    intRegex = /^[0-9]+$/;
	 	if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
	 	{
	        if(item!='')
	        {
	 	        alert('Please enter a valid quantity');
	            jQuery(id).val('1');
	        }
	        
	        return false;
	 	}
	}

	function totopslow()
	{
		window.scrollTo(0, 0);
	}


</script>

	<script type="text/javascript">
        $(document).ready(function() {
          $("#main-menu > ul").addClass("megamenu");
		      $("#main-menu > ul").addClass("dropdown-menu");
            //$( "ul" ).addClass( "cat-nav" );
            $('ul').not(':has(li)').remove();
            setTimeout(function(){ $("ul.pagination").wrap("<div class='col-xs-12 text-center'></div>")}, 1000);
        });
    </script>

	<script type="text/javascript">
        $(document).ready(function() {
            $( "#main-menu > ul.dropdown-menu > li > ul#nav > li" ).each(function() {
              	if($(this).find('ul').length > 0){ 
                  $(this).find('a:first').addClass('triger-wrap');
                  $(this).find('ul:first').addClass('sub-wrap'); 
                }
            });

            $(".triger-wrap").click(function(event){  
                event.preventDefault();
                if($(this).parent().hasClass('active')){
                   $(this).parent().find('.sub-wrap').toggle();
                   $(this).toggleClass('rotate');
                }
                else {
                  $('.sub-wrap').css('display','none'); 
                  $(this).parent().find('.sub-wrap').toggle();  
                }
                $(".triger-wrap").parent().removeClass('active');
                $(this).parent().addClass('active');
            });  
         });
    </script>  

    <script>
		$(function(){
    		$(".dropdown").hover(            
	            function() {
	                $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
	                $(this).toggleClass('open');
	            },
	            function() {
	                $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
	                $(this).toggleClass('open');
	            }
	        );
    	});
	</script> 

</div><!--footerarea-->

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"fcca5ccf06","applicationID":"4405536","transactionName":"b1dWMkUAWUJXUhddW1YddQVDCFhfGVUCR1xaXVUUU05eX1JUGxtdVlZRHg==","queueTime":0,"applicationTime":1334,"atts":"QxBVRA0aSkw=","errorBeacon":"bam.nr-data.net","agent":"js-agent.newrelic.com\/nr-632.min.js"}
</script>

<?php 
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody() 
?>

</body>
</html>

<?php $this->endPage() ?>

