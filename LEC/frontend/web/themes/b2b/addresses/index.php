<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$user = User::findOne(Yii::$app->user->Id);
?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="col-main">
			
			<?= Yii::$app->controller->renderPartial('_form',compact('address','user')); ?>

			<br>
			<h3>Address book</h3>
			
			<p>The default shipping and billing address is shaded grey.</p>

			<table class="data-table" border="2">
				<thead>
					<tr>
						<th>User name</th>
						<th>Store Name</th>
						<th>Street Address</th>
						<th>City</th>
						<th>State</th>
						<th>Country</th>						
						<th>Post Code</th>
						<th>Phone</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach ($user->b2bAddresses as $address) { //var_dump($address->billingAddress);die(); ?>
					<tr <?=($address->defaultAddress == "1")? 'style="background: #ededed;border-left:red 2px solid"' : ''?>>
						<td><?=$user->fullname?></td>
						<td><?=$address->storeName?></td>
						<td><?=$address->streetAddress?></td>
						<td><?=$address->city?></td>
						<td><?=$address->state?></td>
						<td><?=$address->country?></td>
						<td><?=$address->postCode?></td>
						<td><?=$address->phone?></td>
						<td>
						<a href="<?=\yii\helpers\Url::to(['addresses/update', 'id'=>$address->id]) ?>" style="display: block;font-weight: bold;">Edit Address</a>
						<?php if($address->defaultAddress != "1") { ?>
						<a href="<?=\yii\helpers\Url::to(['addresses/delete', 'id'=>$address->id]) ?>" onclick="return confirm('Are you sure you want to remove this address?')" style="display: block;font-weight: bold;">Delete</a>
						<?php } ?> </td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<!--<div class="rightside">
		    <div class="block block-account">
		        <div class="block-title">
		            <span>My Account</span>
		        </div>
		        <div class="block-content">
		            <ul>
		                <li><a href="<?=\yii\helpers\Url::to(['account/index']) ?>">Account Dashboard</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>">Address Book</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/orders']) ?>">My Orders</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['cart/saved']) ?>">Saved Cart Lists</a></li>
		               	<li><a href="<?=Yii::$app->params['b2csignupurl']?>" target="_blank">Sign Up for Website</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/changepassword']) ?>">Change Password</a></li>
		                <li class="last"><a href="<?=\yii\helpers\Url::to(['default/logout']) ?>">Logout</a></li>
		            </ul>
		        </div>
		    </div>
		</div>-->
		<div class="clear"></div>
	</div>
</div>		
