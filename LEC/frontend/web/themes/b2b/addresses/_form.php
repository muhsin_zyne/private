<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use kartik\widgets\TimePicker;

?>
<?php //var_dump($user->id);die(); 
	$checkOptions = ($address->defaultAddress=="1")?  ['disabled' => 'disabled'] : [];
?>
<div class="leftside">
	<h2>Address Book</h2>
	<p> Please fill out the fields below to get them activated in your ordering platform </p>

	<?php $form = ActiveForm::begin(['action' => (Yii::$app->controller->action->id == "update")? "" : Url::to(['addresses/create'])]); ?>
	<?= $form->field($address, 'ownerId')->hiddenInput(['value' => $user->id])->label('') ?>
	<div class="box-add">
		<?= $form->field($address, 'storeName')->textInput() ?>				
	</div>
	
	<div class="box-add">
		<?= $form->field($address, 'streetAddress')->textInput() ?>
	</div>

	<div class="box-add">				
		<?= $form->field($address, 'city')->textInput() ?>
	</div>
	<!--<div class="box-add">
		<?= $form->field($address, 'tradingHours')->textarea(['rows' => 4]) ?>
	</div>-->
	
	<div class="box-add">
		<?=$form->field($address, 'state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])?>
	</div>
	<div class="box-add">
		<?=$form->field($address, 'country')->dropDownList(['Australia'=>'Australia'], ['prompt'=>'--Select--'])?>
	</div>
	<div class="box-add">
		<?=$form->field($address, 'postCode')->textInput(['maxlength' => 255])?>
	</div>
	<div class="box-add">
		<?=$form->field($address, 'phone')->textInput(['maxlength' => 255])?>
	</div>
	<div class="box-add">
		<?= $form->field($address, 'latitude')->textInput(['maxlength' => 255]) ?>
	</div>
	<div class="box-add">
		<?= $form->field($address, 'longitude')->textInput(['maxlength' => 255]) ?>
	</div>

	<div class="box-add">
		<input type = "hidden" name = "B2bAddresses[defaultAddress]" value = <?=(($address->defaultAddress=="1")? "'1'" : "'0'")?>/>
		<?= $form->field($address, 'defaultAddress')->checkbox(array_merge($checkOptions, ['uncheck' => null])) ?>
	</div>	
	<div class="clear"></div>
	<p>If you are not sure about your Latitude and Longitude, you may use a third party service like <a href="http://itouchmap.com/latlong.html"> "http://itouchmap.com/latlong.html" </a> to find the values.</p>
</div>
<div class="rightside">
    <div class="block block-account">
        <div class="block-title">
            <span>My Account</span>
        </div>
        <div class="block-content">
            <ul>
                <li><a href="<?=\yii\helpers\Url::to(['account/index']) ?>">Account Dashboard</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>">Address Book</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['account/orders']) ?>">My Orders</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['cart/saved']) ?>">Saved Cart Lists</a></li>
               	<li><a href="<?=Yii::$app->params['b2csignupurl']?>" target="_blank">Sign Up for Website</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['account/changepassword']) ?>">Change Password</a></li>
                <li class="last"><a href="<?=\yii\helpers\Url::to(['default/logout']) ?>">Logout</a></li>
            </ul>
        </div>
    </div>
</div>			
<?php if($address->isNewRecord){
    $address->mondayFrom='08:00 AM'; $address->mondayTo='5:00 PM';
    $address->tuesdayFrom='08:00 AM'; $address->tuesdayTo='5:00 PM';
    $address->wednesdayFrom='08:00 AM'; $address->wednesdayTo='5:00 PM';
    $address->thursdayFrom='08:00 AM'; $address->thursdayTo='5:00 PM';
    $address->fridayFrom='08:00 AM'; $address->fridayTo='5:00 PM';
    $address->saturdayFrom='08:00 AM'; $address->saturdayTo='5:00 PM';
    $address->sundayFrom='00:00 AM'; $address->sundayTo='00:00 PM';
} else {
    //var_dump($address->b2bTradingHoursFormatted);die;
    $address->mondayFrom=isset($address->b2bTradingHoursFormatted['0']['monday']['from']) ? $address->b2bTradingHoursFormatted['0']['monday']['from'] :'';
    $address->mondayTo=isset($address->b2bTradingHoursFormatted['0']['monday']['to']) ? $address->b2bTradingHoursFormatted['0']['monday']['to'] :'';
    $address->tuesdayFrom=isset($address->b2bTradingHoursFormatted['0']['tuesday']['from']) ? $address->b2bTradingHoursFormatted['0']['tuesday']['from'] : '' ;
    $address->tuesdayTo=isset($address->b2bTradingHoursFormatted['0']['tuesday']['to']) ? $address->b2bTradingHoursFormatted['0']['tuesday']['to'] : '';
    $address->wednesdayFrom=isset($address->b2bTradingHoursFormatted['0']['wednesday']['from']) ? $address->b2bTradingHoursFormatted['0']['wednesday']['from'] : ''; 
    $address->wednesdayTo=isset($address->b2bTradingHoursFormatted['0']['wednesday']['to']) ? $address->b2bTradingHoursFormatted['0']['wednesday']['to'] : '';
    $address->thursdayFrom=isset($address->b2bTradingHoursFormatted['0']['thursday']['from']) ? $address->b2bTradingHoursFormatted['0']['thursday']['from'] : ''; 
    $address->thursdayTo=isset($address->b2bTradingHoursFormatted['0']['thursday']['to']) ? $address->b2bTradingHoursFormatted['0']['thursday']['to'] : '';
    $address->fridayFrom=isset($address->b2bTradingHoursFormatted['0']['friday']['from']) ? $address->b2bTradingHoursFormatted['0']['friday']['from']: ''; 
    $address->fridayTo=isset($address->b2bTradingHoursFormatted['0']['friday']['to']) ? $address->b2bTradingHoursFormatted['0']['friday']['to'] : '';
    $address->saturdayFrom=isset($address->b2bTradingHoursFormatted['0']['saturday']['from']) ? $address->b2bTradingHoursFormatted['0']['saturday']['from'] : ''; 
    $address->saturdayTo=isset($address->b2bTradingHoursFormatted['0']['saturday']['to']) ? $address->b2bTradingHoursFormatted['0']['saturday']['to'] :'';
    $address->sundayFrom=isset($address->b2bTradingHoursFormatted['0']['sunday']['from']) ? $address->b2bTradingHoursFormatted['0']['sunday']['from'] : ''; 
    $address->sundayTo=isset($address->b2bTradingHoursFormatted['0']['sunday']['to']) ? $address->b2bTradingHoursFormatted['0']['sunday']['to'] : '';
} ?>
<div class="clear"></div>
<div class="row"> 
<div class="store-trading">

<h1><b>Store Trading hours</b></h1> 
	<table>
	    <tr>
	        <td><span class="f-chk-mon"><?= $form->field($address, 'mondayFrom')->widget(TimePicker::classname(), []);?></span></td> 
	        <td><span class="f-chk-tue"><?= $form->field($address, 'tuesdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
	        <td><span class="f-chk-wed"><?= $form->field($address, 'wednesdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
	        <td><span class="f-chk-thu"><?= $form->field($address, 'thursdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
	        <td><span class="f-chk-fri"><?= $form->field($address, 'fridayFrom')->widget(TimePicker::classname(), []);?></span></td> 
	        <td><span class="f-chk-sat"><?= $form->field($address, 'saturdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
	        <td><span class="f-chk-sun"><?= $form->field($address, 'sundayFrom')->widget(TimePicker::classname(), []);?></span></td>                   
	    </tr>
	    <tr>
	        <td align="center"><span>to</span></td>
	        <td align="center"><span>to</span></td>
	        <td align="center"><span>to</span></td>
	        <td align="center"><span>to</span></td>
	        <td align="center"><span>to</span></td>
	        <td align="center"><span>to</span></td>
	        <td align="center"><span>to</span></td>	        
	    </tr>
	    <tr>                  
	        <td><span class="f-chk-mon"><?= $form->field($address, 'mondayTo')->widget(TimePicker::classname(), []);?></span></td>
	        <td><span class="f-chk-tue"><?= $form->field($address, 'tuesdayTo')->widget(TimePicker::classname(), []);?></span></td>
	        <td><span class="f-chk-wed"><?= $form->field($address, 'wednesdayTo')->widget(TimePicker::classname(), []);?></span></td>
	        <td><span class="f-chk-thu"><?= $form->field($address, 'thursdayTo')->widget(TimePicker::classname(), []);?></span></td>
	        <td><span class="f-chk-fri"><?= $form->field($address, 'fridayTo')->widget(TimePicker::classname(), []);?></span></td>
	        <td><span class="f-chk-sat"><?= $form->field($address, 'saturdayTo')->widget(TimePicker::classname(), []);?></span></td>
	        <td><span class="f-chk-sun" ><?= $form->field($address, 'sundayTo')->widget(TimePicker::classname(), []);?></span></td>                    
	    </tr>
	    <tr>
	        <td><span><input type="checkbox" name="B2bAddresses[chkMon]" class="chk-closed" id="chk-mon">Monday Closed</span></td>
	        <td><span><input type="checkbox" name="B2bAddresses[chkTue]" class="chk-closed" id="chk-tue">Tuesday Closed</span></td>
	        <td><span><input type="checkbox" name="B2bAddresses[chkWed]" class="chk-closed" id="chk-wed">Wednesday Closed</span></td>
	        <td><span><input type="checkbox" name="B2bAddresses[chkThu]" class="chk-closed" id="chk-thu">Thursday Closed</span></td>
	        <td><span><input type="checkbox" name="B2bAddresses[chkFri]" class="chk-closed" id="chk-fri">Friday Closed</span></td>
	        <td><span><input type="checkbox" name="B2bAddresses[chkSat]" class="chk-closed" id="chk-sat">Saturday Closed</span></td>
	        <td><span><input type="checkbox" name="B2bAddresses[chkSun]" class="chk-closed" id="chk-sun">Sunday Closed</span></td>
	    </tr>
	</table>
</div>
</div>
<div class="clear"></div>

<?= Html::submitButton($address->isNewRecord ? 'Add Address' : 'Update', ['class' => $address->isNewRecord ? 'button' : 'button']) ?>



<!-- <input class="button" type="submit" value="Add Address" name="addnew"> -->


<?php ActiveForm::end(); ?>
<script type="text/javascript">
    $(document).ready(function(){ 
        <?php if(!$address->isNewRecord){            
            if($address->mondayFrom=='closed'){ ?>
                $('#chk-mon').click(); $('.f-chk-mon').hide();
            <?php } ?>
            <?php if($address->tuesdayFrom=='closed') {?>
                $('#chk-tue').click(); $('.f-chk-tue').hide();
            <?php } ?>
            <?php if($address->wednesdayFrom=='closed') {?>
                $('#chk-wed').click(); $('.f-chk-wed').hide();
            <?php } ?>
            <?php if($address->thursdayFrom=='closed') {?>
                $('#chk-thu').click(); $('.f-chk-thu').hide();
            <?php } ?>
            <?php if($address->fridayFrom=='closed') { ?>
                $('#chk-fri').click(); $('.f-chk-fri').hide();
            <?php } ?>
            <?php if($address->saturdayFrom=='closed') { ?>
                $('#chk-sat').click(); $('.f-chk-sat').hide();
            <?php }?>
            <?php if($address->sundayFrom=='closed') { ?>
                $('#chk-sun').click(); $('.f-chk-sun').hide();
        <?php } } ?>
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){     
	    $('.chk-closed').on('change', function() { 
	    	if($(this).is(':checked')){
                $('.f-'+this.id).hide();
            } else {
                $('.f-'+this.id).show();
            }
	    });       
    });
</script>