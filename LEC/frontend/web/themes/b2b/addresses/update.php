<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
$user = User::findOne(Yii::$app->user->Id);
?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="col-main">
			<?= Yii::$app->controller->renderPartial('_form',compact('address','user')); ?>
		</div>
		<!--<div class="rightside">
		    <div class="block block-account">
		        <div class="block-title">
		            <span>My Account</span>
		        </div>
		        <div class="block-content">
		            <ul>
		                <li><a href="<?=\yii\helpers\Url::to(['account/index']) ?>">Account Dashboard</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>">Address Book</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/orders']) ?>">My Orders</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['cart/saved']) ?>">Saved cart lists</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/changepassword']) ?>">Change password</a></li>
		                <li class="last"><a href="<?=\yii\helpers\Url::to(['default/logout']) ?>">Logout</a></li>
		            </ul>
		        </div>
		    </div>
		</div>-->
		<div class="clear"></div>
	</div>
</div>		