<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use frontend\components\Helper;
?>

<?php
$user = User::findOne(Yii::$app->user->Id);
//var_dump($user->store->id);die();
//var_dump($promotions);die();
/*foreach ($promotions as $promotion) {
	var_dump($promotion->title);die();
}*/
//
?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="col-main leftside">
			
			<h2>Opt in to a Promotion</h2>
			
			<?= GridView::widget([
    			'dataProvider' => $dataProvider,
    			'class'=>'data-table',
        		'columns' => [
        				'title',
        				[
        				'attribute'=>'fromDate',
        				'label'=>'Start Date',
        				'value' => function($model,$attribute){
        					return Helper::date($model->fromDate);
        				}
        				],
        				[
        				'attribute'=>'toDate',
        				'label'=>'End Date',
        				'value' => function($model,$attribute){
        					return Helper::date($model->toDate);
        				}
        				],
        				'shortTag',
        		['class' => 'yii\grid\ActionColumn','template' => '{view}']]
    		]); ?>

		</div>
		<div class="rightside">
		    <div class="block block-account">
		        <div class="block-title">
		            <span>My Account</span>
		        </div>
		        <div class="block-content">
		            <ul>
		                <li><a href="">Account Dashboard</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>">Address Book</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/orders']) ?>">My Orders</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/cartlist']) ?>">Saved cart lists</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['promotions/index']) ?>">Opt in to a Promotion</a></li>
		                <li><a href="<?=\yii\helpers\Url::to(['account/changepassword']) ?>">Change password</a></li>
		                <li class="last"><a href="<?=\yii\helpers\Url::to(['account/logout']) ?>">Logout</a></li>
		            </ul>
		        </div>
		    </div>
		</div>
		<div class="clear"></div>
	</div>
</div>

