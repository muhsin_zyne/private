<?php

//use Yii;
use common\models\User;
use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\components\Helper;
use kartik\date\DatePicker;
use common\models\StorePromotions;
use yii\grid\GridView;
use yii\widgets\DetailView;
?>

<?php
$user = User::findOne(Yii::$app->user->Id);

 ?>

<div class="mainarea">
	<div class="wrapper_inner">
		<div class="promotion">
			<h2><?=$promotion->title?></h2>
			<div class="date_offer">
				<div class="offer">
					<b><span>Promotion Period:</span></b> <span><?=Helper::date($promotion->fromDate);?></span> <b>to</b> <span><?=Helper::date($promotion->toDate);?></span>
				</div>
				<div class="offer">
					<b><span>Buying Period:</span></b> <span><?=Helper::date($promotion->fromDate);?></span> <b>to</b> <span><?=Helper::date($promotion->costEndDate);?></span>
				</div>
				<div class="clear"></div>
			</div>
			<p>Leading Edge Jewellers has created a promotion, selling some of the products at a discounted price to boost sales. You may accept or reject this by clicking on the buttons. You can see the list of all products below.</p>

			<p>If you decide to Accept this promotion, these products will have the revised price from <?=Helper::date($promotion->fromDate);?>. At 12:01 am <?=Helper::date($promotion->toDate);?>, the price will revert back to normal.</p>

			<?= GridView::widget([
    			'dataProvider' => $dataProvider,
        		'columns' => ['pageId',['attribute'=>'product.name','label'=>'Item(s)'],'product.sku',['attribute'=>'product.price','label'=>'Original Selling Price'],'offerSellPrice',['attribute'=>'product.cost_price','label'=>'Original Cost Price'],'offerCostPrice'],
    		]); ?>

			<div class="cost_html">
				<b>Please set the selling period. (<span style="font-size: 11px;">required</span>)</b>
				<?php $form = ActiveForm::begin(['action'=>Url::to(['promotions/accept','id'=>$promotion->id])]); ?>
					
					<label>From Date</label>
					<input type="text" name="StorePromotion[startDate]" id="datestart" class="hasDatepicker startdate">
					<label>End Date</label>
					<input type="text" name="StorePromotion[endDate]" id="dateend" class="hasDatepicker enddate">
				<div class="clear"></div>
			</div>

			<div class="btnarea">
				<input type="checkbox" class="agree" id="agree" name="StorePromotion[agree]"> I agree to the terms and conditions above.
				<input type="submit" value="I Accept This Promotion" name="accept" class="button accept">
				<a href="<?=\yii\helpers\Url::to(['promotions/index']) ?>">
					<input type="button" value="Not Interested In This Promotion" name="reject" class="button reject">
				</a>	
				<div class="clear"></div>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
	</div>
</div>		

<script type="text/javascript">
    $(document).ready(function () {
        $('#datestart').datepicker({
            format: "dd/mm/yyyy"
        }); 

    });
</script>


<script type="text/javascript">
    $(document).ready(function () {
		$('#dateend').datepicker({
            format: "dd/mm/yyyy",
            startDate: '2015-06-01',
        });  
    });
</script>    		

<script type="text/javascript">
	$(document).ready(function () {
		$('.accept').on("click", function(){   
			if($('.agree').prop( "checked" )){//return true;
				//alert('checked');
				return true;
			}
			else{ 
				alert('Please agree terms and conditions');
				return false;
			}
		});	
	});	
</script>