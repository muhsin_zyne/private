<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<?php
    /*$date = date_create();
    echo date_format($date, 'Y-m-d H:i:s') . "\n";*/
?>

<div class="mainarea">
    <div class="wrapper_inner">
    	<div class="col-left">
        	<?php
                    ProductFilter::begin([
                        'category' => $category,
                        'attributes' => ['color', 'size','material']
                    ]);
                    ProductFilter::end();
                ?>
        </div>
        <div class="col-main col-right Product_Featured">
            <h2 class="">Products</h2>
                <div class="area_filter">
                    <?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => \yii\helpers\Url::to(['cart/add'])]); ?>
                    <input type="hidden" class="productsids" name="Products[id]"/>
                    <div id="type_view"></div>
                    <div class="flit_search">
                    <input type="text" class="search" name="query" value="<?=isset($_REQUEST['keyword'])? $_REQUEST['keyword'] : ''?>" placeholder="Search by SKU, EzCode, Title" />
                    <input type="button" name="filter_search" value="Search" class="button search-button" onclick="validsearch()">
                    </div>
                    <div class="view">
                    <span>View</span>
                    <a class="grid" href="javascript:void(0);" onclick="">Grid</a>
                    <a class="list active" href="javascript:void(0);" onclick="">List</a>
                    <div class="clear"></div>
                </div>
                    <div class="filt_select">
                    <select name="filter_value" onchange="jQuery('#filter_form').submit();">
                    <option value="">Sort by</option>
                    <option  value="price_low">Price (Low to High)</option>
                    <option  value="price_high">Price (High to Low)</option> -->
                    <!--<option  value="name">Name</option>-->
                   <option  value="sku">SKU</option>
                    <option  value="ezcode">EzCode</option>
                    </select>
                    </div>
                    
                    <div class="clear"></div>
                    </form>
                    
                </div>
                
                <div class="clear"></div>
                <?php $form = ActiveForm::begin(['method' => 'POST', 'action' => Url::to(['cart/add']), 'id' => 'list_form_pro']); ?>
                    <input type="hidden" name="form_key" value="ItAThARoVgdWrAGq" />
                    <div class="product_list">
                        <div id="checkall">
                            <a href="javascript:void(0);" onclick="selectvisible()">Select visible</a>
                            <a href="javascript:void(0);" onclick="unselectvisible()">Unselect visible</a>
                        </div>

                <ol>
                    <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                    <?php 
                        echo ListView::widget( [
                        'id' => 'testclass',
                        'dataProvider' => $products,
                        'itemView' => '/categories/_product',
                        'summary' => '',
                        'itemOptions' => ['class' => 'items col-md-4']
                        ] );
                    ?>
                    <?php  \yii\widgets\Pjax::end();  ?> 
                </ol>
                <div id="noresults"></div>
                <div class="animation_image" style="display:none" align="center">
                    <img src="http://www.orders.legj.com.au/skin/frontend/default/b2b/images/ajax-loader.gif"/>
                </div>
                </div>
                <div id="floatdiv">
                    <input type="submit" class="button list_bttn order_now" name="order" value="Order" />
                    <div class="clear"></div>
                </div>
            <?php ActiveForm::end(); ?>


<!-- GRID LOAD HERE -->
    <div class="clear"></div>
</div>
	<div class="clear"></div>	
</div><!-- end .wrapper-->
</div><!--mainarea-->



<script type="text/javascript">
function validsearch(){
    var sename = $('.search').val();
    if(sename=="")
    {
       alert('search field cannot be empty !!')
       return false;
    }
    else{
        //$('.col-left').append('<input type="text" name="Products[keyword]" value='+sename+'>');
        $('.keyword').val(sename);
        filterproducts();
    }    
}
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.product_slider .item:first-child').addClass('active');
    $(".order_now").click(function() {
        //$(this).prev().addClass('hai');
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required')
          return false;
        }  
    }); 
  });  
</script>

<script type="text/javascript">
$(".grid").click(function() {
    $("#category-products #testclass").removeClass('list-view');
    $("#category-products #testclass").addClass('grid-product');
});
</script>

<script type="text/javascript">
$(".list").click(function() {
    $("#category-products #testclass").addClass('list-view');
    $("#category-products #testclass").removeClass('grid-product');
});
</script>

<script>
function chk_validaion()
{
    if(jQuery("#list_form_pro .chkbx:checked").length==0)
    {
       alert("Please select atleast one product to continue");
       return false;
    }
}
function selectvisible()
{
    jQuery('#results .chkbx').attr('checked', 'checked');
}

function unselectvisible()
{
    jQuery('#results .chkbx').removeAttr('checked');
}
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('.chkbx').change(function(){
        $('#product_ids').val()
    })
})
</script>

<script type="text/javascript">
jQuery("#nav li").each(function() {
    var href = jQuery(this).find('a').attr('href');
    var cur = 'http://www.orders.legj.com.au/dashboard/';
    if(href==cur)
    {
        //alert(href);
        jQuery(this).addClass('active');
    }
});


function minprice(id,max,min)
{
    var num = jQuery(id).val();
    chknumber(id);
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}
                    
function maxprice(id,max,min)  
{
    var num = jQuery(id).val(); 
    chknumber(id);
    
    if(num>max)
    {
        if(num!='')
        {
            alert("Please order below "+max+" quantity.");
            jQuery(id).val(max);
        }
    }
    if(num<min)
    {
        if(num!='')
        {
            alert("Minimum order quantity should be "+min);
            jQuery(id).val(min);
        }
    }
}

function chknumber(id)
{
    var item = jQuery(id).val();
    //alert(item);
    jQuery(id).onkeyup = function() {
        var key = event.keyCode || event.charCode;
        //alert(key);
        if( key == 8 || key == 46 )
            return false;
    };
    intRegex = /^[0-9]+$/;
    if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
    {
        if(item!='')
        {
            alert('Please enter a valid quantity');
            jQuery(id).val('1');
        }
        
        return false;  
    }
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".chkbx").change(function() {
            if(this.checked) {
                 $('.productsids').val($(this).val());
            }
        });
    });    
</script>

