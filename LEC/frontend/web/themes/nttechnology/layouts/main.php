<?php  //test
    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use frontend\assets\AppAsset;
    use frontend\widgets\Alert;
    use frontend\components\FrontNavBar;
    use frontend\components\MainMenu;
    use frontend\components\HeaderMenu;
    use frontend\components\FooterMenu;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use common\models\Stores;
    use common\models\Categories;   
    use frontend\components\Helper; 
    AppAsset::register($this);
        
?>
<?php
    $userId=Yii::$app->user->id;
    $storeId=Yii::$app->params['storeId'];
    $store = Stores::findOne(Yii::$app->params['storeId']);
    $facebookUrl = $store->getfacebookUrl();
    $youtubeUrl = $store->getyoutubeUrl();
    $linkedinUrl = $store->getlinkedinUrl();
    $googleplusUrl = $store->getgoogleplusUrl();
    $twitterUrl = $store->gettwitterUrl();
    $instagramUrl = $store->getinstagramUrl();
    $pinterestUrl = $store->getpinterestUrl();
        
?>
<?php $this->beginPage() ?>
<!doctype html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="title" content="<?= Yii::$app->controller->metaTitle?>" />
    <meta name="keywords" content="<?= Yii::$app->controller->metaKeywords?>" />
    <meta name="description" content="<?= Yii::$app->controller->metaDescription?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=1200">
    <link rel="icon" href="/themes/nttechnology/images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/themes/nttechnology/images/favicon.ico" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->controller->metaTitle?></title>
    <?php $this->head() ?>
    <?php include 'scripts.php';?> <!-- All css and js included -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-86738421-19', 'auto');
      ga('send', 'pageview');

    </script>
    
</head>
<body>
    <?php $this->beginBody() ?>
<?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>
                <a href="<?=\yii\helpers\Url::to(['byod/logout'])?>" class="logout-fix" title="Logout from BYOD"><i class="fa fa-sign-out"></i><span>Logout <br/>from BYOD</span></a>
        <?php } ?>
    <div class="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-5 top-menu"> 

                        <?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>  
                          <a href="<?=\yii\helpers\Url::to(['byod/view'])?>" style="margin-right:20px;"  title="BYOD" class="top-link-byod"> BYOD</i></a>
                         <?php } else {  ?>
                          <a href="<?=\yii\helpers\Url::to(['byod/apply'])?>" title="BYOD" style="margin-right:20px;" class="top-link-byod bootstrap-modal" data-complete="window.location = '/byod/view';" data-parameters='{"noFlash":"true"}'> BYOD</i></a>
                        <?php } ?>

                        <a href="<?=\yii\helpers\Url::to(['checkout/index'])?>" title="Checkout" class="top-link-checkout"> <i class="fa fa-sign-in"></i>Checkout</i></a>
                        <?php if (!\Yii::$app->user->isGuest) { ?>
                            <a href="<?=\yii\helpers\Url::to(['/site/wishlist'])?>" title="View Cart" class="top-link-cart"> <i class="fa fa-heart"></i>Wish List <span>(
                            <?php $n=count(common\models\WishlistItems::find()->where(['storeId'=>$storeId,'userId'=>$userId])->all()); echo $n;?> )</span></a> 
                 
                            <a href="<?=\yii\helpers\Url::to(['site/account'])?>" title="My Account" ><i class="fa fa-user"></i>My Account</a>
                            <a href="<?=Yii::$app->urlManager->createUrl(['site/logout']) ?>" title="Log In" ><i class="fa fa-sign-out"></i>Log Out</a> 
                        <?php } else { ?>
                            <a href="<?=\yii\helpers\Url::to(['site/signup']) ?>" title="Register" ><i class="fa fa-key"></i> Register</a> 
                            <a href="<?=\yii\helpers\Url::to(['site/login'])?>" title="Log In" ><i class="fa fa-lock"></i>Login</a>
                        <?php } ?>
                         
                    </div>
                    <div class="col-xs-7 topcontact"> 
                        <span class="email"><i class="fa fa-envelope"></i> <?=$store->email?></span> 
                        <span><i class="fa fa-phone"></i> 1300 NTTECH(<?= $store->phone?>)</span> 
                        <span>   
                            <div class="dropdown"> 
                            <a class="dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <i class="fa fa-shopping-bag"></i>                             
                             <?php $n=count(Yii::$app->cart->positions); echo $n.' Item(s) - AU$'.Yii::$app->cart->cost?>                       
                            <span class="fa fa-angle-down "></span>
                            </a>                        
                            <div class="dropdown-menu cart-drop"> 
                            <a href="<?=\yii\helpers\Url::to(['cart/view'])?>">ViewCart</a> 
                            <a href="<?=\yii\helpers\Url::to(['checkout/index'])?>">Checkout</a>
                            </div>
                        </div></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-middle">
            <div class="container">
                <div class="logo"> <a href="<?=\yii\helpers\Url::to('@web/')?>"><img src="/themes/nttechnology/images/nt-logo.jpg" alt="logo"  ></a>      
                </div>
                <div class="search-outer">
                    <div class="search-wrapper">
                        <form id="search_mini_form" action="/site/search" method="get">
                            <input type="text" value="" id="searchname" name="q" required placeholder="Search here...">
                            <select class="cat-select" name="Categories[]">
                                <option value="" disabled selected>All Categories</option>
                                <?php  $activeCategory=$store->activeCategoryIds;
                                $parentCategories=Categories::find()->where(['parent'=>0,'id'=>$activeCategory,'disabled'=>0])->all();
                                foreach ($parentCategories as $cat) { ?>
                                    <option value="<?=$cat->id ?>" name="Categories[]"> <?=$cat->title ?></option>
                                <?php } ?>
                             
                            </select>
                            <button class="fa fa-search" value=""></button>
                        </form>                        
                       
                         </div>
                    </div>
                     <div class="cart-container dropdown"> 
                       <a class="navbar-brand" href="<?=\yii\helpers\Url::to('@web/')?>"><img src="/themes/nttechnology/images/logo.png"></a>  
                    </div>
                    
                </div>
            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="nav-inner">
                        <div class="catmenu" id="main-menu">
                        <a class="cat-wrapper"> <i class="fa fa-shopping-cart"></i> Shop <i class="fa fa-angle-down"></i> </a>
                            <?php echo MainMenu::widget([ ]);?>
                            
                        </div>
                        <div class="navigation">
                            <?php echo HeaderMenu::widget([ ]);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  content section  -->
        <?php if(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='site/index' && Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='categories/products') {
        ?>
            <div class="cart-heading"><!-- breadcrumb -->
                <div class="container">
                    <!--<h4><?= Html::encode($this->title) ?></h4>-->
                    <div class="pull-right">                          
                        <?= Breadcrumbs::widget([
                            'tag' => 'ol',
                            //'options' => ['class' => 'breadcrumb'],
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                    </div>
                </div>
            </div> 
            <?php } 
            else {?>
                <div class="site-index">               

                </div>       
        <?php } ?>
      
        <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
          <div class="clearfix"></div>
            <?= $content ?>
        <!--  content section end -->
        <div class="footer">
            <div class="container">
                <div class="row">
                	<div class="fotter-menu">
                		<?php echo FooterMenu::widget([ ]);?>
                    <div class="clientportal-div" style="display:none;">
                      <?php if(isset(Yii::$app->session['clientportal'])) { ?> 
                        <a href="<?=\yii\helpers\Url::to(['client-portal/view'])?>" title="Client Portal" class="top-link-portal"> Client Portal</i></a>
                      <?php } else {  ?>
                        <a href="<?=\yii\helpers\Url::to(['client-portal/apply'])?>" title="Client Portal" class="top-link-portal bootstrap-modal" data-complete="window.location = '/client-portal/view';" data-parameters='{"noFlash":"true"}'> Client Portal</i></a>
                      <?php } ?>
                    </div>
                    </div>
                    <div class="col-xs-4">
                      <h3>Social</h3>
                        <div class="social">
                         <?php if($facebookUrl!=''){ ?>
                          <a href="<?=$facebookUrl?>" target="_blank"><i class="fa fa-round fa-facebook"></i></a>
                          <?php } ?>
                          <?php if($youtubeUrl!=''){ ?>
                          <a href="<?=$youtubeUrl?>" target="_blank"><i class="fa fa-round fa-youtube"></i></a>
                          <?php } ?>
                          <?php if($linkedinUrl!=''){ ?>
                          <a href="<?=$linkedinUrl?>" target="_blank"><i class="fa fa-round fa-linkedin"></i></a>
                          <?php } ?>
                          <?php if($googleplusUrl!=''){ ?>
                          <a href="<?=$googleplusUrl?>" target="_blank"><i class="fa fa-round fa-google"></i></a>
                          <?php } ?>
                          <?php if($twitterUrl!=''){ ?>
                          <a href="<?=$twitterUrl?>" target="_blank"><i class="fa fa-round fa-twitter"></i></a>
                          <?php } ?>
                          <?php if($instagramUrl!=''){ ?>
                          <a href="<?=$instagramUrl?>" target="_blank"><i class="fa fa-round fa-instagram"></i></a>
                          <?php } ?>
                          <?php if($pinterestUrl!=''){ ?>
                          <a href="<?=$pinterestUrl?>" target="_blank"><i class="fa fa-round fa-pinterest"></i></a>
                          <?php } ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                    <div class="copyright">
                      <div class="row">
                        <div class="col-xs-6">Copyright © <?=$store->title?> <?= date('Y') ?>. All rights reserved</div>
                        <div class="col-xs-6 c_right" >
                            <img src="/themes/nttechnology/images/visa.jpg" width="44" height="19"  alt=""/>
                            <img src="/themes/nttechnology/images/master_acrd.jpg" width="30" height="19"  alt=""/>
                            <!-- <img src="/themes/nttechnology/images/americalex.jpg" width="19" height="19"  alt=""/>  -->
                            <!-- <img src="/themes/nttechnology/images/paypal.jpg" width="62" height="19"  alt=""/> -->
                        </div>
                        </div>
                        </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
 
<script>
    $('.dropdown').on('show.bs.dropdown', function(e){
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
  });
   $('.dropdown').on('hide.bs.dropdown', function(e){
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
  });
  </script> 
<script>
  
    $(document).ready(function() {
		
		$(".catmenu > ul").addClass("megamenu");
		  $(".catmenu > ul").addClass("dropdown-menu");
		
      $("#owl").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true,
    loop:true,
    items:1,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true

      });
    var owl = $("#featuredslider");
    owl.owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,

        loop:false,
        items:4,
        margin:30,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true
    });
   
    $("#footer_slider").owlCarousel({

      navigation : false,
      slideSpeed : 100,
      paginationSpeed : 400,
      singleItem : true,
    loop:true,
    items:5,
    animateOut: 'fadeOut',
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true

      });

  
  
  
    });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $( "#main-menu > ul.dropdown-menu > li > ul.nav > li" ).each(function() { //console.log($(this).find('ul').length);
                if($(this).find('ul').length > 1){ 
                  $(this).find('a:first').addClass('triger-wrap');
                  $(this).find('ul:first').addClass('sub-wrap'); 
                }
            });

            $(".triger-wrap").click(function(event){  
                event.preventDefault();
                if($(this).parent().hasClass('active')){
                   $(this).parent().find('.sub-wrap').toggle();
                   $(this).toggleClass('rotate');
                }
                else {
                  $('.sub-wrap').css('display','none'); 
                  $(this).parent().find('.sub-wrap').toggle();  
                }
                $(".triger-wrap").parent().removeClass('active');
                $(this).parent().addClass('active');
            });  
         });
    </script>
    
    <script>
$(document).ready(function(e) {

/* $(function() {

  $('.nav li').mouseenter(function() {

   $(this).parents('li').addClass('hover');

   var len = $(this).find('ul').siblings().length;

  if (len > 15) {
   $(this).find('ul').addClass("smallmenu");
  } else {
   $(this).find('ul').removeClass("smallmenu");
  }
  
  
  
  });

  $('.nav li').mouseleave(function() {

   $(this).parents('li').removeClass('hover');

  });

 });*/
 
 
 
 
 $('.cat-select').each(function () {

    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    // Hides the select element
    $this.addClass('s-hidden');

    // Wrap the select element in a div
    $this.wrap('<div class="select"></div>');

    // Insert a styled div to sit over the top of the hidden select element
    $this.after('<div class="styledSelect"></div>');

    // Cache the styled div
    var $styledSelect = $this.next('div.styledSelect');

    // Show the first select option in the styled div
    $styledSelect.text($this.children('option').eq(0).text());

    // Insert an unordered list after the styled div and also cache the list
    var $list = $('<ul />', {
        'class': 'options'
    }).insertAfter($styledSelect);

    // Insert a list item into the unordered list for each select option
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    // Cache the list items
    var $listItems = $list.children('li');

    // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
    $styledSelect.click(function (e) {
        e.stopPropagation();
        $('div.styledSelect.active').each(function () {
            $(this).removeClass('active').next('ul.options').hide();
        });
        $(this).toggleClass('active').next('ul.options').toggle();
    });

    // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option
    $listItems.click(function (e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        /* alert($this.val()); Uncomment this for demonstration! */
    });

    // Hides the unordered list when clicking outside of it
    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

});

</script>

<script type="text/javascript">
        $(document).ready(function() {
            $('ul').not(':has(li)').remove();
            setTimeout(function(){ $("ul.pagination").wrap("<div class='col-xs-12 text-center'></div>")}, 1000);
            setTimeout(function(){ $(".rating-star").rating({disabled: true, showClear: false, showCaption: false})}, 500);
        });
</script>
<script type="text/javascript">

    $(document).ready(function() {

        $('ul').not(':has(li)').remove();

    });

</script>

<?php
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody()
?>
</body>
</html>
<?php $this->endPage() ?>
