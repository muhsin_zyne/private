<?php  
	use yii\helpers\Html;
	use yii\bootstrap\Nav;
	use yii\bootstrap\NavBar;
	use yii\widgets\Breadcrumbs;
	use frontend\assets\AppAsset;
	use frontend\widgets\Alert;
	use frontend\components\FrontNavBar;
	use frontend\components\MainMenu;
	use frontend\components\HeaderMenu;
	use frontend\components\FooterMenu;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use common\models\Stores;
	use common\models\Categories;
	use frontend\components\Helper;
	use yii\db\ActiveQuery;
	use yii\db\Query;
	use common\models\CmsPages;
	AppAsset::register($this);
		
?>
<?php
	$userId=Yii::$app->user->id;
	$storeId=Yii::$app->params['storeId'];
	$store = Stores::findOne(Yii::$app->params['storeId']);
	$facebookUrl = $store->getfacebookUrl();
	$youtubeUrl = $store->getyoutubeUrl();
	$linkedinUrl = $store->getlinkedinUrl();
	$googleplusUrl = $store->getgoogleplusUrl();
	$twitterUrl = $store->gettwitterUrl();
	$instagramUrl = $store->getinstagramUrl();
	$pinterestUrl = $store->getpinterestUrl();
		
?>
<?php $this->beginPage() ?>
		
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
		<head>
			<meta charset="<?= Yii::$app->charset ?>"/>
			<meta name="title" content="<?= Yii::$app->controller->metaTitle?>" />
			<meta name="keywords" content="<?= Yii::$app->controller->metaKeywords?>" />
			<meta name="description" content="<?= Yii::$app->controller->metaDescription?>" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="viewport" content="width=1200">
			<meta name="robots" content="noindex,nofollow">
			<?= Html::csrfMetaTags() ?>
			<title><?= Yii::$app->controller->metaTitle?></title>
			  	<link rel="icon" href="/themes/site5/images/favicon.ico" type="image/x-icon" />
    			<link rel="shortcut icon" href="/themes/site5/images/favicon.ico" type="image/x-icon" />
			<?php $this->head() ?>
			<?php include 'scripts.php';?> <!-- All css and js included -->
		</head>
		
		<body>
		<?php $this->beginBody() ?>
		<?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>
                <a href="<?=\yii\helpers\Url::to(['byod/logout'])?>" class="logout-fix" title="Logout from BYOD"><i class="fa fa-sign-out"></i><span>Logout <br/>from BYOD</span></a>
        <?php } ?>
		<div class="header">
		  <div class="header-top">
			<div class="container">
			  <div class="row">
				<div class="col-xs-5 topcontact">
				</div>
				<div class="col-xs-7 pull-right top-menu"> 
				 	<?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>  
		              <a href="<?=\yii\helpers\Url::to(['byod/view'])?>" style="margin-right:20px;"  title="BYOD" class="top-link-byod"> BYOD</i></a>
		            <?php } else {  ?>
		              <a href="<?=\yii\helpers\Url::to(['byod/apply'])?>" title="BYOD" style="margin-right:20px;" class="top-link-byod bootstrap-modal" data-complete="window.location = '/byod/view';" data-parameters='{"noFlash":"true"}'> BYOD</i></a>
		            <?php } ?>
				  <?php if (!\Yii::$app->user->isGuest) { ?> 
				  <a href="<?=\yii\helpers\Url::to(['site/account'])?>" title="My Account" ><i class="fa fa-user"></i> My Account</a>
				  <a href="<?=Yii::$app->urlManager->createUrl(['site/logout']) ?>" title="Log In" ><i class="fa fa-sign-out"></i> Log Out</a>
				  <a href="<?=\yii\helpers\Url::to(['/site/wishlist'])?>" title="View Cart" class="top-link-cart"> <i class="fa fa-heart"></i> Wish List <span>(
				  <?php $n=count(common\models\WishlistItems::find()->where(['storeId'=>$storeId,'userId'=>$userId])->all()); echo $n;?> )</span></a> 
				  <?php } else { ?>
				  <a href="<?=\yii\helpers\Url::to(['checkout/index'])?>" title="Checkout" class="top-link-checkout"> <i class="fa fa-sign-in"></i> Checkout</i></a>
				  <a href="<?=\yii\helpers\Url::to(['site/signup']) ?>" title="Register" ><i class="fa fa-key"></i> Register</a> 
				  <a href="<?=\yii\helpers\Url::to(['site/login'])?>" title="Log In" ><i class="fa fa-lock"></i> Login</a>
				  
					    
				  <?php } ?>
				  <div class="cart-container dropdown"> 

				  		<a class="dropdown-toggle" type="button" data-toggle="dropdown"> 
				  		
				  		 <i class="fa fa-shopping-bag"></i> 
				  		 <div class="Shopping-bg">
					  		 <span>Shopping cart</span>
					  		 <p>
							 <?php $n=count(Yii::$app->cart->positions); echo $n.' Item(s) - AU$'.Yii::$app->cart->cost?>   
							 </p>
						 </div>

						 <span class="fa fa-angle-down "></span>   
						 

						</a>
						 
						 <div class="dropdown-menu cart-drop"> <a href="<?=\yii\helpers\Url::to(['cart/view'])?>">View Cart</a> <a href="<?=\yii\helpers\Url::to(['checkout/index'])?>">Checkout</a> </div>
						 </div>
						 </div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="header-middle">
			<div class="container">
			  <div class="logo"> <a href="<?=\yii\helpers\Url::to('@web/')?>"><img src="/themes/site5/images/logo.png"  alt="logo" width="320px" height="65px"></a> </div>
			  <!-- <div class="navigation"> <?php echo HeaderMenu::widget([ ]);?> </div> -->
			  <div class="search-outer">
                                  <div class="search-wrapper">
                                    <form id="search_mini_form" action="/site/search" method="get">
                                        <input type="text" value="" id="searchname" name="q" required placeholder="Search...">
                                        
                                        <select class="cat-select" name="Categories[]">
                                            <option value="" disabled selected>Categories</option>
                                            <?php  $activeCategory=$store->activeCategoryIds;
                    							$parentCategories=Categories::find()->where(['parent'=>0,'id'=>$activeCategory,'disabled'=>0])->all();
                                            foreach ($parentCategories as $cat) { ?>
                                                <option value="<?=$cat->id ?>" name="c">  <?=$cat->title ?></option>
                                            <?php } ?>
                                         
                                        </select>
                                        <input type="submit" class="fa-search" value=""/>
                                    </form>
                                  </div>
						</div>
						<div class="bend-logo"> <a href="<?=\yii\helpers\Url::to('@web/')?>"><img src="/themes/site5/images/logo-site5.jpg"  alt="logo" width="200px" height="65px"></a> </div>
			</div>
		  </div>
		  <div class="header-bottom">
				<div class="container">
					  <div class="nav-inner">
						  <div class="catmenu" id="main-menu">
								<a class="cat-wrapper">All Categories <i class="fa fa-angle-down"></i> </a>
								<?php echo MainMenu::widget([ ]);?>
							</div> 
							<div class="navigation"> <?php echo HeaderMenu::widget([ ]);?> </div>
				</div>
		  </div>
		</div>
		</div>
        
		<div class="banner">
			<?php if(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='site/index' && Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='categories/products') {?>
				<div class="cart-heading"><!-- breadcrumb -->
					<div class="container">
					    <!--<h4><?= Html::encode($this->title) ?></h4>-->
					    <div class="pull-right">                          
					        <?= Breadcrumbs::widget([
					            'tag' => 'ol',
					            //'options' => ['class' => 'breadcrumb'],
					            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					        ]) ?>
					    </div>
					</div>
				</div>
			<?php } 
			else {?>
				<div class="site-index">
				
				</div>
			<?php } ?>
			<?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
			<div class="clearfix"> </div> 
		  	<?= $content ?>
		</div>
        
		<div class="footer">
		  	<div class="container">
				<div class="row">
			  		<div class="col-xs-4 ftr-abt">
					 <?=$this->renderBlock('home-page'); // cms block page slug ?>
					</div>
			  
              	<div class="ftr-group">              		
		           <?php echo FooterMenu::widget([ ]);?>
		           	<div class="clientportal-div" style="display:none;">
		                <?php if(isset(Yii::$app->session['clientportal'])) { ?> 
		                  <a href="<?=\yii\helpers\Url::to(['client-portal/view'])?>" title="Client Portal" class="top-link-portal"> Client Portal</i></a>
		                <?php } else {  ?>
		                  <a href="<?=\yii\helpers\Url::to(['client-portal/apply'])?>" title="Client Portal" class="top-link-portal bootstrap-modal" data-complete="window.location = '/client-portal/view';" data-parameters='{"noFlash":"true"}'> Client Portal</i></a>
		                <?php } ?>
              		</div> 
            	</div>
			<div class="col-xs-4 ftr-two">
				<h3>Subscribe Newsletter</h3>
				<div class="ns-letter"> To recieve updates on new arrivals, specials offers and other discount information
					<div class="ns-field">						
     					<form id="sub-form"  action="/ajax/subscribe" method="post" onsubmit="return isValidEmailAddress()">
						  	 <input type="email"  name="email" id="sub-email"   placeholder="Enter your email" class="form-control" required >
						  	<input type="submit" class="fa-paper-plane" value="" />
						</form>							
					</div>
					<div class="connect">
					<span>Connect With Us</span>
                      <div class="social-connect">
		              <?php if($facebookUrl!=''){ ?>
                          <a href="<?=$facebookUrl?>" target="_blank"><i class="fa fa-round fa-facebook"></i></a>
                          <?php } ?>
                          <?php if($youtubeUrl!=''){ ?>
                          <a href="<?=$youtubeUrl?>" target="_blank"><i class="fa fa-round fa-youtube"></i></a>
                          <?php } ?>
                          <?php if($linkedinUrl!=''){ ?>
                          <a href="<?=$linkedinUrl?>" target="_blank"><i class="fa fa-round fa-linkedin"></i></a>
                          <?php } ?>
                          <?php if($googleplusUrl!=''){ ?>
                          <a href="<?=$googleplusUrl?>" target="_blank"><i class="fa fa-round fa-google"></i></a>
                          <?php } ?>
                          <?php if($twitterUrl!=''){ ?>
                          <a href="<?=$twitterUrl?>" target="_blank"><i class="fa fa-round fa-twitter"></i></a>
                          <?php } ?>
                          <?php if($instagramUrl!=''){ ?>
                          <a href="<?=$instagramUrl?>" target="_blank"><i class="fa fa-round fa-instagram"></i></a>
                          <?php } ?>
                          <?php if($pinterestUrl!=''){ ?>
                          <a href="<?=$pinterestUrl?>" target="_blank"><i class="fa fa-round fa-pinterest"></i></a>
                        <?php } ?>
                        <div>
		        </div>
				</div>
			</div>
		</div>
	</div>
</div>

  	
</div>
<div class="ftr-bottom">
		<div class="container">
	  		<div class="copyright">
				<div class="row">
		  			<div class="col-xs-6">Copyright © <?=$store->title?> <?= date('Y') ?>. All rights reserved</div>					
		  			<div class="col-xs-6 c_right" ><img src="/themes/site5/images/cards.png" height="26"  alt=""/> </div>
				</div>
	  		</div>
		</div>
  	</div>

  	<script>		
	$('.dropdown').on('show.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
  	});
   	$('.dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
  	});
</script> 
		
<script type="text/javascript">
	$(document).ready(function() {
 		$('ul').not(':has(li)').remove();
 		setTimeout(function(){ $("ul.pagination").wrap("<div class='col-xs-12 text-center'></div>")}, 1000);
 		//$("#input-id").rating();
 		setTimeout(function(){ $(".rating-star").rating({disabled: true, showClear: false, showCaption: false})}, 500);
 		//$(".rating-star").rating({disabled: true, showClear: false, showCaption: false});
 	});
</script>

 <script type="text/javascript">
        $(document).ready(function() {
          $(".catmenu > ul").addClass("megamenu");
		  $(".catmenu > ul").addClass("dropdown-menu");
            //$( "ul" ).addClass( "cat-nav" );
            $('ul').not(':has(li)').remove();
            setTimeout(function(){ $("ul.pagination").wrap("<div class='col-xs-12 text-center'></div>")}, 1000);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $( "#main-menu > ul.dropdown-menu > li > ul.nav > li" ).each(function() {
              	if($(this).find('ul').length > 0){ 
                  $(this).find('a:first').addClass('triger-wrap');
                  $(this).find('ul:first').addClass('sub-wrap'); 
                }
            });

            $(".triger-wrap").click(function(event){  
                event.preventDefault();
                if($(this).parent().hasClass('active')){
                   $(this).parent().find('.sub-wrap').toggle();
                   $(this).toggleClass('rotate');
                }
                else {
                  $('.sub-wrap').css('display','none'); 
                  $(this).parent().find('.sub-wrap').toggle();  
                }
                $(".triger-wrap").parent().removeClass('active');
                $(this).parent().addClass('active');
            });  
         });
    </script>
	
<script>
	$(document).ready(function() {
		$("#owl").owlCarousel({
			navigation : true,
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem : true,
			loop:true,
			items:1,
			animateOut: 'fadeOut',
			autoplay:true,
			autoplayTimeout:3000,
			autoplayHoverPause:true
		});        
        var owl = $("#featuredslider");
        
	    owl.owlCarousel({
		    navigation : true,
		    slideSpeed : 300,
		    paginationSpeed : 400,
		    loop:true,
		    items:4,
		    margin:30,
		    autoplay:true,
		    autoplayTimeout:3000,
		    autoplayHoverPause:true
	    });
        
        $("#footer_slider").owlCarousel({ 
            navigation : false,        
            slideSpeed : 300,        
            paginationSpeed : 400,        
            singleItem : true,        
            loop:true,        
            items:6,        
            animateOut: 'fadeOut',        
            autoplay:true,
            autoplayTimeout:3000,        
            autoplayHoverPause:true 
        });        
    });        
</script> 
<script>
    $(document).ready(function(e) {    
        $(function() {    
            $('.nav li').mouseenter(function() {    
                $(this).parents('li').addClass('hover');    
                var len = $(this).find('ul').siblings().length;    
                if (len > 15) {
                   $(this).find('ul').addClass("smallmenu");
                } else {
                   $(this).find('ul').removeClass("smallmenu");
                }
            });
    
            $('.nav li').mouseleave(function() {    
                $(this).parents('li').removeClass('hover');
            });    
        });
		
		
        $('.cat-select').each(function () {    
            // Cache the number of options
            var $this = $(this),
            numberOfOptions = $(this).children('option').length;
    
            // Hides the select element
            $this.addClass('s-hidden');
        
            // Wrap the select element in a div
            $this.wrap('<div class="select"></div>');
        
            // Insert a styled div to sit over the top of the hidden select element
            $this.after('<div class="styledSelect"></div>');
        
            // Cache the styled div
            var $styledSelect = $this.next('div.styledSelect');
        
            // Show the first select option in the styled div
            $styledSelect.text($this.children('option').eq(0).text());
        
            // Insert an unordered list after the styled div and also cache the list
            var $list = $('<ul />', {
                'class': 'options'
            }).insertAfter($styledSelect);
    
            // Insert a list item into the unordered list for each select option
            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }
        
            // Cache the list items
            var $listItems = $list.children('li');
        
            // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
            $styledSelect.click(function (e) {
                e.stopPropagation();
                $('div.styledSelect.active').each(function () {
                    $(this).removeClass('active').next('ul.options').hide();
                });
                $(this).toggleClass('active').next('ul.options').toggle();
            });
        
            // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
            // Updates the select element to have the value of the equivalent option
            $listItems.click(function (e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                /* alert($this.val()); Uncomment this for demonstration! */
            });
    
            // Hides the unordered list when clicking outside of it
            $(document).click(function () {
                $styledSelect.removeClass('active');
                $list.hide();
            });
    
        });
    });
</script>

<script>
/*function validateForm() {
    var x = document.forms["sub-form"]["sub-email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Please enter a valid email.");
        return false;
    }
}*/

function isValidEmailAddress() {
    var emailAddress = document.forms["sub-form"]["sub-email"].value;
    if(emailAddress.length != 0){
      var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
      if(pattern.test(emailAddress) == false ){
        alert("Please enter a valid email.");
          return false;
      }
    }
    else{
      alert('Please enter a valid email.');
      return false;
    }  
};

</script>
<script type="text/javascript">

    $(document).ready(function() {

        $('ul').not(':has(li)').remove();

    });

</script>

	<?php
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody()
	?>
</body>
</html>
<?php $this->endPage() ?>


