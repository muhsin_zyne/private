<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use yii\captcha\Captcha;

use yii\widgets\Breadcrumbs;



/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */



$this->title = 'Contact';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">
  <div class="inner-container">
    <div class="site-contact">
      
      <div class="row">
        <div class="col-xs-12">
          <h1 class="cntct-head">Get in touch</h1>
          <p> If you have business enquiries or other questions, please fill out the following form to contact us. Thank you. </p>
        </div>
        <div class="col-xs-9">
          <div class="row">
          <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
          <div class="col-xs-6">
          <?= $form->field($model, 'name') ?>
          </div>
          <div class="col-xs-6">
          <?= $form->field($model, 'email') ?>
          </div>
          <div class="col-xs-12">
          <?= $form->field($model, 'subject') ?>
          <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
          <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-xs-3">{image}<a href="#" id="fox"><i class="fa fa-refresh"></i></a></div><div class="col-xs-9">{input}</div></div>',
            ]) ?>
          <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
          </div>
          </div>
         
          <?php ActiveForm::end(); ?>
           </div>
        </div>
        
        
        <div class="col-xs-3">
         <div class="row">
        <div class="col-xs-12">
              <div class="trading_hours">
              <span><i class="fa fa-clock-o"></i>TRADING HOURS:</span>
                 MON     8.30am  &ndash; 5.00pm<br>
                TUE     8.30am  &ndash; 5.00pm<br>
                WED     8.30am  &ndash; 5.00pm<br>
                THU     8.30am  &ndash; 5.30pm<br>
                FRI     8.30am  &ndash; 5.00pm<br>
                SAT     9.00am &ndash; 12.30pm<br>
                SUN     Closed </div>
            </div>
            
            <div class="col-xs-12">
          <div class="contact-bottom ">
              <div class="mapaddress">
              <span><i class="fa fa-map-marker"></i> Address:</span>
              Shop 7 - 14 Anthony Rd<br>
                WEST RYDE<br>
                NSW 2114<br>
                P: 02 9645 5544<br>
              </div>
            </div>
            
          </div>
          
          </div>
        </div>
            
            
        <div class="col-xs-12 cnct-form">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3315.2238847587714!2d151.08616451496246!3d-33.80653502326075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a44dfc8063b3%3A0xc4e9dc9214938227!2s7%2F14+Anthony+Rd%2C+West+Ryde+NSW+2114%2C+Australia!5e0!3m2!1sen!2sin!4v1452599942720" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        
      </div>
    </div>
  </div>
</div>
<script>
$("#fox").click(function(event){ // captcha refresh button 
    event.preventDefault();
    $("img[id$='-verifycode-image']").click();
})
</script>