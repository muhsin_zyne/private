<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Your order has been received';
//$this->params['breadcrumbs'][] = $this->title;
?>

<?php //die('success'); ?>

<div class="container">
    <div class="site-about success-msg">
        <h1><?= Html::encode($this->title) ?></h1>
    
        <p class="p-one">Thank you for your order :)</p>
        <p>Your order id is #<?=$orderId?></p>
        <p>Please refer to your nominated email address to access your Order Confirmation.</p>
        <p><u>Next Step</u> - We will also update you by email when your order is ready for collection.</p>
        <p>For any other questions please don’t hesitate to contact us in store…</p>
        <div class="continue_button">
            <a href="<?=\yii\helpers\Url::to('@web/')?>">
                <button class="cont"><span>Continue Shopping</span></button>
            </a>    
        </div>
    </div>
</div>
<?php // only for google analytics ecommerce
$order=common\models\Orders::findOne($orderId);
$orderItems=common\models\OrderItems::find()->where(['orderId'=>$order->id])->all();
$google_analytic=common\models\Configuration::findSetting('google_analytics', Yii::$app->params['storeId']);?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', '<?= $google_analytic ?>', 'auto');
    ga('send', 'pageview');
    ga('require', 'ecommerce');
    ga('ecommerce:addTransaction', {
        'id': <?= $order->id?>, // Transaction ID. Required.
        'revenue': <?= $order->grandTotal?>, // Grand Total.
        'shipping': <?= $order->shippingAmount?>, // Shipping.
        'tax': <?= $order->appliedTax?> // Tax.
        //'currency': 'AU$'
    });
    <?php foreach ($orderItems as  $orderItem) { ?>
       ga('ecommerce:addItem', {            
            'id': <?= $order->id ?>, // Transaction ID. Required.
            'name': '<?= $orderItem->product->name ?>', // Product name. Required.
            'sku': '<?= $orderItem->product->sku ?>', // SKU/code.
            'price': <?= $orderItem->price ?>, // Unit price.
            'quantity': <?= $orderItem->quantity ?> // Quantity.
            //'currency': 'AU$'
        }); 
    <?php }?>    
    ga('ecommerce:send');
</script>