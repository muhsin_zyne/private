<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use frontend\components\Helper;
use bryglen\braintree\braintree;
use frontend\components\eway\Eway;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = $this->title;
$gift_wrap_check= \common\models\Stores::findOne(Yii::$app->params['storeId'])->hasGiftWrap;
$paymentMethod = \common\models\Stores::findOne(Yii::$app->params['storeId'])->paymentMethod;
$this->registerJsFile('../js/jquery.creditCardValidator.js', ['position' => \yii\web\View::POS_BEGIN]);


if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
    $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
else
    $sessionVar = 'clientPortal';

?>
<!-- <script type="text/javascript" src="/js/inte.activeForm.js"></script> -->
<script src="https://assets.braintreegateway.com/js/braintree-2.17.6.js"></script>
<div class="checkout_container container">
    <div class="site-checkout" id="site-checkout">
        <div class="cart_dec account">
            <h1>Checkout</h1>
        </div>
    
        <div class="cart_dec account">
        
        <?php //$form = ActiveForm::begin(['id' => 'checkout-form', 'action' => Url::to(['checkout/process'])]); 
        $action = isset($orderId)? Url::to(['checkout/process', 'orderId' => $orderId]) : Url::to(['checkout/process']);
        $form = ActiveForm::begin(['id' => 'checkout-form', 'action' => $action,'options' => ['data-eway-encrypt-key'=>Yii::$app->eway->encryptionKey]]); ?>
        <?php 

        	if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
           		$sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
            else
            	$sessionVar = 'clientPortal';
        ?>
        <ol class="opc" id="checkoutSteps">
			<?php if(Yii::$app->user->isGuest){ ?>
			<li id="opc-login" class="section allow">
			     <div class="checkout_head ">Checkout method <a href="#" class="pull-right">Edit</a></div>
			        <div id="checkout-step-login" class="checkout_content step a-item" style=" ">
			            
			       
			        <h3>Checkout as a Guest or Register</h3>
                    
            <div class="checkout_type">
                <p><input type="radio" name="checkout_method" id="login-guest" value="guest" class="radio authentication">Checkout as Guest</p>
                <p><input type="radio" name="checkout_method" id="login-register" value="register" class="radio authentication">Register</p>
            </div>			                    
			       

                        <div class="buttons-set">
                          
                                          <button id="onepage-guest-register-button" type="button" class="button continue"><span>Continue</span></button>
                                  </div>

					       <br>
			                <!-- <form id="login-form" action="<?=Url::to(['site/login'])?>" method="post"> -->
			        <fieldset>
			            <p>Already registered?</p>
                        
                        
         <label>Email Address</label>
          <input type="text">
          <label>Password</label>
          <input type="text">
            <p><a href="#">Forgot your password?</a></p>
            <a class=" btn btn-block" href="#">Login</a>                        
                        
			            <!--<p>Please log in below:</p>-->
			       <!--     <ul class="form-list">
			                <li>
			                    <label for="login-email" class="required"><em>*</em>Email Address</label>
			                    <div class="input-box">
			                        <input type="text" class="login-field input-text required-entry validate-email" id="login-email" name="LoginForm[email]" value="" />
			                    </div>
			                </li>
			                <li>
			                    <label for="login-password" class="required"><em>*</em>Password</label>
			                    <div class="input-box">
			                        <input type="password" class="login-field input-text required-entry" id="login-password" name="LoginForm[password]" />
			                    </div>
			                </li>
			                            </ul>
			            <input name="context" type="hidden" value="checkout" />
			        <div class="col-2">
				        <div class="buttons-set">
				            

				            <button type="submit" class="button login-button"><span>Login</span></button>
				                   <a href="http://www.legj.com.au/site1/customer/account/forgotpassword/" class="f-left">Forgot your password?</a>
				        </div>
			    	</div>-->
			        </fieldset>
			        <!-- </form> -->
			 
                <div class="clearfix"></div>
			
			</div>
		</li>
	<?php } ?>
    <li id="opc-billing" class="section">
        <div class="step-title">
            <h2>Billing Information</h2><a href="#" class="pull-right">Edit</a>
        </div>
        <div id="checkout-step-billing" class="step a-item" style=" ">
            <form id="co-billing-form" action="">
<fieldset>
        <ul class="form-list">
            <li id="billing-new-address-form">
       <!--  <fieldset> -->
            
            <ul id="billing-address-form">
                    <?php
        if(!Yii::$app->user->isGuest && \common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->exists()){ ?>
            <div class="field name-address">
                <label for="billing:address" class="required"><em>*</em>Billing Address</label>
                <div class="input-box">
                    <?=Html::dropDownList('CheckoutForm[billingAddress][customerAddressId]', [], ArrayHelper::map(\common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->all(), 'id', 'addressAsTextForMobile') + ['new' => 'New Address'], ['class' => 'stored-billing-address'])?>
                </div>
            </div>
        <?php } ?>
                <li class="fields"><div class="customer-name">
        <div class="field name-firstname">
        <label for="billing:firstname" class="required"><em>*</em>First Name</label>
        <div class="input-box">
            <?=$form->field($checkout, 'billing_firstname')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->firstname] : [])->label('')?>
        </div>
    </div>
    <div class="field name-lastname">
        <label for="billing:lastname" class="required"><em>*</em>Last Name</label>
        <div class="input-box">
            <?=$form->field($checkout, 'billing_lastname')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->lastname] : [])->label('')?>
        </div>
    </div>
</div>
</li>
                <li class="fields">
                    <div class="field">
                        <label for="billing:company">Company</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'billing_company')->textInput()->label('')?>
                        </div>
                    </div>
                            <div class="field">
                        <label for="billing:email" class="required"><em>*</em>Email Address</label>
                        <div class="input-box">
                             <?=$form->field($checkout, 'email')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->email] : [])->label('')?>
                        </div>
                    </div>
                        </li>
                <li class="wide">
                    <label for="billing:street1" class="required"><em>*</em>Address</label>
                    <div class="input-box">
                        <?=$form->field($checkout, 'billing_street')->textArea()->label('')?>
                    </div>
                </li>
                        <li class="fields">
                    <div class="field">
                        <label for="billing:city" class="required"><em>*</em>City</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'billing_city')->textInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="billing:region_id" class="required"><em>*</em>State/Province</label>
                        <div class="input-box">
                        <?=$form->field($checkout, 'billing_state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])->label('')?>
                        </div>
                    </div>
                </li>
                <li class="fields">
                    <div class="field">
                        <label for="billing:postcode" class="required"><em>*</em>Zip/Postal Code</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'billing_postcode')->textInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="billing:country_id" class="required"><em>*</em>Country</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'billing_country')->dropDownList(['Australia'], ['prompt'=>'--Select--'])->label('')?>   
                        </div>
                    </div>
                </li>
                <li class="fields">
                    <div class="field">
                        <label for="billing:telephone" class="required"><em>*</em>Telephone</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'billing_phone')->textInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="billing:fax">Fax</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'billing_fax')->textInput()->label('')?>
                        </div>
                    </div>
                </li>

                <?php if(Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar)) { ?>

                	<li class="fields">
	                  	<div class="field">
		                    <label for="shipping:telephone" class="required">Student Name</label>
		                    <div class="input-box">
		                    	<div class="form-group">
		                    		<input type="text" name="OrderMeta[byod_studentName]" class="byod_metavals form-control">
		                    	</div>	
		                    </div>
	                  	</div>
	                  	<div class="field">
		                    <label for="shipping:fax">Parent Name</label>
		                    <div class="input-box">
		                    	<div class="form-group">
		                    		<input type="text" name="OrderMeta[byod_parentName]" class="byod_metavals form-control">
		                    	</div>	
		                    </div>
	                  	</div>
                	</li>

                	<li class="fields">
	                  	<div class="field">
		                    <label for="shipping:telephone" class="required">Order ID</label>
		                    <div class="input-box">
		                    	<div class="form-group">
		                    		<input type="text" name="OrderMeta[byod_orderId]" class="byod_metavals form-control">
		                    	</div>	
		                    </div>
	                  	</div>
	                  	<div class="field">
		                    <label for="shipping:fax">Student ID</label>
		                    <div class="input-box">
		                    	<div class="form-group">
		                    		<input type="text" name="OrderMeta[byod_studentId]" class="byod_metavals form-control">
		                    	</div>	
		                    </div>
	                  	</div>
                	</li>

                <?php } ?>
                            
                <?php if(Yii::$app->user->isGuest){ ?>
                <li class="fields password" id="register-customer-password">
                    <div class="field">
                        <label for="billing:customer_password" class="required"><em>*</em>Password</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'password')->passwordInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="billing:confirm_password" class="required"><em>*</em>Confirm Password</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'confirmPassword')->passwordInput()->label('')?>
                        </div>
                    </div>
                </li>
                <?php } ?>
                    <li class="no-display"><input type="hidden" name="billing[save_in_address_book]" value="1" /></li>
                            </ul>
            <div id="window-overlay" class="window-overlay" style="display:none;"></div>
<div id="remember-me-popup" class="remember-me-popup" style="display:none;">
    <div class="remember-me-popup-head">
        <h3>What's this?</h3>
        <a href="#" class="remember-me-popup-close" title="Close">Close</a>
    </div>
    <div class="remember-me-popup-body">
        <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
        <div class="remember-me-popup-close-button a-right">
            <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a>
        </div>
    </div>
</div>
<!-- </fieldset> -->
     </li>
            <li class="control" style="display: none;">
            <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_yes" value="1" checked="checked" title="Ship to this address" onclick="$('shipping:same_as_billing').checked = true;" class="radio" /><label for="billing:use_for_shipping_yes">Ship to this address</label></li>
        <li class="control" style="display: none;">
            <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_no" value="0" title="Ship to different address" onclick="$('shipping:same_as_billing').checked = false;" class="radio" /><label for="billing:use_for_shipping_no">Ship to different address</label>
        </li>
        </ul>
        <div class="buttons-set" id="billing-buttons-container">
        <p class="required">* Required Fields</p>
        <button type="button" title="Continue" class="button continue"><span>Continue</span></button>
        <span class="please-wait" id="billing-please-wait" style="display:none;">
            <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...        </span>
    </div>
</fieldset>
        </div>
    </li>
<?php if(!Yii::$app->cart->hasOnlyGiftVouchers()){ 
      if(!Yii::$app->cart->isClientPortalInstorePickup($sessionVar) && !Yii::$app->cart->isClientPortalInstorePickup('clientPortal') && Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar)) {
  ?>

<li id="opc-shipping" class="section">
        <div class="step-title">
            <h2>Shipping Information</h2>
            <a href="#" class="pull-right">Edit</a>
        </div>
        <div id="checkout-step-shipping" class="step a-item" style=" ">

<fieldset>
        <ul class="form-list">
            <li id="shipping-new-address-form">
        <!--<fieldset>  -->
            
            <ul id="shipping-address-form">
                    <?php
        if(!Yii::$app->user->isGuest && \common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->exists()){ ?>
            <div class="field name-address">
                <label for="shipping:address" class="required"><em>*</em>Shipping Address</label>
                <div class="input-box">
                    <?=Html::dropDownList('CheckoutForm[shippingAddress][customerAddressId]', [], ArrayHelper::map(\common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->all(), 'id', 'addressAsTextForMobile') + ['new' => 'New Address'], ['class' => 'stored-shipping-address'])?>
                </div>
            </div>
        <?php } ?>
                <li class="fields"><div class="customer-name">
    <div class="field name-firstname">
        <label for="shipping:firstname" class="required"><em>*</em>First Name</label>
        <div class="input-box">
            <?=$form->field($checkout, 'shipping_firstname')->textInput()->label('')?>
        </div>
    </div>
    <div class="field name-lastname">
        <label for="shipping:lastname" class="required"><em>*</em>Last Name</label>
        <div class="input-box">
            <?=$form->field($checkout, 'shipping_lastname')->textInput()->label('')?>
        </div>
    </div>
</div>
</li>
                <li class="fields">
                    <div class="field">
                        <label for="shipping:company">Company</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'shipping_company')->textInput()->label('')?>
                        </div>
                    </div>
                    <!--         <div class="field">
                        <label for="shipping:email" class="required"><em>*</em>Email Address</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'email')->textInput()->label('')?>
                        </div>
                    </div> -->
                        </li>
                <li class="wide">
                    <label for="shipping:street1" class="required"><em>*</em>Address</label>
                    <div class="input-box">
                        <?=$form->field($checkout, 'shipping_street')->textArea()->label('')?>
                    </div>
                </li>
                        <li class="fields">
                    <div class="field">
                        <label for="shipping:city" class="required"><em>*</em>City</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'shipping_city')->textInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="shipping:region_id" class="required"><em>*</em>State/Province</label>
                        <div class="input-box">
                        <?=$form->field($checkout, 'billing_state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])->label('')?>
                        </div>
                    </div>
                </li>
                <li class="fields">
                    <div class="field">
                        <label for="shipping:postcode" class="required"><em>*</em>Zip/Postal Code</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'shipping_postcode')->textInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="shipping:country_id" class="required"><em>*</em>Country</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'shipping_country')->dropDownList(['Australia'], ['prompt'=>'--Select--'])->label('')?>   
                        </div>
                    </div>
                </li>
                <li class="fields">
                    <div class="field">
                        <label for="shipping:telephone" class="required"><em>*</em>Telephone</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'shipping_phone')->textInput()->label('')?>
                        </div>
                    </div>
                    <div class="field">
                        <label for="shipping:fax">Fax</label>
                        <div class="input-box">
                            <?=$form->field($checkout, 'shipping_fax')->textInput()->label('')?>
                        </div>
                    </div>
                </li>
                <li class="fieldss">
                                        <div class="field">
                        
                        <div class="input-box">
                            <div class="form-group field-checkoutform-shipping_phone has-success">
                            <input name="CheckoutForm[sameAsBilling]" type="checkbox" id="shipping-sameas-billing" checked/>
                            <label for="shipping:fax">Same as Billing Address</label>
                        </div>
                        </div>
                    </div>
                </li>
                
                            
                            </ul>
            <div id="window-overlay" class="window-overlay" style="display:none;"></div>
<!--</fieldset> -->
     </li>
            <li class="control" style="display: none;">
            <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_yes" value="1" checked="checked" title="Ship to this address" onclick="$('shipping:same_as_billing').checked = true;" class="radio" /><label for="billing:use_for_shipping_yes">Ship to this address</label></li>
        <li class="control" style="display: none;">
            <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_no" value="0" title="Ship to different address" onclick="$('shipping:same_as_billing').checked = false;" class="radio" /><label for="billing:use_for_shipping_no">Ship to different address</label>
        </li>
        </ul>
        <div class="buttons-set" id="billing-buttons-container">
        <p class="required">* Required Fields</p>
        <button type="button" title="Continue" class="button continue"><span><span>Continue</span></span></button>
        <span class="please-wait" id="billing-please-wait" style="display:none;">
            <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...        </span>
    </div>
</fieldset>
        </div>
    </li>
 <?php } ?>

    <li id="opc-shipping_method" class="section">
        <div class="step-title">
            <h2>Shipping Method</h2>
            <a href="#" class="pull-right">Edit</a>
        </div>
        <div id="checkout-step-shipping_method" class="step a-item" style=" ">
    <div id="checkout-shipping-method-load">
            <dl class="sp-methods">
              <!-- <b> Ship To My Address</b>-->
           
                           <?=$form->field($checkout, 'shipping_method')->radioList(ArrayHelper::map($shippingMethods, 'id', 'detail'), 
                                    [
                                        'item' => function($index, $label, $name, $checked, $value){
                                            $details = explode(":", $label);
                                            $class_text = explode("(", $label);
                                            //var_dump(str_replace(" ", "",$class_text['0']));die();

                                            return '<div class="radio"><label><input id="checkoutform-shipping_method" class="shipping-method-radio '.str_replace(" ", "",$class_text['0']).'" data-price="'.$details[1].'" type="radio" name="'.$name.'" value="'.$value.'"> '.$details[0].'</label></div>';
                                        }
                                    ]
                                )?>
                            

            </dl>
            <div class="collect_store_div">
                        <?php if(isset($addresses) && count($addresses) > 1) { ?>
                        <dl class='collect_store' style="display:none">
                            <?=Html::dropDownList('CheckoutForm[storeAddressId]', [], ArrayHelper::map($addresses, 'id', 'addressAsText'), ['class' => 'stored-billing-address'])?>
                        </dl>
                        <?php } //else {  var_dump($addresses); die(); ?>
                            <!-- <input type="hidden" name="CheckoutForm[storeAddressId]" value="<?php /*$addresses->id */?>"> -->
                        <?php //} ?>
                    </div>

    </div>
    <div id="onepage-checkout-shipping-method-additional-load">
    </div>
    <div class="buttons-set" id="shipping-method-buttons-container">
        <button type="button" class="button continue"><span><span>Continue</span></span></button>
        <span id="shipping-method-please-wait" class="please-wait" style="display:none;">
            <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...        </span>
    </div>
        </div>
    </li>



    <li id="opc-review" class="section">
        <div class="step-title">
            <h2>Order Review</h2>
            <a href="#" class="pull-right">Edit</a>
        </div>
<div id="checkout-step-review" class="step a-item">
            <div class="order-review" id="checkout-review-load">    
<div id="checkout-review-table-wrapper">
    <table class="data-table" id="checkout-review-table">
                <colgroup><col>
        <col width="1">
        <col width="1">
        <col width="1">
                </colgroup><thead>
            <tr class="first last">
                <th rowspan="1">Product Name</th>
                <th colspan="1" class="a-center">Price</th>
                <th rowspan="1" class="a-center">Qty</th>
                <th colspan="1" class="a-center">Subtotal</th>
            </tr>
                    </thead>
        <tfoot>
    <tr class="first sub-total">
    <td style="" class="a-right" colspan="3">
        Subtotal    </td>
    <td style="" class="a-right last">
        <span data-price="<?=Yii::$app->cart->cost?>" class="price"><?=Helper::money(Yii::$app->cart->cost)?></span>    </td>
</tr>
<tr class="payment-summary">
    <td id="shipping-method" style="" class="a-right" colspan="3">
        Shipping &amp; Handling    </td>
    <td id="shipping-price" style="" class="a-right last">
        <span class="price">AU$0.00</span>    </td>
</tr>
<tr class="voucher-summary" style="display:none;">
    <td style="" class="a-right" colspan="3">
        Gift Voucher Discount    </td>
    <td id="voucher-discount" style="" class="a-right last">
        <span data-price="<?=$voucherDiscount?>" class="price"><?=Helper::money($voucherDiscount)?></span>    </td>
</tr>

<tr class="coupon-summary" style="display:none;">
    <td style="" class="a-right" colspan="3">
        Coupon Discount    </td>
    <td id="coupon-discount" style="" class="a-right last">
        <span data-price="<?=$couponDiscount?>" class="price"><?=Helper::money($couponDiscount)?></span>    </td>    
</tr>

<tr class="last grand-total">
    <td style="" class="a-right" colspan="3">
        <strong>Grand Total</strong>
    </td>
    <td style="" class="a-right last">
        <strong><span data-price="<?=Yii::$app->cart->getCost(true)?>" class="price"><?=Helper::money(Yii::$app->cart->getCost(true))?></span></strong>
    </td>
</tr>

    </tfoot>
        <tbody>
            <?php 
            $positions = Yii::$app->cart->positions;
            foreach($positions as $position){ ?>
                        <tr class="first last odd">
            <td><h3 class="product-name"><?=$position->product->name?></h3>
                        </td>
            <td class="a-right">
                <span class="cart-price">
                    <span class="price"><?=Helper::money($position->price)?></span>
                </span>
            </td>
            <td class="a-center"><?=$position->quantity?></td>
            <td class="a-right last">
                <span class="cart-price">
                <span class="price"><?=Helper::money($position->cost)?></span>            
            </span>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div id="checkout-paypaliframe-load" class="authentication"></div>


<div id="checkout-review-submit">
    
    <div class="buttons-set" id="review-buttons-container">
        <!-- <button type="submit" title="Pay Now" class="button btn-checkout"><span><span>Pay Now</span></span></button> -->
        <button type="button" title="Continue" class="button continue"><span><span>Continue</span></span></button>
        <span class="please-wait" id="review-please-wait" style="display:none;">
            <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Submitting order information..." title="Submitting order information..." class="v-middle"> Submitting order information...        </span>
    </div>
    
</div>
</div>
        </div>
    </li>

    <?php } ?>

    <li id="opc-payment" class="section">
        <div class="step-title">
            <h2>Payment Information</h2>
            <a href="#" class="pull-right">Edit</a>
        </div>
        <div id="checkout-step-payment" class="step a-item" style=" ">
        

        <?php 
        if($paymentMethod != "eway") {
            $braintree = true;
 			if(in_array(Yii::$app->params['storeId'], array(80,136)))
				$braintree = false; 
			if($braintree ==false) :?>
			<for$braintree =m action="" method="post">
				<input type="hidden" name="cmd" value="_xclick">
			</form>
			<?php endif;?>	
        <?php } ?>    

        <fieldset>
            <dl class="sp-methods" id="checkout-payment-method-load">
                <!-- <div style="position:relative" class="v_m_logo">
                	<div><img border="0" alt="visa and mastercard" src="http://www.legj.com.au/skin/frontend/default/hammertons/images/visaandmastercard.jpg"></div>
                </div> 
                <dt>
                        <label for="p_method_giftvoucher">Gift Voucher </label>
                </dt>-->

                <i class="fa fa-plus-circle" data-toggle="collapse" data-target="#giftvoucher-option"><span class="check-gift-title">Apply Gift Voucher</span></i>
                <div id="giftvoucher-option" class="collapse">
                <dd id="payment_form_giftvoucher_dd">
                    <ul class="form-list" id="payment_form_giftvoucher">
                    	<li class="giftvoucher-description">
                    		Use your gift voucher that you have to spend for this order!	
                        </li>
                        <li class="giftvoucher-discount-code">
                            <ul>
                                <?php 
                                if(isset(Yii::$app->session['appliedVouchers'])){
                                    foreach(Yii::$app->session['appliedVouchers'] as $id => $amount){
                                             $voucher = \common\models\GiftVouchers::findOne($id);
                                    ?>
                                    <li class="voucher">
                                        <strong>
                                            Gift Voucher <?=$voucher->code?> (<span class="price"><?=Helper::money($amount)?></span>)
                                        </strong>
                                        <button type="button" data-id="<?=$id?>" id="remove_voucher_<?=$id?>" title="Remove" class="remove-voucher">
                                            <img src="images/btn_remove.gif" alt="Remove">
                                        </button>
                                    </li>
                                    <?php }
                                    }
                                ?>
                                    </ul>
                        </li>
                        <li class="notice-msg">
                        	<ul>
                        		<li>Grand total of this order is <strong><span data-price="<?=Yii::$app->cart->getCost(true)?>" class="price"><?=Helper::money(Yii::$app->cart->getCost(true))?></span></strong></li>
                        	</ul>
                    	</li>
                    	<li id="giftvoucher_message">
                            <ul class="error-msg" style="display:none;"><li>Invalid voucher code.</li></ul>
                    	</li>
                    	<li>
                    		<label for="giftvoucher_code" class="required"><em>*</em>Your Gift Voucher Code</label>
                    		<div class="input-box">
                    			<?=$form->field($checkout, 'voucherCode')->textInput()->label('');?>
                    		</div>
                    		To check your Gift Voucher balance, please click <a target="_blank" href="http://www.legj.com.au/site1/giftvoucher/index/check/">here</a>.	
                        </li>
                    	<li class="col-xs-12">
                    		<div class="input-box">
                    			<button type="button" class="button" id="giftvoucher_add">
                                    <div id="giftvoucher_cache_url" style="display:none;">http://www.legj.com.au/site1/giftvoucher/checkout/addgift</div>
                    				<input type="hidden" value="http://www.legj.com.au/site1/giftvoucher/checkout/addgift" />
                    				<span><span>Add Gift Voucher</span></span>
                    			</button>
                    			<span id="giftvoucher_wait" style="display:none;">
                    				<img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Adding Gift Voucher..." title="Adding Gift Voucher..." class="v-middle" />
                    				Adding Gift Voucher...			</span>
                    			<input type="hidden" class="required-entry" id="giftvoucher_passed" />
                    		</div>
                    	</li>
            		   <p></p>
                       <li id="giftvoucher_message">
                            <ul class="error-msg" style="display:none;"><li>Invalid voucher code.</li></ul>
                        </li>
                    </ul>    
                </dd>
                </div>
                <!-- <dt>
                    <input id="p_method_cards" value="hosted_pro" checked type="radio" name="payment[method]" title="Payment by cards or by PayPal account" class="radio" />
                    <label for="p_method_cards">Payment by cards or by PayPal account </label>
                </dt>
                <dd>
                    <ul id="payment_form_cards" style="display:none" class="form-list">
                        <li> You will be required to enter your payment details after you place an order.</li>
                    </ul>
                </dd> -->

                <div id="checkout-step-review" class="step a-item">
                    <div class="order-review" id="checkout-review-load">    
                        <div id="checkout-paypaliframe-load" class="authentication"></div>
                        <div id="checkout-review-submit">
                            <div class="buttons-set" id="review-buttons-container">
                                <?php if(Yii::$app->cart->isPaymentRequired($sessionVar) && Yii::$app->cart->isPaymentRequired('clientPortal')){  ?>
                                <?php if($paymentMethod == "braintree") { ?>
                                    <form>
                                        <div id="dropin-container"></div>
                                    </form>
                                    <button type="submit" title="Pay Now" class="button continue btn-checkout">Pay Now</button>
                                <?php }  elseif($paymentMethod == "eway"){ ?>
                                <?php $eWAY = new Eway(['model'=>$checkout]);
                                      $eWAY->renderForm($form);
                                ?>
                                <?php }  else { ?>  
                                <button type="submit" title="Checkout" class="button continue btn-checkout">Checkout</button>
                                <?php } } else { ?>
                                <button type="submit" title="Checkout" class="button continue btn-checkout">Checkout</button>
                                <?php } ?>
                                <!-- <button type="submit" title="Pay Now" class="button btn-checkout" style="width:150px"><span><span>Pay Now</span></span></button> -->
                                <!--<div class="v_m_logo pull-right">
                                    <img border="0" width="85px" alt="visa and mastercard" src="/images/visaandmastercard.jpg">
                                </div> -->
                            </div>
                        </div>
                    </div>
            <!-- <img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="right" style="margin-right:7px;"> -->
                </div>


            </dl>
        </fieldset>
        <!-- <div class="tool-tip" id="payment-tool-tip" style="display:none;">
            <div class="btn-close"><a href="#" id="payment-tool-tip-close" title="Close">Close</a></div>
            <div class="tool-tip-content"><img src="http://www.legj.com.au/skin/frontend/default/hammertons/images/cvv.gif" alt="Card Verification Number Visual Reference" title="Card Verification Number Visual Reference" /></div>
        </div>
        <div class="buttons-set" id="payment-buttons-container">
            <button type="button" class="button continue"><span><span>Continue</span></span></button>
            <span class="please-wait" id="payment-please-wait" style="display:none;">
                <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...    </span>
        </div> -->
    </div>
</li>

</ol>
<?php ActiveForm::end(); ?>
    </div>

</div>

<?php
if($paymentMethod != "eway") {
    if($braintree==true){
    	$braintree = Yii::$app->braintree;
    	$clientToken = $braintree->call('ClientToken', 'generate', []); 
    }
}    

//var_dump($clientToken);die(); 

?>

<script type="text/javascript">

function calculateGrandTotal(){

    var shippingCost = 0.00;

    var discount = 0.00;

    var subTotal = $('.sub-total span.price').attr('data-price');

    if($('.shipping-method-radio').is(':checked'))

        shippingCost = $('.shipping-method-radio:checked').length > 0 ? $('.shipping-method-radio:checked').attr("data-price") : 0;

    if($('#voucher-discount .price').attr("data-price") != '0'){

        discount = $('#voucher-discount .price').attr("data-price");

        $('.voucher-summary').show();

    }

    if($('#coupon-discount .price').attr("data-price") != '0'){

        discount = +discount + +$('#coupon-discount .price').attr("data-price");

        $('.coupon-summary').show();

    }

    console.log(discount);

    return showAsFloat(+subTotal + +shippingCost - +discount);

}

function showBTForm(){

    $('#dropin-container').show();

    $('.btn-checkout').die('click');

}

function hideBTForm(){

    $('#dropin-container').hide();

    $('.btn-checkout').click(function(){

        $('#checkout-form').submit();

        $('#checkout-form').submit();

    })

}

$(document).ready(function(){
    
     $('.control-label').remove(); /*star remove*/

    $('#checkoutform-billing_firstname').change();

    $('#checkoutform-billing_lastname').change();

    <?php 
    if($paymentMethod != "eway") {

    if($braintree == true){ ?>

    var clientToken ='<?php echo $clientToken ;?>';

    braintree.setup(clientToken, "dropin", {

        container: "dropin-container"

    });

    <?php } } ?>

    $('.login-button').click(function(){

        submitLogin();

    })

    calculateGrandTotal();

    $('.shipping-method-radio').click(function(){

        $('#shipping-price .price').html('AU$' + $(this).attr("data-price"));

        $('.grand-total .price').html("AU$" + calculateGrandTotal());

        $('.notice-msg span.price span').html('AU$'+calculateGrandTotal());

    })

    $('body').on('click', 'button.remove-voucher', function(){

        var removeButton = this;

        $.ajax({

            url: "<?=Url::to(['checkout/removevoucher'])?>",

            method: 'POST',

            data: {id: $(removeButton).attr("data-id")}

        }).success(function(data){

            data = $.parseJSON(data);

            if(data.status=="success"){

                $(removeButton).parents('li.voucher').remove();

                $('#voucher-discount .price').attr("data-price", showAsFloat(data.discount));

                $('#voucher-discount .price').html("AU$"+showAsFloat(data.discount));

                $('.notice-msg span.price span').html('AU$'+calculateGrandTotal());

                $('.notice-msg span.price span').attr('data-price', calculateGrandTotal());

                $('.grand-total .price').html('AU$'+calculateGrandTotal());

                $('.grand-total .price').attr('data-price', calculateGrandTotal());

                if(calculateGrandTotal() != "0.00"){

                    showBTForm();

                }

            }

        })

    })

    $('#giftvoucher_add').click(function(){

        $.ajax({

            url: "<?=Url::to(['checkout/applyvoucher'])?>",

            method: 'POST',

            data: {code: $('#checkoutform-vouchercode').val(), grandTotal: $('.grand-total .price').attr("data-price")}

        }).success(function(data){

            data = $.parseJSON(data);

            if(data.status=="success"){

                $('.error-msg').hide();

                $('.giftvoucher-discount-code ul').append('<li class="voucher"><strong>Gift Voucher '+data.code+' (<span class="price"><span class="">AU$'+showAsFloat(data.discountFromThis)+'</span></span>)</strong><button type="button" data-id="'+data.id+'" id="remove_voucher_'+data.id+'" title="Remove" class="remove-voucher"><img src="/images/btn_remove.gif" alt="Remove"></button><input type="hidden" name="CheckoutForm[voucherCode]"/></li>');

                $('#voucher-discount .price').html('AU$'+data.discount);

                $('#voucher-discount .price').attr('data-price', data.discount);

                $('.notice-msg span.price span').html('AU$'+calculateGrandTotal());

                $('.voucher-summary').show();

                $('.grand-total .price').html('AU$'+calculateGrandTotal());

                $('.grand-total .price').attr('data-price', calculateGrandTotal());

                if(calculateGrandTotal() == "0.00"){

                    hideBTForm();

                }

            }else if(data.status=="failed"){

                $('.error-msg').show();

                $('.error-msg').html("Invalid Voucher Code.");

            }else

                $('.error-msg').hide();

        })

    })

    // $('#p_method_giftvoucher').click(function(){

    //     $('#payment_form_giftvoucher').show();

    //     $('#payment_form_cards').hide();

    // })

    // $('#p_method_cards').click(function(){

    //     $('#payment_form_giftvoucher').hide();

    //     $('#payment_form_cards').show();

    // })

    $("#checkoutSteps").accordion({

        heightStyle: "content"

    });
    //alert('hiii');

    $(".step-title").addClass("ui-state-disabled");

    $(".step-title").first().removeClass("ui-state-disabled");

        var accordion = $("#checkoutSteps").data("uiAccordion");

        accordion._std_clickHandler = accordion._clickHandler;

        accordion._clickHandler = function( event, target ) {

            var clicked = $( event.currentTarget || target );

            if (! clicked.hasClass("ui-state-disabled")) {

                this._std_clickHandler(event, target);

            }

    };

    $('button.continue').click(function(){

        if($('input[type="radio"].authentication').length>0 && $('input[type="radio"].authentication:checked').length<1){

            alert('Please choose to register or checkout as a guest');

            return false;

        }

        $('#checkout-form').yiiActiveForm('validate');

        validateTab(this);

        var thisButton = this;

        setTimeout(function(){

            if($(thisButton).parents('li.section').find('.help-block-error').contents().length<1){

                $(thisButton).parents('li.section').next().find('div.step-title').removeClass("ui-state-disabled").click();

                $('.form-group').removeClass('has-error');

                $('.help-block-error').html('');
                if($(thisButton).hasClass("eway-pay")){
                    $('#checkout-form').submit();
                }

            }

        }, 300)

        

    });



    $('input[type="radio"].authentication').click(function(){

        if($(this).attr("id")=="login-register"){
            $('.password').show();
            $('input[type="checkbox"][name="billing[save_in_address_book]"]').parents('.no-display').show();
            $('input[type="checkbox"][name="shipping[save_in_address_book]"]').parents('.no-display').show();
        }else{
            $('.password').hide();
            $('input[type="checkbox"][name="billing[save_in_address_book]"]').parents('.no-display').hide();
            $('input[type="checkbox"][name="shipping[save_in_address_book]"]').parents('.no-display').hide();
        }

    });

    $('#opc-billing .form-control').change(function(){ 

        if($('#shipping-sameas-billing:checked').length>0){

            var billingField = this;

            $('#'+$(billingField).attr("id").replace("billing", "shipping")).val($(billingField).val());

        }

    });

    $('.stored-billing-address').change(function(){

        if($('#shipping-sameas-billing:checked').length>0){

            $('.stored-shipping-address').val($('.stored-billing-address').val());

            $('.stored-shipping-address').change();

        }

    })



    $('#shipping-sameas-billing').click(function(){

        if($(this).is(':checked')){

            $('.stored-shipping-address').val($('.stored-billing-address').val());

            $('#opc-billing .form-control').each(function(){

                var billingField = this;

                $('#'+$(billingField).attr("id").replace("billing", "shipping")).val($(billingField).val());

            })

            $('.stored-shipping-address').change();

        }

    })



    <?php if(!Yii::$app->user->isGuest){ ?>

    if($('.stored-billing-address').length > 0)

        $('#billing-address-form .fields, #billing-address-form .wide').hide();

    if($('.stored-shipping-address').length > 0)

        $('#shipping-address-form .fields, #shipping-address-form .wide').hide();

    $('.stored-billing-address').change(function(){

        if($(this).val()=="new"){

            $('#billing-address-form .fields, #billing-address-form .wide').show();

        }else{

            $('#billing-address-form .fields, #billing-address-form .wide').hide();

        }

    });

    $('.stored-shipping-address').change(function(){

        if($(this).val()=="new"){

            $('#shipping-address-form .fields, #shipping-address-form .wide').show();

            $('.no-display').css('display','block');

            //$(this).parent().next().find('.no-display').css('display','block');

            //$(this).parent().parent().addClass('hai');

        }else{

            $('#shipping-address-form .fields, #shipping-address-form .wide').hide();

            $('.no-display').css('display','none');

        }

    });

    $('#shipping-address-form .form-control').change(function(){

        $('#shipping-sameas-billing').prop('checked', false);

    })

    <?php } else {?>

            $('.no-display').css('display','none');

    <?php } ?>  

})

function validateTab(button){ 

    $(button).parents('.step').find('input, select, textarea').each(function(){ //console.log($(this).attr("id"));

        $('#checkout-form').yiiActiveForm('validateAttribute', $(this).attr("id"));

    })

}



function submitLogin () {

  var theForm, newInput1, newInput2;

  theForm = document.createElement('form');

  theForm.action = '<?=Url::to(["site/login"])?>';

  theForm.method = 'post';

  newInput1 = document.createElement('input');

  newInput1.type = 'hidden';

  newInput1.name = 'LoginForm[email]';

  newInput1.value = $('input[name="LoginForm[email]"]').val();

  newInput2 = document.createElement('input');

  newInput2.type = 'hidden';

  newInput2.name = 'LoginForm[password]';

  newInput2.value = $('input[name="LoginForm[password]"]').val();

  newInput3 = document.createElement('input');

  newInput3.type = 'hidden';

  newInput3.name = 'context';

  newInput3.value = 'checkout';

  theForm.appendChild(newInput1);

  theForm.appendChild(newInput2);

  document.getElementById('site-checkout').appendChild(theForm);

  theForm.submit();

}

</script> 

<!-- <script src="https://js.braintreegateway.com/v2/braintree.js"></script> --> 

<script>

$(document).ready(function() {

    /*if($('.stored-billing-address').length == 0){



    }*/

    



    //console.log(result);



    // $('.btn-checkout').click(function(){

    //  $('#checkout-form').yiiActiveForm('validate');

    //     validateTab(this);

    //     var thisButton = this;

    //     setTimeout(function(){

    //         if($(thisButton).parents('li.section').find('.help-block-error').contents().length<1){

    //             $('#hidden-submit').click();

    //             $('.form-group').removeClass('has-error');

    //             $('.help-block-error').html('');

    //         }

    //     }, 300)

    // })

    

    //$('.no-display').css('display','block');



    <?php if(!Yii::$app->user->isGuest){ ?>

    

    if($('.stored-billing-address').length > 0){

        var val = $('.stored-shipping-address').val();

        if (val == "new")

            $('.no-display').css('display','block');

        else

            $('.no-display').css('display','none');

    }

    <?php } ?>  

    





    $('.btn_checkout').click(function(){

        $(this).attr("type", "button");

    })



    $('body').on('click','.fa-plus-circle', function(){   

        $(this).removeClass('fa-plus-circle');

        $(this).addClass('fa-minus-circle');      

    });



    $('body').on('click','.fa-minus-circle', function(){   

        $(this).removeClass('fa-minus-circle');

        $(this).addClass('fa-plus-circle');      

    }); 



    $('body').on('click','.CollectfromStore', function(){   //console.log($('.collect_store').html());

        

        $(this).closest('.radio').append($('.collect_store_div').html());

        //$(this).parent().next().css('display','block');
    $(this).parent().next().find('.collect_store').css('display','block');
    

    }); 



    $('body').on('click','.Shiptomyaddress', function(){   //console.log($('.shipping-method-radio:checked').parent().text());

        $('.collect_store').css('display','none');

    }); 



    // $('.billing_continue').click(function(){

    //  var i=0;

    //  $("#checkout-step-billing .help-block-error").each(function() {

    //      if(!$(this).is(':empty')){

    //          i=1;

    //      }

    //  }); 

    //  if(i==0) {

    //      $('.checkout_billing').css('display','block');  

    //      if($('.checkout_billing').is(':empty')){

 //             $('.checkout_billing').append($('#checkoutform-billing_firstname').val()+' '+$('#checkoutform-billing_lastname').val()).append('<br />' );

    //          $('.checkout_billing').append($('#checkoutform-billing_company').val()).append('<br/>');

    //          $('.checkout_billing').append($('#checkoutform-billing_street').val()).append('<br/>');

    //          $('.checkout_billing').append($('#checkoutform-billing_city').val()+','+$('#checkoutform-billing_state option:selected').text()+','+$('#checkoutform-billing_postcode').val()).append('<br/>');

    //          $('.checkout_billing').append($('#checkoutform-billing_country option:selected').text()).append('<br/>');

    //          $('.checkout_billing').append('T:'+$('#checkoutform-billing_phone').val()).append('<br/>');

    //          //$('.checkout_billing').append('F:'+$('#checkoutform-billing_fax').val()).append('<br/>');

    //      } 



    //  }   

    // });



 //    $('.shipping_continue').click(function(){

 //     var j=0;

 //     $("#checkout-step-shipping .help-block-error").each(function() {

    //      if(!$(this).is(':empty')){

    //          j=1;

    //      }

    //  });

 //         if(j==0) {

    //      $('.checkout_shipping').css('display','block');

    //      if($('.checkout_shipping').is(':empty')){

    //          $('.checkout_shipping').append($('#checkoutform-shipping_firstname').val()+' '+$('#checkoutform-shipping_lastname').val()).append('<br />' );

    //          $('.checkout_shipping').append($('#checkoutform-shipping_company').val()).append('<br/>');

    //          $('.checkout_shipping').append($('#checkoutform-shipping_street').val()).append('<br/>');

    //          $('.checkout_shipping').append($('#checkoutform-shipping_city').val()+','+$('#checkoutform-shipping_state option:selected').text()+','+$('#checkoutform-shipping_postcode').val()).append('<br/>');

    //          $('.checkout_shipping').append($('#checkoutform-shipping_country option:selected').text()).append('<br/>');

    //          $('.checkout_shipping').append('T:'+$('#checkoutform-shipping_phone').val()).append('<br/>');

    //      }

    //  }           

    // }); 



  //   $('.shipping_method_continue').click(function(){

  //       $('.checkout_shipping_method').css('display','block');

        // $('.checkout_shipping_method').append($('.shipping-method-radio:checked').parent().text()).append('<br />' );

  //   });    

});    

</script>
