<?php 

    use frontend\components\Helper;
    use kartik\rating\StarRating;
    use common\models\User;
    use yii\helpers\ArrayHelper;

    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();

    if (!\Yii::$app->user->isGuest) { 
        $user=User::findOne(\Yii::$app->user->identity->id);

        $wishlist = ArrayHelper::map($user->wishlistItems,'productId','user');
        //var_dump($user->wishlistitems);die();
    }

?>

<div class="items-bg">
    <?php 
      if (!\Yii::$app->user->isGuest) {
        if(array_key_exists($model->id, $wishlist)) { ?>
    <div class="fav-mainbg"><a class="fa fa-heart" id='wishlist<?=$model->id?>'></a> </div>
    <?php }  else {?>
    <div class="fav-mainbg"><a class="fa fa-heart-o" id='wishlist<?=$model->id?>'></a> </div>
    <?php } } ?>

    <div class="product-img">
        <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax=0>
            <img src="<?=$thumbImagePath?>" alt="">
        </a>
    </div>
    <div class="prdct-details">
        <h5>
            <?=$model->name?>
        </h5>
        <div class="rating-pdt">
            <?=StarRating::widget([
                'name' => 'rating',
                'class'=>'rating-star',
                'value' => $model->rating,
                'pluginOptions' => ['disabled'=> true, 'showClear'=> false, 'showCaption'=> false]
            ]);?>
        </div>
        <div class="price">
            <?=Helper::money($model->price)?>
        </div>
        <div class="add-cart-bg"> 
            <form method="POST" id="product_cart_form_<?=$model->id?>" action="<?=\yii\helpers\Url::to(['cart/add'])?>">
                <input type="hidden" id="products-id" name="Products[id]" value="<?=$model->id?>">
                <input type="hidden" name="qty" value="1"/>
            </form>
            <!-- <a href="<?php //\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax="0">
                <i class="fa fa-shopping-cart"></i> Add to cart
            </a> -->
            <a class="add-to-cart" id="<?=$model->id?>" href="javascript:;"><i class=" fa fa-shopping-cart" ></i>Add to Cart</a> 
        </div>
    </div>
</div>

<script type="text/javascript">
$('#wishlist<?=$model->id?>').on("click", function(){
        <?php  
        if(!\Yii::$app->getUser()->isGuest) { 
          $login_user = \Yii::$app->user->identity;?>;
          var product_id=<?= $model->id; ?>;
          var user_id=<?= \Yii::$app->user->identity->id ?>;
          var store_id=<?= \Yii::$app->params['storeId']?>;
        <?php } ?>
          $.ajax({
              type: "POST",
              url: "<?=Yii::$app->urlManager->createUrl(['wishlist-items/create'])?>",                
              data: "&product_id=" + product_id + "&user_id=" + user_id + "&store_id=" + store_id ,
              dataType: "html", 
              success: function (data) {
              }        
          });
      }); 
</script>