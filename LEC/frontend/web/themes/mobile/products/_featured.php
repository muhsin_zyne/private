<?php 

use frontend\components\Helper;

?>

<li>
    <a title="Sterling Silver Heart Ring" class="Xpreview" href="#">   
        <img src="themes/site1/images/MP3752_1.png" width="200" height="175" alt="#"> 
    </a>
    <h3>    
        <a href="#" title="Sterling Silver Heart Ring"><?=$model->name ?> </a>
    </h3>
    <div class="FeaturedPrice">
        <div class="price-box">
            <span id="product-price-469" class="regular-price">
                <span class="price"><?=Helper::money($model->price)?></span>                
            </span>
        </div>
    </div>
    <div class="cartButton">
        <a href="#">Buy now</a>
     </div>
</li>
        
