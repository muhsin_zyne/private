<?php
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	use yii\widgets\ListView;
	use yii\widgets\ActiveForm;
	use frontend\components\Helper;
	use yii\helpers\Url;
	use kartik\rating\StarRating;
?>

<?php
	
	$this->title = $product->name;
	$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
	$id = $product->id;
?>
<div class="clearfix">	</div>
  <div class="main_content">
    <div class="product_det">
        <div class="">
        	
            <div class="col-md-12">
            <div class="product_head"><?= Html::encode($this->title)?></div>
                <div class="category_image"> 
                	<a href="#"><img src="<?=$baseImagePath?>" width="350" height="350"></a> 
                </div>
                <span class="sku-text" style="color: #ccc;"><?=$product->sku?></span>
               <!-- <div class="enlarge">Please click on the image(s) to enlarge</div>-->
            </div>
            <div class="col-mgd-12">
            <div class="cate_detail">
                  		<div class="category_title">
                        	<!--<span class="">Email to a Friend</span>-->
                            
                            <div class="filter pull-right">
                                    <div class="fb-like" data-href="https://www.facebook.com/lecbairnsdale" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>                            
							</div>
                            <div class="clearfix"></div> 
                        </div>
                <div class="product_detail">
                	
                   	<div class="price">Price: <?=Helper::money($product->price)?></div><br>
                    <p><?=$product->description?> </p>

                    <div class="pdt-detailrating">
                    	<?=StarRating::widget([
									'name' => 'rating_2',
									'value' => $product->rating,
									'pluginOptions' => [
											'readonly' => true,
											'showClear' => false,
											'showCaption' => false,
									],
							]);
							?>
                    </div>

                    <div class="detail_cart">
                       <!-- <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>-->
                       
                       <!--  <div class="price pull-left"><?=Helper::money($product->price)?></div> -->
						<div class="product-options" id="product-options-wrapper">
		    			<?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => isset($_REQUEST['positionId'])? Url::to(['cart/update', 'positionId' => $_REQUEST['positionId']]) : Url::to(['cart/add'])]); ?>
			    			
	                                <label class="quan" for="qty">Qty</label>
	                                <input type="text" class="" title="Qty" value="1" maxlength="12" id="qty" name="qty">
	                                <?=Html::activeHIddenInput($product, 'id', ['name' => 'Products[id]']);?>
	                               
	                           
        					<dl>
	        					<?php if($product->typeId == "configurable"){ ?>
	        						<div class="config-attributes">
	        							<?=$product->renderConfigurableFields($form);?>
	        						</div>
	        					<?php }elseif($product->typeId == "bundle"){ ?>
        							<div class="config-attributes">
        								<?=$product->renderBundleFields($form);?>
        							</div>
        						<?php } ?>
	        				</dl>
            			 <a title="Add to Cart" class="add_to_cart btn btn-block" href="javascript:;" onclick="$('form#products-form').submit();">Add to Cart</a>	
						<?php ActiveForm::end(); ?>
						</div>


                        <div class="clearfix"></div>
                         <!--<div class="pull-left">Be the first to review this product</div>
                        <div class="pull-right wishlist"> <a href="#">Add to Wishlist</a></div>-->
                        <div class="clearfix"></div>
                    </div>

                    <?php 
						if (!\Yii::$app->user->isGuest) {
							$form = ActiveForm::begin(['id' => 'review-form']); 
							echo $form->field($userReview, 'stars')->widget(StarRating::classname(), [
								'pluginOptions' => ['size'=>'xs','disabled'=>false, 'showClear'=>false,'starCaptions'=>false],
								'pluginEvents' => [
								"rating.change" => 'function() {  
										if(confirm("Are you sure you want to rate this product?")){
											$(".review_btn").click();
										}

										else
											$("#productreviews-stars").rating("reset");
								}',
												 
								],
							]);
						}
								?>

                    <b>Product Details</b>

                     <?php /* DetailView::widget([

        					'model' => $product,
        					'attributes' => $product->detailViewAttributes,
    					]);*/
    				 ?>
                    <?php echo $this->render('/products/_getmeta', compact('product')); ?>

                    <?php echo $this->render('/products/_getmeta', compact('product')); ?>


    				<?=$form->field($userReview, 'productId')->hiddenInput(['class'=>'form-control','value'=>$product->id])->label(false) ?>
										<?=Html::submitButton($userReview->isNewRecord ? 'Post Review' : 'Update Review', ['class' => 'review_btn','style'=>'display:none']) ?>
								<!-- </div> --> 

                </div>
            </div>    
            </div>
        </div>
        <?php /*?><div class="row">
        	<div class="col-md-4">
               <div class="fb-page" data-href="https://www.facebook.com/LeadingEdgeJewellersHO" data-width="312" data-height="312" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"></div>
            </div>

          <!--  <div class="col-md-8">
               	<div class="Featured Product_Featured">
            		<ul>

        	<?php  echo ListView::widget([
 				'id' => 'category-products',
 				'dataProvider' => $dataProvider,
 				'itemView' => '/products/_featured',
 				'itemOptions' => ['class' => 'items_featured']
			]);
			?>

					</ul>
				</div>
			</div>	-->	

        </div><?php */?>
    </div>
</div>        

<div class="clearfix"></div>

       

<script type="text/javascript">
	$(document).ready(function(){
		$('.attribute-select').attr('disabled', 'disabled');
		$('.config-attributes .form-group:first-child select').attr('disabled', false);
		$(".attribute-select").change(function(){
	   	var selected = $(this).val();
	   	var attrId1 = $(this).attr('data-attrid');
	   	//var price = <?=$product->price?>;
	    var prices =  $(".sum").text();
	   	var price = prices.replace("AU$", "");
		$('.config-attributes .form-group select').attr('disabled', false);

		//$(this).next().addClass('hai');
		$.ajax({
	   		  url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=products/getoptions',
	          type: 'POST',
	          data: {selected: selected, attrId1: attrId1, id:<?=$id?>, price: price},
	                success: function(data)
	                {
	                 	$(".sum").html(data);
	                 	console.log(data);
					}
		});
	});
});
</script>

<script type="text/javascript">
	$(document).ready(function(){
	
		$('.bundle_dropdown').on("change", function(){  
				var isdefined = $('.bundle_dropdown option:selected').attr('data-isuserdefined');
				var defaultQty = $('.bundle_dropdown option:selected').attr('data-defaultQty');
				var id = $(this).val();
			
				if(isdefined==1){
					$('.qtytext').remove();
					$('.bundle_dropdown').after('<div class="qtytext"><input type="text" class="item_qty" onchange="findvalues()" name="Products[BundleItems][qty]['+id+']" value="'+defaultQty+'"></div>');
				}
				else {
						if(id != ""){
						$('.qtytext').remove();
						$('.bundle_dropdown').after('<div class="qtytext">'+defaultQty+'<input type="hidden" class="item_qty" name="Products[BundleItems][qty]['+id+']" value="'+defaultQty+'"></div>');
					}
						else { $('.qtytext').hide(); }
				}
				findvalues();
		});
		$('.bundle_radio').on("click", function(){
			values = [];
			var params = $('.bundle_radio:checked').attr('data-params');
			values =  $.parseJSON(params);
			var id = $(this).val();
			$.each( values, function( key, value ) {
  				if(value['data-isUserDefined']==1){
					$('.qtytextradio').remove();
					$('.'+id).next().after('<div class="qtytextradio">Qty: <input type="text" class="item_qtyradio" onchange="findvalues()" name="Products[BundleItems][qty]['+id+']" value="'+value['data-defaultQty']+'"></div>');
				}
				else {
						if(id != ""){
							$('.qtytextradio').remove();
							$('.bundle_radio label').after('<div class="qtytextradio">'+value['data-defaultQty']+'<input type="hidden" class="item_qtyradio" name="Products[BundleItems][qty]['+key+']" value="'+value['data-defaultQty']+'"></div>');
						}
					else { $('.qtytextradio').hide(); }
				}	
  			
			});
			findvalues();
		});	
		$('.bundle_checkbox').on("click", function(){ 
			findvalues();
		});	
	
		$('.bundle_multisel').on("click", function(){
			findvalues();
		});	
	});	
</script>

<script type="text/javascript">

function findvalues() {
	selected = [];
	selectedqty = [];
	var prices =  $(".price").text();
	var cur_price = prices.replace("AU$", "");
	var price = <?=$product->price?>;
	$('.bundle_checkbox:checked').each(function(){
		selected.push(this.value);
	});
	$('.bundle_radio:checked').each(function(){
   		selected.push(this.value); 
	});
	if($('.bundle_dropdown').val() != ""){
		selected.push($('.bundle_dropdown').val());
		qty = $('.item_qty').val(); 
	}
	if($('.bundle_multisel').val() != null){
		for( var i = 0, xlength = $('.bundle_multisel').val().length; i < xlength; i++){
			selected.push($('.bundle_multisel').val()[i]);	
		}	
	}
	if($('.item_qty').val() != ""){ 
		id = $('.bundle_dropdown').val();
		qty = $('.item_qty').val(); 
		var object = {};
		object[id] = qty;
		selectedqty.push(object);
	}
	if($('.item_qtyradio').val() != ""){
		id = $('.bundle_radio').val();
		qty = $('.item_qtyradio').val(); 
		var object = {};
		object[id] = qty;
		selectedqty.push(object);
	}
	$.ajax({
		url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=products/test1',	
		type: 'POST',
		data: {selected: selected, price: price, cur_price: cur_price, selectedqty: selectedqty},
		       success: function(data){
		            $(".price").html(data);
				}
	});
}

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#review-form').submit(function(e) {
            data = $('#review-form').serialize();
            $.ajax({
                type: 'POST',
                url: '<?php echo ($userReview->isNewRecord) ? Yii::$app->urlManager->createUrl(["product-reviews/create"]) : Yii::$app->urlManager->createUrl(["product-reviews/update","id"=>$userReview->id])?>',
                data: data,
                success: function(msg){  console.log(msg);
                    $('.review_wrapper').html('Thanks for your review of <?=$product->name?>');
                }
            });
            return false;
        });
    }); 
</script>
