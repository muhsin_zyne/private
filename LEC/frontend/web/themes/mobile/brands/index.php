<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use frontend\components\ProductFilter;
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\widgets\Breadcrumbs;
?>

<?php 
    $this->title = "OUR BRANDS";
    $this->params['breadcrumbs'][] = "Brands";
?>

    <div class="clearfix mrg-15"></div>
    <div class="content">
        <div class="content_area_home brand_list">
            <div class="page-title">
                <div class="col-xs-12"><h2><?=$this->title?></h2></div>
            </div>
            <div class="category-products">
                <div class="row brand-row">
                    <?php \yii\widgets\Pjax::begin(['id' => 'category-products']); ?>
                    <?=ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '/brands/_brandlogo',
						'summary' => '',
                        'itemOptions' => ['class' => 'items brand_logos col-xs-6']
                    ]);
                    echo "<script> $(document).ready(function() { $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>'); }) </script>";
                    ?>
                    <?php  \yii\widgets\Pjax::end();  ?> 
                </div>
                <div class="clear"></div>  
            </div>
        </div>
    </div>