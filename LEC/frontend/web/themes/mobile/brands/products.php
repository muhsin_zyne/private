<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\MobileProductFilter;

?>

<style type="text/css">
    .fixed-nav .outer-search{ width:66.6667%;}
    .filter-outer{ display:inline-block;}
</style>

    <div class="modal fade bs-example-modal-sm" id="sortModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">SORT</h4>
          </div>
          <div class="modal-body">
            
            <div class="radio-group-bg">
                <div class="radio">
                  <label>Sort by New <div class="radio-type"><input type="radio" class="sort-val" name="optradio" value="dateAdded"></div></label>
                </div>
                <div class="radio">
                  <label>Sort by Price <div class="radio-type"><input type="radio" class="sort-val" name="optradio" value="price"></div></label>
                </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- Small modal -->


    <!-- Large modal -->
    <div class="modal fade bs-example-modal-lg" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- modal-header --> 
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">All Filters</h4>
            <button type="button" class="btn btn-default">Clear All</button>
            
          </div>
          <!--End modal-header --> 
          <!-- modal-body 
          <div class="modal-body"> -->
                
                  <?php
                    MobileProductFilter::begin([
                        'basedModel' => $brand,
                        'query' => clone $products->query,
                        'attributes' => $brand->filterAttributes, 
                      ]);
                    MobileProductFilter::end();
                  ?>        
        </div>
      </div>
    </div>
    <!-- Large modal -->

<div class="main_content">
    <div class="cate_detail">
        <div class="category_title">
            <span class="category_name"><?=$brand->title?></span>
            <span class="filter"><span class="pull-right"><a href="#">Sort</a></span><span class="pull-right"><i class="fa fa-caret-down"></i></span></span>
            <div class="clearfix"></div> 
        </div>
        <section class="innerpage-list">
          <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
          <?php 
            echo ListView::widget( [
                'id' => 'testclass',
                'dataProvider' => $products,
                'itemView' => '/products/_product',
                'summary' => '',
                'itemOptions' => ['class' => 'col-xs-6 items-padd']
            ] );
          ?>
          <?php \yii\widgets\Pjax::end(); ?>   
        </section>

    </div>
</div>    	
<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.radio .sort-val', function(){ console.log($(this).val());
            $('#filter-form .sort').val($(this).val());
            filterproducts();
            $("#sortModal").modal('hide');
        })
    })
</script>