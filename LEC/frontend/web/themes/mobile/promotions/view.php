<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use frontend\components\Helper;
?>

<div class="main_content">
    <div class="cate_detail promo_list">
        <div class="category_title">
            <span class="category_name"><?=$promotion->title?></span>
            <span class="filter">
                <span class="pull-right"><a href="#">Sort</a></span>
                <span class="pull-right"><i class="fa fa-caret-down"></i></span>
            </span>
        </div>
        <div class="category_list">
            <div class="promotion-start"><span>Start Date:</span><?=Helper::date($storePromotion->startDate)?></div>
            <div class="promotion-start"><span>End Date:</span><?=Helper::date($storePromotion->endDate)?></div>
            <button class="hide-catalog">Hide catalogue</button>
            <div class="clearfix"></div>
            <div class="issu-promotion">
                <?=$promotion->catalog?>
            </div>
            <div class="toolbar">
                <div class="clearfix"></div>
            </div>
            <ul>
                <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                <?php 
                    $products->pagination->pageSize = 6;
                    echo ListView::widget( [
                        'id' => 'testclass',
                        'dataProvider' => $products,
                        'itemView' => '/categories/_product',
                        'summary' => '',
                        'itemOptions' => ['class' => 'items']
                    ] );
                ?>
                <?php  \yii\widgets\Pjax::end();  ?> 
            </ul>
        </div>
    </div>
</div>    

<script type="text/javascript">
$(document).ready(function(){
    $('body').on('change', '.limiter select', function(){
        $('#filter-form .page-size').val($(this).val());
        filterproducts();
    });

    $('body').on('change', '.sort-by select', function(){
        $('#filter-form .sort').val($(this).val());
        filterproducts();
    });

    $('body').on('click', '.hide-catalog', function(){
        $('.issu-promotion').toggle();
        if($('.issu-promotion:visible').length == 0)
        {
            $('.hide-catalog').text('Show Catalogue');
        }
        else{
            $('.hide-catalog').text('Hide Catalogue');
        }
    });    
})
</script>