<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
?>

<!-- <link rel="stylesheet" href="http://www.legj.com.au/skin/frontend/default/mobile_epicajewellers/css/normalize.min.css" />
<link rel="stylesheet" href="http://www.legj.com.au/skin/frontend/default/mobile_epicajewellers/css/ion.rangeSlider.css" />
<link rel="stylesheet" href="http://www.legj.com.au/skin/frontend/default/mobile_epicajewellers/css/ion.rangeSlider.skinFlat.css" />
<script type="text/javascript" src="http://www.legj.com.au/skin/frontend/default/default/jqueryfiles/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="http://www.legj.com.au/skin/frontend/default/default/jqueryfiles/jQuery-noconflict.js"></script>
<script type="text/javascript" src="http://www.legj.com.au/skin/frontend/default/mobile_epicajewellers/js/ion.rangeSlider.min.js"></script> -->


<script>
jQuery.noConflict();
</script>
<div class="main_content">
    <div class="product_det">
        <div class="row">
            <div class="col-md-6">
             <div class="product_detail">

				<h4>SEND A PHOTO</h4>	
                <p>Need help finding that special piece for that special someone... Simply send us a photo and a budget of what you have in mind!</p>
                
<?php

$form = ActiveForm::begin(['id'=>'take_a_photo','options'=>['class'=>'form','enctype'=>'multipart/form-data']]);
?>
	<input type="hidden" name="range" id="range" value="" />
    <label><b>Price Range</b></label><br />
    <span style="text-align: left; font-style:italic; font-size: 10px; color:#545454; width:100%; float:left;">Please drag the slider and set your budget-range</span>
    <div style=" padding: 10px 0; overflow:hidden; width:100% !important; position:relative;">
 
    <input type="text" id="range_1" />
    <input type="hidden" name="storeId" value=<?=Yii::$app->params['storeId']?>"">

</div>
<?= $form->field($model,'name')->label('Name');?>
<?= $form->field($model,'email')->input('email')->label('Email');?>
<?= $form->field($model,'phone')->label('Phone');?>
<p style="font-size: 10px; align: center; font-style: italic;">Please fill the details above before uploading the photo</p>
<div class="choose_file"><?= $form->field($model,'photo')->fileInput()->label('Upload or Take a Photo');?></div>
<?= $form->field($model,'comment')->textarea()->label('Comments');?>

<?= Html::submitButton('Send', ['class' => 'btn btn-block', 'name' => 'submit-button']) ?>

<?php ActiveForm::end();?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/jQuery-noconflict.js"></script>

<script type="text/javascript">
      $(document).ready(function() {
    	//$('#name').on( "touchstart", function(){});
        $("#range_1").ionRangeSlider({
            min: 500,
            max: 10000,
            from: 500,
            to: 10000,
            type: 'double',
            step: 100,
            prefix: "$",
            
            hasGrid: false,
			onChange: function (obj) {     
        get(obj);
			}});

    });
	function get(obj)
	{
		jQuery("#range").val("$"+parseInt(obj.fromNumber) + " and " + "$"+parseInt(obj.toNumber));
	}
//-->
</script>
