<?php  
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\components\FrontNavBar;
use frontend\components\MainMenu;
use common\models\Stores;
use yii\helpers\BaseUrl;
use common\models\Configuration;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$store = Stores::findOne(Yii::$app->params['storeId']);
$facebookUrl = $store->getfacebookUrl();
$youtubeUrl = $store->getyoutubeUrl();
$linkedinUrl = $store->getlinkedinUrl();
$googleplusUrl = $store->getgoogleplusUrl();
$twitterUrl = $store->gettwitterUrl();
$instagramUrl = $store->getinstagramUrl();
$pinterestUrl = $store->getpinterestUrl();

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <?= Html::csrfMetaTags() ?>
    <title>Leading Edge Computers</title>
    <?php $this->head() ?>
    <style></style>

    <!-- Bootstrap -->

    <link href="/themes/mobile/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/themes/mobile/css/font-awesome-4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/themes/mobile/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/themes/mobile/css/normalize.css" />
    <!--<link rel="stylesheet" type="text/css" href="/themes/mobile/css/icons.css" />
    <link rel="stylesheet" type="text/css" href="/themes/mobile/css/component.css" />-->
    <link href="/css/star-rating.css" rel="stylesheet">
    <link href="/css/ion.rangeSlider.css" rel="stylesheet" type="text/css">
    <link href="/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css"/>
    <link href="/themes/mobile/css/style.css" type="text/css" rel="stylesheet">
    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/ajax-update.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/themes/mobile/js/modernizr.custom.js"></script>
    <script src="/js/ion.rangeSlider.js"></script>
    <script src="/js/easyResponsiveTabs.js"></script>
    <script src="/themes/mobile/js/owl.carousel.js"></script>
    <script src="/js/scripts.js"></script>
    <script type="text/javascript" src="/js/jquery.gritter.min.js"></script>
    <script type="text/javascript" src="/js/wl_Form.js"></script>
    <script src="/js/star-rating.js"></script>
    <script type="text/javascript" src="/js/bootstrap-modal.js"></script>

    <?php 
    $googleAnalyticsId = Configuration::findSetting('google_analytics', Yii::$app->params['storeId']); 
    if ($googleAnalyticsId != "") {
    ?>
      <script>

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



        ga('create', '<?= $googleAnalyticsId ; ?>', 'auto');

        ga('send', 'pageview');



      </script>
    <?php } ?>
    </head>
    
    <body>
	<?php $this->beginBody(); ?>
<div class="main-bg">
      <div class="mp-pusher" id="mp-pusher">
        <nav id="mp-menu" class="mp-menu">
          <div class="mp-level"><h2 class="icon icon-world mp-back">Menu<a class=" fa fa-close" href="#"></a></h2>
           <?php echo MainMenu::widget(['is_Mobile'=>true]); ?> </div>
        </nav>
      </div>
      <div class="scroller">
    <header>
          <div class="logo-bg">
        <div class="logo"> <a href="<?=\yii\helpers\Url::to(['/'])?>"><img src="<?=Yii::$app->params["rootUrl"].$store->logoPath?>" alt=""/></a> </div>
      </div>
          <div class="nav-bg">
        <div class="col-xs-12">
              <ul>
            <li class="col-xs-3">
                  <div class="ico-bg"><a id="trigger">
                    <div class="ico-bar"></div>
                    </a></div>
                </li>
            <li class="col-xs-3">
                  <div class="ico-bg">
                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                    <div class="ico-user"></div>
                    </a>
                    <?php if (!\Yii::$app->user->isGuest) { ?>
                    <ul class="dropdown-menu">
                        <!-- <li><a href="<?php //\yii\helpers\Url::to(['site/dashboard'])?>">Account Dashboard</a></li> -->
                        <li><a href="<?=\yii\helpers\Url::to(['site/account'])?>">Account Information</a></li>
                        <li><a href="<?=\yii\helpers\Url::to(['site/addressbook'])?>">Address Book</a></li>
                        <li><a href="<?=\yii\helpers\Url::to(['site/myorders'])?>">My Orders</a></li>
                        <li><a href="<?=\yii\helpers\Url::to(['site/wishlist'])?>">My Wishlist</a></li> 
                        <li><a href="<?=\yii\helpers\Url::to(['site/logout'])?>">Logout</a></li>
                        <?php if(isset(Yii::$app->session['clientPortal'])) { ?>  
                          <li><a href="<?=\yii\helpers\Url::to(['client-portal/view'])?>">Client Portal</a></li>
                        <?php } else { ?>
                          <li><a href="<?=\yii\helpers\Url::to(['client-portal/apply'])?>">Client Portal</a></li>
                        <?php } ?>
                        <?php if(isset(Yii::$app->session['schoolByod']) || isset(Yii::$app->session['studentByod'])) { ?>  
                          <li><a href="<?=\yii\helpers\Url::to(['byod/view'])?>">BYOD</a></li>
                        <?php } else { ?>
                          <li><a href="<?=\yii\helpers\Url::to(['byod/apply'])?>">BYOD</a></li>
                        <?php } ?>
                      </ul>
                  <?php } else { ?>
                    <ul class="dropdown-menu">
                        <li><a href="<?=\yii\helpers\Url::to(['site/login'])?>">Login</a></li>
                        <li><a href="<?=\yii\helpers\Url::to(['site/signup'])?>">Register</a></li>
                        <?php if(isset(Yii::$app->session['clientPortal'])) { ?>  
                          <li><a href="<?=\yii\helpers\Url::to(['client-portal/view'])?>">Client Portal</a></li>
                        <?php } else { ?>
                          <li><a href="<?=\yii\helpers\Url::to(['client-portal/apply'])?>">Client Portal</a></li>
                        <?php } ?>
                        <?php if(isset(Yii::$app->session['schoolByod']) || isset(Yii::$app->session['studentByod'])) { ?>  
                          <li><a href="<?=\yii\helpers\Url::to(['byod/view'])?>">BYOD</a></li>
                        <?php } else { ?>
                          <li><a href="<?=\yii\helpers\Url::to(['byod/apply'])?>">BYOD</a></li>
                        <?php } ?>
                      </ul>
                  <?php } ?>    
              </div>
                </li>
            <li class="col-xs-3">
                  <div class="ico-bg">
                 
                    <a href="<?=\yii\helpers\Url::to(['site/wishlist'])?>"><div class="ico-fav"></div></a>
                    
               <!--  <ul class="dropdown-menu">
                      <li><a href="<?php //\yii\helpers\Url::to(['checkout/index'])?>">Checkout</a></li>
                      <li><a href="<?php //\yii\helpers\Url::to(['site/wishlist'])?>">Wishlist</a></li>
                    </ul> -->
              </div>
                </li>
            <li class="col-xs-3">
                  <div class="ico-bg">
                <div class="ico-cart">
                      <div class="badge"><a href="<?=\yii\helpers\Url::to(['cart/view'])?>">
                    <?=count(Yii::$app->cart->positions)?>
                    </a></div>
                    </div>
                </div>
                </li>
          </ul>
            </div>
      </div>
    </header>
    
    
    <?php if(Yii::$app->session->getFlash('error'))

              { ?>
    <div class="alert alert-danger alert-dismissable"> <i class="icon fa fa-ban"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?= Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php } ?>
    <?php if(Yii::$app->session->getFlash('success'))

              { ?>
    <div class="alert alert-success alert-dismissable"> <i class="icon fa fa-check"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?= Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php } ?>

    <div id="filter-page">
    <section>
      <div class="fixed-nav">
        <div class="col-xs-12 padding-zero outer-search">
          <div class="search-bg">
            <form role="search" class="navbar-right" id="search_mini_form"  onsubmit="return validheadersearch();" action="/site/search" method="get">
              <div class="input-group col-xs-12">
                <input type="text" class="form-control input-lg search" placeholder="Search here" id="searchname" name="q"/>
                <span class="input-group-btn">
                <button class="btn btn-lg" type="submit"> <i class="fa fa-search"></i> </button>
                </span> </div>
            </form>
          </div>
        </div>

        <div class="col-xs-2 padding-zero filter-outer">
                <!-- Start sort-btn -->
                <a href="" data-toggle="modal" data-target=".bs-example-modal-sm" class="sort-filter"><div class="sort-btn">SORT</div></a>
                <!-- End sort-btn -->
        </div>
            <!-- End col-xs-2 -->
            <!-- Start col-xs-2 -->
        <div class="col-xs-2 padding-zero filter-outer">
                <!-- Start filter-btn -->
          <a href="" data-toggle="modal" data-target=".bs-example-modal-lg" class="sort-filter"><div class="filter-btn">FILTER</div></a>
                <!-- End filter-btn -->
        </div>
      </div>  
    </section>


    <?= $content ?>
    </div>
    
    <!-- Start footer -->
    
    <section> 
      
      <a href="tel:<?=$store->phone?>">
        <div class="col-xs-6 bottom-icons">
          <div class="bottom-icons-bg"><i class="fa fa-phone"></i></div>
          <h4>Call us</h4>
        </div>
      </a> 

      <?php if (!\Yii::$app->user->isGuest) { ?>
        <a href="<?=\yii\helpers\Url::to(['site/logout'])?>">
          <div class="col-xs-6 bottom-icons border-right">
            <div class="bottom-icons-bg"><i class="fa fa-unlock"></i></div>
            <h4>Logout</h4>
          </div>
        </a> 
      <?php } else { ?>
        <a href="<?=\yii\helpers\Url::to(['site/login'])?>">
          <div class="col-xs-6 bottom-icons border-right">
            <div class="bottom-icons-bg"><i class="fa fa-lock"></i></div>
            <h4>Login</h4>
          </div>
        </a> 
      <?php } ?>  
          
    </section>
    
    <footer>
          <h1><a href="<?=\yii\helpers\Url::to(['/brands'])?>">See Our Brands <i class="fa fa-angle-right"></i></a></h1>
          
          <!-- Start bottom-logo -->

          <div class="col-xs-12 footer-logo">
           <div class="logo-bg">
            <div class="bottom-logo">
             <a href="<?=\yii\helpers\Url::to(['/'])?>"><?= $store->mainLogo?> </a>
            </div>
           </div> 
          </div>

          <!-- End bottom-logo -->   
       
          <!-- Start col-xs-12 -->
          
          <div class="col-xs-12">
        <div class="bottom-links">
              <ul>
                <li><a href="<?=\yii\helpers\Url::to('@web/'.'about-us.html')?>">About Us</a></li>
                <li><a href="<?=\yii\helpers\Url::to('@web/'.'collection-returns.html')?>">Collection & Returns</a></li>
                <li><a href="<?=\yii\helpers\Url::to('@web/'.'terms-conditions.html')?>">Terms & Conditions</a></li>
                <li><a href="<?=\yii\helpers\Url::to(['site/contact'])?>">Contact Us</a></li>
              </ul>
            </div>
      </div>
          
          <!-- End col-xs-12 --> 
          
          <!-- Start social-bg -->
          
        <div class="social-bg">
            <div class="col-xs-12">
                <?php if($facebookUrl!=''){ ?><a href="<?=$facebookUrl?>" target="_blank"><i class="fa fa-round fa-facebook"></i></a><?php } ?>
                <?php if($youtubeUrl!=''){ ?><a href="<?=$youtubeUrl?>" target="_blank"><i class="fa fa-round fa-youtube"></i></a><?php } ?>
                <?php if($linkedinUrl!=''){ ?><a href="<?=$linkedinUrl?>" target="_blank"><i class="fa fa-round fa-linkedin"></i></a><?php } ?>
                <?php if($googleplusUrl!=''){ ?><a href="<?=$googleplusUrl?>" target="_blank"><i class="fa fa-round fa-google"></i></a><?php } ?>
                <?php if($twitterUrl!=''){ ?><a href="<?=$twitterUrl?>" target="_blank"><i class="fa fa-round fa-twitter"></i></a><?php } ?>
                <?php if($instagramUrl!=''){ ?><a href="<?=$instagramUrl?>" target="_blank"><i class="fa fa-round fa-instagram"></i></a><?php } ?>
                <?php if($pinterestUrl!=''){ ?><a href="<?=$pinterestUrl?>" target="_blank"><i class="fa fa-round fa-pinterest"></i></a><?php } ?>  
                <p>Copyright © <?php echo $store->title; ?>&nbsp;<?= date('Y') ?></p>
            </div>
        </div>
          
          <!-- End social-bg --> 
          
        </footer>
    
    <!-- End footer --> 
    
  </div>
    </div>
<?php 

    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);

    $this->endBody() 

?>

</body>
    </html>
<?php $this->endPage() ?>
<script type="text/javascript">
  function validheadersearch() {
      var senames = document.getElementById('searchname').value;
      if(senames=="") {
          alert('search field cannot be empty !!');
          return false;
      }
  }

</script>

<script src="/themes/mobile/js/classie.js"></script>
<script src="/themes/mobile/js/mlpushmenu.js"></script>
<script>

      new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ), {
        type : 'cover'
      } );

       $(".mp-back" ).click(function() {
          $( ".mp-pusher" ).addClass('back-drop');
        });
     
     $("#close_menu" ).click(function() {
          $( "#mp-menu .mp-level" ).removeClass('mp-level-open');
        });

      /*$(function(){
        $("#close_menu" ).click(function() {
          $( "#trigger" ).get(0).click();
        });
      });
      $(".fav-mainbg a").click(function(){
        $(this).toggleClass("fa-heart-o fa-heart");
      });

    $("#close_menu").click(function(){
        $('#mp-menu').hide();
    });
    $(".ico-bar").click(function(){ alert('clicked');
      $('#mp-menu').show();
    });*/
    $(".fav-mainbg a").click(function(){
        $(this).toggleClass("fa-heart-o fa-heart");
      });
</script>
     <!-- scrolltofixed  -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    Include all compiled plugins (below), or include individual files as needed 

    <script src="js/bootstrap.min.js"></script>-->

<!-- Include js plugin 

  <script src="js/owl.carousel.js"></script>-->

<script>

    $(document).ready(function() {
      <?php
                if(Yii::$app->params['storeId'] == "143"){ ?>
                    $("span:contains('About Us')").parents('li').hide();
		    $("a:contains('About Us')").parents('li').hide();
          <?php } ?>
      $("#owl-demo").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        loop:true
      });

     setTimeout(function(){ $(".rating-star").rating({disabled: true, showClear: false, showCaption: false})}, 3000);

    });

    </script>

  <script type="text/javascript">
    $(document).ready(function() {
      // grab the initial top offset of the navigation 
        var stickyNavTop = $('.fixed-nav').offset().top;
        
        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var stickyNav = function(){
          var scrollTop = $('.scroller').scrollTop(); // our current vertical position from the top
               
          // if we've scrolled more than the navigation, change its position to fixed to stick to top,
          // otherwise change it back to relative
          if (scrollTop > stickyNavTop) { 
              $('.fixed-nav').addClass('sticky');
          } else {
              $('.fixed-nav').removeClass('sticky'); 
          }
      };

      stickyNav();
      // and run it again every time you scroll
      $('.scroller').scroll(function() {
        stickyNav();
      });

    });
  </script>
