<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>
<div class="content">
    <div class="content_area_home search_content">
        <div class="page-title">
            <h2 class="">Search Results for -"<?=$_REQUEST['q']?>"</h2>
        </div>
        
        <section class="innerpage-list">
          <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
          <?php 
              echo ListView::widget( [
                'id' => 'testclass',
                'dataProvider' => $dataProvider,
                'itemView' => '/products/_product',
                'summary' => '',
                'itemOptions' => ['class' => 'col-xs-6 items-padd']
                ] );
              ?>
              <?php \yii\widgets\Pjax::end(); ?>   
        </section>

    </div>
</div>