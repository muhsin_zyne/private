<?php

    use yii\widgets\ActiveForm;

    use yii\helpers\Url;

    use yii\helpers\Html;

    use yii\grid\GridView;

    use common\models\WishlistItems;

    use common\models\Products;



    use frontend\components\Helper;

     $this->title ='Payment Review';

    $this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">
  <div class="inner-container"?>
    <div class="customcontentArea">
      <div class="row">
        <div class="tabbable tabs-left">
          <div class="tab-content col-xs-12">
            <div class="m-wrap">
              <div class="account-title">
                <h2>Order #
                  <?=$order->id?>
                  - Payment Review</h2>
              </div>
              <div class="orderdate" style="margin-bottom:20px"> <span style="font-weight:bold">Order Date: </span>
                <?=$order->orderDate?>
              </div>
              <div class="row">
                <div class="col-xs-6">
                  <div class="billing-title" style="margin-bottom:10px;font-weight:bold">Billing Address</div>
                  <?php if (isset($order->billingAddressText)) { ?>
                  <div class="billing-address">
                    <?=$order->billingAddressText ?>
                  </div>
                  <?php } else {

                        		echo "No Address Found...";	

                        	}?>
                </div>
                <div class="col-xs-6">
                  <div class="billing-title" style="margin-bottom:10px;font-weight:bold">Payment Method</div>
                  <div class="billing-address"> Payment by cards or by PayPal account:
                    <?=$order->paymentMethod?>
                  </div>
                  <div class="billing-address"> Payer Email:
                    <?=$order->customer->email?>
                  </div>
                </div>
              </div>
              <div class="clear"></div>
              <div class="billing-title" style="margin-top:20px; margin-bottom:5px;font-weight:bold;">Items Ordered</div>
              <div class="table-responsive">
              
              <?php $form = ActiveForm::begin(); ?>
              <?= GridView::widget([

                                'dataProvider' => $orderItems,

                                'summary' => '',

                                'columns' => [

                                    ['class' => 'yii\grid\SerialColumn'],

                                        [

                                            'label'=> 'Product Name',

                                            'attribute'=>'product.name'

                                        ],

                                        [

                                            'label' => 'Option(s)',

                                            'value' => 'superAttributeValuesText',

                                            'format' => 'html'

                                        ],

                                        [

                                            'label'=> 'SKU',

                                            'attribute' => 'productId',

                                            'value' => 'product.sku'

                                        ],

                                        [

                                            'label'=> 'Item Price',

                                            'format' => 'html',

                                             'value' => function ($model) {

                                                return Helper::money($model->price / $model->quantity);

                                            },

                                        ],

                                        [   

                                            'label'=> 'Quantity',

                                            'attribute' => 'quantity',

                                        ],

                                        [

                                           'label' => 'Item Total',

                                            'format' => 'html',

                                            'value' => function($model, $attribute){

                                                return Helper::money($model->price);

                                            }

                                           

                                        ],

                                        

                                    ],

                                ]); 

                            ?>
              <?php ActiveForm::end(); ?>
              </div>
              <div class="test-class" style="float:right">
                <div class="order-subtotal"><span style="font-weight:bold"> Sub Total: </span>
                  <?=Helper::money($order->subTotal)?>
                </div>
                <div class="order-grandtotal"> <span style="font-weight:bold;font-size: 17px;">Grand Total: </span>
                  <?=Helper::money($order->grandTotal)?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>

<!-- </div> -->