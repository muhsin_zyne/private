<?php
use frontend\components\Helper;
use yii\helpers\Url;
?>

<?php //var_dump($model->getThumbnailImage()); die;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$model->getThumbnailImage();    
?>


<li class="item first">
<h2 class="product-name"><a href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>" ><?=$model->name?></a></h2>
    <a class="list-link-hc" href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>" ><img alt="<?=$model->name?>" src="<?=$thumbImagePath?>"></a>
    
    <!--<a href="http://www.legj.com.au/site1/sterling-silver-blue-topaz-diamond-ring-152.html" title="Sterling Silver Blue Topaz &amp; Diamond Ring" class="product-image"><img src="http://www.legj.com.au/media/catalog/product/cache/3/small_image/160x/9df78eab33525d08d6e5fb8d27136e95/m/p/mp2873_1.png" width="160" height="165" alt="Sterling Silver Blue Topaz &amp; Diamond Ring"></a>
    -->
    
         
        <div class="product_div">
        <div class="price-box">
            <span class="regular-price" id="product-price-152">
                    <!--<b>Retail price: </b>--><span><?=Helper::money($model->price)?></span>
            </span>  
        </div>
        <div class="actions">
            <a href="<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>" ><button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation('<?=\yii\helpers\Url::to(['products/view', 'id'=>$model->id])?>')"><span><span>Add to Cart</span></span></button></a>
                    
        </div>
        </div>
</li>