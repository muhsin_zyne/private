<?php 

use common\models\Storebanners;
use common\models\Configuration;
use common\models\Banners;
use frontend\components\MainMenu;
use yii\db\Query;
use yii\widgets\ListView;

//var_dump(Yii::$app->params["rootUrl"]);die();

?>

<?php //var_dump($mobile_banner);die(); ?>


<section>
    <div id="owl-demo" class="owl-carousel owl-theme">
        <?php foreach ($mobile_banner as $index => $banner){ 
            $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one();?>
            <div class="item">
                <?php if($banner_images['html']!=''){ //var_dump($banner_images->youtubeUrl);die;?>
                    <a class="modalButton" data-toggle="modal" data-src="<?=$banner_images->youtubeUrl?>?rel=0&wmode=transparent&fs=0" data-target="#myModal"> 
                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" >
                    </a>
                <?php 
                } 
                elseif ($banner_images['url']!='') {   
                    if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url']; }                                                 
                    else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                    else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                    ?> 
                        <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                                            
                            <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>'  >
                        </a>
                    <?php //echo "</div>"; 
                } 
                else{ ?>
                     <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" > 
                <?php }  ?>
               
            </div>
        <?php } ?>
       
    </div>
</section>

<section class="all-list-bg">
  <div class="row">
  <div class="col-xs-12">
        	<h1>Featured Products</h1>
        	<div class="small-border"></div>
        </div>
    <?php  

        $featuredProducts->pagination = [
            'defaultPageSize' => 6,
        ];

        echo ListView::widget([
            'id' => 'homepage-products',
			'summary'=>'', 
            'dataProvider' => $featuredProducts,
            'layout' => "{items}", 
            'itemView' => '/products/_product',
            'itemOptions' => ['class' => 'col-xs-6']

        ]);?>
  </div>
</section>

<!-- Start Section --> 

<!-- End Section --> 

