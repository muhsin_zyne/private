<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
    use common\models\WishlistItems;
    use common\models\Products;
    use frontend\components\Helper;
    
    $this->title = 'My Orders';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="main_content">
    <section class="innerpage-list">
        <h1 class="wish-head">My Orders</h1>
        <div class="wish-wrapper">
            <div class="table-responsive">
                <?php $form = ActiveForm::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $orders,
                            'summary' =>'',
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'label'=> 'Order',
                                    'format' => 'html',
                                    'attribute' => 'id',
                                ],
                                [   
                                    'label'=> 'Date',
                                    'format' => ['date', 'php:d/m/Y'],
                                    'attribute' => 'orderDate',
                                ],
                                [
                                    'label'=> 'Ship to',
                                    'value' => function($model){
                                        return isset($model->shippingAddress->streetAddress) ?  $model->shippingAddress->streetAddress : "";
                                    }
                                ],
                                [
                                    'label'=> 'Order Total',
                                    'attribute' => 'grandTotal',
                                ],
                                [
                                    'label'=> 'Status',
                                    'attribute' => 'status',
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{orderdetails}',
                                    'buttons'=>[
                                        'orderdetails' => function ($url, $model) {
                                            return Html::a('<span class="view_text">View Order</span>', $url, [
                                                        'title' => Yii::t('yii', 'Create'),
                                            ]);                                
                                        }
                                    ]  
                                ],
                            ],
                        ]);?>
                        <?php ActiveForm::end(); ?> 
                    </div>
                </div>                                                                                                                                                     </div>                                                                                                         
    </section>
</div>