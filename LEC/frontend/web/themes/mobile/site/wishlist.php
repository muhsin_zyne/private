<?php
	use yii\widgets\ActiveForm;
	use yii\helpers\Url;
	use yii\helpers\Html;
	use yii\grid\GridView;
	use common\models\WishlistItems;
	use common\models\Products;
	use frontend\components\Helper;

	$this->title = 'My Wishlist';
	$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main_content">
	<section class="innerpage-list">
		<h1 class="wish-head">My Wishlist</h1>
		<div class="wish-wrapper">
			<div class="table-responsive">
				<?php $form = ActiveForm::begin(); ?>
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'summary' =>'',
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],
						[
							'label'=> 'PRODUCT',
							'format' => 'html',
							'value' => function($model){
								$thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();
								return Html::img($thumbImagePath,['alt'=>'yii','class'=>'wish-img']).'<div class="wish-right"><span align="center">'.Html::a($model->product->name,['products/view','id' => $model->productId]).'</br><span class="price">'.Helper::money($model->product->price).'</b></div>';
							}
						],
						[
							'label' => 'COMMENT',
							'attribute' => 'id',
							'format' => 'raw',
							'value' => function ($model) {
								return ' <textarea class="form-control" id="'.$model->id.'" > '.$model->comments.'</textarea>' ;
							},
						],
						[
							'label'=> 'ADDED ON',
							'attribute' => 'id',
							'format' => 'html',
							'value' => function($model){
								return  Helper::date($model->createdDate, " F jS, Y ");
							}
						],
						[
							'label'=>'ADD TO CART',
							'format' => 'html',
							'value' => function($model){
								return Html::a('Add to cart',['products/view','id' => $model->productId]);
							}
						], 
						[
							'class' => 'yii\grid\ActionColumn',
							'template' => '{delete}',
						],
					],
				]); ?>
				</div>
				<input type="button" class=" update-wishlist btn btn-primary" id="wli_update" value="Update">
			<?php ActiveForm::end(); ?>
		</div>
	</section>    
</div>