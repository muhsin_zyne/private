<?php

    use yii\widgets\ActiveForm;

    use yii\helpers\Url;

    use yii\helpers\Html;

    use yii\grid\GridView;

    use common\models\WishlistItems;

    use common\models\Products;



    use frontend\components\Helper;
    $this->title = 'Address Book';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="container" id="address-block">
  <div class="inner-container"?>
    <div class="customcontentArea">
      <div class="row">
        <div class="tabbable tabs-left">
          <div class="tab-content col-xs-12">
            <div class="m-wrap">
              <div class="account-title book-ttl">
                <h2>Address Book</h2>
                <a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>" class="bootstrap-modal" data-width="650" data-update="address-block">
                <button class="add_address btn-primary" value="Add New Address" title="Add New Address"><i class="fa fa-plus"></i> Add New Address</button>
                </a> </div>
              <div class="col-1 addresses-primary">
                <div class="row">
                  <div class="item address col-xs-12">
                    <h4>Default Billing Address</h4>
                    <?php if(isset($defaultBillingAddress->streetAddress)) {  ?>
                    <?=$user->fullName?>
                    <br/>
                    <?=$defaultBillingAddress->streetAddress ?>
                    <br/>
                    <?php } else { ?>
                    <a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>" class="bootstrap-modal" data-width="650" data-update="address-block"> 
                    <!-- <span> Add address </span> --> 
                    Add address </a>
                    <?php } ?>
                  </div>
                  <div class="item address col-xs-12">
                    <h4>Default Shipping Address</h4>
                    <?php if(isset($defaultShippingAddress->streetAddress)) {  ?>
                    <?=$user->fullName?>
                    <br/>
                    <?=$defaultShippingAddress->streetAddress?>
                    <br/>
                    <a class="bootstrap-modal" data-width="650" href="<?=\yii\helpers\Url::to(['addresses/update','id'=>$defaultShippingAddress->id]) ?>">Change Shipping Address</a>
                    <?php } else { ?>
                    <a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>" class="bootstrap-modal" data-width="650" data-update="address-block"> Add address </a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 address ad-btm" >
                  <h4>Additional Address Entries</h4>
                  <!-- <p>You have no additional address entries in your address book.</p> -->
                  
                  <?php if(!empty($additionalAddresses)) { ?>
                  <?php foreach ($additionalAddresses as $address) {  ?>
                  <li class="item">
                    <?=$address['streetAddress']?>
                    <br/>
                    <a href="<?=\yii\helpers\Url::to(['addresses/update', 'id'=> $address['id']]) ?>" class="bootstrap-modal" data-width="650"> 
                    
                    <!-- <span class="edit_span"> Edit address </span> --> 
                    
                    Edit address </a> | 
                    
                    <!-- <a href="<?=\yii\helpers\Url::to(['addresses/delete', 'id'=> $address['id']]) ?>">

                                                    <span class="edit_span"> Delete address </span>

                                                    Delete address

                                                </a> -->
                    
                    <?= Html::a('Delete Address', ['addresses/delete', 'id' => $address['id']], 

                                                        ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?>
                  </li>
                  <?php } } else { ?>
                  <p>You have no additional address entries in your address book.</p>
                  <?php } ?>
                  </li>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!--</div>-->
        
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $('body').addClass('popup-absolute');

    });    

</script> 
