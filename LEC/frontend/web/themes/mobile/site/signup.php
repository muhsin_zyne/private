<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use yii\captcha\Captcha;





/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */



$this->title = 'Signup';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">

    <div class="site-signup">

        <h1><?= Html::encode($this->title) ?></h1>

    

        <p>Please fill out the following fields to signup:</p>

    

        <div class="row">

            <div class="col-lg-12">

                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                	<div class="fieldset">

                    	<h2 class="legend">Personal Information</h2>

						<?= $form->field($model, 'firstname') ?>

                        <?= $form->field($model, 'lastname') ?>

                        <div class="clear"></div>

                        	<?= $form->field($model, 'email') ?>

                        <div class="clear"></div>

                    </div>

                    <div class="fieldset">

                    	<h2 class="legend">Login Information</h2>

						<?= $form->field($model, 'password')->passwordInput() ?>

                       <!--  <label>Confirm Password</label> -->

                        <?= $form->field($model, 'confirm_password')->passwordInput() ?>

                        <div class="clear"></div>

                    </div>

                    <div class="fieldset">

                    	<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [

                			'template' => '<div class="row"><div class="col-lg-3">{image}<a href="#" id="fox"><i class="fa fa-refresh"></i></a></div><div class="col-lg-6">{input}</div></div>',

            			]) ?>

                    </div>    

                        <div class="clear"></div>

                        <div class="form-group">

                        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>

                    </div>

                    </div>

                    

                <?php ActiveForm::end(); ?>

            </div>

        </div>

    </div>

</div>