<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use common\models\Stores;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

//var_dump($defaultAddress);die();


$store = Stores::findOne(Yii::$app->params['storeId']);
$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <div class="row">
        <div class="col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>
		    <p>If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.</p>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'phone')->label('Phone') ?>
                <div class="collect_store_div form-group">
                    <?php if(isset($addresses) && count($addresses) >1)  { ?>
                    <label for="contactform-subject" class="control-label">Store:</label>
                        <?=Html::dropDownList('ContactForm[storeAddressId]', [], ArrayHelper::map($addresses, 'id', 'addressAsText'), ['class' => 'stored-billing-address form-control'])?>
                
                    <?php    } ?>
                </div>
                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row sm-row"><div class="col-xs-6">{input}</div><div class="col-xs-6">{image}</div></div>',
                ]) ?>
                
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="mapaddress"><span>Address:</span>
                <p><?=$store->title?><br><?=$defaultAddress->billingAddress; ?><br>Ph: <?=$defaultAddress->phone?></p>
                <a href='http://maps.google.com/maps?q=<?=$defaultAddress->billingAddressAsText?>' target='_blank'>View Map</a>
			</div>
        </div>
    </div>
