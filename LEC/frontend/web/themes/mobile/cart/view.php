<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\components\Helper;
/* @var $this yii\web\View */
$this->title = "Shopping Cart";
$positions = $cart->positions;
?>
<div class="main_content">
    <form class="cart-form" action="<?=Url::to(['cart/update'])?>" method="post">
    <?php foreach($positions as $position){ 
	   $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$position->product->getThumbnailImage();
	   if(!isset($position->product)){ var_dump($position); die;}?>
        <div class="cart_dec">
            <div class="product-image">
                <img src="<?=$thumbImagePath?>" width="75" height="75"  alt="<?=$position->product->name?>"/>
            </div>
            <div class="product_detail">
                <a href="<?=Url::to(['cart/delete', 'positionId' => $position->getId()])?>"><i class="fa fa-times pull-right"></i></a>
                <?=$position->product->name?><br/>  
                <div class="quantity pull-left">
                    <input name="Cart[<?=$position->getId()?>][qty]" value="<?=$position->quantity?>"   type="text" onkeyup="chknumber(this)" class="pdt_qty"/>
                </div>
                <div class="cart_price pull-right"><?=Helper::money($position->price)?></div><br/>
                <div class="clearfix"></div>
                <a href="<?=Url::to(['cart/update', 'positionId' => $position->getId()])?>" class="pull-right edit"><i class="fa fa-pencil"></i>Edit</a>
                <?php if(!empty($position->config)){ ?>
                    <a id="holding_options_show_<?=$position->getId();?>" href="Javascript:$('#holding_options_<?=$position->getId();?>').toggle();$('#holding_options_hide_<?=$position->getId();?>').toggle(); $('#holding_options_show_<?=$position->getId();?>').toggle(); void(0);">Show Options</a>
                
                    <a style="display: none;" id="holding_options_hide_<?=$position->getId();?>" href="Javascript:$('#holding_options_<?=$position->getId();?>').toggle();$('#holding_options_hide_<?=$position->getId();?>').toggle(); $('#holding_options_show_<?=$position->getId();?>').toggle(); void(0);">Hide Options</a>
                    <br />
                    <div id="holding_options_<?=$position->getId();?>" style="display: none;">
                        <dl class="item-options">
                            <?php foreach($position->config as $config => $details){
                                if($position->product->typeId=="bundle"){ ?>
                                    <p><?=$details['name']?>: <?=$details['qty']?></p>
                                <?php }elseif($position->product->typeId=="configurable"){ ?>
                                     <p><?=$details['attr']['title']?>: <?=$details['value']?></p>
                                <?php } ?>
                            <?php } ?>
    					</dl>
                <?php } else{echo"<br/>";} ?>
                    </div>
            </div>
        </div>
    <?php } ?>
    </form>
        
    <div class="cart_dec">
    	<!-- <a class=" btn btn-block" href="<?php //Url::to(['cart/update', 'positionId' => $position->getId()])?>">Update Shopping Cart</a> -->
        <button type="button" title="Update Shopping Cart" class="button btn-update btn btn-block shopping-btn-bg"><span><span>Update Shopping Cart</span></span></button>
    </div>
    <?php if(!$cart->hasOnlyByodProducts() && !$cart->hasOnlyClientPortalProducts()){ ?>	
    <div class="cart_dec promo">
    	<a href="Javascript:$('#promocode_form').toggle();void(0);">Have a Promo Code?</a>
        <div id="promocode_form" style="display: none;">

            <form id="discount-coupon-form" action="#" method="post">
                <div class="discount">
                    <div class="discount-form">
                        <p>Enter your promotional code.</p>
                        <input type="hidden" name="remove" id="remove-coupone" value="0">
                        <div class="input-box">
                            <input style="width:80% !important; margin-bottom:4px !important;;text-align: center;border: 1px solid #ccc;padding: 4px;" class="input-text" id="coupon_code" name="coupon_code" value=""/>
                        </div>
                        <div class="buttons-set">
                            <button type="button" title="Apply Promo code" class="button btn apply-code" onclick="discountForm.submit(false)" value="Apply Coupon"><span><span>Apply Promo code</span></span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php } ?>
    <div class="cart_dec cart_amount">
    	<p>Subtotal: <?=Helper::money(Yii::$app->cart->cost)?></p>
        <?php if(isset(Yii::$app->session['appliedCoupons'])){ ?>
            <p>Coupon Discount:  <?=Helper::money(Yii::$app->cart->cost - Yii::$app->cart->getCost(true))?>   </p>
        <?php } ?>
        <p><b>Grand Total: <?=Helper::money(Yii::$app->cart->getCost(true))?></b></p>
    </div>
    <div class="cart_dec">
    	<a class=" btn btn-block btn-prcd-chkout" onclick="window.location='<?=Url::to(['checkout/index'])?>';" href="Javascript:void(0);">Proceed To Checkout</a>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.btn-update').click(function(){
        var qty = $('.pdt_qty').val();
        if(qty.length == 0){
            alert('Please enter a valid quantity');
            $('.pdt_qty').val('1');
        }
        $('form.cart-form').submit();
    })
    $('.btn-checkout').click(function(){
        location.href = "<?=Url::to(['checkout/index'])?>";
    })
    $('.apply-code').click(function(){
        location.href = "<?=Url::to(['cart/applycode'])?>?code="+$('#coupon_code').val();
    })
    $('.coupon_form_text a').click(function(){      
        $('#discount-coupon-form').show();      
        return false;       
    });
    // $('.apply-code').click(function(){
    //     $.ajax({
    //         method: 'POST',
    //         url: '<?=\yii\helpers\Url::to(["cart/applyCode"])?>?code='+$('#coupon_code').val(),
    //     }).success(function(data){
    //         if(data.status == "success")
    //             alert('Coupon Code Applied Successfully');
    //         else
    //             alert('Invalid Coupon Code');
    //     })
    // })   
})

function chknumber(id)
{
    var item = jQuery(id).val();
    console.log(item.length);
    jQuery(id).onkeyup = function() {
        var key = event.keyCode || event.charCode;
        //alert(key);
        if( key == 8 || key == 46 )
            return false;
    };
    intRegex = /^[0-9]+$/;
    if((!intRegex.test(item))) //||x.indexOf(" ")!=-1
    {
        if(item!='')
        {
            alert('Please enter a valid quantity');
            jQuery(id).val('1');
        }
        return false;  
    }
    if(item == 0){
        alert('Please enter a valid quantity');
        jQuery(id).val('1');
    }
}

</script>