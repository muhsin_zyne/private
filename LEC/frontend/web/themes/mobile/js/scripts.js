$(document).ready(function(){

	$('.chkbx').click(function(){
		if($(this).is(':checked')){
			$(this).parents('.items').find('.vsamll').val('1');
		}
	})
	$(window).keydown(function(event){
        if(event.keyCode == 13) {
        	$( "#pi_search" ).keypress(function() {
        		//alert('poii');
        		$("#p_search").click();        	            
            	event.preventDefault();
            	return false;
        	});
        }
    });

    var count = 0;
    var error = 0;
    var url;

    $('body').on('click', '.pagination a', function(){  
    	var url = $(this).attr('href');
    	$('#continue').closest('a').attr("href", url);
        $('.product_list').append('<input type="hidden" name="tourl" value="'+url+'">');
    //$(".pagination a").click(function() { alert('clicked');return false;
        $(".chkbx").each(function() {
            if($(this).is(':checked')){
                count = 1; 
            }
        });    
        if(count==1){
        	
        	$("#dialog" ).dialog({
            height: 300,
            width: 350,
            modal: true,
            resizable: true,
            dialogClass: 'no-close success-dialog cart-dialog'
            });
            count = 0;
            error=0;
            return false;
            
        }  
        
        if(error>0){  
            error = 0;
            return false;
        }
    }); 

    $("#cancel").click(function() {
            $('#dialog').dialog('close'); 
            return false;
        });
        $("#add_tocart").click(function() {
            $('#list_form_pro').submit();
        });

    $('.alert').fadeOut(5000);   
    $('body').on('change', '.addto input', function(){
        $(".vsamll").each(function() {
            if($(this).val() > 0){
                $(this).parents('.items').find('.chkbx').prop('checked',true);
            }
            else{
                $(this).parents('.items').find('.chkbx').prop('checked',false);
            }
        });
    })
    $('body').on('click', '.order_now', function(){    
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required');
          return false;
        }
        if($('.chkbx:checked').length < 1 && $('.chkbx').length > 0){
            alert('Please tick atleast one product to order');
            return false;
        }  

    });
    
    $('body').on('change', '.chkbx', function(){     
        if(this.checked) {
            $('.productsids').val($(this).val());
            $(this).parents('.items').find('.vsamll').val('1');
        }

        if(!$(this).is(':checked')){
            $(this).parents('.items').find('.vsamll').val('');
        }
    }); 
});

function setConfig(pid){
    $('form#list_form_pro').append('<input type="hidden" name="Products['+pid+'][config]" value="'+$('.product-settings-form form').serialize()+'"/>');
    $("#ajax-update").dialog("close");
    throw new Error('That\'s all we know');
}


