<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\MobileProductFilter;
?>

<style type="text/css">
    .fixed-nav .outer-search{ width:66.6667%;}
    .filter-outer{ display:inline-block;}
</style>

    <div class="modal fade bs-example-modal-sm" id="sortModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">SORT</h4>
          </div>
          <div class="modal-body">
            
            <div class="radio-group-bg">
                <div class="radio">
                  <label>Sort by New <div class="radio-type"><input type="radio" class="sort-val" name="optradio" value="dateAdded"></div></label>
                </div>
                <div class="radio">
                  <label>Sort by Price <div class="radio-type"><input type="radio" class="sort-val" name="optradio" value="price"></div></label>
                </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- Small modal -->


    <!-- Large modal -->
    <div class="modal fade bs-example-modal-lg" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- modal-header --> 
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">All Filters</h4>
            <button type="button" class="btn btn-default clear_filter">Clear All</button>
            
          </div>
          <!--End modal-header --> 
          <!-- modal-body 
          <div class="modal-body"> -->
                
                  <?php
                    MobileProductFilter::begin([
                          'basedModel' => $byod,
                          'attributes' => $byod->filterAttributes
                      ]);
                    MobileProductFilter::end();
                  ?>        
        </div>
      </div>
    </div>
    <!-- Large modal -->

    <section class="innerpage-list">

      <div class="list-head">
              <?php if(!is_null($byod->organisation_logopath)){
                      $logoPath = Yii::$app->params["rootUrl"].$byod->organisation_logopath;
              ?>   
              <div class="img-field">       
                <img src="<?=$logoPath?>" alt="<?=$byod->organisation_name?>">
              </div>  
              <?php } ?> 
              <div class="pr-right-btns">
                <?=$byod->organisation_name?>,<?=$byod->address?>
                <?=$byod->city?>,<?=$byod->postcode?>
              </div>
      </div>        

      <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
      <?php 
          echo ListView::widget([
            'id' => 'testclass',
            'dataProvider' => $products,
            'itemView' => '/products/_product',
            'summary' => '',
            'itemOptions' => ['class' => 'col-xs-6 items-padd']
            ] );
            echo "<script> $(document).ready(function() { $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>'); }) </script>";
          ?>
          <?php \yii\widgets\Pjax::end(); ?>   
    </section>

<script type="text/javascript">
  $(document).ready(function(){
      
      $('body').on('click', '.radio .sort-val', function(){ console.log($(this).val());
          $('#filter-form .sort').val($(this).val());
          filterproducts();
          $("#sortModal").modal('hide');
      })

      $('body').on('click', '.clear_filter', function(){
          $('.check-group-bg .checkbox .check-type').prop('checked', false);
      });  

      /*if($('.list-view').length >0){
          $('.list-view').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>')
      }*/
  })
</script>