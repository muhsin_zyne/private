<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="site-contact">
    <div class="row">
        <div class="col-lg-12">
        	<div class="pop-title">Enter Your Client Code</div>
        	<?php $form = ActiveForm::begin(['id'=>'client_form']); ?>
        		<?=$form->field($clientPortal, 'code')->textInput(['placeholder'=>'Client Code'])->label('') ?>
        		<?= Html::submitButton('SUBMIT', ['class' => 'btn pdt-cart pop-submit']) ?>
        	<?php ActiveForm::end(); ?>
        	<!-- <div class="byod-desc">
	    		Technology plays a huge role in student's lives. Bring Your Own Device (BYOD) is a technology model that allows students to bring their own devices to school/organisation.
	    	</div> 
	    	<div class="cond-apply">* Conditions Apply</div>-->
        </div><br/>
    </div>
</div>        