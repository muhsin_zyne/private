<?php

use yii\helpers\ArrayHelper;

use yii\helpers\Url;

use yii\helpers\Html;

use frontend\components\Helper;

use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */

$this->title = "Your Shopping Cart";

$positions = $cart->positions;

$gift_wrap_check= \common\models\Stores::findOne(Yii::$app->params['storeId'])->hasGiftWrap;
?>

<div >
   <br>             
</div>

<div class="container">

    <div class="inner-container yourshopcart-view"?>

    <h1><?=strtoupper($this->title)?> </h1>

        <div class="cart-main-bg">

        

            <div class="cart-content-bg">

                <form class="cart-form" action="<?=Url::to(['cart/update'])?>" method="post">

                    <div class="table-bg">

                        <table class="table table-bordered">

                            <thead class="table-header">

                                <tr>

                                  <th>Image</th>

                                  <th>Product Name</th>

                                  <th>Unit Price</th>

                                  <th>Quantity</th>

                                  <th>Subtotal</th>

                                  <th>Comments</th>

                                  <th>Actions</th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php foreach($positions as $position){

                                    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$position->product->getThumbnailImage();

                                    if(!isset($position->product)){ var_dump($position); die;}?>

                                    <tr >

                                    <td class="prdct-img"><a href="<?=\yii\helpers\Url::to('@web/'.$position->product->urlKey.".html")?>" title="<?=$position->product->name?>'" class="product-image"><img src="<?=$thumbImagePath?>" width="75" height="75" alt="<?=$position->product->name?>"></a></td>

                                    <td>

                                        <p class="product-name">

                                            <a href="<?=\yii\helpers\Url::to('@web/'.$position->product->urlKey.".html")?>"><?=$position->product->name?></a>

                                            </p>

                                            <?php if(!empty($position->config)){ ?>

                                                <dl class="item-options">

                                                    <?=$position->superAttributeValuesText?>

                                                </dl>

                                            <?php } ?>
                                            
                                    <?php if($gift_wrap_check) {
                                        $gift_wrap = \common\models\Configuration::findSetting('gift_wrapping', Yii::$app->params['storeId']);
                                        ?>
                                            <dl class="item-options">
                                                <div class="checkbox-inline">
                                                    <label><input type="checkbox" onclick="update();" name="Cart[<?= $position->getId() ?>][giftWrap]" <?= (!empty($position->giftWrap) ? 'checked' : ''); ?>><?= $gift_wrap!=0?'':'Free'; ?> Gift Wrapping <?= $gift_wrap!=0?'$'.$gift_wrap:''; ?></label>
                                                </div>
                                                    
                                            </dl> 
                                    <?php } ?>

                                    </td>
                                    

                                    <td class="unit-price">

                                            <span class=""><?=Helper::money($position->price)?></span>                                   

                                    </td>

                                    <td class="input-cls ">

                                        <input name="Cart[<?=$position->getId()?>][qty]" value="<?=$position->quantity?>" size="4" title="Qty" class="input-text qty form-control only-numeric" maxlength="12">

                                    </td>

                                    <td class="subtotal">

                                        <span class=""><?=Helper::money($position->cost)?></span>                                    

                                    </td>

                                    <td id="comments_25199 ">

                                        <textarea name="Cart[<?=$position->getId()?>][comments]" id="comment_<?=$position->getId()?>" class="form-control"><?=$position->comments?></textarea>

                                    </td>

                                    

                                    <td class="a-center last sm-icons">

                                        <a href="<?=Url::to(['cart/update', 'positionId' => $position->getId()])?>" title="Edit item" class="btn-edit fa fa-pencil"></a>

                                        <a href="<?=Url::to(['cart/delete', 'positionId' => $position->getId()])?>" title="Remove item" class="btn-remove fa fa-trash"></a>

                                    </td>

                                </tr>

                                <?php } ?>

                            </tbody>

                        </table>

                    </div>

                </form>

                <!--- End table-bg-->

                

                <div class="pull-left">
                    <?php if(!$cart->hasOnlyByodProducts() && !$cart->hasOnlyClientPortalProducts()){ ?>
                    <a href="<?=yii\helpers\Url::to('@web/')?>">
                        <button type="button" title="Continue Shopping" class="button btn-continue shopping-btn-bg"><span><span>Continue Shopping</span></span></button>
                    </a>
                    <?php } else { ?>
                    <a href="<?=yii\helpers\Url::to(['byod/view'])?>">
                        <button type="button" title="Continue Shopping" class="button btn-continue shopping-btn-bg"><span><span>Continue Shopping</span></span></button>
                    </a>
                    <?php } ?>    
                </div>

                <div class="pull-right">

                    <button type="button" title="Update Shopping Cart" class="button btn-update shopping-btn-bg"><span><span>Update Shopping Cart</span></span></button>

                </div>

                

                <div class="promo-code-bg">

                    <?php  
                        $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
                    ?>

                    <?php if(!$cart->hasOnlyByodProducts() && !$cart->hasOnlyClientPortalProducts()){ ?>

                    <div class="pull-left">

                        <p class="coupon_form_text">

                            <a href="javascript:javascript:;">Have a Promo Code? </a>

                        </p>

                        <div class="ns-field">

                            <form id="discount-coupon-form" action="#" method="post" > 

                                <div class="discount">

                                    <div class="discount-form">                                       

                                        <input type="hidden" name="remove" id="remove-coupone" value="0">

                                        <div class="input-box">

                                            <input class="input-text" id="coupon_code" name="coupon_code" value="" placeholder="Enter your promotional code.">                                           

                                              <button type="button" title="Apply Promo code" class="button apply-code" value="Apply Coupon">Apply Promo Code</button>                                            

                                        </div>

                                        <div class="buttons-set">

                                            

                                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                    <?php }  ?>

                    <div class="pull-right">

                        <p><span>Sub Total</span>     <b><?=Helper::money(Yii::$app->cart->cost)?></b></p>                        

                        <?php if(isset(Yii::$app->session['appliedCoupons'])){ ?>

                             <p><span>Coupon Discount </span> <b><?=Helper::money(Yii::$app->cart->cost - Yii::$app->cart->getCost(true))?></b></p>

                        <?php } ?>
                             
                        <?php if ($gift_wrap_check) { ?>
                            <p><span>Gift Wrapping Total </span> <b><?= Helper::money(Yii::$app->cart->giftWrapcost) ?></b></p>
                        <?php } ?>          

                        <p><span>Grand Total</span>    <b><?=Helper::money(Yii::$app->cart->getCost(true,true))?></b></p>

                    </div>

                </div>

                

                <div class="proceed-bg">

                    <div class="pull-right">

                        <button type="button" title="Proceed to Checkout" class="button btn-proceed-checkout btn-checkout" onclick="window.location='<?=Url::to(['checkout/index'])?>';"><span><span>Proceed to Checkout</span></span></button>

                    </div>

                </div>

                

            </div>

        </div>

        <!--- End container-->

    </div>

</div>

<script type="text/javascript">

$(document).ready(function(){

    var breakOut = false;

    $('.btn-update').click(function(){

        breakOut = false;

        var status = $('.qty').each(function(){ 
            if($(this).val()<1){ 
                    alert('Quantity cannot be zero!');
                    breakOut = true;
                    return false; 
            } 
        });

        console.log(breakOut);

        if(breakOut)

            return false;

        $('form.cart-form').submit();

    })

     $('.btn-checkout').click(function(){
        //location.href = "<?=Url::to(['checkout/index'])?>";
        breakOut = false;
        var status = $('.qty').each(function(){ 
            if($(this).val()<1){ 
                alert('Quantity cannot be zero!');
                breakOut = true;
                return false; 
            } 
        });
        //console.log(breakOut);
        if(breakOut)
            return false;
        $('form.cart-form').attr('action', '<?=\yii\helpers\Url::to(["cart/update", 'positionId' =>null,'checkout'=>'true'])?>');
        $('form.cart-form').submit();

    });   

    $('.apply-code').click(function(){

        location.href = "<?=Url::to(['cart/applycode'])?>?code="+$('#coupon_code').val();

    })

    $('.coupon_form_text a').click(function(){      

        $('#discount-coupon-form').show();      

        return false;       

    });

    // $('.apply-code').click(function(){

    //     $.ajax({

    //         method: 'POST',

    //         url: '<?=\yii\helpers\Url::to(["cart/applyCode"])?>?code='+$('#coupon_code').val(),

    //     }).success(function(data){

    //         if(data.status == "success")

    //             alert('Coupon Code Applied Successfully');

    //         else

    //             alert('Invalid Coupon Code');

    //     })

    // })   

})      

/*function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}*/
function update() {
        breakOut = false;
        var status = $('.qty').each(function(){ 
            if($(this).val()<1){ 
                alert('Quantity cannot be zero!');
                breakOut = true;
                return false; 
            } 
        });
        console.log(breakOut);
        if(breakOut)
            return false;
        $('form.cart-form').submit();
}
</script>