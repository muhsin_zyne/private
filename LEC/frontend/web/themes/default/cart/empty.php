<?php 	
	$this->title = 'Shopping Cart';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="inner-container"?>	 	
      	<div class="empty-wrapper">        
          <i class="fa fa-shopping-cart"></i>
          <h1>Shopping Cart is Empty</h1>
          <p><span>You have no items in your shopping cart.</span></p>
        	<p><a href="<?=yii\helpers\Url::to('@web/')?>">Continue shopping &nbsp; </a></p>
        
    	</div>
	</div>
	<div class="clear"></div>
</div>
