<style type="text/css">
.paypalcenter{
	display: block;
	margin:0 auto;
	text-align: center;
	margin-top: 10%;
}
</style>
<div class="paypalcenter">
 <h2> Redirecting to PayPal </h2>
 <h3> Plese donot close or refresh the window. </h3>
</div>
<form action="https://www.paypal.com/cgi-bin/webscr" id="paypalform" method="post">

<!-- Identify your business so that you can collect the payments. -->
<input type="hidden" name="business" value="<?=Yii::$app->params['paypalEmail']?>">

<!-- Specify a Buy Now button. -->
<input type="hidden" name="cmd" value="_xclick">

<!-- Specify details about the item that buyers will purchase. -->
<input type="hidden" name="item_name" value="Payment towards the purchase">
<input type="hidden" name="item_number" value="<?= $orderId ?>">
<input type="hidden" name="amount" value="<?= $amount ?>">
<input type="hidden" name="currency_code" value="AUD">
<input type="hidden" name="notify_url" value="<?= \yii\helpers\Url::toRoute('checkout/ipnprocess',true)?>">
<input type="hidden" name="return" value="<?= \yii\helpers\Url::toRoute('checkout/success',true)?>">
</form>
<script type="text/javascript">
document.getElementById("paypalform").submit();
</script>

          
