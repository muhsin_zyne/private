  <?php
  use yii\helpers\Html;
  use yii\bootstrap\ActiveForm;
  use yii\helpers\Url;
  /* @var $this yii\web\View */
  /* @var $form yii\bootstrap\ActiveForm */
  /* @var $model \frontend\models\SignupForm */
  
  $this->title = 'Checkout';
  $this->params['breadcrumbs'][] = $this->title;
  ?>
  <div class="container">
	  <div class="site-checkout">
		  <h1><?= Html::encode($this->title) ?></h1>
  
		  <div class="twocolumleft_left">
		  <div id="checkout-progress-wrapper"><div class="block block-progress opc-block-progress">
	  <div class="block-title">
		  <strong><span>Your Checkout Progress</span></strong>
	  </div>
	  <div class="block-content">
		  <dl>
							  <dt>
				  Billing Address            </dt>
				  
							  <dt>
				  Shipping Address            </dt>
				  
							  <dt>
				  Shipping Method            </dt>
				  
							  <dt>
				  Payment Method            </dt>
						  </dl>
	  </div>
  </div></div>
  </div>
  <div class="twocolumleft_right">
	  <?php $form = ActiveForm::begin(['id' => 'checkout-form']); ?>
		  <ol class="opc" id="checkoutSteps">
			  <?php if(Yii::$app->user->isGuest){ ?>
				  <li id="opc-login" class="section allow">
					  <div class="step-title">
						  <h2>Checkout Method</h2>
						  <a href="#">Edit</a>
					  </div>
					  <div id="checkout-step-login" class="step a-item" style=" ">
						  <div class="col2-set">
					  <div class="col-1">
					  <h3>Checkout as a Guest or Register</h3>
								  <p>Register with us for future convenience:</p>
										  <ul class="form-list">
											  <li class="control">
								  <input type="radio" name="checkout_method" id="login:guest" value="guest" checked="checked" class="radio" /><label for="login:guest">Checkout as Guest</label>
							  </li>
											  <li class="control">
								  <input type="radio" name="checkout_method" id="login:register" value="register" class="radio" /><label for="login:register">Register</label>
							  </li>
						  </ul>
						  <h4>Register and save time!</h4>
						  <p>Register with us for future convenience:</p>
						  <ul class="ul">
							  <li>Fast and easy check out</li>
							  <li>Easy access to your order history and status</li>
						  </ul>
						  </div>
				  <div class="col-2">
					  <h3>Login</h3>
							  <form id="login-form" action="<?=Url::to(['site/login'])?>" method="post">
					  <fieldset>
						  <h4>Already registered?</h4>
						  <p>Please log in below:</p>
						  <ul class="form-list">
							  <li>
								  <label for="login-email" class="required"><em>*</em>Email Address</label>
								  <div class="input-box">
									  <input type="text" class="input-text required-entry validate-email" id="login-email" name="LoginForm[email]" value="" />
								  </div>
							  </li>
							  <li>
								  <label for="login-password" class="required"><em>*</em>Password</label>
								  <div class="input-box">
									  <input type="password" class="input-text required-entry" id="login-password" name="LoginForm[password]" />
								  </div>
							  </li>
										  </ul>
						  <input name="context" type="hidden" value="checkout" />
					  <div class="col-2">
						  <div class="buttons-set">
							  
  
							  <button type="submit" class="button"><span>Login</span></button>
									 <a href="http://www.legj.com.au/site1/customer/account/forgotpassword/" class="f-left">Forgot your password?</a>
						  </div>
					  </div>
					  </fieldset>
					  </form>
				  </div>
			  </div>
			  <div class="col2-set">
				  <div class="col-1">
					  <div class="buttons-set">
						  
										  <button id="onepage-guest-register-button" type="button" class="button"><span>Continue</span></button>
								  </div>
				  </div>
			  </div>
			  </div>
		  </li>
	  <?php } ?>
	  <li id="opc-billing" class="section">
		  <div class="step-title">
			  <h2>Billing Information</h2>
			  <a href="#">Edit</a>
		  </div>
		  <div id="checkout-step-billing" class="step a-item" style=" ">
			  <form id="co-billing-form" action="">
  <fieldset>
	  <ul class="form-list">
		  <li id="billing-new-address-form">
		  <fieldset>
			  <input type="hidden" name="billing[address_id]" value="" id="billing:address_id" />
			  <ul>
				  <li class="fields"><div class="customer-name">
	  <div class="field name-firstname">
		  <label for="billing:firstname" class="required"><em>*</em>First Name</label>
		  <div class="input-box"> 
			  <?=$form->field($checkout, 'billing_firstname')->textInput()->label(false)?>
		  </div>
	  </div>
	  <div class="field name-lastname">
		  <label for="billing:lastname" class="required"><em>*</em>Last Name</label>
		  <div class="input-box">
			  <input type="text" id="billing:lastname" name="billing[lastname]" value="" title="Last Name" class="input-text required-entry"  />
		  </div>
	  </div>
  </div>
  </li>
				  <li class="fields">
					  <div class="field">
						  <label for="billing:company">Company</label>
						  <div class="input-box">
							  <input type="text" id="billing:company" name="billing[company]" value="" title="Company" class="input-text" />
						  </div>
					  </div>
							  <div class="field">
						  <label for="billing:email" class="required"><em>*</em>Email Address</label>
						  <div class="input-box">
							  <input type="text" name="billing[email]" id="billing:email" value="" title="Email Address" class="input-text validate-email required-entry" />
						  </div>
					  </div>
						  </li>
				  <li class="wide">
					  <label for="billing:street1" class="required"><em>*</em>Address</label>
					  <div class="input-box">
						  <input type="text" title="Street Address" name="billing[street][]" id="billing:street1" value="" class="input-text required-entry" />
					  </div>
				  </li>
						  <li class="wide">
					  <div class="input-box">
						  <input type="text" title="Street Address 2" name="billing[street][]" id="billing:street2" value="" class="input-text" />
					  </div>
				  </li>
						  <li class="fields">
					  <div class="field">
						  <label for="billing:city" class="required"><em>*</em>City</label>
						  <div class="input-box">
							  <input type="text" title="City" name="billing[city]" value="" class="input-text required-entry" id="billing:city" />
						  </div>
					  </div>
					  <div class="field">
						  <label for="billing:region_id" class="required"><em>*</em>State/Province</label>
						  <div class="input-box">
							  <select id="billing:region_id" name="billing[region_id]" title="State/Province" class="validate-select" style="display:none;">
								  <option value="">Please select region, state or province</option>
							  </select>
							 
							 
							  <select id="billing:region" name="billing[region]"  title="State/Province" class="input-text" >
							  <option value=""></option>
							  <option value="QLD">QLD</option>
							  <option value="NSW/ACT">NSW/ACT</option>
							  <option value="VIC">VIC</option>
							  <option value="TAS">TAS</option>
							  <option value="SA">SA</option>
							  <option value="WA">WA</option>
							  <option value="NT">NT</option>
							  </select>
						  </div>
					  </div>
				  </li>
				  <li class="fields">
					  <div class="field">
						  <label for="billing:postcode" class="required"><em>*</em>Zip/Postal Code</label>
						  <div class="input-box">
							  <input type="text" title="Zip/Postal Code" name="billing[postcode]" id="billing:postcode" value="" class="input-text validate-zip-international required-entry" />
						  </div>
					  </div>
					  <div class="field">
						  <label for="billing:country_id" class="required"><em>*</em>Country</label>
						  <div class="input-box">
							  <select name="billing[country_id]" id="billing:country_id" class="validate-select" title="Country" ><option value="" > </option><option value="AU" >Australia</option></select>                        </div>
					  </div>
				  </li>
				  <li class="fields">
					  <div class="field">
						  <label for="billing:telephone" class="required"><em>*</em>Telephone</label>
						  <div class="input-box">
							  <input type="text" name="billing[telephone]" value="" title="Telephone" class="input-text required-entry" id="billing:telephone" />
						  </div>
					  </div>
					  <div class="field">
						  <label for="billing:fax">Fax</label>
						  <div class="input-box">
							  <input type="text" name="billing[fax]" value="" title="Fax" class="input-text" id="billing:fax" />
						  </div>
					  </div>
				  </li>
				  
							  
			  
				  <li class="fields" id="register-customer-password">
					  <div class="field">
						  <label for="billing:customer_password" class="required"><em>*</em>Password</label>
						  <div class="input-box">
							  <input type="password" name="billing[customer_password]" id="billing:customer_password" title="Password" class="input-text required-entry validate-password" />
						  </div>
					  </div>
					  <div class="field">
						  <label for="billing:confirm_password" class="required"><em>*</em>Confirm Password</label>
						  <div class="input-box">
							  <input type="password" name="billing[confirm_password]" title="Confirm Password" id="billing:confirm_password" class="input-text required-entry validate-cpassword" />
						  </div>
					  </div>
				  </li>
																	  <li class="no-display"><input type="hidden" name="billing[save_in_address_book]" value="1" /></li>
							  </ul>
			  <div id="window-overlay" class="window-overlay" style="display:none;"></div>
  <div id="remember-me-popup" class="remember-me-popup" style="display:none;">
	  <div class="remember-me-popup-head">
		  <h3>What's this?</h3>
		  <a href="#" class="remember-me-popup-close" title="Close">Close</a>
	  </div>
	  <div class="remember-me-popup-body">
		  <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
		  <div class="remember-me-popup-close-button a-right">
			  <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a>
		  </div>
	  </div>
  </div>
  </fieldset>
	   </li>
			  <li class="control" style="display: none;">
			  <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_yes" value="1" checked="checked" title="Ship to this address" onclick="$('shipping:same_as_billing').checked = true;" class="radio" /><label for="billing:use_for_shipping_yes">Ship to this address</label></li>
		  <li class="control" style="display: none;">
			  <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_no" value="0" title="Ship to different address" onclick="$('shipping:same_as_billing').checked = false;" class="radio" /><label for="billing:use_for_shipping_no">Ship to different address</label>
		  </li>
		  </ul>
		  <div class="buttons-set" id="billing-buttons-container">
		  <p class="required">* Required Fields</p>
		  <button type="button" title="Continue" class="button"><span>Continue</span></button>
		  <span class="please-wait" id="billing-please-wait" style="display:none;">
			  <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...        </span>
	  </div>
  </fieldset>
		  </div>
	  </li>
	  <li id="opc-shipping" class="section">
		  <div class="step-title">
			  <h2>Shipping Information</h2>
			  <a href="#">Edit</a>
		  </div>
		  <div id="checkout-step-shipping" class="step a-item" style=" ">
			  <form action="" id="co-shipping-form">
	  <ul class="form-list">
			  <li id="shipping-new-address-form">
			  <fieldset>
				  <input type="hidden" name="shipping[address_id]" value="" id="shipping:address_id" />
				  <ul>
					  <li class="fields"><div class="customer-name">
	  <div class="field name-firstname">
		  <label for="shipping:firstname" class="required"><em>*</em>First Name</label>
		  <div class="input-box">
			  <input type="text" id="shipping:firstname" name="shipping[firstname]" value="" title="First Name" class="input-text required-entry" onchange="shipping.setSameAsBilling(false)" />
		  </div>
	  </div>
	  <div class="field name-lastname">
		  <label for="shipping:lastname" class="required"><em>*</em>Last Name</label>
		  <div class="input-box">
			  <input type="text" id="shipping:lastname" name="shipping[lastname]" value="" title="Last Name" class="input-text required-entry" onchange="shipping.setSameAsBilling(false)" />
		  </div>
	  </div>
  </div>
  </li>
					  <li class="fields">
						  <div class="fields">
							  <label for="shipping:company">Company</label>
							  <div class="input-box">
								  <input type="text" id="shipping:company" name="shipping[company]" value="" title="Company" class="input-text" onchange="shipping.setSameAsBilling(false);" />
							  </div>
						  </div>
								  </li>
					  <li class="wide">
						  <label for="shipping:street1" class="required"><em>*</em>Address</label>
						  <div class="input-box">
							  <input type="text" title="Street Address" name="shipping[street][]" id="shipping:street1" value="" class="input-text required-entry" onchange="shipping.setSameAsBilling(false);" />
						  </div>
						  </li>
								  <li class="wide">
						  <div class="input-box">
							  <input type="text" title="Street Address 2" name="shipping[street][]" id="shipping:street2" value="" class="input-text" onchange="shipping.setSameAsBilling(false);" />
						  </div>
					  </li>
								  <li class="fields">
						  <div class="field">
							  <label for="shipping:city" class="required"><em>*</em>City</label>
							  <div class="input-box">
								  <input type="text" title="City" name="shipping[city]" value="" class="input-text required-entry" id="shipping:city" onchange="shipping.setSameAsBilling(false);" />
							  </div>
						  </div>
						  <div class="field">
							  <label for="shipping:region" class="required"><em>*</em>State/Province</label>
							  <div class="input-box">
								  <select id="shipping:region_id" name="shipping[region_id]" title="State/Province" class="validate-select" style="display:none;">
									  <option value="">Please select region, state or province</option>
								  </select>
								  <input type="text" id="shipping:region" name="shipping[region]" value="" title="State/Province" class="input-text" style="display:none;" />
							  </div>
						  </div>
					  </li>
					  <li class="fields">
						  <div class="field">
							  <label for="shipping:postcode" class="required"><em>*</em>Zip/Postal Code</label>
							  <div class="input-box">
								  <input type="text" title="Zip/Postal Code" name="shipping[postcode]" id="shipping:postcode" value="" class="input-text validate-zip-international required-entry" onchange="shipping.setSameAsBilling(false);" />
							  </div>
						  </div>
						  <div class="field">
							  <label for="shipping:country_id" class="required"><em>*</em>Country</label>
							  <div class="input-box">
								  <select name="shipping[country_id]" id="shipping:country_id" class="validate-select" title="Country" onchange="shipping.setSameAsBilling(false);"><option value="" > </option><option value="AU" >Australia</option></select>                            </div>
						  </div>
					  </li>
					  <li class="fields">
						  <div class="field">
							  <label for="shipping:telephone" class="required"><em>*</em>Telephone</label>
							  <div class="input-box">
								  <input type="text" name="shipping[telephone]" value="" title="Telephone" class="input-text required-entry" id="shipping:telephone" onchange="shipping.setSameAsBilling(false);" />
							  </div>
						  </div>
						  <div class="field">
							  <label for="shipping:fax">Fax</label>
							  <div class="input-box">
								  <input type="text" name="shipping[fax]" value="" title="Fax" class="input-text" id="shipping:fax" onchange="shipping.setSameAsBilling(false);" />
							  </div>
						  </div>
					  </li>
									  <li class="no-display"><input type="hidden" name="shipping[save_in_address_book]" value="1" /></li>
								  </ul>
			  </fieldset>
		  </li>
		  <li class="control">
			  <input type="checkbox" name="shipping[same_as_billing]" id="shipping:same_as_billing" value="1" title="Use Billing Address" onclick="shipping.setSameAsBilling(this.checked)" class="checkbox" /><label for="shipping:same_as_billing">Use Billing Address</label>
		  </li>
	  </ul>
	  <div class="buttons-set" id="shipping-buttons-container">
		  <p class="required">* Required Fields</p>
		  <p class="back-link"><a href="#" onclick="checkout.back(); return false;"><small>&laquo; </small>Back</a></p>
		  <button type="button" class="button" title="Continue" onclick="shipping.save()"><span>Continue</span></button>
		  <span id="shipping-please-wait" class="please-wait" style="display:none;">
			  <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...        </span>
	  </div>
  </form>
  
		  </div>
	  </li>
	  <li id="opc-shipping_method" class="section">
		  <div class="step-title">
			  <h2>Shipping Method</h2>
			  <a href="#">Edit</a>
		  </div>
		  <div id="checkout-step-shipping_method" class="step a-item" style=" ">
			  <form id="co-shipping-method-form" action="">
	  <div id="checkout-shipping-method-load">
			  <dl class="sp-methods">
				<!-- <b> Ship To My Address</b>-->
		  <dt>Ship To My Address</dt>
		  <dd>
			  <ul>
											  <li>
																	 <input name="shipping_method" type="radio" value="excellence_excellence" id="s_method_excellence_excellence" class="radio"/>
  
						  
												  <label for="s_method_excellence_excellence">Ship to my address                                                                         (<span class="price">AU$10.00</span>)                                                </label>
									 </li>
						  </ul>
		  </dd> <br />
			<!-- <b> Collect From Store</b>-->
		  <dt>Collect From Store</dt>
		  <dd>
			  <ul>
											  <li>
																	 <input name="shipping_method" type="radio" value="freeshipping_freeshipping" id="s_method_freeshipping_freeshipping" class="radio"/>
  
						  
												  <label for="s_method_freeshipping_freeshipping">Collect from store                                                                                                                        </label>
									 </li>
						  </ul>
		  </dd> <br />
		  </dl>
  
	  </div>
  <div class="buttons-set" id="shipping-method-buttons-container">
	<p class="back-link"><a href="#" onclick="checkout.back(); return false;"><small>&laquo; </small>Back</a></p>
		  <button type="button" class="button" onclick="shippingMethod.save()"><span>Continue</span></button>
		  <span id="shipping-method-please-wait" class="please-wait" style="display:none;">
			  <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...        </span>
	</div>
  </form>
		  </div>
	  </li>
	  <li id="opc-payment" class="section">
		  <div class="step-title">
			  <h2>Payment Information</h2>
			  <a href="#">Edit</a>
		  </div>
		  <div id="checkout-step-payment" class="step a-item" style=" ">
	  <fieldset>
		  <dl class="sp-methods" id="checkout-payment-method-load">
  <div style="position:relative" class="v_m_logo">
	  <div style="position:absolute;top:0;right:0;"><img border="0" alt="visa and mastercard" src="http://www.legj.com.au/skin/frontend/default/hammertons/images/visaandmastercard.jpg"></div>
  </div>
	  <dt>
			  <input id="p_method_giftvoucher" value="giftvoucher" type="radio" name="payment[method]" title="Gift Voucher" onclick="payment.switchMethod('giftvoucher')" class="radio" />
			  <label for="p_method_giftvoucher">Gift Voucher </label>
	  </dt>
		  <dd>
		  <ul class="form-list" id="payment_form_giftvoucher" style="display:none;">
	  <li class="giftvoucher-description">
		  Use your gift voucher that you have to spend for this order!	</li>
				  <li class="notice-msg">
		  <ul>
			  <li>Grand total of this order is <strong><span class="price">AU$19.95</span></strong></li>
			  <li>You need to add a gift voucher or choose orther payment methods to spend on this order!</li>
		  </ul>
	  </li>
  <li>
	<label for="giftvoucher_code" class="required"><em>*</em>Your Gift Voucher Code</label>
		  <div class="input-box">
			  <input type="text" title="Your Gift Voucher Code" class="input-text required-entry" id="giftvoucher_code" name="payment[gift_code]"  />
		  </div>
		  To check your Gift Voucher balance, please click <a target="_blank" href="http://www.legj.com.au/site1/giftvoucher/index/check/">here</a>.	</li>
	  <li>
		  <div class="input-box">
			  <button type="button" class="button" id="giftvoucher_add" onclick="addGiftVoucher();">
				  <div id="giftvoucher_cache_url" style="display:none;">http://www.legj.com.au/site1/giftvoucher/checkout/addgift</div>
				  <input type="hidden" value="http://www.legj.com.au/site1/giftvoucher/checkout/addgift" />
				  <span>Add Gift Voucher</span>
			  </button>
			  <span id="giftvoucher_wait" style="display:none;">
				  <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Adding Gift Voucher..." title="Adding Gift Voucher..." class="v-middle" />
				  Adding Gift Voucher...			</span>
			  <input type="hidden" class="required-entry" id="giftvoucher_passed" />
		  </div>
	  </li>
  </ul>    </dd>
		  <dt>
			  <input id="p_method_hosted_pro" value="hosted_pro" type="radio" name="payment[method]" title="Payment by cards or by PayPal account" onclick="payment.switchMethod('hosted_pro')" class="radio" />
			  <label for="p_method_hosted_pro">Payment by cards or by PayPal account </label>
	  </dt>
		  <dd>
		  <ul id="payment_form_hosted_pro" style="display:none" class="form-list">
	  <li>
	  You will be required to enter your payment details after you place an order.    </li>
  </ul>
	  </dd>
	  </dl>
	  </fieldset>
  <div class="tool-tip" id="payment-tool-tip" style="display:none;">
	  <div class="btn-close"><a href="#" id="payment-tool-tip-close" title="Close">Close</a></div>
	  <div class="tool-tip-content"><img src="http://www.legj.com.au/skin/frontend/default/hammertons/images/cvv.gif" alt="Card Verification Number Visual Reference" title="Card Verification Number Visual Reference" /></div>
  </div>
  <div class="buttons-set" id="payment-buttons-container">
	  <p class="required">* Required Fields</p>
	  <p class="back-link"><a href="#" onclick="checkout.back(); return false;"><small>&laquo; </small>Back</a></p>
	  <button type="button" class="button" onclick="payment.save()"><span>Continue</span></button>
	  <span class="please-wait" id="payment-please-wait" style="display:none;">
		  <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...    </span>
  </div>
  </div>
	  </li>
	  <li id="opc-review" class="section">
		  <div class="step-title">
			  <h2>Order Review</h2>
			  <a href="#">Edit</a>
		  </div>
		  <div id="checkout-step-review" class="step a-item" style=" ">
			  <div class="order-review" id="checkout-review-load">
	  </div>
		  </div>
	  </li>
  </ol>
  <?php ActiveForm::end(); ?>
	  </div>
  </div>
  </div>
  
  <script type="text/javascript">
  $(document).ready(function(){
	  $("#checkoutSteps").accordion();
	  $(".step-title").addClass("ui-state-disabled");
	  $(".step-title").first().removeClass("ui-state-disabled");
		  var accordion = $("#checkoutSteps").data("uiAccordion");
		  accordion._std_clickHandler = accordion._clickHandler;
		  accordion._clickHandler = function( event, target ) {
			  var clicked = $( event.currentTarget || target );
			  if (! clicked.hasClass("ui-state-disabled")) {
				  this._std_clickHandler(event, target);
			  }
	  };
	  $('button').click(function(){
		  $('#checkout-form').yiiActiveForm('validate');
		  //console.log($(this).parents('li.section').find('.help-block-error').html());
		  //if($(this).parents('li.section').find('.help-block-error').html()=="")
			  $(this).parents('li.section').next().find('div.step-title').removeClass("ui-state-disabled").click();
	  })
  })
  </script>