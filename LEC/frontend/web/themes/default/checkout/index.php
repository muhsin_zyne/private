<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use frontend\components\Helper;
use bryglen\braintree\braintree;
use frontend\components\eway\Eway;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = $this->title;
$gift_wrap_check= \common\models\Stores::findOne(Yii::$app->params['storeId'])->hasGiftWrap;
$paymentMethod = \common\models\Stores::findOne(Yii::$app->params['storeId'])->paymentMethod;
//var_dump(Yii::$app->eway->encryptionKey);die();
$this->registerJsFile('../js/jquery.creditCardValidator.js', ['position' => \yii\web\View::POS_BEGIN]);
?>

<!-- <script type="text/javascript" src="js/inte.activeForm.js"></script> 

<script src="https://js.braintreegateway.com/v2/braintree.js"></script>-->

<script src="https://assets.braintreegateway.com/js/braintree-2.17.6.js"></script>
<!-- <script src="/js/braintree.js"></script> -->
<style type="text/css">
/*#checkout-step-payment{

  display: block !important;

 }*/

.fa-plus-circle {
  font-size: 18px;
  color: #BF1F3C;
}
.fa span {
  color: #000;
  font-size: 18px;
}
.fa-minus-circle {
  font-size: 18px;
  color: #BF1F3C;
}
#dropin-container {
  margin-bottom: 15px;
}
</style>

<div class="container">
<div class="site-checkout" id="site-checkout">
<div class="row checkindex">
<div class="col-xs-12">
<?php 

      $action = isset($orderId)? Url::to(['checkout/process', 'orderId' => $orderId]) : Url::to(['checkout/process']);

      $form = ActiveForm::begin(['id' => 'checkout-form', 'action' => $action,'options' => ['data-eway-encrypt-key'=>Yii::$app->eway->encryptionKey]]); ?>
<ul class="opc" id="checkoutSteps">
<?php if(Yii::$app->user->isGuest){ ?>
<li id="opc-login" class="section allow">
  <div class="panel-heading">
    <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
      <div class="numbers step-title">1</div>
      Checkout Method </a> </h4>
  </div>
  <!--/.panel-heading -->
  
      <div id="checkout-step-login" class="step a-item">
        <div class="col2-set">
          <div class="col-xs-7 new-customer-bg">
            <h1>Checkout as a Guest or Register</h1>
            <p>Register with us for future convenience:</p>
            
            
            <div class="radio-bg">
                            <div class="radio">
                              <label><input type="radio" name="checkout_method" id="login-guest" value="guest" class=" authentication">Checkout as Guest</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" name="checkout_method" id="login-register" value="register" class=" authentication">Register</label>
                            </div>
                        </div>
            
            <p>By creating an account you will be able to shop faster, be up to  date on an order's status,  and keep track of the orders you have previously made.</p>
            
            <!--<h4>Register and save time!</h4>
            <p>Register with us for future convenience:</p>
            <ul class="ul">
              <li>Fast and easy check out</li>
              <li>Easy access to your order history and status</li>
            </ul>-->
            <div class="buttons-set">
              <button id="onepage-guest-register-button" type="button" class="button continue"><span>Continue</span></button>
            </div>
          </div>
          <div class="col-xs-5 returning-customer">
            <h1>Returning Customer</h1>
            <!-- <form id="login-form" action="<?=Url::to(['site/login'])?>" method="post"> -->
            
            <fieldset>
              <p>Please log in below:</p>
              
              <div class="input-bg">
                 <div class="form-group">
                  <label for="login-email" class="required">Email Address*</label>
                  <input class="login-field input-text required-entry validate-email form-control" id="login-email" name="LoginForm[email]" value="">
                </div>
                <div class="form-group">
                  <label for="login-password" class="required">Password*</label>
                  <input type="password" class="login-field input-text required-entry form-control" id="login-password" name="LoginForm[password]" value="">
                </div>
            </div>
            
                <div class="buttons-set">
                  <div class="pull-left">
                  <button type="button" class="button login-button"><span>Login</span></button>
                  </div>
                  <div class="pull-right">
                  <a href="<?=Url::to(['site/request-password-reset'])?>" class="f-left">Forgot your password?</a>
                  </div>
                </div>
              
            </fieldset>
            
            <!-- </form> --> 
            
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
</li>
<?php } ?>
<li id="opc-billing" class="section">
<div class="panel-heading step-title">
  <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
    <div class="numbers ">2</div>
    Billing Information </a> </h4>
</div>
<!--/.panel-heading -->

<div id="checkout-step-billing" class="step a-item" style=" ">
<form id="co-billing-form" action="">
  <fieldset>
    <ul class="form-list">
      <li id="billing-new-address-form">
        <fieldset>
          <ul id="billing-address-form">
            <?php

                if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
                    $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
                else
                    $sessionVar = 'clientPortal';

                /*if(!Yii::$app->user->isGuest && \common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->exists() && !Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar) && !Yii::$app->cart->isClientPortalDeliveryProgram('clientPortal')){*/
                if(!Yii::$app->user->isGuest && \common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->exists()){ ?>
            <div class="field name-address row">
              <div class="col-xs-6">
              <label for="billing:address" class="required">Billing Address*</label>
              <div class="input-box one-field">
                <?=Html::dropDownList('CheckoutForm[billingAddress][customerAddressId]', [], ArrayHelper::map(\common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->all(), 'id', 'addressAsTextForDropdown') + ['new' => 'New Address'], ['class' => 'stored-billing-address form-control'])?>
              </div>
              </div>
            </div>
            <?php } ?>
            <li class="fields row">
              <div class="customer-name">
                <div class="field name-firstname col-xs-6">
                  <label for="billing:firstname" class="required">First Name*</label>
                  <div class="input-box">
                    <?=$form->field($checkout, 'billing_firstname')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->firstname] : [])->label('')?>
                  </div>
                </div>
                <div class="field name-lastname  col-xs-6">
                  <label for="billing:lastname" class="required">Last Name*</label>
                  <div class="input-box">
                    <?=$form->field($checkout, 'billing_lastname')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->lastname] : [])->label('')?>
                  </div>
                </div>
              </div>
            </li>
            <li class="fields row">
              <div class="field col-xs-6">
                <label for="billing:company">Company</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_company')->textInput()->label('')?>
                </div>
              </div>
              <div class="field col-xs-6">
                <label for="billing:email" class="required">Email Address*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'email')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->email] : [])->label('')?>
                </div>
              </div>
            </li>
            <li class="wide row">
              <div class="col-xs-12">
              <label for="billing:street1" class="required">Address*</label>
              <div class="input-box">
                <?=$form->field($checkout, 'billing_street')->textArea()->label('')?>
              </div>
              </div>
            </li>
            <li class="fields row">
              <div class="field col-xs-6">
                <label for="billing:city" class="required">Suburb/Town*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_city')->textInput()->label('')?>
                </div>
              </div>
              <div class="field col-xs-6">
                <label for="billing:region_id" class="required">State/Province*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])->label('')?>
                </div>
              </div>
            </li>
            <li class="fields row">
              <div class="field col-xs-6">
                <label for="billing:postcode" class="required">Zip/Postal Code</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_postcode')->textInput()->label('')?>
                </div>
              </div>
              <div class="field col-xs-6">
                <label for="billing:country_id" class="required">Country*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_country')->dropDownList(['Australia'])->label('')?>
                </div>
              </div>
            </li>
            <li class="fields row">
              <div class="field col-xs-6">
                <label for="billing:telephone" class="required">Telephone*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_phone')->textInput()->label('')?>
                </div>
              </div>
              <div class="field col-xs-6">
                <label for="billing:fax">Fax</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'billing_fax')->textInput()->label('')?>
                </div>
              </div>
            </li>

            <?php if(Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar)) { ?>
                <li class="fields row">
                  <div class="field col-xs-6">
                    <label for="shipping:telephone" class="required">Student Name</label>
                    <div class="input-box form-group ">
                      <input type="text" name="OrderMeta[byod_studentName]" class="byod_metavals form-control">
                    </div>
                  </div>
                  <div class="field col-xs-6">
                    <label for="shipping:fax">Parent Name</label>
                    <div class="input-box form-group ">
                      <input type="text" name="OrderMeta[byod_parentName]" class="byod_metavals form-control">
                    </div>
                  </div>
                </li>

                <li class="fields row">
                  <div class="field col-xs-6">
                    <label for="shipping:telephone" class="required">Order ID</label>
                    <div class="input-box form-group ">
                      <input type="text" name="OrderMeta[byod_orderId]" class="byod_metavals form-control">
                    </div>
                  </div>
                  <div class="field col-xs-6">
                    <label for="shipping:fax">Student ID</label>
                    <div class="input-box form-group ">
                      <input type="text" name="OrderMeta[byod_studentId]" class="byod_metavals form-control">
                    </div>
                  </div>
                </li>
                <?php } ?>

            <li class="no-display">
              <input type="checkbox" name="billing[save_in_address_book]" value="1" />
              Add to address book </li>
            <?php if(Yii::$app->user->isGuest){ ?>
            <li class="fields password row" id="register-customer-password">
              <div class="field col-xs-6">
                <label for="billing:customer_password" class="required">Password*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'password')->passwordInput()->label('')?>
                </div>
              </div>
              <div class="field col-xs-6">
                <label for="billing:confirm_password" class="required">Confirm Password*</label>
                <div class="input-box">
                  <?=$form->field($checkout, 'confirmPassword')->passwordInput()->label('')?>
                </div>
              </div>
            </li>
            <?php } ?>
          </ul>
          <div id="window-overlay" class="window-overlay" style="display:none;"></div>
          <div id="remember-me-popup" class="remember-me-popup" style="display:none;">
            <div class="remember-me-popup-head">
              <h3>What's this?</h3>
              <a href="#" class="remember-me-popup-close" title="Close">Close</a> </div>
            <div class="remember-me-popup-body">
              <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
              <div class="remember-me-popup-close-button a-right"> <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a> </div>
            </div>
          </div>
        </fieldset>
      </li>
      <li class="control" style="display: none;">
        <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_yes" value="1" checked="checked" title="Ship to this address" onclick="$('shipping:same_as_billing').checked = true;" class="radio" />
        <label for="billing:use_for_shipping_yes">Ship to this address</label>
      </li>
      <li class="control" style="display: none;">
        <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_no" value="0" title="Ship to different address" onclick="$('shipping:same_as_billing').checked = false;" class="radio" />
        <label for="billing:use_for_shipping_no">Ship to different address</label>
      </li>
    </ul>
    <div class="buttons-set" id="billing-buttons-container">
      <p class="required star">* Required Fields</p>
      <button type="button" title="Continue" class="button continue billing_continue"><span>Continue</span></button>
      <span class="please-wait" id="billing-please-wait" style="display:none;"> <img src="" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step... </span> </div>
  </fieldset>
  </div>
  </li>
  <?php /*if(!Yii::$app->cart->hasOnlyGiftVouchers()){ 
      if(!Yii::$app->cart->isClientPortalInstorePickup($sessionVar) && !Yii::$app->cart->isClientPortalInstorePickup('clientPortal') && Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar)) { */

        if(!Yii::$app->cart->hasOnlyGiftVouchers()){ 
          if(Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar))  {
  ?>
  <li id="opc-shipping" class="section">
    <div class="panel-heading step-title">
      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        <div class="numbers">3</div>
        Shipping Information </a> </h4>
    </div>
    <!--/.panel-heading -->
    
    <div id="checkout-step-shipping" class="step a-item" style=" ">
      <fieldset>
        <ul class="form-list">
          <li id="shipping-new-address-form">
            <fieldset>
              <ul id="shipping-address-form">
                <?php

        /*if(!Yii::$app->user->isGuest && \common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->exists() && !Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar)){*/

          if(!Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar)){
            
         ?>
                <div class="field name-address row">
                  <div class="col-xs-6">
                  <label for="shipping:address" class="required">Shipping Address*</label>
                  <div class="input-box one-field">
                    <?=Html::dropDownList('CheckoutForm[shippingAddress][customerAddressId]', [], ArrayHelper::map(\common\models\UserAddresses::find()->where(['userId' => Yii::$app->user->id])->all(), 'id', 'addressAsText') + ['new' => 'New Address'], ['class' => 'stored-shipping-address form-control'])?>
                  </div>
                  </div>
                </div>
                <?php } ?>
                <li class="fields ">
                  <div class="customer-name row">
                    <div class="field name-firstname col-xs-6">
                      <label for="shipping:firstname" class="required">First Name*</label>
                      <div class="input-box">
                        <?=$form->field($checkout, 'shipping_firstname')->textInput()->label('')?>
                      </div>
                    </div>
                    <div class="field name-lastname col-xs-6">
                      <label for="shipping:lastname" class="required">Last Name*</label>
                      <div class="input-box">
                        <?=$form->field($checkout, 'shipping_lastname')->textInput()->label('')?>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="fields row">
                  <div class="field col-xs-6">
                    <label for="shipping:company">Company</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_company')->textInput()->label('')?>
                    </div>
                  </div>
                  
                  <!--         <div class="field">

                        <label for="shipping:email" class="required"><em>*</em>Email Address</label>

                        <div class="input-box">

                            <?=$form->field($checkout, 'email')->textInput()->label('')?>

                        </div>

                    </div> --> 
                  
                </li>
                <li class="wide row">
                  <div class="col-xs-12">
                  <label for="shipping:street1" class="required">Address*</label>
                  <div class="input-box">
                    <?=$form->field($checkout, 'shipping_street')->textArea()->label('')?>
                  </div>
                  </div>
                </li>
                
                <li class="fields row">
                  <div class="field  col-xs-6">
                    <label for="shipping:city" class="required">Suburb/Town*</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_city')->textInput()->label('')?>
                    </div>
                  </div>
                  <div class="field col-xs-6">
                    <label for="shipping:region_id" class="required">State/Province*</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])->label('')?>
                    </div>
                  </div>
                </li>
                <li class="fields row">
                  <div class="field col-xs-6">
                    <label for="shipping:postcode" class="required">Zip/Postal Code</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_postcode')->textInput()->label('')?>
                    </div>
                  </div>
                  <div class="field col-xs-6">
                    <label for="shipping:country_id" class="required">Country</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_country')->dropDownList(['Australia'])->label('')?>
                    </div>
                  </div>
                </li>
                <li class="fields row">
                  <div class="field col-xs-6">
                    <label for="shipping:telephone" class="required">Telephone*</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_phone')->textInput()->label('')?>
                    </div>
                  </div>
                  <div class="field col-xs-6">
                    <label for="shipping:fax">Fax</label>
                    <div class="input-box">
                      <?=$form->field($checkout, 'shipping_fax')->textInput()->label('')?>
                    </div>
                  </div>
                </li>

                <li class="no-display">
                  <input type="checkbox" name="shipping[save_in_address_book]" value="1" />
                  Add to address book </li>
                <?php if(!Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar) && !Yii::$app->cart->isClientPortalDeliveryProgram('clientPortal')) { ?>  
                <li class="fieldss">
                  <div class="field">
                    <div class="input-box">
                      <div class="form-group field-checkoutform-shipping_phone has-success">
                        <input name="CheckoutForm[sameAsBilling]" type="checkbox" id="shipping-sameas-billing" checked/>
                        <label for="shipping:fax">Same as Billing Address</label>
                      </div>
                    </div>
                  </div>
                </li>
                <?php } ?>
              </ul>
              <div id="window-overlay" class="window-overlay" style="display:none;"></div>
            </fieldset>
          </li>
          <li class="control" style="display: none;">
            <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_yes" value="1" checked="checked" title="Ship to this address" onclick="$('shipping:same_as_billing').checked = true;" class="radio" />
            <label for="billing:use_for_shipping_yes">Ship to this address</label>
          </li>
          <li class="control" style="display: none;">
            <input type="radio" name="billing[use_for_shipping]" id="billing:use_for_shipping_no" value="0" title="Ship to different address" onclick="$('shipping:same_as_billing').checked = false;" class="radio" />
            <label for="billing:use_for_shipping_no">Ship to different address</label>
          </li>
        </ul>
        <div class="buttons-set" id="billing-buttons-container">
          <p class="required">* Required Fields 
            <span style="color:#ccc;font-style:italic;font-size:13px;">
                <?php if(Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar) || Yii::$app->cart->isClientPortalDeliveryProgram('clientPortal')) { ?>
                  Your Shipping Address Cannot be Edited as you are using a BYOD/Client Code.
                <?php } ?>  
            </span>
          </p>
          <button type="button" title="Continue" class="button continue shipping_continue"><span><span>Continue</span></span></button>
          <span class="please-wait" id="billing-please-wait" style="display:none;"> <img src="" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step... </span> </div>
      </fieldset>
    </div>
  </li>
  <?php  } ?>
  <li id="opc-shipping_method" class="section">
    <div class="panel-heading step-title">
      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        <div class="numbers">4</div>
        Shipping Method </a> </h4>
    </div>
    <!--/.panel-heading -->
    
    <div id="checkout-step-shipping_method" class="step a-item" style=" ">
      <div id="checkout-shipping-method-load">
        
          
          <!-- <b> Ship To My Address</b>-->
          <?php if(!Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}() && !Yii::$app->cart->hasOnlyClientPortalProducts()){ 
            
          ?>
          
          <?=$form->field($checkout, 'shipping_method')->radioList(ArrayHelper::map($shippingMethods, 'id', 'detail'), 

                                    [

                                        'item' => function($index, $label, $name, $checked, $value){

                                            $details = explode(":", $label);

                                            $class_text = explode("(", $label);

                      //var_dump(str_replace(" ", "",$class_text['0']));die();



                                            return '<div class="radio-bg"><div class="radio"><label><input id="checkoutform-shipping_method" class="shipping-method-radio '.str_replace(" ", "",$class_text['0']).'" data-price="'.$details[1].'" type="radio" name="'.$name.'" value="'.$value.'"> '.$details[0].'</label></div></div>';

                                        }

                                    ]

                                )?>
                         <div id="ci-store" title="Basic dialog">
                            <p align="center" class="pp-head"><b>Collect from Store!</b></p>
                          <p class="pp-cnt">Once your order is ready, we will contact you via email to advise your item/s are available for collection.Please bring driver's licence or any photo ID when you arrive for collection (this ID should match with the details furnished while placing the order). Orders will generally be available for collection within one business day, however some orders will take 3-5 business days.</p>

                        <p class="pp-cnt">In peak trading periods or where item/s in your order are not available and need to be sourced from a supplier, there may be delays to the above schedule. When this occurs we will notify you and discuss alternative arrangements.</p>
                        <p align="center">
                          <button type="button" id="ci-store-ok" class="cfstore button ">OK</button>
                          <button type="button" id="ci-store-cancel" class="cfstore button ">CANCEL</button>
                        </p>
                      </div>       
                                
              <?php } else {
                if(Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar) || Yii::$app->cart->isClientPortalDeliveryProgram('clientPortal')){


                  $name = "CheckoutForm[shipping_method]";
                  $items = [2 => "  Ship to above address(AU$0.00)"];
                  $selection = '';

                  echo Html::radioList($name, $selection, $items, [
                      'item' => function ($index, $label, $name, $checked, $value) {  
                          $disabled = false; // replace with whatever check you use for each item
                          /*return Html::radio($name, $checked, [
                              'value' => $value,
                              'label' => Html::encode($label),
                              'disabled' => $disabled,
                              'class'=>'shipping-method-radio Shiptomyaddress',
                              'id'=>'checkoutform-shipping_method',
                              'data-price'=>'0.00',
                          ]);*/
                          return '<div class="radio-bg"><div class="radio"><label><input id="checkoutform-shipping_method" class="shipping-method-radio Shiptomyaddress" data-price="0.00" type="radio" name="'.$name.'" value="2" checked="checked"> Ship to above address(AU$0.00)</label></div></div>';
                      },
                  ]);

              ?> 


              <?php }  
              elseif(Yii::$app->cart->isClientPortalInstorePickup($sessionVar) || Yii::$app->cart->isClientPortalInstorePickup('clientPortal')){   //var_dump($defaultAddress->billingAddress);die();

                  $collectWithAddress =  "Collect from Store(AU$0.00) : ".$defaultAddress->billingAddress;
                  $name = "CheckoutForm[shipping_method]";
                  $items = [1 => $collectWithAddress];
                  $selection = '';

                  echo Html::radioList($name, $selection, $items, [
                      'item' => function ($index, $label, $name, $checked, $value) {
                          $disabled = false; // replace with whatever check you use for each item
                          /*return Html::radio($name, $checked, [
                              'value' => $value,
                              'label' => Html::encode($label),
                              'disabled' => $disabled,
                              'class'=>'shipping-method-radio CollectfromStore',
                              'id'=>'checkoutform-shipping_method',
                              'data-price'=>'0.00',
                          ]);*/
                          return '<div class="radio-bg"><div class="radio"><label><input id="checkoutform-shipping_method" class="shipping-method-radio CollectfromStore" data-price="0.00" type="radio" name="'.$name.'" value="1" checked="checked"> '.$label.' </label></div></div>';
                      },
                  ]);

              } } ?>                 
                                
        
        <div class="collect_store_div">
          <?php if(isset($addresses) && count($addresses) > 1) { //var_dump($addresses[0]->billingAddress);die(); ?>
          <div class="row">
          <dl class='collect_store col-xs-6' style="display:none">
            <?=Html::dropDownList('CheckoutForm[storeAddressId]', [], ArrayHelper::map($addresses, 'id', 'addressAsText'), ['class' => 'store-address form-control'])?>
           </dl>
          </div>
          <?php } else {  foreach ($addresses as $address) { ?>
           
           <input type="hidden" name="CheckoutForm[storeAddressId]" value="<?=$address->id?>">
          
          <?php } } ?>
        </div>
        <?php //} ?>
      </div>
      <div id="onepage-checkout-shipping-method-additional-load"></div>
      <div class="buttons-set" id="shipping-method-buttons-container">
        <button type="button" class="button continue shipping_method_continue"><span><span>Continue</span></span></button>
        <span id="shipping-method-please-wait" class="please-wait" style="display:none;"> <img src="" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step... </span> </div>
    </div>
  </li>
  <?php  } ?>
  <li id="opc-payment" class="section">
    <div class="panel-heading step-title">
      <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        <div class="numbers">5</div>
        Payment Information </a> </h4>
    </div>
    <!--/.panel-heading -->
    
    <div id="checkout-step-payment" class="step a-item checkout-payment-form" style=" ">
    <fieldset>
    <dl class="sp-methods" id="checkout-payment-method-load">
    
    <!-- <div style="position:relative" class="v_m_logo">

  <div style="position:absolute;top:0;right:0;"><img border="0" alt="visa and mastercard" src="http://www.legj.com.au/skin/frontend/default/hammertons/images/visaandmastercard.jpg"></div>

</div> -->
    
    <div id="checkout-review-table-wrapper">
      <table class="data-table check-table" id="checkout-review-table">
        
        <thead>
          <tr class="first last">
            <th rowspan="1">Product Name</th>
            <th colspan="1" class="a-center">Price</th>
            <th rowspan="1" class="a-center">Qty</th>
            <th colspan="1" class="a-center">Subtotal</th>
          </tr>
        </thead>
        <tfoot>
          <tr class="first sub-total">
            <td style="" class="a-right" colspan="3"> Subtotal </td>
            <td style="" class="a-right last"><span data-price="<?=Yii::$app->cart->cost?>" class="price">
              <?=Helper::money(Yii::$app->cart->cost)?>
              </span></td>
          </tr>
          <?php if ($gift_wrap_check) { ?>
            <tr class="first sub-total">
               <td style="" class="a-right" colspan="3">
                    Gift Wrapping Total    </td>
               <td style="" class="a-right last">
                   <span data-price="<?=Yii::$app->cart->cost?>" class="price"><?= Helper::money(Yii::$app->cart->giftWrapcost) ?></span>    </td>
           </tr>
           <?php } ?>
          <tr class="payment-summary">
            <td id="shipping-method" style="" class="a-right" colspan="3"> Shipping &amp; Handling </td>
            <td id="shipping-price" style="" class="a-right last"><span class="price">AU$0.00</span></td>
          </tr>
          <tr class="voucher-summary" style="display:none;">
            <td style="" class="a-right" colspan="3"> Gift Voucher Discount </td>
            <td id="voucher-discount" style="" class="a-right last"><span data-price="<?=$voucherDiscount?>" class="price">
              <?=Helper::money($voucherDiscount)?>
              </span></td>
          </tr>
          <tr class="coupon-summary" style="display:none;">
            <td style="" class="a-right" colspan="3"> Coupon Discount </td>
            <td id="coupon-discount" style="" class="a-right last"><span data-price="<?=$couponDiscount?>" class="price">
              <?=Helper::money($couponDiscount)?>
              </span></td>
          </tr>
          <tr class="last grand-total">
            <td style="" class="a-right" colspan="3"><strong>Grand Total</strong></td>
            <td style="" class="a-right last"><strong><span data-price="<?=Yii::$app->cart->getCost(true,true)?>" class="price">
              <?=Helper::money(Yii::$app->cart->getCost(true,true))?>
              </span></strong></td>
          </tr>
        </tfoot>
        <tbody>
          <?php 

            $positions = Yii::$app->cart->positions;

            foreach($positions as $position){ ?>
          <tr class="first last odd">
            <td><h3 class="product-name">
                <?=$position->product->name?>
              </h3></td>
            <td class="a-right"><span class="cart-price"> <span class="price">
              <?=Helper::money($position->price)?>
              </span> </span></td>
            <td class="a-center"><?=$position->quantity?></td>
            <td class="a-right last"><span class="cart-price"> <span class="price">
              <?=Helper::money($position->cost)?>
              </span> </span></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <?php 

    //var_dump($paymentMethod);die();
    
    if($paymentMethod != "eway") {

    $braintree = true;

    if(in_array(Yii::$app->params['storeId'], array(80,136,100,83,67,39,63,25,115,74)))

    $braintree = false; 

    if($braintree ==false) :?>
    <form action="" method="post">
      <input type="hidden" name="cmd" value="_xclick">
    </form>
<?php endif;?>
<?php } //data-toggle="collapse" href="#payment_form_giftvoucher_dd"  ?>

<?php if(!Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}() && !Yii::$app->cart->hasOnlyClientPortalProducts()){ ?>
<div class="giftvoucher-details"> 
  
  <i class="fa fa-plus-circle fa-gift-font" data-toggle="collapse" data-target="#giftvoucher-option"><span>Apply Gift Voucher</span></i>
  
  <div id="giftvoucher-option" class="collapse">
    <dd id="payment_form_giftvoucher_dd">
      <ul class="form-list" id="payment_form_giftvoucher">
        <li class="giftvoucher-description"> Use your gift voucher that you have to spend for this order! </li>
        <li class="giftvoucher-discount-code">
          <ul>
            <li></li>
            <?php 

                  if(isset(Yii::$app->session['appliedVouchers'])){

                      foreach(Yii::$app->session['appliedVouchers'] as $id => $amount){

                               $voucher = \common\models\GiftVouchers::findOne($id);

                      ?>
            <li class="voucher"> <strong> Gift Voucher
              <?=$voucher->code?>
              (<span class="price">
              <?=Helper::money($amount)?>
              </span>) </strong>
              <button type="button" data-id="<?=$id?>" id="remove_voucher_<?=$id?>" title="Remove" class="remove-voucher"> <img src="/images/btn_remove.gif" alt="Remove"> </button>
            </li>
            <?php }

                      }

                  ?>
          </ul>
        </li>
        <li class="notice-msg">
          <ul>
            <li>Grand total of this order is <strong><span data-price="<?=Yii::$app->cart->getCost(true)?>" class="price">
              <?=Helper::money(Yii::$app->cart->getCost(true))?>
              </span></strong></li>
          </ul>
        </li>
        <li>
          <label for="giftvoucher_code" class="required"><em>*</em>Your Gift Voucher Code</label>
        </li>
        <div class="row check-row">
          <li class="col-xs-4">
            <div class="input-box">
              <?=$form->field($checkout, 'voucherCode')->textInput()->label('');?>
            </div>
            
            <!-- To check your Gift Voucher balance, please click <a target="_blank" href="http://www.legj.com.au/site1/giftvoucher/index/check/">here</a>. --> 
            
          </li>
          <li class="col-xs-4">
            <div class="input-box">
              <button type="button" class="button apply-code" id="giftvoucher_add">
              <div id="giftvoucher_cache_url" style="display:none;">http://www.legj.com.au/site1/giftvoucher/checkout/addgift</div>
              <input type="hidden" value="http://www.legj.com.au/site1/giftvoucher/checkout/addgift" />
              <span><span>Add Gift Voucher</span></span>
              </button>
              <span id="giftvoucher_wait" style="display:none;"> <img src="" alt="Adding Gift Voucher..." title="Adding Gift Voucher..." class="v-middle" /> Adding Gift Voucher... </span>
              <input type="hidden" class="required-entry" id="giftvoucher_passed" />
            </div>
          </li>
        </div>
        <li id="giftvoucher_message">
          <ul class="help-block help-block-error error-msg" style="display:none;">
            <li>Invalid voucher code.</li>
          </ul>
        </li>
      </ul>
    </dd>
  </div>
</div>

<?php } ?>

<div class="payment-card-paypal"> </div>
<div id="checkout-step-review" class="step a-item">
  <div class="order-review" id="checkout-review-load">
    <div id="checkout-paypaliframe-load" class="authentication"></div>
    <div id="checkout-review-submit">
      <div class="buttons-set" id="review-buttons-container">

          <?php if(Yii::$app->cart->isPaymentRequired($sessionVar) && Yii::$app->cart->isPaymentRequired('clientPortal')){  ?>
            <?php if($paymentMethod == "braintree") { ?>
            <form>
              <div id="dropin-container"></div>
            </form>
            <button type="submit" title="Pay Now" class="button continue btn-checkout">Pay Now</button>
            <?php } 
              elseif($paymentMethod == "eway"){
                $eWAY = new Eway(['model'=>$checkout]);
            ?>
            <?php $eWAY->renderForm($form);//Eway::renderForm(); ?>
            <!-- <button type="submit" title="Pay Now" class="button continue btn-checkout">Pay Now</button> -->
          <?php }  else { ?>  
            <button type="submit" title="Checkout" class="button continue btn-checkout">Checkout</button>
          <?php } } else { ?>
            <button type="submit" title="Checkout" class="button continue btn-checkout">Checkout</button>
          <?php } ?>  
        <span class="please-wait" id="review-please-wait" style="display:none;"> <img src="" alt="Submitting order information..." title="Submitting order information..." class="v-middle"> Submitting order information... </span>
        
      </div>
    </div>
  </div>
  
  <!-- <img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="right" style="margin-right:7px;"> --> 
  
</div>
</dl>
</fieldset>

<!-- <div class="tool-tip" id="payment-tool-tip" style="display:none;">

    <div class="btn-close"><a href="#" id="payment-tool-tip-close" title="Close">Close</a></div>

    <div class="tool-tip-content"><img src="http://www.legj.com.au/skin/frontend/default/hammertons/images/cvv.gif" alt="Card Verification Number Visual Reference" title="Card Verification Number Visual Reference" /></div>

</div>

<div class="buttons-set" id="payment-buttons-container">

    <button type="button" class="button continue"><span><span>Continue</span></span></button>

    <span class="please-wait" id="payment-please-wait" style="display:none;">

        <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Loading next step..." title="Loading next step..." class="v-middle" /> Loading next step...    </span>

</div> -->

</div>
</li>

<!-- <li id="opc-review" class="section">

        <div class="step-title">

            <h2>Order Review</h2>

        </div> --> 

<!-- <div id="checkout-step-review" class="step a-item">

        <div class="order-review" id="checkout-review-load">    

        <div id="checkout-paypaliframe-load" class="authentication"></div>

        <div id="checkout-review-submit">

            <div class="buttons-set" id="review-buttons-container">

                <button type="submit" title="Pay Now" class="button btn-checkout"><span><span>Pay Now</span></span></button>

                <span class="please-wait" id="review-please-wait" style="display:none;">

                    <img src="http://www.legj.com.au/skin/frontend/default/default/images/opc-ajax-loader.gif" alt="Submitting order information..." title="Submitting order information..." class="v-middle"> Submitting order information...        </span>

            </div>

          </div>

      </div>

    </div> --> 

<!-- </li> -->

</ol>
<input id="hidden-submit" type="submit" style="display:none;"/>
<?php ActiveForm::end(); ?>
</div>
</div>
</div>
</div>
<?php 

if($paymentMethod != "eway") {

if($braintree==true){

$braintree = Yii::$app->braintree;

$clientToken = $braintree->call('ClientToken', 'generate', []);

}

}
//var_dump($clientToken);die(); 

?>
<script type="text/javascript">

function calculateGrandTotal(){
    
     var giftWrapCost ='<?= Yii::$app->cart->giftWrapcost; ?>';

    var shippingCost = 0.00;

    var discount = 0.00;

    var subTotal = $('.sub-total span.price').attr('data-price');

    if($('.shipping-method-radio').is(':checked'))

        shippingCost = $('.shipping-method-radio:checked').length > 0 ? $('.shipping-method-radio:checked').attr("data-price") : 0;

    if($('#voucher-discount .price').attr("data-price") != '0'){

        discount = $('#voucher-discount .price').attr("data-price");

        $('.voucher-summary').show();

    }

    if($('#coupon-discount .price').attr("data-price") != '0'){

        discount = +discount + +$('#coupon-discount .price').attr("data-price");

        $('.coupon-summary').show();

    }

    console.log(discount);

    return showAsFloat(((+subTotal + +shippingCost + +giftWrapCost - +discount) > 0)? (+subTotal + +shippingCost+ +giftWrapCost - +discount) : 0);

}

function showBTForm(){

  $('#dropin-container').show();

  $('.btn-checkout').die('click');

}

function hideBTForm(){
    //alert('hideBTForm');
  $('#dropin-container').hide();

  $('.btn-checkout').click(function(){

    $('#checkout-form').submit();

    $('#checkout-form').submit();

  })

}

$(document).ready(function(){
  
   $('.control-label').remove(); /*star remove*/

  $('#checkoutform-billing_firstname').change();

  $('#checkoutform-billing_lastname').change();

  <?php 

    if($paymentMethod != "eway") {

    if($braintree == true){ 

  ?>

  var clientToken ='<?php echo $clientToken ;?>';

  braintree.setup(clientToken, "dropin", {

      container: "dropin-container"

  });

  <?php } } ?>

    $('.login-button').click(function(){

        submitLogin();

    })

    calculateGrandTotal();

    $('.shipping-method-radio').click(function(){

        $('#shipping-price .price').html('AU$' + $(this).attr("data-price"));

        $('.grand-total .price').html("AU$" + calculateGrandTotal());

        $('.notice-msg span.price span').html('AU$'+calculateGrandTotal());

    })

    $('body').on('click', 'button.remove-voucher', function(){

        var removeButton = this;

        $.ajax({

            url: "<?=Url::to(['checkout/removevoucher'])?>",

            method: 'POST',

            data: {id: $(removeButton).attr("data-id")}

        }).success(function(data){

            data = $.parseJSON(data);

            if(data.status=="success"){

                $(removeButton).parents('li.voucher').remove();

                $('#voucher-discount .price').attr("data-price", showAsFloat(data.discount));

                $('#voucher-discount .price').html("AU$"+showAsFloat(data.discount));

                $('.notice-msg span.price span').html('AU$'+calculateGrandTotal());

                $('.notice-msg span.price span').attr('data-price', calculateGrandTotal());

                $('.grand-total .price').html('AU$'+calculateGrandTotal());

                $('.grand-total .price').attr('data-price', calculateGrandTotal());

                if(calculateGrandTotal() != "0.00"){

                  showBTForm();

                }

            }

        })

    })

    $('#giftvoucher_add').click(function(){

        $.ajax({

            url: "<?=Url::to(['checkout/applyvoucher'])?>",

            method: 'POST',

            data: {code: $('#checkoutform-vouchercode').val(), grandTotal: $('.grand-total .price').attr("data-price")}

        }).success(function(data){

            data = $.parseJSON(data);

            if(data.status=="success"){

                $('.error-msg').hide();

                $('.giftvoucher-discount-code ul').append('<li class="voucher"><strong>Gift Voucher '+data.code+' (<span class="price"><span class="">AU$'+showAsFloat(data.discountFromThis)+'</span></span>)</strong><button type="button" data-id="'+data.id+'" id="remove_voucher_'+data.id+'" title="Remove" class="remove-voucher"><img src="/images/btn_remove.gif" alt="Remove"></button><input type="hidden" name="CheckoutForm[voucherCode]"/></li>');

                $('#voucher-discount .price').html('AU$'+data.discount);

                $('#voucher-discount .price').attr('data-price', data.discount);

                $('.notice-msg span.price span').html('AU$'+calculateGrandTotal());

                $('.voucher-summary').show();

                $('.grand-total .price').html('AU$'+calculateGrandTotal());

                $('.grand-total .price').attr('data-price', calculateGrandTotal());

                if(calculateGrandTotal() == "0.00"){

                  hideBTForm();

                }

            }else if(data.status=="failed"){

                $('.error-msg').show();

                $('.error-msg').html("Invalid Voucher Code.");

            }else

                $('.error-msg').hide();

        })

    })

    // $('#p_method_giftvoucher').click(function(){

    //     $('#payment_form_giftvoucher').show();

    //     $('#payment_form_cards').hide();

    // })

    // $('#p_method_cards').click(function(){

    //     $('#payment_form_giftvoucher').hide();

    //     $('#payment_form_cards').show();

    // })

  $("#checkoutSteps").accordion({

        heightStyle: "content"

    });
    //alert('hiii');

    $(".step-title").addClass("ui-state-disabled");

    $(".step-title").first().removeClass("ui-state-disabled");

        var accordion = $("#checkoutSteps").data("uiAccordion");

        accordion._std_clickHandler = accordion._clickHandler;

        accordion._clickHandler = function( event, target ) {

            var clicked = $( event.currentTarget || target );

            if (! clicked.hasClass("ui-state-disabled")) {

                this._std_clickHandler(event, target);

            }

    };

    $('button.continue').click(function(){
        //$('.eway-pay').attr("disabled", "disabled"); //preventing submission until validation is complete
        if($('input[type="radio"].authentication').length>0 && $('input[type="radio"].authentication:checked').length<1){
            alert('Please choose to register or checkout as a guest');
            return false;
        }
        $('#checkout-form').yiiActiveForm('validate');
        validateTab(this);
        var thisButton = this;
        setTimeout(function(){
            if($(thisButton).parents('li.section').find('.help-block-error').contents().length<1){
                $(thisButton).parents('li.section').next().find('div.step-title').removeClass("ui-state-disabled").click();
                $('.form-group').removeClass('has-error');
                $('.help-block-error').html('');
                if($(thisButton).hasClass("eway-pay")){
                    $('#checkout-form').submit();
                }
            }
        }, 300)
    
    });

    // $('#checkout-form').submit(function(){
    //     if($('.help-block-error').contents().length < 1){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // });

    $('input[type="radio"].authentication').click(function(){

        if($(this).attr("id")=="login-register"){
            $('.password').show();
            $('input[type="checkbox"][name="billing[save_in_address_book]"]').parents('.no-display').show();
            $('input[type="checkbox"][name="shipping[save_in_address_book]"]').parents('.no-display').show();
        }else{
            $('.password').hide();
            $('input[type="checkbox"][name="billing[save_in_address_book]"]').parents('.no-display').hide();
            $('input[type="checkbox"][name="shipping[save_in_address_book]"]').parents('.no-display').hide();
        }

    });

    $('#opc-billing .form-control').change(function(){ 

        if($('#shipping-sameas-billing:checked').length>0){

            var billingField = this;

            $('#'+$(billingField).attr("id").replace("billing", "shipping")).val($(billingField).val());

        }

    });

    $('.stored-billing-address').change(function(){

        if($('#shipping-sameas-billing:checked').length>0){

            $('.stored-shipping-address').val($('.stored-billing-address').val());

            $('.stored-shipping-address').change();

        }

    })



    $('#shipping-sameas-billing').click(function(){

        if($(this).is(':checked')){

            $('.stored-shipping-address').val($('.stored-billing-address').val());

            $('#opc-billing .form-control').each(function(){

                var billingField = this;

                $('#'+$(billingField).attr("id").replace("billing", "shipping")).val($(billingField).val());

            })

            $('.stored-shipping-address').change();

        }

    })

    <?php if(Yii::$app->cart->isClientPortalDeliveryProgram($sessionVar) || Yii::$app->cart->isClientPortalDeliveryProgram('clientPortal')) { ?>

        $('.billing_continue').click(function(){
          $('#checkoutform-shipping_firstname').val($('#checkoutform-billing_firstname').val());
          $('#checkoutform-shipping_lastname').val($('#checkoutform-billing_lastname').val());
          $('#checkout-step-shipping').find('input, textarea, select').attr('disabled','disabled');
          $('.byod_metavals').attr('disabled',false);
        });


    <?php } ?>  
    
    <?php if(!Yii::$app->user->isGuest){ ?>

    if($('.stored-billing-address').length > 0)

      $('#billing-address-form .fields, #billing-address-form .wide').hide();

    if($('.stored-shipping-address').length > 0)

      $('#shipping-address-form .fields, #shipping-address-form .wide').hide();

    $('.stored-billing-address').change(function(){

        if($(this).val()=="new"){

            $('#billing-address-form .fields, #billing-address-form .wide').show();

        }else{

            $('#billing-address-form .fields, #billing-address-form .wide').hide();

        }

    });

    $('.stored-shipping-address').change(function(){

        if($(this).val()=="new"){

            $('#shipping-address-form .fields, #shipping-address-form .wide').show();

            $('.no-display').css('display','block');

            //$(this).parent().next().find('.no-display').css('display','block');

            //$(this).parent().parent().addClass('hai');

        }else{

            $('#shipping-address-form .fields, #shipping-address-form .wide').hide();

            $('.no-display').css('display','none');

        }

    });

    $('#shipping-address-form .form-control').change(function(){

        $('#shipping-sameas-billing').prop('checked', false);

    })

    <?php } else {?>

        $('.no-display').css('display','none');

    <?php } ?>  

})

function validateTab(button){ 

    $(button).parents('.step').find('input, select, textarea').each(function(){ //console.log($(this).attr("id"));

        $('#checkout-form').yiiActiveForm('validateAttribute', $(this).attr("id"));

    })

}



function submitLogin () {

  var theForm, newInput1, newInput2;

  theForm = document.createElement('form');

  theForm.action = '<?=Url::to(["site/login"])?>';

  theForm.method = 'post';

  newInput1 = document.createElement('input');

  newInput1.type = 'hidden';

  newInput1.name = 'LoginForm[email]';

  newInput1.value = $('input[name="LoginForm[email]"]').val();

  newInput2 = document.createElement('input');

  newInput2.type = 'hidden';

  newInput2.name = 'LoginForm[password]';

  newInput2.value = $('input[name="LoginForm[password]"]').val();

  newInput3 = document.createElement('input');

  newInput3.type = 'hidden';

  newInput3.name = 'context';

  newInput3.value = 'checkout';

  theForm.appendChild(newInput1);

  theForm.appendChild(newInput2);

  theForm.appendChild(newInput3);

  document.getElementById('site-checkout').appendChild(theForm);

  theForm.submit();

}

</script> 

<!-- <script src="https://js.braintreegateway.com/v2/braintree.js"></script> --> 

<script>

$(document).ready(function() {

  /*if($('.stored-billing-address').length == 0){



  }*/

    



    //console.log(result);



    // $('.btn-checkout').click(function(){

    //  $('#checkout-form').yiiActiveForm('validate');

    //     validateTab(this);

    //     var thisButton = this;

    //     setTimeout(function(){

    //         if($(thisButton).parents('li.section').find('.help-block-error').contents().length<1){

    //             $('#hidden-submit').click();

    //             $('.form-group').removeClass('has-error');

    //             $('.help-block-error').html('');

    //         }

    //     }, 300)

    // })

  

  //$('.no-display').css('display','block');



  <?php if(!Yii::$app->user->isGuest){ ?>

    

    if($('.stored-billing-address').length > 0){

    var val = $('.stored-shipping-address').val();

    if (val == "new")

      $('.no-display').css('display','block');

    else

      $('.no-display').css('display','none');

  }

  <?php } ?>  

  





  $('.btn_checkout').click(function(){

    $(this).attr("type", "button");

  })



    $('body').on('click','.fa-plus-circle', function(){   

    $(this).removeClass('fa-plus-circle');

    $(this).addClass('fa-minus-circle');      

  });



  $('body').on('click','.fa-minus-circle', function(){   

    $(this).removeClass('fa-minus-circle');

    $(this).addClass('fa-plus-circle');      

  }); 



  $('body').on('click','.CollectfromStore', function(){   //console.log($('.collect_store').html());

    $('.collect_store').css('display','block');

    //$(this).closest('.radio').append($('.collect_store_div').html());

    //$(this).parent().next().css('display','block');
    //$(this).parent().next().find('.collect_store').css('display','block');
  

  }); 



  $('body').on('click','.Shiptomyaddress', function(){   //console.log($('.shipping-method-radio:checked').parent().text());

    $('.collect_store').css('display','none');

  }); 



  // $('.billing_continue').click(function(){

  //  var i=0;

  //  $("#checkout-step-billing .help-block-error").each(function() {

  //    if(!$(this).is(':empty')){

  //      i=1;

  //    }

  //  }); 

  //  if(i==0) {

  //    $('.checkout_billing').css('display','block');  

  //    if($('.checkout_billing').is(':empty')){

 //           $('.checkout_billing').append($('#checkoutform-billing_firstname').val()+' '+$('#checkoutform-billing_lastname').val()).append('<br />' );

  //          $('.checkout_billing').append($('#checkoutform-billing_company').val()).append('<br/>');

  //          $('.checkout_billing').append($('#checkoutform-billing_street').val()).append('<br/>');

  //          $('.checkout_billing').append($('#checkoutform-billing_city').val()+','+$('#checkoutform-billing_state option:selected').text()+','+$('#checkoutform-billing_postcode').val()).append('<br/>');

  //          $('.checkout_billing').append($('#checkoutform-billing_country option:selected').text()).append('<br/>');

  //          $('.checkout_billing').append('T:'+$('#checkoutform-billing_phone').val()).append('<br/>');

  //          //$('.checkout_billing').append('F:'+$('#checkoutform-billing_fax').val()).append('<br/>');

  //      } 



  //  }   

  // });



 //    $('.shipping_continue').click(function(){

 //     var j=0;

 //     $("#checkout-step-shipping .help-block-error").each(function() {

  //    if(!$(this).is(':empty')){

  //      j=1;

  //    }

  //  });

 //     if(j==0) {

  //    $('.checkout_shipping').css('display','block');

  //    if($('.checkout_shipping').is(':empty')){

  //          $('.checkout_shipping').append($('#checkoutform-shipping_firstname').val()+' '+$('#checkoutform-shipping_lastname').val()).append('<br />' );

  //          $('.checkout_shipping').append($('#checkoutform-shipping_company').val()).append('<br/>');

  //          $('.checkout_shipping').append($('#checkoutform-shipping_street').val()).append('<br/>');

  //          $('.checkout_shipping').append($('#checkoutform-shipping_city').val()+','+$('#checkoutform-shipping_state option:selected').text()+','+$('#checkoutform-shipping_postcode').val()).append('<br/>');

  //          $('.checkout_shipping').append($('#checkoutform-shipping_country option:selected').text()).append('<br/>');

  //          $('.checkout_shipping').append('T:'+$('#checkoutform-shipping_phone').val()).append('<br/>');

  //      }

  //  }         

  // }); 



  //   $('.shipping_method_continue').click(function(){

  //       $('.checkout_shipping_method').css('display','block');

    // $('.checkout_shipping_method').append($('.shipping-method-radio:checked').parent().text()).append('<br />' );

  //   });    

});    

</script> 
<script>//---aaron 
    $(document).ready(function(){     
      $('#ci-store').hide();
      $(".CollectfromStore").change(function(){ 
        $( "#ci-store" ).dialog({           
            modal: true,                   
            });       
        $("#ci-store-ok").click(function(){ 
          <?php if((isset($addresses) && count($addresses) <= 1)) { ?>
            $( ".shipping_method_continue" ).click();
           <?php } ?>
          $("#ci-store").dialog("close");
        }); 
        $("#ci-store-cancel").click(function(){ 
          $("#ci-store").dialog("close");
        });     
        
      });
    });
</script>
<style type="text/css">
  .ui-front.ui-widget-overlay {
      border: 0 none !important;    
      left: 0 !important;
      position: fixed !important;
      right: 0 !important;
      width: 100% !important; display: block !important;
  }
</style>
