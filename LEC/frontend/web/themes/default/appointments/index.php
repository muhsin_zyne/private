<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use kartik\date\DatePicker;

$this->title = "Book a Consultation";
//$this->params['breadcrumbs'][] = "Book a Design Consultation";


if(isset($_GET['date']))
	$selectedDate = $_GET['date'];
else
	$selectedDate = date('d-M-Y',strtotime(' +1 day'));

?>

<div class="content">
  <div class="container">
    <div class="content_area_home brand_list appointment-breadcrumb">
    	<!-- <div class="col-md-12"> 
            <ol class="breadcrumb">
                <?php  /*Breadcrumbs::widget([
                    'tag' => 'ol',
                    'options' => ['class' => 'breadcrumb'],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])*/ ?>
            </ol>
         </div> -->

        <div class="page-title">
            <h2><?=$this->title?></h2>
        </div>

        <div class="page-descri">
        	If you'd like to sit down with one of our friendly jewellers and put your ideas down on paper, simply choose an available time and enter your details. Our jewellers will help you articulate your ideas, and put together  a sketch to allow you to visualise exactly what your next piece look like.
        </div>

        <div class="bk-head">
        	<div class="booking-calendar">

        	Viewing booking for: <i class="fa fa-calendar"></i>
                        <?=DatePicker::widget([
                            'name' => 'appointments[bookingDate]',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => $selectedDate,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-M-yyyy',
                                'startDate'=>'+1d',
                            ]
                        ]);?>
        		
        	</div>
        	<div class="booking-calendat-text">
        		Booking for <span><?=$selectedDate?></span>
        	</div>
        </div>

       	<div class="category-products">
       		<div class="book-table">
                  <table class="table">
                    <tr>
                       	<?php 

                        if(!empty($timeSlots) && count(array_filter($timeSlots)) != 0){ //var_dump(count(array_filter($timeSlots)));die();

                          foreach ($timeSlots as $key => $slots) {  
                                $splitSlots = explode(" - ", $slots);
                                //var_dump($splitSlots);die();
                                if(count(array_filter($splitSlots)) != 0){
                         		      $startDate = new DateTime($selectedDate.$splitSlots[0]);
                                  $startTime = $startDate->format('Y-m-d H:i:s');
                         	?>
                         		<td>
  	                       		<div class="tl-book">
  	                                <input type="hidden" class="slots-inputs" value="<?=$slots?>">
  	                                <div class="tl-one"><?=$splitSlots[0]?></div>
  	                                <div class="tl-two"><span>to</span></div>
  	                                <div class="tl-one"><?=$splitSlots[1]?></div>
  	                            </div>	
  	                            <div class="drop-wrap">
  	                            <?php if($appointment->isAvailable($startTime)){  
                                          if($startTime < date('Y-m-d', strtotime(' +24 day'))){ 
                                ?>
  	                            		<a href="<?=\yii\helpers\Url::to(['appointments/create','date'=>$selectedDate,'timeslot'=>$slots])?>" class="ajax-update view-booking-class" data-title="Make an Appointment" data-width="600px" data-complete="location.reload();">
                                              	<button class="btn btn-primary booknow-button button">Book Now</button>
                                              </a>
  	                            <?php } else { ?>

                                  <button class="btn btn-primary contact-store">Book Now</button>

                                <?php } } else { if($appointment->isBooked($startTime)){ ?>
  	                            			
  	                            			<span>Booked</span>
  	                            
  	                            <?php	} else{ ?>
  	                            			
  	                            			<span>Unavailable</span>
  	                            <?php	} }  ?>		
  	                            </div>
  	                        </td>    
                         	<?php } } }  else{ ?>
                            Sorry, no booking available for this date.
                          <?php } ?>  
                    </tr>
                    <input type="hidden" class="selected-timeslot" value="" name="appointments[timeslot]">
                  </table>
               
                </div>
        </div>
	  </div>
  </div>  
</div>    

<script type="text/javascript">
    $(document).ready(function() {
        $('.booknow-button').click(function(){ 
            var timeslot = $(this).parent().parent().find('.slots-inputs').val();
            $('.selected-timeslot').val(timeslot);  
        });

        $("#w0").change(function(){
          	var selecteddate = $(this).val();
          	url = location.href.split('?')[0];
          	location.href = url + "?date="+selecteddate;
        });

        $('.contact-store').click(function(e){
            e.preventDefault();
            alert('Please contact store to book your appointment');
        });
    });   
</script>