<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;
?>

<div class="pop1">
    <div class="home-pop" id="dialog" >
      <div class="popon-head">Book a Consultation</div>
	    <div class="popon-sub">You are booking an appointment on <?=$date?>, <?=$timeslot?>.</div>
	    <?php $form = ActiveForm::begin(['id'=>'appointment-booking','enableAjaxValidation' => false,'enableClientValidation' => true]); ?>
	    	<div class="bookingform-left">
	    		<?php  // $form->field($appointment, 'name')->textInput(['maxlength' => 45,'placeholder'=>'Full Name'])->label('') ?>
	    		<?= $form->field($appointment, 'name')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->firstname,'maxlength' => 45,'placeholder'=>'Full Name'] : ['maxlength' => 45,'placeholder'=>'Full Name'])->label('') ?>
				<?= $form->field($appointment, 'email')->textInput((!Yii::$app->user->isGuest)? ['value' => Yii::$app->user->identity->email,'maxlength' => 45,'placeholder'=>'Email Address'] : ['maxlength' => 45,'placeholder'=>'Email Address'])->label('') ?>
				<?= $form->field($appointment, 'phone')->textInput(['maxlength' => 45,'placeholder'=>'Phone number'])->label('') ?>
				<?= $form->field($appointment, 'verifyCode',['template' => "{label}{input}\n<i class='fa fa-refresh'></i>\n{hint}\n{error}"])->widget(Captcha::className(), array('captchaAction' => '/appointments/captcha',));  ?>
			</div>
	    	<div class="bookingform-right">
	    		<?= $form->field($appointment, 'message')->textarea(['rows' => 5,'placeholder'=>'Any notes or special requests'])->label('') ?>
	    		<div class="form-group">	
            		<?= Html::submitButton('Reserve this time', ['class' => 'btn btn-success']) ?>
        		</div>
	    	</div>
	    	<input type="hidden" name="Appointments[bookingDate]" value="" class="appo-booking-date">
			<input type="hidden" name="Appointments[bookingSlot]" value="" class="appo-booking-slot">
	    <?php ActiveForm::end(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
    	var date = $('#w0').val();
    	var timeslot = $('.selected-timeslot').val();
    	$('.booking-date-desc').append(date+', '+timeslot);
    	$('.appo-booking-date').val(date);
    	$('.appo-booking-slot').val(timeslot);
		
		
	});

	$(".fa-refresh").click(function(event){ // captcha refresh button 
    	event.preventDefault();
    	$("img[id$='appointments-verifycode-image']").click();
	});
	
	

</script>
<style type="text/css">
  .ui-front{ width:743px !important; border:0 !important;}
  .ui-dialog .home-pop{ float:none;}
  .ui-widget-overlay.ui-front{ width:100% !important; height:100% !important; display:block !important;}
  
</style>