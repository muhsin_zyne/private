<?php
use frontend\components\Helper;
use yii\helpers\Url;
?>

    <?php $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$model->getThumbnailImage(); ?>

    <a class="Xpreview" title="<?=$model->name ?>" href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>">
        <img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>">
    </a> 
    
    <h3>
    <a title="<?=$model->name ?>" href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><?=Helper::stripText($model->name) ?></a>
    </h3>

    <div class="FeaturedPrice">
        <div class="price-box">
            <span id="" class="regular-price">
                    <span class="price">AU$ <?=$model->price?></span>                
            </span>
        </div>
    </div>

    <div class="cartButton">
        <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>">Add to Cart</a>
    </div>
