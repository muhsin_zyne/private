<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
 $this->title = $brand->title;
$this->params['breadcrumbs'][] = $brand->title;
?>

<div class="container">
    <div class="inner-container">        
        <div class="cms-pages-view">
            
            <p> <?=$brand->resolvedContent?> </p>  
            <div class="clearfix"></div>
        </div>       
    </div>   
</div>
