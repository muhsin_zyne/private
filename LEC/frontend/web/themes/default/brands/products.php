<?php



use yii\helpers\Html;

use yii\widgets\ListView;

use frontend\components\ProductFilter;

use common\models\Attributes;

use yii\widgets\Breadcrumbs;

use yii\helpers\Url;

?>



<?php 

    //var_dump($category->title);die();

    $this->title = $brand->title;

    $this->params['breadcrumbs'][] = $this->title;

?>



<div class="container">

    <div class="inner-container">

        <div class="row">

            <?php // if($category->urlKey != "gift-voucher"){ ?>

            <div class="content_area_products">

              

                <div class="col-xs-3">
                <div class="filter-outer">
                    <div class="block-title">

                         Refine Search

                    </div>

                    <div class="block-content">

                    <?php

                    if(!Yii::$app->request->isAjax){

                        ProductFilter::begin([

                            'basedModel' => $brand,

                            'query' => clone $products->query,

                            'attributes' => $brand->filterAttributes, //[['code' => 'material', 'id' => 15, 'Title' => 'material']],

                        ]);

                        ProductFilter::end();

                    }

                    ?>

                    </div>
                    </div>

               </div>

             <div class="col-xs-9">

                <div class="Featured Product_Featured listing">
                
                <div class="list-head">

                    <h2><?=$brand->title?></h2>

                <div class="toolbar">
                    <div class="pager">
                            <p class="amount">
                            <?=$products->totalCount?> Item(s)
                            </p>
                            <div class="limiter">
                                <!--<label>Show</label>-->
                                <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12 Per Page', '36' => '36 Per Page', '90' => '90 Per Page'])?>
                            </div>
                        </div>

                    <div class="sorter">
                            <div class="sort-by">
                                
                                <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['dateAdded' => 'Sort by New', 'price' => 'Sort by Price'])?>
                                <?php if(strpos(isset($_GET['sort'])? $_GET['sort'] : "", "-") !== FALSE){ ?>
                                    <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"><span class="custom_sort">ASC</span> </a>
                                <?php }else{ ?>
                                    <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    <div class="clearfix"></div>

                </div>
                
                </div>

               <div class="row">

                    <?php \yii\widgets\Pjax::begin(['id' => 'category-products','options' => ['class' => 'product_list']]); ?>

                    <?php if(Yii::$app->request->isAjax){ ?>

                    <?php 

                    

                        echo ListView::widget([

                            'id' => 'homepage-products',

                            'summary'=>'', 

                            'dataProvider' => $products,

                            'itemView' => '/products/_productlist', 

                            'itemOptions' => ['class' => 'col-xs-4']

                        ]); 

                        
                            echo "<script> $('.pagination li').click(function(){ window.scrollTo(0, 0); });
                                $(document).on('pjax:complete', function() { $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');$('.rating-star').rating({disabled: true, showClear: false, showCaption: false});  })</script>";

                    ?>



                    <?php } ?>

                    <?php  \yii\widgets\Pjax::end(); ?>

                    <?php 

                    if(!Yii::$app->request->isAjax){

                    ?> 

                        <div style="text-align: center;">

                            <img src="/themes/b2b/images/loading.gif"/>

                        </div>

                        

                    <?php } ?>

                </div>

            </div>          

        </div>

            </div>

            <?php //} ?>

            

        </div>

        </div>

    </div>

</div>

<script type="text/javascript">

$(document).ready(function(){

    $('body').on('change', '.limiter select', function(){

        $('#filter-form .page-size').val($(this).val());

        filterproducts();

    });



    $('body').on('change', '.sort-by select', function(){

        $('#filter-form .sort').val($(this).val());

        filterproducts();

    })

    if($('.list-view').length >0){

        $('.list-view').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>')

    }

    

})

</script>