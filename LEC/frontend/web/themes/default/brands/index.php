<?php



use yii\helpers\Html;

use yii\widgets\ListView;

use frontend\components\ProductFilter;

use yii\widgets\ActiveForm;

use yii\helpers\Url;

use yii\widgets\Breadcrumbs;

?>
<?php 

    //var_dump($category->title);die();

    $this->title = "OUR BRANDS";

    $this->params['breadcrumbs'][] = "Brands";

?>

        <div class="container">
          <div class="inner-container brand-view">
          
           <h1><?=strtoupper($this->title)?> </h1>
            <div class="category-products brands-wrapper"> 
              <div class="row">
              <!-- <div class="pager"></div> -->
              
              <?php \yii\widgets\Pjax::begin(['id' => 'category-products']); ?>
              <?php 
        
                                echo ListView::widget( [
        
                                'dataProvider' => $dataProvider,
        
                                'itemView' => '/brands/_brandlogo',
        
                                'itemOptions' => ['class' => 'items col-xs-3']
        
                                ] );

                                echo "<script> $(document).on('pjax:complete', function() { $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');})</script>";
        
                            ?>
              <?php  \yii\widgets\Pjax::end();  ?>
              <div class="clear"></div>
              </div>
              
              <!-- <div class="pager"></div>  --> 
              
            </div>
          </div>
        </div>
