<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
$this->title = $model->contentHeading;
$this->params['breadcrumbs'][] = $model->contentHeading;
?>

<div class="container">
    <div class="inner-container">
        <div class="cms-pages-view">
            <h1><?=strtoupper($model->contentHeading)?> </h1>
            <p> <?=$model->resolvedContent?> </p>  
        </div>
    </div>
</div>   
