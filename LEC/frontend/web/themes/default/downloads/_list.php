<?php 
use yii\helpers\Html;
?>
<div class="test">

    <div class="col-xs-10">
        <h6><?= $model->title?></h6>
        <i>Added on : <?= $model->formatedDateUpdated?></i>
        
        <div class="download">
            <span class="more">
				<?= $model->description?>
            </span>
        </div> 
    </div> 
    
    <div class="col-xs-2"> 
    	<div class="download-btn-bg">
        <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Download', ['/downloads/download','id'=>$model->id], ['class'=>'btn btn-primary btn-download']) ?>
        </div>
    </div>

</div>


  

