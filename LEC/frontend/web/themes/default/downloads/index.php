<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\widgets\Breadcrumbs;

$this->title = 'Download Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="inner-container download-bg">
        <h2>Downloads</h2> 
        <form id="sub-form"  action="/downloads/index" method="post" class="navbar-form pull-right" onsubmit="">
            <div class="input-group">
              <input type="text"  name="s-text" id="s-text"  placeholder="Search in Downloads" required="true" class="form-control">
              <div class="input-group-btn down">
                <button class="btn btn-default" type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i></button>
              </div>
            </div>
        </form>
        <?php
            $dataProvider->pagination = [
                'defaultPageSize' => 5,
            ];
             echo ListView::widget([
            'id' => '',
            'summary'=>'', 
            'dataProvider' => $dataProvider,                
            'itemView' => '/downloads/_list',
            'layout' => "{summary}\n{items}\n<div class='col-xs-12 text-center'>{pager}</div>",
            //'summary'=>'',
            //'layout' => "{items}",
            'itemOptions' => ['class' => 'col-xs-12 download-cont-bg']
        ]);?> 
       
    </div>
</div>


<script src="/js/jquery.scrollbar.js"></script>
<script>
 	jQuery(document).ready(function(){
	
		jQuery('.download.scrollbar-macosx').scrollbar();
	});
</script>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 320;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "View more";
    var lesstext = "View less";
    $('.more').each(function() {
        var content = $(this).html(); 
        if(content.length > showChar) { 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar); 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>'; 
            $(this).html(html);
        } 
    }); 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>

  

