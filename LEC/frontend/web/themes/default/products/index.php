<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use common\models\Attributes;
?>

<?php //var_dump($category->title);die(); ?>
<div class="container">
    <div class="listingpage">
    <div class="row">
        <div class="content_area_products">
        	<div class="col-xs-4">
        		<div class="block-title">
            		 <strong><span>Shop By</span></strong>
        		</div>
        		<div class="block-content" style="min-height:600px !important">
                <?php

                    ProductFilter::begin([
                        'basedModel' => $searchModel,
                        'query' => clone $products->query,
                        'attributes' => $searchModel->filterAttributes,
                    ]);
                    ProductFilter::end();
                ?>
        		</div>
        	</div>
            
            <?php //var_dump($products);die(); ?>
<div class="col-xs-8">
               	<div class="Featured Product_Featured listing">
                  	<h2><?=isset($category)? $category->title : ""?></h2>
                
                <div class="toolbar">
                    <div class="pager">
                        <p class="amount">
                          <strong><?=$products->totalCount?> Item(s)</strong>
                        </p>
                        <div class="limiter">
                        	<label>Show</label>
                            <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12', '36' => '36', '90' => '90'])?>
                            per page
                        </div>
                    </div>
                    <div class="sorter">
        				<div class="sort-by">
            				<label>Sort By</label>
                            <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? $_GET['sort'] : "", ['dateAdded' => 'New', 'price' => 'Price'])?>
            				<a title="Set Descending Direction" href=""><img class="v-middle" alt="Set Descending Direction" src="themes/site1/images/i_asc_arrow.gif"><span class="custom_sort">ASC</span> </a>
            			</div>
                    	<div class="clearfix"></div>
                	</div>
    				<div class="clearfix"></div>
                </div>

                <ul>
                	<?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
                    <?php if(Yii::$app->request->isAjax){ ?>
					<?php 
						echo ListView::widget([
				 		'id' => 'testclass',
				 		'dataProvider' => $products,
				 		'itemView' => '/categories/_product',
						'summary' => '',
						'itemOptions' => ['class' => 'items']
						] );
					?>
                    <?php } ?>
					<?php  \yii\widgets\Pjax::end();  ?> 
                </ul>


            </div>      	
        </div>
            </div>
        	
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('body').on('change', '.limiter select', function(){
        $('#filter-form .page-size').val($(this).val());
        filterproducts();
    });

    $('body').on('change', '.sort-by select', function(){
        $('#filter-form .sort').val($(this).val());
        filterproducts();
    })
})
</script>
