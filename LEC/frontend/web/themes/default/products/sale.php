<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use common\models\Attributes;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
?>

<?php 
    $this->title = "Sale";
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="listingpage">
    <div class="row">
            <?php // if($category->urlKey != "gift-voucher"){ ?>
            <div class="content_area_products">
              <div class="col-xs-12">
                <ol class="breadcrumb">
                    <?= Breadcrumbs::widget([
                        'tag' => 'ol',
                        'options' => ['class' => 'breadcrumb'],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </ol>
              </div>
                <div class="col-xs-4">
                
                    <div class="block-title">
                         <strong><span>Shop By</span></strong>
                    </div>
                    <div class="block-content">
                    <?php
                        ProductFilter::begin([
                            'basedModel' => $searchModel,
                            'query' => clone $products->query,
                            'attributes' => $searchModel->filterAttributes, //[['code' => 'material', 'id' => 15, 'Title' => 'material']],
                        ]);
                        ProductFilter::end();
                    ?>
                    </div>
               </div>
             <div class="col-xs-8">
                <div class="Featured Product_Featured listing">
                    <h2><?=$this->title?></h2>
                <div class="toolbar">
                    <div class="pager">
                        <p class="amount">
                          <strong><?=$products->totalCount?> Item(s)</strong>
                        </p>
                        <div class="limiter">
                            <label>Show</label>
                            <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12', '36' => '36', '90' => '90'])?>
                            per page
                        </div>
                    </div>
                    <div class="sorter">
                        <div class="sort-by">
                            <label>Sort By</label>
                            <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['dateAdded' => 'New', 'price' => 'Price'])?>
                            <?php if(strpos(isset($_GET['sort'])? $_GET['sort'] : "", "-") !== FALSE){ ?>
                            <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"><span class="custom_sort">ASC</span> </a>
                            <?php }else{ ?>
                            <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <ul>
                    <?php \yii\widgets\Pjax::begin(['id' => 'category-products','options' => ['class' => 'product_list']]); ?>
                    <?php if(Yii::$app->request->isAjax){ ?>
                    <?php 
                        echo ListView::widget([
                        'id' => 'category-products',
                        'dataProvider' => $products,
                        'itemView' => '/categories/_product',
                        'summary' => '',
                        'itemOptions' => ['class' => 'items']
                        ] );
                        
                        echo "<script> $('.pagination li').click(function(){
                             window.scrollTo(0, 0);
                        })</script>";
                    ?>

                    <?php } ?>
                    <?php  \yii\widgets\Pjax::end(); ?>
                    <?php 
                    if(!Yii::$app->request->isAjax){
                    ?> 
                        <div style="text-align: center;">
                            <img src="/themes/b2b/images/loading.gif"/>
                        </div>
                        
                    <?php } ?>
                </ul>
            </div>          
        </div>
            </div>
            <?php //} ?>
            
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('body').on('change', '.limiter select', function(){
        $('#filter-form .page-size').val($(this).val());
        filterproducts();
    });

    $('body').on('change', '.sort-by select', function(){
        $('#filter-form .sort').val($(this).val());
        filterproducts();
    })
    if($('.list-view').length >0){
        $('.list-view').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>')
    }
    
})
</script>