<?php
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;
use common\models\OrderComment;
use common\models\B2bAddresses;
use common\models\SalesComments;


$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
$smallImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/small/".$product->getSmallImage();
//var_dump($thumbImagePath);die();
?>

    <!-- <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
        <div class="modal-dialog modal-lg quick-view-wrapper" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="quick-left">
               <a href="<?=\yii\helpers\Url::to('@web/'.$product->urlKey.".html")?>" data-pjax="0"><img alt="" src="<?=$smallImagePath?>"></a>
            </div>
            <div class="quick-right">
                <div class="quick-head"><a href="<?=\yii\helpers\Url::to('@web/'.$product->urlKey.".html")?>" data-pjax="0"><?=$product->name?></a></div>
                <div class="star-row star-small">
                    <!-- <div class="stars five"></div> -->
                    <input id="input-id" type="text" class="rating rating-star" value="<?=$product->rating?>">
                </div>
                <div class="quick-desc">
                    <?=$product->short_description?>... <a href="#" class="view_desc">view more</a>
                </div>
                <div class="detail-descri" style="display:none">
                    <?=$product->description?>
                </div>
                <div class="quick-price"><?=Helper::money($product->price)?></div>
                <div class="pdt-btm quick-btns">
                    <a class="pdt-cart" href="<?=\yii\helpers\Url::to('@web/'.$product->urlKey.".html")?>"><i class=" fa fa-shopping-cart"></i> Add to cart</a> 
                    <?php if (!\Yii::$app->getUser()->isGuest) {?>
                        <?php $form = ActiveForm::begin(['action' => ['wishlist-items/create'],'options' => ['method' => 'post'],'id'=>'wishlist-form']) ?>                   
                            <input type="hidden" name="user_id" value="<?=Yii::$app->user->id?>">
                            <input type="hidden" name="store_id" value="<?=Yii::$app->params['storeId']?>">
                            <input type="hidden" name="product_id" value="<?=$product->id?>">
                            <?= Html::submitButton('<i class="fa fa-heart"></i>',array('class' => 'submit-wish')) ?>   
                        <?php ActiveForm::end(); ?>
                    <?php } else {?>
                        <a href="/site/login" class="fa fa-heart"></a>
                    <?php }?>
                </div>
            </div>
        </div>
   <!--  </div> -->
    
<script type="text/javascript">
    $(document).ready(function () {
        $(".view_desc").click(function() {
            $('.quick-desc').css('display','none');
            $('.detail-descri').css('display','block');
        });    
        $(".rating-star").rating({disabled: true, showClear: false, showCaption: false});
    });    
</script>        
            
            
