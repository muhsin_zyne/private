
<?php 
    use frontend\components\Helper;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();
?>




    <div class="product">
        <div class="pdt-img"> 
            <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>"></a> 
        </div>
        <div class="pdt-cnt">
            <div class="pdt-head"> <?=Helper::stripText($model->name,25) ?> </div>
            <div class="pr-left">
                <div class="price"><?=Helper::money($model->price)?></div>
                <div class="stars five"></div>
            </div>
            <div class="pr-right">
              <?=Html::a('<i class="fa fa-eye"></i>', ['products/view', 'id' => $model->id],['class'=>'modal-class sm-dialog','data-title' =>'',]); ?> <a href="#" class="fa fa-heart"></a>
            </div>
        </div>
        <div class="pdt-btm"> 
            <form method="POST" id="product_cart_form_<?=$model->id?>" action="<?=\yii\helpers\Url::to(['cart/add'])?>">
                <input type="hidden" id="products-id" name="Products[id]" value="<?=$model->id?>">
                <input type="hidden" name="qty" value="1"/>
            </form>
           <!--  <a class="pdt-cart" href="<?php //\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><i class=" fa fa-shopping-cart"></i> Add to cart</a>  -->
           <a class="add-to-cart" id="<?=$model->id?>" href="javascript:;">Add to Cart</a>
        </div>
    </div>
