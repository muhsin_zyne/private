<?php
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	use yii\widgets\ListView;
	use yii\widgets\ActiveForm;
	use frontend\components\Helper;
	use yii\helpers\Url;
	use backend\assets\AppAsset;
	use common\models\Stores;
	use common\models\Banners;
	use common\models\Products;
	use yii\widgets\Breadcrumbs;
	use kartik\rating\StarRating;
	//use yii\widgets\ListView;
?>
<?php
	
	$this->title = $product->name;
	//$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
	$id = $product->id;
	$store = Stores::findOne(Yii::$app->params['storeId']);
?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>
<?php $current_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];  ?>
<!--<div class="cart-heading">
	 <div class="container">
		<h4>HP DeskJet 1112 Printer</h4>
		<div class="pull-right">
			<ol class="breadcrumb">
				<li><a href=""><i class="fa fa-home"></i></a></li>
				
			</ol>
		</div>
	</div> 
</div>-->
<div class="container">
	<div class="inner-container">
		<div class="product_det">
			<div class="row">
				<div class="col-xs-4">
					<div class="pro_det_img">
						<a data-toggle="modal" data-target="#myModal" class="group <?php echo ($product->isCustomProduct()) ? 'ribbon-detail' : '' ?>" rel="group" title="<?=$product->name?>">
							<img src="<?=$smallImagePath?>" width="350" height="350">
						</a>
						<span class="sku-text">
							<?php if($product->video !=''):?>
							<a data-toggle="modal" data-target="#myModalv" style="cursor: pointer;"> <img src="http://root.inte.everybuy.com.au/store/Watch-Video.jpg" /> </a>
							<?php endif;?>
						</span>
					</div>
					
					<div class="modal fade image-popup" id="myModal" role="dialog"><!-- image popup -->
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">x</button>
								</div>
								<img src="<?=$baseImagePath?>"  alt="<?=$product->name ?>" /> </div>
							</div>
						</div>
						<!-- image popup end -->
					
						<div class="modal fade image-popup" id="myModalv" role="dialog"><!-- image popup -->
							<div class="modal-dialog" style="width:570px !important;">
								<div class="modal-content">
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">x</button>
									</div>
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $product->video;?>" frameborder="0" allowfullscreen></iframe>
								</div>
							</div>
						</div>
						<!-- image popup end -->
					
						<!-- <div class="description" style="white-space: pre-line;">
							<?php //Helper::stripText($product->description, 300)?>
						</div> -->
					</div>
					<div class="col-xs-8">
						<div class="product_detail">
							<h2><?= Html::encode($this->title) ?></h2>
							<div class="review-line">
	                          	<div class="star-small">
	                            	<?=StarRating::widget([
										'name' => 'rating_2',
										'value' => $product->rating,
										'pluginOptions' => [
												'readonly' => true,
												'showClear' => false,
												'showCaption' => false,
										],
									]);?>								
	                            </div>                        
								<div class="share">
									<div class="fb-share-button" data-href="<?=$current_url?>" data-layout="button"></div>
								</div>  
								<div style="margin-left:7px;display: inline-block;color: #969595;font-size: 11px;"><?=$product->sku?></div>
								<!-- <div class="stars five"></div>
								<a href="#">0 Reviews</a> <a href="#"><i class="fa fa-pencil"></i> Write a review</a>
								<div class="share">
									<div class="fb-share-button" data-href="<?=$current_url?>" data-layout="button"></div>
									<!-- <button class="btn btn-info share-btn">Share</button> --> 
								<!--</div> -->							
							</div>						
							<div class="detail_cart"> 
								<!-- <div class="fb-like" data-href="https://www.facebook.com/MiaBellaJewellery" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div> -->
								<?php if($product->typeId != "gift-voucher"){ ?>
									<div class="price">
										<?php /*Helper::money($product->price) */ ?>
										<?php if (isset(Yii::$app->session['schoolByod']) && $product->isInByod('schoolByod')) { 
				                            $gstPercentage = \common\models\Configuration::findSetting('gst_percentage',0);
				                            echo Helper::money(Helper::getPriceExclGST($product->price,$gstPercentage)); 
				                                echo "<div class='gst_text'>exc GST</div>";
	                           			} elseif(isset(Yii::$app->session['studentByod']) && $product->isInByod('studentByod')) {
	                           					echo Helper::money($product->price);
	                           					echo "<div class='gst_text'>incl. GST</div>";
	                           			} else{
	                           					echo Helper::money($product->price);
	                           			} ?>
	                           			<div class="special-desc">
											<?php if(!$product->isInPromotion()) { 
		                						if($product->hasSpecialPrice()) {
		            						?>        
		                					was <span class="strike-price-desc"><?=Helper::money($product->originalPrice)?></span>
		            						SAVE <?=Helper::money($product->originalPrice-$product->price)?>
		            						<?php } } ?>
		            					</div>
									</div>
								<?php } ?>
								<?php if($product->typeId == "configurable"){ ?>
									<!-- <div class="ring-size"> <a href="/images/ring_size_lej.pdf" target="_blank"> Ring Size Guide </a> </div> -->
								<? } ?>
								
								<?php /*if($product->byod_only == Yes) {  
									if($product->byod_only != "Yes" && (!(isset(Yii::$app->session['schoolByod']) && $product->isInByod('schoolByod')) && !(isset(Yii::$app->session['studentByod']) && $product->isInByod('studentByod')))) { */
								if(!$product->isByodOnly() || (($product->isByodOnly()) && ((isset(Yii::$app->session['schoolByod']) && $product->isInByod('schoolByod')) || (isset(Yii::$app->session['studentByod']) && $product->isInByod('studentByod'))))) { ?>
									<div class="product-options" id="product-options-wrapper">
										<?php  $form = ActiveForm::begin(['id' => 'products-form', 'action' => isset($_REQUEST['positionId'])? Url::to(['cart/update', 'positionId' => $_REQUEST['positionId']]) : Url::to(['cart/add'])]); ?>

										<div class="quantity"> 
											<!-- <label class="quan" for="qty">Qty:</label> -->
											<?= $form->field($product, 'qty')->textInput(['name' => 'qty','value'=>'1'])->label('Qty:', ['class' => 'quan']); ?>
											<!-- <input type="text" class="" title="Qty" value="1" maxlength="12" id="qty" name="qty"> --> 
										</div>
								
								 
										<?php //  var_dump($product->parent);die(); ?>
										<dl class="confg-options">
											<?php 
											if(($product->typeId == "simple" && $product->parent instanceof \common\models\Products && $product->parent->typeId == "configurable") || $product->typeId == "configurable"){ //var_dump($product->parent->id); die;?>
											<div class="config-attributes">
												<?=($product->typeId == "simple")? $product->parent->renderConfigurableFields($form, $product) : $product->renderConfigurableFields($form, $product);?>
											</div>
											<?php }elseif($product->typeId == "bundle"){ ?>
											<div class="config-attributes">
												<?=$product->renderBundleFields($form);?>
											</div>
											<?php }elseif($product->typeId == "gift-voucher"){ ?>
											<div class="config-attributes">
												<?=$product->renderVoucherFields($form);?>
											</div>
											<?php } ?>
										</dl>
										
										<?=Html::activeHIddenInput($product, 'id', ['name' => 'Products[id]']);?>
										<a title="Add to Cart" class="pdt-btn add_to_cart" href="javascript:;" onclick="$('form#products-form').submit();"><i class="fa fa-shopping-cart"></i> Add to Cart</a>
										<?php ActiveForm::end(); ?>
										 <?php if (!\Yii::$app->getUser()->isGuest) {?>
										 	<?php $form = ActiveForm::begin(['action' => ['wishlist-items/create'],'options' => ['method' => 'post']]) ?>
										 		<input type="hidden" name="user_id" value="<?=Yii::$app->user->id?>">
		                        				<input type="hidden" name="store_id" value="<?=Yii::$app->params['storeId']?>">
		                        				<input type="hidden" name="product_id" value="<?=$product->id?>">
		                        				<?= Html::submitButton('<i class="fa fa-heart"></i>',array('class' => 'submit-wishbtn')) ?>  
		                        			<?php ActiveForm::end(); ?>
										 
										 <?php }?>
									</div>
									<div class="brand-logo">
										<?php if(!empty($product->brand->path)){ 
											$logoPath = Yii::$app->params["rootUrl"].$product->brand->path;?>
											<img src="<?=$logoPath?>" alt="<?=$product->brand->title?>" width="150px">
										<?php } ?>
									</div>
									<div class="clearfix"></div>
								<?php } else {  ?>

									<div class="product-options"><span>Product Can be Purchased Only Using a BYOD Code</span></div>
									<div class="clearfix"></div>

								<?php }  ?>

								<?php 	if (!\Yii::$app->user->isGuest) {
									$form = ActiveForm::begin(['id' => 'review-form']); 
									echo '<label class="dtls-head">Rate this product</label>';
									echo $form->field($userReview, 'stars')->label(false)->widget(StarRating::classname(), [
										'pluginOptions' => ['size'=>'xs','disabled'=>false, 'showClear'=>false,'starCaptions'=>false],
										'pluginEvents' => [
											"rating.change" => 'function() {  
														//$(".review_btn").click();
														if(confirm("Are you sure you want to rate this product?")){
															$(".review_btn").click();
														}
														else{ 
															$("#productreviews-stars").rating("reset");
														}
											}',												 
										],
									]);
								} ?>							
							</div>
							
							<?php // if($product->typeId != "gift-voucher"){ ?>
							<!-- <h3 class="dtls-head">Product Details</h3>-->
							<?php /*DetailView::widget([
										'model' => $product,
										'attributes' => $product->detailViewAttributes,
								]); */
							?>
							<?php // } ?>
						</div>
						<h3 class="dtls-head product-details-head">Product Details</h3>
						<div class="pdt-text">
							<?=Helper::stripText($product->description, 10000)?>
	                        <?php echo $this->render('/products/_getmeta', compact('product')); ?>
						</div>
					</div>
					<!-- <div class="col-xs-12">
						<div class="review_wrapper" id="write_review">
								
							 <div class="review_head dtls-head">
										<!-- Reviews (<?php //count($allReviews)?>) -->
										<?php /*if(!empty($allReviews)) { ?>
												Reviews (<?=count($allReviews)?>)
										<?php } */?>
								<!-- </div>

									 <div class="review_content">
									 <?php //foreach ($allReviews as $review) {   ?>
											 
										<div class="review_sub_wrapper">
												<div class="review_sub_left">
														<div class="review_sub_left_raw stars"></div>
														<div class="review_sub_left_raw"><?php //$review->user->fullName?></div>
														<div class="review_sub_left_raw"><span><?php //Helper::date($review->createdDate)?></span></div>
												</div>
												<div class="review_sub_right">
														<span><?php //$review->title?></span> <?php //$review->description?> 
												</div>
										</div>
										<?php //} ?>
								</div> -->

								<?php 
										//if (!\Yii::$app->user->isGuest) {   //var_dump(count($allReviews));die();
								?>  
								<!-- <div class="review_form_wrapper">
										<div class="review_head dtls-head write-head">Write a Review</div>
										<!-- <input type="text" class="form-control"  onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" placeholder="Review Title" />
										<textarea class="review_comment_area form-control" onfocus="if(this.value==this.defaultValue)this.value='';">Your Review</textarea>
												<div class="staring_wrapper"> -->
										<?php // $form->field($userReview, 'productId')->hiddenInput(['class'=>'form-control','value'=>$product->id])->label(false) ?>    
										<?php // $form->field($userReview, 'title')->textInput(['class'=>'form-control']) ?>
										<?php // $form->field($userReview, 'description')->textarea(['rows' => 6,'class'=>'review_comment_area form-control']) ?>    
										<!--<div class="quan">Rating:</div>
										<div class="staring_two">add stars here</div>-->
										<?php //$form->field($userReview, 'productId')->hiddenInput(['class'=>'form-control','value'=>$product->id])->label(false) ?>
										<?php //Html::submitButton($userReview->isNewRecord ? 'Post Review' : 'Update Review', ['class' => 'review_btn','style'=>'display:none']) ?>
								<!-- </div> --> 
								 <?php //} ?>
								
						 <!--</div>
					</div> -->
				</div>
			 	</form>				
			</div>
			<div class="row">
				<?php 
        			//$latestProducts = Products::find()->orderBy(['dateAdded' => SORT_DESC])->limit(9)->all();
        			echo $this->render('/products/_after-detail', compact('latestProducts','dataProvider','featuredProducts','relatedProducts'));
        		?>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
	$(document).ready(function(){
	// 	$('.attribute-select').attr('disabled', 'disabled');
	// 	$('.config-attributes .form-group:first-child select').attr('disabled', false);
	// 	$(".attribute-select").change(function(){
	// 		var selected = $(this).val();
	// 		var attrId1 = $(this).attr('data-attrid');
	// 		//var price = <?=$product->price?>;
	// 		var prices =  $(".sum").text();
	// 		var price = prices.replace("AU$", "");
	// 	$('.config-attributes .form-group select').attr('disabled', false);

	// 	//$(this).next().addClass('hai');
	// 	$.ajax({
	// 				url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=products/getoptions',
	// 					type: 'POST',
	// 					data: {selected: selected, attrId1: attrId1, id:<?=$id?>, price: price},
	// 								success: function(data)
	// 								{
	// 									$(".sum").html(data);
	// 									console.log(data);
	// 				}
	// 	});
	// });
	<?php
	if($product->typeId == "simple"){ ?>
	$('.attribute-select').change(function(){
		$(this).parents('.config-attributes').prepend($(this).parents('.form-group'));
		$('#products-form').attr('action', '<?=Url::to(["products/configurate"])?>');
		$('#products-form').submit();
	});
	<?php } ?>
});
</script> 
<script type="text/javascript">
	$(document).ready(function(){

		/*$('#input-id').on('rating.change', function(event, value, caption) {
				console.log(value);
				console.log(caption);
		});*/

		$('.bundle_dropdown').on("change", function(){  
				var isdefined = $(this).find('option:selected').attr('data-isuserdefined');
				var defaultQty = $(this).find('option:selected').attr('data-defaultQty');
				var id = $(this).val();
			
				if(isdefined==1){
					$('.qtytext').remove();
					$('.bundle_dropdown').after('<div class="qtytext"><input type="text" class="item_qty" onchange="findvalues()" name="Products[BundleItems][qty]['+id+']" value="'+defaultQty+'"></div>');
				}
				else {
						if(id != ""){
						$('.qtytext').remove();
						$('.bundle_dropdown').after('<div class="qtytext">'+defaultQty+'<input type="hidden" class="item_qty" name="Products[BundleItems][qty]['+id+']" value="'+defaultQty+'"></div>');
					}
						else { $('.qtytext').hide(); }
				}
				findvalues();
		});
		$('.bundle_radio').on("click", function(){
			values = [];
			var params = $('.bundle_radio:checked').attr('data-params');
			values =  $.parseJSON(params);
			var id = $(this).val();
			$.each( values, function( key, value ) {
					if(value['data-isUserDefined']==1){
					$('.qtytextradio').remove();
					$('.'+id).next().after('<div class="qtytextradio">Qty: <input type="text" class="item_qtyradio" onchange="findvalues()" name="Products[BundleItems][qty]['+id+']" value="'+value['data-defaultQty']+'"></div>');
				}
				else {
						if(id != ""){
							$('.qtytextradio').remove();
							$('.bundle_radio label').after('<div class="qtytextradio">'+value['data-defaultQty']+'<input type="hidden" class="item_qtyradio" name="Products[BundleItems][qty]['+key+']" value="'+value['data-defaultQty']+'"></div>');
						}
					else { $('.qtytextradio').hide(); }
				}	
				
			});
			findvalues();
		});	
		$('.bundle_checkbox').on("click", function(){ 
			findvalues();
		});	
	
		$('.bundle_multisel').on("click", function(){
			findvalues();
		});	

		/*$('#wishlist').on("click", function(){
			<?php  
			if(!\Yii::$app->getUser()->isGuest) { 
				$login_user = \Yii::$app->user->identity;?>;
				var product_id=<?= $product->id; ?>;
				var user_id=<?= \Yii::$app->user->identity->id ?>;
				var store_id=<?= \Yii::$app->params['storeId']?>;
			<?php } ?>
			// /alert(product_id);
			$.ajax({
							type: "POST",
							url: "<?=Yii::$app->urlManager->createUrl(['wishlist-items/create'])?>",                
							data: "&product_id=" + product_id + "&user_id=" + user_id + "&store_id=" + store_id ,
							dataType: "html", 
							success: function (data) {
									//alert(data);
									
							}        
					});
		});*/	
	});	
</script> 
<script type="text/javascript">

function findvalues() {
	selected = [];
	selectedqty = [];
	var prices =  $(".price").text();
	var cur_price = prices.replace("AU$", "");
	var price = <?=$product->price?>;
	$('.bundle_checkbox:checked').each(function(){
		selected.push(this.value);
	});
	$('.bundle_radio:checked').each(function(){
			selected.push(this.value); 
	});
	if($('.bundle_dropdown').val() != ""){
		selected.push($('.bundle_dropdown').val());
		qty = $('.item_qty').val(); 
	}
	if($('.bundle_multisel').val() != null){
		for( var i = 0, xlength = $('.bundle_multisel').val().length; i < xlength; i++){
			selected.push($('.bundle_multisel').val()[i]);	
		}	
	}
	if($('.item_qty').val() != ""){ 
		id = $('.bundle_dropdown').val();
		qty = $('.item_qty').val(); 
		var object = {};
		object[id] = qty;
		selectedqty.push(object);
	}
	if($('.item_qtyradio').val() != ""){
		id = $('.bundle_radio').val();
		qty = $('.item_qtyradio').val(); 
		var object = {};
		object[id] = qty;
		selectedqty.push(object);
	}
	$.ajax({
		url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=products/test1',	
		type: 'POST',
		data: {selected: selected, price: price, cur_price: cur_price, selectedqty: selectedqty},
					 success: function(data){
								$(".price").html(data);
				}
	});
}

</script> 

<div id="fb-root"></div>
<script>
(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script> 

<script type="text/javascript">
    $(document).ready(function() {
        $('#review-form').submit(function(e) {
            data = $('#review-form').serialize();
            $.ajax({
                type: 'POST',
                url: '<?php echo ($userReview->isNewRecord) ? Yii::$app->urlManager->createUrl(["product-reviews/create"]) : Yii::$app->urlManager->createUrl(["product-reviews/update","id"=>$userReview->id])?>',
                data: data,
                success: function(){ 
                    $('.review_wrapper').html('Thanks for your review of <?=$product->name?>');
                }
            });
            return false;
        });
    }); 
</script>
