<?php 
    use frontend\components\Helper;
    use yii\helpers\Html;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();
?>


<div class="bst-row">
    <div class="bt-prodt">
        <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>"></a>             
    </div>
    <div class="bt-prodt-cnt">
        <a href="#" class="bpc-title">
           <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><?=Helper::stripText($model->name,25) ?></a>
        </a>
        <div class="bpc-price">
            <span><?=Helper::money($model->price)?></span>
            <div class="star-small">
                <input id="input-id" type="text" class="rating rating-star" value="<?=$model->rating?>">
            </div>
        </div>
    </div>
</div>   