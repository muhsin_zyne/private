<?php 
    use frontend\components\Helper;
    use yii\helpers\Html;
    use kartik\rating\StarRating;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();
?>



<div class="col-xs-3">
    <div class="product">
        <div class="pdt-img"> 
            <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax=0><img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>"></a> 
        </div>
        <div class="pdt-cnt">
            <div class="pdt-head"> <?=Helper::stripText($model->name,25) ?> </div>
            <div class="pr-left">
                <div class="price"><?=Helper::money($model->price)?></div>
                 <div class="starsrating">
                            <input id="input-id" type="text" class="rating rating-star" value="<?=$model->rating?>">
                </div>
            </div>
            <div class="pr-right">
              <?=Html::a('<i class="fa fa-eye"></i>', ['products/view', 'id' => $model->id],['class'=>'modal-class sm-dialog','data-title' =>'',]); ?> <a href="#" class="fa fa-heart"></a>
            </div>
        </div>
        <div class="pdt-btm"> 
            <a class="pdt-cart" href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>" data-pjax=0><i class=" fa fa-shopping-cart"></i> Add to cart</a> 
        </div>
    </div>
</div>
    