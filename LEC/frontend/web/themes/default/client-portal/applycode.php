<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="modal-dialog modal-lg quick-view-wrapper portal-wrapper" role="document">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="pop-title">Enter Your Client Code</div>
	<div class="left-pop col-md-9">
		<?php $form = ActiveForm::begin(['id'=>'client_form','enableAjaxValidation' => false,'enableClientValidation' => true, 'validateOnType' => true, 'validationDelay' => '0']); ?>
		<div class="pop-left col-md-6">
	    	<?=$form->field($clientPortal, 'code')->textInput(['placeholder'=>'Client Code'])->label('') ?>
	    </div>
	    <div class="form-group">
        <?= Html::submitButton('SUBMIT', ['class' => 'btn pdt-cart pop-submit']) ?>
    	</div>
    	<?php ActiveForm::end(); ?>
    	<!-- <div class="byod-desc col-md-9">
	    	Technology plays a huge role in student's lives. Bring Your Own Device (BYOD) is a technology model that allows students to bring their own devices to school/organisation.
	    </div>
	    <div class="cond-apply">* Conditions Apply</div> -->	
    </div>
</div>