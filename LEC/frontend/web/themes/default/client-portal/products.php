<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use common\models\Attributes;
use yii\widgets\Breadcrumbs;
use kartik\rating\StarRating;
use frontend\components\Helper;
?>

<?php 
    $this->title = "Client Portal";
    $this->params['breadcrumbs'][] = $this->title;

    //var_dump(Yii::$app->session);die();

?>

<div class="container">
  <div class="inner-container">
    <div class="row">
      <?php /*if($category->urlKey != "gift-voucher"){*/ ?>
      <div class="content_area_products">
        <div class="col-xs-3">
          <div class="filter-outer">
            <div class="block-title"> Refine Search </div>
            <div class="block-content" style="min-height:600px !important">
              <?php //var_dump($byod->filterAttributes);die();

                        ProductFilter::begin([
                            'basedModel' => $clientPortal,
                            'attributes' => $clientPortal->filterAttributes

                        ]);

                        ProductFilter::end();

                    ?>
            </div>
          </div>
        </div>
      </div>
      <?php //var_dump($storePromotion);die(); ?>
      <div class="col-xs-9">
        <div class="Featured Product_Featured listing ">

            <?php if($clientPortal->quote == 1){ ?>

            <div class="quote-text">QUOTE</div>
            <?php } ?>
            <?php if($clientPortal->shipment_type == "delivery-program") {  
                    
            ?>
            <div class="list-head">
              <?php if(!is_null($clientPortal->organisation_logopath)){
                      $logoPath = Yii::$app->params["rootUrl"].$clientPortal->organisation_logopath;
              ?>          
              <img src="<?=$logoPath?>" alt="<?=$clientPortal->organisation_name?>">
              <?php } ?> 
              <div class="pr-right-btns">
              <?=$clientPortal->organisation_name?><br/>
              <?=$clientPortal->address?><br/>
              <?=$clientPortal->city?><br/>
              <?=$clientPortal->postcode?><br/>
              </div>
            </div>  
            <?php }  

            elseif($clientPortal->shipment_type == "instore-pickup"){

            	if(!is_null($clientPortal->name) && !is_null($clientPortal->email)){
            ?>
             <div class="list-head" >
            <div class="pr-right-btns" style="float:left;padding:5px;">
              <b>Name</b> : <?=$clientPortal->name?><br/>
              <b>Email Id</b> : <?=$clientPortal->email?><br/>
            </div>
            </div>
            <?php }  }?>

            
             <!-- <div class="promotion-start"><span>Start Date:</span>20-09-2015</div>
			<div class="promotion-start"><span>End Date:</span>30-11-2015</div>  -->
              
          <div class="clear"></div>
          <div class="issu-promotion">
            <?php //$this->title?>
          </div>
          <div class="list-head">  <div class="byod-title"> <?=$this->title?></div>
            <div class="clientportal-start"><span>Valid From:</span>
                <?=Helper::date($clientPortal->validFrom)?>
              </div>
              <div class="clientportal-start"><span>Expires On:</span>
                <?=Helper::date($clientPortal->expiresOn)?>
              </div>
            <div class="toolbar">
              <div class="pager">
                <p class="amount">
                  <?=$products->totalCount?>
                  Item(s) </p>
                <div class="limiter"> 
                  <!--<label>Show</label>-->
                  <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12 Per Page', '36' => '36 Per Page', '90' => '90 Per Page'])?>
                </div>
              </div>
              <div class="sorter">
                <div class="sort-by">
                  <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['dateAdded' => 'Sort by New', 'price' => 'Sort by Price'])?>
                  <?php if(strpos(isset($_GET['sort'])? $_GET['sort'] : "", "-") !== FALSE){ ?>
                  <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"> <span class="custom_sort">ASC</span> </a>
                  <?php }else{ ?>
                  <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                  <?php } ?>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <?php if(Yii::$app->request->isAjax){ ?>
          <div class="row">
            <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
            <?php 

						echo ListView::widget([

				 		'id' => 'testclass',

				 		'dataProvider' => $products,

				 		'itemView' => '/products/_productlist', 
						'summary' => '',

                        'itemOptions' => ['class' => 'col-xs-4']
						

						] );

					?>
            <?php  \yii\widgets\Pjax::end();  ?>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
