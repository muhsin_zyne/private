<?php
use yii\helpers\Html;

use yii\widgets\DetailView;

use common\models\GalleryImages;
use yii\widgets\Breadcrumbs;
$this->title = 'Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $gallery_images = GalleryImages::find()->where(['galleryId' => $id])->orderBy('position')->all();?>

<div class="container">
    <div class="inner-container"> 
        <h2><?= strtoupper($model->title) ?></h2>
	    <div id="owl" class="owl-carousel owl-theme">           
            <?php foreach($gallery_images as $key => $gallery_name){?>
                <div class="item">
                    <img src ='<?=Yii::$app->params["rootUrl"]?>/store/gallery/images/<?=$gallery_name['title']?>' width="auto">
                    <div class="carousel-caption"> <p><h2><?=$gallery_name['description']?></h2></p></div> 
                </div>
            <?php   } ?>
        </div>
    </div>
</div>   

        


  

