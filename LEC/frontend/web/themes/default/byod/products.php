<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use common\models\Attributes;
use yii\widgets\Breadcrumbs;
use kartik\rating\StarRating;
use frontend\components\Helper;
?>

<?php 
    $this->title = "BYOD";
    $this->params['breadcrumbs'][] = $this->title;

    //var_dump(Yii::$app->session);die();

?>

<div class="container">
  <div class="inner-container">
    <div class="row">
      <?php /*if($category->urlKey != "gift-voucher"){*/ ?>
      <div class="content_area_products">
        <div class="col-xs-3">
          <div class="filter-outer">
            <div class="block-title"> Refine Search </div>
            <div class="block-content" style="min-height:600px !important">
              <?php //var_dump($byod->filterAttributes);die();

                        ProductFilter::begin([
                            'basedModel' => $byod,
                            'attributes' => $byod->filterAttributes

                        ]);

                        ProductFilter::end();

                    ?>
            </div>
          </div>
        </div>
      </div>
      <?php //var_dump($storePromotion);die(); ?>
      <div class="col-xs-9">
        <div class="Featured Product_Featured listing ">
            <?php //if($byod->shipment_type == "delivery-program") {  
                    
            ?>
            <div class="list-head">
              <?php if(!is_null($byod->organisation_logopath)){
                      $logoPath = Yii::$app->params["rootUrl"].$byod->organisation_logopath;
              ?>          
              <img src="<?=$logoPath?>" alt="<?=$byod->organisation_name?>">
              <?php } ?> 
              <div class="pr-right-btns">
              <?=$byod->organisation_name?><br/>
              <?=$byod->address?><br/>
              <?=$byod->city?><br/>
              <?=$byod->postcode?><br/>
              </div>
            </div>  
            <?php /*}  

            elseif($byod->shipment_type == "instore-pickup"){

            	if(!is_null($byod->name) && !is_null($byod->email)){*/
            ?>
             <!-- <div class="list-head" >
            <div class="pr-right-btns" style="float:left;padding:5px;">
              <b>Name</b> : <?=$byod->name?><br/>
              <b>Email Id</b> : <?=$byod->email?><br/>
            </div>
            </div> -->
            <?php //}  }?>

            
             <!-- <div class="promotion-start"><span>Start Date:</span>20-09-2015</div>
			<div class="promotion-start"><span>End Date:</span>30-11-2015</div>  -->
              
          <div class="clear"></div>
          <div class="issu-promotion">
            <?php //$this->title?>
          </div>
          <div class="list-head">  <div class="byod-title"> <?=$this->title?></div>
            <div class="byod-start"><span>Valid From:</span>
                <?=Helper::date($byod->validFrom)?>
              </div>
              <div class="byod-start"><span>Expires On:</span>
                <?=Helper::date($byod->expiresOn)?>
              </div>
            <div class="toolbar">
              <div class="pager">
                <p class="amount">
                  <?=$products->totalCount?>
                  Item(s) </p>
                <div class="limiter"> 
                  <!--<label>Show</label>-->
                  <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12 Per Page', '36' => '36 Per Page', '90' => '90 Per Page'])?>
                </div>
              </div>
              <div class="sorter">
                <div class="sort-by">
                  <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['dateAdded' => 'Sort by New', 'price' => 'Sort by Price'])?>
                  <?php if(strpos(isset($_GET['sort'])? $_GET['sort'] : "", "-") !== FALSE){ ?>
                  <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"> <span class="custom_sort">ASC</span> </a>
                  <?php }else{ ?>
                  <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                  <?php } ?>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <?php if(Yii::$app->request->isAjax){ ?>
          <div class="row">
            <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
            <?php 

						echo ListView::widget([

				 		'id' => 'testclass',

				 		'dataProvider' => $products,

				 		'itemView' => '/products/_productlist', 
						'summary' => '',

                        'itemOptions' => ['class' => 'col-xs-4']
						

						] );

					?>
            <?php  \yii\widgets\Pjax::end();  ?>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
