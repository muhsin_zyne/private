<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="modal-dialog modal-lg quick-view-wrapper byod-wrapper" role="document">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="pop-title">Enter Your BYOD Code</div>
	<div class="left-pop col-md-9">
		<?php $form = ActiveForm::begin(['id'=>'byod_forms','enableAjaxValidation' => false,'enableClientValidation' => true, 'validateOnType' => true, 'validationDelay' => '0']); ?>
		<div class="pop-left col-md-6">
	    	<?=$form->field($byod, 'code')->textInput(['placeholder'=>'BYOD Code'])->label('') ?>
	    </div>
	    <div class="form-group">
        <?= Html::submitButton('SUBMIT', ['class' => 'btn pdt-cart pop-submit']) ?>
    	</div>
    	<?php ActiveForm::end(); ?>
    	<div class="byod-desc col-md-8">
	    	Technology now plays a huge role in a student's life. Bring Your Own Device (BYOD) is a solution / service 
that allows the student to select an appropriate device to learn from and bring to school.
	    </div>
	    <div class="cond-apply">* Conditions Apply</div>	
    </div>
</div>