<?php



use yii\helpers\Html;

use yii\widgets\ListView;

use frontend\components\ProductFilter;

use common\models\Attributes;

use yii\widgets\Breadcrumbs;

use frontend\components\Helper;

?>
<?php 

	//$attribute_set = Attributes::getfilterAttributes($products->query);

	$this->title = $promotion->title;

    $this->params['breadcrumbs'][] = $this->title; 



    //die();

    //var_dump($promotion->catalog);die();

?>

<div class="container">
  <div class="inner-container">
    <div class="row">
      <?php /*if($category->urlKey != "gift-voucher"){*/ ?>
      <div class="content_area_products">
        <div class="col-xs-3">
          <div class="filter-outer">
            <div class="block-title"> Refine Search </div>
            <div class="block-content" style="min-height:600px !important">
              <?php

                        ProductFilter::begin([

                            'basedModel' => $promotion,

                            //'query' => clone $products->query,

                            'attributes' => $promotion->filterAttributes

                        ]);

                        ProductFilter::end();

                    ?>
            </div>
          </div>
        </div>
      </div>
      <?php //var_dump($storePromotion);die(); ?>
      <div class="col-xs-9">
        <div class="Featured Product_Featured listing ">
          <div class="list-head">
            <h2>
              <?=$promotion->title?>
            </h2>
            
            <!-- <div class="promotion-start"><span>Start Date:</span>20-09-2015</div>

                    <div class="promotion-start"><span>End Date:</span>30-11-2015</div> -->
            <div class="pr-right-btns">
              <div class="promotion-start"><span>Start Date:</span>
                <?=Helper::date($storePromotion->startDate)?>
              </div>
              <div class="promotion-start"><span>End Date:</span>
                <?=Helper::date($storePromotion->endDate)?>
              </div>
              <button class="hide-catalog btn btn-primary">Hide catalogue</button>
            </div>
          </div>
          <div class="clear"></div>
          <div class="issu-promotion">
            <?=$promotion->catalog?>
          </div>
          <div class="list-head">
            <div class="toolbar">
              <div class="pager">
                <p class="amount">
                  <?=$products->totalCount?>
                  Item(s) </p>
                <div class="limiter"> 
                  <!--<label>Show</label>-->
                  <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12 Per Page', '36' => '36 Per Page', '90' => '90 Per Page'])?>
                </div>
              </div>
              <div class="sorter">
                <div class="sort-by">
                  <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['dateAdded' => 'Sort by New', 'price' => 'Sort by Price'])?>
                  <?php if(strpos(isset($_GET['sort'])? $_GET['sort'] : "", "-") !== FALSE){ ?>
                  <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"> <span class="custom_sort">ASC</span> </a>
                  <?php }else{ ?>
                  <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                  <?php } ?>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <?php if(Yii::$app->request->isAjax){ ?>
          <div class="row">
            <?php \yii\widgets\Pjax::begin(['id' => 'category-products',]); ?>
            <?php 

						echo ListView::widget([

				 		'id' => 'testclass',

				 		'dataProvider' => $products,

				 		'itemView' => '/products/_productlist', 
						'summary' => '',

                        'itemOptions' => ['class' => 'col-xs-4']
						

						] );

					?>
            <?php  \yii\widgets\Pjax::end();  ?>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">

$(document).ready(function(){

    $('body').on('change', '.limiter select', function(){

        $('#filter-form .page-size').val($(this).val());

        filterproducts();

    });



    $('body').on('change', '.sort-by select', function(){

        $('#filter-form .sort').val($(this).val());

        filterproducts();

    });



    $('body').on('click', '.hide-catalog', function(){

        $('.issu-promotion').toggle();

        if($('.issu-promotion:visible').length == 0)

        {

            $('.hide-catalog').text('Show Catalogue');

        }

        else{

            $('.hide-catalog').text('Hide Catalogue');

        }

    });    

})

</script>