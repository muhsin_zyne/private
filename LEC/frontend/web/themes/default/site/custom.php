<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Json;
use kartik\widgets\ActiveForm; // or yii\widgets\ActiveForm
use kartik\widgets\FileInput;
use common\models\B2cComputersSignupRequests;
use common\models\B2cSignupRequests;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model common\models\B2cSignupRequests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">
    <h2>MY LEC STORE</h2>
    <h3>Submission Form</h3>
    <div class="b2c-signup-requests-form lej_form" >

        <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data'], 'id' => 'request-form' ]); 
         $session = Yii::$app->session; 
       // $model = new B2cComputersSignupRequests();
         $model = new B2cSignupRequests();
        //$model->phoneStore='123456789';
        $model->phoneStore=$session['b2c_requests_phoneStore'];
        $model->email=$session['b2c_requests_email'];
        $model->abn=$session['b2c_requests_abn'];
        echo $form->field($model, 'email')->hiddenInput()->label(false);
        echo $form->field($model, 'phoneStore')->hiddenInput()->label(false); 
        echo $form->field($model, 'abn')->hiddenInput()->label(false); ?>
        
        <?= $form->field($model, 'logo[]')->fileInput(['multiple'=>true]) ?>
        <?= $form->field($model, 'storeImage[]')->fileInput(['multiple'=>true]) ?>
        

        <!--<p>Customers may choose for the products to be shipped to their home.
         What is the maximum order value you want the customers to be able to get a home delivery?</p>
        <?= $form->field($model, 'orderValueMaximum')->textInput(array('placeholder' => 'Maximum order value')) ?>

        <p>Below are the three types of shipping fees you can charge your customers for an online order delivered to the customer's home.</p>
       <?= $form->field($model, 'shippingFee')->radioList(array('Free shipping (All orders are shipped for free)'=>'Free shipping (All orders are shipped for free)',
            'Flat $10 fee (All orders are charged $10 regardless of location / order value)'=>'Flat $10 fee (All orders are charged $10 regardless of location / order value)',
            '$10 fee for orders below $100 ($10 shipping fee for all orders below $100. Free shipping for orders above $100)'=>'$10 fee for orders below $100 ($10 shipping fee for all orders below $100. Free shipping for orders above $100)')); ?>
            -->
        <?= $form->field($model, 'websiteColour')->textInput(array('placeholder' => 'Enter preferred Colours')) ?>

        <?= $form->field($model, 'websiteAddress')->textInput(array('placeholder' => 'Enter web address / URL (if applicable)')) ?>
        <p>Please provide the social media URLs</p>

        <?= $form->field($model, 'facebookUrl')->textInput(array('placeholder' => 'Facebook URL')) ?>

        <?= $form->field($model, 'twitterUrl')->textInput(array('placeholder' => 'Twitter URL')) ?>

        <?= $form->field($model, 'youtubeUrl')->textInput(array('placeholder' => 'Youtube URL')) ?>

        <?= $form->field($model, 'pinterestUrl')->textInput(array('placeholder' => 'Pinterest URL')) ?>

        <?= $form->field($model, 'envision')->textarea(array('placeholder' => 'You may give us examples of sites that have impressed you...You may also wish to give us full freedom of design - engage our designers to build you something unique')) ?>

        <?= $form->field($model, 'captcha')->widget(Captcha::className())->hiddenInput()->label(false) ?>

        <?php $model->captcha_validate=1; ?>

       
    

        <div class="clear"></div>
        <!--<div class="form-group" style="width:100px; float:left;">
            <?= Html::submitButton( 'Submit' , ['class' => 'btn1 btn-success btn2' ]) ?>
        </div>-->
        <?php ActiveForm::end(); ?> 
    <form name="review"  ACTION="/computers/b2csignup/?id=standard" METHOD="POST" onsubmit="return checkform(this);">
            <font color="" size="2.5px">Captcha Code :   </font>  
            <font color="" size="5px"><span id="txtCaptchaDiv" style="background-color:#9C9C9C; width:100px; height:50px;" ></span></font><br/><br/>
            <input type="hidden" id="txtCaptcha" />
            <input type="text" name="txtInput" id="txtInput" size="15" /><br/><br/>
            <input type="submit" value="Submit"/>
        </form>
        <img src="http://orders.inte.com.au/images/loading.gif" id="loading-img" style="display:none"/ >
    </div>

</div>
<script type="text/javascript">
    function checkform(theform){
        var why = "";

        if(theform.txtInput.value == ""){
            why += "Captcha code should not be empty.\n";
        }
        if(theform.txtInput.value != ""){
            if(ValidCaptcha(theform.txtInput.value) == 1){
                why += "1";
            }
            else
            {
                  why += "Captcha code did not match.\n";
            }
        }

        if(why != "" && why == "1"){
            //alert(why);
            return false;
        }
        else
        {
            alert(why);
            return false;
        }

    }

    //Generates the captcha function
    var a = Math.ceil(Math.random() * 9)+ '';
    var b = Math.ceil(Math.random() * 9)+ '';
    var c = Math.ceil(Math.random() * 9)+ '';
    var d = Math.ceil(Math.random() * 9)+ '';
    var e = Math.ceil(Math.random() * 9)+ '';

    var code = a + b + c + d + e;
    document.getElementById("txtCaptcha").value = code;
    document.getElementById("txtCaptchaDiv").innerHTML = code;

    // Validate the Entered input aganist the generated security code function
    function ValidCaptcha(){
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2){
          $("#request-form").submit();
           //alert('hiiiiiiiii');
    $('#loading-img').show();
            return 1;
        }else{
            return false;
        }
    }

    // Remove the spaces from the entered and generated code
    function removeSpaces(string){
        return string.split(' ').join('');
    }
</script>

  