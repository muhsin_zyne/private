<?php

    use yii\widgets\ActiveForm;

    use yii\helpers\Url;

    use yii\helpers\Html;

    use yii\grid\GridView;

    use common\models\WishlistItems;

    use common\models\Products;

    use frontend\components\Helper;



    $this->title = 'My Account';

    $this->params['breadcrumbs'][] = $this->title;

?>

<?php //var_dump($error);die(); ?>

<div class="main_con cartpage">
<div class="container">
  <div class="inner-container"?>
    <div class="customcontentArea">
      <div class="row"> 
        
        <!-- tabs left -->
        
        <div class="tabbable tabs-left">
          <?=$this->render('/site/_sidemenu')?>
          <div class="tab-content col-xs-9"> 
            
            <!-- <div class="tab-pane" id="Information"> -->
            
            <div class="account-title">
              <h2>Edit Account Information</h2>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="fieldset">
              <?= $form->field($user, 'firstname')->textInput(['maxlength' => 45]) ?>
              <?= $form->field($user, 'lastname')->textInput(['maxlength' => 45]) ?>
              <?= $form->field($user, 'email')->textInput(['maxlength' => 45]) ?>
            </div>
            <div class="check-wrap">
              <input type="checkbox" class="checkbox change_password" name="change_password" <?=isset($error) ? ($error == "1") ? "checked" : "" : ""?> value="0">
              Change Password
            </div>
            <div class="fieldset passwords_change" style="display:none">
              
              <?php // Html::label('Current Password', 'Current Password', ['class' => 'control-label']) ?>
              <?php // Html::input('text', 'User[currentPassword]', '', ['class' =>'form-group form-control']) ?>
              <?php // Html::label('New Password', 'New Password', ['class' => 'control-label']) ?>
              <?php // Html::input('text', 'User[newPassword]', '', ['class' =>'form-group form-control']) ?>
              <?php // Html::label('Confirm New Password', 'Confirm New Password', ['class' => 'control-label']) ?>
              <?php // Html::input('text', 'User[confirmPassword]', '', ['class' =>'form-group form-control']) ?>

              <?= $form->field($changePassword, 'oldpass')->passwordInput(['maxlength' => 45]) ?>
              <?= $form->field($changePassword, 'newpass')->passwordInput(['maxlength' => 45]) ?>
              <?= $form->field($changePassword, 'repeatnewpass')->passwordInput(['maxlength' => 45]) ?>
              <?=Html::Input('hidden', 'User[applyChange]', '0', ['class' =>'form-group form-control apply_change']) ?>
            </div>
            
			<div class="info-btn-wrap"><?= Html::submitButton('Submit', ['class' => 'button btn btn-primary']) ?></div>
            <?php ActiveForm::end(); ?>
            
            <!--  </div> --> 
            
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
        $(".change_password").click( function(){
          if( $(this).is(':checked') ) {
            $('.passwords_change').css('display','block');
              $('.apply_change').val('1');
            }
            else{
              $('.passwords_change').css('display','none');
              $('.apply_change').val('0');
            }
        });

        if($(".change_password").is(":checked")){
            $('.passwords_change').css('display','block');
            $('.apply_change').val('1');
        }
    });

</script>