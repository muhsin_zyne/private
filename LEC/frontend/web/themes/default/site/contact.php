<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */



$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">


    <div class="contact_area">
    <h1><?= Html::encode($this->title) ?></h1>
        <div class="site-contact">
            <p>
                If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
            </p>
        
            <div class="row">
                <div class="col-xs-6">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                        <?= $form->field($model, 'name') ?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'subject') ?>
                        <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-xs-3">{image}</div><div class="col-xs-6">{input}</div></div>',
                        ]) ?>
                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-xs-6">
           <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13269.145179480522!2d151.146957!3d-33.753267!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12a8872d7c42d3%3A0xe5ad02a58157dbd5!2s3+Fitzsimons+Ln%2C+Gordon+NSW+2072%2C+Australia!5e0!3m2!1sen!2sin!4v1434517006372" width="470" height="450" frameborder="0" style="border:0"></iframe>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-6">
              		<div class="mapaddress"><span>Address:</span>
                    BJZ Jewellers<br>
                    Level 1, 3 Fitzsimmons Lane<br>
                    Gordon NSW 2072<br>
                    Sydney, Australia<br>
                    <br>
                    P: (02) 9497 4077<br>
                    E: info@bjz.com.au<br>
				</div>
              </div>
                <div class="col-xs-6">
          			<div class="trading_hours"><span>TRADING HOURS:</span>
                        MON 	9.00am &ndash; 5.30pm<br>
                        TUE 	9.00am &ndash; 5.30pm<br>
                        WED 	9.00am &ndash; 5.30pm<br>
                        THU 	9.00am &ndash; 5.30pm<br>
                        FRI 	9.00am &ndash; 5.30pm<br>
                        SAT 	9.00am &ndash; 2.00pm<br>
                        SUN 	Closed.		
					</div>
                </div>
            </div>
        
        </div>
    </div>
</div>
