<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="site-about">
        <h1><?= Html::encode($this->title) ?></h1>
    
        <p>BJZ Jewellers was established in June 2013 by Beau Joshua Zarb in Sydney NSW. Beau has an extensive background in Jewellery Design and has been trained formally as both a goldsmith and silversmith. Beau has a passion for unique pieces utilising some of the world's best gemstones. Today Beau focuses on custom creations utilising his formal hand skills as well as utilising Computer Aided Design (CAD) to produce items for clients otherwise unachievable by traditional jewellery manufacturing techniques.</p>
        <p>Beau is regularly all over the world searching for some of nature's most exquisite gemstones. Beau primarily travels to South East Asia and South America to source coloured gems for his clients & has a permanent office in Antwerp where he hand picks some of the world's finest diamonds for use in his masterpieces.</p>
        <p>To date Beau has received some of the world's leading Jewellery Design awards and currently has two pieces on show in the Australian Museum of Fine Art. Beau is also a fully trained GIA Diamond Grader / Gemmologist & Accredited Jewellery Valuer and regularly speaks at Diamond Council conferences in New York and Tel Aviv.</p>
        <div class="about_image">
            <div class="row">
                <div class="col-lg-4">
                    <img src="themes/site1/images/1.jpg">
                </div>
                <div class="col-lg-4">
                    <img src="themes/site1/images/2.jpg">
                </div>
                <div class="col-lg-4">
                    <img src="themes/site1/images/3.jpg">
                </div>
            </div>
        </div>
    
        
    </div>
</div>
