<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
    use common\models\WishlistItems;
    use common\models\Products;

    use frontend\components\Helper;

    $this->title = 'My Wishlist';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="inner-container"?>
        <div class="customcontentArea">
            <div class="row">
            <!-- tabs left -->
            <div class="tabbable tabs-left">
                
                <?=$this->render('/site/_sidemenu')?>

                <div class="tab-content col-xs-9">                   

                       <div class="account-title">
                          <h2>My Wishlist Items </h2>
                        </div>
                        
                        <?php $form = ActiveForm::begin(); ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
								'summary' =>'',
                                //'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    //'id',
                                
                                    [
                                        'label'=> 'PRODUCT',
                                        'format' => 'html',
                                        'value' => function($model){
                                            $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();
                                            return Html::img($thumbImagePath,['alt'=>'yii','class'=>'wish-img']).'<div class="wish-right"><span align="center">'.Html::a($model->product->name,['products/view','id' => $model->productId]).'</br><span class="price">'.Helper::money($model->product->price).'</b></div>';
                                        }
                                    ],
                                    //'productId',
                                    
                                    [
                                        'label' => 'COMMENT',
                                        'attribute' => 'id',
                                        'format' => 'raw',
                                        'value' => function ($model) { //var_dump(ArrayHelper::map($enabledProducts, 'id', 'productId'));die();
                                             return '<textarea name="WishlistItems['.$model->id.'][comments]" class="form-control" id="'.$model->id.'" > '.trim($model->comments).'</textarea>' ;
                                        },
                                          
                                    ],
                                    [
                                        'label'=> 'ADDED ON',
                                        'attribute' => 'id',
                                        'format' => 'html',
                                        'value' => function($model){
                                            return  Helper::date($model->createdDate, " F jS, Y ");
                                        }
                                    ],
                                    // [
                                    //     'label'=>'ADD TO CART',
                                    //     'format' => 'raw',
                                    //     'value'=>function ($model) {
                                    //     return Html::a('ADD TO CART',['wishlistitems/config','id' => $model->id],['class' => 'ajax-update btn']);
                                    //         //return ' <input type="button" class="btn btn-success cart" id="'.$model->productId.'" value="ADD TO CART">';
                                    //     },
                                    // ], 
                                    [
                                        'label'=>'Action',
                                        'format' => 'html', 
                                        'value' => function($model){
                                        return Html::a('<i class="fa fa-trash"></i>',['site/delete','id' => $model->id]);
                                        }
                                    ], 
                                    
                                    /*[
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{delete}',
                                    ],*/
                                ],
                            ]); ?>
                            <div style="float: right;" >
                                <?php if($dataProvider->count!=0) { ?>
                                    <input type="submit" class=" update-wishlist btn btn-primary" id="wli_update" value="Update">
                                <?php } ?>
                            </div>
                        <?php ActiveForm::end(); ?> 
                    <!-- </div> -->

                    </div>

                </div>
            <!--</div> -->
            <div class="clearfix"></div>
        </div>                                                                                                                                                     </div>                                                                                                         
    </div>
</div>
<script>
$(document).ready(function(){        
        $(".fa-trash").click(function(){
          var result = confirm("Are you sure you want to delete this item?");
if (result) {
    //Logic to delete the item
    //alert("This item has been successfully deleted");
        }
    else
    {
        return false;
    }
            
});
    });
</script>
