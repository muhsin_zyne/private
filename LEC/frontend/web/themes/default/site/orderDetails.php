<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
    use common\models\WishlistItems;
    use common\models\Products;

    use frontend\components\Helper;
     $this->title ='Payment Review';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="inner-container"?>
        <div class="customcontentArea">
            <div class="row">
                <div class="tabbable tabs-left">                
                <?=$this->render('/site/_sidemenu')?>
                    <div class="tab-content col-xs-9">
                        <div class="account-title"> 
                            <h2>Order #<?=$order->id?> - Payment Review</h2>
                        </div>
                        <div class="row" style="margin-bottom:20px">
                            <div class="col-xs-6">
                                <span style="font-weight:bold">Order Date: </span> <?= \backend\components\Helper::date($order->orderDate) ?>
                            </div>
                            <div class="col-xs-6">
                                <span style="font-weight:bold;">Order Status: </span> <?=$order->formattedStatus ?>
                            </div>
                        </div>
                                               
                        <div class="row">
                        <div class="col-xs-6">
                            <div class="billing-title" style="margin-bottom:10px;font-weight:bold">Collect From Store</div>
                            <?php if (isset($order->orderDeliveredAddress)) { ?>
                                <div class="billing-address">
                                    <?=$order->orderDeliveredAddress ?>
                                </div>
                            <?php } else {
                                echo "No Address Found..."; 
                            }?>
                        </div>
                        <div class="col-xs-6">
                            <div class="billing-title" style="margin-bottom:10px;font-weight:bold">Payment Information</div>
                            <div class="billing-address">
                               <?= $order->paymentInformation ?>
                            </div>
                            <div class="billing-address">
                                Payer Email: <?=$order->customer->email?>
                            </div>
                        </div>
                        </div>
                        <div class="clear"></div>

                        <div class="billing-title" style="margin-top:20px; margin-bottom:5px;font-weight:bold;">Items Ordered</div>

                        <?php $form = ActiveForm::begin(); ?>
                            <?= GridView::widget([
                                'dataProvider' => $orderItems,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'label'=> 'Product Name',
                                            'attribute'=>'product.name'
                                        ],
                                        [
                                            'label' => 'Option(s)',
                                            'value' => 'superAttributeValuesText',
                                            'format' => 'html'
                                        ],
                                        [
                                            'label'=> 'SKU',
                                            'attribute' => 'productId',
                                            'value' => 'product.sku'
                                        ],
                                        [
                                            'label'=> 'Item Price',
                                            'format' => 'html',
                                             'value' => function ($model) {
                                                return Helper::money($model->price);
                                            },
                                        ],
                                        [   
                                            'label'=> 'Quantity',
                                            'attribute' => 'quantity',
                                        ],
                                        [
                                           'label' => 'Item Total',
                                            'format' => 'html',
                                            'value' => function($model, $attribute){
                                                return Helper::money($model->price * $model->quantity);
                                            }
                                           
                                        ],
                                        
                                    ],
                                ]); 
                            ?>
                            
                            <?php ActiveForm::end(); ?> 

                            <div class="test-class" style="float:right">
                                <div class="order-subtotal" style="float:right">
                                    <span style="font-weight:bold"> Sub Total: </span> 
                                    <?=Helper::money($order->subTotal)?> 
                                </div> 
                                <div class="order-grandtotal" > 
                                    <span style="font-weight:bold;font-size: 17px;">Grand Total: </span>
                                    <?=Helper::money($order->grandTotal)?> 
                                </div>
                            </div>         
                           
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>                                                                                                       </div>                                                                                                         
    </div>
<!-- </div> -->