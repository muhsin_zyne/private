<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Json;
use kartik\widgets\ActiveForm; // or yii\widgets\ActiveForm
use kartik\widgets\FileInput;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model common\models\B2cSignupRequests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form">

				<?php if(Yii::$app->session->getFlash('error'))
						{ ?>
						<div class="alert alert-danger alert-dismissable b2c-alert">
								<i class="icon fa fa-ban"></i>
								<span style="color:red; font-size:14px;"><?= Yii::$app->session->getFlash('error'); ?></span>
						</div> 
				<?php } ?>
				<?php if(Yii::$app->session->getFlash('success'))
						{ ?>
						<div class="alert alert-success alert-dismissable b2c-alert">
								<i class="icon fa fa-check"></i>
								<span style="color:green; font-size:14px; align:center;"><?= Yii::$app->session->getFlash('success'); ?></span>
						</div> 
				<?php } ?>
		<h2>MY LEC STORE</h2>
		<h3>Submission Form</h3>
		
		<div class="b2c-signup-requests-form lej_form" >

				<?php $form = ActiveForm::begin(['action' => ['site/b2csignup'], 'id' => 'request-form']) ?>

				<?= $form->field($model, 'lejMemberNumber')->textInput(array('placeholder' => 'Enter LEC Member Number')) ?>

				<?= $form->field($model, 'contactPerson')->textInput(array('placeholder' => 'Enter The Name of Contact Person')) ?>

				<?= $form->field($model, 'email')->textInput(array('placeholder' => 'Enter Email Address for orders and other automated messages')) ?>

				<?= $form->field($model, 'additionalEmail')->textInput(array('placeholder' => 'If desired, enter Additional Email Address to receive orders and automated messages')) ?>

				<?= $form->field($model, 'phoneStore')->textInput(['maxlength'=>15,'placeholder' => 'Enter Store Phone number to appear on your website']) ?>

				<?= $form->field($model, 'mobile')->textInput(['maxlength'=>15,'placeholder' => 'Enter Mobile Number']) ?>

				<?= $form->field($model, 'storeTradingName')->textInput(array('placeholder' => 'Enter Store Trading Name')) ?>

				<?= $form->field($model, 'abn')->textInput(array('placeholder' => 'Enter ABN')) ?>

				<?= $form->field($model, 'talkbox')->textarea(array('placeholder' => 'Enter Talkbox Details')) ?>
				<p style=" font-size:18px;"><strong>Trading Hours</strong></p>
				
				<div class="row">
				<div class="form-group days-field">
						<label class="control-label">Monday</label>
						<input type="text" id="b2csignuprequests-monday" class="form-control" name="B2cSignupRequests[monday]" value="9.00am to 5.30pm">
				 </div>
				 <div class="form-group days-field">
						<label class="control-label">Tuesday</label>
						<input type="text" id="b2csignuprequests-tuesday" class="form-control" name="B2cSignupRequests[tuesday]" value="9.00am to 5.30pm">
				 </div>
				 <div class="form-group days-field">
						<label class="control-label">Wednesday</label>
						<input type="text" id="b2csignuprequests-wednesday" class="form-control" name="B2cSignupRequests[wednesday]" value="9.00am to 5.30pm">
				 </div>
				 <div class="form-group days-field">
						<label class="control-label">Thursday</label>
						<input type="text" id="b2csignuprequests-thursday" class="form-control" name="B2cSignupRequests[thursday]" value="9.00am to 5.30pm">
				 </div>
				 <div class="form-group days-field">
						<label class="control-label">Friday</label>
						<input type="text" id="b2csignuprequests-friday" class="form-control" name="B2cSignupRequests[friday]" value="9.00am to 5.30pm">
				 </div>
				 <div class="form-group days-field">
						<label class="control-label">Saturday</label>
						<input type="text" id="b2csignuprequests-saturday" class="form-control" name="B2cSignupRequests[saturday]" value="9.00am to 2.00pm">
				 </div>
				 <div class="form-group days-field">
						<label class="control-label">Sunday</label>
						<input type="text" id="b2csignuprequests-sunday" class="form-control" name="B2cSignupRequests[sunday]" value="Closed">
				 </div>
				 </div>
				 
						
						
						
						<p>  </p>
				
				<?= $form->field($model, 'primaryStoreAddress')->textarea(array('placeholder' => 'Enter Primary Store Address here')) ?>

				<?= $form->field($model, 'multipleStores')->textarea(array('placeholder' => 'Do you have other stores to link to this website? If yes, please enter the details here.(if the stores work in different timings, please mention this).')) ?>

				<?= $form->field($model, 'domainDetails')->textarea(array('placeholder' => 'Enter Domain Details. This will be your web address, domain manager, admin user ID and password for the domain, or the contact details for someone who manages your domain')) ?>

				<?= $form->field($model, 'aboutus')->textarea(array('placeholder' => 'Explain Briefly about your store. If you are happy with the About Us content on your existing website, cut and paste it here')) ?>

				<?php $model->captcha_validate=0; ?>

				<?= $form->field($model, 'captcha_validate')->hiddenInput()->label(false) ?>

			 	

				<p>You can either select from the three standard skins we provided or go for a custom designed website.
				 If you choose custom design, your website will be unique and we shall design it as per your specifications.</p>
				<a class="button" id="standard">
						<span><?= Html::button( 'I want to choose from the standard skins' , ['class' => 'btn1 btn-success','id'=>'standard' ]) ?></span>
				</a>
			 
				<p style="font-size:12px; font-style:italic; margin-top:5px;">
						Price to populate / design this option is inclusive with the offer 
						
				</p>
				<a class="button" style=" margin-top:15px;" id="custom">
						<span> <?= Html::button( 'I want my site to be unique' , ['class' => 'btn1 btn-success' ,'id'=>'custom' ]) ?></span>
				</a>
				
				<p style="font-size:12px; font-style:italic; margin-top:5px;">
						Once you submit the form we will give you an exact quote to both design and deliver
				</p>
				<div class="form-group" style="width:100px; float:left;">
					
					
				</div>
				<?php ActiveForm::end(); ?>      
		</div>
</div>
<script>
$(document).ready(function(){
		//alert('hooo');
		$("#standard").click(function(){
			//alert($(".typechange").attr("type","s")); 
			//alert('hii'); 
			 $("#request-form").attr("action", "/site/b2csignup?type=standard");
				$("#request-form").submit();
		});
		$("#custom").click(function(){
				$("#request-form").attr("action", "/site/b2csignup?type=custom");
				 $("#request-form").submit();
		});
});
</script>