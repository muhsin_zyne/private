<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */



$this->title = 'Login';

$this->params['breadcrumbs'][] = $this->title;

?>



<div class="container">

	<div class="inner-container login-view">

         <h1><?=strtoupper($this->title)?> </h1>

        <div class="site-login">
        

         <div class="row">

            <div class="col-lg-6">

            	<h2>New Customers</h2>

                <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>

                <a href="<?=\yii\helpers\Url::to(['site/signup']) ?>">

                <button class="button signup_button btn btn-primary" title="Create an Account" type="button"><span>Create an Account</span></button>

                </a>

            </div>

            <div class="col-lg-6">

            	<h2>Registered Customers</h2>

                <p>If you have an account with us, please log in.</p>

                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'rememberMe')->checkbox() ?>

                    <div style="color:#999;margin:1em 0">

                        If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.

                    </div>

                    <div class="form-group">

                        <?= Html::submitButton('Login', ['class' => 'btn btn-primary button-login', 'name' => 'login-button']) ?>

                    </div>

                <?php ActiveForm::end(); ?>

            </div>

            

        </div>

    </div>

    </div>

</div>

