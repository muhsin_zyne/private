<?php

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use yii\captcha\Captcha;

use yii\widgets\Breadcrumbs;


$this->title = 'Sign Up';

$this->params['breadcrumbs'][] = $this->title;

?>


<?php $prevUrl = Yii::$app->request->referrer; ?>

<div class="container">
    <div class="inner-container signup-view">
        <h1><?=strtoupper($this->title)?> </h1>
    	<div class="site-signup">

        <p>Please fill out the following fields to sign up:</p>

        <div class="row">

            <div class="col-lg-12">

                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                   <div class="row">
                   
                     <div class="col-xs-6">
                	<div class="fieldset">
                        <input type="hidden" name="SignupForm[storeId]" value="<?=Yii::$app->params['storeId']?>"/>
                        <h2 class="legend">Personal Information</h2>

						<?= $form->field($model, 'firstname') ?>

                        <?= $form->field($model, 'lastname') ?>

                        <div class="clear"></div>

                            <?= $form->field($model, 'email') ?>

                        <div class="clear"></div>

                    </div>
                    </div>
                    
                    <div class="col-xs-6">

                    <div class="fieldset">

                        <h2 class="legend">Login Information</h2>

    					          <?= $form->field($model, 'password')->passwordInput() ?>

                        <div class="form-group">

                        <?= Html::label('Confirm Password', 'confirm_password', ['class' => 'control-label']) ?>

                        <?= Html::input('password', 'SignupForm[confirm_password]', '', ['class' =>' form-control']) ?>                      
                           
                       

                        </div>
                        <div class="clear"></div>
                        <br/>
                       <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="label_width"><div class="row"><div class="col-xs-3">{image}</div><div class="col-xs-9">{input}</div></div></div>',
                        ]) ?>

                        <div class="clear"></div>

                    </div>
                    </div>
                    
                    </div>
                    
                    
                    
                    
                    
                    
                    

                    <div class="clear"></div>

                    <div class="fm-footer">

                      <a href="<?=$prevUrl?>"><i class="fa fa-angle-double-left"></i>Back</a>

                      <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>

                    </div>

                    

            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
</div>
