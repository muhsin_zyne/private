<div  class="col-xs-3">
<div class="my-account-menu-container">
    <div class="block-title">

        <strong><span>My Account</span></strong>

    </div>

    <ul class="nav m-account-menu">

        <li><a href="<?=\yii\helpers\Url::to(['site/dashboard']) ?>">Account Dashboard</a></li>

        <li><a href="<?=\yii\helpers\Url::to(['site/account']) ?>">Account Information</a></li>

        <li><a href="<?=\yii\helpers\Url::to(['site/addressbook']) ?>">Address Book</a></li>

        <li><a href="<?=\yii\helpers\Url::to(['site/myorders']) ?>">My Orders</a></li>

        <li><a href="<?=\yii\helpers\Url::to(['site/wishlist']) ?>" >My Wishlist</a></li>

    </ul>

</div>
</div>
