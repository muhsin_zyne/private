<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use kartik\rating\StarRating;

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = 'Search Results';
?>
<div class="container">
    <div class="inner-container search-inner">
            <div class="row">
              <div class="col-md-12">
                 <div class="page-title">
                   <h2 class="search-head">Search Results for -"<?=$_REQUEST['q']?>"</h2>
                 </div>
              </div>
                <?php \yii\widgets\Pjax::begin(['id' => 'category-products','options' => ['class' => 'product_list']]); ?>
                    <?php 
                        echo ListView::widget( [                      
                        'dataProvider' => $dataProvider,
                        'itemView' => '/products/_productlist',                       
                        'itemOptions' => ['class' => 'col-xs-3'],
                        'layout' => "{summary}\n{items}\n<div class='col-xs-12 text-center'>{pager}</div>",
                        'viewParams' => compact('options')
                        ] );
                        echo "<script> $('.pagination li').click(function(){ window.scrollTo(0, 0); });
                                $(document).on('pjax:complete', function() { $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');$('.rating-star').rating({disabled: true, showClear: false, showCaption: false});  })</script>";
                    ?>

                <?php  \yii\widgets\Pjax::end();  ?> 
            </div>     

        </div>
    </div>
</div>

<script type="text/javascript">
  /*$(window).load(function() {  
    $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});
  });  */
</script>

<script type="text/javascript">
  setTimeout(function(){ $('.rating-star').rating({disabled: true, showClear: false, showCaption: false}); }, 100);
</script>