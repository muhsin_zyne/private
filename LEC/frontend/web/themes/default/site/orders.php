<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
    use common\models\WishlistItems;
    use common\models\Products;

    use frontend\components\Helper;
    $this->title = 'My Orders';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <div class="inner-container"?>
        <div class="customcontentArea">
            <div class="row">
            <!-- tabs left -->
                <div class="tabbable tabs-left">                
                    <?=$this->render('/site/_sidemenu')?>
                    <div class="tab-content col-xs-9">
                        <!-- <div class="tab-pane" id="myorders">  -->
                        
                            <div class="account-title"> 
                                <h2>My Orders</h2>
                            </div>    
                            <?php $form = ActiveForm::begin(); ?>
                            <?= GridView::widget([
                                'dataProvider' => $orders,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'label'=> 'Order',
                                            'format' => 'html',
                                            'attribute' => 'id',
                                        ],
                                        [   
                                            'label'=> 'Date',
                                            'format' => ['date', 'php:d/m/Y'],
                                            'attribute' => 'orderDate',
                                        ],
                                   /*     [
                                            'label'=> 'Ship to',
                                            'value' => function($model){
                                               return isset($model->shippingAddressText) ?  $model->shippingAddressText : "";
                                            }
                                        ],*/
                                        [
                                            'label'=> 'Order Total',
                                            'attribute' => 'grandTotal',
					    'value' => function($model){ return \frontend\components\Helper::money($model->grandTotal); },
					    'format' => 'html'
                                        ],
                                        [
                                            'label'=> 'Status',
                                            'attribute' => 'status',
                                            'value' => function($model){ 
                                                return $model->formattedStatus; 
                                            }
                                        ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{orderdetails}',
                                            'buttons'=>[
                                              'orderdetails' => function ($url, $model) {     //var_dump($url);die(); 
                                                return Html::a('<span class="view_text">View Order</span>', $url, [
                                                        'title' => Yii::t('yii', 'Create'),
                                                ]);                                
                            
                                              }
                                            ]  
                                        ],
                                    ],
                                ]); 
                            ?>
                             <?php ActiveForm::end(); ?> 
                        <!-- </div> -->

                    </div>
                </div>
            <!--</div>-->
            <div class="clearfix"></div>
        </div>                                                                                                                                                     </div>                                                                                                         
    </div>
</div>
