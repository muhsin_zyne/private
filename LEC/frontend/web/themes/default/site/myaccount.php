<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
    use common\models\WishlistItems;
    use common\models\Products;
    use frontend\components\Helper;
    
    $this->title = 'My Dashboard';
    $this->params['breadcrumbs'][] = $this->title;

?>

<div class="container" id="address-block">
  <div class="inner-container"?>
    <div class="customcontentArea">
      <div class="row"> 
        
        <!-- tabs left -->
        
        <div class="tabbable tabs-left">
          <?=$this->render('/site/_sidemenu')?>
          <div class="tab-content col-xs-9">
            <div class="account-title">
              <h2>My Dashboard</h2>
            </div>
            <div class="welcome-msg">
              <p class="hello"><strong>Hello,
                <?=$user->firstname?>
                !</strong></p>
              <p>Here you can view the snapshot of your recent account activity and update your account information.</p>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <?= GridView::widget([

                                'dataProvider' => $latestOrders,

                                'columns' => [

                                    ['class' => 'yii\grid\SerialColumn'],

                                        [

                                            'label'=> 'Order',

                                            'format' => 'html',

                                            'attribute' => 'id',

                                        ],

                                        [   

                                            'label'=> 'Date',

                                            //'format' => 'dd/mm/yyyy',

                                            'format' => ['date', 'php:d/m/Y'],

                                            'attribute' => 'orderDate',

                                        ],

                                       /* [

                                            'label'=> 'Ship to',

                                            'value' => function($model){

                                               return isset($model->shippingAddress->streetAddress) ?  $model->shippingAddress->streetAddress : "";

                                            }

                                        ],*/

                                        [

                                            'label'=> 'Order Total',

                                            //'format' => 'html',

                                            'attribute' => 'grandTotal',
                                            'value' => function($model){ return \frontend\components\Helper::money($model->grandTotal); },
					    'format' => 'html'

                                        ],

                                        [

                                            'label'=> 'Status',

                                            //'format' => 'html',

                                            'attribute' => 'status',
                                            'value' => function($model){ 
                                                return $model->formattedStatus; 
                                            }

                                        ],

                                        [

                                            'class' => 'yii\grid\ActionColumn',

                                            'template' => '{orderdetails}',

                                            'buttons'=>[

                                              'orderdetails' => function ($url, $model) {     //var_dump($url);die(); 

                                                return Html::a('<span class="view_text">View Order</span>', $url, [

                                                        'title' => Yii::t('yii', 'Create'),

                                                ]);                                

                            

                                              }

                                            ]  

                                        ],

                                    ],

                                ]); 

                            ?>
            
            <!-- <div style="float: right;" >

                                <input type="button" class="btn-success" id="wli_update" value="Update">

                            </div> -->
            
            <?php ActiveForm::end(); ?>
            <div class="box-account box-info">
              <div class="box-head">
                <h2>Account Information</h2>
              </div>
            </div>
            <div class="clear"></div>
            <div class="col2-set">
              <div class="box box-big">
                <div class="step_head account current"> Contact Information <a href="<?=\yii\helpers\Url::to(['site/account']) ?>">Edit</a> </div>
                <div class="box-content">
                  <p>
                    <?=$user->firstname?>
                    <?=$user->lastname?>
                    <br>
                    <?=$user->email?>
                    <br>
                    <a href="<?=\yii\helpers\Url::to(['site/account']) ?>">Change Password</a> </p>
                </div>
              </div>
            </div>
            <div class="col2-set">
              <div class="box box-big">
                <div class="step_head account current"> Address Book <a href="<?=\yii\helpers\Url::to(['site/addressbook']) ?>">Manage Addresses</a> </div>
                <div class="box-content">
                  
                 <div class="row">
                  
                  <div class="col-xs-6 address">
                    <h4>Default Billing Address</h4>
                    <?php if(isset($defaultBillingAddress->streetAddress)) {  ?>
                    <address>
                    <?=$user->firstname?>
                    <?=$user->lastname?>
                    <br>
                    <?=$defaultBillingAddress->streetAddress ?>
                    <br/>
                    
                    <!--  <a href="http://www.legj.com.au/site1/customer/address/edit/id/188/">Edit Address</a> --> 
                    
                    <a href="<?=\yii\helpers\Url::to(['addresses/update','id'=>$defaultBillingAddress->id])?>" class="bootstrap-modal" data-width="650" data-update="address-block">Edit Address</a>
                    </address>
                    <?php } else { ?>
                    <a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>" class="bootstrap-modal" data-update="address-block" data-width="650"> Add address </a>
                    <?php } ?>
                  </div>
                  
                  
                  <div class="col-xs-6 address">
                    <h4>Default Shipping Address</h4>
                    <?php if(isset($defaultShippingAddress->streetAddress)) {  ?>
                    <address>
                    <?=$user->firstname?>
                    <?=$user->lastname?>
                    <br>
                    <?=$defaultShippingAddress->streetAddress?>
                    <br/>
                    
                    <!--  <a href="http://www.legj.com.au/site1/customer/address/edit/id/188/">Edit Address</a> --> 
                    
                    <a href="<?=\yii\helpers\Url::to(['addresses/update','id'=>$defaultShippingAddress->id]) ?>" class="bootstrap-modal" data-width="650" data-update="address-block">Edit Address</a>
                    </address>
                    <?php } else { ?>
                    <a href="<?=\yii\helpers\Url::to(['addresses/create']) ?>" class="bootstrap-modal" data-update="address-block" data-width="650"> Add address </a>
                    <?php } ?>
                  </div>
                  </div>
                  
                  <div class="clear"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
</div>
<script>

    $(document).ready(function(){

        $('#wli_update').on("click", function(){   // Wish list Items comments add

            var wlcomments=[];

            <?php  $login_user = \Yii::$app->user->identity;       

                $user_id= \Yii::$app->user->identity->id; 

                $store_id= $login_user->storeId ;

            ?>

            var user_id=<?= $user_id ?>;

            var store_id=<?= $store_id ?>;

            <?php $wlitem = WishlistItems::find()->where(['userId' =>$user_id,'storeId' =>$store_id])->all(); 

            foreach ($wlitem as $key => $value) {?>

                var id=<?= $value['id'] ?>;

                var wlc = document.getElementById(id).value;

                wlcomments.push(wlc);

            <?php } ?>

            //alert(user_id);

             $.ajax({

                type: "POST",                

                url: "<?=Yii::$app->urlManager->createUrl(['site/comments'])?>",                

                data: "&wlcomments=" + wlcomments  + "&user_id=" + user_id + "&store_id=" + store_id ,                   

                dataType: "html", 

                success: function (data) {

                   // alert(data); 

                           

                }

            });  

        });

        $('.cart').on("click", function(){  // add to cart

            var product_id=this.id;

            var qty=1;

            //alert(product_id);

             $.ajax({

                type: "POST",                

                url: "<?=Yii::$app->urlManager->createUrl(['cart/add'])?>",                

                data: "&Products[id]=" + product_id  + "&qty=" + qty,                   

                dataType: "html", 

                success: function (data) {

                    //alert(data); 

                    //$("#catageries_list").html(data);                   

                }

            });  

        }); 



        $('body').addClass('popup-absolute');   



    });



</script>