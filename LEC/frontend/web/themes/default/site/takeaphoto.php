<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;

use yii\web\UploadedFile;

use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = 'SEND A PHOTO';

?>
<script>

jQuery.noConflict();

</script>
<div class="container">
    <div class="inner-container">
        <div class="site-contact">
            <div class="site-contact"> 
                <h4>SEND A PHOTO</h4>   
                <p>Need help finding that special piece for that special someone... Simply send us a photo and a budget of what you have in mind!</p>            

                <?php
                $form = ActiveForm::begin(['id'=>'take_a_photo','options'=>['class'=>'form','enctype'=>'multipart/form-data']]);
                ?>
                
                <input type="hidden" id="amount1">
                <input type="hidden" id="amount2">
                <p>
                    <b>Price Range :</b> <span id="amount"></span>
                </p>

                <div id="slider-range"></div>
                <input type="hidden" name="range" id="range" value="" />

                <!--<label><b>Price Range</b></label><br />
                <span style="text-align: left; font-style:italic; font-size: 10px; color:#545454; width:100%; float:left;">Please drag the slider and set your budget-range</span>
                <div style=" padding: 10px 0; overflow:hidden; width:100% !important; position:relative;">
                    <input type="text" id="range_1" />
                   
                </div>-->
                <input type="hidden" name="storeId" value=<?=Yii::$app->params['storeId']?>"">
                <?= $form->field($model,'name')->label('Name');?>
                <?= $form->field($model,'email')->input('email')->label('Email');?>
                <?= $form->field($model,'phone')->label('Phone');?>
                <p style="font-size: 10px; align: center; font-style: italic;">Please fill the details above before uploading the photo</p>
                <div class="choose_file"><?= $form->field($model,'photo')->fileInput()->label('Upload  a Photo');?></div>
                <?= $form->field($model,'comment')->textarea(array('rows'=>5,'cols'=>5))->label('Comments');?>
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary button-login', 'name' => 'submit-button']) ?>
                <?php ActiveForm::end();?>   
            </div>
        </div>
    </div>   
</div> 
<script type="text/javascript">
    $=jQuery;
    $(document).ready(function() {       
        $("#range_1").ionRangeSlider({
            min: 500,
            max: 10000,
            from: 500,
            to: 10000,
            type: 'double',
            step: 100,
            prefix: "$",    
            hasGrid: false,
            onChange: function (obj) {  
               // alert( get(obj));
                get(obj);
            }
        });
    });
    function get(obj)
    {
        jQuery("#range").val("$"+parseInt(obj.fromNumber) + " and " + "$"+parseInt(obj.toNumber));
    }
</script>
<script type="text/javascript">  
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 1,
            max: 9999,
            values: [ 500, 6999 ],
            slide: function( event, ui ) {
                $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                $( "#amount1" ).val(ui.values[ 0 ]);
                $( "#amount2" ).val(ui.values[ 1 ]);
                $('#range').val("$AU" + $( "#slider-range" ).slider( "values", 0 ) +
                    " - $AU" + $( "#slider-range" ).slider( "values", 1 ));
            }
        });   
        $( "#amount" ).html( "$AU" + $( "#slider-range" ).slider( "values", 0 ) +
         " - $AU" + $( "#slider-range" ).slider( "values", 1 ) );
    }); 
</script>


  
