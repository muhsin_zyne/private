<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <!--<div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div> -->

<div class="contentmain">
<div class="content">
<div class="banner">
<div id="slider">
<a href="javascript:void(0)"><img src="images/banners/30_Ice-Concept.png"  width="1000" height="400"  alt="banner"  /></a><a href="javascript:void(0)"><img src="images/banners/PAN_Leading_Edge_Jewellers_websitebanner_1000x400_Symbolsim.png"  width="1000" height="400"  alt="banner"  /></a><a href="javascript:void(0)"><img src="images/banners/Main-Banner_7.jpg"  width="1000" height="400"  alt="banner"  /></a><a class="group fancybox.iframe" href="http://www.youtube.com/embed/promotions/?rel=0&autoplay=1"><img src="banners/TS-Charm-Club-1000-x-400.jpg" width="1000" height="400" alt=""  /></a><a href="javascript:void(0)"><img src="images/banners/Morgan_Watches_SS14_1000x400.png"  width="1000" height="400"  alt="banner"  /></a><a href="javascript:void(0)"><img src="images/banners/STEEL4.png"  width="1000" height="400"  alt="banner"  /></a>
</div><!--slider-->
</div>
<div class="miniBanners">
<span class="">
<a href=""><img src="images/banners/collections.jpg"  width="312" height="158"  alt="banner"  /></a>

</span>
<span class="spanMiddle">
<a href=""><img src="images/banners/promo.jpg"  width="312" height="158"  alt="banner"  /></a>

</span>
<span class="">
<a href="" target="_blank"><img src="images/banners/vip.jpg"  width="312" height="158"  alt="banner"  /></a>

</span>
<div class="clear"></div>

</div>

<div class="gift">
<div class="fb-like-box" data-href="https://www.facebook.com/LeadingEdgeJewellersHO" data-width="285" data-height="346" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="true" data-show-border="true"></div></div>

    
<div class="std"><div class="Featured Product_Featured">
    <ul>
        <li >
                                          
            <a href="" class="Xpreview" rel="images/ECUSA.png" title="9ct Gold Sapphire Dress Earring">
                <img src="images/ECUSA.png" width="200" height="175" alt="9ct Gold Sapphire Dress Earring" />
             </a>
                   
            <h3>

            <a title="9ct Gold Sapphire Dress Earring" href="">9ct Gold Sapphire Dress Earring </a></h3>
                  
            <div class="FeaturedPrice">
            <div class="price-box">
                            
                 <span class="regular-price" id="product-price-6795">
                    <span class="price">AU$1,399.00</span>                
                 </span>
                        
            </div>
            </div>
                    
            <div class="cartButton">
                <a href="">Buy now</a>
            </div>
  
            <li class="middle">
                                           
                <a href="" class="Xpreview" rel="images/5208857.png" title="STORY Gold Plated Skull Black CZ Button">   
                    
                    <img src="images/5208857.png" width="200" height="175" alt="STORY Gold Plated Skull Black CZ Button" /></a>
                
                <h3><a title="STORY Gold Plated Skull Black CZ Button" href="http://www.legj.com.au/site1/story-gold-plated-skull-black-cz-button.html">STORY Gold Plated Skull Black CZ Button </a></h3>
                  
            <div class="FeaturedPrice">
            <div class="price-box">
                <span class="regular-price" id="product-price-5541">
                    <span class="price">AU$108.00</span>                
                </span>
                        
            </div>
            </div>
                    

            <div class="cartButton">
                <a href="">Buy now</a>
            </div>
  
            <li >
                                            
                <a href="" class="Xpreview" rel="images/SR104Y-9_3.png" title="9ct Gold Plait Bead">   

                    <img src="images/SR104Y-9_3.png" width="200" height="175" alt="9ct Gold Plait Bead" /></a>
                    
                <h3>

                    <a title="9ct Gold Plait Bead" href="">9ct Gold Plait Bead </a>

                </h3>
                  
                    <div class="FeaturedPrice">
                    
                    <div class="price-box">
                         <span class="regular-price" id="product-price-7644">
                            
                            <span class="price">AU$262.00</span>                

                        </span>
                        
                    </div>

            </div>
                   
                <div class="cartButton">
                
                    <a href="">Buy now</a>
                
                </div>
  
            </ul>
            
            </div>
            
            <div class="clear"></div></div>

<div class="clear"></div>
</div>
</div>


</div>
