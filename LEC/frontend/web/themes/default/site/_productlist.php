
<?php 
    use frontend\components\Helper;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();
?>

    <?php $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$model->getThumbnailImage(); ?>
    <div class="product">
        <div class="pdt-img"> 
            <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>"></a> 
    <a class="Xpreview <?php echo ($model->isCustomProduct()) ? 'ribbon' : 'no-ribbon' ?>" title="<?=$model->name ?>" href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html".(isset($options[$model->id])? '?'.http_build_query($options[$model->id]) : ''))?>">
        <img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>">
    </a> 
    
    <h3>
    <a title="<?=$model->name ?>" href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html".(isset($options[$model->id])? '?'.http_build_query($options[$model->id]) : ''))?>"><?=Helper::stripText($model->name) ?></a>
    </h3>

    <div class="FeaturedPrice">
        <div class="price-box">
            <span id="" class="regular-price">
                    <span class="price">AU$ <?=$model->price?></span>                
            </span>
        </div>
        <div class="pdt-cnt">
            <div class="pdt-head"> <?=Helper::stripText($model->name,25) ?> </div>
            <div class="pr-left">
                <div class="price"><?=Helper::money($model->price)?></div>
                <div class="stars five"></div>
            </div>
            <div class="pr-right">
              <a href="#" class="fa fa-eye"></a> <a href="#" class="fa fa-heart"></a>
            </div>
        </div>
        <div class="pdt-btm"> 
            <a class="pdt-cart" href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><i class=" fa fa-shopping-cart"></i> Add to cart</a> 
        </div>
    </div>

