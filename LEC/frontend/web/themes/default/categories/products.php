<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\components\ProductFilter;
use common\models\Attributes;
use yii\widgets\Breadcrumbs;
use kartik\rating\StarRating;
use common\models\Categories;
?>
<?php 
    if(isset($category->parentCategory)){
        $parentcat=Categories::find()->where(['id' => $category->parentCategory->parent])->one(); 
    } 
    $this->title = $category->title; 
?>
<div class="cart-heading">
    <div class="container"> 
        <div class="pull-right"> 
            <?= Breadcrumbs::widget([  
                'tag' => 'ol', 
                'links' => [ 
                    isset($parentcat) ? ($parentcat->title!='Root') ? ['label' => $parentcat->title, 'url' => $parentcat->urlKey.'.html','template' => "<li>{link}</li>\n",]: ['label' =>'','template' => '']: ['label' =>'','template' => ''],
                    isset($category->parentCategory->title) ? ($category->parentCategory->title!='Root') ? ['label' => $category->parentCategory->title, 'url' => $category->parentCategory->urlKey.'.html','template' => "<li>{link}</li>\n",]: ['label' =>'','template' => '']: ['label' =>'','template' => ''],
                    $category->title,
                ],
            ])?>
        </div>
    </div>
</div>
<div class="container">
    <div class="inner-container">
        <div class="row">
            <?php if($category->urlKey != "gift-voucher"){ ?>
            <div class="content_area_products"> 
                <div class="col-xs-3">
                    <div class="filter-outer">
                        <div class="block-title">
                           Refine Search
                        </div>
                		<div class="block-content">
                            <?php
                            if(!Yii::$app->request->isAjax){
                                ProductFilter::begin([
                                    'basedModel' => $category,
                                    'query' => clone $products->query,
                                    'attributes' => $category->filterAttributes, //[['code' => 'material', 'id' => 15, 'Title' => 'material']],
                                ]);
                                ProductFilter::end();
                            }
                            ?>
                		</div>
                    </div>
                </div>
                <div class="col-xs-9">
                    <div class="list-head">                 
                        <h2><?=$category->title?></h2>
                        <div class="toolbar">
                            <div class="pager">
                                <p class="amount">
                                  <?=$products->totalCount?> Item(s)
                                </p>
                                <div class="limiter">
                                    <!--<label>Show</label>-->
                                    <?=\yii\helpers\Html::dropDownList("", isset($_GET['per-page'])? $_GET['per-page'] : "", ['12' => '12 Per Page', '36' => '36 Per Page', '90' => '90 Per Page'])?>
                                </div>
                            </div>
                            <div class="sorter">
                                <div class="sort-by"> 
                                    <?=\yii\helpers\Html::dropDownList("", isset($_GET['sort'])? str_replace("-","",$_GET['sort']) : "", ['price' => 'Sort by Price', 'dateAdded' => 'Sort by New'])?>
                                    <?php if(strpos(isset($_GET['sort'])? $_GET['sort'] : "", "-") !== FALSE){ ?>
                                        <a class="sort-direction asc" title="Set Ascending Direction" href="javascript:;"><img class="v-middle" alt="Set Ascending Direction" src="/images/i_asc_arrow.gif"><span class="custom_sort">ASC</span> </a>
                                    <?php }else{ ?>
                                        <a class="sort-direction desc" title="Set Descending Direction" href="javascript:;"><img class="v-middle" alt="Set Descending Direction" src="/images/i_desc_arrow.gif"><span class="custom_sort">DESC</span> </a>
                                    <?php } ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>      
               	    <div class="Featured Product_Featured listing">
                        <div class="row">
                            <?php \yii\widgets\Pjax::begin(['id' => 'category-products','options' => ['class' => 'product_list']]); ?>
                            <?php if(Yii::$app->request->isAjax){ //die(); ?>
                            <?php 
                               /* echo ListView::widget([
                                'id' => 'category-products',
                                'dataProvider' => $products,
                                'itemView' => '/categories/_product',
                                'summary' => '',
                                'itemOptions' => ['class' => 'col-xs-3'], 
                                ] );*/
                                 echo ListView::widget([
                                        'id' => 'homepage-products',
                                        'summary'=>'', 
                                        'dataProvider' => $products,
                                        'itemView' => '/products/_productlist', 
                                        'itemOptions' => ['class' => 'col-xs-4']
                                    ]); 

                                echo "<script> $('.pagination li').click(function(){ window.scrollTo(0, 0); });
                                $(document).on('pjax:complete', function() { $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');$('.rating-star').rating({disabled: true, showClear: false, showCaption: false});  })</script>";
        					?>
                            <?php } ?>
                            <?php  \yii\widgets\Pjax::end(); ?>
                            <?php 
                                if(!Yii::$app->request->isAjax){
                                ?> 
                                <div style="text-align: center;">
                                    <img src="/themes/default/images/sm-loader.gif"/>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if($this->renderCategoryBlock($category->id)!='') {?>
                        <div class="welcome-bg">
                            <div class="readmore scrollbar-macosx">
                                <div class="description-content box after" id="dot5">                                           
                                    <?=$this->renderCategoryBlock($category->id); ?>
                                </div>
                            </div>
                        </div>
                    <?php  } ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>





<script src="/js/jquery.scrollbar.js"></script>


<script type="text/javascript">

$(document).ready(function(){

    $('body').on('change', '.limiter select', function(){

        $('#filter-form .page-size').val($(this).val());

        filterproducts();

    });

    $('body').on('change', '.sort-by select', function(){

        $('#filter-form .sort').val($(this).val());

        filterproducts();

    })
    if($('.list-view').length >0){

        $('.list-view').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>')

    }
    $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');

    $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});

});
</script>

<script>
 	jQuery(document).ready(function(){
		jQuery('.readmore.scrollbar-macosx').scrollbar();
	});
 </script>

<script type="text/javascript" language="javascript" src="/js/jquery.dotdotdot.js"></script>
<script type="text/javascript" language="javascript">
	$(function() {

		var $dot5 = $('#dot5');
		$dot5.append( '<a class="toggle" href="#"><span class="open">Read more »</span><span class="close">Read less »</span></a>' );


		function createDots()
		{
			$dot5.dotdotdot({
				after: 'a.toggle'
			});
		}
		function destroyDots() {
			$dot5.trigger( 'destroy' );
		}
		createDots();

		$dot5.on(
			'click',
			'a.toggle',
			function() {
				$dot5.toggleClass( 'opened' );

				if ( $dot5.hasClass( 'opened' ) ) {
					destroyDots();
				} else {
					createDots();
				}
				return false;
			}
		);

		$('#dot6 .pathname').each(function() {
			var path = $(this).html().split( '/' );
			if ( path.length > 1 ) {
				var name = path.pop();
				$(this).html( path.join( '/' ) + '<span class="filename">/' + name + '</span>' );
				$(this).dotdotdot({
					after: '.filename',
					wrap: 'letter'
				});						
			}
		});

	});
</script>
