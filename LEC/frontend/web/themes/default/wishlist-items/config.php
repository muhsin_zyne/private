<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Products;

/* @var $this yii\web\View */
/* @var $model common\models\WishlistItems */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="main_con cartpage">
   
        
			<div class="wishlist-items-form">
			<h3>Set Product Details</h3>
    			<?php $form = ActiveForm::begin(['action' =>  Url::to(['cart/add'])]); 
                $product = Products::findOne($model->productId);
                //$product->qty=1;
                echo $form->field($product, 'id')->hiddenInput()->label(false); 
                //echo $form->field($product, 'qty')->hiddenInput()->label(false); 
                if($product->typeId=="configurable"){
                        echo $product->renderConfigurableFields($form);
                }
                elseif($product->typeId=="bundle"){
                    echo $product->renderBundleFields($form);
                } 
                ?>
                <input type="hidden" class="" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                <div class="form-group">
                    <?= Html::submitButton('Set', ['class' => 'btn btn-success']) ?>
                </div>

    			<?php ActiveForm::end(); ?>
    		</div>
    	
    
</div>
