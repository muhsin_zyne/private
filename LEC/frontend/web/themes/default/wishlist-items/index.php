<?php 
    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\helpers\url;
    use yii\widgets\ActiveForm;
    use frontend\components\Helper;
    use common\models\WishlistItems;
    use common\models\Products;
    
?>
<div class="main_con cartpage">
    <div class="container">
        <div class="row">
            <?php
            $this->title = 'Wishlist Items';
            $this->params['breadcrumbs'][] = $this->title;
            ?>
            <div id="parentVerticalTab">
                <div class="col-md-4">
                    <div class="block-title">My Account</div>
                    <div class="block-content">
                        <ul class="resp-tabs-list hor_1">
                            <li>Account Dashboard</li>
                            <li>Account Information</li>
                            <li>Address Book</li>
                            <li>My Orders</li>
                            <li>My Product Reviews</li>
                            <li>My Tags</li>
                            <li>My Wishlist</li>
                            <li>Check Gift Voucher Balance</li>
                        </ul>                       
                    </div>
                </div>

                <?php $form = ActiveForm::begin(); ?>
                <div class="col-md-8">
                <h1><?= Html::encode($this->title) ?></h1>
                
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    
                    [
                        'label'=> 'PRODUCT',
                        'format' => 'html',
                        'value' => function($model){
                            $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();
                            return Html::img($thumbImagePath,['alt'=>'yii']).'<br/><span align="center">'.Html::a($model->product->name,['products/view','id' => $model->productId]).'<span><br/><b><font size="4">'.Helper::money($model->product->price).'</font></b>';
                        }
                    ],
                    //'productId',
                    
                    [
                        'label' => 'COMMENT',
                        'attribute' => 'id',
                        'format' => 'raw',
                        'value' => function ($model) { //var_dump(ArrayHelper::map($enabledProducts, 'id', 'productId'));die();
                             return ' <textarea id="'.$model->id.'" rows="8" cols="20"> '.$model->comments.'</textarea>' ;
                        },
                          
                    ],
                    [
                        'label'=> 'ADDED ON',
                        'attribute' => 'id',
                        'format' => 'html',
                        'value' => function($model){
                            return  Helper::date($model->createdDate, " F jS, Y ");
                        }
                    ],
                    // [
                    //     'label'=>'ADD TO CART',
                    //     'format' => 'raw',
                    //     'value'=>function ($model) {
                    //     return Html::a('ADD TO CART',['wishlistitems/config','id' => $model->id],['class' => 'ajax-update']);
                    //         //return ' <input type="button" class="btn-success cart" id="'.$model->productId.'" value="ADD TO CART">';
                    //     },
                    // ], 
                    [
                        'label'=>'ADD TO CART',
                        'format' => 'raw',
                        'value' => 'cartAdd'
                    ], 
                    
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                    ],
                
                ],
                ]); ?>
               

                </div> 
            </div>
            <div style="float: right;" >
              
                <input type="button" class="btn-success" id="wli_update" value="Update">
            </div>
              <?php ActiveForm::end(); ?>                                                                                                                                                                                                                    
        </div>
    </div>
</div><!--main_con-->



<script>
    $(document).ready(function(){
        $('#wli_update').on("click", function(){   // Wish list Items comments add
            var wlcomments=[];
            <?php  $login_user = \Yii::$app->user->identity;       
                $user_id= \Yii::$app->user->identity->id; 
                $store_id= $login_user->storeId ;
            ?>
            var user_id=<?= $user_id ?>;
            var store_id=<?= $store_id ?>;
            <?php $wlitem = WishlistItems::find()->where(['userId' =>$user_id,'storeId' =>$store_id])->all(); 
            foreach ($wlitem as $key => $value) {?>
                var id=<?= $value['id'] ?>;
                var wlc = document.getElementById(id).value;
                wlcomments.push(wlc);
            <?php } ?>
            //alert(user_id);
             $.ajax({
                type: "POST",                
                url: "<?=Yii::$app->urlManager->createUrl(['wishlist-items/comments'])?>",                
                data: "&wlcomments=" + wlcomments  + "&user_id=" + user_id + "&store_id=" + store_id ,                   
                dataType: "html", 
                success: function (data) {
                   // alert(data); 
                           
                }
            });  
        });
        $('.cart').on("click", function(){  // add to cart
            var product_id=this.id;
            var qty=1;
            //alert(product_id);
             $.ajax({
                type: "POST",                
                url: "<?=Yii::$app->urlManager->createUrl(['cart/add'])?>",                
                data: "&Products[id]=" + product_id  + "&qty=" + qty,                   
                dataType: "html", 
                success: function (data) {
                    //alert(data); 
                    //$("#catageries_list").html(data);                   
                }
            });  
        });        
    });

</script>