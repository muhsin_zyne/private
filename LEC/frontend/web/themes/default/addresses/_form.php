<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserAddresses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-dialog modal-lg quick-view-wrapper" role="document">

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

<div class="user-addresses-form">

<div class="my-account">
   
    <?php $form = ActiveForm::begin(['id'=>'address_form','enableAjaxValidation' => false,'enableClientValidation' => true, 'validateOnType' => true, 'validationDelay' => '0']); ?>
    <div class="fieldset">
        <h2 class="legend sm-head">Contact Information</h2>
        <ul class="form-list">
            <li class="fields">
                <div class="customer-name row">
                    <div class="field col-xs-6 ">
                        <div class="input-box"> <?= $form->field($model, 'firstName')->textInput(['value' => (Yii::$app->controller->action->id != "update")? $user->firstname : $model->firstName]) ?> </div>
                    </div>
                    <div class="field col-xs-6">
                        <div class="input-box"> <?= $form->field($model, 'lastName')->textInput(['value' => (Yii::$app->controller->action->id != "update")? $user->lastname : $model->lastName]) ?> </div>
                    </div>    
                </div>
            </li>
            <li class="wide"> 
                <div class="input-box"> 
                <?= $form->field($model, 'company')->textInput(['maxlength' => 255]) ?> 
                </div> 
            </li>
            
            <li class="fields row">
              <div class="field col-xs-6"> 
                <div class="input-box"> 
                  <?= $form->field($model, 'telephone')->textInput(['maxlength' => 255]) ?> 
                </div>
              </div>
              <div class="field col-xs-6 ">
                <div class="input-box"> 
                    <?= $form->field($model, 'fax')->textInput(['maxlength' => 255]) ?> 
                </div>
              </div>
            </li>
        </ul>    

        <?= $form->field($model, 'streetAddress')->textarea(['rows' => 1]) ?>
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>
            </div>        
            <div class="col-xs-6">
                <?=$form->field($model, 'state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])?>
            </div>
        </div>
        <div class="row">
		  <div class="col-xs-6">
		    <?= $form->field($model, 'zip')->textInput(['maxlength' => 255]) ?>
          </div>
          <div class="col-xs-6">
            <?= $form->field($model, 'country')->dropDownList(['Australia'=>'Australia']) ?>
          </div>
        </div>
        
        <?=$form->field($model, 'defaultBilling')->checkBox() ?>  
        <?=$form->field($model, 'defaultShipping')->checkBox() ?>     
    
    </div>

    

 <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
