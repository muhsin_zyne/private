<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserAddresses */

$this->title = 'Update User Addresses: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-addresses-update">

<?php //var_dump('hai');die(); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('/addresses/_form',compact('model','user')) ?>

</div>
