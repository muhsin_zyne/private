<?php $this->beginPage(); ?>
<?php //$this->beginBody() ?>
<html class="no-js" lang="en"><!--<![endif]-->
    <head>
        <meta name="viewport" content="width=device-width,user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <title>Leading Edge Computers - Signup Form</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->
        <link href="/css/styleb2crequest.css" type="text/css" rel="stylesheet">

        <script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
        
        
    </head>
    <body>
        <div class="container">
            <h1 style="background: white none repeat scroll 0 0;border-bottom: 1px solid #d2d2d2;margin: 0;padding: 20px 0;text-align: center;">
            <a href="#"><img src="/images/b2csignup/legc.png" alt="leading edge Computers"></a></h1>
            
            <?= $content ?>
            <div class="footer">
               © <?= date('Y') ?> Xtrastaff. All rights reserved.
               <a target="_blank" href="http://xtrastaff.com.au"><img src="/images/b2csignup/footer_logo_xtrastaff.png" alt="leading edge Computers"></a>
            </div>
        </div>
    </body>
</html>
<?php 
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody() 
?>
<?php $this->endPage() ?>

