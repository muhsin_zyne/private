<?php  

use yii\helpers\Html;

use yii\bootstrap\Nav;

use yii\bootstrap\NavBar;

use yii\widgets\Breadcrumbs;

use frontend\assets\AppAsset;

use frontend\widgets\Alert;

use frontend\components\FrontNavBar;

use frontend\components\MainMenu;

use app\assets\InteAsset;

/* @var $this \yii\web\View */

/* @var $content string */



AppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>

    <meta charset="<?= Yii::$app->charset ?>"/>

    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->

    <meta name="description" content="BJZ Jewellers" />

    <meta name="keywords" content="BJZ Jewellers" />

    <meta name="robots" content="INDEX,FOLLOW" />

    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >

    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



    <?= Html::csrfMetaTags() ?>

    <title>Home</title>

    <?php $this->head() ?>



    <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(n,e,t){function r(t){if(!e[t]){var o=e[t]={exports:{}};n[t][0].call(o.exports,function(e){var o=n[t][1][e];return r(o?o:e)},o,o.exports)}return e[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({QJf3ax:[function(n,e){function t(n){function e(e,t,a){n&&n(e,t,a),a||(a={});for(var u=c(e),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,t);return s}function a(n,e){f[n]=c(n).concat(e)}function c(n){return f[n]||[]}function u(){return t(e)}var f={};return{on:a,emit:e,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=n("gos");e.exports=t()},{gos:"7eSDFh"}],ee:[function(n,e){e.exports=n("QJf3ax")},{}],3:[function(n,e){function t(n){return function(){r(n,[(new Date).getTime()].concat(i(arguments)))}}var r=n("handle"),o=n(1),i=n(2);"undefined"==typeof window.newrelic&&(newrelic=window.NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit","noticeError"];o(a,function(n,e){window.NREUM[e]=t("api-"+e)}),e.exports=window.NREUM},{1:12,2:13,handle:"D5DuLP"}],gos:[function(n,e){e.exports=n("7eSDFh")},{}],"7eSDFh":[function(n,e){function t(n,e,t){if(r.call(n,e))return n[e];var o=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(n,e,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return n[e]=o,o}var r=Object.prototype.hasOwnProperty;e.exports=t},{}],D5DuLP:[function(n,e){function t(n,e,t){return r.listeners(n).length?r.emit(n,e,t):(o[n]||(o[n]=[]),void o[n].push(e))}var r=n("ee").create(),o={};e.exports=t,t.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(n,e){e.exports=n("D5DuLP")},{}],XL7HBI:[function(n,e){function t(n){var e=typeof n;return!n||"object"!==e&&"function"!==e?-1:n===window?0:i(n,o,function(){return r++})}var r=1,o="nr@id",i=n("gos");e.exports=t},{gos:"7eSDFh"}],id:[function(n,e){e.exports=n("XL7HBI")},{}],loader:[function(n,e){e.exports=n("G9z0Bl")},{}],G9z0Bl:[function(n,e){function t(){var n=h.info=NREUM.info;if(n&&n.licenseKey&&n.applicationID&&f&&f.body){c(l,function(e,t){e in n||(n[e]=t)}),h.proto="https"===d.split(":")[0]||n.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var e=f.createElement("script");e.src=h.proto+n.agent,f.body.appendChild(e)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=n("handle"),c=n(1),u=(n(2),window),f=u.document,s="addEventListener",p="attachEvent",d=(""+location).split("?")[0],l={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-593.min.js"},h=e.exports={offset:i(),origin:d,features:{}};f[s]?(f[s]("DOMContentLoaded",o,!1),u[s]("load",t,!1)):(f[p]("onreadystatechange",r),u[p]("onload",t)),a("mark",["firstbyte",i()])},{1:12,2:3,handle:"D5DuLP"}],12:[function(n,e){function t(n,e){var t=[],o="",i=0;for(o in n)r.call(n,o)&&(t[i]=e(o,n[o]),i+=1);return t}var r=Object.prototype.hasOwnProperty;e.exports=t},{}],13:[function(n,e){function t(n,e,t){e||(e=0),"undefined"==typeof t&&(t=n?n.length:0);for(var r=-1,o=t-e||0,i=Array(0>o?0:o);++r<o;)i[r]=n[e+r];return i}e.exports=t},{}]},{},["G9z0Bl"]);</script>



    <link rel="icon" href="http://www.legj.com.au/media/favicon/websites/3/fav_1.png" type="image/x-icon" />

    <link rel="shortcut icon" href="http://www.legj.com.au/media/favicon/websites/3/fav_1.png" type="image/x-icon" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/easy-responsive-tabs.css " />

   <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 

   <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->

   

    <script src="js/jquery-1.9.1.min.js"></script>

     <script src="js/bootstrap.min.js"></script>

    

    <script src="js/ajax-update.js"></script>

    <script src="js/jquery-ui.js"></script>



    <link rel="stylesheet" type="text/css" href="themes/default/css/widgets.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/styles.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/style.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/custom.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/giftvoucher.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/jquery-ui-1.8.21.custom.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes/default/css/loader.css" media="all" />

    <link rel="stylesheet" type="text/css" href="themes22/default/css/print.css" media="print" />

    

    <link rel="stylesheet" type="text/css" href="themes/default/css/owl.carousel.css" media="all" />



    <!--<script type="text/javascript" src="js/024a09ae5f40e4b3915f3b17b4da59ed.js"></script> -->



    <script src="js/easyResponsiveTabs.js"></script>

   <script src="js/owl.carousel.min.js"></script>

       <!-- <script src="http://code.jquery.com/jquery-1.8.2.js"></script>

    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>-->
  




    <script type="text/javascript">

    //<![CDATA[

    optionalZipCountries = ["HK","IE","MO","PA"];

    //]]>

    </script>

   <!-- <script type="text/javascript">var Translator = new Translate({"Please use only letters (a-z or A-Z), numbers (0-9) or underscore(_) in this field, first character should be a letter.":"Please use only letters (a-z or A-Z), numbers (0-9) or underscores (_) in this field, first character must be a letter."});</script>-->

</head>

<body>

<?php //var_dump(Yii::$app->user->identity);die(); ?>

    <?php $this->beginBody() ?>

    <div class="wrap">

        <?php

            FrontNavBar::begin([

                'brandLabel' => 'My Company',

                'brandUrl' => Yii::$app->homeUrl,

                'options' => [

                    'class' => 'navbar-inverse navbar-fixed-top navbar-static-top',

                ],

                'userName'=>'jsdf'

            ]);

            $menuItems = [

                ['label' => 'Home', 'url' => ['/site/index']],

            ];

            if (Yii::$app->user->isGuest) {

                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];

            } else {

                $menuItems[] = [

                    'label' => 'Logout (' . Yii::$app->user->identity->email . ')',

                    'url' => ['/site/logout'],

                    'linkOptions' => ['data-method' => 'post']

                ];



            }

            

            FrontNavBar::end();

        ?>



        <script>



/*jQuery(document).ready(function($) {



    $('#nav .sub').on(click,function() {



     $(this).addClass('over');



   // alert('hai');



    });



});*/



$(document).ready(function () {

    $('.sub').hover(function () {

        $(this).addClass('over');

    },function() {

     

        $(this).removeClass('over');



    });

});



</script>



        <div class="container">



        <div class="navOuter">

        <div class="wrapper_content">

        <div class="nav">

        

       <?php



         echo MainMenu::widget([

         /*'options' => ['class' => 'nav'],

         'items' => frontend\components\MainMenu::init(),

        */

        ]); 





        ?>    



        </div>

        </div>

        </div>







        <div class="contents"> 

            <div class="content_area_home">



         <?= $content ?>



         </div>



        </div> 



        </div>



<footer class="footer">

<div class="container">

<!--  <p class="pull-left">&copy; BJZ Jewellers <?= date('Y') ?>/ <a href=""> Admin login </a></p> -->

   

 

<div class="wrapper_content">

<div class="footer_block">

<ul>

<li class="aboutus_block">

<h3>About Us</h3>

<a href="">Our Story</a>

<a href="">Services</a>

<a href="">Diamond Education</a>

<a href="#">Design Specialists</a>

<a href="">Gallery</a>

<a href="">Brands</a>

</li>

<li class="shop_block">

<h3>Shop</h3>

<a href="">My Account</a>

<a href="">Register</a>

<a href="#">Order Tracking</a>

<a href="">Online Gift Voucher</a>

<a href="" target="_blank">Find My Ring Size</a>

<a href="">Promotions</a>

</li>

<li class="support_block">

<h3>Support</h3>  

<a href="">Delivery &amp; Returns</a>

<a href="">Warranty</a>

<a href="">FAQ</a>

<a href="">Terms &amp; Conditions</a>

<a href="">Contact Us</a>

<!--

<a href="http://www.legj.com.au/site1/our-locations/">Our locations </a>

-->



<a href="">Privacy &amp; Security</a>

</li>

<li class="member_block">

<a href="" target="_blank"><img src="/images/jaa.png" alt="JAA" /></a>

</li>

<li class="social_block">

<div class="finduson_top">

<span>Find us on</span>

<a href="" target="_blank"><img src="/images/facebook_top.png" /></a>

<a href="" target="_blank"><img src="/images/youtube_top.png" /></a>

<a href="" target="_blank"><img src="/images/pininterest_top.png" /></a>

</div>

<div class="clear"></div>

</li>

<li class="clear"></li>

</ul>

<div class="copy">

<span>&copy; BJZ Jewellers <?= date('Y') ?></span>&nbsp;&nbsp;/&nbsp;&nbsp;<span><a href="" target="_blank">Admin login</a></span>

</div>

<div class="go_top">

<a href="javascript:void(0)" onclick="totopslow()"></a>

</div>

<div class="clear"></div>

</div>

</div>





            

<script type="text/javascript">  

 


   

    $(document).ready(function() {      

            

        //Vertical Tab      

        $('#parentVerticalTab').easyResponsiveTabs({        

            type: 'vertical', //Types: default, vertical, accordion     

            width: 'auto', //auto or any width like 600px       

            fit: true, // 100% fit in a container       

            closed: 'accordion', // Start closed if in accordion view       

            tabidentify: 'hor_1', // The tab groups identifier      

            activate: function(event) { // Callback function if tab is switched     

                var $tab = $(this);     

                var $info = $('#nested-tabInfo2');      

                var $name = $('span', $info);       

                $name.text($tab.text());        

                $info.show();       

            }       

        });     

    });     



    setTimeout(function(){ $(".rating-star").rating({disabled: true, showClear: false, showCaption: false})}, 500);

</script>

<script type="text/javascript">

//<![CDATA[

 /*   var dataForm = new VarienForm('form-validate', true);

    function setPasswordForm(arg){

        if(arg){

            $('current_password').up(3).show();

            $('current_password').addClassName('required-entry');

            $('password').addClassName('required-entry');

            $('confirmation').addClassName('required-entry');



        }

        /*else{

            $('current_password').up(3).hide();

            $('current_password').removeClassName('required-entry');

            $('password').removeClassName('required-entry');

            $('confirmation').removeClassName('required-entry');

        }*/

  //  }

    //]]>

</script>

<script>

  $(document).ready(function(){

  $('.owl-carousel').owlCarousel({

    loop:true,

    margin:10,

    responsiveClass:true,

    responsive:{

        0:{

            items:1,

            nav:true

        }

    }

})

});





//$("#main-menu .nav .nav li > a").addClass("triger-wrap");
//$("#main-menu").children().find('li a').addClass("triger-wrap");




$(".triger-wrap").click(function(event){
	event.preventDefault();
	if($(this).parent().hasClass('active'))
	{  
		 $(this).parent().find('.sub-wrap').toggle();
		 $(this).toggleClass('rotate');
		 
	}
	else
	{
	$('.sub-wrap').css('display','none');	
    $(this).parent().find('.sub-wrap').toggle();	
	}
	$(".triger-wrap").parent().removeClass('active');
	$(this).parent().addClass('active');
});

$(".triger-wrap").click(function(){
    $(this).toggleClass("main");
});


</script>
<script src="/js/canvas.js"></script>


</div><!--footer-->







     





        </div>

    </footer>



    <?php 

    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);

    $this->endBody() 

    ?>

</body>

</html>

<?php $this->endPage() ?>

