<?php 
    use frontend\components\Helper;
    use yii\helpers\Html;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->getThumbnailImage();
?>



<li> <a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>">
    <div class="product-img"><img alt="<?=$model->name ?>" src="<?=$thumbImagePath?>"></div>
    </a> 
    <!-- Start prdct-details -->
    <div class="prdct-details">
      <h5><a href=""><?=Helper::stripText($model->name,23) ?></a></h5>
      <div class="price"><?=Helper::money($model->price)?></div>
      <!-- Start add-cart-bg -->
      <div class="add-cart-bg">
        <div class="pull-left"><a href="<?=\yii\helpers\Url::to('@web/'.$model->urlKey.".html")?>"><i class="fa fa-shopping-cart"></i> Add to cart</a></div>
        <div class="pull-right"> <a href=""><i class="fa fa-eye"></i></a> <a href=""><i class="fa fa-heart"></i></a> </div>
      </div>
      <!-- Start add-cart-bg --> 
    </div>
    <!-- End prdct-details --> 
  </li>