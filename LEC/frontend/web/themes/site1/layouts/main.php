<?php  //test
  use yii\helpers\Html;
  use yii\bootstrap\Nav;
  use yii\bootstrap\NavBar;
  use yii\widgets\Breadcrumbs;
  use frontend\assets\AppAsset;
  use frontend\widgets\Alert;
  use frontend\components\FrontNavBar;
  use frontend\components\MainMenu;
  use frontend\components\HeaderMenu;
  use frontend\components\FooterMenu;
  use yii\helpers\Url;
  use yii\widgets\ActiveForm;
  use common\models\Stores;
  use common\models\Categories;
  use yii\db\Query;     
  AppAsset::register($this);
    
?>
    <?php
    
      $store = Stores::findOne(Yii::$app->params['storeId']);
    
      $facebookUrl = $store->getfacebookUrl();
    
      $youtubeUrl = $store->getyoutubeUrl();
    
      $linkedinUrl = $store->getlinkedinUrl();
    
      $googleplusUrl = $store->getgoogleplusUrl();
    
      $twitterUrl = $store->gettwitterUrl();
    
      $instagramUrl = $store->getinstagramUrl();
    
      $pinterestUrl = $store->getpinterestUrl();
    
    ?>
    <?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="<?= Yii::$app->charset ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="title" content="<?= Yii::$app->controller->metaTitle?>" />
<meta name="keywords" content="<?= Yii::$app->controller->metaKeywords?>" />
<meta name="description" content="<?= Yii::$app->controller->metaDescription?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=1310">
<meta name="robots" content="noindex">
<link rel="icon" href="/themes/site1/images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/themes/site/images/favicon.ico" type="image/x-icon" />
      <?= Html::csrfMetaTags() ?>
      <title>
      <?= Yii::$app->controller->metaTitle?>
      </title>
      <?php $this->head() ?>
<?php include "scripts.php" 
//Yii::$app->view->renderPartial('scripts'); ?>

<!--<link href="css/theme.css" rel="stylesheet">-->


<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77264465-4', 'auto');
  ga('send', 'pageview');

</script>
</head>
    <body>
    <?php $this->beginBody() ?>
<?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>
                <a href="<?=\yii\helpers\Url::to(['byod/logout'])?>" class="logout-fix" title="Logout from BYOD"><i class="fa fa-sign-out"></i><span>Logout <br/>from BYOD</span></a>
        <?php } ?>
    <?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>
      <a href="<?=\yii\helpers\Url::to(['byod/logout'])?>" class="logout-fix" title="Logout from BYOD"><i class="fa fa-sign-out"></i><span>Logout <br/>from BYOD</span></a>
    <?php } ?>  
    
    <!-- Start header -->
    <header> 
      <!-- Start top -->
      <div class="top"> 
        <!-- Start container -->
        <div class="container"> 
          <!-- Start top-left-icons -->
          <div class="top-left-icons pull-left">
            <ul>
            <?php if($facebookUrl!=''){ ?><li>
          <a href="<?=$facebookUrl?>" target="_blank"><i class="fa fa-round fa-facebook"></i></a></li>
          <?php } ?>
          <?php if($youtubeUrl!=''){ ?><li>
          <a href="<?=$youtubeUrl?>" target="_blank"><i class="fa fa-round fa-youtube"></i></a></li>
          <?php } ?>
          <?php if($linkedinUrl!=''){ ?><li>
          <a href="<?=$linkedinUrl?>" target="_blank"><i class="fa fa-round fa-linkedin"></i></a></li>
          <?php } ?>
         <?php if($googleplusUrl!=''){ ?> <li>
          <a href="<?=$googleplusUrl?>" target="_blank"><i class="fa fa-round fa-google"></i></a></li>
          <?php } ?>
          <?php if($twitterUrl!=''){ ?><li>
          <a href="<?=$twitterUrl?>" target="_blank"><i class="fa fa-round fa-twitter"></i></a></li>
          <?php } ?>
          <?php if($instagramUrl!=''){ ?><li>
          <a href="<?=$instagramUrl?>" target="_blank"><i class="fa fa-round fa-instagram"></i></a></li>
          <?php } ?>
          <?php if($pinterestUrl!=''){ ?><li>
          <a href="<?=$pinterestUrl?>" target="_blank"><i class="fa fa-round fa-pinterest"></i></a></li>
          <?php } ?></ul>
          </div>
          <!-- End top-left-icons --> 
          
          <!-- Start top-right-icons -->
          <div class="top-right-icons pull-right">
            <form class="navbar-form pull-left" role="search" id="search_mini_form" onsubmit="return validheadersearch();" action="/site/search" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search here..." value="" name="q" id="searchname" required>
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
          <ul>
	<li> <?php if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])) { ?>	

		<a href="<?=\yii\helpers\Url::to(['byod/view'])?>" style="margin-right:20px;"  title="BYOD" class="top-link-byod"> BYOD</i></a>
	 <?php } else {  ?>
		<a href="<?=\yii\helpers\Url::to(['byod/apply'])?>" title="BYOD" style="margin-right:20px;" class="top-link-byod bootstrap-modal" data-complete="window.location = '/byod/view';" data-parameters='{"noFlash":"true"}'> BYOD</i></a>
	<?php } ?>
	<?php if (!\Yii::$app->user->isGuest) { ?>
          <a href="<?=\yii\helpers\Url::to(['site/account'])?>" title="My Account" class="myaccount"><i class="fa fa-user"></i>MY ACCOUNT</a><a href="<?=Yii::$app->urlManager->createUrl(['site/logout']) ?>" title="Log In" >LOG OUT</a>
          <a href="<?=\yii\helpers\Url::to(['site/wishlist'])?>" title="View Cart" class="top-link-cart"><li><i class="fa favrite-icon"></i></li></a>
          <li><a href="<?=\yii\helpers\Url::to(['site/myorders'])?>" title="View Cart" class="top-link-cart"><i class="fa van-icon"></i></a></li>
          <?php } else { ?>
          <a href="<?=\yii\helpers\Url::to(['site/signup']) ?>" title="Register" class="register">REGISTER</a> <a href="<?=\yii\helpers\Url::to(['site/login'])?>" title="Log In" >LOGIN</a>
          <?php } ?></li>
              <li class="cart-icon"><a href="<?=\yii\helpers\Url::to(['cart/view'])?>" title="Checkout" class="top-link-checkout"> <span class="cart-value"><?php $n=count(Yii::$app->cart->positions); echo $n;?></span></a></li>
               <!--<li> <a href="">
                <div class="van-icon">site/myorders</div>
                </a> </li>
             <li><a href="<?=\yii\helpers\Url::to(['site/wishlist'])?>" title="View Cart" class="top-link-cart">
                <div class="favrite-icon"></div>
                </a> </li>-->
              <li> <a href="<?=\yii\helpers\Url::to(['site/contact'])?>" title="Contact" >
                <div class="headset-icon"></div>
                </a> </li>
            </ul>
          </div>
          <!-- End top-right-icons --> 
        </div>
        <!-- End container --> 
      </div>
      <!-- End top --> 
      
      <!-- Start nav-bg -->
      <div class="nav-bg"> 
        <!-- Start container -->
        <div class="container"> 
         
          <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
              <div class="navbar-header"> 
              <a href="<?=\yii\helpers\Url::to('@web/')?>"><img src="/themes/site1/images/logo-site1.png"  alt="logo" width="200px" height="65px"></a> 
               
                <ul class="nav navbar-nav pull-right">
                <?php //echo HeaderMenu::widget([ ]);?> 
              </ul>

              </div>
              <div class="bend-logo"> 
              <a class="navbar-brand" href="<?=\yii\helpers\Url::to('@web/')?>"><img src="<?=Yii::$app->params["rootUrl"]?>/store/site/logo/logo.png"></a>  
              </div>
        </div>
            </div>
          </nav>
         
        </div>
       
      </div>
     
      <div class="nav-bg2"> 
        <div class="container"> 
        
          <nav class="navbar navbar-default" role="navigation">
            <div class="category-menu">
              <?php echo MainMenu::widget([ ]);?>
              
            </div>
          </nav>
        
        </div>
        
      </div>
    
      <!--<div class="banner-bg"><img src="images/banner.jpg"></div>-->
      
     
    </header>
    <!-- End header --> 
    
    <!-- Start section -->
    <section> 
      <!-- Start cont-bg -->
      <div class="cont-bg"> 
        <!-- Start container -->
      <div class="banner">
    <?php if(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='site/index' && Yii::$app->controller->id.'/'.Yii::$app->controller->action->id!='categories/products') {?>
        <div class="cart-heading"><!-- breadcrumb -->
          <div class="container">
              <!-- <h4><?= Html::encode($this->title) ?></h4> -->
              <div class="pull-right">                          
                  <?= Breadcrumbs::widget([
                      'tag' => 'ol',
                      //'options' => ['class' => 'breadcrumb'],
                      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                  ]) ?>
              </div>
          </div>
        </div>
        </div>
    <?php } ?>
    <div class="clearfix"></div>
        <div>
        
        <div class="clearfix"></div>
      <?= $content ?>
      </div>
        <!-- End container --> 
      </div>
      <!-- End cont-bg --> 
    </section>
    <!-- End section --> 
    
    <!-- Start Footer -->
    <footer> 
      <!-- Start footer-cont1-bg -->
      <div class="footer-cont-bg1"> 
        <!-- Start container -->
        <div class="container">
          <div class="subscribe-btn">
            <div class="suscribe">SUBSCRIBE</div>
          </div>
            <form id="sub-form"  action="/ajax/subscribe" method="post" class="navbar-form pull-left" onsubmit="return isValidEmailAddress()">
            <div class="input-group">
                <input type="email"  name="email" id="sub-email"  placeholder="Enter your email" class="form-control" required >
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit" value=""><i class="fa fa-angle-right"></i></button>
              </div>
            </div>
          </form>
          
          <!-- Start scl-icon -->
          <div class="scl-icon pull-right">
            <ul>
              <li><a href="">Follow us</a></li>
              <?php if($facebookUrl!=''){ ?><li>
              <a href="<?=$facebookUrl?>" target="_blank" class="fa fa-round fa-facebook"></a></li>
              <?php } ?>
              <?php if($youtubeUrl!=''){ ?><li>
              <a href="<?=$youtubeUrl?>" target="_blank" class="fa fa-round fa-youtube"></a></li>
              <?php } ?>
              <?php if($linkedinUrl!=''){ ?><li>
              <a href="<?=$linkedinUrl?>" target="_blank" class="fa fa-round fa-linkedin"></a></li>
              <?php } ?>
             <?php if($googleplusUrl!=''){ ?> <li>
              <a href="<?=$googleplusUrl?>" target="_blank" class="fa fa-round fa-google"></a></li>
              <?php } ?>
              <?php if($twitterUrl!=''){ ?><li>
              <a href="<?=$twitterUrl?>" target="_blank" class="fa fa-round fa-twitter"></a></li>
              <?php } ?>
              <?php if($instagramUrl!=''){ ?><li>
              <a href="<?=$instagramUrl?>" target="_blank" class="fa fa-round fa-instagram"></a></li>
              <?php } ?>
              <?php if($pinterestUrl!=''){ ?><li>
              <a href="<?=$pinterestUrl?>" target="_blank" class="fa fa-round fa-pinterest"></a></li>
              <?php } ?>
            </ul>
          </div>
          <!-- End scl-icon --> 
        </div>
        <!-- End container --> 
      </div>
      <!-- Start footer-cont1-bg --> 
      
      <!-- Start footer-cont-bg2 -->
      <div class="footer-cont-bg2"> 
        <!-- Start container -->
        <div class="container"> 
          <!-- Start bttm-link -->
          <div class="col-xs-8 bttm-link">
            <?php echo FooterMenu::widget([ ]);?>
            <div class="clientportal-div" style="display:none;">
                <?php if(isset(Yii::$app->session['clientportal'])) { ?> 
                  <a href="<?=\yii\helpers\Url::to(['client-portal/view'])?>" title="Client Portal" class="top-link-portal"> Client Portal</i></a>
                <?php } else {  ?>
                  <a href="<?=\yii\helpers\Url::to(['client-portal/apply'])?>" title="Client Portal" class="top-link-portal bootstrap-modal" data-complete="window.location = '/client-portal/view';" data-parameters='{"noFlash":"true"}'> Client Portal</i></a>
                <?php } ?>
              </div> 
          </div>
          <!-- End bttm-link --> 
          
          <!-- Start bttm-link -->
          <div class="payment-icons">
            <ul>
              <li><img src="/themes/site1/images/icon-visa.png"></li>
              <li><img src="/themes/site1/images/icon-mastercard.png"></li>
              <li><img src="/themes/site1/images/icon-discover.png"></li>
              <!-- <li><img src="/themes/site1/images/icon-paypal.png"></li> -->
            </ul>
          </div>
          <!-- End bttm-link --> 
        </div>
        <!-- End container --> 
      </div>
      <!-- End footer-cont-bg2 --> 
      
      <!-- Start footer-cont-bg3 -->
      <div class="footer-cont-bg3"> 
        <!-- Start container -->
        <div class="container"> 
          
          <!-- Start bttm-cnt-bg -->
          <div class="bttm-cnt-bg"> 
            <!-- Start bttm-cnt -->
            
              <?=$this->renderBlock('home-page'); // cms block page slug ?>
            <!-- End bttm-cnt1 --> 
          </div>
          <!-- End bttm-cnt-bg -->
          <p class="copyright">Copyright © <?=$store->title?> <?= date('Y') ?>. All rights reserved</p>
        </div>
        <!-- End container --> 
      </div>
      <!-- End footer-cont-bg3 --> 
      
    </footer>
    <!-- Start Footer --> 
   
    
    <script>
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                items:1,
                loop:true,
                autoplay:true,
                autoplayTimeout:7000,
                autoplayHoverPause:true
            });
        </script>

    <script type="text/javascript">
        $(document).ready(function () {

          $(".category-menu ul li").each(function( index ) {
            if($(this).children().find('ul').length > 0){
              $(this).find('a').append('<i class="fa fa-angle-down"></i>');
            }
          });

           
        });    
    </script>    

    <script type="text/javascript">
        /*$(document).ready(function() {
            $('ul').not(':has(li)').remove();
            setTimeout(function(){ $("ul.pagination").wrap("<div class='col-xs-12 text-center'></div>")}, 1000);
        });*/
    </script>

   <?php
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody()
?>
</body>
</html>
<?php $this->endPage() ?>
<script>
/*function validateForm() {
    var x = document.forms["sub-form"]["sub-email"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Please enter a valid email.");
        return false;
    }
}*/
function isValidEmailAddress() {
    var emailAddress = document.forms["sub-form"]["sub-email"].value;
    if(emailAddress.length != 0){
      var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
      if(pattern.test(emailAddress) == false ){
        alert("Please enter a valid email.");
          return false;
      }
    }
    else{
      alert('Please enter a valid email.');
      return false;
    }  
};
</script>
<script type="text/javascript">

    $(document).ready(function() {

        $('ul').not(':has(li)').remove();

    });

</script>

