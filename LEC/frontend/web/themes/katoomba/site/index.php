<?php 
use common\models\Storebanners;
use common\models\Configuration;
use common\models\Banners;
use common\models\Categories;
use yii\widgets\ListView;
use yii\db\Query;
use frontend\components\MainCategory;
?>
<link rel="stylesheet" href="/themes/katoomba/css/jquery.mCustomScrollbar.css">
    
    
    <script>
      $(document).ready(function(){
          $('.scrollbar-macosx').scrollbar({ "ignoreMobile": false, "ignoreOverlay": false });
      });
    </script>
<div class="container">
      <div class="banner">
        <div class="row">
          <div class="col-xs-3">
            <div class="menu-container left-menu-wrap demo">
              <div class="scrollbar-macosx">
              
              <?php echo MainCategory::widget([ ]);?>
                </div>
              
              
            </div>
          </div>
          
          <!-- <div class="demo">
            <div class="scrollbar-macosx">
              <a href="#"><div class="notification-item one">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item two">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item three">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item four">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item five">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item six">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item blah1">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item blah2">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item blah3">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item blah4">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
              <a href="#"><div class="notification-item blah5">adasdasdad dasdasdasdas asdas asd asdsa</a></div> <br>
            </div>
          </div> -->
                       
                       
          
          <div class="col-xs-9">
            <div class="banner-wrapper">
              <div id="owl-demo" class="owl-carousel owl-theme">
                <!-- <div class="item"><img src="/themes/katoomba/images/banner.jpg" alt=""/></div>
                <div class="item"><img src="/themes/katoomba/images/banner.jpg" alt=""/></div>
                <div class="item"><img src="/themes/katoomba/images/banner.jpg" alt=""/></div> -->
                    <?php foreach ($main_banner as $index => $banner){ 
                        $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one();?>
                        <div class="item">
                            <?php if($banner_images['html']!=''){ //var_dump($banner_images->youtubeUrl);die;?>
                                <a class="modalButton" data-toggle="modal" data-src="<?=$banner_images->youtubeUrl?>?rel=0&wmode=transparent&fs=0" data-target="#myModal"> 
                                  <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" >
                                </a>
                            <?php 
                            } 
                            elseif ($banner_images['url']!='') {   
                                if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url']; }                                                 
                                else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                                else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                                ?> 
                                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                                            
                                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>'  >
                                    </a>
                                <?php //echo "</div>"; 
                            } 
                            else{ ?>
                                 <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" > 
                            <?php }  ?>
                           
                        </div>
                    <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="products-wrapper">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#bestseller" aria-controls="home" role="tab" data-toggle="tab">Best Sellers</a></li>
          <li role="presentation"><a href="#special" aria-controls="profile" role="tab" data-toggle="tab">Featured Products</a></li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="bestseller">
            <div class="products-container">
              <div class="row">
              
              <?php 
                                        $bestSellers->pagination = [
                                            'defaultPageSize' => 10,
                                        ]; 
                                        echo ListView::widget([
                                            'id' => 'homepage-products',
                                            'summary'=>'', 
                                            'dataProvider' => $bestSellers,
                                            'itemView' => '/products/_productlist',
                                            'summary'=>'',
                                            'layout' => "{items}", 
                                            'itemOptions' => ['class' => 'items_featured col-xs-3']
                                        ]);
                                         echo "<script> $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});</script>";
                                    ?>
              </div>
            </div>
          </div>
        <div role="tabpanel" class="tab-pane" id="special"><div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="special">
            <div class="products-container">
              <div class="row">
              
              <?php 
                                        $featuredProducts->pagination = [
                                            'defaultPageSize' => 10,
                                        ]; 
                                        echo ListView::widget([
                                            'id' => 'homepage-products',
                                            'summary'=>'', 
                                            'dataProvider' => $featuredProducts,
                                            'itemView' => '/products/_productlist',
                                            'summary'=>'',
                                            'layout' => "{items}", 
                                            'itemOptions' => ['class' => 'items_featured col-xs-3']
                                        ]);
                                         echo "<script> $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});</script>";
                                    ?> </div>
              </div>
        </div></div>
        </div></div>
        </div>
        </div>
    </div>
    </div>
      <div class="ad-full"><?php foreach ($strip_banner as $index => $banner){
            $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one(); ?>           
                <?php if($banner_images['url']!='') { 
                    if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url'];}                                                  
                        else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                        else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                    ?>
                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                            
                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                    </a>
                <?php } else { ?>
                <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
            <?php } ?>
        <?php } ?></div>
     <div class="carousal-wrapper">
      <div class="container">
          <?php                        
                    echo ListView::widget([
                    //'id' => 'homepage-products',
                    'summary'=>'', 
                    'options' => [
                        'tag' => 'div',
                        'class' => 'owl-carousel2',
                        'id' => '',
                    ],
                    'dataProvider' => $brands,
                    'itemView' => '/products/_brandlist',
                    'summary'=>'',
                    'layout' => "{items}",                                                    
                    'itemOptions' => ['class' => 'item']
                ]);?>
      </div>
    </div>
   <!-- <style type="text/css">
  .catr-menus{ overflow: hidden;}
  .attached .catr-menus{ overflow: visible;}
 
    </style>-->
    
    