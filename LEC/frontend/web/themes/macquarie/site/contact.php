<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */
$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
    <div class="inner-container">
        <div class="site-contact">  
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="cntct-head">Get in touch</h1>
                    <p> If you have business enquiries or other questions, please fill out the following form to contact us. Thank you. </p>
                </div>
                <div class="col-xs-9">
                    <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'name') ?>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'email') ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'subject') ?>
                           <div class="collect_store_div form-group">
                                <?php if(isset($addresses) && count($addresses) >1)  { 
                                    $options = Array();
                                    foreach ($addresses as $address) {
                                        $options[$address->id] = ['data-address'=>$address->billingAddressAsText];
                                    } ?>                                
                                    <label for="contactform-subject" class="control-label">Store:</label>
                                    <?=Html::dropDownList('ContactForm[storeAddressId]', [], ArrayHelper::map($addresses, 'id', 'addressForDropdown'), ['class' => 'stored-billing-address form-control','id'=>'b2b-address', 'options' => $options])?>                            
                                <?php    } ?>
                            </div>   
                            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                'template' => '<div class="row"><div class="col-xs-3">{image}<a href="#" id="fox"><i class="fa fa-refresh"></i></a></div><div class="col-xs-9">{input}</div></div>',]) ?>
                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                            </div>
                        </div> 
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="trading_hours">
                                <span><i class="fa fa-clock-o"></i>TRADING HOURS:</span>
                                    MON     8.00am  &ndash; 6.00pm<br>
                                    TUE     8.00am  &ndash; 6.00pm<br>
                                    WED     8.00am  &ndash; 6.00pm<br>
                                    THU     8.00am  &ndash; 6.00pm<br>
                                    FRI     8.00am  &ndash; 6.00pm<br>
                                    SAT     Closed<br>
                                    SUN     Closed
                                <span id="trading-hours"> <!-- trading hours --></span>                            
                            </div> 
                        </div>
                        <div class="col-xs-12">
                            <div class="contact-bottom ">
                                <div class="mapaddress">
                                    <span><i class="fa fa-map-marker"></i> Address:</span> 
                                    Leading Edge Computers Port Macquarie,<br/>
                                    2/171 Lake Rd,<br/>
                                    Port Macquarie,<br/>
                                    NSW 2444,<br/>
                                    Australia.<br/>
                                    P:02 6584 4955

                                    <div class="address_dropdown"></div>
                                </div>
                            </div>  
                        </div>         
                    </div>
                </div> 
                <div class="col-xs-12 cnct-form">
                    <iframe width='100%' id="map-frame" height='330' frameborder='0' style="border:0" src='https://maps.google.com/maps?&amp;q=<?=$defaultAddress->billingAddressAsText?>&amp;output=embed'></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#fox").click(function(event){ // captcha refresh button 
        event.preventDefault();
        $("img[id$='-verifycode-image']").click();
    });
    $(document).ready(function(){
        $('.stored-billing-address').change(function(){
            address = $('.stored-billing-address option:selected').attr("data-address");
            src = "https://maps.google.com/maps?&q="+address+"&output=embed";
            $('#map-frame').attr('src',src);
           
            $('.address_dropdown').html($('.stored-billing-address option:selected').text().replace(/\,/g,'<br/>'));
        //  $('.address_dropdown').html($('.stored-billing-address option:selected').text().replace(/\ /g,'<br/>'));
        }); 

        $('.address_dropdown').html($('.stored-billing-address option:selected').text().replace(/\,/g,'<br/>'));
    });       

</script>
<script type="text/javascript">
    $(document).ready(function(){        
        $('.stored-billing-address').change(function(){            
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['site/get-trading-hours'])?>",
                data: {id: $("#"+this.id+" option:selected").val()},
                dataType: "html", 
                success: function (data) {
                    //alert(data);
                    $( '#trading-hours' ).html( data );                              
                }                
            });
        });
        $.ajax({
            type: "POST",
            url: "<?=Yii::$app->urlManager->createUrl(['site/get-trading-hours'])?>",
            data: {id: $("#b2b-address option:selected").val()},
            dataType: "html", 
            success: function (data) {                
                $( '#trading-hours' ).html( data );                              
            }                
        });
    });
</script>