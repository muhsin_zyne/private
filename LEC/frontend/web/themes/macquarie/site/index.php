<?php 
use common\models\Storebanners;
use common\models\Configuration;
use common\models\Banners;
use yii\widgets\ListView;
use yii\db\Query;
?>



 <div id="owl-demo" class="owl-carousel owl-theme">
    <?php foreach ($main_banner as $index => $banner){ 
        $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one();?>
        <div class="item">
            <?php if($banner_images['html']!=''){ //var_dump($banner_images->youtubeUrl);die;?>
                <a class="modalButton" data-toggle="modal" data-src="<?=$banner_images->youtubeUrl?>?rel=0&wmode=transparent&fs=0" data-target="#myModal"> 
                    <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" >
                </a>
            <?php 
            } 
            elseif ($banner_images['url']!='') {   
                if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url']; }                                                 
                else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                ?> 
                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                                            
                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>'  >
                    </a>
                <?php //echo "</div>"; 
            } 
            else{ ?>
                 <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' alt="#" > 
            <?php }  ?>
           
        </div>
    <?php } ?> 
</div>
<!---       youtube popup         -->
      <!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                        <button type="button" class="close model-close-sm" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <div class="modal-body frame-sm">
                        <iframe frameborder="0"></iframe>
                  </div>         
              </div>/.modal-content -->
        <!--  </div> /.modal-dialog -->
      <!--</div> /.modal -->
<!-- main banner end-->
<div class="container">
          <h3>Featured Products</h3>
          <div class="border-div"></div>
          
        
          <div class="item-first-row row">
          
            <?php  
            $featuredProducts->pagination = [
                'defaultPageSize' => 10,
                                        ];
                 echo ListView::widget([
                'id' => 'homepage-products',
                'summary'=>'', 
                'dataProvider' => $featuredProducts,
                'itemView' => '/products/_productlist',
                'summary'=>'',
                'layout' => "{items}",
                'itemOptions' => ['class' => 'col-xs-3']
            ]);?> 
          
          </div>
          <div class="advt">
           <?php foreach ($strip_banner as $index => $banner){
            $banner_images = Banners::find()->where(['id' => $banner['sbid']])->one(); ?>           
                <?php if($banner_images['url']!='') { 
                    if(preg_match("/http/",$banner_images['url'])) {$banners_url=$banner_images['url'];}                                                  
                        else if(preg_match("/www/",$banner_images['url'])) {$banners_url='http://'.$banner_images['url'];} 
                        else{$banners_url=Yii::$app->urlManager->baseUrl.$banner_images['url'];} 
                    ?>
                    <a target="<?=$banner_images['target']?>" href="<?=$banners_url?>">                            
                        <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
                    </a>
                <?php } else { ?>
                <img src ='<?=Yii::$app->params["rootUrl"].$banner_images['path']?>' >
            <?php } ?>
        <?php } ?>
          </div>
          
        </div>
        <!-- End container -->       
<div id="fb-root"></div>        
<script type="text/javascript">
    $('a.modalButton').on('click', function(e) {   //youtube popup
      var src = $(this).attr('data-src');
      var height = $(this).attr('data-height') || 320;
      var width = $(this).attr('data-width') || 450;      
      $("#myModal iframe").attr({'src':src,'height': height,'width': width}); 
  });

</script>
<script>(function(d, s, id) {
        
          var js, fjs = d.getElementsByTagName(s)[0];
        
          if (d.getElementById(id)) return;
        
          js = d.createElement(s); js.id = id;
        
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
        
          fjs.parentNode.insertBefore(js, fjs);
        
        }(document, 'script', 'facebook-jssdk'));
</script> 