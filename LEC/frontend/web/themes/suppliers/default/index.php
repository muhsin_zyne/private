<?php

use common\models\OrderItems;


$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
$sid=yii::$app->user->id;

?>

<div class="dash-top">
  <div class="row">
    <div class="col-sm-6">
       <div class="btn-group">
          <button type="button" class="btn btn-primary">B2B</button>
          <button type="button" class="btn btn-primary">B2C</button>
          <button type="button" class="btn btn-primary active">ALL</button>
        </div>
    </div>
    <div class="col-sm-6 dt-right">
       <a href="#">Last 7 Days</a>|
       <a href="#" class="active">30 Days</a>|
       <a href="#">6 Months</a>|
       <a href="#">1 Year</a>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="dash-box">
          <h2><?php $order_count = OrderItems::find()->where(['supplierUid' => \Yii::$app->user->identity->id])->sum('quantity'); echo $order_count; ?></h2>
            <h3 class="box-title">Total number of units orderd</h3>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="dash-box dash-box-two">
          <h2><?php $order_price = OrderItems::find()->where(['supplierUid' => 746])->count(); echo $order_count; ?></h2>
            <h3 class="box-title">Total value of units orderd</h3>
        </div>
    </div>

    <div class="col-md-6">
      <div class="box">
        <div class="box-body">
          <div class="dash-graph-head">Orders by Brand</div>
          <div class="dash-graph-cnt">
            
            place your graph here
          
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="box">
        <div class="box-body">
          <div class="dash-graph-head dgh-two">
            Orders by member
            <i class="fa fa-desktop active"></i>
            <select>
              <option>J360A</option>
              <option>J270A</option>
            </select>
            
          </div>
          <div class="dash-graph-cnt">
            
            place your graph here
          
          </div>
        </div>
      </div>
    
    </div>
    
    
    <div class="col-md-6">
      <div class="box">
        <div class="box-body">
          <div class="dash-table-head">
            recent b2b Orders
            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i>
            <a href="#">View all</a>
          </div>
          <div class="table-responsive dash-table-cnt">
            <table class="table">
              <tr>
                <th>Date</th>
                <th>Member No</th>
                <th>No of items</th>
                <th>Grand total</th>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
            </table>
          
          </div>
        </div>
      </div>
    </div>
    
    
    <div class="col-md-6">
      <div class="box">
        <div class="box-body">
          <div class="dash-table-head">
            recent b2c Orders
            <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i>
            <a href="#">View all</a>
          </div>
          <div class="table-responsive dash-table-cnt">
            <table class="table">
              <tr>
                <th>Date</th>
                <th>Member No</th>
                <th>No of items</th>
                <th>Grand total</th>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
              <tr>
                <td>11/03/2016</td>
                <td>JE360A</td>
                <td>11</td>
                <td>AU$263.78</td>
              </tr>
            </table>
          
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
</div>


