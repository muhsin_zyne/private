<?php
use frontend\components\Helper;
use yii\helpers\Url;
?>

<?php 
	//var_dump($model->path);die();
if(!empty($model->path))
	$logoPath = Yii::$app->params["rootUrl"].$model->path;
else
	$logoPath = Yii::$app->params["rootUrl"]."/store/brands/logo/nologo.jpg";

?>

<div class="brand_logo" title="<?=$model->title ?>">
	<a data-pjax=0  href="<?=\yii\helpers\Url::to(['brands/products','id'=>$model->id])?>"><img src="<?=$logoPath?>" alt="<?=$model->title?>"></a>
</div>