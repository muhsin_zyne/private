<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */



$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="form-box" id="login-box">
    <div class="col-main">
        <div class="header">Supplier Portal</div>
        <div class="body bg-gray">
       
        <p>Please enter your registered email and we will send you a link to reset password.</p>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
          
            <div class="textfeild">
                <?= $form->field($model, 'email') ?>
            </div>
           <?= Html::submitButton('Send', ['class' => 'btn bg-olive btn-block']) ?>
        <?php ActiveForm::end(); ?> 
       </div>

    </div>    
</div>

    