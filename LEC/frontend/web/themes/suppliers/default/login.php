<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="form-box" id="login-box">
    <div class="header">Supplier Portal</div>
    <?php $form = ActiveForm::begin(['id' => 'loginform']); ?>
        <div class="body bg-gray">
            <?php if(Yii::$app->session->getFlash('error'))
              { ?>
              <div class="alert alert-danger alert-dismissable">
                <i class="icon fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                         
                <?= Yii::$app->session->getFlash('error'); ?>       
              </div> 
            <?php } ?>
            <?php if(Yii::$app->session->getFlash('success'))
              { ?>
              <div class="alert alert-success alert-dismissable">
                <i class="icon fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?= Yii::$app->session->getFlash('success'); ?>       
              </div> 
            <?php } ?>
            <input type="hidden" name="form_key" value="EuSTNKx0DsGG2KSf" />
            <div class="textfeild">
                <?= $form->field($model, 'email') ?>
            </div>
            <div class="textfeild">
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>
            <div class="footer">
                <?= Html::submitButton('Login', ['class' => 'btn bg-olive btn-block', 'name' => 'login']) ?>
                <a ><?= Html::a('Forgot Password?', ['default/request-password-reset']) ?></a>
            </div>
            
        </div>
    <?php ActiveForm::end(); ?>
</div>
    
<script type="text/javascript">
  function validate_login_form()
    {  
        //alert('hiii');
      var x=document.forms["loginForm"]["username"].value;
      if (x==null || x=="")
        {
          alert("Please enter Username");
          return false;
        }  
      var x=document.forms["loginForm"]["password"].value;
      if (x==null || x=="")
        {
          alert("Please enter Password");
          return false;
        } 
      jQuery("#myOverlay").show();     
    }
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    //alert('hii');
     jQuery('span:contains("Catalogues")').parent().attr("href", "javascript:;");
  })
</script>