<?php
use yii\widgets\DetailView;
use common\models\User;
use common\models\Stores;
use yii\widgets\Pjax;
use common\models\B2bAddresses;
use common\models\OrderItems;
use common\models\Products;
use common\models\Orders;
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;

$this->title = 'Online Retailers'.' - '.$model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::csrfMetaTags() ?>
<div class="col-main">
    <div class="box">
    <div class="box-body">
    <div class="col-md-12">
        <div class="products-index">
            <div class="row">
            <!--<?= Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['default/export'], Yii::$app->request->queryParams)), ['class'=>'btn btn-success pull-right right-up']); ?>-->
                <?= \app\components\AdminGridView::widget([
                    //'id' => 'order-items',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        
                        //'id',
                        'memberNumber',
                        'storeName',
                        'storeEmail',
                        'storeBillingAddress',
                        //'storeShippingAddress',
                        'storePhone',
                        //'brandId',
                        //'type',
                    ],

                ]); ?>
                
            </div>
            </div>
        </div>
        <div class="clearfix"></div>
        </div>
    </div>
</div>

      
       
        
        
