<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use common\models\Suppliers;
use common\models\BrandSuppliers;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Products */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Please select a Brand';
$this->params['breadcrumbs'][] = $this->title; 
$this->params['tooltip']='Click on the brand to see the stores that sell it online.';
$this->params['tooltip']='Click on the brand to see the stores that sell it online.';
?>

<div class="products-index">
	<div class="box brand-box">
    <div class="box-body">
		<div class="row">
	 	<?php //$supplier = Suppliers::findOne(Yii::$app->user->Id);
	    $brandsuppliers=BrandSuppliers::find()->where(['supplierUid'=>Yii::$app->user->Id])->all();
	   	foreach ($brandsuppliers as $key => $value) {
	   		//var_dump($value->brand->path);die;
	 		//$brands=common\models\Brands::findOne($value['brandId']);//var_dump($brands);//
			if(!empty($value->brand->path))
				$logoPath = Yii::$app->params["rootUrl"].$value->brand->path;
			else
				$logoPath = Yii::$app->params["rootUrl"]."/store/brands/logo/nologo.jpg";
			if(isset($value->brand))
			{
			?>
			<div class="col-md-3">
               	<div class="box box-solid">                
                   	<div class="box-body sm-checkox">
						<div class="brand_logo" title="<?=$value->brand->title ?>">
							<a data-pjax=0  href="<?=\yii\helpers\Url::to(['default/brandproduct','id'=>$value->brand->id])?>"><img src="<?=$logoPath?>" alt="<?=$value->brand->title?>"></a>
						</div>
					</div>
				</div>
			</div>
		<?php } } ?>
	</div>
	</div>
    </div>
</div>
