<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-box" id="login-box">
    <div class="header">Supplier Portal</div>
    <div class="body bg-gray">
        <p><b>Please enter your new password.</b></p>
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'password_repeat')->passwordInput()->label('Re-enter Password') ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn bg-olive btn-block btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>  
</div>