<?php  
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\sidenav\SideNav;
use frontend\components\SuppliersNavBar;
use frontend\components\SuppliersMainMenu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- bootstrap 3.2.0 -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <!-- <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" /> -->
    <!-- local css -->
    <link href="/themes/suppliers/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/themes/suppliers/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> 
    <link rel="stylesheet" href="/themes/suppliers/css/jquery.gritter.css" />
    <link href="/themes/suppliers/css/site.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/themes/suppliers/css/skins.css" />

    
    <script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <!--<script type="text/javascript" src="themes/b2b/js/script.js"></script>-->
    <script type="text/javascript" src="/js/jquery.gritter.min.js"></script>
    <script type="text/javascript" src="/js/ajax-update.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/wl_Form.js"></script>
    <script src="/js/fileinput.js"></script>
    
    <!--<script src="js/kv-tree.js"></script>-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-blue">
    <?php $this->beginBody() ?>
            <?php
            SuppliersNavBar::begin([
                'brandLabel' => 'My Company',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top navbar-static-top',
                ],
                'userName'=>Yii::$app->user->identity->fullName
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->email . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];

            }
            // echo Nav::widget([
            //     'options' => ['class' => 'navbar-nav navbar-right'],
            //     'items' => $menuItems,
            // ]);
            SuppliersNavBar::end();
        ?>
    <div class="wrapper  row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas" style="min-height: 2250px;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/themes/suppliers/images/avatar5.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?=substr(Yii::$app->user->identity->fullName,0,22)?>.</p>

                            <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
                        </div>
                    </div>
                    <!-- search form -->
                    <!-- <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="seach" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu nav nav-pills nav-stacked kv-sidenav">
                       <!--<li class="<?=(Yii::$app->controller->id=='default')?'active':''?>"> 
                            <a href="<?=\yii\helpers\Url::to(['/suppliers/default'])?>"><span class="fa fa-cubes"></span> Dashboard</a>
                        </li>-->
                        <li class="<?=(Yii::$app->controller->id=='products')?'active':''?>"> 
                            <a href="<?=\yii\helpers\Url::to(['/suppliers/products'])?>"><span class="fa fa-cubes"></span> Items</a>
                        </li>
                        <li class="<?=(Yii::$app->controller->id=='orders')?'active':''?>"> 
                            <a href="<?=\yii\helpers\Url::to(['/suppliers/orders'])?>" ><span class="fa fa-shopping-cart"></span> Orders</a>
                        </li>
                        <li class="<?=(Yii::$app->controller->id=='default')?'active':''?>"> 
                            <a href="<?=\yii\helpers\Url::to(['/suppliers/default/brand'])?>" ><span class="fa fa-users"></span>Online Retailers</a> 
                        </li>                         
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
        <aside class="right-side">
            <section class="content-header">
                <h1><span><?=$this->title?></span>
                <?php if (isset($this->params['tooltip'])) {  ?>
                    <i class="fa fa-question-circle question-layout" data-toggle="tooltip" data-placement="right" title="<?=$this->params['tooltip']?>"></i><div class="clearfix"></div>
                <?php } ?>
                <div class="clearfix"></div>
                </h1>
                <?= Breadcrumbs::widget([
                'tag' => 'ol',
                'options' => ['class' => 'breadcrumb'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            </section>
            <section class="content">
            <?php if(Yii::$app->session->getFlash('error'))
              { ?>
              <div class="alert alert-danger alert-dismissable">
                <i class="icon fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>                         
                <?= Yii::$app->session->getFlash('error'); ?>       
              </div> 
            <?php } ?>
            <?php if(Yii::$app->session->getFlash('success'))
              { ?>
              <div class="alert alert-success alert-dismissable">
                <i class="icon fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?= Yii::$app->session->getFlash('success'); ?>       
              </div> 
            <?php } ?>
            <?= $content ?>
            </section>
        </aside>
    </div>

    <!-- <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer> -->

    
<?php 
    $this->off(\yii\web\View::EVENT_END_BODY, [\yii\debug\Module::getInstance(), 'renderToolbar']);
    $this->endBody() 
?>

</body>
</html>
<?php $this->endPage() ?>
