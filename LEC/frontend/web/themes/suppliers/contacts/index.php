<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    
        <div class="site-contact">
            <p>
                If you have any doubts/suggestions/queries, please feel free to fill the form below. We shall get back to you in a flash!
            </p>
            
            <div class="row">
            
                <div class="col-lg-4">

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                        <?= $form->field($model, 'name') ?>

                        <?= $form->field($model, 'email') ?>

                        <?= $form->field($model, 'phone') ?> 

                        <?= $form->field($model, 'enquery') ?>
                        
                       

                        
                        
                       
                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>

                </div>
                <div class="col-lg-5">
                    <div class="contact_right"><img src="/images/contact.png"></div>
                </div>
            </div>
        </div>
    
</div>

