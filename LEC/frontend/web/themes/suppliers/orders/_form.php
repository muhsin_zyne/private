<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="sales-comments-form">
	<h2>Create Shipment</h2>
    <?php $form = ActiveForm::begin(); $model->orderId=$id;   ?>

    	<?= $form->field($model, 'orderId')->hiddenInput()->label(false); ?>

    	<?= $form->field($model, 'title') ?>

    	<?= $form->field($model, 'carrier')->dropDownList(['carrier A' => 'carrier A', 'carrier b' => 'carrier B', 'carrier c' => 'carrier C']); ?>

    	<?= $form->field($model, 'trackingNumber') ?>

    	<input type="hidden" id="shipitems" name="shipitems" value="">

    	<div class="form-group">
        	<?= Html::submitButton('Ship', array('class' => 'btn btn-primary')) ?>
    	</div>
    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	var orderItems=$(".enabled_checkbox:checked").map(function(){ return $(this).val(); }).get();
	
	$("#shipitems").val(orderItems);
		
	
	});
</script>

