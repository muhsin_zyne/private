<?php
use yii\widgets\DetailView;
use common\models\User;
use common\models\Stores;
use yii\widgets\Pjax;
use common\models\B2bAddresses;
use common\models\OrderItems;
use common\models\Products;
use common\models\Orders;
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use yii\widgets\ActiveForm;
use yii\helpers\url;


use yii\widgets\ListView;
use common\models\OrderComment;
use common\models\SalesComments;

$this->title = 'Order #'.$model->orderId.' | '. $model->orderPlacedDate;
$this->params['breadcrumbs'][] = $this->title;

?>

<?= Html::csrfMetaTags() ?>

<div class="col-xs-12"> 
    <div class="row">
        <div style="float:right; margin-bottom:15px;">
            <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/index'], ['class' => 'btn btn-default back-btn ']) ?>
            <?php  if($model->suppliersOrderStatus=="Acknowledged"){ ?>
                <?= Html::a('<i class="fa fa-check"></i> Acknowledged', ['orders/acknowledge','id'=>$model->orderId], ['class' => 'btn btn-success acknowledge']) ?>
                <?php }else { ?>
                <?= Html::a('<i class="fa fa-hand-o-right"></i> Acknowledge', ['orders/acknowledge','id'=>$model->orderId], ['class' => 'btn btn-primary ','id'=>'acknowledge-btn']) ?>
                  <?= Html::a('<i class="fa fa-check"></i> Acknowledged', ['orders/acknowledge','id'=>$model->orderId], ['class' => 'btn btn-success acknowledge','id'=>'acknowledge-sucsess']) ?>
                <?php } ?>
            <i class="fa fa-question-circle question" data-toggle="tooltip" data-placement="left" title="" data-original-title="When you click this button, the order will automatically be marked as Acknowledged and customer will be notified!."></i>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="box">
<div class="box-body credit-memos-view">
<div class="col-main">
    
    
        <div class="row">
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <i class="fa fa-file-text-o"></i><h3 class="box-title">Order # <?= $model->orderId ?> (the order confirmation email was sent)</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p>Order Date : <?= $model->orderPlacedDate ?></p>
                        <p>Order Status : <?= ucfirst($model->suppliersOrderStatus) ?></p>
                        <p>Order Total : <?= Helper::money($model->getSuppliersOrderTotal(Yii::$app->user->id)) ?></p>
                        <p>Order Type : <?= $model->order_type ?></p>
                        <p>Order Comment : <?= $model->customerNotes ?></p>                       
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i><h3 class="box-title">Account Information</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <p> Store Name : <?= ucfirst($model->store->title) ?></p>
                        <p> Address : <?= ucfirst($model->billingAddress) ?></p>
                        <p> Email : <?= $model->customer->email ?></p>
                        <p> Member number : <?= $model->customer->jenumber ?></p>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    <?php  
        $orderitems=common\models\OrderItems::find()->where(['orderId'=>$model->id,'supplierUid'=>Yii::$app->user->Id])->one();
        //var_dump($orderitems);die;
        //var_dump($orderitems->b2baddress->billingAddress);die;
    ?>
    
    <div class="row">
        <div class="col-md-6">

            <div class="box box-default">

                <div class="box-header with-border">

                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Billing Address</h3>

                </div>

                <div class="box-body">
                <?=(isset($orderitems->b2baddress->billingAddress)) ? $orderitems->b2baddress->billingAddress : 'N/A'?>
                   

                </div>

            </div>

        </div>

        <div class="col-md-6">
            <div class="box box-default">                            
                    <div class="box-header with-border">
                        <i class="fa fa-bullhorn"></i><h3 class="box-title">Shipping Address</h3>
                    </div>
                    <div class="box-body">
                       <?= (isset($orderitems->b2baddress->shippingAddress)) ? $orderitems->b2baddress->shippingAddress : 'N/A'?>
                    </div>                         
            </div>
        </div>
    </div>

    <div class="row">

                    <div class="col-md-12">

                        <div class="box box-default">

                            <div class="box-header with-border">

                                <i class="fa fa-shopping-basket"></i><h3 class="box-title">Items Ordered</h3>

                            </div><!-- /.box-header -->
                            <div class="right-btn-sm">
                                <div class="right-btn-one">
                                <?= Html::a('<i class="fa fa-upload"></i> Export as CSV', ['orders/exportorderitems','id'=>$model->id], 
                                    ['class' => 'btn btn-success export-btn',]) ?> 
                                   <i class="fa fa-question-circle question" data-toggle="tooltip" data-placement="left" title="" data-original-title="Once you export the order, it will automatically be marked as Acknowledged and customer will be notified!."></i>
                                   </div>
                                   <div class="price-wrap">Order Total : <span><?= Helper::money($model->getSuppliersOrderTotal(Yii::$app->user->id)) ?></span></div>
                            </div>
                            <div class="box-body">

                                <?= GridView::widget([

                                    'id' => 'order-items',

                                    'dataProvider' => $dataProvider,

                                    //'filterModel' => $searchModel,

                                    'columns' => [

                                    ['class' => 'yii\grid\SerialColumn'],

                        

                                        //'id',

                                        [

                                            'label'=> 'Item Orderd',

                                            'attribute' => 'id',

                                            'value' => 'product.name'

                                        ],

                                        [

                                            'label' => 'Option(s)',

                                            'value' => 'superAttributeValuesText',

                                            'format' => 'html'

                                        ],

                                        //'productId',

                                        [

                                            'label'=> 'SKU',

                                            'attribute' => 'productId',

                                            'value' => 'sku'

                                        ],
                                        [
                                            'label' => 'To Store',
                                            'attribute' => 'id',
                                            'format' => 'html',
                                            'value' => function ($model) {
                                               return ($model->b2baddress)? $model->b2baddress->storeName : "N/A";
                                            },
                                        ],

                                        //'status',
                                       /* [
                                            'label' => 'Status',
                                            'attribute' => 'status',
                                            'format' => 'html',
                                            'value' => function ($model) {
                                                return ucfirst($model->status);
                                            },
                                        ],
                                        [
                                            'label'=> 'Billing Address',
                                            'attribute' => 'billingAddress',
                                            'value' => 'b2baddress.billingAddress'
                                        ],
                                        [
                                            'label'=> 'Shipping Address',
                                            'attribute' => 'shippingAddress',
                                            'value' => 'b2baddress.shippingAddress'
                                        ], */ 

                                        [

                                            'label' => 'Price',

                                            'attribute' => 'id',

                                            'format' => 'html',

                                            'value' => function ($model) {

                                                return Helper::money($model->price);

                                            },

                                        ],

                                        

                                        [

                                            'label' => 'Qty',

                                            'attribute' => 'id',

                                            'format' => 'html',

                                            'value' => function ($model) {

                                                return $model->quantity;

                                            },

                                        ],

                                        [

                                            'label' => 'Sub Total',

                                            'attribute' => 'id',

                                            'format' => 'html',

                                            'value' => function ($model) {

                                                return Helper::money($model->price * $model->quantity);

                                            },

                                        ],

                                        'comment',
                                    ],

                                ]); ?>

                            </div><!-- /.box-body -->

                        </div>

                    </div>

                </div>


                

</div>

<div class="row">

    <div class="col-md-6">

        <div class="box box-default">

            <div class="box-header with-border">

                <i class="fa fa-comments-o"></i><h3 class="box-title">Comments History</h3>

            </div><!-- /.box-header -->

            <div class="box-body">

                <h3>Add Order Comments</h3>

               <div class="order-comment-form">

                    <?php $form = ActiveForm::begin(['action' =>  Url::to(['orders/comments'])]); 

                    $model_oc= new SalesComments(); 
                    $model_oc->notify=1;
                    $model_oc->typeId=$model->id; $model_oc->type='order';$model_oc->orderId=$model->id; ?> 

                    <?= $form->field($model_oc, 'type')->hiddenInput()->label(false)  ; ?>

                    <?= $form->field($model_oc, 'typeId')->hiddenInput()->label(false)  ; ?>

                     <?= $form->field($model_oc, 'orderId')->hiddenInput()->label(false)  ; ?>

                    <?= $form->field($model_oc, 'comment')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model_oc, 'notify')->checkbox(); ?>

                    <div class="form-group">

                        <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-success']) ?>

                    </div>

                    <!--<?= $form->field($model_oc, 'notify')->checkbox(array('label'=>'Notify Customer by Email')); ?>-->

                    <?php ActiveForm::end(); ?>

                    

                </div> 

            </div><!-- /.box-body -->

        </div><!-- /.box -->

    </div>
    <div class="col-md-6">

        <div class="box box-default">

            <div class="box-header with-border">

                <i class="fa fa-comments-o"></i><h3 class="box-title">Comments History</h3>

            </div><!-- /.box-header -->

            <div class="box-body">

               <?= ListView::widget([
                'dataProvider' => $dataProvider_salescomments,
                //'filterModel' => $searchModel_ordercomment,
                'itemView' => '@app/views/orders/_listview',
                //'itemView' => '_listview',
            ]); ?>

            </div><!-- /.box-body -->

        </div><!-- /.box -->

    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#acknowledge-sucsess').hide();
        $('.export-btn').click(function(){
           //setTimeout(function(){ location.reload() }, 5000);
            $('#acknowledge-btn').hide();
            $('#acknowledge-sucsess').show();
           $(".acknowledge").attr("disabled", true);

        })
    })
</script>
<script>
$(document).ready(function(){
    <?php if($model->suppliersOrderStatus=="Acknowledged"){ ?>
        $(".acknowledge").attr("disabled", true);
    <?php } ?>   
});
  
</script>

