<?php

use yii\helpers\Html;
use common\models\Shipments;


/* @var $this yii\web\View */
/* @var $model common\models\Shipments */
$model = new Shipments;
$this->title = 'Create Shipments';
$this->params['breadcrumbs'][] = ['label' => 'Shipments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipments-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
