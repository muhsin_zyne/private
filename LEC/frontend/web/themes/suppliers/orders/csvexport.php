<?php

use common\models\Products;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Orders;
use common\models\OrderItems;
//var_dump(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]);die;
if(isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]) && Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]!='')
{
    //die('hiii');
    $start_date = date(date('Y-m-d 00:00:00', strtotime(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"])));
}
else

    $start_date = '1969-12-31 00:00:00';

if(isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"]) && Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"]!='')
    $end_date = date(date('Y-m-d 23:59:59', strtotime(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"])));
else
    $end_date = date('Y-m-d H:i:s');
//var_dump($end_date);die;

 if (isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]) && isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"])) {
    $items = ArrayHelper::map(OrderItems::find()->join('JOIN','Orders as o','orderId=o.id')->where('o.type="b2b" AND o.orderDate BETWEEN "'.$start_date.'" AND "'.$end_date.'"')->all(),'id','self');

}

else{
    $items = ArrayHelper::map(OrderItems::find()->join('JOIN','Orders as o','orderId=o.id')->where('o.type="b2b"')->all(),'id','self');
}

//var_dump($items);die;

$suid=Yii::$app->user->Id;
$output = "";
$condition="";
//var_dump(Yii::$app->request->queryParams);die;
//var_dump($start_date);die;
$connection = Yii::$app->getDb();

$condition="AND Orders.orderDate BETWEEN '$start_date' AND '$end_date'";

if(isset(Yii::$app->request->queryParams["OrdersSearch"]["id"]) && Yii::$app->request->queryParams["OrdersSearch"]["id"]!='' ){
    $id=Yii::$app->request->queryParams["OrdersSearch"]["id"];
    $condition=$condition."AND Orders.id='$id'";
}
//var_dump($condition);die;
if(isset(Yii::$app->request->queryParams["OrdersSearch"]["storeId"]) && Yii::$app->request->queryParams["OrdersSearch"]["storeId"]!=''){
    $storeId=Yii::$app->request->queryParams["OrdersSearch"]["storeId"];
    $condition=$condition."AND Orders.storeId='$storeId'";
}
/*if(isset(Yii::$app->request->queryParams["OrdersSearch"]["grandTotal"])){
    $grandTotal=Yii::$app->request->queryParams["OrdersSearch"]["grandTotal"];
    $condition=$condition."AND Orders.grandTotal='$grandTotal'";
}*/

//$condition="AND Orders.orderDate BETWEEN '$start_date' AND '$end_date'";
//var_dump($condition);die;
$command = $connection->createCommand("SELECT user.jenumber,OrderItems.id,OrderItems.orderId,Orders.storeId, Orders.orderDate,Orders.customerNotes,Orders.grandTotal,B2bAddresses.storeName,AttributeValues.value,Products.sku,Products.typeId,OrderItems.price,OrderItems.conferenceId,OrderItems.quantity,OrderItems.comment, supplier.firstname FROM OrderItems 
        INNER JOIN Orders ON Orders.id=OrderItems.orderId AND Orders.type='b2b'
        INNER JOIN B2bAddresses ON B2bAddresses.id=OrderItems.storeAddressId 
        INNER JOIN Products ON Products.id=OrderItems.productId 
        INNER JOIN Attributes ON Attributes.code='name'
        INNER JOIN Users as supplier ON supplier.id= OrderItems.supplierUid
        INNER JOIN Users as user ON user.id= Orders.customerId 
        INNER JOIN AttributeValues ON AttributeValues.productId=OrderItems.productId 
        WHERE AttributeValues.attributeId=Attributes.id AND Orders.type='b2b' AND OrderItems.supplierUid='$suid' $condition
        ORDER BY OrderItems.OrderId
    ");




$result = $command->queryAll();
$heder=array("Order Id", "Date", "Store Name","JE Number","Product Name", "SKU", "Price", "Quantity","Options","Supplier Name","Conference Product","Order Item Comment","Order Total","Order Comment");
foreach ($heder as  $value) {
    $heading    =   $value;
    $output     .= '"'.$heading.'",';
}
$output .="\n";

$f=0;

foreach($result as $key=>$value)
    { 
        if($value['orderId'] != $f)
            {
                if($value['conferenceId'] != NULL)
                    $conference = "Yes"; 
                else
                    $conference = "No";

                $output .='"'.$value['orderId'].'","'.$value['orderDate'].'","'.$value['storeName'].'","'.$value['jenumber'].'","","","","","","","","","'.$value['grandTotal'].'","'.$value['customerNotes'].'"';
                $output .="\n";
                $output .= '"","","","","'.$value['value'].'","'.$items[$value['id']]->sku.'","'.$value['price'].'","'.$value['quantity'].'","'.strip_tags($items[$value['id']]->superAttributeValuesText).'","'.$value['firstname'].'","'.$conference.'","'.$value['comment'].'",""';
                $f=$value['orderId'];
            }

            else{
                $output .= '"","","","","'.$value['value'].'","'.$items[$value['id']]->sku.'","'.$value['price'].'","'.$value['quantity'].'","'.strip_tags($items[$value['id']]->superAttributeValuesText).'","'.$value['firstname'].'","'.$conference.'","'.$value['comment'].'",""';
            }
            $output .="\n";
    }       

$filename ='Suppliers_Order_'.date("Y-m-d").'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
echo $output;
exit;
    
?>
