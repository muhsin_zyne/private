<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\export\ExportMenu;
use frontend\components\Helper;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
$sid=yii::$app->user->id;
$stores=common\models\Stores::find()->all();
$listData=ArrayHelper::map($stores,'id','title');
?>
<div class="orders-index">
<div class="box">
<div class="box-body">


   
    <?php
        $form = \kartik\form\ActiveForm::begin(['id' => 'b2borders-form']);
        \kartik\form\ActiveForm::end();
    ?>
    <?= Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['orders/export'], Yii::$app->request->queryParams)), ['class'=>'btn btn-success pull-right right-up']); ?>
        
  

    <!--<?= Html::a('<i class="fa fa-upload"></i> Export as CSV', ['orders/export'], ['class' => 'btn btn-success pull-right right-up ']) ?>-->
      
         <?= \app\components\AdminGridView::widget([
            'id' => 'products-orders',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                //'orderDate',
                //'userJenumber',
                
                /* [
                    'label' => 'Member #',
                    //'attribute' => 'userJenumber',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->user->jenumber;
                    },
                ],*/
              
                
                
                [
                    'label' => 'Order Date',
                    'attribute' => 'id',
                    'value' => function($model){ return \frontend\components\Helper::date($model->orderDate); },
                    'filter' => \kartik\field\FieldRange::widget([
                        'form' => $form,
                        'model' => $searchModel,
                        'template' => '{widget}{error}',                            
                        'attribute1' => 'orderDate_start',
                        'attribute2' => 'orderDate_end',
                        'type' => \kartik\field\FieldRange::INPUT_DATE,
                        // 'saveFormat'=>'U',
                                    
                    ])
                ],
                [
                    'label' => 'MEMBER NUMBER',
                    'attribute' => 'customer.jenumber',
                ],
                [
                    'label' => 'Store Name',
                    'attribute' => 'storeId',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->store->title;
                    },
                    'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'storeId', $listData,
                    ['class'=>'form-control','prompt' => '']),
                ],
                [
                    'label' => 'Number Of Items',
                    //'attribute' => 'id',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model->orderItemsCount;
                    },
                ],
                
                [
                    'label' => 'Grand Total',
                    //'attribute' => 'grandTotal',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money($model->getSuppliersOrderTotal(Yii::$app->user->id));
                    },
                ],
                [
                    'label' => 'Status',
                    'attribute' => 'suppliersOrderStatus',
                    'format' => 'html',
                    'value' => 'suppliersOrderStatus',
                   
                ],
                //'status',
                //'suppliersOrderStatus',
                /*[
                    'label' => 'Status',
                    'attribute' => 'status',
                    'value' => function($model, $attribute){ 
                        return $model->status;
                    },
                    'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['pending' => 'Pending',
                        'complete' => 'Complete', 'acknowledge' => 'Acknowledge',],
                        ['class'=>'form-control','prompt' => 'All']),
                ],*/

                //'grandTotalFormatted',
                //'storeName',
                // [
                //     'label'=> 'Store Name',
                //     'attribute' => 'storeId',
                //     'value' => 'store.title'
                // ],
                // [
                //     'label' => 'Status',
                //     //'format' => 'raw',
                //     'value' => function ($model) { 
                //         return $model->getOrderStatus();
                //     }, 
                // ],
                [
                    'label'=> 'View',
                    'format' => 'html',
                    'value' => function($model){
                        return Html::a('View',['orders/view','id' => $model->id]);
                    }
                ],
            ],
        ]); ?>
    
    </div>
    </div>
       
</div>
