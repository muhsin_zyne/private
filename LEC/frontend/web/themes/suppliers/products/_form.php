<?php
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;
use common\models\OrderComment;
use common\models\B2bAddresses;
use common\models\SalesComments;
use common\models\Products;
use kartik\file\FileInput;
use yii\web\UploadedFile;

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;


//var_dump($productImages);die();

?>

<div class="box">
<div class="categories-index">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs ">
            <li class="active"><a data-toggle="tab" href="#prices">Prices</a></li>
            <li id="m_title" ><a data-toggle="tab" href="#description">Description</a></li>
            <li id="m_title" ><a data-toggle="tab" href="#images">Images</a></li>
           
        </ul>
    </div> 
</div> 
<div class="box-body">
<div class="col-xs-12">
    <?php $form = ActiveForm::begin(['id' => 'products-form', 'options' => ['enctype'=>'multipart/form-data']]); ?>
    <div class="tab-content responsive">
        <div class="tab-pane active" id="prices">
                <?= $form->field($model, 'cost_price')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'sellingPrice')->textInput(['maxlength' => 255]) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div> 
            
        </div>
        <div class="tab-pane" id="description">
           
                <?= $form->field($model, 'description')->textarea(array('rows'=>4,'cols'=>5)); ?>
                <?= $form->field($model, 'short_description')->textarea(array('rows'=>4,'cols'=>5)); ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div> 
           
        </div>
        <div class="tab-pane" id="images">
            <div class="deleted_images"></div>
            <div class="input_fields_wrap" id="input_fields">
                <h3> Ideal image dimension - 600 px X 600 px.</h3> 
                <div class="add_prod_images">   
                    <?=Html::activeInput('file',$model, 'images',$options = ['class'=>'file','id'=>'file-1','multiple'=>'true','data-overwrite-initial'=>'false','data-min-file-count'=>'1']) ?>
                </div> 
                <h4>Once you browse and select your image, please click on the 'Upload' button </h4> 
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="clearfix"></div>
</div>
</div>
<script>

    //var randomValue = randomString(10);
    var randomValue = 1;
    var url = "<?=\yii\helpers\Url::to(['product-images/upload'])?>";
    var key = 0;

    $("#file-1").fileinput({ 
        uploadUrl: url+'?rand='+randomValue, 
        allowedFileExtensions : ['jpg', 'png','gif'],
        overwriteInitial: false,
        uploadAsync: true,
        maxFileSize: 20000,
        maxFilesNum: 3,
        'showPreview' : true,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) { 
            filename = randomValue+'_'+filename;
            /*$('.tab-content').find('input').iCheck({radioClass: 'iradio_minimal checked'});
            $('.tab-content').find('input').iCheck({checkBoxClass: 'icheckbox_minimal'});*/

            $('.tab-content').iCheck({checkboxClass: 'icheckbox_minimal',radioClass: 'iradio_minimal'});

            <?php if(!$model->isNewRecord) {  ?>
                len = $('.file-preview-frame').length;
                //console.log('before:'+len);
                if(len != 1 || len != 0){ //alert('after');
                    key = $('.base_image:last').val();
                    //console.log(key);
                    $('.file-preview-frame').attr('data-fileindex',(parseInt(key)+1));
                }
                else{
                    $('.file-preview-frame').attr('data-fileindex','0');
                }   
            <?php } ?>
            return filename.replace(')', ')').replace(']', '_');
        }
    });


//});
    
</script> 

<script type="text/javascript">
    $(document).ready(function() {

        var key = 0;
        <?php 
            if(isset($productImages)) {   //var_dump($productImages);die();
                    foreach($productImages as $image) {
            ?>
            
        var imageurl = "<?=$thumbImageUrl."/".Products::getImagePathCaseCorrected($image['path'])?>";
        
        $('.file-drop-zone table tbody').append('<tr data-imageid="<?=$image["id"]?>" data-imagename="<?=$image["path"]?>"><td><img src="'+imageurl+'"></td><td><input type="text" name="Products[images][old]['+key+'][sort_order]" id="sort_order" value="<?=$image["sortOrder"]?>"></td><td><input type="radio" name="Products[images][base_image]" class="base_image" value="'+key+'" <?=(($image["isBase"]=="1") ? "checked" : "")?>></td><td><input type="radio" name="Products[images][small_image]" class="small_image" value="'+key+'" <?=(($image["isSmall"]=="1") ? "checked" : "")?>></td><td><input type="radio" name="Products[images][thumbnail]" class="thumbnail" value="'+key+'" <?=(($image["isThumbnail"]=="1") ? "checked" : "")?>></td><td><input type="checkbox" name="Products[images][old]['+key+'][exclude]" id="exclude" value="<?=(($image["exclude"]=="1") ? "1" : "0")?>" <?=(($image["exclude"]=="1") ? "checked" : "")?>></td><td><a href="#" class="remove">X</a></td><input type="hidden" class="image-old" name="Products[images][old]['+key+'][imageId]" value="<?=$image["id"]?>"></tr>');

        key = key+1;
        
        <?php }  }  ?> 

    }); 
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var key=1;
        $(".file-preview").on("click",".remove", function(e){
            <?php if(!$model->isNewRecord) { ?>
                e.preventDefault();
                var imageid = $(this).closest('tr').attr('data-imageid');
                var imagename = $(this).closest('tr').attr('data-imagename');
                $('.deleted_images').append('<input type="hidden" name="Products[deletedImages]['+key+'][id]" id="deletedImages" value="'+imageid+'"/><input type="hidden" name="Products[deletedImages]['+key+'][name]" id="deletedImages" value="'+imagename+'"/>');  
                $(this).closest('tr').remove();
                key = key+1;
            <?php } else {?>    
                e.preventDefault(); 
                $(this).closest('tr').remove();
            <?php } ?>
        }); 
        $('option[checked]').attr("selected", "selected").removeAttr("checked");

        $('.field-products-price label').append('<span style="color:#bbbbbb;font-size:12px;  font-weight: normal; margin-left:15px; font-style:italic;">Price changes of products which are in active promotions/catalogues will not take effect until the promotion expires.</span>');
       
    }); 
</script>    

<script type="text/javascript">
    $(document).ready(function() {
        var count = 0;
        var pendingUpload = false;
        $('#products-form').on('beforeSubmit', function() {
            $.each( $(".file-preview-frame"), function( key, value ) {
                console.log($(this).attr('data-fileindex'));
                if($(this).attr('data-fileindex') != "-1"){
                    pendingUpload = true;
                    return;
                    // $('.kv-fileinput-upload').click();
                    // return;
                }
                else{
                    pendingUpload =false;
                }
            });
            if(pendingUpload){
                $('a[href="#images"]').click();
                if(confirm('You have one or more images pending to be uploaded. Do you want to continue without saving them?')){
                    $.each( $(".file-preview-frame"), function( key, value ) {
                        if($(this).attr('data-fileindex') != "-1"){
                            $(this).remove();
                        }
                    });
                    return true;
                }else{
                    return false;
                }
            }
        });
    });
</script>
