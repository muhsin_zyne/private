<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Stores;
use yii\helpers\url;




/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Products */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-body">
        <div class="products-index">
            <?php \yii\widgets\Pjax::begin(['id' => 'suppliers-products']); ?>
            <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'main-banner-form']] ); ?>   
            <div class="right-align">
                     
                <select name="enabledisable">
                    <option value="1">Enable</option>
                    <option value="0">Disable</option>
                </select>            
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success','id'=>'product-ed']) ?>        
            </div>
    
    
    
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <!--<?= Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['products/export'], Yii::$app->request->queryParams)), ['class'=>'btn btn-success pull-right right-up']); ?>-->
            <?= \app\components\AdminGridView::widget([

                'id' => 'suppliers-products',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    
                    [
                        'format' => 'raw',
                        'value' => function ($model) { 
                            
                            return '<input type="checkBox" class="enabled_checkbox"  id="'.$model->id.'"  name="Products[]" value="'.$model->id.'">' ;
                        },
                    ],
                    'sku',
                    //'ezcode',
                    
                    //'brandName',
                    [
                        'label'=> 'Brand',
                        'attribute' => 'brandName',
                        'value' => 'brandName'
                    ],
                    'name',
                    // [
                    //     'label'=> 'SKU',
                    //     'attribute' => 'id',
                    //     'value' => 'sku'
                    // ],
                    //'sku',
                    //'price',

                    [
                        'label' => 'Cost',
                        'attribute' => 'cost_price',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Helper::money($model->cost_price);
                        },
                    ],
                    [
                        'label' => 'Retail Price',
                        'attribute' => 'price',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Helper::money($model->sellingPrice);
                        },
                    ],
                    //'status',
                   
                    'status' => [
                        'label' => 'Status',
                        'value' => function($model, $attribute){ return $model->status=="1"? "Enabled" : "Disabled"; },
                        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Disabled', '1' => 'Enabled'],['class'=>'form-control','prompt' => '']),
                    ],
                    [
                        'label' => 'Action',
                       
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a('<i class="fa fa-pencil"></i>', ['/suppliers/products/update', 'id' => $model->id],['class'=>'','data-title' => 'Update Menu',]).' '.
                            Html::a('<i class="fa fa-eye"></i>', ['/suppliers/products/view', 'id' => $model->id],['class'=>'ajax-update sm-dialog','data-title' =>'',]);
                                                                                            
                        },
                    ], 
                    //'cost_price',
                    
                    
                ],
            ]); ?>


            <?php ActiveForm::end(); ?> 
            <?php \yii\widgets\Pjax::end(); ?>   
        </div>
    </div>
</div>

<script>

$(document).on('ready pjax:success', function() {
   $("#product-ed").attr("disabled", true);
    $(".enabled_checkbox").click(function(){
        var orderitems = $(".enabled_checkbox:checked").map(function(){return $(this).val();}).get();
        if(orderitems!='')
        {
            $("#product-ed").attr("disabled", false);
        }
        else
        {
            $("#product-ed").attr("disabled", true);
        }
    });
});
</script>

