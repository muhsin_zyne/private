<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CmsPages */

?>
<div class="cms-pages-update">

    <?= $this->render('/products/_form', compact('model','productImages','thumbImageUrl')) ?>

</div>
