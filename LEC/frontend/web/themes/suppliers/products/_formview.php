<?php
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;
use common\models\OrderComment;
use common\models\B2bAddresses;
use common\models\SalesComments;

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
//var_dump($model->invoiceStatus);
?>

    <div class="row">
        <div class="col-xs-12">
           <div class="mod-head">
		   <?=$model->name; ?></div>             
        </div>
        <div class="col-xs-5">
          <div class="mod-img">
              <?php $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/small/".$model->getSmallImage();?>
              <img src="<?= $thumbImagePath?>" >
            </div>
            
            <div class="mod-ds">
               <span >Description</span> <br/>
               <?=$model->description?>
               
            </div>
            
        </div>

        <div class="col-xs-7 sm-tble">
            <div class="product_detail">
                <div class="right-hd">Product Details <div class="price-right"> Cost Price: <span><?=frontend\components\Helper::money($model->cost_price)?></span> </div></div>
                <?php 
                    $array_status =array(['label'=>'Status', 'value'=>($model->status==1)?'Enabled':'Disabled']);
                    $finalDetailViewAttributes=array_merge($model->detailViewAttributes,$array_status);
                 ?>              
                <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => $finalDetailViewAttributes,
                    ]);
                ?>


                             
            </div>
        </div> 
    </div>
    
        
            
            
