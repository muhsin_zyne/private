<?php
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	use yii\widgets\ListView;
	use yii\widgets\ActiveForm;
	use frontend\components\Helper;
	use yii\helpers\Url;
	use backend\assets\AppAsset;
	use common\models\Stores;
	use common\models\Banners;
	use common\models\Products;
	use yii\widgets\Breadcrumbs;
	use kartik\rating\StarRating;
	//use yii\widgets\ListView;
?>
<div class="col-xs-12 newproducts">                
    <ul class="nav nav-tabs" role="tablist">
        <li class="tab-head">NEW PRODUCTS</li>                        
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">                        
        <div role="tabpanel" class="tab-pane active"  activeid="featuredproducts">
            <div class="products-container">
                <div class="row">
                    <?php 
                        $featuredProducts->pagination = [
                            'defaultPageSize' => 4,
                        ]; 
                        echo ListView::widget([
                            'id' => 'homepage-products',
                            'summary'=>'', 
                            'dataProvider' => $featuredProducts,
                            'itemView' => '/products/_productlist',
                            'summary'=>'',
                            'layout' => "{items}", 
                            'itemOptions' => ['class' => 'items_featured col-xs-3']
                        ]);
                         echo "<script> $('.rating-star').rating({disabled: true, showClear: false, showCaption: false});</script>";
                    ?> 
                    
                </div>
            </div>
        </div>
    </div>
</div>