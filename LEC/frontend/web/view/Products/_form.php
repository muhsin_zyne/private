<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'typeId')->dropDownList([ 'simple' => 'Simple', 'bundle' => 'Bundle', 'grouped' => 'Grouped', 'configurable' => 'Configurable', 'virtual' => 'Virtual', 'downloadable' => 'Downloadable', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'attributeSetId')->textInput() ?>

    <?= $form->field($model, 'parent')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
