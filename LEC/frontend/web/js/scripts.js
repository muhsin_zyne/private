$(document).ready(function(){

    //$(".megamenu").css('display','block');
    //$(".catmenu > ul").css("display","block");

    $(document).on("pjax:beforeSend", function(event) {
        if($('.product_list').length >0){
            $('.product_list').html('<div style=\"text-align: center;\"><img src=\"/images/loading_filter.gif\"/></div>')
        }
    });
    $(document).on('ready pjax:success', function() {
        if($(this).hasClass('rating-star')){ 
            $(".rating-star").rating({disabled: true, showClear: false, showCaption: false});
            $('ul.pagination').wrap('<div class=\'col-xs-12 text-center\'></div>');
        }    
    });
	$('.chkbx').click(function(){
		if($(this).is(':checked')){
			$(this).parents('.items').find('.vsamll').val('1');
		}
	})
	$(window).keydown(function(event){
        if(event.keyCode == 13) {
        	$( "#pi_search" ).keypress(function() {
        		//alert('poii');
        		$("#p_search").click();        	            
            	event.preventDefault();
            	return false;
        	});
        }
    });

    var count = 0;
    var error = 0;
    var url;

    $('body').on('click', '.pagination a', function(){  
    	var url = $(this).attr('href');
    	$('#continue').closest('a').attr("href", url);
        $('.product_list').append('<input type="hidden" name="tourl" value="'+url+'">');
    //$(".pagination a").click(function() { alert('clicked');return false;
        $(".chkbx").each(function() {
            if($(this).is(':checked')){
                count = 1; 
            }
        });    
        if(count==1){
        	
        	$("#dialog" ).dialog({
            height: 300,
            width: 350,
            modal: true,
            resizable: true,
            dialogClass: 'no-close success-dialog cart-dialog'
            });
            count = 0;
            error=0;
            return false;
            
        }  
        
        if(error>0){  
            error = 0;
            return false;
        }
    }); 

    $("#cancel").click(function() {
            $('#dialog').dialog('close'); 
            return false;
        });
        $("#add_tocart").click(function() {
            $('#list_form_pro').submit();
        });

    $('.alert').fadeOut(5000);   
    $('body').on('change', '.addto input', function(){
        $(".vsamll").each(function() {
            if($(this).val() > 0){
                $(this).parents('.items').find('.chkbx').prop('checked',true);
            }
            else{
                $(this).parents('.items').find('.chkbx').prop('checked',false);
            }
        });
    })
    $('body').on('click', '.order_now', function(){    
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required');
          return false;
        }
        if($('.chkbx:checked').length < 1 && $('.chkbx').length > 0){
            alert('Please tick atleast one product to order');
            return false;
        }  

    });
    
    $('body').on('change', '.chkbx', function(){     
        if(this.checked) {
            $('.productsids').val($(this).val());
            $(this).parents('.items').find('.vsamll').val('1');
        }

        if(!$(this).is(':checked')){
            $(this).parents('.items').find('.vsamll').val('');
        }
    });

    $('body').on('keypress', '.only-numeric', function(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        }
        return true;
    });    

    var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);

    var sideurl = window.location.href.substr(window.location.href.lastIndexOf("au")+2);

    //console.log(sideurl);
	
	
	
            $(".navbar-left li a").each(function(){
                  if($(this).attr("href") == "/"+pgurl){   
                    $('.round-home a').removeClass('active');
                    $(this).addClass("active");
                  }
                  if(pgurl == "index" || pgurl.length == 0)  {
                    $('.round-home a').addClass('active');
                  }
            });

            $(".m-account-menu li a").each(function(){  //console.log($(this).attr("href"));
                if($(this).attr("href") == sideurl){   
                    $(this).addClass("active");
                }
            });
//});
//});       
            


    $('body').on('click', '.add-to-cart', function(){ //console.log($('form#product_cart_form_'+$(this).attr("id"))); return false;
        $('form#product_cart_form_'+$(this).attr("id")).submit();
    })
});

function setConfig(pid){
    $('form#list_form_pro').append('<input type="hidden" name="Products['+pid+'][config]" value="'+$('.product-settings-form form').serialize()+'"/>');
    $("#ajax-update").dialog("close");
    throw new Error('That\'s all we know');
}

function showAsFloat(n){
    return parseFloat(n).toFixed(2);
    // if(n == 0)
    //     return 0.00;
    // return !Number(n) ? n : (Number(n)%1 === 0 ? Number(n).toFixed(2) : n);
}

function sleep(milliseconds){
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++){
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}
 function totopslow()
{
    window.scrollTo(0, 0);
    //jQuery('html, body').animate({scrollTop:0}, 'slow');
}
