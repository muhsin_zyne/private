<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\Products;
use common\models\UserAddresses;
use common\models\Orders;
use common\models\OrderItems;
use common\models\ProductsSearch;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\User;
use common\models\WishlistItems;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\FrontController;
use yii\data\ActiveDataProvider;
use common\models\Configuration;
use common\models\B2cSignupRequests;
use yii\web\UploadedFile;
use bryglen\braintree\braintree;
use yii\db\Query;
use common\models\Stores;
use common\models\B2bAddresses;
use yii\db\ActiveQuery;
use common\models\Brands;
use common\models\Categories;
use common\models\ChangePasswordForm;
use frontend\models\TakeaphotoForm;
use frontend\components\Helper;
use yii\db\Expression;
use frontend\components\eway;

/**
 * Site controller
 */

/* yfyftdc hctd5dc gr5sdcf6f5c bf5dws4sdcv gfeyhiv  fsdgrtvyh */


class SiteController extends FrontController
{
    /**
     * @inheritdoc
     */



    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','login'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                     
                    [
                        'actions' => ['logout', 'test', 'signup','login'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [ 
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
		'offset' => -5,
            ],
        ];
    }

    public function actionIndex()
    {        //var_dump(get_class(Yii::$app->controller));die();    

        $store=Stores::findOne(Yii::$app->params['storeId']);
        $dataProvider = new ActiveDataProvider([
            'query' => Products::find()->orderBy('rand()')->limit(3),
            'pagination' => false,
        ]);
        //var_dump($store->getFeatured());die;
        $bestSellers = new ActiveDataProvider([ 
            'query' => Products::find()->andWhere(['id' => $store->getBestSellers()]),
            'pagination' => false,
        ]);
        //var_dump($store->getFeatured());die();

//        $featuredProducts = new ActiveDataProvider([
//            'query' => Products::find()->andWhere(['id' => $store->getFeatured()]),
//            'pagination' => false,
//        ]);


        /*$featuredProducts = new ActiveDataProvider([ 
            'query' => Products::find()->join()
        ]);*/

        //var_dump($store->getFeatured());die();
        
        /* $latestProducts = Products::find()
                //->join('JOIN','ProductImages as pi', 'pi.productId = Products.id')
                ->andWhere(['not in','typeId',array('gift-voucher')])
                //->andWhere('pi.paths != NULL')
                ->orderBy(['dateAdded' => SORT_DESC])
                ->limit(9)
                ->all(); 

        $bestsellersImages = Products::find()
                ->andWhere(['not in','typeId',array('gift-voucher')])
                ->orderBy(['dateAdded' => SORT_DESC])
                ->limit(16)
                ->all();       

       */
       $store = Stores::findOne(Yii::$app->params['storeId']);
      //        $featured=$store->getFeatured();
        
        $featured=$store->getFeatured()->orderBy(new Expression('rand()'));        
//        $dataProvider =new ActiveDataProvider([
//            'query' => $featured,
//            'pagination' => false,
//        ]);   
        
        $featuredProducts =new ActiveDataProvider([
            'query' => $featured,
            'pagination' => false,
        ]);
 
        $query = Brands::find(); //store brands slider  * aaron 09-june-2016 * 
        $storeId = Yii::$app->params['storeId']; 
        $storebrands = $query
            ->from('Brands')
            ->join('JOIN',
                'StoreBrands as sb',
                'sb.brandId = Brands.id'
            )
            ->groupBy('Brands.id')
            ->andWhere("sb.enabled = 1 and sb.storeId = $storeId and sb.type = 'b2c' and Brands.isVirtual = 0 and Brands.path is not null")
            ->orderBy("Brands.title");
        $parentCategories=Categories::find()->all();
        //------------- store brands------------------ 
         $brands =new ActiveDataProvider([
            'query' => $storebrands->andWhere('path!=""'),
            'pagination' => false,
        ]); 
        //$brands=Brands::find()->where(['enabled'=>1])->all();   
        //--------------------------------------------- banner management --------------------------------------------------------------
        $query = new Query;  // main banner
        $query->select(['StoreBanners.bannerId AS sbid'])->orderBy(['StoreBanners.position' => SORT_ASC])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2c-main' and StoreBanners.storeId = ".Yii::$app->params['storeId']."");
        $command = $query->createCommand();
        $main_banner = $command->queryAll();   

        $query = new Query; // mini banner
        $query  ->select(['StoreBanners.bannerId AS sbid' ] )->orderBy(['rand()' => SORT_DESC])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2c-mini' and StoreBanners.storeId = ".Yii::$app->params['storeId']."")->LIMIT(3);;
        $command = $query->createCommand();
        $mini_banner = $command->queryAll();  

        $query = new Query; //side banner
        $query  ->select(['StoreBanners.bannerId AS sbid' ] )->orderBy(['rand()' => SORT_DESC])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2c-side' and StoreBanners.storeId = ".Yii::$app->params['storeId']."")->LIMIT(1);
        $command = $query->createCommand();
        $side_banner = $command->queryAll();

        $query = new Query; //strip banner
        $query  ->select(['StoreBanners.bannerId AS sbid' ] )->orderBy(['rand()' => SORT_DESC])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2c-strip' and StoreBanners.storeId = ".Yii::$app->params['storeId']."")->LIMIT(1);
        $command = $query->createCommand();
        $strip_banner = $command->queryAll();


        $query = new Query; //mobile banner
        $query  ->select(['StoreBanners.bannerId AS sbid' ] )->orderBy(['rand()' => SORT_DESC])->where(['storeId' => Yii::$app->params['storeId']])->from('StoreBanners')
        ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2c-mobile' and StoreBanners.storeId = ".Yii::$app->params['storeId']."")->LIMIT(6);

        //$query  ->select('id')->from('Banners')->where("type ='b2c-mobile'")->LIMIT(6);

        $command = $query->createCommand();
        $mobile_banner = $command->queryAll();
        
        //--------------------------------------------- banner management  end--------------------------------------------------------------
        return $this->render('index',compact('latestProducts','dataProvider','bestSellers','featuredProducts','main_banner','mini_banner','side_banner','brands','parentCategories','strip_banner','mobile_banner'));
    }

    public function actionLogin()
    {   //die('ksjdfhsei');
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if(isset($_POST['context'])){ 
                //var_dump(\yii\helpers\Url::to([$_POST['context']."/index"])); die;
                return $this->redirect(\yii\helpers\Url::to(["checkout/index"]));
            }
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {   
        $store = Stores::findOne(Yii::$app->params['storeId']);
        $addresses = B2bAddresses::find()->where(['ownerId'=>$store->adminId])->all();
        if(!$defaultAddress = B2bAddresses::find()->where(['ownerId'=>$store->adminId])->one()){
            $defaultAddress = "";
        }
        if(count($addresses) > 1){
            $addressFormap = B2bAddresses::find()->where(['ownerId'=>$store->adminId])->andWhere('longitude!="" and latitude!=""')->one();
            $billingAddress = $addressFormap->longitude;
            //$shippingAddress = $addressFormap->latitude;
        }    

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {  
            //var_dump(Yii::$app->request->post());die;
            if(isset(Yii::$app->request->post()['ContactForm']['storeAddressId'])){
                if ($model->sendEmail(Yii::$app->params['storeId'],Yii::$app->request->post()['ContactForm']['storeAddressId'])) 
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            }
            else{ //var_dump('hiii');die;
                if ($model->sendEmail(Yii::$app->params['storeId'])) 
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            }    
            return $this->refresh();
        }
        //else{ var_dump($model->geterrors());die;}
        if(count($addresses) > 1){
            return $this->render('contact', compact('addresses','model','addressFormap','defaultAddress'));
        }
        else{
            return $this->render('contact', compact('addresses','model','defaultAddress'));
        }    
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionDashboard()
    {  
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
        if ($user->load(Yii::$app->request->post())) {  //var_dump(Yii::$app->request->post());die();       
            if (!User::find()->where(['id'=>$user->id, 'email'=>$user->email])->one()) {        
                $user->firstname = $_POST['User']['firstname'];     
                $user->lastname = $_POST['User']['lastname'];       
                $user->email = $_POST['User']['email'];     
                $user->save(false);     
            }       
            if(!empty($_POST['User']['newPassword'])) {     
                if($user->validatePassword($_POST['User']["currentPassword"])) {        
                    $user->setPassword($_POST['User']["newPassword"]);      
                    $user->save(false);     
                }       
            }      
        } 

        $defaultBillingAddress = $user->defaultB2cBillingAddress;
        $defaultShippingAddress = $user->defaultB2cShippingAddress;

        $dataProvider = new ActiveDataProvider([
            'query' => WishListItems::find()->where(['userId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC,]),
        ]); 

        // $latest = Orders::find()->where(['customerId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC])->limit(4);    

        // $latestOrders = new ActiveDataProvider([
        //     'query' => $latest,
        // ]);

        $latestOrders =new ActiveDataProvider([
            'query' => Orders::find()->where(['customerId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC])->limit(5),
            'pagination' => false,
        ]);

        $orders = new ActiveDataProvider([
            'query' => Orders::find()->where(['customerId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC,]),
        ]);    

        return $this->render('myaccount',compact('user','defaultBillingAddress','defaultShippingAddress','dataProvider','tab','orders','latestOrders','additionalAddresses'));
        
    }

    public function actionAccount(){ 
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
        $changePassword = new ChangePasswordForm;

        if($changePassword->load(Yii::$app->request->post())){ 
            if($_POST['User']['applyChange'] == 1){
                if($changePassword->validate()){
                    $user->password_hash = $user->setPassword($_POST['ChangePasswordForm']['newpass']);
                    if($user->save(false)){
                        Yii::$app->getSession()->setFlash('success','Password changed');
                        return $this->redirect(['account']);
                    }else{ 
                        Yii::$app->getSession()->setFlash('error','Password not changed');
                        return $this->redirect(['account']);
                    }
                }
                else{
                    $error = 1;
                    //Yii::$app->getSession()->setFlash('error','An issue occured. Please check the details.');
                    return $this->render('accountinfo',compact('user','changePassword','error'));
                }    
            }
        }
        if($user->load(Yii::$app->request->post())){
            if($user->save(false)){
                Yii::$app->getSession()->setFlash('success','Details Updated Successfully');
                return $this->redirect(['account']);
            }
        }    

        return $this->render('accountinfo',compact('user','changePassword'));
    }

    public function actionAddressbook(){
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
        $defaultBillingAddress = $user->defaultB2cBillingAddress;
        $defaultShippingAddress = $user->defaultB2cShippingAddress;
        $additionalAddresses = UserAddresses::find()->where('userId = '.$id.' and defaultShipping = 0 and defaultBilling=0')->all(); 
        if(\Yii::$app->request->isAjax)
           return $this->renderAjax('addressbook',compact('user','defaultBillingAddress','defaultShippingAddress','additionalAddresses')); 
        else
            return $this->render('addressbook',compact('user','defaultBillingAddress','defaultShippingAddress','additionalAddresses'));
    }

    public function actionMyorders(){
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
        $latest = Orders::find()->where(['customerId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->limit(5)->orderBy(['id'=>SORT_DESC]);   

        $latestOrders = new ActiveDataProvider([
            'query' => $latest,
        ]);

        $orders = new ActiveDataProvider([
            'query' => Orders::find()->where(['customerId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC,]),
        ]); 

        return $this->render('orders',compact('user','orders','latestOrders'));
    }

    public function actionWishlist(){
         $dataProvider = new ActiveDataProvider([
            'query' => WishListItems::find()->where(['userId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC,]),
        ]);
        if(Yii::$app->request->post())
        {
            foreach ($_POST['WishlistItems'] as $id => $comment) {
                $wishlistItem = WishlistItems::find()->where(['id'=>$id])->one();
                $wishlistItem->comments = $_POST['WishlistItems'][$id]['comments'];
                $wishlistItem->save(false);
            }
        } 
        return $this->render('wishlist',compact('user','dataProvider'));
    }

    public function actionOrderdetails($id){
        $userId= \Yii::$app->user->identity->id;
        $user=User::findOne($userId);

        $order = Orders::find()->where(['id'=>$id])->one();
        //var_dump($order);die();

        $orderItems = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId'=>$order->id])->orderBy(['id'=>SORT_DESC,]),
        ]);     

        return $this->render('orderDetails',compact('user','order','orderItems'));
    }

    public function actionSignup()
    { 
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            
            if ($user = $model->signup()) {//die('hai');
                if (Yii::$app->getUser()->login($user)) {
                    $user = User::findOne(\Yii::$app->user->id);
                    Yii::$app->getSession()->setFlash('success', 'Account Created Successfully!.......');
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { //die('validated');
            if ($model->sendEmail()) { //die('validated');
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                //die('validated');
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);

        //die('end');
    }

    public function actionResetPassword($token)
    {
            //$this->layout = "login";
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Your password is updated!.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionEditDefaultBillingAddress($id){
        $billingAddress = UserAddresses::findOne($id);

    }    


    public function actionRenamedir(){
        $alphabets = ['0','1','2','3','4','5','6','7','8','9','A','B','C', 'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
        //var_dump(file_exists('../../store/')); die;
        foreach($alphabets as $alpha){
            if(file_exists("../../store/products/images/base/".($alpha))){
                foreach($alphabets as $beta){
                    if(file_exists("../../store/products/images/base/".($alpha)."/".strtolower($beta))){
                        var_dump(rename("../../store/products/images/base/".($alpha)."/".strtolower($beta), "../../store/products/images/base/".($alpha)."/".strtoupper($beta)));
                        var_dump($alpha); 
                    }else{
                        var_dump(("../../store/products/images/base/".($alpha)."/".strtolower($beta)));
                    }
                }
                
            }
        }
    }

    public function actionTest(){
        die;
        var_dump(Yii::$app->security->generatePasswordHash('M@$t3r@dmiN')); die;
        $tempPages = \common\models\CmsPagesTemp::find()->all();
        // $pages = \yii\helpers\ArrayHelper::map(\common\models\CmsPages::find()->all(), 'id', 'id');
        // $diff = array_diff($pages, $tempPages);var_dump($diff); die;
        foreach($tempPages as $tempPage){
            if(!$page = \common\models\CmsPages::findOne($tempPage->id)){
                $page = new \common\models\CmsPages;
                $page->title = $tempPage->title;
                $page->contentHeading = $tempPage->contentHeading;
                $page->slug = $tempPage->slug;
                $page->storeId = $tempPage->storeId;
                $page->status = $tempPage->status;
                $page->dateUpdated = $tempPage->dateUpdated;
                $page->createdBy = $tempPage->createdBy;
            }
            $page->title = $tempPage->title;
            $page->contentHeading = $tempPage->contentHeading;
            $page->content = $tempPage->content;
            if(!is_null($tempPage->metaTitle) && $tempPage->metaTitle != "")
                $page->metaTitle = $tempPage->metaTitle;
            if(!is_null($tempPage->metaDescription) && $tempPage->metaDescription != "")
                $page->metaDescription = $tempPage->metaDescription;
            if(!is_null($tempPage->metaKeywords) && $tempPage->metaDescription != ""){
                //var_dump($page->metaKeywords); die;
                $page->metaKeywords = $tempPage->metaKeywords;
            }
            if(!$page->save(false)){
                var_dump($page->getErrors()); die;
            }
        }
    }

    function lowerDIr(){

    }

    public function actionTests(){
        die('failed');
    }

    public function actionClearcache($type = null){
        if(isset($type))
            Yii::$app->params['temp'][$type] = [];
        else
            Yii::$app->params['temp'] = [];
        var_dump(Yii::$app->params['temp']); die;
    }

    public function actionSlug($slug){ //var_dump($slug); die;
        $storeId = Yii::$app->params['storeId'];
        if($category = \common\models\Categories::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('categories/products', ['categoryId' => $category->id]);
        }elseif($product = \common\models\Products::find()->where(['urlKey' => $slug])->one()){ 
            return Yii::$app->runAction('products/view', ['id' => $product->id]);
        }elseif($giftregistry = \common\models\GiftRegistry::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('gift-registry/products', ['id' => $giftregistry->id]);
        }elseif($page = \common\models\CmsPages::find()->join('JOIN', 'StorePages as sp','sp.cmspageId=CmsPages.id')->where(['slug' => $slug, 'sp.storeId'=>$storeId])->one()){
            return Yii::$app->runAction('cms/view', ['id' => $page->id]);
        }elseif($brand = \common\models\Brands::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('brands/products', ['id' => $brand->id]);
        }else{
            throw new \yii\web\NotFoundHttpException('Page Not Found');
        }
    }

    public function actionTest1(){
        $conditions = [
            [
            'type' => 'combination',
            'aggregator' => '&&',
            'value' => 'true',
            'conditions' => [
                                [
                                    'type' => 'single',
                                    'attribute' => 'cart.quantity',
                                    'operator' => '>=',
                                    'value' => '12'
                                ],
                                [
                                    'type' => 'single',
                                    'attribute' => 'product.color',
                                    'operator' => '==',
                                    'value' => 'black'
                                ],
                                [
                                'type' => 'combination',
                                'aggregator' => '||',
                                'value' => 'true',
                                'conditions' => [
                                                    [
                                                        'type' => 'single',
                                                        'attribute' => 'cart.quantity',
                                                        'operator' => '>=',
                                                        'value' => '12'
                                                    ],
                                                    [
                                                        'type' => 'single',
                                                        'attribute' => 'product.color',
                                                        'operator' => '!=',
                                                        'value' => 'blue'
                                                    ]
                                                ]
                                ]
                            ]
                ]
        ];
    }
    public function actionTestemail(){
       $userid=23;
       $user=User::find()->where(['id'=>$userid])->one();
       //var_dump($user);die;
       $user->sendActivationMail();
    }


    public function actionPayment(){
        
        $braintree = Yii::$app->braintree;
        
        $result2 = $braintree->call('Transaction', 'sale', [
        'amount' => '30',
        'creditCard' => array(
            'number' => '3566003566003566',
          //  'expirationDate' => '05/12',
           // 'cvv' => '123'
        )
        
      ]);

        var_dump($result2);
        //var_dump($result->customer->paymentMethods()[0]->token);
        die();

        $result->success;
        # true

        $result->customer->id;

        //var_dump($response);die();
    }

    // public function actionSendaphoto(){

    //     return $this->render('/mobile/takeaphoto');
    // }
    
   
    public function actionB2csignup($id= NULL,$type=NULL)
    {   
        //die('hiiii');
        //$baseUrl = "http://b2b.legc.everybuy.com.au/site/b2csignup";
        //$baseUrl = "http://signup.legj.com.au";
        $this->layout = "b2clayout";
        $model = new B2cSignupRequests();
        $session = Yii::$app->session; 
        if($model->load(Yii::$app->request->post()) )
        { 
            //var_dump($model);die;

            //var_dump($_POST);die();
            //
            if($model->validate())
            { //die('hiiii');

                if($id == NULL)
                {
                    //var_dump($model);die;
                    $tradingHours=array('Monday' => $_POST['B2cSignupRequests']['monday'], 'Tuesday' => $_POST['B2cSignupRequests']['tuesday'],'Wednesday' => $_POST['B2cSignupRequests']['wednesday'],
                    'Thursday' => $_POST['B2cSignupRequests']['thursday'],'Friday' => $_POST['B2cSignupRequests']['friday'],'Saturday' => $_POST['B2cSignupRequests']['saturday'],
                    'Sunday' => $_POST['B2cSignupRequests']['sunday']);
                    $model->tradingHours=json_encode($tradingHours);
                    $session->set('b2c_requests_phoneStore',$model->phoneStore);
                    $session->set('b2c_requests_email',$model->email);
                    $session->set('b2c_requests_lejMemberNumber',$model->lejMemberNumber);
                    $session->set('b2c_requests_contactPerson',$model->contactPerson);
                    $session->set('b2c_requests_additionalEmail',$model->additionalEmail);
                    $session->set('b2c_requests_mobile',$model->mobile);
                    $session->set('b2c_requests_storeTradingName',$model->storeTradingName);
                    $session->set('b2c_requests_abn',$model->abn);
                    $session->set('b2c_requests_primaryStoreAddress',$model->primaryStoreAddress);
                    $session->set('b2c_requests_multipleStores',$model->multipleStores);
                    $session->set('b2c_requests_domainDetails',$model->domainDetails);
                    $session->set('b2c_requests_aboutus',$model->aboutus);
                    $session->set('b2c_requests_tradingHours',$model->tradingHours);
                    $session->set('b2c_requests_talkbox',$model->talkbox);
                  
                    //$session->set('b2c_requests_step1', $model);
                   
                    //var_dump($session['b2c_requests_step1']['phoneStore']);die;
                    if($type=='standard')
                    {   //var_dump($_SESSION);die;
                        return $this->redirect('/site/b2csignup/?id=standard');
                    }
                    else if ($type=='custom')
                    {
                        return $this->redirect('/site/b2csignup/?id=custom');
                    }
                    else
                    {
                        //return $this->redirect($baseUrl);
                    }  
                }
                else
                {
                    //var_dump($_SESSION);die;
                    $logo_images = UploadedFile::getInstances($model, 'logo');
                    
                    $store_images = UploadedFile::getInstances($model, 'storeImage');
                    if (!empty($logo_images))
                    {

                        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/b2c-requests/logo/";
                        $imagenames[]=0;  
                        foreach ($logo_images as $key => $logo_image) 
                        {   
                            $ext = end((explode(".", $logo_image->name))); //var_dump($ext);die;
                            $imagename= Yii::$app->security->generateRandomString().".{$ext}";
                            $path = Yii::$app->params['uploadPath'] . $imagename; 
                            $logo_image->saveAs($path);
                            $imagenames[$key]=$imagename;
                        }
                        $model->logo=json_encode($imagenames);
                    }
                    else
                    {$model->logo=NULL;}
                    if(!empty($store_images))
                    {
                        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/b2c-requests/store/";
                        $imagenames[]=0;  
                        foreach ($store_images as $key => $store_image) 
                        {
                            $ext = end((explode(".", $store_image->name)));
                            $imagename = Yii::$app->security->generateRandomString().".{$ext}";
                            $path = Yii::$app->params['uploadPath'] . $imagename; 
                            $store_image->saveAs($path);
                            $imagenames1[$key]=$imagename;
                            //var_dump($path);die;
                        }
                        $model->storeImage=json_encode($imagenames1);
                        //var_dump($model->storeImage);die;
                    }
                    else 
                    {$model->storeImage=NULL; }
                    if(isset($_POST['B2cSignupRequests']['skin']))
                    {
                        $model->skin=$_POST['B2cSignupRequests']['skin'];
                    }
                    //var_dump($_SESSION);die;
                    //var_dump($session['b2c_requests_step1']['abn']);die;
                    $model->phoneStore=$session['b2c_requests_phoneStore'];
                    $model->email=$session['b2c_requests_email'];
                    $model->lejMemberNumber=$session['b2c_requests_lejMemberNumber'];
                    $model->contactPerson=$session['b2c_requests_contactPerson'];
                    $model->additionalEmail=$session['b2c_requests_additionalEmail'];
                    $model->mobile=$session['b2c_requests_mobile'];
                    $model->storeTradingName=$session['b2c_requests_storeTradingName'];
                    $model->abn=$session['b2c_requests_abn'];
                    $model->primaryStoreAddress=$session['b2c_requests_primaryStoreAddress'];
                    $model->multipleStores=$session['b2c_requests_multipleStores'];
                    $model->domainDetails=$session['b2c_requests_domainDetails'];
                    $model->aboutus=$session['b2c_requests_aboutus'];
                    $model->talkbox=$session['b2c_requests_talkbox'];
                    $model->tradingHours=$session['b2c_requests_tradingHours'];
                    //var_dump($model);die;
                    if($model->save(false))
                    {
                       
                        //die('hiiii');
                        $model->sendB2cSignupRequestMail();
                        Yii::$app->getSession()->setFlash('success', 'Request Sent Successfully. We will respond to you as soon as possible.');//die('hii');
                        //return $this->render('standard');  
                        return $this->redirect('/site/b2csignup');              
                    }
                    else
                    {
                        //var_dump($model->errors);die;
                    }

                }
            }
            else
            {
                //var_dump($model->errors);die;
                if($id=='custom')
                {
                    return $this->render('custom', [ 'model' => $model,]);
                }
                else if($id=='standard')
                {
                //die('hii');   
                    return $this->render('standard', [ 'model' => $model,]);
                }   
                else
                {
                    return $this->render('b2csignup', ['model' => $model, ]);
                }
            }

          
            
        }
        else
        { //die('test');
            if($id=='custom')
            {
                return $this->render('custom', [ 'model' => $model,]);
            }
            else if($id=='standard')
            {
                //die('hii');   
                return $this->render('standard', [ 'model' => $model,]);
            }   
            else
            {
                    return $this->render('b2csignup', ['model' => $model, ]);
            }
        }
    }

    public function actionSearch($q)
    {
        
        $_REQUEST = yii\helpers\ArrayHelper::merge(['keyword' => $q, 'Products' => ['priceRange' => ['min' => '0', 'max' => '99999999']]], $_REQUEST);
        $options = [];
        $searchModel = new \common\models\search\ProductsSearch();
        $childProductQuery = new \yii\db\ActiveQuery('\common\models\Products');
        //$childProductQuery->join('LEFT JOIN', 'ProductLinkages pl', 'Products.id = pl.productId');
        $childProductQuery->where("sku LIKE '%$q%' AND visibility = 'not-visible-individually'");
        $childProducts = yii\helpers\ArrayHelper::map($childProductQuery->all(),'parent.id', 'self');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(count($childProducts) > 0){
            $dataProvider->query->orWhere("Products.id in ('".implode(',', array_keys($childProducts))."')");
            foreach($childProducts as $parentId => $product){
                $superAttrs = \common\models\ProductSuperAttributes::find()->where(['productId' => $parentId])->all();
                foreach($superAttrs as $attr){
                    $aop = \common\models\AttributeOptionPrices::find()->where(['attributeId' => $attr->attributeId, 'productId' => $parentId])->one();
                    $options[$parentId] = ['options' => [$attr->attr->code => $aop->id]];
                }
            }
        } 
        if(\Yii::$app->request->isAjax)
            return $this->renderAjax('search', compact('searchModel', 'dataProvider', 'options'));
        else
            return $this->render('search', compact('searchModel', 'dataProvider', 'options'));    
    }

    public function actionDelete($id)
    {
        $model=WishlistItems::findOne($id);
        $model->delete();

        return $this->redirect(['site/wishlist']);
    }
    public function actionComments()
    {
       $store_id=$_POST['store_id'];
       $user_id=$_POST['user_id'];
       $wlarray=explode(",",$_POST['wlcomments']);
       //var_dump($wlarray[0]);
       $i=0;
       $wlitem = WishlistItems::find()->where(['userId' =>$user_id,'storeId' =>$store_id])->all(); 
        foreach ($wlitem as $key => $value) {
            $wlmodel = WishlistItems::find()->where(['id' =>$value['id']])->one(); 
            $wlmodel->comments=$wlarray[$i];
            $wlmodel->save();
            $i++;
        }
        return $this->redirect(['site/account','tab'=>'wli']);
    }

    public function actionDuplicateTransaction(){ die();
	require_once '../components/eway/lib/eway-rapid-php-master/include_eway.php';
    	$client = \Eway\Rapid::createClient('C3AB9Cj1JuJ63RcfD3zte92RBRHCL1+4qDpaz4vwHYuRuLWNQaEAMPKpqXOj3fk96DQOXg', 'FvonQRdb', 'Sandbox');
        $transaction = [
    'Customer' => [
        'CardDetails' => [
            'Name' => 'John Smith',
            'Number' => '4444333322221111',
            'ExpiryMonth' => '12',
            'ExpiryYear' => '25',
            'CVN' => '123',
        ]
    ],
    'Payment' => [
        'TotalAmount' => 1000, 
    ],
    'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
];

$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);

var_dump($response);die();	
		
    }

    public function actionSetgproducts(){
        $products = Products::find()->where("sku in ('CH1036','CH1035','CH1092','CH1278','CH1084','CH1355','CH1356','CH3522','CH3523','CH3524','CH11135','CH10652','CH1331','CH1808','CH4545','CH1332','CH4546','CH4547','CH6000','CH10651','CH10475','CH3330','CH10477','CH10493','CH10498','CH5997','CH11369','CH11370','CH9772','CH9773','CH11060','CH11062','CH11064','CH9727','CH9733','CH9735','CH9726','CH9732','CH9659','CH6993','CH9658','CH6992','CH11555','CH10518','CH11383','CH11485','CH8570','CH6079','CH8580','CH6090','TN4156','CH10932','CH10520','CH11096','CH10113','CH10417','CH10421','CH10423','CH10409','CH10411','CH10412','CH5087','CH5100','CH6362','CH8054','CH10662','CH11358','CH9917','CH8065','CH11361','CH1302','CH1303','CH1304','CH11359','CH10495','CH10654','CH1706','CH11138','CH11143','CH1013','CH1212','CH1378','CH6186','CH11306','CH7885','CH10425','CH9254','CH7899','CH10800','CH5233','CH10917','CH11137','CH1097','CH1095','CH10473','CH10469','EG0555RW','EG0775RW','EG0885RW','CH4830','CH10749','CH4827','CH10746','CH10644','CH10464','CH10463','CH10907','CH10906','CH9501','CH9503','CH11839','CH11397','CH8193','CH11398','CH10820','CH11840','CH4177','CH8510','CH8664','CH8640','CH11446','CH11476','CH11466','CH11846','CH8603','CH8629','CH10892','CH11436','CH9970','CH8661','CH8651','CH10319','CH8625','CH11433','CH9473','CH8637','CH11351','CH11187','CH11855','CH11856','CH9452','CH10069','CH9474','CH8662','CH9243','CH9547','CH9548','CH9520','CH10861','CH10912','CH11859','CH11857','CH11858','CH10247','CH10246','CH10245','CH10339','ES0555BW','ES0775BW','ES0885BW','CH11473','CH9972','CH10049','CH10008','CH11461','CH11454','CH6284','CH10226','CH10233','CH3222','CH11849','CH10230','CH11451','CH9475','CH11854','CH9009','CH6272','CH9419','CH9089','CH10836','CH11191','CH11666','CH11665','CH11645','CH11657','CH11540','CH10199','CH10200','CH10201','CH11704','CH10874','CH9442','CH9408','CH9414','CH9416','CH10208','CH1748','CH9868','CH10209','CH1744','CH9352','CH9867','CH10211','CH10204','CH9365','CH9364','CH10829','CH9527','CH9528','CH9871','CH10825','CH10828','CH6471','CH9883','CH10072','CH10070','CH10071','CH11267','CH11268','CH11269','CH11271','CH11263','CH11266','CH10227','CH1461','CH11265','CH10223','CH1448','CH10228','CH9888','CH10229','CH11659','CH11661','CH10191','CH6469','CH10858','CH10356','CH10234','CH10041','CH10235','CH11848','CH10835','CH2202','CH11847','CH9278','CH11147','CH10231','CH7509','CH10214','CH1323','CH10771','CH6472','CH9275','CH10881','CH11690','CH9808','CH11636','CH10882','CH10364','CH8982','CH10363','CH8350','CH11851','CH11850','CH8230','CH11852','CH10758','CH8228','CH11853','CH10770','CH9447','CH10184','CH9446','CH6214','CH9308','CH10352','CH9323','CH9324','CH9325','CH9315','CH9316','CH9317','CH11646','CH11647','CH11648','CH9321','CH9322','CH3940','CH9326','CH9327','MOR317/40','CH6255','CH10948','CH6231','CH10950','CH6254','CH10901','CH6229','CH10903','CH10904','CH6239','CH10764','CH10763','CH10765','CH10768','CH10766','CH10767','CH9310','CH11643','CH11644','CH11697','CH11183','CH12124','CH12125','CH12126','CH12127','CH12128','CH12129','CH12130','CH12131')")->all();
        //echo count($products); die;
        foreach($products as $product){
            $sp = new \common\models\StoreProducts;
            $sp->storeId = "136";
            $sp->productId = $product->id;
            $sp->type = "b2c";
            $sp->enabled = "1";
            var_dump($sp->save(false));
        }
    }
    public function actionSetmap(){
       // var_dump($_REQUEST["addressId"]);
        $b2bAddress = B2bAddresses::findOne($_REQUEST["addressId"]);
        //var_dump($b2bAddress->longitude);var_dump($b2bAddress->latitude);die();

        //var_dump($b2bAddress->addressAsText);die();

        $session = Yii::$app->session;
        Yii::$app->session['addressId'] = $_REQUEST["addressId"];
        /*Yii::$app->session['longitude'] = $b2bAddress->longitude;
        Yii::$app->session['latitude'] = $b2bAddress->latitude;*/
        Yii::$app->session['addressText'] = $b2bAddress->addressAsText;
        return $_REQUEST["addressId"];

    }

    public function actionLoadCart(){
        $serialized = 'a:4:{s:32:"a08010f06eff91edc7a3816b7c54efe1";O:33:"common\models\ProductCartPosition":7:{s:11:" * _product";N;s:2:"id";i:13273;s:6:"config";a:3:{s:13:"recipientName";s:24:"reception@midcare.com.au";s:14:"recipientEmail";s:24:"reception@midcare.com.au";s:16:"recipientMessage";s:20:"All the best Rebecca";}s:10:"b2bAddress";N;s:11:"supplierUid";N;s:8:"comments";N;s:12:" * _quantity";s:1:"1";}s:32:"17aec3879ccec64124af97f0ef4b716a";O:33:"common\models\ProductCartPosition":7:{s:11:" * _product";N;s:2:"id";i:13272;s:6:"config";a:3:{s:13:"recipientName";s:24:"reception@midcare.com.au";s:14:"recipientEmail";s:24:"reception@midcare.com.au";s:16:"recipientMessage";s:20:"All the best Rebecca";}s:10:"b2bAddress";N;s:11:"supplierUid";N;s:8:"comments";N;s:12:" * _quantity";s:1:"1";}s:32:"b113160b9245c31e9620f5d2382e742c";O:33:"common\models\ProductCartPosition":7:{s:11:" * _product";N;s:2:"id";i:13275;s:6:"config";a:3:{s:13:"recipientName";s:24:"reception@midcare.com.au";s:14:"recipientEmail";s:24:"reception@midcare.com.au";s:16:"recipientMessage";s:20:"All the best Rebecca";}s:10:"b2bAddress";N;s:11:"supplierUid";N;s:8:"comments";N;s:12:" * _quantity";s:1:"1";}s:32:"827baa1d7119c8fcb72d1d939b898892";O:33:"common\models\ProductCartPosition":7:{s:11:" * _product";N;s:2:"id";i:13271;s:6:"config";a:3:{s:13:"recipientName";s:24:"reception@midcare.com.au";s:14:"recipientEmail";s:24:"reception@midcare.com.au";s:16:"recipientMessage";s:20:"All the best Rebecca";}s:10:"b2bAddress";N;s:11:"supplierUid";N;s:8:"comments";N;s:12:" * _quantity";s:1:"1";}}';
        $cart = Yii::$app->cart;
        $cart->positions = unserialize($serialized);
        var_dump($cart->count); die;
    }
    public function actionSendAPhoto(){
        $model = new TakeaphotoForm();
        $uniqueKey = Helper::generateRandomString();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())){
            //var_dump($_POST);die;
            $model->photo = UploadedFile::getInstance($model, 'photo');
            if($model->photo->saveAs(\Yii::$app->basePath."/../store/upload-a-photo/". $uniqueKey .'.' . $model->photo->extension)){
                $model->priceRange = $_POST['range'];
                $model->storeId = $_POST['storeId'];
                $model->name = $_POST['TakeaphotoForm']['name'];
                $model->email = $_POST['TakeaphotoForm']['email'];
                $model->phone = $_POST['TakeaphotoForm']['phone'];
                $model->comment = $_POST['TakeaphotoForm']['comment'];
                $model->photo = '/store/upload-a-photo/' . $uniqueKey .'.' . $model->photo->extension;
                $model->attributes;
                if($model->save(false))
                {
                    Yii::$app->getSession()->setFlash('success', 'Thank you for your interest in us, we will be contacting you shortly.');
                    $model->sendMail();
                    $model->sendMailUser();
                    return $this->redirect('send-a-photo',['model'=>$model]);
                }
            }
        }
        return $this->render('takeaphoto',['model'=>$model]);
    }
    public function actionGetTradingHours(){ // site contactus trading hours set  @ aaron 02/11/2016
        $b2baddress=B2bAddresses::findOne($_POST['id']);        
        $mon=($b2baddress->b2bTradingHoursFormatted['0']['monday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['monday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['monday']['to'];
        $tue=($b2baddress->b2bTradingHoursFormatted['0']['tuesday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['tuesday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['tuesday']['to'];
        $wed=($b2baddress->b2bTradingHoursFormatted['0']['wednesday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['wednesday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['wednesday']['to'];
        $thu=($b2baddress->b2bTradingHoursFormatted['0']['thursday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['thursday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['thursday']['to'];
        $fri=($b2baddress->b2bTradingHoursFormatted['0']['friday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['friday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['friday']['to'];
        $sat=($b2baddress->b2bTradingHoursFormatted['0']['saturday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['saturday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['saturday']['to'];
        $sun=($b2baddress->b2bTradingHoursFormatted['0']['sunday']['from']=='closed')? 'Closed' : 
            $b2baddress->b2bTradingHoursFormatted['0']['sunday']['from'].'&ndash; '.
            $b2baddress->b2bTradingHoursFormatted['0']['sunday']['to'];

       $html='MON: '.$mon.'<br>
            TUE: '.$tue.'<br>
            WED: '.$wed.'<br>
            THU: '.$thu.'<br>
            FRI: '.$fri.'<br>
            SAT: '.$sat.'<br>
            SUN: '.$sun.'<br>';
        echo $html;
    }
    
}
