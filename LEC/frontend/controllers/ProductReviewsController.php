<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ProductReviews;

class ProductReviewsController extends \app\components\FrontController
{

	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */

    public function actionCreate(){
    	$review = new ProductReviews();
    	if ($review->load(Yii::$app->request->post())){  //var_dump($_POST);die();
    		$review->storeId = Yii::$app->params['storeId'];
    		$review->userId = Yii::$app->user->id;
    		$review->save(false);
    	}	

    }

    public function actionUpdate($id){
    	$review = $this->findModel($id);
    	if ($review->load(Yii::$app->request->post())){
    		$review->storeId = Yii::$app->params['storeId'];
    		$review->userId = Yii::$app->user->id;
    		$review->save(false);
    	}	
    }

    protected function findModel($id)
    {
        if (($model = ProductReviews::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

?>