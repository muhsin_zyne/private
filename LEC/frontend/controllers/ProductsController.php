<?php

namespace frontend\controllers;

use Yii;
use common\models\Products;
use common\models\ProductsSearch;
use common\models\AttributeOptionPrices;
use common\models\AttributeValues;
use common\models\BundleItems;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;
use yz\shoppingcart\ShoppingCart;
use yii\helpers\ArrayHelper;
use common\models\ProductReviews;
use yii\helpers\Url;
use yii\db\Expression;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends \app\components\FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */

    public function actionIndex(){
        $searchModel = new \common\models\search\ProductsSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->andWhere('Attributes.code = "price"');
            $query->groupBy("Products.id");
        }

        $products = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 12,
            ],
        ]);

        $products->setSort([
            'attributes' => [
                'dateAdded',
                'price' => [
                    'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                ]
            ]
        ]);

        if(\Yii::$app->request->isAjax){
            return $this->renderAjax('index', compact('products', 'searchModel')); 
        }    
        else
            return $this->render('index', compact('products', 'searchModel'));
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->byodOverride = true;
        $product = $this->findModel($id);
        $store = \common\models\Stores::findOne(Yii::$app->params['storeId']);
        $this->metaTitle = ($product->meta_title!="")? $product->meta_title : $store->title.": ".$product->name;
        $this->metaKeywords = ($product->meta_keywords!="")? $product->meta_keywords : $store->title.": ".$product->name;
        $this->metaDescription = ($product->meta_description!="")? $product->meta_description : $store->title.": ".$product->name;
        //$product->scenario = "purchase";
        //$productThumbImage = $product->thumbnailImage;
        $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$product->getThumbnailImage();

        //$productBaseImage = $product->baseImage;
        $baseImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."base/".$product->getThumbnailImage();
           
        //$productSmallImage = $product->smallImage;
        $smallImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."small/".$product->getThumbnailImage();


        if (!\Yii::$app->user->isGuest) {
            if(!$userReview = ProductReviews::find()->where(['userId'=>\Yii::$app->user->identity->id,'productId'=>$id,'storeId'=>Yii::$app->params['storeId']])->one()){
                $userReview = new ProductReviews();
            }
        }
        else
            $userReview = new ProductReviews();

        $allReviews = ProductReviews::find()->where(['productId'=>$id,'storeId'=>Yii::$app->params['storeId']])->all();        
            
        $this->byodOverride = false;

        $dataProvider = new ActiveDataProvider([
            'query' => Products::find()->orderBy('rand()')->limit(3),
            'pagination' => false,
        ]);

        $latestProducts = Products::find()
                //->join('JOIN','ProductImages as pi', 'pi.productId = Products.id')
                ->andWhere('typeId not in ("gift-voucher")')
                //->andWhere('pi.paths != NULL')
                ->orderBy(['dateAdded' => SORT_DESC])
                ->limit(9)
                ->all();

        $bestsellersImages = Products::find()
                ->andWhere(['not in','typeId',array('gift-voucher')])
                ->orderBy(['dateAdded' => SORT_DESC])
                ->limit(16)
                ->all();         

        $featuredProducts =new ActiveDataProvider([
            'query' => Products::find()->orderBy('rand()')->limit(4),
            'pagination' => false,
        ]);
        
        $related= new \common\models\RelatedProducts();
        
        $related=$related->getRelated($id)->orderBy(new Expression('rand()'));
        
        $relatedProducts = new ActiveDataProvider([
            'query' => $related,
            'pagination' => false,
        ]);
       
        if(isset($_GET['options']) && $product->typeId == "configurable"){
            $_REQUEST['Products']['attributeSetId'] = $product->attributeSetId;
            if(is_array($_GET['options'])){
                foreach($_GET['options'] as $attr => $option){
                    $product->$attr = $option;
                }
            }
        }
        
        if(\Yii::$app->request->isAjax)
            return $this->renderAjax('_formview', compact('product','thumbImagePath')); 
        else
            return $this->render('view', compact('product', 'dataProvider','latestProducts','bestsellersImages','featuredProducts','thumbImagePath','baseImagePath','smallImagePath','userReview','allReviews','relatedProducts'));
    }

    public function actionQuickview($id){
        $product = $this->findModel($id);
        $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$product->getThumbnailImage();

    }

    public function actionSlug($slug){
        if($product = Products::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('products/view', ['id' => $product->id]);
        }else
            throw new \yii\web\NotFoundHttpException('Page Not Found');
        }
   

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * fskfdfnsf jdkfsnfis sjfsjfdfnkf knfsjfns fsknsfikjfsn fknfksnfsklsf kifnsfnjks skfsnfks ikfsfskb asdfsd ijkdng dmbhk d 
     * ksnclkfsfoin siskfn sfm isksnfs foildaf fik iksn sisfksfsfs iknsfkfsf iknfksnfs fiksffsfn iskf 
     * fsjfsbfskf sfskf,nfs ffksfnsf,s fifnskf,s fisksfm,s fkfssnskf 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionGetoptions()
    {
         $selected= $_REQUEST['selected'];
         $attrId1 = $_REQUEST['attrId1'];
         $id = $_REQUEST['id'];
         $price = $_REQUEST['price'];
         // $current = $_REQUEST['current'];

        if($selected != NULL){
            $groups = AttributeOptionPrices::find()->where(['and', "productId = $id", "id = $selected", "attributeId = $attrId1"])->all();
            foreach ($groups as $value) {
                $tot_price = $price + $value->price;
                return Helper::money($tot_price);
            }
        }

        else {
            return Helper::money(110);
        }
    }
    public function actionInsertslug(){ //die('insert slug');
        $products = Products::find()->all();
        foreach($products as $product){
            $product->urlKey = \frontend\components\Helper::slugify($product->name);
            $product->save(false);
            echo $product->id."-";
        }
    }
    public function actionTest1()
    {
        $tot_price = 0;

        if(isset($_REQUEST['price']))
            $price = $_REQUEST['price'];
        if($tot_price == 0)
            $tot_price = $_REQUEST['price'];
        if(isset($_REQUEST['selected']) && !empty($_REQUEST['selected'])){
            foreach ($_REQUEST['selected'] as $bundleItemId) {
                $prodPrice = BundleItems::findOne($bundleItemId)->product->price;
                    if(isset($_REQUEST['selectedqty'])){

                        foreach ($_REQUEST['selectedqty'] as $qty) {

                            if(key($qty) == (int)$bundleItemId) {
                                $quantity = $qty[(int)$bundleItemId]; 

                            }

                            else
                                $quantity = 1;

                        }
                        $tot_price = $tot_price+($quantity*$prodPrice);
                    }

                    else
                        $tot_price = $tot_price+$prodPrice;

            }
            return Helper::money($tot_price);
            $tot_price = 0;
        }

        else
            return Helper::money($price);

    }


    public function actionSale(){

        $todays = date("d-m-Y");

        $searchModel = new \common\models\search\ProductsSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        
        $validFromProducts = ArrayHelper::map(Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->where('a.code = "special_from_date" and STR_TO_DATE(value,"%d-%m-%Y") < STR_TO_DATE("'.$todays.'","%d-%m-%Y")')->andWhere(['storeId' => (!is_null(Yii::$app->params['storeId'])? Yii::$app->params['storeId'] : 0)])->all(), 'id', 'self');

        $validToProducts = ArrayHelper::map(Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->where('a.code = "special_to_date" and STR_TO_DATE(value,"%d-%m-%Y") > STR_TO_DATE("'.$todays.'","%d-%m-%Y")')->andWhere(['storeId' => (!is_null(Yii::$app->params['storeId'])? Yii::$app->params['storeId'] : 0)])->all(), 'id', 'self');
        
        $specialProducts = array_intersect(array_keys($validFromProducts), array_keys($validToProducts));
        if(!empty($specialProducts))
			$query->andWhere("Products.id in (".implode(',', $specialProducts).')');
		else
			$query->andWhere("1=0");

        if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->andWhere('Attributes.code = "price"');
            $query->groupBy("Products.id");
        }

        $products = new ActiveDataProvider([  
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 16,
            ],
        ]); 

        $products->setSort([
            'attributes' => [
                'dateAdded',
                'price' => [
                    'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                ]
            ]
        ]);

        return $this->render('sale', compact('products','searchModel'));
    }

    public function actionConfigurate(){
        $subProduct = false;
        $matchingAttrs = [];
        if(isset($_POST['Products'])){
            $product = Products::findOne($_POST['Products']['id']);
            if($product->parent){
                foreach($product->parent->linkedProducts as $prod){
                    $j = 1;
                    foreach($_POST['Products'] as $attr => $val){ // Max: loop through the super attributes
                        if($attr == "id" || !$prod->product)
                            continue;
                        if($prod->product->$attr == $val){
                            $subProduct = $prod->product;
                        }else{
                            $subProduct = false;
                            $matchingAttrs[$prod->product->id] = $j;
                            break;
                        }
                        $j++;
                    }
                    if($subProduct)
                        break;
                }
                if($subProduct){
                    $this->redirect(Url::to(['products/view', 'id' => $subProduct->id]));
                }else{ // Max: no perfect matching subproduct is found, hence selecting the most matching one
                    arsort($matchingAttrs); reset($matchingAttrs);
                    if(empty($matchingAttrs)){
                        Yii::$app->session->setFlash('error', 'Sorry, the configuration you selected was not found in our.'); 
                        $this->redirect(Url::to(['products/view', 'id' => $product->id])); //die;
                    }else{
                        $this->redirect(Url::to(['products/view', 'id' => key($matchingAttrs)]));
                    }
                }
            }
        }
    }

    public function actionOldConfigurate(){
        $subProduct = false;
        if(isset($_POST['Products'])){
            $product = Products::findOne($_POST['Products']['id']);
            if($product->parent){
                foreach($product->parent->linkedProducts as $prod){
                    foreach($_POST['Products'] as $attr => $val){
                        if($attr == "id")
                            continue;
                        if($prod->product->$attr != $val){ var_dump($prod->product->$attr." - ".$val."\n");
                            $subProduct = false;
                            break;
                        }else
                            $subProduct = $prod->product;
                    }
                    if($subProduct)
                        break;
                }
                if($subProduct){
                    $this->redirect(Url::to(['products/view', 'id' => $subProduct->id]));
                }
            }
        }
    }

}
