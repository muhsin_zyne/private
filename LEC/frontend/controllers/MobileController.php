<?php
namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use frontend\models\TakeaphotoForm;
use yii\web\UploadedFile;
use common\components\MailHandler;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\components\Helper;

class MobileController extends Controller
{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        
    }
    
    public function actionTakeaphoto(){
        $model = new TakeaphotoForm();
        $uniqueKey = Helper::generateRandomString();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())){
		    $model->photo = UploadedFile::getInstance($model, 'photo');
                if($model->photo->saveAs(\Yii::$app->basePath."/../store/upload-a-photo/".$model->photo->baseName .'_' . $uniqueKey .'.' . $model->photo->extension)){
					$model->priceRange = $_POST['range'];
					$model->storeId = $_POST['storeId'];
					$model->name = $_POST['TakeaphotoForm']['name'];
					$model->email = $_POST['TakeaphotoForm']['email'];
					$model->phone = $_POST['TakeaphotoForm']['phone'];
					$model->comment = $_POST['TakeaphotoForm']['comment'];
					$model->photo = '/store/upload-a-photo/'.$model->photo->baseName .'_' . $uniqueKey .'.' . $model->photo->extension;
					$model->attributes;
					if($model->save(false))
					{
						Yii::$app->getSession()->setFlash('success', 'Thank you for your interest in us, we will be contacting you shortly.');
						$model->sendMail();
						$model->sendMailUser();
					}
				}
        }

        return $this->render('takeaphoto',['model'=>$model]);
    }
	public function actionTest()
    {
        $EmailQueue = \common\models\EmailQueue::find()->where(['id' => '310'])->one();
         echo $EmailQueue->message;exit;
    }

}