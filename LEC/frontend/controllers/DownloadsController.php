<?php

namespace frontend\controllers;
use Yii;
use common\models\Downloads;
use common\models\search\DownloadsSaerch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class DownloadsController extends \app\components\FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    { 
        $dataProvider = new ActiveDataProvider([
                'query' => Downloads::find()->where(['storeId'=>Yii::$app->params['storeId'],'status'=>1])->orderBy(['dateUpdated' => SORT_DESC]),
        ]);
        if (Yii::$app->request->post()){
            $search=$_POST['s-text'];
            $dataProvider = new ActiveDataProvider([
                'query' => Downloads::find()->where(['storeId'=>Yii::$app->params['storeId'],'status'=>1])->orderBy(['dateUpdated' => SORT_DESC]) 
                ->andFilterWhere(['like', 'title', $search]),
            ]);
        }
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDownload($id){
        $model = $this->findModel($id);       
        $file = Yii::$app->basePath."/../".$model->filePath;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
   
    protected function findModel($id)
    {
        if (($model = Downloads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
