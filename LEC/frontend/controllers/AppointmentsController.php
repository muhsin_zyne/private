<?php

namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;
use common\models\Stores;
use common\models\User;
use kartik\date\DatePicker;

class AppointmentsController extends \app\components\FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [ 
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    { 
    	if(isset($_GET['date'])){
			$date = new \DateTime($_GET['date']);
			$currentDate = $date->format('Y-m-d');
		}
		else
  			$currentDate = date('d-M-Y',strtotime(' +1 day'));

		$weekDay = lcfirst(date('D',strtotime($currentDate)));
		if($currentDateSetting = AppointmentDefaultSettings::find()->select($weekDay)->where(['storeId'=>Yii::$app->params['storeId']])->one()) {
			$workingHours = json_decode($currentDateSetting->$weekDay,true);
			foreach ($workingHours as $key => $slots) {
				$timeSlots = $slots;
			}
		}
		else{
			if ($weekDay == "sun")
				$timeSlots = [];
			else
				$timeSlots = ['9:00 am - 10:00 am','10:00 am - 11:00 am','11:00 am - 12:00 pm','12:00 pm - 1:00 pm','1:00 pm - 2:00 pm','2:00 pm - 3:00 pm','3:00 pm - 4:00 pm','4:00 pm - 5:00 pm','5:00 pm - 6:00 pm'];
		}	
		$currentDateAppointments = Appointments::find()->where('storeId = "'.Yii::$app->params['storeId'].'" and startTime like "%'.$currentDate.'%"')->all();
        $appointment = new Appointments(); 
		return $this->render('index',compact('currentDate','currentDateSetting','timeSlots','currentDateAppointments','appointment'));
    }

    public function actionCreate(){
        $appointment = new Appointments();
        $date = $_GET['date'];
        $timeslot = $_GET['timeslot']; 
        if ($appointment->load(Yii::$app->request->post())) {  
            $splitSlots = explode(" - ", $_GET['timeslot']);
            $startTime = new \DateTime($_POST['Appointments']['bookingDate'].$splitSlots[0]);
            $endTime = new \DateTime($_POST['Appointments']['bookingDate'].$splitSlots[1]);
            $appointment->startTime = $startTime->format('Y-m-d H:i:s');
            $appointment->endTime = $endTime->format('Y-m-d H:i:s');
            $appointment->status = "booked";
            $appointment->storeId = Yii::$app->params['storeId'];
            if(!Appointments::find()->where(['storeId'=>Yii::$app->params['storeId'],'status'=>'booked','startTime'=>$startTime->format('Y-m-d H:i:s')])->one()){
                $appointment->save(false);
                $appointment->sendAppointmentConfirmation();
                $appointment->sendAppointmentNotification();
                Yii::$app->session->setFlash('success', 'Appointment Booked successfully'); 
            }       
        }
        else{
            if(\Yii::$app->request->isAjax){
                return $this->renderAjax('_form',compact('appointment','date','timeslot'));
            }
            else {    
                return $this->render('_form', compact('appointment'));
            } 
        }    
    }

    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}    	
?>