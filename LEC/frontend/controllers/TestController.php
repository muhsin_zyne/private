<?php
namespace frontend\controllers;
use Yii;
use common\models\Orders;
use common\models\OrderItems;
use common\models\Products;
use common\models\AttributeOptions;
use common\models\ProductLinkages;
use common\models\OrderItemChildren;
use common\models\AttributeOptionPrices;
use yii\helpers\ArrayHelper;
class TestController extends \yii\web\Controller
{
    public function actionRandomproductview(){
        $product = Products::find()->orderBy('RAND()')->one();
        return Yii::$app->runAction('products/view', ['id' => $product->id]);
    }

    public function actionOrder(){
        $product = Products::find()->orderBy('RAND()')->one();
        $productId = $product->id;
        $_POST['Products'] = ['id' => $productId]; 
        if(!$product)
            {break;}
        foreach($product->superAttributes as $attr){ //var_dump($_POST['Products']); die;
            $_POST['Products'][$attr->attr->code] = $attr->attributeOptionPrices[0]->id;
        }
        $_POST['qty'] = 1;
        $position = $product->getCartPosition(); //var_dump($position->getCost()); die;
        $position->config = []; //var_dump($product->typeId); die;
        if($product->typeId=="bundle"){
            foreach($_POST['Products']['BundleItems'] as $item){
                if(!is_array($item)){
                    $bundleItem = BundleItems::findOne($item);
                    if(isset($_POST['Products']['BundleItems']['qty'][$item])){
                        $position->config[$bundleItem->productId] = ['qty' => $_POST['Products']['BundleItems']['qty'][$item], 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                    }else{
                        $position->config[$bundleItem->productId] = ['qty' => $bundleItem->defaultQty, 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                    }
                }
            }
        }elseif($product->typeId=="configurable"){
            foreach($_POST['Products'] as $attrCode => $optionPriceId){
                if($attrOptionPrice = AttributeOptionPrices::findOne($optionPriceId)){
                    $position->config[$attrOptionPrice->id] = ['attr' => $attrOptionPrice->attr->toArray(), 'value' => $attrOptionPrice->option->value, 'optionId' =>$attrOptionPrice->optionId];
                }
            }
        }
        Yii::$app->cart->put($position, $_POST['qty']);
        $this->checkout();
        Yii::$app->cart->removeAll();
    }

    public function actionBulkorder(){
        $productIds = ArrayHelper::map(Products::find()->where(['typeId' => 'configurable'])->orderBy('RAND()')->limit(10)->all(), 'id', 'id');
        //var_dump($productIds); die;
        foreach($productIds as $productId){
            $product = Products::findOne($productId);
            $_POST['Products'] = ['id' => $productId]; 
            if(!$product)
                {break;}
            foreach($product->superAttributes as $attr){ //var_dump($_POST['Products']); die;
                $_POST['Products'][$attr->attr->code] = $attr->attributeOptionPrices[0]->id;
            }
            $_POST['qty'] = 1;
            $position = $product->getCartPosition(); //var_dump($position->getCost()); die;
            $position->config = []; //var_dump($product->typeId); die;
            if($product->typeId=="bundle"){
                foreach($_POST['Products']['BundleItems'] as $item){
                    if(!is_array($item)){
                        $bundleItem = BundleItems::findOne($item);
                        if(isset($_POST['Products']['BundleItems']['qty'][$item])){
                            $position->config[$bundleItem->productId] = ['qty' => $_POST['Products']['BundleItems']['qty'][$item], 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                        }else{
                            $position->config[$bundleItem->productId] = ['qty' => $bundleItem->defaultQty, 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                        }
                    }
                }
            }elseif($product->typeId=="configurable"){
                foreach($_POST['Products'] as $attrCode => $optionPriceId){
                    if($attrOptionPrice = AttributeOptionPrices::findOne($optionPriceId)){
                        $position->config[$attrOptionPrice->id] = ['attr' => $attrOptionPrice->attr->toArray(), 'value' => $attrOptionPrice->option->value, 'optionId' =>$attrOptionPrice->optionId];
                    }
                }
            }
            //var_dump($position->config); die;

            Yii::$app->cart->put($position, $_POST['qty']);
            }
            //var_dump(Yii::$app->cart->positions); die;
            $this->checkout();
            Yii::$app->cart->removeAll();
            //return $this->redirect(['cart/view']);
        }

    public function checkout(){
    	//$post = $_POST['Orders'];
    	$cart = Yii::$app->cart;
    	$order = new Orders;
    	$order->type = 'b2b';
    	$order->customerId = Yii::$app->user->id;
    	$order->shippingAmount = '0.00';
    	$order->subTotal = $cart->cost;
    	$order->grandTotal = $cart->cost;
    	if($order->save(false)){
    		foreach($cart->positions as $position){
    			$item = new OrderItems;
    			$item->orderId = $order->id;
    			$item->productId = $position->product->id;
    			$item->quantity = $position->quantity;
    			$item->price = $position->price;
    			$item->config = json_encode($position->config);
    			if($item->save(false)){
    				if($position->product->typeId=="configurable"){
    					$linkages = ProductLinkages::find()->where(['parent' => $position->id])->all(); 
    					foreach($linkages as $link){ //logic for finding the child product with given config
    						foreach($position->config as $config){
    							if($link->product->{$config['attr']['code']} == $config['value'])
    								$childProduct = $link->product;
								else{
    								$childProduct = false;
    								break;
    							}
    						}
    						if($childProduct) break;
    					}
    					$child = new OrderItemChildren;
    					$child->parentProductId = $position->id;
    					$child->orderItemId = $item->id;
    					$child->productId = $childProduct->id;
    					$child->quantity = 1;
    					$child->type = "configurable";
    					$child->save(false);
    				}elseif($position->product->typeId=="bundle"){
    					foreach($position->config as $productId => $config){
	    					$child = new OrderItemChildren;
	    					$child->parentProductId = $position->id;
	    					$child->orderItemId = $item->id;
	    					$child->productId = $productId;
	    					$child->quantity = $config['qty'];
	    					$child->type = "bundle";
	    					$child->save(false);
    					}
    				}
    			}
    		}
    	}
    }
}
