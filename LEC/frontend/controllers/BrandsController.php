<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\FrontController;
use common\models\Categories;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use common\models\Products;
use common\models\Brands;
use common\models\User;
use common\models\search\ProductsSearch;
use yii\widgets\Pjax;
use yii\db\ActiveQuery;

class BrandsController extends FrontController
{
    public function actionIndex()
    {
        $query = \common\models\Brands::find();
        $storeId = Yii::$app->params['storeId'];
        /*$brands = $query
        			->from('Brands as b')
                    ->join('JOIN',
                        'StoreBrands as sb',
                        'sb.brandId = b.id'
                    )
                    ->join('JOIN', 'Products as p', 'b.id = p.brandId')
                    ->join('JOIN', 'StoreProducts as sp', 'sp.productId = p.id')
                    ->groupBy('b.id')
                    ->where("sb.enabled = 1 and sb.storeId = $storeId and sb.type = 'b2c' and sp.storeId = $storeId and p.status = 1 and sp.type = 'b2c' and b.isVirtual = 0 and b.path is not null")
                    ->orderBy("b.title");
        //var_dump($brands);die();      */

        $brands = $query
                    ->from('Brands')
                    ->join('JOIN',
                        'StoreBrands as sb',
                        'sb.brandId = Brands.id'
                    )
                    ->groupBy('Brands.id')
                    ->andWhere("sb.enabled = 1 and sb.storeId = $storeId and sb.type = 'b2c' and Brands.isVirtual = 0 and Brands.path is not null")
                    ->orderBy("Brands.title");     

        $dataProvider = new ActiveDataProvider([
    		'query' => $brands,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 16,
            ],
        ]);            

        return $this->render('index', compact('dataProvider'));
    }

    public function actionProducts($id = null, $slug = null){
    	//$products = Products::find();
	$brand = (is_null($id) && is_null($slug))? false : (!is_null($id)? Brands::findOne($id) : Brands::find()->where(['urlKey' => $slug])->one());
    	$searchModel = new \common\models\search\ProductsSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        $productsQuery = $brand->getStoreProducts(Yii::$app->params['storeId'], 'b2c');

        //var_dump(count($productsQuery->all()));die();

        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->andWhere('Attributes.code = "price"');
            $query->groupBy("Products.id");
        }

    	$products = new ActiveDataProvider([
    		//'query' => Products::find()->where('brandId = '.$id.''),
    		'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 16,
            ],
        ]); 

        $products->setSort([
            'attributes' => [
                'dateAdded',
                'price' => [
                    'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                ]
            ]
        ]);
	if(!\common\models\StoreBrands::find()->where(['brandId' => $brand->id, 'storeId' => Yii::$app->params['storeId'], 'enabled' => '1'])->exists()){
	    throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
	}
        if($productsQuery->exists()){
            if(\Yii::$app->request->isAjax)
                return $this->renderAjax('products', compact('products','brand')); 
            else
        	   return $this->render('products', compact('products','brand'));
        }
        
        else
            return $this->render('view', compact('brand'));   
    }
}    
