<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\FrontController;
use common\models\ClientPortal;
use common\models\ClientPortalSearch;
use common\models\User;
use frontend\models\ByodForm;
use yii\data\ActiveDataProvider;


class ByodController extends FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        /*$storeId = Yii::$app->params['storeId'];
        $byod = new ClientPortal();
        if(\Yii::$app->request->isAjax)
            return $this->renderAjax('index', compact('byod')); 
        else
            return $this->render('index', compact('products', 'searchModel'));*/

        if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
            return $this->redirect(['/byod/view']);

        $this->view->registerJs("$('.top-link-byod').click();");    
        return Yii::$app->runAction('site/index');
    }

    public function actionApply(){
        $storeId = Yii::$app->params['storeId'];
        $byod = new ByodForm();
        $todays = date("d-m-Y");
        if ($byod->load(Yii::$app->request->post())){ 
            if(\Yii::$app->request->isAjax) {
                if($appliedCode = ClientPortal::find()->where(['studentCode'=>$byod->code,'storeId'=>Yii::$app->params['storeId']])->one()){  
                    $todays = date("d-m-Y");
                    if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) >= strtotime($todays))){ 
                        Yii::$app->session->set('studentByod', $byod->code);
                        return true;
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                        return false;
                    }

                } 
                elseif($appliedCode = ClientPortal::find()->where(['schoolCode'=>$byod->code,'storeId'=>Yii::$app->params['storeId']])->one()) {
                    $todays = date("d-m-Y");
                    if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) >= strtotime($todays))){ 
                        Yii::$app->session->set('schoolByod', $byod->code);
                        return true;
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                        return false;
                    }
                }
                else{
                    die('Invalid Code!');
                }   
            }
            
            else{
                if($appliedCode = ClientPortal::find()->where(['studentCode'=>$byod->code,'storeId'=>Yii::$app->params['storeId']])->one()){  
                    $todays = date("d-m-Y");
                    if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) >= strtotime($todays))){ 
                        Yii::$app->session->set('studentByod', $byod->code);
                        return $this->redirect('/byod/view');
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Please enter a valid BYOD Code!!!'); 
                        return $this->redirect('/site/index');
                    }

                } 
                elseif($appliedCode = ClientPortal::find()->where(['schoolCode'=>$byod->code,'storeId'=>Yii::$app->params['storeId']])->one()) {
                    $todays = date("d-m-Y");
                    if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) >= strtotime($todays))){ 
                        Yii::$app->session->set('schoolByod', $byod->code);
                        return $this->redirect('/byod/view');
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Please enter a valid BYOD Code!!!'); 
                        return $this->redirect('/site/index');
                    }
                }
                else{
                    die('Invalid Code!');
                }  
            }    
        }
        else {
            if(\Yii::$app->request->isAjax){
                return $this->renderAjax('applycode',compact('byod'));
            }
            // else{
            //     if(isset(Yii::$app->session['schoolByod']) || isset(Yii::$app->session['studentByod']))
            //         return $this->redirect('/byod/view');
            //     else
                    return $this->render('applycode', compact('byod'));        
           // }
            
        }    
    }

    public function actionView(){  
        
        if(isset(Yii::$app->session['studentByod']))
            $byod = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['studentByod'],'storeId'=>Yii::$app->params['storeId']])->one();
        elseif (isset(Yii::$app->session['schoolByod'])) 
            $byod = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session['schoolByod'],'storeId'=>Yii::$app->params['storeId']])->one();    
        
        if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod'])){ 
            $storeId = Yii::$app->params['storeId'];
            $searchModel = new \common\models\search\ProductsSearch();
            $query = $searchModel->search(Yii::$app->request->queryParams)->query;
            $productsQuery = $byod->getProducts($storeId);
            $query->join = $productsQuery->join;
            $query->andWhere($productsQuery->where);

            if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
                $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
                $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
                $query->andWhere('Attributes.code = "price"');
                $query->groupBy("Products.id");
            }

            $products = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 12,
                ],
            ]);

            $products->setSort([
                'attributes' => [
                    'dateAdded',
                    'price' => [
                        'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                        'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                    ]
                ]
            ]);

            if(\Yii::$app->request->isAjax){  
                return $this->renderAjax('products', compact('products','byod')); 
            }    
            else
                return $this->render('products', compact('products','byod'));
        }
        else{
            Yii::$app->session->setFlash('error', 'Please enter a valid BYOD Code!!!'); 
            return $this->redirect('/site/index');
        }    
    } 

    public function actionAuthenticate($token){
        if($appliedCode = ClientPortal::find()->where(['studentAuthCode'=>$token,'storeId'=>Yii::$app->params['storeId']])->one()){  
            $todays = date("d-m-Y");
            if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) > strtotime($todays))){ 
                Yii::$app->session->set('studentByod', $appliedCode->studentCode);
                return $this->redirect('/byod/view');
            }
            else{
                Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                return $this->redirect('/site/index');
            }

        }
        elseif ($appliedCode = ClientPortal::find()->where(['schoolAuthCode'=>$token,'storeId'=>Yii::$app->params['storeId']])->one()) {
            $todays = date("d-m-Y");
            if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) > strtotime($todays))){ 
                Yii::$app->session->set('schoolByod', $appliedCode->schoolCode);
                return $this->redirect('/byod/view');
            }
            else{
                Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                return $this->redirect('/site/index');
            }
        }
        else{
            return $this->redirect('/site/index');
        }
    }

    public function actionLogout()
    {
        if(isset(Yii::$app->session['studentByod']))
            unset(Yii::$app->session['studentByod']);
        elseif (isset(Yii::$app->session['schoolByod'])) 
            unset(Yii::$app->session['schoolByod']); 

        return $this->redirect('/site/index');       
    }

    protected function findModel($id)
    {
        if (($model = ClientPortal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    
}        

?>
