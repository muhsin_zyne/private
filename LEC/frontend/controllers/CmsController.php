<?php

namespace frontend\controllers;

use Yii;
use common\models\CmsPages;
use common\models\search\CmsPagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\StorePages;
/**
 * CmsController implements the CRUD actions for CmsPages model.
 */
class CmsController extends \app\components\FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays a single CmsPages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!in_array(Yii::$app->params['storeId'], ArrayHelper::map(StorePages::find()->where(['cmspageId' => $id])->all(), 'id', 'storeId')))
            throw new \yii\web\NotFoundHttpException('Page Not Found');

       $model=$this->findModel($id);
        //var_dump($model);die;
        $this->metaTitle = $model->metaTitle;
        $this->metaKeywords = $model->metaKeywords;
        $this->metaDescription = $model->metaDescription;
        $this->view->registerJsFile('/js/canvas.js');
        return $this->render('view', [
            'model' => $model,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = CmsPages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
