<?php

namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\FrontController;
use common\models\Categories;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use common\models\Products;
use common\models\User;
use common\models\search\ProductsSearch;
use yii\widgets\Pjax;

class CategoriesController extends FrontController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProducts($categoryId = null, $slug = null)
    {
        $category = (is_null($categoryId) && is_null($slug))? false : (!is_null($categoryId)? Categories::findOne($categoryId) : Categories::find()->where(['urlKey' => $slug])->one());


        //var_dump($category);die();

        if(!$category)
            throw new \yii\web\NotFoundHttpException('Page Not Found');
        //var_dump($category); die;
        $store = \common\models\Stores::findOne(Yii::$app->params['storeId']);
        $this->metaTitle = $store->title.": ".$category->title;//var_dump($this->metaTitle);die;
        $this->metaKeywords = $store->title.": ".$category->title;
        $this->metaDescription = $store->title.": ".$category->title;
        $searchModel = new \common\models\search\ProductsSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        if(\Yii::$app->request->isAjax){
            $productsQuery = $category->getProducts(Yii::$app->params['storeId'], 'b2c');
        }else{
            $productsQuery = new \yii\db\ActiveQuery('common\models\Products');
        }
        $productsQuery = $category->getProducts(Yii::$app->params['storeId'], 'b2c');
        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        if($category->urlKey == "gift-vouchers"){
            $_GET['sort'] = "price";
        }
        if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->andWhere('Attributes.code = "price"');
            $query->groupBy("Products.id");
        }
        $products = new ActiveDataProvider([
    		'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 12,
            ],
        ]);
        // if(\Yii::$app->request->isAjax)
        //     {var_dump($products->pagination->pageSize); die;}
        $products->setSort([
            'attributes' => [
                'dateAdded',
                'price' => [
                    'asc' => ['`AttributeValues`.`value` +0' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value` +0' => SORT_DESC]
                ]
            ]
        ]);
        if(\Yii::$app->request->isAjax){
            return $this->renderAjax('products', compact('products','category')); 
        }    
        else
            return $this->render('products', compact('products','category'));
    }

    public function actionSlug($slug){
        if($category = Categories::find()->where(['urlKey' => $slug])->one()){
            return Yii::$app->runAction('categories/products', ['categoryId' => $category->id]);
        }else
            throw new \yii\web\NotFoundHttpException('Page Not Found');
    }
}
