<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\FrontController;
use common\models\ClientPortal;
use common\models\ClientPortalSearch;
use common\models\User;
use frontend\models\ByodForm;
use yii\data\ActiveDataProvider;


class ClientPortalController extends FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if(isset(Yii::$app->session['clientPortal'])){
            return $this->redirect('/client-portal/view');
        }
        else {    
            $this->view->registerJs("$('.top-link-portal').click();");    
            return Yii::$app->runAction('site/index');
        }    
    }

    public function actionApply(){  
        $storeId = Yii::$app->params['storeId'];
        $clientPortal = new ByodForm();
        $todays = date("d-m-Y");
        if ($clientPortal->load(Yii::$app->request->post())){ 
            if(\Yii::$app->request->isAjax) {
                if($appliedCode = ClientPortal::find()->where(['studentCode'=>$clientPortal->code,'storeId'=>Yii::$app->params['storeId']])->one()){ 
                    $todays = date("d-m-Y");
                    if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) >= strtotime($todays))){  
                        Yii::$app->session->set('clientPortal', $clientPortal->code);
                        return true;
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                        return false;
                    }

                } 
                else{
                    die('Invalid Code!');
                }
            }
            else{
                if($appliedCode = ClientPortal::find()->where(['studentCode'=>$clientPortal->code,'storeId'=>Yii::$app->params['storeId']])->one()){ 
                    $todays = date("d-m-Y");
                    if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn) >= strtotime($todays))){  
                        Yii::$app->session->set('clientPortal', $clientPortal->code);
                        return $this->redirect('/client-portal/view');
                    }
                    else{
                        Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                        return $this->redirect('/site/index');
                    }
                } 
                else{
                    die('Invalid Code!');
                }
            }       
        }
        else {
            if(\Yii::$app->request->isAjax){
                return $this->renderAjax('applycode',compact('clientPortal'));
            }
            return $this->render('applycode', compact('clientPortal'));
        }    
    }

     public function actionAuthenticate($token){
        if($appliedCode = ClientPortal::find()->where(['studentAuthCode'=>$token,'storeId'=>Yii::$app->params['storeId']])->one()){  
            $todays = date("d-m-Y");
            if ((strtotime($appliedCode->validFrom) <= strtotime($todays)) &&  (strtotime($appliedCode->expiresOn)  > strtotime($todays))){ 
                Yii::$app->session->set('clientPortal', $appliedCode->studentCode);
                return $this->redirect('/client-portal/view');
            }
            else{
                Yii::$app->session->setFlash('error', 'Invalid Code !'); 
                return $this->redirect('/site/index');
            }

        }
        else{
            return $this->redirect('/site/index');
        }
    }

    public function actionView(){  
        if(isset(Yii::$app->session['clientPortal'])){ 
           $clientPortal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['clientPortal'],'storeId'=>Yii::$app->params['storeId']])->one();
            $storeId = Yii::$app->params['storeId'];
            $searchModel = new \common\models\search\ProductsSearch();
            $query = $searchModel->search(Yii::$app->request->queryParams)->query;
            $productsQuery = $clientPortal->getProducts($storeId);
            $query->join = $productsQuery->join;
            $query->andWhere($productsQuery->where);

            if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
                $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
                $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
                $query->andWhere('Attributes.code = "price"');
                $query->groupBy("Products.id");
            }

            $products = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 12,
                ],
            ]);

            $products->setSort([
                'attributes' => [
                    'dateAdded',
                    'price' => [
                        'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                        'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                    ]
                ]
            ]);

            if(\Yii::$app->request->isAjax){  
                return $this->renderAjax('products', compact('products','clientPortal')); 
            }    
            else
                return $this->render('products', compact('products','clientPortal'));
        }
        else{
            //die('Pls enter your BYOD Code!!!');
            Yii::$app->session->setFlash('error', 'Please enter a valid Client Code!!!'); 
            return $this->redirect('/site/index');
        }    
    } 
}

?>    
