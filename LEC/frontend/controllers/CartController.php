<?php   
namespace frontend\controllers;
use common\models\Products;
use common\models\BundleItems;
use common\models\AttributeOptionPrices;
use common\models\Carts;
use common\models\Stores;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class CartController extends \app\components\FrontController
{
    public function actionAdd(){

        $product = Products::findOne($_REQUEST['Products']['id']);
        //$product->byod = true;
        $request = $_REQUEST;
        unset($request['Products']['id']);
        if(empty($request['Products']) && ($product->typeId == "configurable" || $product->typeId == "gift-voucher")){
            return $this->redirect(Url::to(['products/view', 'id' => $product->id]));
        }

        $position = $product->getCartPosition(); //var_dump($position->getCost()); die;
        //Yii::$app->cart->beforeAdd($);
        $position->config = []; //var_dump($product->typeId); die;
        if($product->typeId=="bundle"){
            foreach($_POST['Products']['BundleItems'] as $item){
                if(!is_array($item)){
                    $bundleItem = BundleItems::findOne($item);
                    if(isset($_POST['Products']['BundleItems']['qty'][$item])){
                        $position->config[$bundleItem->productId] = ['qty' => $_POST['Products']['BundleItems']['qty'][$item], 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                    }else{
                        $position->config[$bundleItem->productId] = ['qty' => $bundleItem->defaultQty, 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                    }
                }
            }
        }elseif($product->typeId=="configurable"){ //var_dump($_POST['Products']); die;
            foreach($_POST['Products'] as $attrCode => $optionPriceId){
                if($attrCode == "id")
                    continue;
                if($attrOptionPrice = AttributeOptionPrices::findOne($optionPriceId)){
                    $position->config[$attrOptionPrice->id] = ['attr' => $attrOptionPrice->attr->toArray(), 'value' => $attrOptionPrice->option->value, 'optionId' =>$attrOptionPrice->optionId];
                }
            }

        }elseif($product->typeId=="gift-voucher"){           
            $position->config = ['recipientName'=>$_POST['Products']['recipientName'], 'recipientEmail'=>$_POST['Products']['recipientEmail'], 'recipientMessage'=>$_POST['Products']['recipientMessage'],'voucherValue'=>$_POST['Products']['voucherValue'],];

        }
        //var_dump($position); die;
        if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
            $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
        else
            $sessionVar = 'studentByod';

        if(Yii::$app->cart->check($position,$sessionVar) && Yii::$app->cart->check($position,'clientPortal')){
            Yii::$app->cart->put($position, $_POST['qty']);
        }
        else{ 
            //Yii::$app->session->setFlash("error", "Sorry, you cant mix BYOD products with regular products!!"); 
            if(Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}() || Yii::$app->cart->hasOnlyClientPortalProducts()){
                Yii::$app->session->setFlash("error", "Sorry, BYOD/Client Portal products and regular products cannot be purchased together. Please either checkout your cart or clear cart and add the products."); 
            }
             else{
                Yii::$app->session->setFlash("error", "Sorry, regular products cannot be purchased together with BYOD/Client Portal products. Please check out your cart and buy the regular products or remove the BYOD/Client Portal product from cart and buy the regular products");
            }   
        }     

        //var_dump(Yii::$app->cart->positions);die();
        //var_dump($_POST['qty']);die();

        //Yii::$app->cart->put($position, $_POST['qty']);
        $this->saveCart();
        return $this->redirect(['cart/view']);
    }

    public function actionView()
    {
        $cart = Yii::$app->cart; 
        if(empty($cart->positions))
            return $this->render('empty');
        return $this->render('view', compact('cart'));
    }

    public function actionDelete($positionId)
    {
        Yii::$app->cart->removeById($positionId);
        $this->redirect(Url::to(['cart/view'])); 
    }

    public function actionUpdate($positionId = null, $checkout="false")
    {
       //var_dump($_POST['Products']);die;
        $app = Yii::$app;
        $positions = $app->cart->positions;
        if(isset($_POST['Products'])){
            $product = Products::findOne($_POST['Products']['id']);
            $position = $positions[$positionId]; //die(md5(serialize($position->config)));
            $oldConfig = $position->config;
            $position->config = [];
            if($product->typeId=="bundle"){
                foreach($_POST['Products']['BundleItems'] as $item){
                    if(!is_array($item)){
                        $bundleItem = BundleItems::findOne($item);
                        if(isset($_POST['Products']['BundleItems']['qty'][$item])){
                            $position->config[$bundleItem->productId] = ['qty' => $_POST['Products']['BundleItems']['qty'][$item], 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                        }else{
                            $position->config[$bundleItem->productId] = ['qty' => $bundleItem->defaultQty, 'name' => $bundleItem->product->name,'price' => ($bundleItem->product->priceType=="fixed")? ($bundleItem->product->price + $bundleItem->price) : $bundleItem->product->price];
                        }
                    }
                }
            }elseif($product->typeId=="configurable"){ //var_dump($_POST['Products']); die;
                foreach($_POST['Products'] as $attrCode => $optionPriceId){
                    if($attrOptionPrice = AttributeOptionPrices::findOne($optionPriceId)){
                        $position->config[$attrOptionPrice->id] = ['attrTitle' => $attrOptionPrice->attr->title, 'value' => $attrOptionPrice->option->value];
                    }
                }
            }elseif($product->typeId=="gift-voucher"){           
            $position->config = ['recipientName'=>$_POST['Products']['recipientName'], 'recipientEmail'=>$_POST['Products']['recipientEmail'], 'recipientMessage'=>$_POST['Products']['recipientMessage'],'voucherValue'=>$_POST['Products']['voucherValue'],];
            }

            elseif($product->typeId=="gift-voucher"){           
            $position->config = ['recipientName'=>$_POST['Products']['recipientName'], 'recipientEmail'=>$_POST['Products']['recipientEmail'], 'recipientMessage'=>$_POST['Products']['recipientMessage'],'voucherValue'=>$_POST['Products']['voucherValue'],];
            }

            unset($positions[$positionId]);
            $position->quantity = $_POST['qty'];
            $positions[$position->getId()] = $position;
            $app->cart->setPositions($positions);
            $this->redirect(Url::to(['cart/view']));
        }elseif(isset($_POST['Cart'])){
            $giftWrapCost=0;
            $gift_wrap = \common\models\Configuration::findSetting('gift_wrapping', Yii::$app->params['storeId']);
            foreach($_POST['Cart'] as $positionId => $pos){
                $positions[$positionId]->quantity = $pos['qty'];
                if(isset($pos['comments'])){
                    $positions[$positionId]->comments = $pos['comments'];
                }
                else{
                    $positions[$positionId]->comments = "";
                }   
                $positions[$positionId]->giftWrap = false;
                if(isset($pos['giftWrap'])){
                    $positions[$positionId]->giftWrap = true;
                    if($gift_wrap!='no')
                        $positions[$positionId]->giftWrapCost = $gift_wrap;
            }
                
            }
            $app->cart->setPositions($positions);  //var_dump($positions); die;
            if ($checkout == 'true') {
                return $this->redirect('/checkout/index');
            } else
                return $this->redirect('/cart/view');
        }
        //var_dump($positions); die;
        return Yii::$app->runAction('products/view', ['id' => $positions[$positionId]->id]);
    }

    public function actionEmpty(){
        Yii::$app->cart->removeAll();
        $where = Yii::$app->user->isGuest? compact('sessionId') : ['userId' => Yii::$app->user->id];
        if($savedCart = Carts::find()->where($where)->one())
            $savedCart->delete(); //deleting the abandoned cart if exists
        $this->redirect(Url::to(['cart/view']));
    }


    public function actionApplycode($code){

        if(Yii::$app->cart->hasOnlyByodProducts() || Yii::$app->cart->hasOnlyClientPortalProducts()){
            Yii::$app->session->setFlash('error', "Invalid Promo Code!.");
            return Yii::$app->runAction('cart/view');
        }

    	if(isset(Yii::$app->session['appliedCoupons']) && !empty(Yii::$app->session['appliedCoupons'])){
    		Yii::$app->session->setFlash('error', "You are not allowed to apply more than one coupon to the cart.");
            return $this->redirect(Url::to(['cart/view']));
    	}
	if($rule = \common\models\PriceRules::find()->join('JOIN', 'StorePriceRules as spr', 'PriceRules.id = spr.ruleId')->where(['spr.storeId' => Yii::$app->params['storeId']])->andWhere("BINARY code = '$code'")->one()){
            if($rule->type=="fixed-product" || $rule->type=="percent-product"){
                $positions = Yii::$app->cart->positions;
                $valid = false;
                $productRuleDetails = [];
                foreach($positions as $position){
                    if($rule->isValidWith($position)){
                        $valid = true;
                        $productCost = $position->cost;
                        if($rule->type=="fixed-product"){
                            $discount = $rule->discount;
                            $withDiscount = $position->cost - (($rule->discount<=$productCost)? $rule->discount : $productCost);
                        }
                        elseif ($rule->type=="percent-product") {
                            $discount = $productCost*($rule->discount/100);
                            $withDiscount = $productCost - (($rule->discount<=$productCost)? $discount : $productCost);
                        }else{
                            $valid = false;
                            break;
                        }
                        $productRuleDetails[$position->product->id] = $discount;
                    }
                }
                if($valid){  // die('product');
 
                    //var_dump($withDiscount);die();
 
                    if(!isset(Yii::$app->session['appliedCoupons']))
                        Yii::$app->session['appliedCoupons'] = [];
 
                    if(isset(Yii::$app->session['appliedCoupons'][$rule->id])){ //var_dump(Yii::$app->session['appliedCoupons']); die;
                        Yii::$app->session->setFlash('error', "Coupon $code is already applied");
                        return $this->redirect(Url::to(['cart/view']));
                    }
                    Yii::$app->session['appliedCoupons'] = Yii::$app->session['appliedCoupons'] + [$rule->id => $productRuleDetails];
                    //Yii::$app->session['appliedCoupons'] = Yii::$app->session['appliedCoupons'] + [$rule->id => $discount];
 
                    //var_dump(Yii::$app->session['appliedCoupons']); die;
 
                    Yii::$app->session->setFlash('success', "Coupon $code applied successfully");
                    $this->redirect(Url::to(['cart/view']));
 
                }
                else{
                    Yii::$app->session->setFlash('error', "Invalid Coupon Code");
                    $this->redirect(Url::to(['cart/view']));
                }
                // Yii::$app->session['appliedCoupons'] = Yii::$app->session['appliedCoupons'] + [$rule->id => $discount];
 
                // Yii::$app->session->setFlash('success', "Coupon $code applied successfully");
                // $this->redirect(Url::to(['cart/view']));
                //return json_encode(['status' => 'success', 'discount' => $withDiscount]);
            }else{
                if($rule->isValidWith(Yii::$app->cart)){
                    $cartCost = Yii::$app->cart->cost;
                    if($rule->type=="fixed-cart"){
                        $discount = $rule->discount;
                        $withDiscount = $cartCost - (($rule->discount<=$cartCost)? $rule->discount : $cartCost);
                    }elseif($rule->type=="percent-cart"){ //var_dump($rule->type);die();
                        //$discount = (($rule->discount/$cartCost)/100);
                        $discount = $cartCost*($rule->discount/100);
                        //var_dump($discount);die();
                        $withDiscount = $cartCost - (($rule->discount<=$cartCost)? ($cartCost*($rule->discount/100)) : $cartCost);
                        //$withDiscount = $cartCost - $rule->discount;
                    }
                    elseif($rule->type=="free-shipping"){
                        $discount = 0;
                        $withDiscount = $cartCost;
                    }
                    //var_dump(Yii::$app->session['appliedCoupons']); die;
                    if(!isset(Yii::$app->session['appliedCoupons']))
                        Yii::$app->session['appliedCoupons'] = [];
                    
                    if(isset(Yii::$app->session['appliedCoupons'][$rule->id])){ //var_dump(Yii::$app->session['appliedCoupons']); die;
                        Yii::$app->session->setFlash('error', "Coupon $code is already applied");
                        return $this->redirect(Url::to(['cart/view']));
                    }
                    Yii::$app->session['appliedCoupons'] = Yii::$app->session['appliedCoupons'] + [$rule->id => $discount];
                    
                    Yii::$app->session->setFlash('success', "Coupon $code applied successfully");
                    $this->redirect(Url::to(['cart/view']));
                    //return json_encode(['status' => 'success', 'discount' => $withDiscount]);
                }else{
                    Yii::$app->session->setFlash('error', "Invalid Promo Code!.");
                    $this->redirect(Url::to(['cart/view']));
                }
            }
        }else{
            Yii::$app->session->setFlash('error', "Invalid Promo Code!.");
            return Yii::$app->runAction('cart/view');
        }
    }

    public function saveCart(){
        $app = Yii::$app;
        $sessionId = $app->session->id;
        $where = $app->user->isGuest? compact('sessionId') : ['userId' => $app->user->id];
        if(!$cart = Carts::find()->where($where)->one()){
            $cart = new Carts;
            if(!$app->user->isGuest)
                $cart->userId = $app->user->id;
        }
        $cart->email = isset($app->session->email)? $app->session->email : null;
        $cart->positionsData = serialize($app->cart->positions);
        $cart->type = "abandoned";
        $cart->updatedDate = date('Y-m-d H:i:s');
        $cart->sessionId = $sessionId;
        $cart->save(false);
    }

    public function actionTest(){
        $voucher = \common\models\GiftVouchers::findOne(6);
        $voucher->sendEmail(false); die;

    }
}
