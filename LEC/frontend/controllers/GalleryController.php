<?php

namespace frontend\controllers;
use Yii;
use common\models\Gallery;
use common\models\GalleryImages;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends \app\components\FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {  
        
        $id = Gallery::find()->where(['storeId'=>Yii::$app->params["storeId"]])->one()->id;

        return $this->render('view', [
            'model' => $this->findModel($id),
            'id'=>$id,
        ]); 
    }
   
    public function actionView($id)
    { 
        return $this->render('view', [
            'model' => $this->findModel($id),
            'id'=>$id,
        ]);
    }

    

    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
