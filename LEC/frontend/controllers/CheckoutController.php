<?php 

namespace frontend\controllers;
use Yii;
use common\models\Orders;
use common\models\OrderItems;
use common\models\Products;
use common\models\AttributeOptions;
use common\models\ProductLinkages;
use common\models\OrderItemChildren;
use common\models\GiftVouchers;
use common\models\UserAddresses;
use common\models\B2bAddresses;
use common\models\ClientPortal;
use common\models\Stores;
use common\models\Carts;
use common\models\OrderMeta	;
use frontend\components\Helper;
use common\models\EmailQueue;
use bryglen\braintree\braintree;
use frontend\components\eway;

class CheckoutController extends \app\components\FrontController
{
    public function actionIndex($orderId = null){
        //var_dump(Yii::$app->session['appliedVouchers']); die;
        $checkout = new \frontend\models\CheckoutForm;
        $shippingMethods = \common\models\ShippingMethods::findShippingMethods();
        $voucherDiscount = Yii::$app->cart->voucherDiscount;
        $couponDiscount = Yii::$app->cart->couponDiscount;
        $store = Stores::findOne(Yii::$app->params['storeId']);
        $addresses = B2bAddresses::find()->where(['ownerId'=>$store->adminId])->all();
        $defaultAddress = B2bAddresses::find()->where(['ownerId'=>$store->adminId])->one();
        $portal = '';
		
		if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
        	$sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
        else
        	$sessionVar = 'clientPortal';

        if(isset(Yii::$app->session[$sessionVar])){ 

        	if($sessionVar == "studentByod")
				$portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
			elseif($sessionVar == "schoolByod")
				$portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one();
			else
				$portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session[$sessionVar],'storeId'=>Yii::$app->params['storeId']])->one(); 

            if($portal->shipment_type == "delivery-program"){ 
                $checkout->shipping_company = $portal->organisation_name;
                $checkout->shipping_street = $portal->address;
                $checkout->shipping_city = $portal->city;
                $checkout->shipping_state = $portal->state;
                $checkout->shipping_postcode = $portal->postcode;
                $checkout->shipping_phone = $portal->phone;
            }    

		}

       	if(empty(Yii::$app->cart->positions))
            return $this->render('/cart/empty');
        return $this->render('index', compact('checkout', 'shippingMethods', 'voucherDiscount', 'couponDiscount', 'orderId','addresses','portal','defaultAddress'));
            
    }

    public function actionLogin(){
        Yii::$app->user->setReturnUrl(['cart/checkout']);
        Yii::$app->runAction("site/login");
    }

    public function actionProcess($orderId = null){

    	$store = Stores::findOne(Yii::$app->params['storeId']);



        if(count(Yii::$app->cart->positions)==0){
            Yii::$app->session->setFlash('error','Sorry, no item(s) in the cart');
            return $this->redirect(\yii\helpers\Url::to(['cart/view']));    
        }

            if(!isset($_POST['CheckoutForm']))
                return $this->redirect(\yii\helpers\Url::to(['site/index']));

            if (isset($_POST['CheckoutForm'])) {  

                //if(!empty(count(Yii::$app->cart->positions))){}

                    if(isset($_POST['checkout_method']) && $_POST['checkout_method'] == "register"){ //var_dump($_POST['checkout_method']); die;
                        $signup = new \frontend\models\SignupForm;
                        $signup->scene = "checkout";
                        $signup->firstname = $_POST['CheckoutForm']['billing_firstname'];
                        $signup->lastname = $_POST['CheckoutForm']['billing_lastname'];
                        $signup->email = $_POST['CheckoutForm']['email'];
                        $signup->password = $_POST['CheckoutForm']['password'];
                        $signup->confirm_password = $_POST['CheckoutForm']['confirmPassword'];
			$signup->storeId = Yii::$app->params['storeId'];
                        if($user = $signup->signup()){
                            Yii::$app->getUser()->login($user);
                        }else
                            Yii::$app->session->setFlash('error','There was an error when trying to register your user account. Please try again.');
                    }

                    if(isset($_POST['billing']['save_in_address_book'])){
                        $newBillingAddress = new UserAddresses;
                        $newBillingAddress->userId = Yii::$app->user->id;
                        $newBillingAddress->firstName = $_POST['CheckoutForm']['billing_firstname'];
                        $newBillingAddress->lastName = $_POST['CheckoutForm']['billing_lastname'];
                        $newBillingAddress->company = $_POST['CheckoutForm']['billing_company'];
                        $newBillingAddress->telephone = $_POST['CheckoutForm']['billing_phone'];
                        $newBillingAddress->fax = $_POST['CheckoutForm']['billing_fax'];
                        $newBillingAddress->streetAddress = $_POST['CheckoutForm']['billing_street'];
                        $newBillingAddress->company = $_POST['CheckoutForm']['billing_company'];
                        $newBillingAddress->defaultShipping = '0';
                        $newBillingAddress->zip = $_POST['CheckoutForm']['billing_postcode'];
                        $newBillingAddress->country = $_POST['CheckoutForm']['billing_country'];
                        if(!UserAddresses::find()->where(['userId' => Yii::$app->user->id, 'defaultBilling' => '1'])->one()){
                            $newBillingAddress->defaultBilling = '1';
                        }
                        else
                            $newBillingAddress->defaultBilling = '0';    

                        $newBillingAddress->save(false);

                    }

                    if(isset($_POST['shipping']['save_in_address_book'])){

                        $newShippingAddress = new UserAddresses;
                        $newShippingAddress->userId = Yii::$app->user->id;
                        $newShippingAddress->firstName = $_POST['CheckoutForm']['shipping_firstname'];
                        $newShippingAddress->lastName = $_POST['CheckoutForm']['shipping_lastname'];
                        $newShippingAddress->company = $_POST['CheckoutForm']['shipping_company'];
                        $newShippingAddress->telephone = $_POST['CheckoutForm']['shipping_phone'];
                        $newShippingAddress->fax = $_POST['CheckoutForm']['shipping_fax'];
                        $newShippingAddress->streetAddress = $_POST['CheckoutForm']['shipping_street'];
                        $newShippingAddress->zip = $_POST['CheckoutForm']['shipping_postcode'];
                        $newShippingAddress->country = $_POST['CheckoutForm']['shipping_country'];

                        if(!UserAddresses::find()->where(['userId' => Yii::$app->user->id, 'defaultShipping' => '1'])->one()){
                            $newShippingAddress->defaultShipping = '1';
                        }
                        else
                            $newShippingAddress->defaultShipping = '0';

                        if(isset($newBillingAddress)){

                            $diff=false;

                            foreach ($newBillingAddress as $key => $value) {
                                if($key != "id" && $key != "defaultBilling" && $key != "defaultShipping"){
                                    if ($value != $newShippingAddress->$key) {
                                        $diff = true;
                                        break;
                                    }
                                }
                            }
                            if($diff){
                                $newShippingAddress->save(false);
                            }
                        }
                        
                        else
                            $newShippingAddress->save(false);    
                    }
                }

            
                if($order = $this->placeOrder($orderId)){//var_dump($order);die('orderId'); 
                    $orderId = $order->id;
                    $voucherDiscount = 0;
                    if(isset(Yii::$app->session['appliedVouchers'])){
                        $voucherDiscount = array_sum(array_values(Yii::$app->session['appliedVouchers']));
                    }
                    //var_dump($order->grandTotal > $voucherDiscount); die;
                    if($order->grandTotal > $voucherDiscount){
                       // print_r($_POST); die();
                        if(isset($_POST['cmd'])){
                            $order->status = "failed";
                            $order->paymentMethod = "Braintree";
                            $order->save(false);
                            $this->emptyCart(); 
                            $this->afterPayment($order);
                            $amount = $order->grandTotal - $voucherDiscount;
                            return $this->render('paypal',compact('amount','orderId')); 
                        } 
                        else {

                            if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
                                $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
                            else
                                $sessionVar = 'clientPortal';

                            //var_dump(Yii::$app->cart->isPaymentRequired($sessionVar));die();

                            if(!Yii::$app->cart->isPaymentRequired($sessionVar)){ 
                                $order->status = "failed";   
                                // $order->paymentMethod = "Braintree";
                                // $order->transactionId = $response->transaction->id;
                                // $order->cardNumber = $response->transaction->creditCard['last4'];
                            }
                            else{
    	                        $response = $this->doPayment($order, $voucherDiscount);

    	                        if($store->getPaymentMethod() == "eway"){ 
									if($response->TransactionStatus) { 
    	                        		$order->status = "in-process";
	    	                            $order->paymentMethod = "Eway";
	    	                            $order->transactionId = $response->TransactionID;
	    	                            $order->cardNumber = $response->Customer->CardDetails->Number;
    	                        	}
    	                        	else{
    	                        		foreach ($response->getErrors() as $error) {
                                            $order->gatewayResponse = \Eway\Rapid::getMessage($error);
                                        }
                                        $order->save(false);
    	                        		Yii::$app->getSession()->setFlash('error', 'An error occurred while communicating with the Payment Gateway. Please try again');
	    	                            return $this->redirect(\yii\helpers\Url::to(['checkout/index', 'orderId' => $orderId]));
    	                        	}
    	                        }
    	                        else{
	    	                        if($response->success) {
	    	                            $order->status = "in-process";
	    	                            $order->paymentMethod = "Braintree";
	    	                            $order->transactionId = $response->transaction->id;
	    	                            $order->cardNumber = $response->transaction->creditCard['last4'];
	    	                        }
	    	                        else{
	    	                            $order->gatewayResponse = json_encode((array)$response);
	    				                $order->save(false);
	    	                            Yii::$app->getSession()->setFlash('error', 'An error occurred while communicating with the Payment Gateway. Please try again');
	    	                            return $this->redirect(\yii\helpers\Url::to(['checkout/index', 'orderId' => $orderId])); 
	    	                        }
	    	                    }    
                        	}
                        }
                    }
                    else{
                        $order->status = "in-process";
                        $order->paymentMethod = "Gift Voucher";
                    }
                    //$order->collectFrom = $_POST['CheckoutForm']['storeAddressId'];
                    $order->save(false);
                    $this->emptyCart();
                    $this->afterPayment($order);
                    $order->sendOrderConfirmation();
                    $order->sendOrderNotification();
                    $order->invoicesCreate();
                   /* if($order->invoicesCreate()){
                    	if(!Yii::$app->cart->isPaymentRequired($sessionVar) || !Yii::$app->cart->isPaymentRequired('clientPortal'))
                    		$order->status='failed';
                    	else
                    		$order->status='processing';
					}
					else{
						$order->status='complete';
					}*/
	          //       if($order_status==1)
	          //   		$order->status='processing'; 
	        		// else      
	          //   		$order->status='complete';      
	        		$order->save(false); 
                    return $this->render('_success',compact('orderId'));
                }

        //}    
    }

    public function actionIpn(){
        
        $orderId = 1;
        
        // code for processing payflow response

        $this->afterPayment($order);
        $order->sendOrderConfirmation();
        $order->sendOrderNotification();
    }

    public function actionIpnprocess(){
        //foreach ($_REQUEST as $paymentDetails) {
        $paymentDetails = $_REQUEST;
            if(isset($paymentDetails['payment_status'])){
                if(isset($paymentDetails['item_number'])){
                    if(isset($paymentDetails['payment_status']) && $paymentDetails['payment_status']=='Completed'){
                        $orderId = $paymentDetails['item_number'];
                        $order = Orders::findOne($orderId);
                        $order->status = "in-process";
			$order->paymentMethod = "Paypal";
                        $order->save(false);
                        $this->afterPayment($order);
                        $order->sendOrderConfirmation();
                        $order->sendOrderNotification();
                    }
                }
            }
        //}
    }

    public function actionSuccess(){
        $orderId = $_REQUEST['item_number'];
        return $this->render('_success',compact('orderId'));
    }

    public function afterPayment($order){
        if($order->hasGiftVoucher()){ //if this order has a product of type 'gift-voucher'
            foreach($order->giftVouchers as $item){
                //die(json_encode(['recipientName'=>'Test User', 'recipientEmail'=>'test@test.com', 'recipientMessage'=>'Recipient Message']));
                for($i = 1; $i <= $item->quantity; $i++){
                    $config = json_decode($item->config, true);
                    $voucher = new GiftVouchers;
                    $voucher->code = Helper::getVoucherCode($item->id);
                    $voucher->initialAmount = $item->price;
                    $voucher->balance = $item->price;
                    $voucher->status = "active";
                    $voucher->creatorId = $order->customerId;
                    $voucher->orderId = $order->id;
                    $voucher->recipientName = $config['recipientName'];
                    $voucher->recipientEmail = $config['recipientEmail'];
                    $voucher->recipientMessage = $config['recipientMessage'];
                    $voucher->expiredDate = date('Y-m-d H:i:s', strtotime("+1 year", time()));  
                    $voucher->storeId = $order->storeId;
                    $voucher->save(false);
                    $voucher->sendEmail();
                }
            }
        }
        if($vouchers = $order->hasGiftVouchersApplied()){
            foreach($vouchers as $id => $amount){
                $vouch = GiftVouchers::findOne($id);
                $vouch->status = "used";
                $vouch->balance = $vouch->balance - $amount; //abs(($order->subTotal + $order->shippingAmount) - $amount);
                $vouch->lastUsedDate = date('Y-m-d H:i:s');
                $vouch->save(false);
                $usage = new \common\models\VoucherUsage;
                $usage->voucherId = $vouch->id;
                $usage->userId = $order->customerId;
                $usage->orderId = $order->id;
                $usage->save(false);
            }
        }
        if($coupons = $order->hasCouponsApplied()){
            foreach($coupons as $id => $amount){
                $coupon = \common\models\PriceRules::findOne($id);
                $coupon->couponUsage = $coupon->couponUsage + 1;
                $coupon->save(false);
                $usage = new \common\models\CouponUsage;
                $usage->ruleId = $coupon->id;
                $usage->userId = $order->customerId;
                $usage->orderId = $order->id;
                $usage->save(false);
            }
        }
    }

    public function doPayment($order, $voucherDiscount = 0){

        $store = Stores::findOne(Yii::$app->params['storeId']);

        if($store->getPaymentMethod() == "braintree") { //die('braintree');
        
            $braintree = Yii::$app->braintree;

            $amount = $order['grandTotal'] - $voucherDiscount;
            $response = $braintree->call('Transaction', 'sale', [
                'amount' => money_format('%!.2n', trim($order['grandTotal'])),
                'paymentMethodNonce' => $_POST["payment_method_nonce"],
                'orderId' => $order->id,
                'options' => [
                    'submitForSettlement' => True
                ],
               // 'deviceData' => $_POST['device_data'],
            ]);
        }
        
        elseif($store->getPaymentMethod() == "eway"){
            if(Yii::$app->user->isGuest || $order->billingAddressId == "" || is_null($order->billingAddressId)){
                $firstname = $order->billing_firstname;
                $lastname = $order->billing_lastname;
                $email = $order->email;
                $street1 = $order->billing_street;
                $city1 = $order->billing_city;
                $postalCode = $order->billing_postcode;
                $country = 'au';
            }else{
                $address = UserAddresses::findOne($order->billingAddressId);
                $firstname = $address->firstName;
                $lastname = $address->lastName;
                $email = $order->customer->email;
                $street1 = $address->streetAddress;
                $city1 = $address->city;
                $postalCode = $address->zip;
                $country = 'au';
            }
        	$eway = Yii::$app->eway;
			$eway->transaction = [
			    'Customer' => [
                    'FirstName' => $firstname,
                    'LastName' => $lastname,
                    'Email' => $email,
                    'Street1' => $street1,
                    'City' => $city1,
                    'PostalCode' => $postalCode,
                    'Country' => $country,
	            'Country' => 'au',		
                    'CardDetails' => [
			            'Name' => $_POST['EWAY_CARDNAME'],
			            'Number' => $_POST['EWAY_CARDNUMBER'],
			            'ExpiryMonth' => $_POST['EWAY_CARDEXPIRYMONTH'],
			            'ExpiryYear' => $_POST['EWAY_CARDEXPIRYYEAR'],
			            'CVN' => $_POST['EWAY_CARDCVN'],
			        ]
			    ],
			    'Payment' => [
			        'TotalAmount' => $eway->convertAmount(money_format('%!.2n', trim($order['grandTotal']))),
			        'InvoiceReference' => $order->id,
                    'InvoiceDescription' => 'Online Sale',
			    ],
                'CustomerIP' => (isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER['REMOTE_ADDR']),
		    	'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
			];

            //var_dump($eway->transaction);die();

			$response = Yii::$app->eway->processTransaction();

            /*$eway = Yii::$app->eway;
            $eway->transaction = [
                'Payment' => [
                    'TotalAmount' => 1000,
                ],
                'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
                'SecuredCardData' => $_POST['SecuredCardData'],
            ];

            $response = Yii::$app->eway->processTransaction();*/

        }    

        //var_dump($response);die();

       return $response;
        
    }

    public function emptyCart(){
        Yii::$app->cart->removeAll();
        $where = Yii::$app->user->isGuest? compact('sessionId') : ['userId' => Yii::$app->user->id];
        if($savedCart = Carts::find()->where($where)->one())
            $savedCart->delete(); //deleting the abandoned cart if exists
    }

    public function placeOrder($orderId = null){  //var_dump($_POST['CheckoutForm']);die();

        if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
            $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
        else
            $sessionVar = 'clientPortal';

    	$shippingAmount = 0;
    	$cart = Yii::$app->cart;
        if(!$order = Orders::findOne($orderId))
    	   $order = new Orders;
    	if(Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}()){ //die('hasOnlyByodProducts');
    		//$order->type = 'byod';
            if($sessionVar == "studentByod")
    		    $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['studentByod'],'storeId'=>Yii::$app->params['storeId']])->one();
            elseif($sessionVar == "schoolByod")
                $portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session['schoolByod'],'storeId'=>Yii::$app->params['storeId']])->one();
            else
                $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['clientPortal'],'storeId'=>Yii::$app->params['storeId']])->one();

            $order->type = ($sessionVar == "clientPortal") ? "client-portal" : "byod";
    		
            $order->portalId = $portal->id;
    	}
        elseif(Yii::$app->cart->hasOnlyClientPortalProducts()){ //die('hasOnlyByodProducts');
            $order->type = 'client-portal';
            $clientPortal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['clientPortal'],'storeId'=>Yii::$app->params['storeId']])->one();
            $order->portalId = $clientPortal->id;
        }
    	else		
    		$order->type = 'b2c';
    	//(Yii::$app->cart->hasOnlyByodProducts()) ? 'byod' : 'b2c';
        $order->storeId = Yii::$app->params['storeId'];
    	$order->customerId = Yii::$app->user->id;
        if(isset($_POST['CheckoutForm']['shipping_method'])){  //var_dump($_POST['CheckoutForm']['shipping_method']);die();
            if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
                $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
            else
                $sessionVar = 'clientPortal';

            if(Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}()){
                $shippingAmount  = 0;
                $order->shippingMethodId = $_POST['CheckoutForm']['shipping_method'];
            }
            else{    
                $shippingMethod = \common\models\ShippingMethods::findOne($_POST['CheckoutForm']['shipping_method']);
                $order->shippingMethodId = $_POST['CheckoutForm']['shipping_method'];
                $order->shippingAmount = $shippingMethod->getCalculatedPrice($cart->cost);
                $shippingAmount = $order->shippingAmount;
            }    
        }
        //die('Shipment type not set');
    	$order->subTotal = $cart->cost;
    	$order->grandTotal = number_format($cart->getCost(true,true) + $shippingAmount, 2, '.', '');
        $order->status = "failed";
        $totalDiscount = 0;
        if(isset(Yii::$app->session['appliedVouchers'])){
            $order->vouchers = json_encode(Yii::$app->session['appliedVouchers']);
            //Yii::$app->session['appliedVouchers'] = null;
        }

        if(isset(Yii::$app->session['appliedCoupons'])){
            $order->coupons = json_encode(Yii::$app->session['appliedCoupons']);
            foreach(Yii::$app->session['appliedCoupons'] as $id => $details){
                if(is_array($details)){ // it's a per product coupon
                    $discount = array_values($details)[0];
                    $totalDiscount += $discount;
                    // if(key($details) == $position->product->id){
                    //     $item->coupons = json_encode([$id => $details]);
                    // }
                }else //per cart coupon
                    $totalDiscount += $details;
            }
            //Yii::$app->session['appliedCoupons'] = null;
        }
        if(!Yii::$app->user->isGuest)
            $order->customerId = Yii::$app->user->id;
        if(isset($_POST['CheckoutForm']['billingAddress']['customerAddressId']) && $_POST['CheckoutForm']['billingAddress']['customerAddressId'] != "new"){
            $order->billingAddressId = $_POST['CheckoutForm']['billingAddress']['customerAddressId'];
        }else{
            $order->email = $_POST['CheckoutForm']['email'];
            $order->billing_firstname = $_POST['CheckoutForm']['billing_firstname'];
            $order->billing_lastname = $_POST['CheckoutForm']['billing_lastname'];
            $order->billing_lastname = $_POST['CheckoutForm']['billing_lastname'];
            $order->billing_company = $_POST['CheckoutForm']['billing_company'];
            $order->billing_street = $_POST['CheckoutForm']['billing_street'];
            $order->billing_city = $_POST['CheckoutForm']['billing_city'];
            $order->billing_state = $_POST['CheckoutForm']['billing_state'];
            $order->billing_postcode = $_POST['CheckoutForm']['billing_postcode'];
            $order->billing_country = $_POST['CheckoutForm']['billing_country'];
            $order->billing_phone = $_POST['CheckoutForm']['billing_phone'];
            $order->billing_fax = $_POST['CheckoutForm']['billing_fax'];
        }

        if(isset($_POST['CheckoutForm']['sameAsBilling'])){
            if(!is_null($order->billingAddressId)){
                $order->shippingAddressId = $order->billingAddressId;
            }else{
                $order->shipping_firstname = $_POST['CheckoutForm']['billing_firstname'];
                $order->shipping_lastname = $_POST['CheckoutForm']['billing_lastname'];
                $order->shipping_lastname = $_POST['CheckoutForm']['billing_lastname'];
                $order->shipping_company = $_POST['CheckoutForm']['billing_company'];
                $order->shipping_street = $_POST['CheckoutForm']['billing_street'];
                $order->shipping_city = $_POST['CheckoutForm']['billing_city'];
                $order->shipping_state = $_POST['CheckoutForm']['shipping_state'];
                $order->shipping_postcode = $_POST['CheckoutForm']['billing_postcode'];
                $order->shipping_country = $_POST['CheckoutForm']['billing_country'];
                $order->shipping_phone = $_POST['CheckoutForm']['billing_phone'];
                $order->shipping_fax = $_POST['CheckoutForm']['billing_fax'];
            }
        }else{
            if(isset($_POST['CheckoutForm']['shippingAddress']['customerAddressId']) && $_POST['CheckoutForm']['shippingAddress']['customerAddressId'] != "new"){
                $order->shippingAddressId = $_POST['CheckoutForm']['shippingAddress']['customerAddressId'];
            }elseif(isset($_POST['CheckoutForm']['shipping_firstname'])){
                $order->shipping_firstname = $_POST['CheckoutForm']['shipping_firstname'];
                $order->shipping_lastname = $_POST['CheckoutForm']['shipping_lastname'];
                $order->shipping_lastname = $_POST['CheckoutForm']['shipping_lastname'];
                $order->shipping_company = $_POST['CheckoutForm']['shipping_company'];
                $order->shipping_street = $_POST['CheckoutForm']['shipping_street'];
                $order->shipping_city = $_POST['CheckoutForm']['shipping_city'];
                $order->shipping_state = $_POST['CheckoutForm']['shipping_state'];
                $order->shipping_postcode = $_POST['CheckoutForm']['shipping_postcode'];
                $order->shipping_country = $_POST['CheckoutForm']['shipping_country'];
                $order->shipping_phone = $_POST['CheckoutForm']['shipping_phone'];
                $order->shipping_fax = $_POST['CheckoutForm']['shipping_fax'];
            }
        }
        if(isset($_POST['CheckoutForm']['storeAddressId'])){
            $order->collectFrom = $_POST['CheckoutForm']['storeAddressId'];
        }    
        //var_dump($order->attributes); die;
        //return $order;
    	if($order->save(false)){ //die('Order Placed');  var_dump($cart->positions); die;
	    if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
                $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
            else
                $sessionVar = 'clientPortal';

    		if(Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}()){
	    		if($portal->shipment_type == 'delivery-program'){
	    			foreach ($_POST['OrderMeta'] as $key => $value) {
	    				$orderMeta = new OrderMeta();
	    				OrderMeta::setValue(''.$key.'', $value, $order->id);
	    			} 
	    		}
	    	}	

            $order->orderDate = date('Y-m-d H:i:s');
    		foreach($cart->positions as $position){ //die('fdsdsdf');
			if($item = OrderItems::find()->where(['productId' => $position->product->id, 'config' => json_encode($position->config), 'orderId' => $orderId])->one())
                    		continue;
    			$item = new OrderItems;
    			$item->orderId = $order->id;
    			$item->productId = $position->product->id;
    			$item->quantity = $position->quantity;
                $item->giftWrap= $position->giftWrap;
                $item->giftWrapAmount= $position->giftWrapCost;
    			$item->price = $position->price;
                $item->comment=$position->comments;
    			$item->config = json_encode($position->config);
                if(isset(Yii::$app->session['appliedCoupons'])){
                    foreach(Yii::$app->session['appliedCoupons'] as $id => $details){
                        if(is_array($details)){ // it's a per product coupon
                            if(key($details) == $position->product->id){
                                $item->coupons = json_encode([$id => $details]);
                                $item->discount = array_values($details)[0];
                            }
                        }
                    }
                }    
    			if($item->save(false)){ //var_dump($order->id); die;
    				if($position->product->typeId=="configurable"){
                        $childProduct = $position->childProduct;
    					// $linkages = ProductLinkages::find()->where(['parent' => $position->product->id])->all();
    					// foreach($linkages as $link){ //logic for finding the child product with given config
    					// 	foreach($position->config as $config){ 
         //                        $linkedProductQuery = new \yii\db\ActiveQuery('common\models\Products');
         //                        $linkedProduct = $linkedProductQuery->from('Products')->where('id = '.$link->productId)->one();
    					// 		if($linkedProduct->{$config['attr']['code']} == $config['value']){
    					// 			$childProduct = $linkedProduct;
         //                            break;
         //                        }else
    					// 			$childProduct = false;
    					// 	}
    					// 	if($childProduct) break;
    					// }
    					$child = new OrderItemChildren;
    					$child->parentProductId = $position->id;
    					$child->orderItemId = $item->id;
    					$child->productId = $childProduct->id;
                        $childProduct = '100';
    					$child->quantity = 1;
    					$child->type = "configurable";
    					$child->save(false);
    				}elseif($position->product->typeId=="bundle"){
    					foreach($position->config as $productId => $config){
	    					$child = new OrderItemChildren;
	    					$child->parentProductId = $position->id;
	    					$child->orderItemId = $item->id;
	    					$child->productId = $productId;
	    					$child->quantity = $config['qty'];
	    					$child->type = "bundle";
	    					$child->save(false);
    					}
    				}
    			}
    		}
            //$order->sendOrderConfirmation();
	    $order->addActivity('order-placed');
            return $order;
    	}else{  //die('failed');
            return false;
        }
    }

    public function actionApplyvoucher(){

        if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']))
            $sessionVar = isset(Yii::$app->session['studentByod']) ? "studentByod" : "schoolByod";
        else
            $sessionVar = 'clientPortal';

        if(Yii::$app->cart->{"hasOnly".ucfirst($sessionVar)."Products"}()){
           return json_encode(['status' => 'failed']);
        }
        $code = $_POST['code'];
        $grandTotal = $_POST['grandTotal']; 
        if($voucher = GiftVouchers::find()->where("code='$code' AND expiredDate >= '".date('Y-m-d H:i:s')."' AND (status='active' || status='used') AND storeId = '".Yii::$app->params['storeId']."' AND balance!='0'")->one()){  
            if($grandTotal < $voucher->balance){
                $discount = $grandTotal;
                $balance = $voucher->balance - $grandTotal;
            }elseif($grandTotal >= $voucher->balance){ //var_dump($voucher->balance); die;
                $balance = 0;
                $discount = $voucher->balance;
            }
            if(isset(Yii::$app->session['appliedVouchers'][$voucher->id]))
                return json_encode(['status' => 'already-applied']);
            if(!isset(Yii::$app->session['appliedVouchers']))
                Yii::$app->session['appliedVouchers'] = [];
            Yii::$app->session['appliedVouchers'] = (Yii::$app->session['appliedVouchers'] + [$voucher->id => $discount]);
            return json_encode(['status' => 'success', 'code' => $code, 'id' => $voucher->id, 'discount' => Yii::$app->cart->voucherDiscount, 'discountFromThis' => $discount]);
        }else{
            return json_encode(['status' => 'failed']);
        }
    }

    public function actionRemovevoucher(){
        $id = $_POST['id'];
        $session = Yii::$app->session['appliedVouchers'];
        $discountFromThis = $session[$id];
        unset($session[$id]);
        Yii::$app->session['appliedVouchers'] = $session;
        return json_encode(["status" => "success", 'discount' => Yii::$app->cart->voucherDiscount, 'discountFromThis' => $discountFromThis]);
    }

    public function actionTest(){
        $order = Orders::findOne(442);
        var_dump($order->fromPaymentGateway); die;
    }
    
    public function actionTest1() {

        $eway = Yii::$app->eway;

//        $customer = [
//            'CardDetails' => [
//                'Name' => 'John Smith',
//                'Number' => '4444333322221111',
//                'ExpiryMonth' => '12',
//                'ExpiryYear' => '25',
//                'CVN' => '123',
//            ],
//        ];
//        
//        $eway->transaction = [
//            'Customer' => $customer,
//            'Capture' => true,
//            'Payment' => [
//                'TotalAmount' => 1000,
//            ],
//            //'CustomerIP' => $_SERVER['REMOTE_ADDR'],
//            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
//        ];
        //var_dump($eway->transaction);die();
        //$response = Yii::$app->eway->processTransaction();



        $response = Yii::$app->eway->refundAmount('14157631',$eway->convertAmount(100));

        print_r($response);
    }

}
