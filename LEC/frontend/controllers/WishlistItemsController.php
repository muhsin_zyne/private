<?php

namespace frontend\controllers;

use Yii;
use common\models\WishlistItems;
use common\models\search\WishlistItemsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Products;
use yii\data\ActiveDataProvider;

/**
 * WishlistitemsController implements the CRUD actions for WishlistItems model.
 */
class WishlistitemsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WishlistItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $model = new WishlistItems();
        // if (Yii::$app->request->post())
        // {
        //     var_dump(Yii::$app->request->post());die;
        // }
        // $searchModel = new WishlistItemsSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
        $dataProvider = new ActiveDataProvider([
            'query' => WishListItems::find()->where(['userId' => Yii::$app->user->id,'storeId'=>Yii::$app->params['storeId']])->orderBy(['id'=>SORT_DESC,]),
        ]);
        return $this->render('index', [
           
            'dataProvider' => $dataProvider,
        ]);         

    }

    /**
     * Displays a single WishlistItems model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionComments()
    {
       $store_id=$_POST['store_id'];
       $user_id=$_POST['user_id'];
       $wlarray=explode(",",$_POST['wlcomments']);
       //var_dump($wlarray[0]);
       $i=0;
       $wlitem = WishlistItems::find()->where(['userId' =>$user_id,'storeId' =>$store_id])->all(); 
        foreach ($wlitem as $key => $value) {
            $wlmodel = WishlistItems::find()->where(['id' =>$value['id']])->one(); 
            $wlmodel->comments=$wlarray[$i];
            $wlmodel->save();
            $i++;
        }
        return $this->redirect(['site/account']);
    }

    /**
     * Creates a new WishlistItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //var_dump($_POST);die;
        if(!$model = WishlistItems::find()->where(['userId'=>$_POST['user_id'],'storeId'=>$_POST['store_id'],'productId'=>$_POST['product_id']])->one()){
            $model = new WishlistItems();//die('poii');
            $model->productId=$_POST['product_id'];
            $model->userId=$_POST['user_id'];
            $model->storeId=$_POST['store_id'];
            $model->save(false);
                      
        }
        return $this->redirect(['/site/wishlist']);   
    }
    

    /**
     * Updates an existing WishlistItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionConfig($id)
    {
        $model = $this->findModel($id);
        if(\Yii::$app->request->isAjax)
        {  
            return $this->renderPartial('config',compact('model'));
        }
        
    }

    /**
     * Deletes an existing WishlistItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WishlistItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WishlistItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WishlistItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
