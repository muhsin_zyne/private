<?php

namespace frontend\controllers;

use Yii;
use common\models\UserAddresses;
use common\models\User;
use common\models\UserAdressesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\FrontController;
/**
 * AddressesController implements the CRUD actions for UserAddresses model.
 */
class AddressesController extends FrontController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserAddresses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserAdressesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserAddresses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserAddresses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserAddresses();

        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);
       
        if ($model->load(Yii::$app->request->post())) {  //var_dump(Yii::$app->request->post());die();
            $model->userId = $user->id;
            if($_POST['UserAddresses']['defaultBilling'] == "true"){ 
                $model->defaultBilling = 1;
                if($defaultBillingAddress = UserAddresses::find()->where(['userId'=>$id,'defaultBilling'=>1])->one()){
                    $defaultBillingAddress->defaultBilling = 0;
                    //$defaultBillingAddress->save(false);
                }
            }
            if($_POST['UserAddresses']['defaultShipping'] == "true"){ 
                $model->defaultShipping = 1;
                if($defaultShippingAddress = UserAddresses::find()->where(['userId'=>$id,'defaultShipping'=>1])->one()){
                    $defaultShippingAddress->defaultShipping = 0;
                    //$defaultShippingAddress->save(false);
                }
            } 
            if($model->save()){
                die('Address saved successfully');
            }
            else{
                var_dump($model->errors);die('error');
            }
        } else {
           if(\Yii::$app->request->isAjax){
                return $this->renderAjax('_form',compact('model','user'));
            }
            return $this->render('create', compact('model','user'));
        }
    }

    /**
     * Updates an existing UserAddresses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /*$model = $this->findModel($id);
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);

        if ($model->load(Yii::$app->request->post())){ 
            if($_POST['UserAddresses']['defaultBilling'] == "true"){ 
                $model->defaultBilling = 1;
                if($defaultBillingAddress = UserAddresses::find()->where(['userId'=>$id,'defaultBilling'=>1])->one()){
                    $defaultBillingAddress->defaultBilling = 0;
                    //$defaultBillingAddress->save(false);
                }
            }
            else{
                $model->defaultBilling = 0;
            }

            if($_POST['UserAddresses']['defaultShipping'] == "true"){ 
                $model->defaultShipping = 1;
                if($defaultShippingAddress = UserAddresses::find()->where(['userId'=>$id,'defaultShipping'=>1])->one()){
                    $defaultShippingAddress->defaultShipping = 0;
                    //$defaultShippingAddress->save(false);
                }
            }
            else{
               $model->defaultShipping = 0; 
            }
            //var_dump($model);die();
            $model->save(false);

            
        } else {
                if(\Yii::$app->request->isAjax){
                    return $this->renderAjax('_form',compact('model','user'));
                }
                return $this->render('update', compact('model','user'));
        }*/

        $model = $this->findModel($id);
        $id= \Yii::$app->user->identity->id;
        $user=User::findOne($id);

        if($model->load(Yii::$app->request->post())){
            if($_POST['UserAddresses']['defaultBilling'] == "true"){ 
                $model->defaultBilling = 1;
                if($defaultBillingAddress = UserAddresses::find()->where(['userId'=>$id,'defaultBilling'=>1])->one()){
                    $defaultBillingAddress->defaultBilling = 0;
                    $defaultBillingAddress->save(false);
                }
            }
            else{
                $model->defaultBilling = 0;
            }
            if($_POST['UserAddresses']['defaultShipping'] == "true"){ 
                $model->defaultShipping = 1;
                if($defaultShippingAddress = UserAddresses::find()->where(['userId'=>$id,'defaultShipping'=>1])->one()){  
                    $defaultShippingAddress->defaultShipping = 0;
                    $defaultShippingAddress->save(false);
                }
            }
            else{
                $model->defaultShipping = 0;
            }
            //var_dump($model);die();
            if($model->save()){
                die('saved successfully');
            }
        }
        else {
            if(\Yii::$app->request->isAjax){
                return $this->renderAjax('_form',compact('model','user'));
            }
            else
                return $this->render('update', compact('model','user'));
        }
    }

    /**
     * Deletes an existing UserAddresses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //var_dump($this->findModel($id));die();
        
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the UserAddresses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAddresses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAddresses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
