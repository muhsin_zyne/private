<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\FrontController;
use common\models\ConsumerPromotions;
use common\models\StorePromotions;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use common\models\Products;
use common\models\User;
use common\models\search\ProductsSearch;
use yii\widgets\Pjax;


class PromotionsController extends FrontController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($id)
    {
        $promotion = $this->findModel($id);

        if(!$storePromotion = StorePromotions::find()->where(['promotionId'=>$promotion->id,'storeId'=>Yii::$app->params['storeId'],'active'=>1])->one()){

                Yii::$app->session->setFlash('error','Sorry, this promotion is not active');
                return $this->redirect(\yii\helpers\Url::to('@web/'));
        }
        
        $searchModel = new \common\models\search\ProductsSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        $productsQuery = $promotion->getProducts(Yii::$app->params['storeId'], 'b2c');


        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        
        if(isset($_GET['sort']) && strpos($_GET['sort'], "price")!==FALSE){
            $query->join('LEFT JOIN', 'AttributeValues', 'AttributeValues.productId = Products.id');
            $query->join('LEFT JOIN', 'Attributes', 'Attributes.id = AttributeValues.attributeId');
            $query->andWhere('Attributes.code = "price"');
            $query->groupBy("Products.id");
        }

        $products = new ActiveDataProvider([
    		'query' => $query,
            'pagination' => [
                'pageSize' => isset($_GET['per-page'])? $_GET['per-page'] : 12,
            ],
        ]);

        //var_dump($products);die();

       	$products->setSort([
            'attributes' => [
                'dateAdded',
                'price' => [
                    'asc' => ['`AttributeValues`.`value`+0' => SORT_ASC],
                    'desc' => ['`AttributeValues`.`value`+0' => SORT_DESC]
                ]
            ]
        ]);
        $bestSellers = new ActiveDataProvider([
            'query' => Products::find()->orderBy('rand()')->limit(3),
            'pagination' => false,
        ]);
        if(\Yii::$app->request->isAjax){
            return $this->renderAjax('view', compact('products', 'promotion','storePromotion','bestSellers')); 
        }    
        else
            return $this->render('view', compact('products', 'promotion','storePromotion','bestSellers'));

    }

    protected function findModel($id)
    {
        if (($model = ConsumerPromotions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
?>    