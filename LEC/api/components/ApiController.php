<?php
namespace api\components;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


class ApiController extends \yii\rest\ActiveController
{
	public function behaviors(){
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	        'class' => CompositeAuth::className(),
	        'authMethods' => [
	            HttpBasicAuth::className(),
	            HttpBearerAuth::className(),
	            QueryParamAuth::className(),
	        ],
	    ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];
        return $behaviors;
	    return $behaviors;
	}

	protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'options' => ['OPTIONS'],
        ];
    }

	public function beforeAction($action){
		$this->checkAccess($action);
		return parent::beforeAction($action);
	}

	public function checkAccess($action, $model = null, $params = [])
    {
		//throw new \yii\web\ForbiddenHttpException('Not Allowed');
    }

    public static function getAuthenticatedUser(){
    	return \common\models\User::findIdentityByAccessToken(Yii::$app->request->authUser);
    }

    public function actionOptions(){
    	return "ok";
    }
}
