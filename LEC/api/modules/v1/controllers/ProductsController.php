<?php
namespace api\modules\v1\controllers;
use common\models\Products;
use yii\web\NotFoundHttpException;
use Yii;

class ProductsController extends \api\components\ApiController
{
	public $modelClass = '\common\models\Products';
	public function actions(){
		return [
			// 'index' => [
	  //           'class' => 'yii\rest\IndexAction',
	  //           'modelClass' => $this->modelClass,
	  //           'checkAccess' => [$this, 'checkAccess'],
   //      	],
        	// 'view' => [
	        //     'class' => 'yii\rest\ViewAction',
	        //     'modelClass' => $this->modelClass,
	        //     'checkAccess' => [$this, 'checkAccess'],
        	// ]
        ];
	}
	
	public function actionIndex(){ 
		$searchModel = new \common\models\search\ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(isset($_GET['offset']))
        	$dataProvider->offset = $_GET['offset'];
		return $dataProvider;
	}

	public function actionView($id){
		// var_dump($_REQUEST); die;
		// var_dump($id); die;
		$product = $this->findModel($id);
        // if ($this->checkAccess) {
        //     call_user_func($this->checkAccess, $this->id, $product);
        // }
        return $product;
	}

	protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
