<?php
namespace api\modules\v1\controllers;
use Yii;
class AuthenticationController extends \yii\rest\Controller{

	public function actionCreate(){
		$model = new \common\models\LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			$hash = md5(uniqid(rand(), TRUE));
			if(\api\components\ApiController::getAuthenticatedUser())
				return ["accessToken" => Yii::$app->request->authUser];
			$token = new \common\models\AuthTokens;
			$token->userId = $model->user->id;
			$token->type = 'User';
			$token->token = $hash;
			$token->expiresAt = date("Y-m-d H:i:s", strtotime("+1 month"));
			$token->save(false);
			return ["accessToken" => $hash];
		}
		throw new \yii\web\UnauthorizedHttpException('Invalid Credentials');
	}

}