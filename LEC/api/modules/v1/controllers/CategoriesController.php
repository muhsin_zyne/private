<?php
namespace api\modules\v1\controllers;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\Categories;

class CategoriesController extends \api\components\ApiController
{
	public $modelClass = '\common\models\Categories';
	public function actions(){
		return [];
	}
	
	public function actionIndex(){
		// $store = $this->authenticatedUser->store;
		// $storeCategories = Yii::$app->db->createCommand("select pc.categoryId from ProductCategories pc join StoreProducts sp on sp.productId = pc.productId join Products p on p.id = sp.productId where sp.storeId = ".$store->id." and (pc.storeId = '".$store->id."' OR pc.storeId = 0) and sp.enabled = 1 and p.status = 1 group by pc.categoryId")
  //       ->queryAll();
  //       return ArrayHelper::map($storeCategories, 'categoryId', 'parent');
		$exclusions = ['PINK STYLE', 'GIFT IDEAS', 'THREAD-THROUGH', 'FEATURED JEWELLERY', 'SERVICE ITEMS'];
		$baseCondition = "";
		if(isset($_GET['onlyBase']))
			$baseCondition = "image IS NOT NULL AND image != ''";
		elseif(isset($_GET['onlyRoot']))
			$baseCondition = "parent = 0";
		$items = Categories::find()
                ->where(['active' => '1'])
                ->where("EXISTS (SELECT 1 FROM ProductCategories pc WHERE  pc.categoryId = Categories.id)")
                ->andWhere("title NOT IN ('".implode("','", $exclusions)."')")
                ->andWhere($baseCondition)
                ->orderBy('position asc')
                //->asArray()
                ->all();
        return $items;
		//return $this->authenticatedUser->store->activeCategoryIds;
	}

	public function actionFilters($id){ 
		$attributeExceptions = ['f_size', 'country_of_manufacture', 'length'];
		if(!$category = Categories::findOne($id))
			throw new \yii\web\NotFoundHttpException('The requested data does not exist.');
		$attributes = $category->filterAttributes;
		$filterAttributes = [];
		foreach($attributes as $i => $attribute){
			$values = $category->getFilterAttributeValues($attribute->code);
			if(!in_array($attribute->code, $attributeExceptions) && count($values) > 1){
				$filterAttributes[$i] = $attribute;
				$filterAttributes[$i]->values = ArrayHelper::map($values, 'id', 'value');
			}
		}
		return $filterAttributes;
	}

	public function actionOptions(){
    	return "ok";
    }

}
