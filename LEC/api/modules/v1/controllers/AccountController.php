<?php
namespace api\modules\v1\controllers;
use common\models\User;
use yii\web\NotFoundHttpException;
use Yii;

class AccountController extends \api\components\ApiController
{
	public $modelClass = '\common\models\User';
	public function actions(){
		return [];
	}
	public function actionView(){
		$user = $this->authenticatedUser;
		//$user->logo = $user->store->logo;
		return $user;
	}

}