<?php
 
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
 
return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
	'treemanager' => [
            'class' => '\kartik\tree\Module',

            'treeStructure' => [
                    'treeAttribute' => 'root',
                    'leftAttribute' => 'lft',
                    'rightAttribute' => 'rgt',
                    'depthAttribute' => 'lvl',
                 ],

            'dataStructure' => [
                    'keyAttribute' => 'id',
                    'nameAttribute' => 'title',
                    'iconAttribute' => 'icon',
                    'iconTypeAttribute' => 'icon_type'
                ],
	],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/authentication',   
                    'pluralize' => false,
                    // 'tokens' => [
                    //     '{id}' => '<id:\\w+>'
                    // ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/account',   
                    'pluralize' => false,
                    'only' => ['view'],
                    'patterns' => [
                        'GET,HEAD' => 'view'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/products',   
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/categories', 
                    'pluralize' => false,  
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                    'patterns' => [
                        'GET,HEAD' => 'index',
                        'GET {id}' => 'filters'
                    ]
                ],
            ],
        ]
    ],
    'params' => $params,
];
