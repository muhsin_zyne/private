<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'mahirocz@gmail.com',
                'password' => 'whatis123',
                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
    ],   
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'params' => [
        'temp' => [],
        'b2c-main' =>['1920X655'=>'1920px X 655px','824X440'=>'824px X 440px','856X406'=>'856px X 406px'], // b2c main banner 
        'b2c-mini' =>['325X210'=>'325px X 210px'], // b2c mini banner 
        'b2c-side' =>['320X395'=>'320px X 395px'], //b2c side banner
        'b2c-strip' =>['1169X143'=>'1169px X 143px'], // b2c stip banner
        'b2c-mobile' =>['650X325'=>'650px X 325px'], // b2c mobile banner
        'b2b-main' =>['1000X400'=>'1000px X 400px'],  // b2b main banner
        'b2b-mini' =>['312X158'=>'312px X 158px'], // b2b mini banner
        'b2b-side' =>['285X346'=>'285px X 346px'], // b2b side banner
    ]
];
