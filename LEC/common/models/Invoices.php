<?php

namespace common\models;
use frontend\components\Helper;
use yii\widgets\ListView;
use kartik\mpdf\Pdf;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "Invoices".
 *
 * @property integer $id
 * @property integer $orderId
 * @property string $comments
 * @property string $subTotal
 * @property string $shipping
 * @property string $grandTotal
 * @property string $createdDate
 */
class Invoices extends \yii\db\ActiveRecord
{
    public $reportFrom = null;
    public $reportTo = null;
    public $reportStoreId = 0;
    public $filename;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Invoices';
    }

    /**
     * @inheritdoc
     */
    public $chkmail;
    public $shipment_chk;
    public $shipment_carrier;
    public $shipment_title;
    public $shipment_trackingNumber;
    public $ship_chkmail;
    public function rules()
    {
        return [
            [['orderId', 'subTotal', 'shipping', 'grandTotal'], 'required'],
            [['orderId'], 'integer'],
            [['comments'], 'string'],
            [['subTotal', 'shipping', 'grandTotal'], 'number'],
            [['createdDate', 'chkmail','shipment_chk','notify'], 'safe'],

            [['shipment_carrier','shipment_title','shipment_trackingNumber'], 'required', 'when' => function ($model) {
                return $model->shipment_chk;
            }, 'whenClient' => "function (attribute, value) {                
                    return $('#invoices-shipment_chk').is(':checked');
                }
            "],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Invoice #',
            'orderId' => 'Order #',
            'comments' => 'Comments',
            'subTotal' => 'Sub Total',
            'invoicePlacedDate' => 'Invoice Date',
            'orderBillToAddress'=>'Bill To Address',
            'shipping' => 'Shipping',
            'grandTotal' => 'Grand Total',
            'notify' => 'Notify',
            'createdDate'=>'Invoice Date',
            'shipment_carrier'=>'Carrier',
            'shipment_title'=>'Tracking ID',
            'shipment_trackingNumber'=>'Tracking Link',
            'ship_chkmail'=>'Shipment Mail',
        ];
    }

    public function getTotalCount(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getOrderCount(){
        return Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getInvoicedOrdersCount(){
        $query = Orders::find()
        ->join('LEFT JOIN', 'Invoices inv', 'Orders.id = inv.orderId')
        ->where('inv.createdDate >= "'.$this->reportFrom.'" AND inv.createdDate <= "'.$this->reportTo.'"')
        ->andWhere('EXISTS (SELECT 1 FROM Invoices i WHERE i.orderId = Orders.id)');
        if($this->reportStoreId != 0)
            $query->andWhere("Orders.storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getInvoiced(){
        return Invoices::find()
        ->join('LEFT JOIN', 'Orders o', 'Invoices.orderId = o.id')
        ->where('createdDate >= "'.$this->reportFrom.'" AND createdDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("o.storeId = {$this->reportStoreId}");
        return $query->sum('grandTotal');
    }

     public function sendInvoiceMail(){    
        if($this->order->fromRegisteredUser()==true)
        {
            $user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        }
        else
        {
            $userfullname=ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }
        //var_dump( $useremail);die;
        $invoice = \common\models\Invoices::findOne($this->id);
        $template = \common\models\EmailTemplates::find()->where(['code' => 'invoice'])->one();
        //$user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
        $store = \common\models\Stores::find()->where(['id' => $this->order->storeId])->one();
        $order = \common\models\Orders::find()->where(['id' => $this->orderId])->one();
        //var_dump($invoice);die;
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store'=>$store,'order'=>$order,'invoice'=>$invoice]; 
        $queue->replacements =[ 'logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage, 'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$this->order->storeAddress,];

        $queue->from = "no-reply";
        $queue->recipients =  $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($order);die;
        //var_dump($queue->message);die;
        $queue->save();
    }

    public function generatePdf() {    

        $invoice = $this;
        $filename = \Yii::$app->basePath."/../store/attachments/Invoice_".$this->id."_".$this->order->id.".pdf";
        $content = Yii::$app->controller->renderPartial('@common/views/_pdfContent',compact('invoice'));
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'filename' => $filename, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            //'destination' => Pdf::DEST_FILE, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Tax Invoice'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Tax Invoice'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        /*$response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');*/
    
        // return the pdf output as per the destination setting
        return $pdf->render(); 


    }

    public function printInvoice(){    
        $invoice = $this;
        return Yii::$app->controller->renderPartial('@common/views/_pdfContent',compact('invoice'));
    }

    public function getInvoiceId(){    
        return 'E'.str_pad($this->id,5,"0",STR_PAD_LEFT);
    }
    public function getInvoicePlacedDate()
    {
        return Helper::date($this->createdDate, "j F, Y");
    }

    public function getShortGrid(){
        return ListView::widget([
             'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getInvoiceItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/invoices/_maillistview',
            //'itemView' => (Yii::$app->id == "frontend")?'/orders/_invoicelistview':'/invoices/_maillistview',
        ]);
    }

    public function getPdfShortGrid(){
        return ListView::widget([
             'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getInvoiceItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '@common/views/_pdflistview',
            //'itemView' => (Yii::$app->id == "frontend")?'/orders/_invoicelistview':'/invoices/_maillistview',
        ]);
    }

    /*public function getShortGrid(){
        return \yii\grid\GridView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getInvoiceItemsGrid(), 'sort' => false,'pagination'=> false,]),
            //['class' => 'yii\grid\SerialColumn'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                //'orderItemId',
                [
                    'label'=> 'Product Name',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return $model->orderItem->product->name; 
                    }
                ],
                [
                    'label' => 'Option(s)',
                    'value' => 'orderItem.superAttributeValuesText',
                    'format' => 'html'
                ],
                [
                    'label'=> 'SKU',
                    'attribute' => 'orderItem.sku',
                  
                ],
                [
                   'label' => 'Item Price',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money($model->orderItem->price / $model->orderItem->quantity );
                    },
                ],
                //'quantityInvoiced',
                [
                   'label' => 'Qty',
                    'value' => 'quantityInvoiced',
                ],
                [
                    'label'=> 'Discount',
                    'format' => 'html',
                     'value' => function ($model) {
                        return Helper::money($model->orderItem->discount);
                    },
                ],
                [
                   'label' => 'Item Total',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money((($model->orderItem->price / $model->orderItem->quantity) * $model->quantityInvoiced)-$model->orderItem->discount);
                    },
                ],
               /* [
                   'label' => 'Comment',
                    'value' => 'orderItem.comment',
                ],*/
            /*],
        ]);
    }*/
    public function getInvoiceItemsGrid(){    
        $query = new \yii\db\ActiveQuery('common\models\InvoiceItems');
        return $query->where("invoiceId=".$this->id."")->limit(10);
    }
    public function getInvoiceShippingAmount(){    
        return Helper::money($this->shipping);
    }
    public function getInvoiceItemTotalFormatted(){    
        return Helper::money($this->subTotal);
    }
    public function getInvoiceGrandTotalFormatted(){    
        return Helper::money($this->grandTotal);
    }
    public function getInvoiceItemsUrl(){    
        return '<a href="'.\yii\helpers\Url::to(['/invoice/view', 'id'=>$this->id]).'">';       
    }
    public function getOrder(){    
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }
    
    public function getOrderUsername(){    
        return $this->order->getBillingAddressText;
    }
    public function getOrderDate(){  
        return $this->order->orderDate;
    }
    public function getOrderBillToAddress(){    
        return $this->order->billingAddressText;
    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/tax-invoice.jpg></a>'; 
    }


}
