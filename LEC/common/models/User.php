<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\B2bAddresses;
use common\models\Stores;
use common\models\WishlistItems;
use frontend\components\Helper; 

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    public $password;
    public $confirmPassword;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Users}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function attributes(){
        return array_merge(parent::attributes(), ['test']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([
            // ['status', 'default', 'value' => self::STATUS_ACTIVE],
            // ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['auth_key', 'password_hash', 'email'], 'required'],
            [['roleId'], 'integer'],
            [['roleId'], 'integer'],
            //[['confirmPassword'],'password'],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'email', ], 'string', 'max' => 255],
            [['firstname', 'lastname'], 'string', 'max' => 45],
            [['password','confirmPassword','created_at','updated_at', 'accessToken'],'safe'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
        ], [[['test'], 'integer']]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'roleId' => 'Role ID',
        ];
    }

    public function extraFields(){
        return ['store', 'b2bAddresses', 'logo'];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        if(isset(Yii::$app->params['temp']['api']['authenticatedUser']))
            return Yii::$app->params['temp']['api']['authenticatedUser'];
        if($token = \common\models\AuthTokens::find()->where(['token' => $token, 'type' => 'User'])->andWhere("expiresAt >= '".date('Y-m-d H:i:s')."'")->one()){
            Yii::$app->params['temp']['api']['authenticatedUser'] = $token->user;
            return $token->user;
        }
        return false;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);   
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    } 

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
  
    /**
     * Generates password hash from password and sets it to the model
     *  Test text here ...
     * @param string $password
     */
    public function setPassword($password)
    {
        return $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
 
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
 
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getFullName(){
        return ucfirst($this->firstname)." ".ucfirst($this->lastname);
    }


    public function getStore(){

        //var_dump($this->id);die();
        return $this->hasOne(Stores::className(), ['adminId' => 'id']);
    }

    public function getCustomerStore(){
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }

    public function getRole(){
        return $this->hasOne(\common\models\UserRoles::className(), ['id' => 'roleId']);
    }

    public function getDefaultB2bAddress(){

        return $this->hasOne(B2bAddresses::className(), ['ownerId' => 'id'])->andWhere(['defaultAddress' => '1']);
    }

    public function getDefaultB2cBillingAddress(){
        return $this->hasOne(UserAddresses::className(), ['userId' => 'id'])->andWhere(['defaultBilling' => '1']);
    }

    public function getDefaultB2cShippingAddress(){
         return $this->hasOne(UserAddresses::className(), ['userId' => 'id'])->andWhere(['defaultShipping' => '1']);
    }

    public function getB2bAddresses(){

        return $this->hasMany(B2bAddresses::className(), ['ownerId' => 'id']);
    }

    public function hasBillingAddress(){
        return UserAddresses::find()->where(['userId' => $this->id])->exists();
    }

   public function sendActivationMail(){    
        $user_id=$this->id;
        $store_id=$this->customerStoreId;        //var_dump($user_id);die;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'useractivation'])->one();
        $user = \common\models\User::find()->where(['id' => $user_id])->one();
        $store = \common\models\Stores::find()->where(['id' => $store_id])->one();
        //var_dump($template);die;

        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['user'=>$user,'store'=>$store];
        $queue->replacements =[ 'toEmailAddress'=>$user->email,'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'useremail'=>$user->email,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->billingAddress,];            
        $queue->from = "no-reply";
        $queue->recipients = $user->email;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;            
        $queue->save();
    }
    
    public function getCustomerStoreId()
    {
        if($this->roleId==4)
        {
            return $this->storeId;
        }
        else if($this->roleId==2)
        {
            return '';
        }
        else
        {
            $stores=Stores::find()->where(['adminId' => $this->id])->one();
            //return isset($stores)? $stores->id : '';
            return $stores->id;
        }
    }
    public function getPasswordResetUrl()
    {
        if($this->roleId==3) // case b2b-user
        {
            $link=yii\helpers\Url::base(true);
            //$link='http://b2b.inte.everybuy';
            return '<a href="'.$link.Url::to([(Yii::$app->id == "app-b2b")? '/b2b/default/reset-password' : 'site/reset-password', 'token'=>$this->password_reset_token]).'">'.$link.Url::to([(Yii::$app->id == "app-b2b")? '/b2b/default/reset-password' : 'site/reset-password', 'token'=>$this->password_reset_token]); 
        }
        elseif($this->roleId==2) // case suppliers
        {   
            $link=yii\helpers\Url::base(true);
            //$link='http://suppliers.inte.everybuy';
            return '<a href="'.$link.Url::to(['/suppliers/default/reset-password', 'token'=>$this->password_reset_token]).'">'.$link.Url::to(['/suppliers/default/reset-password', 'token'=>$this->password_reset_token]); 
        }
        else  // case normal users
        {
            $link=$this->getStoreUrl();
            return '<a href="'.$link.Url::to(['/site/reset-password', 'token'=>$this->password_reset_token]).'">'.$link.Url::to(['/site/reset-password', 'token'=>$this->password_reset_token]); 
        }
        
    }
    public function getStoreUrl()
    {
        $stores=Stores::find()->where(['id' => $this->storeId])->one();
        return $stores->siteUrl;
    }
    public function getTestUrl()
    { 
       $link1="http://sit1.com";
      
       return '<a href="'.$link1.Url::to(['/user/test']).'">hhh</a>';
      
    }

    public function getSelf(){
        return $this;
    }
   
    public function getCouponUsage($rule){
        return CouponUsage::find()->where(['ruleId' => $rule->id, 'userId' => $this->id])->count();
    }
 

    public function getLogo(){
        return (!is_null($this->store->logoPath) && $this->store->logoPath != "")? $this->store->logoPath : '/store/site/logo_legj.png';
    }

    public function getWishlistItems(){  //var_dump($this->id); var_dump(Yii::$app->params['storeId']) ;die();
        //return  WishListItems::find()->where(['userId'=>$this->id,'storeId'=>Yii::$app->params['storeId']])->all();
        return $this->hasMany(WishListItems::className(), ['userId' => 'id'])->andWhere(['storeId'=>$this->storeId]);
        
    }

    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/user-activation.jpg></a>'; 
    }
    public function getMailImageTick(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/tick.png></a>'; 
    }
    
}
