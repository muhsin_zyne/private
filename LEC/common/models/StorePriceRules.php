<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StorePriceRules".
 *
 * @property integer $id
 * @property integer $ruleId
 * @property integer $storeId
 */
class StorePriceRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StorePriceRules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ruleId', 'storeId'], 'required'],
            [['ruleId', 'storeId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ruleId' => 'Rule ID',
            'storeId' => 'Store ID',
        ];
    }
}
