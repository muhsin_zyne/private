<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Configuration".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Configuration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Configuration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId'], 'integer'],
            [['key', 'value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    public static function findSetting($attr, $storeId = 0){ 
        $config = new Configuration;
        $val = $config->getAttribute($attr, $storeId);
        if($val == 'true') return true;
        if($val == 'false') return false;
        return $val;
    }

    public function getAttribute($attribute, $storeId = 0){
        if(isset(Yii::$app->params['temp']['systemSettings'][$storeId][$attribute])){
            return Yii::$app->params['temp']['systemSettings'][$storeId][$attribute];
        }else{ 
            if($model = Configuration::find()->where(['key' => $attribute, 'storeId' => $storeId])->one()){
                Yii::$app->params['temp']['systemSettings'][$storeId][$attribute] = $model->value;
                return $model->value;
            }

        }
    }

    public static function setConfig($key, $value, $storeId = 0)
    {
        if(!($conf = Configuration::find()->where(compact('key', 'storeId'))->one()))
        {
            $conf = new Configuration;
            $conf->key = $key;
            $conf->storeId = $storeId;
        }
        $conf->value = $value;
        return $conf->save();
    }
}