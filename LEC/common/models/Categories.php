<?php

namespace common\models;
//use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
//use kartik\tree\TreeView;
use common\models\TreeQuery;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use common\models\Products;
use common\models\Brands;

/**
 * This is the model class for table "Categories".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent
 * @property integer $enabled
 *
 * @property ProductCategories[] $productCategories
 */
class Categories extends \yii\db\ActiveRecord 
{

    public $description;
    public $children;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disabled'], 'integer'],
            [['title'], 'string', 'max' => 45],
            [['description'], 'string'],
            //[['image'], 'string'],
            [['image'], 'safe'],
            //[['position'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['metaKeywords'], 'string'],
            [['pageTitle'], 'string'],
            [['metaDescription'], 'string'],
            [['urlKey'], 'string'],
            [['parent'], 'integer'],
            [['root'], 'integer'],
            [['lft'], 'integer'],
            [['rgt'], 'integer'],
            [['lvl'], 'integer'],
            [['icon'], 'string'],
            [['icon_type'], 'integer'],
            [['active'], 'integer'],
            [['selected'], 'integer'],
            [['readonly'], 'integer'],
            [['visible'], 'integer'],
            [['collapsed'], 'integer'],
            [['movable_u'], 'integer'],
            [['movable_d'], 'integer'],
            [['movable_l'], 'integer'],
            [['movable_r'], 'integer'],
            [['removable'], 'integer'],
            [['removable_all'], 'integer'],
            [['urlKey','disabled','metaDescription','metaKeywords','pageTitle'], 'required'],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Categories', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Products', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\CmsPages', 'targetAttribute' => 'slug', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Brands', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Name',
            'description' => 'Description',
            'image' => 'Thumbnail Image',
            'pageTitle' => 'Page Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
            'disabled' => 'Is Active',
            'urlKey' => 'URL Key',
            /*'parent' => 'Parent',*/
            
        ];
    }

    /**z
     * @return \yii\db\ActiveQuery
     */
   
   /* public function getProductCategories()
    {
        return $this->hasMany(ProductCategories::className(), ['categoryId' => 'id']);
    }*/
 



    public function getProducts($storeId, $type = null)
    {
        
        $categoryIds = array_merge(ArrayHelper::map($this->getChildCategories(), 'id', 'id'), [$this->id]);
        //var_dump(implode(",", $categoryIds));  die;
        $query = \common\models\Products::find();
        
        $products = $query
                    ->from('Products')
                    ->join('JOIN',
                        'ProductCategories as pc',
                        'pc.productId = Products.id'
                    )
                    ->groupBy('pc.productId')
                    ->where("pc.categoryId in (".implode(',', $categoryIds).") AND Products.status = '1'");
                    
        if(Yii::$app->id != "app-b2b")
            $query->andWhere("sp.storeId = '$storeId'"); 
        if(isset($type))
            $products->andWhere('sp.type="'.$type.'"');
        if(Yii::$app->id != "app-b2b"){
            $products->join('JOIN',
                        'StoreProducts as sp',
                        'sp.productId = Products.id'
                    );
            $products->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
            $products->andWhere('(pc.storeId = "'.$storeId.'" OR pc.storeId = 0)');
        }


        //var_dump($products);die();

        return $products; 
    }

    /**
     * Max: Get all the child categories of this category recursively
     */
    public function getChildCategories(){
        if(isset(Yii::$app->params['temp']['childCategories'][$this->id])){
	    return Yii::$app->params['temp']['childCategories'][$this->id];
	}
	$categories = Categories::find()->where(['parent' => $this->id])->all();
        foreach($categories as $category){
            $categories = array_merge($categories, $category->childCategories);
        }
	Yii::$app->params['temp']['childCategories'][$this->id] = $categories;
        return $categories;
    }

    public function getChildrenStructured(){
        $cats = [];
        $categories = Categories::find()->where(['parent' => $this->id])->all();
        //var_dump($categories); die;
        foreach($categories as $category){
            $cats[$category->id] = ['id' => $category->id, 'title' => $category->title, 'image' => $category->image];
            $cats[$category->id]['children'] = $category->childrenStructured;
        }
        //var_dump($cats); die;
        return $cats;
    }

    public function getFilterAttributes(){
        $children = array_merge(ArrayHelper::map($this->childCategories, 'id', 'id'), [$this->id]);
        $query = Attributes::find()
        ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
        ->join('JOIN', 'ProductCategories pc', 'pc.productId = av.productId')
        ->join('JOIN', 'Categories c', 'c.id = pc.categoryId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = pc.productId')
        ->join('JOIN', 'Products p', 'sp.productId = p.id')
        ->where(['c.id' => $children, 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId'], 'sp.enabled' => '1', ]);
	else
	    $query->andWhere('createdBy = 0');
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND av.value != ''")
        ->groupBy('Attributes.id');
        $attributes = $query->all();
        $filterAttributes = [];
        foreach ($attributes as $key=>$attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        } 
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code){
        $children = array_merge(ArrayHelper::map($this->childCategories, 'id', 'id'), [$this->id]);
        $query = AttributeValues::find()
        ->join('JOIN', 'Attributes a', 'a.id = AttributeValues.attributeId')
        ->join('JOIN', 'ProductCategories pc', 'pc.productId = AttributeValues.productId')
        ->join('JOIN', 'Categories c', 'c.id = pc.categoryId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = pc.productId')
        ->join('JOIN', 'Products p', 'sp.productId = p.id')
        ->where(['c.id' => $children, 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId'], 'sp.enabled' => '1',]);
	else
            $query->andWhere('createdBy = 0');
        $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
        ->groupBy('`AttributeValues`.`value`');
        return $query->all();
    }

    public function getProductsMaximumPrice(){
	   $priceAttr = (Yii::$app->id=="app-b2b" || Yii::$app->id=="app-api")? "22" : "10";
            $children = array_merge(ArrayHelper::map($this->childCategories, 'id', 'id'), [$this->id]);
        if(Yii::$app->id == "app-frontend")
            $maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join Products p on p.id = av.productId join ProductCategories pc on av.productId = pc.productId join StoreProducts sp on sp.productId = p.id where attributeId = $priceAttr and pc.categoryId in (".implode(",", $children).") and p.status = 1 and sp.storeId = ".Yii::$app->params['storeId']." and sp.enabled = 1 order by av.value+0 desc limit 1")->one();
        else
            $maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join Products p on p.id = av.productId join ProductCategories pc on av.productId = pc.productId where attributeId = $priceAttr and pc.categoryId in (".implode(",", $children).") and p.status = 1 order by av.value+0 desc limit 1")->one();
        if($maxValue)
            return $maxValue->value;
        else
            return 14000;
    }

    public function getProductsBrands(){
        $categoryIds = array_merge(ArrayHelper::map($this->getChildCategories(), 'id', 'id'), [$this->id]);
        $storeId = Yii::$app->params['storeId'];
        $query = \common\models\Brands::find();
        $brands = $query
                    ->from('Brands')
                    ->join('JOIN',
                        'Products as p',
                        'p.brandId = Brands.id'
                    )
                    ->join('JOIN',
                        'ProductCategories as pc',
                        'pc.productId = p.id'
                    );
                    if(Yii::$app->id != "app-b2b"){
                        $query->join('JOIN',
                            'StoreProducts as sp',
                            'sp.productId = p.id'
                        );
                    }    
            $query  ->groupBy('p.brandId')
                    ->andWhere("pc.categoryId in (".implode(',', $categoryIds).") AND p.status = '1' and Brands.enabled = '1' and Brands.isVirtual = '0'");
                    if(Yii::$app->id != "app-b2b"){
                        $brands->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
                    }
                    $brands->andWhere('(pc.storeId = "'.$storeId.'" OR pc.storeId = 0)');

        return $brands->all();            
    }

    protected function parse($attr, $default = true)
    {
        return isset($this->$attr) ? $this->$attr : $default;
    }
    public function getParentCategory()
    {        
        return $this->hasOne(Categories::className(), ['id' => 'parent']);        
    }   

    public function getSelf(){
        return $this;
    }
    
    /**
     * Stan: Get all the parent categories of this category recursively
     */
    public function getParentCategories(){
	$categories = Categories::find()->where(['id' => $this->parent])->all();
        foreach($categories as $category){
            $categories = array_merge($categories, $category->parentCategories);
        }
        return $categories;
    }
}


