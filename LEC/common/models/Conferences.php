<?php

namespace common\models;

use Yii;
/**
 * This is the model class for table "Conferences".
 *
 * @property integer $id
 * @property string $title
 * @property string $from
 * @property string $to
 * @property integer $status
 * @property string $image
 */
class Conferences extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Conferences';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromDate', 'toDate'], 'safe'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 45],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['fromDate', 'toDate','title'],'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'fromDate' => 'From Date',
            'toDate' => 'To Date',
            'status' => 'Status',
            'image' => 'Image',
        ];
    }

    public function getConferenceTrays()
    {
        return $this->hasMany(ConferenceTrays::className(), ['conferenceId' => 'id']);
    }

    public function getTrayProducts($trayId=null)

    {
        $products = Products::find()->join('JOIN','ConferenceTrayProducts as ctp','ctp.productId = Products.id')->join('JOIN','ConferenceTrays as ct','ct.id = ctp.conferenceTrayId')->where(['ct.conferenceId' => $this->id]);
        if(isset($trayId))
            $products->andWhere(['ctp.conferenceTrayId'=>$trayId]);
        return $products;

    }

    public function willEndToday()
    {
        if($this->toDate <= date('Y-m-d H:i:s')){
            $this->status = 0;
            $this->save(false);
        }
    }
}
