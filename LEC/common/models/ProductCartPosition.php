<?php
/**
 * Author: Max
 */
namespace common\models;
use yz\shoppingcart\CartPositionTrait;
use common\models\Products;
use yz\shoppingcart\CartPositionInterface;
use yii\base\Object;
class ProductCartPosition extends Object implements \yz\shoppingcart\CartPositionInterface
{
    use CartPositionTrait;
    protected $_product;
    public $id;
    public $config;
    public $b2bAddress = null; //for having separate carts for different store locations in B2B
    public $supplierUid;
    public $comments;
    public $giftWrap=false;
    public $giftWrapCost=0;
    public function getId(){
        if(is_object($this->b2bAddress))
            return md5(serialize(array_merge($this->config, [$this->id, $this->b2bAddress->id])));
        else
            return md5(serialize(array_merge($this->config, [$this->id])));
    }

    public function getPrice()
    {
        if($this->product->typeId=="simple")
            return $this->product->price;
        elseif($this->product->typeId=="gift-voucher"){           
            return $this->config['voucherValue'];            
        }
        else
            return $this->product->price + $this->getExtraAmounts();
    }

    /**
     * @return Product
    */
    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = Products::findOne($this->id);
        }
        return $this->_product;
    }

    public function getExtraAmounts(){
        $amount = 0;
        if($this->product->typeId == "bundle"){
            foreach($this->config as $productId => $item){
                $amount = $amount + ($item['qty']*$item['price']);
            }
        }elseif($this->product->typeId == "configurable"){ 
            // if($this->product->id=="3133" && $this->b2bAddress->id=="31"){
            //     var_dump($this->config); die;
            // }
            foreach($this->config as $optionPriceId => $item){
                if($optionPrice = AttributeOptionPrices::findOne($optionPriceId)){ 
                    //var_dump($optionPriceId); die($optionPriceId);
                    $amount = $amount + $optionPrice->price;
                }
            }
        }
        return $amount;
    }

    public function getSuperAttributeValuesText(){
        foreach($this->config as $config => $details){
            if($this->product->typeId=="bundle"){
                echo "<p>".$details['name'].": ".$details['qty']."</p>";
            }elseif($this->product->typeId=="configurable"){
                echo "<p>".(isset($details['attr']['title'])? ($details['attr']['title'].": ".$details['value']) : "")."</p>";
            }
        }
    }

    public function getSku(){
        if($this->product->typeId == "configurable"){
            return ($this->childProduct)? $this->childProduct->sku : $this->product->sku;
        }
        return $this->product->sku;
    }

    public function getChildProduct(){
        $childProduct = false;
        $linkages = \common\models\ProductLinkages::find()->where(['parent' => $this->product->id])->all();
        foreach($linkages as $link){ //logic for finding the child product with given config
            foreach($this->config as $config){ 
                $linkedProductQuery = new \yii\db\ActiveQuery('common\models\Products');
                $linkedProduct = $linkedProductQuery->from('Products')->where('id = '.$link->productId)->one();
                if($linkedProduct->{$config['attr']['code']} == $config['value']){
                    $childProduct = $linkedProduct;
                    break;
                }else
                    $childProduct = false;
            }
            if($childProduct) break;
        }
        return $childProduct;
    }
}