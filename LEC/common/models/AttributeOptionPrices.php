<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "AttributeOptionPrices".
 *
 * @property integer $id
 * @property integer $attributeId
 * @property integer $productId
 * @property integer $optionId
 * @property integer $storeId
 * @property string $price
 * @property string $type
 */
class AttributeOptionPrices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AttributeOptionPrices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attributeId', 'productId', 'optionId', 'price'], 'required'],
            [['attributeId', 'productId', 'optionId', 'storeId'], 'integer'],
            [['price'], 'number'],
            [['type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attributeId' => 'Attribute ID',
            'productId' => 'Product ID',
            'optionId' => 'Option ID',
            'storeId' => 'Store ID',
            'price' => 'Price',
            'type' => 'Type',
        ];
    }

    public function getAttr(){
        return $this->hasOne(Attributes::className(), ['id' => 'attributeId']);
    }

    public function getOption(){
        return $this->hasOne(AttributeOptions::className(), ['id' => 'optionId']);
    }
    
}
