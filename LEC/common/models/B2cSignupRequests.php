<?php

namespace common\models;
//use yii\grid\DetailView;


use Yii;

/**
 * This is the model class for table "B2cSignupRequests".
 *
 * @property integer $id
 * @property string $lejMemberNumber
 * @property string $contactPerson
 * @property string $email
 * @property string $additionalEmail
 * @property string $phoneStore
 * @property string $mobile
 * @property string $storeTradingName
 * @property integer $abn
 * @property string $primaryStoreAddress
 * @property string $multipleStores
 * @property string $domainDetails
 * @property string $aboutus
 * @property string $tradingHours
 * @property integer $skin
 * @property string $logo
 * @property string $storeImage
 * @property integer $orderValueMaximum
 * @property integer $shippingFee
 * @property string $websiteColour
 * @property string $websiteAddress
 * @property string $facebookUrl
 * @property string $twitterUrl
 * @property string $youtubeUrl
 * @property string $pinterestUrl
 * @property string $envision
 */
class B2cSignupRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'B2cSignupRequests';
    }

    /**
     * @inheritdoc
     */
    public $monday;
    public $tuesday;
    public $wednesday;
    public $thursday;
    public $friday;
    public $saturday;
    public $sunday;
    public $captcha;
    public $captcha_validate;
    //public $image;
    public function rules()
    {
       
        return [
            [['phoneStore','email','abn'], 'required'],
            [['orderValueMaximum','abn'], 'integer'],
            [['email', 'additionalEmail',], 'email'],
           // [['facebookUrl', 'websiteAddress','twitterUrl', 'youtubeUrl','pinterestUrl'], 'url'],
            [['abn'], 'integer','message'=>'Please enter a valid 11 digit ABN without space or hypen.'],
            [['phoneStore','mobile'], 'integer','message'=>'Please enter phone number without space, hypen etc.'],
             [['logo','storeImage'], 'file', 'extensions' => 'bmp, gif, jpg, jpeg, jpeg2000, raw, WebP, png, psd, pspimage, thm, tif, tiff, yuv, ai, drw, eps, ps, svg, pdf, xps, doc, docx', 'maxSize' => 3145728, 'tooBig' => 'Image should be less than 3 MB.','maxFiles' => 5],
           /* ['captcha', 'required', 'when' => function($this) {
                return $this->captcha_validate == 1;
                }, logo//'enableClientValidation' => false
            ],
            ['captcha', 'captcha', 'when' => function($this) {
                return $this->captcha_validate == 1;
                }, //'enableClientValidation' => false
            ],*/
           
            //[['abn'],'number',],
            //[['logo'], 'file', 'extensions'=>'jpg, gif, png'],
            [['primaryStoreAddress', 'multipleStores', 'domainDetails', 'aboutus', 'tradingHours',  'envision','shippingFee'], 'string'],
            [['captcha_validate','talkbox'], 'safe'],
            [['lejMemberNumber', 'contactPerson', 'email', 'additionalEmail', 'phoneStore', 'mobile', 'storeTradingName', 'websiteColour', 'websiteAddress', 'facebookUrl', 'twitterUrl', 'youtubeUrl', 'pinterestUrl'], 'string', 'max' => 255]
        ];
    }
    public function validatePassword()
    {
        $this->addError('lejMemberNumber', 'Incorrect username or password.');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lejMemberNumber' => 'Member Number',
            'contactPerson' => 'Contact Person',
            'email' => 'Email Address',
            'additionalEmail' => 'Additional Email Address',
            'phoneStore' => 'Contact Phone (Store)',
            'mobile' => 'Contact Phone (Mobile)',
            'storeTradingName' => 'Store Trading Name',
            'abn' => 'ABN',
            'talkbox'=>'Talkbox Username and Password',
            'primaryStoreAddress' => 'Primary Store Address',
            'multipleStores' => 'Multiple Stores',
            'domainDetails' => 'Domain Details',
            'aboutus' => 'About Us',
            'tradingHours' => 'Trading Hours',
            'skin' => 'Skin',
            'logo' => 'Upload your logo (If we can choose multiple logos)',
            'storeImage' => 'Upload any store and/or staff photos (If we can choose multiple photos)',
            'orderValueMaximum' => 'Order Value Maximum',
            'shippingFee' => 'Shipping Fee',
            'websiteColour' => 'Please provide preferred colours for your website',
            'websiteAddress' => 'Web Address URL ',
            'facebookUrl' => 'Facebook URL',
            'twitterUrl' => 'Twitter URL',
            'youtubeUrl' => 'Youtube URL',
            'pinterestUrl' => 'Pinterest URL',
            'envision' => 'Tell us how you envision your store to be',
        ];
    }
    public function sendB2cSignupRequestMail() // send to admin (b2b user)
    {
        
        $template = \common\models\EmailTemplates::find()->where(['code' => 'b2csignuprequest'])->one();        
        $queue = new \common\models\EmailQueue;
        $queue->models = ['b2c'=>$this]; 
        
        //$queue->recipients = "aaron@xtrastaff.com.au";
        $queue->recipients = "aaron@xtrastaff.com.au";
        $template->layout = "layout";
        $queue->recipients = isset(Yii::$app->params['signupReciepient']) ? Yii::$app->params['signupReciepient'] : $queue->recipients;
        $queue->replacements =['emailFacebookImage'=>$this->getEmailFacebookImage(),'emailTwitterImage'=>$this->getEmailTwitterImage(),'emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'','emailLinkedinImage'=>$this->getEmailLinkedinImage(),'logo'=>'<img  src='.Yii::$app->params["rootUrl"].'/store/site/logo/logo.png>','useremail'=>$queue->recipients,'b2csignuprequestlist'=>$this->getB2cRequestDetail(),'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>''];             
        $queue->from = "no-reply";
        
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
        
    }
    
    public function getB2cRequestDetail()
    {
        //var_dump($this);die;
        return \yii\widgets\DetailView::widget([
        'model' => $this,
        'attributes' => [
            // 
            'id',
            'lejMemberNumber', 
            'contactPerson', 
            'email', 
            'additionalEmail',       
            //'phoneStore', 
            [   
                'label' => 'Phone Store',
                'value' => $this->phoneStore,
            ],
            //'mobile', 
            [   
                'label' => 'Mobile',
                'value' => $this->mobile,
            ],
            'storeTradingName', 
            'abn', 
            'talkbox',
            'primaryStoreAddress', 

            //'multipleStores', 
            'multipleStores:html',
            'domainDetails:html', 
            'aboutus:html', 
            'skin', 
            //'orderValueMaximum', 
            //'shippingFee', 
            [   
                'label' => 'Website Colour',
                'value' => $this->websiteColour,
            ],
            //'websiteColour', 
            'websiteAddress',
            'facebookUrl',
            'twitterUrl',
            'youtubeUrl',
            'pinterestUrl',
            //'envision',
            [   
                'label' => 'Envision',
                'value' => $this->envision,
            ],
            [   
                'label' => 'Trading Hours',
                    'format' => 'html',
                'value' => $this->tradingHoursAll,
            ],
            [   
                'label' => 'Logo',
                'format' => 'html',
                //'format' => ['image',['width'=>'100','height'=>'100']],
                'value' => $this->logoImages,
            ],
            [   
                'label' => 'Store Image',
                'format' => 'html',
                //'format' => ['image',['width'=>'100','height'=>'100']],
                'value' => $this->storeImages,
            ],
            
        ],
        ]);
    }
    public function  getLogoImages()
    {
        
        if(isset($this->logo))
        {
            $logoImages=json_decode($this->logo, TRUE);
             $imgs='';
            foreach ($logoImages as $key => $logoImage) {
              
                //$imgs=$imgs.'<a href='.Yii::$app->params["rootUrl"].'/store/b2c-requests/logo/'.$logoImage.'><img src='.Yii::$app->params["rootUrl"].'/store/b2c-requests/logo/'.$logoImage.'" width="100" height="100" alt=""></a>'.'&nbsp';
        $imgs=$imgs.'<a href='.Yii::$app->params["rootUrl"].'/store/b2c-requests/logo/'.$logoImage.'>'.$logoImage.'</a>'.'<br/>';

            }
            return $imgs;
        }
    }
    public function  getStoreImages()
    {
        if(isset($this->storeImage))
        {
            $storeImages=json_decode($this->storeImage, TRUE);
            $imgs='';
            foreach ($storeImages as $key => $storeImage) {
                $imgs=$imgs.'<a href='.Yii::$app->params["rootUrl"].'/store/b2c-requests/store/'.$storeImage.'>'.$storeImage.'</a>'.'<br/>';
               
            }
            return $imgs;
        }
        
        //return 1;
    }
    public function  getTradingHoursAll()
    {
        if(isset($this->tradingHours))
        {
            $tradingHours=json_decode($this->tradingHours, TRUE);
            $allday='';
            foreach ($tradingHours as $key => $day) {
               $allday=$allday.$key.' : '.$day.'<br>';
            }
            return $allday;
        }
        
    }
    public function getEmailFacebookImage()
    {
        return '<a href="https://www.facebook.com/theleadingedgegroup"><img alt="Facebook" src='.Yii::$app->params["rootUrl"].'/store/site/email/facebook.png height="20" width="20" ></a>';   
    }
    public function getEmailTwitterImage()
    {
        return '<a href="https://twitter.com/LeadingEdgeAU"><img alt="twitter" src='.Yii::$app->params["rootUrl"].'/store/site/email/twitter.png height="20" width="20" ></a>';
    }
    
    public function getEmailGoogleplusImage()
    {        
        return '<a href="https://plus.google.com/+leadingedgegroup/posts"><img alt="instagram" src='.Yii::$app->params["rootUrl"].'/store/site/email/email/twitter.png height="20" width="20" ></a>';
    }
    public function getEmailLinkedinImage()
    {        
        return '<a href="https://www.linkedin.com/company/leading-edge-group"><img alt="instagram" src='.Yii::$app->params["rootUrl"].'/store/site/email/linkedin.png height="20" width="20" ></a>';
    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/b2c-signup.png></a>'; 
    }   
}
