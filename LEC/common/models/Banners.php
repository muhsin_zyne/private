<?php

namespace common\models;
use yii\db\ActiveRecord;

use Yii;



class Banners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $category;
    Public $url_type;
    Public $link;
    public $image;
    public $main_image;
    public $mini_image;
    public $side_image;
    public $demo_path;
    public $main_check;
    public $mini_check;
    public $side_check;
    public $facebook_link;

    public static function tableName()
    {
        return 'Banners';
    }

    /**
     * @inheritdoc
     */
    public $urlcategory;
    public function rules()
    {
        return [
            [['main_image','category'], 'required'],
            [['path', 'url', 'type','demo_path'], 'string'],
            [['image','isFacebook','masterId','target'], 'safe'],
            //[['facebook_link','link'],  'url', 'defaultScheme' => 'http'],   
            [['facebook_link'], 'url', 'defaultScheme' => 'http'],         
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['facebook_link'],  'url', 'defaultScheme' => 'http'], 
            [['x', 'y'], 'integer'],

            // ['link', 'url', 'when' => function ($model) { echo '$model->url_type';die;
            //     return $model->url_type == '1';
            // }, 'whenClient' => "function (attribute, value) {
            //     return $('#banners-url_type').val() == '1';
            // }"],
        ];
        /*return [
            [['path','category','url_type','main_image','mini_image','side_image'], 'required'],
            [['path', 'url', 'type','demo_path'], 'string'],
            [['image','isFacebook','masterId','target'], 'safe'],
            //[['facebook_link','link'],  'url', 'defaultScheme' => 'http'],   
            [['facebook_link'], 'url', 'defaultScheme' => 'http'],         
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['facebook_link'],  'url', 'defaultScheme' => 'http'], 
            [['x', 'y'], 'integer'],

            // ['link', 'url', 'when' => function ($model) { echo '$model->url_type';die;
            //     return $model->url_type == '1';
            // }, 'whenClient' => "function (attribute, value) {
            //     return $('#banners-url_type').val() == '1';
            // }"],
        ]; */
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Image',
            'url' => 'Url',
            'x' => 'X',
            'y' => 'Y',
            'html' => 'Html',
            'type' => 'Type',
            'category' => 'Dimension',
            'isFacebook' => 'Defaut Type',
            'target'=>'Target',
        ];
    }
    public function getYoutubeUrl()
    {
        if($this->html!=''){            
            if(preg_match("/h?v=/", $this->html)) // eg: https://www.youtube.com/watch?v=giIrB8IhRAk
            {
                $explode_url=explode('h?v=', $this->html); 
                $youtube_url='https://www.youtube.com/embed/'.$explode_url[1];
            }
            else if(preg_match("/youtu.be/", $this->html))//eg: https://youtu.be/vKdsDn4Lbts
            {
                $explode_url=explode('youtu.be/', $this->html);
                $youtube_url='https://www.youtube.com/embed/'.$explode_url[1]; 
            }
            else // eg: https://www.youtube.com/embed/vKdsDn4Lbts
            {
                $youtube_url=$this->html;
            }
        }
        return $youtube_url;
    }

   
}
