<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "OrderMeta".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class OrderMeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderMeta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId'], 'integer'],
            [['key', 'value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    public static function findValue($attr, $orderId){ 
        $config = new OrderMeta;
        $val = $config->getValue($attr, $orderId);
        if($val == 'true') return true;
        if($val == 'false') return false;
        return $val;
    }

    public function getValue($attribute, $orderId){
        if(isset(Yii::$app->params['temp']['systemSettings'][$orderId][$attribute])){
            return Yii::$app->params['temp']['systemSettings'][$orderId][$attribute];
        }else{ 
            if($model = OrderMeta::find()->where(['key' => $attribute, 'orderId' => $orderId])->one()){
                Yii::$app->params['temp']['systemSettings'][$orderId][$attribute] = $model->value;
                return $model->value;
            }

        }
    }

    public static function setValue($key, $value, $orderId)
    {
        if(!($conf = OrderMeta::find()->where(compact('key', 'orderId'))->one()))
        {
            $conf = new OrderMeta;
            $conf->key = $key;
            $conf->orderId = $orderId;
        }
        $conf->value = $value;
        return $conf->save();
    }
}
