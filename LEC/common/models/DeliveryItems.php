<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deliveryitems".
 *
 * @property integer $id
 * @property integer $deliveryId
 * @property integer $orderItemId
 * @property integer $quantityDelivered
 */
class DeliveryItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DeliveryItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deliveryId', 'orderItemId', 'quantityDelivered'], 'required'],
            [['deliveryId', 'orderItemId', 'quantityDelivered'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deliveryId' => 'Delivery ID',
            'orderItemId' => 'Order Item ID',
            'quantityDelivered' => 'Quantity Delivered',
        ];
    }
    public function getOrderItem()
    {
        return $this->hasOne(OrderItems::className(), ['id' => 'orderItemId']);
    }
}
