<?php

namespace common\models;
use frontend\components\Helper;
use Yii;
use common\models\OrderItemChildren;
use common\models\DeliveryItems;
use yii\helpers\Html;
/**
 * This is the model class for table "OrderItems".
 *
 * @property integer $id
 * @property integer $orderId
 * @property integer $productId
 * @property integer $quantity
 * @property string $price
 * @property integer $promotionId
 * @property string $storeAddressId
 * @property integer $comment
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */


    public $storeMailId;
    public $supplierMailId;
    public $message;

    public static function tableName()
    {
        return 'OrderItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'orderId', 'productId', 'quantity', 'price', 'config','storeMailId','supplierMailId','message'], 'required'],
            [['id', 'orderId', 'productId', 'quantity', 'promotionId', 'storeAddressId'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderId' => 'Order ID',
            'productId' => 'Product ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'promotionId' => 'Promotion ID',
            'storeAddressId' => 'Store Address ID',
            'comment' => 'Comment',
        ];
    }

    public function getProduct(){       
        $query = $this->hasOne(Products::className(), ['id' => 'productId']);
        $query->where = str_replace("Products.status = 1 AND ", "", $query->where);
        return $query;
    }
    public function getOrder(){    
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }

    public function getB2baddress(){    
        return $this->hasOne(B2bAddresses::className(), ['id' => 'storeAddressId']);
    }

    public function getSupplier(){
        return $this->hasOne(\common\models\Suppliers::className(), ['id' => 'supplierUid']);
    }
    public function getSuperAttributeValuesText(){ 
        $configs = json_decode($this->config, true);
        $return = "";
        if(is_array($configs)){
            foreach($configs as $config => $details){
                if(isset($this->product))
                {
                    if($this->product->typeId=="bundle"){
                        $return = $return .  "<p>".$details['name'].": ".$details['qty']."</p>";
                    }elseif($this->product->typeId=="configurable"){
                        $return = $return . "<p>".(isset($details['attr']['title'])? ($details['attr']['title'].": ".$details['value']) : "")."</p>";
                    }
                }
            }
        }
        return ($return=="")? "N/A" : $return;
    }
    public function getQtyStatus(){  
        //---------------delivery----------------------      
        $deliveries=Deliveries::find()->where(['orderId'=>$this->orderId])->all();
        $deliveryItemCount=0;
        foreach ($deliveries as  $delivery ){
            $Deliveryitems=Deliveryitems::find()->where(['deliveryId'=>$delivery->id,'orderItemId'=>$this->id])->all();
            foreach ($Deliveryitems as $item) {
               $deliveryItemCount=$deliveryItemCount+$item->quantityDelivered;
            }
        }
        //----------------------creditmemo------------------
        $creditMemos = CreditMemos::find()->where(['orderId' => $this->orderId])->all();
        $creditmemoItemCount = 0;
        foreach ($creditMemos as $creditMemo) {
            $creditmemoitems = CreditMemoItems::find()->where(['memoId' => $creditMemo->id, 'orderItemId' => $this->id])->all();
            foreach ($creditmemoitems as $item) {
                $creditmemoItemCount = $creditmemoItemCount + $item->quantityRefunded;
            }
        }
        //----------------------shipment------------------
        $shipments=Shipments::find()->where(['orderId'=>$this->orderId])->all();
        $shipmentItemCount=0;
        foreach ($shipments as  $shipment ){
            $ShipmentItems=ShipmentItems::find()->where(['shipmentId'=>$shipment->id,'orderItemId'=>$this->id])->all();
            foreach ($ShipmentItems as $item) {
               $shipmentItemCount=$shipmentItemCount+$item->quantityShipped;
            }
        }

        $orderType=isset($this->order->collectFrom)?'Collection':'Shipment';
        $redyForCollectionCount=$this->qtyReadyForDelivery-$deliveryItemCount;
        $inProcessCount=$this->quantity-$this->qtyReadyForDelivery;
        $html='';
        if ($creditmemoItemCount != 0) {
            $html .= '<tr>
                <td valign="middle" height="20" >' . '' . '</td>
                <td width="5" style="width:5px;"></td>
                <td valign="middle" height="20" style="padding:0 5px;text-align:center;color:#fff;border:1px solid #f56954;font-size:12px;border-radius:3px;background:#f56954;line-height:normal;">Refunded</td>
            </tr>
            <tr>
                <td height="4"></td>
                <td height="4"></td>
            </tr>';
            return $html;
        }
        if($deliveryItemCount!=0){
            $html.='<tr>
                <td valign="middle" height="20" style="width:32px;padding:0;text-align:center;color:#f56954;border:1px solid #f56954;font-size:12px;border-radius:3px;line-height:normal;">'.$deliveryItemCount.'</td>
                <td width="5" style="width:5px;"></td>
                <td valign="middle" height="20" style="padding:0 5px;text-align:center;color:#fff;border:1px solid #f56954;font-size:12px;border-radius:3px;background:#f56954;line-height:normal;">Collected</td>
            </tr>
            <tr>
                <td height="4"></td>
                <td height="4"></td>
            </tr>';
        } 
        if($shipmentItemCount!=0){
            $html.='<tr>
                <td valign="middle" height="20" style="width:32px;padding:0;text-align:center;color:#f56954;border:1px solid #f56954;font-size:12px;border-radius:3px;line-height:normal;">'.$shipmentItemCount.'</td>
                <td width="5" style="width:5px;"></td>
                <td valign="middle" height="20" style="padding:0 5px;text-align:center;color:#fff;border:1px solid #f56954;font-size:12px;border-radius:3px;background:#f56954;line-height:normal;">Shipped</td>
            </tr>
            <tr>
                <td height="4"></td>
                <td height="4"></td>
            </tr>';
        } 
        if($redyForCollectionCount!=0){
            $html.='<tr>
                <td valign="middle" height="20" style="width:32px;padding:0;text-align:center;color:#418835;border:1px solid #418835;font-size:12px;border-radius:3px;line-height:normal;">'.$redyForCollectionCount.'</td>
                <td width="5" style="width:5px;"></td>
                <td valign="middle" height="20" style="padding:0 5px;text-align:center;color:#fff;border:1px solid #418835;font-size:12px;border-radius:3px;background:#418835;line-height:normal;">Ready for '.$orderType.'</td>
            </tr>
            <tr>
                <td height="4"></td>
                <td height="4"></td>
            </tr>';
        }
        if($inProcessCount!=0){
            $html.='<tr>
                <td valign="middle" height="20" style="width:32px;padding:0;text-align:center;color:#ea8e34;border:1px solid #ea8e34;font-size:12px;border-radius:3px;line-height:normal;">'.$inProcessCount.'</td>
                <td width="5" style="width:5px;"></td>
                <td valign="middle" height="20" style="padding:0 5px;text-align:center;color:#fff;border:1px solid #ea8e34;font-size:12px;border-radius:3px;background:#ea8e34;line-height:normal;">In Process</td>
            </tr>';
        }
        return  $html;
    }
    public function getQtyToInvoiced(){    
        $invoicesitem=InvoiceItems::find()->where(['orderItemId'=>$this->id])->all();
        $invoiceitem_qty=0;
        foreach ($invoicesitem as $key => $value) {
           $invoiceitem_qty=$invoiceitem_qty+$value['quantityInvoiced'];
        }
        return $this->quantity-$invoiceitem_qty;
    }
    public function getQtyToShip(){    
        $shipmentitem=ShipmentItems::find()->where(['orderItemId'=>$this->id])->all();
        $shipmentitem_qty=0;
        foreach ($shipmentitem as $key => $value) {
           $shipmentitem_qty=$shipmentitem_qty+$value['quantityShipped'];
        }
        return $this->quantity-$shipmentitem_qty;
    }
    public function getQtyToDelivered(){    
        $deliveryitem=Deliveryitems::find()->where(['orderItemId'=>$this->id])->all();
        $deliveryitem_qty=0;
        foreach ($deliveryitem as $value) {
           $deliveryitem_qty=$deliveryitem_qty+$value['quantityDelivered'];
        }
        return $this->qtyReadyForDelivery-$deliveryitem_qty;
    }
    public function getQtyToShipped(){    
        $shippedItem=ShipmentItems::find()->where(['orderItemId'=>$this->id])->all();
        $shippedItem_qty=0;
        foreach ($shippedItem as $value) {
           $shippedItem_qty=$shippedItem_qty+$value['quantityShipped'];
        }
        return $this->qtyReadyForDelivery-$shippedItem_qty;
    }
    public function getQtyToCreditMemo(){    
        $creditmemoitem=CreditMemoItems::find()->where(['orderItemId'=>$this->id])->all();
        $creditmemoitem_qty=0;
        foreach ($creditmemoitem as $key => $value) {
           $creditmemoitem_qty=$creditmemoitem_qty+$value['quantityRefunded'];
        }
        return $this->quantity-$creditmemoitem_qty;
    }

    public function getSku() {
        if(isset($this->product)){        
            if($this->product->typeId=="configurable" && $this->child){
                $query = $this->child->getProduct();
                $query->where = "";
                return ($product = $query->one())? $product->sku : "";
            }
            return $this->product->sku;
        }
        return 'N/A';
    }

    public function getTotalPrice(){    
        return $this->price * $this->quantity;
    }

    public function getChild(){ 
        return $this->hasOne(\common\models\OrderItemChildren::className(), ['orderItemId' => 'id']);
    }

    public function getChildren(){
        return $this->hasMany(\common\models\OrderItemChildren::className(), ['orderItemId' => 'id']);
    }

    public function orderFromSupplier(){
        $store = \common\models\Stores::find()->where(['id' => $this->order->storeId])->one();
        $template = \common\models\EmailTemplates::find()->where(['code' => 'orderfromsupplier'])->one();
        $order = \common\models\Orders::find()->where(['id' => $this->orderId])->one();
        $product = \common\models\Products::find()->where(['id' => $this->productId])->one();
        $userfullname=Yii::$app->user->identity->store->storeAdminName;
        $from=Yii::$app->user->identity->store->email;
        $recipient = $this->product->brand->supplier->email;
    //$recipient = "dan.b@xtrastaff.com.au"; 
        $queue = new \common\models\EmailQueue;
        $queue->models = ['order'=>$order,'product'=>$product,'orderItem'=>$this,'store'=>$store]; 
        $queue->replacements =[ 'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$store->logo, 'message'=>$_POST['OrderItems']['message'],
            'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'',
            'emailLinkedinImage'=>'',
            'userfullname'=>$userfullname,'useremail'=>$recipient, 'footerText' => 'This order was generated using the Leading Edge Web Platform'];
        $queue->from = $from;
        $queue->recipients = $recipient;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        $queue->save();
        return true;
    }

    public function getSelf(){
        return $this;
    }
    public function getDiscountAmount(){    
        return (isset($this->discount)?$this->discount:0) ;        
    }
    public function getProductImage(){    
        Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$this->product->getThumbnailImage();
    }
    public function getQtyDelivery(){ 
        $a='';
        for($x=0;$x<=$this->qtyAvailable;$x++){  
           $a=$a.'<option value="'.$x.'">'.$x.'</option>';
        } 
        return '<select name="qrfd_'.$this->id.'" id="'.$this->id.'" class="select-qrfd">'.$a.'</select>';
    }
    public function getQtyAvailable(){
        return $this->quantity-$this->qtyReadyForDelivery;
    }
    public function getDeleveredQtyCount(){
        $DeliveryItems=DeliveryItems::find()->where(['orderItemId'=>$this->id])->all();
        $qty=0;
        foreach ($DeliveryItems as $item) {
           $qty=$qty+$item->quantityDelivered;
        }
        return $qty;
    }
    public function getShipmentQtyCount(){
        $DeliveryItems=ShipmentItems::find()->where(['orderItemId'=>$this->id])->all();
        $qty=0;
        foreach ($DeliveryItems as $item) {
           $qty=$qty+$item->quantityShipped;
        }
        return $qty;
    }
    public function getQtyReadyForCollection(){
        return $this->qtyReadyForDelivery-$this->deleveredQtyCount;
    }
    public function getItemSubTotal(){
        return (($this->price * $this->quantity)+($this->giftWrapAmount* $this->quantity)-$this->discount);
    }
    public function getQtyReadyForRefund(){ 
        $a='';
        for($x=0;$x<=$this->quantity;$x++){  
           $a=$a.'<option value="'.$x.'">'.$x.'</option>';
        } 
        return '<select name="qrfd_'.$this->id.'" id="'.$this->id.'" class="select-qrfd">'.$a.'</select>';
    }
    public function getProductDetails(){  // orders details page product details show
        //var_dump($this->product->thumbnailImage);die;
        $productImage=isset($this->product) ? Html::img(Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$this->product->getThumbnailImage()) : '';
        $result='<div class="tb-img">'.$productImage.'</div> '.$this->product->name.'<br/>
            <div class="tb-price">'.Helper::money($this->price).'</div>';
        $result.= !empty($this->giftWrap) ? '<span class="label label-success">$'.round($this->giftWrapAmount).' Gift Wrapping Included</span>':'';
        $result.=$this->superAttributeValuesText!='N/A' ? $this->superAttributeValuesText:'';
        $result.=$this->comment!='' ? '<div class="order-comment"><b>Comment:</b> '.$this->comment.'</div>' : '';
        return $result;
    }
    
    public function getRefundAmountItem() {

        $RefundItems = CreditMemoItems::find()->where(['orderItemId' => $this->id])->all();
        $grandTotal = 0;
        if (!empty($RefundItems)) {
            foreach ($RefundItems as $item) {
                $grandTotal += $item->amount;
            }
            $grandTotal = ($this->itemSubTotal) - $grandTotal;
        } else
            $grandTotal = $this->itemSubTotal;

        return $grandTotal;
    }

}

