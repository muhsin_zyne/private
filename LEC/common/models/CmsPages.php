<?php

namespace common\models;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "CmsPages".
 *
 * @property integer $id
 * @property string $title
 * @property string $contentHeading
 * @property string $slug
 * @property string $content
 * @property integer $storeId
 * @property integer $status
 * @property string $dateUpdated
 */
class CmsPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CmsPages';
    }

    /**
     * @inheritdoc
     */
    public $image;
    public $selected_store;
    public function rules()
    {
        return [
            [['title', 'contentHeading', 'slug',  'selected_store', 'status'], 'required'],
            [['content'], 'string'],
            [['slug'], 'unique', 'targetClass' => 'common\models\Categories', 'targetAttribute' => 'urlKey', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['slug'], 'unique', 'targetClass' => 'common\models\Products', 'targetAttribute' => 'urlKey', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            /*[['slug'], 'unique', 'targetClass' => 'common\models\CmsPages', 'targetAttribute' => 'slug', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],*/
            [['slug'], 'unique', 'targetClass' => 'common\models\Brands', 'targetAttribute' => 'urlKey', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            //[['selected_store', 'status'], 'integer'],
            [['dateUpdated','metaTitle','metaKeywords','metaDescription','content'], 'safe'],
            [['image'], 'file','extensions'=>'jpg, gif, png'],
            [['title', 'contentHeading', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'contentHeading' => 'Content Heading',
            'slug' => 'URL Key',
            'content' => 'Content',
            'selected_store' => 'Stores',
            'status' => 'Status',
            'dateUpdated' => 'Date Updated',
            'metaTitle' => 'Meta Title',
            'metaKeywords' => 'Meta Keywords',
            'metaDescription' => 'Meta Description',
        ];
    }

    public function getStores(){
        return $this->hasMany(Stores::className(), ['id' => 'storeId'])->viaTable('StorePages', ['cmspageId' => 'id']);
    }

    public function getResolvedContent(){
        return str_replace('[!rootUrl]', Yii::$app->params['rootUrl'], $this->content);
    }

    public function findPlaceholders($string){
        if(preg_match_all("/\[!(.*?)\]/",$string,$matches)) // Checking wtheter there is a match for the pattern
            return $matches[1]; // returning the match
        else
            return [];
    }
}
