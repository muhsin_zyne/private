<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StoreProducts".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $productId
 * @property integer $quantity
 * @property integer $inStock
 */
class StoreProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StoreProducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disableDate'], 'safe'],
            [['storeId', 'productId', 'quantity'], 'required'],
            [['storeId', 'productId', 'quantity', 'inStock'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'productId' => 'Product ID',
            'quantity' => 'Quantity',
            'inStock' => 'In Stock',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(\common\models\Products::className(), ['id' => 'productId']);
    }
}
