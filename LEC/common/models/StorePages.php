<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StorePages".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $cmspageId
 */
class StorePages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StorePages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'cmspageId'], 'required'],
            [['storeId', 'cmspageId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'cmspageId' => 'Cmspage ID',
        ];
    }
    public function getStore()
    {
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }
    public function getCmsPage()
    {
        return $this->hasOne(CmsPages::className(), ['id' => 'cmspageId']);
    }


}
