<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $title
 * @property string $dateUpdated
 * @property integer $storeId
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'storeId'], 'required'],
            [['dateUpdated'], 'safe'],
            [['storeId'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'dateUpdated' => 'Date Updated',
            'storeId' => 'Store ID',
        ];
    }
    public function getGalleryImages()
    {
        return $this->hasMany(GalleryImages::className(), ['galleryId' => 'id']);
    }
}
