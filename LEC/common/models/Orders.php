<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use common\models\Stores;
use common\models\User;
use yii\widgets\ListView;
use frontend\components\Helper;

class Orders extends \yii\db\ActiveRecord
{
    public $reportFrom = null;
    public $reportTo = null;
    public $reportStoreId = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'quantity',  'shippingMethodId', 'discountAmount', 'subTotal', 'shippingAmount', 'grandTotal'], 'required'],
            [['storeId', 'quantity', 'customerId', 'billingAddressId', 'shippingAddressId', 'shippingMethodId'], 'integer'],
	    //[['shipping_postcode',  'billing_postcode'], 'number', 'max' => '999999', 'tooBig' => 'Postal Code is invalid'],	
            [['type', 'shipping_street', 'shipping_state', 'billing_street', 'discountDescription', 'customerNotes', 'gatewayResponse'], 'string'],
            [['discountAmount', 'subTotal', 'shippingAmount', 'grandTotal'], 'number'],
            [['orderDate', 'gatewayResponse'], 'safe'],  
            [['email', 'shipping_firstname', 'shipping_lastname', 'shipping_company', 'shipping_city', 'shipping_country', 'shipping_phone', 'shipping_fax', 'billing_firstname', 'billing_lastname', 'billing_company', 'billing_city', 'billing_state', 'billing_country', 'billing_phone', 'billing_fax'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    
     

    
    public function attributeLabels()
    {
        return [
            'id' => 'Order#',
            'storeId' => 'Store ID',
            'orderDate' => 'Order Date',
            'quantity' => 'Quantity',
            'type' => 'Type',
            'customerId' => 'Customer ID',
            'billingAddressId' => 'Billing Address',
            'shippingAddressId' => 'Shipping Address',
            'email' => 'Email',
            'shipping_firstname' => 'Shipping Firstname',
            'shipping_lastname' => 'Shipping Lastname',
            'shipping_company' => 'Shipping Company',
            'shipping_street' => 'Shipping Street',
            'shipping_city' => 'Shipping City',
            'shipping_state' => 'Shipping State',
            'shipping_postcode' => 'Shipping Postcode',
            'shipping_country' => 'Shipping Country',
            'shipping_phone' => 'Shipping Phone',
            'shipping_fax' => 'Shipping Fax',
            //'sameAsBilling' => 'Same As Shipping',
            'billing_firstname' => 'Billing Firstname',
            'billing_lastname' => 'Billing Lastname',
            'billing_company' => 'Billing Company',
            'billing_street' => 'Billing Street',
            'billing_city' => 'Billing City',
            'billing_state' => 'Billing State',
            'billing_postcode' => 'Billing Postcode',
            'billing_country' => 'Billing Country',
            'billing_phone' => 'Billing Phone',
            'billing_fax' => 'Billing Fax',
            'shippingMethodId' => 'Shipping Method',
            'discountAmount' => 'Discount Amount',
            'discountDescription' => 'Discount Description',
            'subTotal' => 'Sub Total',
            'shippingAmount' => 'Shipping Amount',
            'grandTotal' => 'Grand Total',
            'customerNotes' => 'Customer Notes',
        ];
    }

    public function getOrderTotal(){
        $total = 0;
        foreach($this->orderItems as $item){
            $total = $total + ($item->price*$item->quantity);
        }
        return $total;
    }

    public function getTotalCount(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getSalesItemsCount(){
        $query = Orders::find()
        ->join('LEFT JOIN', 'OrderItems oi', 'Orders.id = oi.orderId')
        ->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
    if($this->reportStoreId != 0)
        $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getSalesTotal(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
    if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
    return $query->sum('grandTotal');
    }

    public function getInvoiced(){
        $query = Orders::find()
        ->join('LEFT JOIN', 'Invoices i', 'Orders.id = i.orderId')
        ->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
    if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
    return $query->sum('i.grandTotal');
    }

    public function getShipped(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
    if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
    return $query->sum('shippingAmount');
    }

    public function getRefunded(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
    if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
    return $query->sum('totalRefunded');
    }

    public function getDiscount(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND type="b2c"');
    if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
    return $query->sum('discountAmount');
    }

    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['orderId' => 'id']);
        // $query = new \yii\db\ActiveQuery('common\models\OrderItems');
        // return $query->where("orderId=".$this->id."");
    }

    public function getActivity(){
        return $this->hasMany(\common\models\OrderActivity::className(), ['orderId' => 'id'])->orderBy('date DESC');
    }

    public function getSuppliersOrderItems($suid)
    {
        return $this->hasMany(OrderItems::className(), ['orderId' => 'id'])->andWhere(['orderId' => $this->id])->andWhere(['supplierUid' => $suid])->andWhere(['status' =>'pending']);
    }
    public function getStore(){
        if($this->storeId!=''){        
            return $this->hasOne(Stores::className(), ['id' => 'storeId']);
        } else {
            return $this->hasOne(Stores::className(), ['adminId' => 'customerId']);
        }        
    }
    
    public function getVoucher(){
        return $this->hasOne(GiftVouchers::className(), ['id' => 'voucherId']);
    }

    public function getSuppliers(){
        return Suppliers::find()
        ->join('JOIN', 'OrderItems oi', 'oi.supplierUid = Users.id')
        ->join('JOIN', 'Orders o', 'oi.orderId = o.id')
        ->where('o.id = '.$this->id)
        ->groupBy('Users.id');
    }

    public function hasGiftVoucher(){
        foreach($this->orderItems as $item){
            //$_REQUEST['overrideProductCheck'] = true;
            if($item->product->typeId=="gift-voucher")
                return true;
        }
        return false;
    }

    public function getGiftVouchers(){
        return $this->hasMany(OrderItems::className(), ['orderId' => 'id'])->join('JOIN', 'Products', 'Products.id = OrderItems.productId')->andWhere('Products.typeId = "gift-voucher"');
    }

    public function getShippingMethod(){
        return $this->hasOne(ShippingMethods::className(), ['id' => 'shippingMethodId']);
    }

    public function hasGiftVouchersApplied(){
        return is_null($this->vouchers)? false : json_decode($this->vouchers);
    }

    public function hasCouponsApplied(){
        return is_null($this->coupons)? false : json_decode($this->coupons);
    }

    public function getBillingAddress(){
        return $this->hasOne(UserAddresses::className(), ['id' => 'billingAddressId']);
    }
    public function getShippingAddress(){
        return $this->hasOne(UserAddresses::className(), ['id' => 'shippingAddressId']);
    }

    public function getVoucherDiscount(){
        $discount = 0;
        $vouchers = json_decode($this->vouchers, true);
        foreach($vouchers as $id => $disc){
            $discount += $disc;
        }
        return $discount;
    }

    public function getBillingAddressText(){
        if(!is_null($this->billingAddress)){
            return nl2br($this->billingAddress->addressAsText);
        }else{
            return '<b>'.$this->billing_firstname." ".$this->billing_lastname.'</b><br/>'.$this->billing_street.'<br>'.$this->billing_city.' '.$this->billing_state.' '.$this->billing_postcode.'<br/> Phone: '.$this->billing_phone;
        }
    }

    public function getShippingAddressText(){
        if(!is_null($this->shippingAddressId)){
            return $this->shippingAddress->addressAsText;
        }else if(!is_null($this->shipping_firstname)){
            return $this->shipping_firstname." ".$this->shipping_lastname.', '.$this->shipping_street.', '.$this->shipping_city.', '.$this->shipping_postcode.', Phone : '.$this->shipping_phone;
        }
        else
        {
            return 'N/A';
        }
    }


    public function sendB2bOrderConfirmation(){
        $order_id=$this->id;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'b2borderconfirmation'])->one();
        // var_dump($template); die;
        //$user = \common\models\User::find()->where(['id' => $user_id])->one();
        $order = \common\models\Orders::find()->where(['id' => $order_id])->one();
        $user = \common\models\User::find()->where(['id' => $this->customerId])->one();
        //var_dump($this->customerId); die;
        $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
        $useremail=$user->email;
        $allorderurl=$order->orderUrl;
        //$user = \common\models\User::find()->where(['id' => $user_id])->one();
        $order = $this;
        $customerNotes=($order->customerNotes!='')?'Order comment : '.$order->customerNotes:''; 
        //to the user
        $queue = new \common\models\EmailQueue;
        $queue->models = ['order'=>$order]; 
        $queue->replacements =[ 'fromEmailAddress'=>'','siteUrl'=>'','Logo'=>$this->Logo,
            'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'','customerNotes'=>$customerNotes,
            'emailLinkedinImage'=>'','userfullname'=>$userfullname,'useremail'=>$useremail,'allorderurl'=>$allorderurl, 'footerText' => 'Thanks for using the Leading Edge B2B Platform'];
            
        $queue->from = "no-reply";
        //$queue->recipient = $user;//var_dump($template);die;
        $queue->recipients = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $useremail;
        //$queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();

        // to the admins
        $queue = new \common\models\EmailQueue;
        $queue->models = ['order'=>$order]; 
        $queue->replacements =[ 'fromEmailAddress'=>'','siteUrl'=>'','Logo'=>$this->Logo,
            'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'','customerNotes'=>$customerNotes,
            'emailLinkedinImage'=>'','userfullname'=>$userfullname,'useremail'=>"aaron@xtrastaff.com.au",'allorderurl'=>$allorderurl,
             'footerText' => 'Thanks for using the Leading Edge B2B Platform'];
                                

        $queue->from = "no-reply";
        //$queue->recipient = $user;//var_dump($template);die;
        //$recipient = "zarbj@leadingedgegroup.com.au,admin@legj.com.au,oled@leadingedgegroup.com.au,anto@everybuy.com.au,sreedev@everybuy.com.au";
        $recipient =Yii::$app->params['b2bOrderNotificationRecipients'];
        $queue->recipients = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $recipient;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template); //var_dump($queue->message);die;
       // var_dump($queue);die();
        //var_dump(Yii::$app->params['tempOrderreciepient']);die;
        $queue->save();
    }

    public function sendOrderConfirmation()  // send to user
    {
        $portalText = '';
        if($this->type=="client-portal"){
            $template = \common\models\EmailTemplates::find()->where(['code' => 'clientportalorderconfirmation'])->one();
            $portal = $this->portal;
        }
        elseif ($this->type=="byod") {
            $template = \common\models\EmailTemplates::find()->where(['code' => 'byodorderconfirmation'])->one();
            $portal = $this->portal;
            if($this->portal->shipment_type == "delivery-program" && $this->portal->payment_type == "online-payment")
                $portalText = "Your order will be ready for collection at school on ".Helper::date($this->portal->schoolDeliverydate, "j F, Y");    
        }
        else{
            $template = \common\models\EmailTemplates::find()->where(['code' => 'orderconfirmation'])->one();
            $portal = '';
        }


        $store = \common\models\Stores::find()->where(['id' => $this->storeId])->one();//var_dump($store->id);die;
        if($this->fromRegisteredUser()==true){        
            $user = \common\models\User::find()->where(['id' => $this->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
            $allorderurl=$this->orderUrl;
        } else { 
            $userfullname=ucfirst($this->billing_firstname).' '.ucfirst($this->billing_lastname);
            $useremail=$this->email;
            $allorderurl='';
        }
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store'=>$store,'order'=>$this,'portal'=>$portal]; 
        $queue->replacements =[ 'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$store->logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'allorderurl'=>$allorderurl,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$this->storeAddress,'portalText'=>$portalText]; 
        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        //$queue->recipients = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }

    public function sendOrderNotification() // send to admin
    {
        //$user_id=$this->customerId;
        if($this->fromRegisteredUser()==true){        
            $user = \common\models\User::find()->where(['id' => $this->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
        } else {
            $userfullname=ucfirst($this->billing_firstname).' '.ucfirst($this->billing_lastname);
        }

        $otherRecipients = "";        
        $adminuser = \common\models\Stores::findOne(Yii::$app->params['storeId'])->admin;
        $useremail = $adminuser->email;
        //$useremail = 'a@xtrastaff.com.au';
        $store_id=$this->storeId;
        $order_id=$this->id;
        if($this->type=="client-portal"){
            $template = \common\models\EmailTemplates::find()->where(['code' => 'clientportalordernotification'])->one();
            $portal = $this->portal;
        }
        elseif($this->type=="byod"){
            $template = \common\models\EmailTemplates::find()->where(['code' => 'byodordernotification'])->one();
            $portal = $this->portal;
        }
        else{    
            $template = \common\models\EmailTemplates::find()->where(['code' => 'ordernotification'])->one();
            $portal = '';
        }    
        //$user = \common\models\User::find()->where(['id' => $user_id])->one();
        $store = \common\models\Stores::find()->where(['id' => $store_id])->one();
        $order = $this;
        //var_dump($template);die;
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store'=>$store,'order'=>$order,'portal'=>$portal]; 
        if($otherRecipients = \common\models\Configuration::findSetting('order_confirmation_recipients',Yii::$app->params['storeId']))
            $queue->recipients = $otherRecipients;
        else
            $queue->recipients = $useremail;
            
        $queue->replacements =[ 'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$this->logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$queue->recipients,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$this->storeAddress,];                                    
        $queue->from = "no-reply";
            //$queue->recipients = $useremail;
        $queue->recipients = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $queue->recipients;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }


    public function sendSupplierNotification(){
        if($this->type == "b2b"){
            $template = \common\models\EmailTemplates::find()->where(['code' => 'suppliernotification'])->one();
            //var_dump($template);
            $suppliers = $this->getSuppliers()->all();
            $store = \common\models\Stores::find()->where(['id' => $this->storeId])->one();
            $order =  \common\models\Orders::findOne($this->id); // only for date issue
            $customerNotes=($order->customerNotes!='')?'Order comment : '.$order->customerNotes:''; 
            foreach($suppliers as $supplier){
                $queue = new \common\models\EmailQueue;
                $queue->models = ['order'=>$order, 'supplier' => $supplier,'store'=>$store];
                //var_dump(Yii::$app->params); die;
                $queue->replacements =[
                    'itemsGrid' => $supplier->getOrderItemsGrid($this->id),'fromEmailAddress'=>'','siteUrl'=>'','logo'=>$this->Logo, 'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'', 'emailGoogleplusImage'=>'','emailLinkedinImage'=>'','useremail'=>$supplier->email, 'userName'=>$order->user->fullName,'jeNumber'=>$order->user->jenumber,'customerNotes'=>$customerNotes,'supplierDashboardUrl' => Yii::$app->params['supplierDashboardUrl'].'/suppliers/orders/view?id='.$order->id,'orderTotal'=>$order->getSuppliersOrderTotal($supplier->id),
                ];

                //$supplier->email="aaron@xtrastaff.com.au"; // plz remove when live
                //$recipient = ",admin@legj.com.au,oled@leadingedgegroup.com.au,anto@xtrastaff.com.au";  
                $recipient =','.Yii::$app->params['b2bOrderNotificationRecipients'];               
                $queue->from = "no-reply";
                $supplier->email = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $supplier->email;
                $queue->recipients = $supplier->email.$recipient;
                //$queue->recipients = 'aaron@xtrastaff.com.au';
                $queue->creatorUid = Yii::$app->user->id;
                //var_dump($template->id); die;
                $queue->attributes = $queue->fillTemplate($template);
                //var_dump($queue->message);die;
                $queue->save();
            } 
        }
      
    }
    public function sendSupplierAcknowledgeMail(){
        if($this->type == "b2b"){ //var_dump('hiii');
            $template = \common\models\EmailTemplates::find()->where(['code' => 'supplieracknowledge'])->one();
            //var_dump($template);die;
            //$suppliers = $this->getSuppliers()->all();
            $supplier = Suppliers::findOne(Yii::$app->user->Id);
            //var_dump($this->customer->firstname);die;
           
            $queue = new \common\models\EmailQueue;
            $queue->models = ['order'=>$this, 'supplier' => $supplier];
            //var_dump(Yii::$app->params); die;
            $queue->replacements =[
                'itemsGrid' => $supplier->getOrderItemsGrid($this->id),'fromEmailAddress'=>'','siteUrl'=>'','logo'=>$this->Logo,                    
               'emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'',                    
                'emailLinkedinImage'=>'','useremail'=>$this->customer->email,'username'=>$this->customer->firstname.' '.$this->customer->lastname,
                'supplierDashboardUrl' => Yii::$app->params['supplierDashboardUrl']
                                ];
            $queue->from = "no-reply";
            $queue->recipients = $this->customer->email;
            $queue->recipients = isset(Yii::$app->params['tempOrderreciepient']) ? Yii::$app->params['tempOrderreciepient'] : $queue->recipients;            
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);
           // var_dump($queue->message);die;
            $queue->save();
            

        }
      
    }


    public function cancelUserOrderMail()
    {
        $template = \common\models\EmailTemplates::find()->where(['code' => 'canceluserorder'])->one();
        $store = \common\models\Stores::find()->where(['id' => $this->storeId])->one();
        if($this->fromRegisteredUser()==true)
        {
            $user = \common\models\User::find()->where(['id' => $this->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
            $allorderurl=$this->orderUrl;
        }
        else
        {
            $userfullname=ucfirst($this->billing_firstname).' '.ucfirst($this->billing_lastname);
            $useremail=$this->email;
            $allorderurl='';
        }
        $queue = new \common\models\EmailQueue;
            $queue->models = ['store'=>$store,'order'=>$this]; 
            $queue->replacements =[ 'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$store->logo,
                'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
                'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,
                'emailLinkedinImage'=>$store->emailLinkedinImage,
                'userfullname'=>$userfullname,'useremail'=>$useremail,'allorderurl'=>$allorderurl];

            $queue->from = "no-reply";
            $queue->recipients = $useremail;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
            $queue->save();
    }
    
    public function getOrderId()
    {
        return str_pad($this->id,5,"0",STR_PAD_LEFT);
    }
    public function getOrderPlacedDate()
    {
        //return Helper::date($this->orderDate, "l jS \of F Y h:i:s A");
        return Helper::date($this->orderDate, "j F, Y");
    }
   
    public function getCustomerAddress()
    {
        if($this->customerId==NULL)
        {
            $billing_address=$this->billing_firstname.' '.$this->billing_lastname.'<br>'.$this->billing_street.'<br>'.$this->billing_city.' '.$this->billing_postcode;
        }
        else
        {
            $useradd = UserAddresses::find()->where([ 'userId' => $this->customerId])->one(); 
            $billing_address=$useradd->streetAddress.'<br>'.$useradd->zip;
        }
        return $billing_address;
    }

    public function getShortGrid(){
        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getOrderItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => (Yii::$app->id == "frontend")?'/orders/_maillistview':'/orders/_maillistview',
        ]);
    }
     public function getStatusUpdateShortGrid(){
        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getOrderItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/orders/_mailStatusUpdate',
        ]);
    }

    public function getPortalOrderItemsShortGrid()
    {
        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getOrderItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/byod-orders/_pdflistview',
        ]);
    }

    public function getB2bShortGrid(){
        return \yii\grid\GridView::widget([
             'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getOrderItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
               [
                    'label'=> 'Product Name',
                    'attribute' => 'productId',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        //return $model->product->name; 
                         return $model->product->name; 
                    }
                ],
                /*[
                    'label' => 'Option(s)',
                    'value' => 'superAttributeValuesText',
                    'format' => 'html'
                ],*/
                [
                    'label'=> 'SKU',
                    'attribute' => 'sku',
                    //'value' => 'sku'
                ],
                [
                    'label' => 'Supplier',
                    'attribute' => 'product.supplierName'
                ],
                [
                    'label' => 'To Store',
                    'attribute' => 'id',
                    'format' => 'html',
                    'value' => function ($model) {
                        return ($model->b2baddress)? $model->b2baddress->storeName : "N/A";
                    },
                ],
                [
                    'label'=> 'Item Price',
                    'format' => 'html',
                     'value' => function ($model) {
                        return Helper::money($model->price);
                    },
                ],
                'quantity',
                [
                   'label' => 'Item Total',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return Helper::money($model->price * $model->quantity);
                    }
                   
                ],
            ],
        ]);
    }

    
    public function getOrderItemsGrid()
    {
        $query = new \yii\db\ActiveQuery('common\models\OrderItems');
        return $query->where("orderId=".$this->id."")->limit(10);
    }
    public function getShippingAmountFormatted()
    {
        return Helper::money($this->shippingAmount);
    }
    public function getCouponDiscountFormatted()
    {
        return Helper::money($this->couponDiscount);
    }
    public function getOrderItemTotalFormatted()
    {
        return Helper::money($this->subTotal);
    }
    public function getGrandTotalFormatted()
    {
        return Helper::money($this->grandTotal);
    }

    public function getOrderUrl()
    {
        if($this->type == "b2b"){
            return '<a href="'.Yii::$app->params['b2bUrl'].\yii\helpers\Url::to(['account/orders']).'">See All Items';
        }else{
            //$link=$this->getStoreUrl();
            $link=Yii::$app->params['adminUrl'];
            return '<a href="'.$link.\yii\helpers\Url::to(['/orders/view','id'=>$this->id]).'">See All Items</a>';
        }
    }
    public function getStoreUrl()
    {
        $stores=Stores::find()->where(['id' => $this->storeId])->one();
        return $stores->siteUrl;
    }
    public function getHelpUrl()
    {
        $link=$this->getStoreUrl();
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/about']).'">';
    }
   
    public function getCustomer()
    {
        if($this->customerId==Null)
        {
            $user=new User;
            $user->firstname=$this->billing_firstname;$user->lastname=$this->billing_lastname;$user->email=$this->email;
            return $user;
        }
        else
        {
            return $this->hasOne(User::className(), ['id' => 'customerId']);
        }
    }
    public function getStoreName() {

        return $this->store->title;
    } 
    public function getInvoiceOrderStatus()
    {
        $orderitems=$this->orderItems; 
        foreach ($orderitems as $key => $value) {
            if($value->status!='pending')
            {
                return 1;
            }
        }
        return 0;
    }
    public function getShippingOrderStatus()
    {
        $orderitems=$this->orderItems;
        foreach ($orderitems as $key => $value) {
            if($value->status!='shipped' && $value->status!='refunded')
            {
                return 1;
            }
        }
        return 0;
    }
    public function getCreditMemoOrderStatus()
    {
        $orderitems=$this->orderItems;
        foreach ($orderitems as $key => $value) {  
            if($value->status!='refunded')
            {
                return 1;
            }
        }
        return 0;
    }
    public function getDeliveryOrderStaus(){
        $orderitems=$this->orderItems;
        foreach ($orderitems as $key => $orderitem) {  
            if($orderitem->product->typeId!="gift-voucher")
            {
                return true;
            }
        }
        return false;
    }

    public function fromRegisteredUser(){
        return !is_null($this->customerId);
    }
     public function invoicesCreate()
    {
        $order=$this;
        $invoices=new Invoices();
        $invoices->orderId=$this->id;
        $invoices->subTotal=$this->subTotal;
        $invoices->shipping=$this->shippingAmount;

        $invoices->grandTotal=$this->grandTotal-$this->discountAmount;
        $invoices->status='paid';
        $invoices->notify=1;
        $invoices->save(false);
        $orderitems=OrderItems::find()->where(['orderId'=>$this->id])->all();
        $order_status=0;        
        if(isset($orderitems))
        {
            foreach ($orderitems as $key => $orderitem) {
                $invoicesitems= new InvoiceItems;
                $invoicesitems->invoiceId=$invoices->id;
                $invoicesitems->orderItemId=$orderitem->id;
                $invoicesitems->quantityInvoiced=$orderitem->quantity;
                $invoicesitems->save();
                if($orderitem->product->typeId=="gift-voucher")
                {
                    $orderitem->status='shipped'; 
                }
                else
                {
                    $orderitem->status='invoiced';
                    $order_status=1; 
                } 
                $orderitem->save(false);
            }
        }
        if($order_status==1)
            $order->status='in-process'; 
        else      
            $order->status='complete';      
        $order->save(false);        
        $invoices->sendInvoiceMail();       
        $invoices->save(false);       
        //var_dump('done');die;
    }

    public function getOrderDeliveredAddress(){ 
        //return $this->shippingMethod->title;
        if($this->type == "byod"){
            return $this->store->title.'<br/>'.$this->store->billingAddress.'<br/>Phone : '.$this->store->phone;
        }
        /*if(!isset($this->shippingMethod)){
            return 'N/A';
        } */
        if($this->shippingMethod->title=='Collect from Store'){    
            if(isset($this->collectFrom)){
                return (isset($this->b2bAddresses))? '<b>'.$this->b2bAddresses->storeName.'</b><br/>'.$this->b2bAddresses->streetAddress.'<br/>'.$this->b2bAddresses->city.'  '.$this->b2bAddresses->state.'  '.$this->b2bAddresses->postCode.'<br/> Phone : '.$this->b2bAddresses->phone :'N/A';
            } else{
                return $this->store->title.'<br/>'.$this->store->billingAddress.'<br/>Phone : '.$this->store->phone;     
            }    
        }
        return '<b>'.$this->store->title.'</b><br/>'.$this->store->billingAddress.'<br/>Phone : '.$this->store->phone;
        //return $this->shippingAddressText;
    }    
    
    public function getContactusUrl()
    {  
       $link=$this->getStoreUrl();
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/contact']).'"> Contact Us</a>';
    } 
    public function getAppliedTax()
    {
        return $tax_amount=$this->grandTotal-($this->grandTotal/1.1);
    }
    
    public function getPaymentDetails()
    {
        //var_dump($this->fromPaymentGateway);die;
        $paymentgateway='';$vouchers='';
        if($this->fromPaymentGateway!=NULL){ 
            //--- country of issuance  @ aaron 30/11/2016-------------
            /*$countryOfIssuance=OrderMeta::findValue('countryOfIssuance',$this->id);            
            $country_of_issuance=isset($countryOfIssuance) ? $countryOfIssuance : 'N/A';*/
            //--- end  country of issuance  @ aaron 30/11/2016-------------            
            if($this->cardNumber!=NULL){            
                $paymentgateway='Credit Card '.$this->cardNumber.' : '.Helper::money($this->fromPaymentGateway);
            } else {
                $paymentgateway='PayPal : '.Helper::money($this->fromPaymentGateway);
            }
            //$paymentgateway=$paymentgateway.' <span style="color:blue;">|</span> Country Of Issuance: '.$country_of_issuance;
        }
        if($this->fromVoucher!=NULL){        
            $vouchers='eGift Card : '.Helper::money($this->fromVoucher);
        }
        return $vouchers." ".$paymentgateway;
    }
    public function getCountryOfIssuance(){
        $countryOfIssuance=OrderMeta::findValue('countryOfIssuance',$this->id);            
        return $country_of_issuance=isset($countryOfIssuance) ? 'Country Of Issuance: '.$countryOfIssuance : 'Country Of Issuance: N/A';        
    }
   
    public function getCouponDiscount(){
        if(!is_null($this->coupons)){
            return array_sum(array_values(json_decode($this->coupons, true)));
        }
        return 0;
    }


    public function getFromPaymentGateway(){
        if(!is_null($this->vouchers)){
            $vouchers = json_decode($this->vouchers, true);
            $amount = array_sum(array_values($vouchers));
            return $this->grandTotal - $amount;
        }
        return $this->grandTotal;
    }

    public function getFromVoucher(){
        if(!is_null($this->vouchers)){
            $vouchers = json_decode($this->vouchers, true);
            return array_sum(array_values($vouchers));
        }
        return 0;
    }
    public function getFormattedAppliedTax()
    {
        return Helper::money($this->grandTotal-($this->grandTotal/1.1));
    }
    public function getPaymentStatus()
    {
        if($this->status == "payment-pending")
        {
            return 'Payment Pending';
        }
        elseif (($this->status == "partially-collected" || $this->status == "fully-collected")  && $this->type == "byod") {
                $deliveries = Deliveries::find()->where(['orderId'=>$this->id])->all();
                $amountPaid = 0;
                foreach ($deliveries as $delivery) {
                    $amountPaid = $amountPaid+$delivery->amount;
                }
                if($amountPaid >= $this->grandTotal)
                    return "Full Payment Received";
                else
                    return "Partial Payment Received";

           
        }
        else
        {
            return 'Paid';
        }
    }

    public function getAmountPaid()
    {
        if($this->type="byod"){
            if($this->portal->payment_type == "no-online-payment" && $this->portal->shipment_type == "instore-pickup"){
                $deliveries = Deliveries::find()->where(['orderId'=>$this->id])->all();
                $amountPaid = 0;
                foreach ($deliveries as $delivery) {
                    $amountPaid = $amountPaid+$delivery->amount;
                }
                return $amountPaid;
            }
        }
    }

    public function getLogo()
    {
        return '<img  src='.Yii::$app->params["rootUrl"].'/store/site/logo/logo.png>';
       
    }
    public function getInvoiceStatus()
    {        
        foreach ($this->orderItems as $key => $value) {
            if($value->status=='pending')
            {
                return 1;
            }
        }
        return 0;
    }
    public function getOrderDiscountAmount()
    {
        $oi_discount=0;
        //$orderdiscount=$this->couponDiscount;
        foreach ($this->orderItems as $key => $value) {
            $oi_discount=$oi_discount+$value->discount;
        }
        return $oi_discount+$this->couponDiscount;

    }
    public function getFormatedOrderDiscountAmount()
    {
        return Helper::money($this->orderDiscountAmount);
    }
    public function getUser()
    {  
        return $this->hasOne(User::className(), ['id' => 'customerId']);
    }
    public function getUserEmail()
    {
        if($this->fromRegisteredUser()==true){        
            $user = \common\models\User::find()->where(['id' => $this->customerId])->one();
            $useremail=$user->email;
        } 
        else { 
            $useremail=$this->email;
        }
    }
    public function getSuppliersOrderItemsCount(){
        $total = 0;
        $suppliers_orderItems=OrderItems::find()->where(['orderId'=>$this->id,'supplierUid'=>Yii::$app->user->Id])->all();
        foreach($suppliers_orderItems as $item){
            $total = $total + $item->quantity;
        }
        return $total;
    }
    /*public function getUserJenumber()
    {  
        return $this->user->jenumber;
    }*/
    public function getSuppliersOrderStatus()
    {
        $supplier = Suppliers::findOne(Yii::$app->user->Id);
        $orderitems = $supplier->getOrderItems($this->id);
        $command = $orderitems->createCommand();
        $orderItems = $command->queryAll();
        foreach ($orderItems as $key => $orderItem) {
            if($orderItem['status']!='acknowledged')
            {
                return 'Pending';
            }
        }
        return 'Acknowledged';
    }

    public function getB2bStatus(){
        foreach($this->orderItems as $item){
            if($item->status != "acknowledged")
                return 'Pending';
        }
        return 'Acknowledged';
    }
    public function getSuppliersOrderTotal($sid){
        $total = 0;
        $supplier = Suppliers::findOne($sid);
        $orderitems = $supplier->getOrderItems($this->id);
        $command = $orderitems->createCommand();
        $orderItems = $command->queryAll(); //var_dump($orderItems);die;
         foreach ($orderItems as $key => $orderItem) {  
            $total = $total + ($orderItem['price']*$orderItem['quantity']);
        }
       return $total;
    }
    
    public function getPortal(){
        return $this->hasOne(ClientPortal::className(), ['id' => 'portalId']);
    }
    
    public function getOrderGiftWrapTotal() {
        return $this->hasMany(OrderItems::className(), ['orderId' => 'id'])->sum('giftWrapAmount*quantity');
    }
    
    public function getGiftWrapTotalFormatted()
    {
        return Helper::money($this->orderGiftWrapTotal);
    }

    public function hasGiftWrap() {
        $total = 0;
        foreach ($this->orderItems as $item) {
            if($item->giftWrap==1)
                return true;
        }
        
        return false;
    }

    public function getPaymentInformation(){
        $payinf='';
        if($this->status=='payment-pending'){        
            $payinf=$payinf.'<span style="color:red"> <i class="fa fa-warning"></i> Payment Not Received! </span><br>';
        }           
        if ($this->paymentMethod == "Braintree" || $this->paymentMethod == "Eway") {
            if (is_null($this->cardNumber)) {
               $payinf=$payinf.'<p>Payment Method: Paypal</p> ';
            }
            else{
                $payinf=$payinf.'<p>Payment Method: Credit Card </p>';
                $payinf=$payinf.'<p>Card Number: '.$this->cardNumber.'</p>';
            }
            $payinf=$payinf.'<p>Transaction ID: '.$this->transactionId.'</p>';
            if(!is_null($this->vouchers))
            {
              $giftamount=array_sum(array_values(json_decode($this->vouchers, true)));
              $payinf=$payinf.'<p>eGift Card : '.Helper::money($giftamount).' </p>';
            }
            //-----  country of issuance @ dan # 30/11/2016 ----------
            /*$countryOfIssuance=OrderMeta::findValue('countryOfIssuance',$this->id);
            $riskDataDecision=OrderMeta::findValue('riskDataDecision',$this->id);
            $country_of_issuance=isset($countryOfIssuance) ? $countryOfIssuance : 'N/A';
            $risk_decision=isset($riskDataDecision) ? $riskDataDecision : 'N/A';
            $payinf=$payinf.'Country Of Issuance: '.$country_of_issuance.'<br/>'
                .'Risk Decision: '.$risk_decision.'<br/>';*/
            //----- @ end @--country of issuance @ dan----------
        } 
        elseif ($this->paymentMethod == "Gift Voucher") {
           $payinf=$payinf.'<p>Payment Method: eGift Card </p>';
        }
        else
        {
            if($this->status!='payment-pending')
            {
                if($this->status!="cancelled")
                    $payinf=$payinf.'<span style="color:red"><i class="fa fa-warning"></i> Failed Transaction!.</span>'; 
                else
                    $payinf=$payinf.'<span style="color:red"> <i class="fa fa-warning"></i> N/A </span>';
            }
        }
        return $payinf;
    }

    public function getDeliveryPaymentInformation(){
        $payinf='';
        if($this->status=='payment-pending')
        {
            $payinf=$payinf.'<span style="color:red"> <i class="fa fa-warning"></i> Payment Not Received! </span><br>';
        }           
        if ($this->paymentMethod == "Braintree" || $this->paymentMethod == "Eway") {
            if (is_null($this->cardNumber)) {
               $payinf=$payinf.'<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Payment Method: Paypal</p> ';
            }
            else{
                $payinf=$payinf.'<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Payment Method: By Card </p>';
                $payinf=$payinf.'<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Card Number: '.$this->cardNumber.'</p>';
            }
            $payinf=$payinf.'<p style="margin:0;color:#8c8c7b;">Transaction Id: '.$this->transactionId.'</p>';
            if(!is_null($this->vouchers))
            {
              $giftamount=array_sum(array_values(json_decode($this->vouchers, true)));
              $payinf=$payinf.'<p style="margin:0;color:#8c8c7b;">eGift Card : '.Helper::money($giftamount).' </p>';
            }
        } 
        elseif ($this->paymentMethod == "Gift Voucher") {
           $payinf=$payinf.'<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Payment Method: eGift Card </p>';
        }
        else
        {
            if($this->status!='payment-pending')
            {
                if($this->status!="cancelled")
                    $payinf=$payinf.'<span style="color:red"><i class="fa fa-warning"></i> Set as paid by Admin!.</span>'; 
                else
                    $payinf=$payinf.'<span style="color:red"> <i class="fa fa-warning"></i> N/A </span>';
            }
        }
        return $payinf;
    }

    public function getDeliveryBillingInfo(){
        if(!is_null($this->billingAddress)){
            return $this->billingAddress->deliveryAddressAsText;
        }else{
            return '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">'.$this->billing_firstname." ".$this->billing_lastname.'</p><p style="margin:0;margin-bottom:10px;color:#8c8c7b;">'.$this->billing_street.'</p><p style="margin:0;color:#8c8c7b;margin-bottom:10px;">'.$this->billing_city.' '.$this->billing_state.' '.$this->billing_postcode.'</p><p style="margin:0;color:#8c8c7b;"> E:'.$this->email.' | P:'.$this->billing_phone;
        }
    }

    public function getB2bAddresses(){
        return $this->hasOne(B2bAddresses::className(), ['id' => 'collectFrom']);
    }

    public function hasCreditMemo(){
        $store = Stores::findOne(Yii::$app->params['storeId']);
        if(CreditMemos::find()->where(['orderId'=>$this->id])->one())
            return true;
        else
            return false;
    }

    public function getCollectionCount(){
        $count = Deliveries::find()->where(['orderId'=>$this->id])->count();
        return $count;
    }
    public function getShipmentCount(){
        $count = Shipments::find()->where(['orderId'=>$this->id])->count();
        return $count;
    }
    public function getStoreAddressFormatted(){
        return $this->store->addressFormatted;
    }

    public function getStoreAddress(){   
        return $this->store->billingAddress;    
        /*$b2bAddress = B2bAddresses::find()->where(['ownerId'=>$this->store->adminId])->count();
        if($b2bAddress==1){
            //var_dump($this->collectFrom);die;
           return (isset($this->b2bAddresses))?$this->b2bAddresses->addressAsText:'';
        } else {
            return 'HO : '.$this->store->billingAddress;
        } */     
    } 
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/order-confirmation.jpg></a>'; 
    }

    public function getByodMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/byod-order-confirmation.jpg></a>'; 
    }

    public function getOrderNotificationMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/order-notification.jpg></a>'; 
    }    

    public function getByodOrderNotificationMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/byod-order-notification.jpg></a>'; 
    }

    public function hasOrderCollection(){
        foreach ($this->orderItems as $item) {            
            if($item->qtyReadyForDelivery!=0 && $item->qtyReadyForDelivery-$item->deleveredQtyCount!=0)
                return 1;
        }
    }
    public function hasOrdershipment(){
        foreach ($this->orderItems as $item) {            
            if($item->qtyReadyForDelivery!=0 && $item->qtyReadyForDelivery-$item->shipmentQtyCount!=0)
                return 1;
        }
    }
    public function getRemainingOrderItemIds(){
        $query = new \yii\db\ActiveQuery('\common\models\OrderItems');
        return Yii::$app->db->createCommand("SELECT id from OrderItems WHERE (quantity-qtyReadyForDelivery)!=0 and orderId =".$this->id)
        ->queryAll();
    }
    public function getOrderItemsCount(){
        $total = 0;
        $orderItems=OrderItems::find()->where(['orderId'=>$this->id])->all();
        foreach($orderItems as $item){
            $total = $total + $item->quantity;
        }
        return $total;
    }
    public function getFormattedStatus(){
        if(isset($this->status)){
            $status='';
            foreach (explode('-',$this->status) as $value) {
               $status=$status.ucfirst($value).' ';
            }
            return  $status;
        }
        //return $this->status;
    }

    public function getOrdersStatus(){
        //----------------------------refunded --------------------------
        $creditmemos=CreditMemos::find()->where(['orderId'=>$this->id])->all();
        $refundItemsCount=0;
        foreach ($creditmemos as  $creditmemo) {
            $refundItemsCount=$refundItemsCount+$creditmemo->refundItemCount;
        }
        //----------------------------collection --------------------------
        $collections=Deliveries::find()->where(['orderId'=>$this->id])->all();
        $collectionItemCount=0;
        foreach ($collections as  $collection) {
            $collectionItemCount=$collectionItemCount+$collection->deliveryItemCount;
        }
        //----------------------------collection --------------------------
        $shipments=Shipments::find()->where(['orderId'=>$this->id])->all();
        $shipmentItemCount=0;
        foreach ($shipments as  $shipment) {
            $shipmentItemCount=$shipmentItemCount+$shipment->shipmentItemCount;
        }
        //----------------------------ready for delivery --------------------------
        $readyForDelivery=orderItems::find()->where(['orderId'=>$this->id])->all();
        $readyForDeliveryCount=0;
        foreach ($readyForDelivery as  $items) {
            $readyForDeliveryCount=$readyForDeliveryCount+$items->qtyReadyForDelivery;
        }

        if($refundItemsCount==$this->orderItemsCount){
            return 'partially-refunded';
        } else if($refundItemsCount>0){
            return 'fully-refunded';
        } else if($collectionItemCount==$this->orderItemsCount || $shipmentItemCount==$this->orderItemsCount ){
            return isset($this->collectFrom)?'fully-collected':'fully-shipped';
        } else if($collectionItemCount>0 || $shipmentItemCount>0){
            return isset($this->collectFrom) ? 'partially-collected' : 'partially-shipped';
        } else if($readyForDeliveryCount==$this->orderItemsCount) {
            return isset($this->collectFrom)?'fully-ready-for-collection':'fully-ready-for-shipment';
        } else if($readyForDeliveryCount>0){
            return isset($this->collectFrom) ? 'partially-ready-for-collection' : 'partially-ready-for-shipment';
        } else{
            return 'in-process';
        }
    }

    public function addActivity($type, $entity = null){
        $activity = new \common\models\OrderActivity;
        $activity->orderId = $this->id;
        $activity->type = $type;
        $activity->entity = $entity;
        $activity->description = $activity->generateDescription();
        return $activity->save();
    }
    public function getOrderItemsComments(){ //@ aaron 01/12/2016 mail order item comments
        $comments='';$i=1;
        foreach($this->orderItems as $item){
            if($item->comment!=''){
                $comments.=$i.'. <b>'.$item->product->name.':</b> <span style="font-style: italic;" >'.$item->comment.'</span></br>';
                $i++;
            }
        }
        if($comments!=''){
            return '<tr>
                <td colspan="2" style="padding:20px 10px 0;text-align:center;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                        <tr> 
                            <td colspan="2" style="padding:0 0 20px;font-size:16px;font-weight:bold;padding-bottom:8px;border-bottom:1px solid #eeeeee;">Comments</td>  
                        </tr>
                        <tr>            
                            <td colspan="2" style="font-size:13px;border-bottom:1px solid #eeeeee;">'.$comments.'</td>
                        </tr>                   
                    </table>
                </td>
            </tr>';
        } else {
            return $comments;
        }        
    }
    public function getShipmentType(){  // only for order index    
       $shipmentType=isset($this->collectFrom)?'Collect in store':'Shipment';
       return '<span style="color:green;font-size:12px;">'.$shipmentType.'</span>';
    }
    
    public function getCreditMemoCheck() {
        $orderId = $this->id;

        $RefundItems = CreditMemos::find()->where(['orderId' => $orderId])->all();
        $grandTotal = 0;
        if (!empty($RefundItems)) {
            $grandTotal1 = 0;
            foreach ($RefundItems as $item) {
                $grandTotal1 += $item->grandTotal;
                $grandTotal = ($this->grandTotal) - $grandTotal1;
            }
            if (!empty($grandTotal))
                return true;
            else
                return false;
        } else
            return true;
    }

}
