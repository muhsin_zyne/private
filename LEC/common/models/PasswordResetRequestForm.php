<?php
namespace common\models;
use Yii;

use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $roleIdb2b;
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                //'filter' => (Yii::$app->id=="app-b2b" || Yii::$app->id=="app-backend")? ['status' => User::STATUS_ACTIVE, 'roleId' => '3'] :(Yii::$app->id=="app-suppliers")?['status' => User::STATUS_ACTIVE, 'roleId' => '2']: ['status' => User::STATUS_ACTIVE, 'roleId' => '4','storeId'=>Yii::$app->params['storeId']],
                'filter' => (Yii::$app->id=="app-b2b" || Yii::$app->id=="app-backend")? ['status' => User::STATUS_ACTIVE, 'roleId' => '3'] :(Yii::$app->id=="app-suppliers")?['status' => User::STATUS_ACTIVE, 'roleId' => '2']: ['status' => User::STATUS_ACTIVE, 'roleId' => '4'],
                'message' => 'Sorry this email does not belong to a registered user.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        //var_dump(Yii::$app->params['storeId']);die;
        $roleCheck = (Yii::$app->id=="app-backend" || Yii::$app->id=="app-b2b")? ['roleId' => '3'] : ((Yii::$app->id=="app-frontend")? ['storeId'=>Yii::$app->params['storeId']] : []);
        $user = User::findOne(array_merge([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
            
        ], $roleCheck));

        //var_dump($user);die();

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
               
            }
            $user->updated_at = time();
            if ($user->save(false)) {
                $template = \common\models\EmailTemplates::find()->where(['code' => 'passwordreset'])->one();
                //$user = \common\models\User::find()->where(['id' => $user->id])->one();
                $queue = new \common\models\EmailQueue;//var_dump($user->storeId);die;
                $template->layout = "layout";
                $queue->models = ['user'=>$user]; 
                if($user->storeId!=''){                
                    $store = \common\models\Stores::find()->where(($user->roleId == '3')? ['adminId' => $user->id] : ['id' => $user->storeId])->one();
                    $queue->replacements =[ 'siteUrl'=>$store->siteUrl,'logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'useremail'=>$user->email,'storeName'=>$store->title,'headerLogo'=>$this->mailBanner,'storeContactus'=>$store->contactusUrl,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->billingAddress];
                } else {
                    $queue->replacements =['siteUrl'=>'','Logo'=>'<img  src='.Yii::$app->params["rootUrl"].'/store/site/logo/logo_legc.png>','emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'','emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'','emailLinkedinImage'=>'','useremail'=>$user->email,'storeName'=>'','headerLogo'=>$this->mailBanner,'storeContactus'=>'','appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>''];
                }
                $queue->from = "no-reply";
                $queue->recipients = $user->email;
                $queue->creatorUid = Yii::$app->user->id;
                $queue->attributes = $queue->fillTemplate($template);
                //var_dump($queue->message);die;
                $queue->save();
            }

        }

        return true;
    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/user-password-reset.jpg></a>'; 
    }  
}
