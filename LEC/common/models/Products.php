<?php
/**
 * @author: Max
 */

namespace common\models;

use Yii;
use common\models\AttributeGroups;
use common\models\AttributeValues;
use yii\data\ActiveDataProvider;
use backend\components\Helper;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Stores;
use common\models\User;
use common\models\ProductInventory;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionProviderInterface;
use yz\shoppingcart\CartPositionTrait;
use yii\db\ActiveQuery;
use yii\base\Object;
use yii\helpers\Url;

class Products extends \yii\db\ActiveRecord implements CartPositionProviderInterface
{
    use \common\traits\base\BeforeQueryTrait;
    public $productAttrs = ['attributeSetId', 'typeId', 'id', 'sku', 'dateAdded', 'brandId', 'attributeSetName','createdBy', 'urlKey', 'status', 'visibility'];
    public $scene = "normal";
    public $images;
    public $storeId;
    public $BEFORE_QUERY;
    public $BEFORE_JOIN;
    public $recipientName;
    public $recipientEmail;
    public $qty;
    public $recipientMessage;
    public $supplier;
    private $updatedCustomAttributes = [];
    public $voucherValue;
    public $duplicate = false;
    //public $byod=false;
    /**
     * Max: Overriding getter to make it fetch the custom attributes of the product
     */
    public function __get($name){ //if($name=="name") { var_dump($this->getIsNewRecord()); die; }
        if(!in_array($name, $this->productAttrs) && !$this->getRelation($name, FALSE) && $this->scene == "normal" && !$this->getIsNewRecord() && !method_exists($this, "get$name") && !in_array($name, array_keys($this->updatedCustomAttributes))){
            if($value = $this->attrValue($name))
                return $value;
            else
                return "";
        }elseif(in_array($name, array_keys($this->updatedCustomAttributes))){
            return $this->updatedCustomAttributes[$name];
        }
        return parent::__get($name);
    }
   
    /**
     * Max: Overriding setter to make it set the values of custom attributes explicitley so that they won't get through attrValue()
     */
    public function __set($name, $value)
    {
        if(!in_array($name, $this->productAttrs)){
            $this->updatedCustomAttributes[$name] = $value;
        }
        parent::__set($name, $value);
    }

    public function init(){
        if(isset(Yii::$app->params['storeId']) && Yii::$app->id!="app-b2b"){  //it's frontend or b2b
            if(!empty(Yii::$app->params['temp']['assignedProducts'])){
                $storeProductIds = Yii::$app->params['temp']['assignedProducts'];//
            }else{ //var_dump($this->getAssignedProductsQuery()); die;
                $assignedProductsQuery = $this->getAssignedProductsQuery();
                if(Yii::$app->id == "app-frontend")
                    $assignedProductsQuery = $assignedProductsQuery->andWhere('enabled="1"');
                $storeProductIds = implode(',', array_keys(ArrayHelper::map($assignedProductsQuery->all(), 'id', 'productId')));
                Yii::$app->params['temp']['assignedProducts'] = $storeProductIds;
            }
            if($storeProductIds!="")
                $this->BEFORE_QUERY = "Products.id in ($storeProductIds)";
            else
                $this->BEFORE_QUERY = "Products.id in (0)"; //there are no products assigned for this store
        if(isset($_REQUEST['bypassStoreProductsCheck'])){
                $this->BEFORE_QUERY = "";
            } 
            $this->storeId = Yii::$app->params['storeId'];
        }elseif(Yii::$app->id == "app-b2b"){
            $this->BEFORE_QUERY = "(Products.createdBy = 0 and typeId not in ('gift-voucher'))";
        }
        //var_dump(Yii::$app->params['storeId']); die;
        if((Yii::$app->id == "app-backend" && Yii::$app->user->identity->roleId == "1") || Yii::$app->id == "app-suppliers"){
            $this->BEFORE_QUERY = $this->BEFORE_QUERY; //." ".(($this->BEFORE_QUERY == "")? "" : "AND")." (Products.visibility = 'everywhere')";
        }elseif(Yii::$app->id == "app-backend" && Yii::$app->user->identity->roleId == "3"){
            $this->BEFORE_QUERY = $this->BEFORE_QUERY." ".(($this->BEFORE_QUERY == "")? "" : "AND")." (Products.status = 1)";
        }elseif(Yii::$app->id == "app-console"){
            $this->BEFORE_QUERY = ""; //return all products if it's console.
        }else{
            if(Yii::$app->controller->byodOverride){
                $this->BEFORE_QUERY = $this->BEFORE_QUERY." ".(($this->BEFORE_QUERY == "")? "" : "AND")." (Products.status = 1 AND Products.visibility = 'everywhere')";
            }
            else{
                $subQuery = new ActiveQuery('\common\models\Products');
                $byodOnlyProducts = ArrayHelper::map(Yii::$app->db->createCommand("SELECT `Products`.id FROM `Products` JOIN `AttributeValues` `av` ON av.productId = Products.id JOIN `Attributes` `a` ON av.attributeId = a.id JOIN `StoreProducts` `sp` ON sp.productId = Products.id WHERE Products.status = 1 AND Products.visibility = 'everywhere' AND a.code='byod_only' AND av.value='Yes' AND sp.storeId =".Yii::$app->params['storeId']." AND av.storeId = ".Yii::$app->params['storeId']." GROUP BY `av`.`productId`")->queryAll(),'id','id');
                $this->BEFORE_QUERY = $this->BEFORE_QUERY." ".(($this->BEFORE_QUERY == "")? "" : "AND")." (Products.status = 1 AND Products.visibility = 'everywhere' AND Products.id not in ('".implode("','", $byodOnlyProducts)."'))";
            }    
        }
        //if(Yii::$app->id == "app-b2b"){ $this->BEFORE_QUERY = $this->BEFORE_QUERY." sdf";}
    if(isset($_GET['db'])){ var_dump(Yii::$app->params['storeId']); die; }
    //var_dump ($this->BEFORE_QUERY); die('fkjsgdhkfuew'); 
        //$this->BEFORE_QUERY = $this->BEFORE_QUERY." OR 1=1";
        parent::init();
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Products';
    }
    // public function behaviors()
    // {
    //     return [
    //         [
    //             'class' => SluggableBehavior::className(),
    //             'attribute' => 'name',
    //              'slugAttribute' => 'slug',
    //         ],
    //     ];
    // }
    public function attributes(){
        $attrs = [];
        if(isset($_REQUEST['Products']['attributeSetId'])){
                if(isset(Yii::$app->params['temp']['attributeSet']))
                    $attrSet = Yii::$app->params['temp']['attributeSet'];
                else{
                    $attrSet = AttributeSets::findOne($_REQUEST['Products']['attributeSetId']);
                    Yii::$app->params['temp']['attributeSet'] = $attrSet;
                }
            if(!is_null($attrSet)){ // set the rules for custom attributes
                foreach($attrSet->attributeGroups as $group)
                    foreach($group->productAttributes as $attr)
                        $attrs[] = $attr->code;
            }
        }
        return array_merge(parent::attributes(), $attrs);
    }
    public function rules(){
        $rules = [];
        if(isset($_REQUEST['Products']['attributeSetId'])){ // set the rules for custom attributes
            if(isset(Yii::$app->params['temp']['attributeSet']))
                $attrSet = Yii::$app->params['temp']['attributeSet'];
            else{
                $attrSet = AttributeSets::findOne($_REQUEST['Products']['attributeSetId']);
                Yii::$app->params['temp']['attributeSet'] = $attrSet;
            }
            foreach($attrSet->attributeGroups as $group){
                foreach($group->productAttributes as $attr){
                    if($attr->required=="1" && !($attr->code == "price" && Yii::$app->user->identity->roleId == "3" && !$this->isNewRecord && $this->createdBy == 0))
                        $rules[] = [[$attr->code], 'required'];
                    else
                        $rules[] = [[$attr->code], 'safe'];
                    if($attr->validation!=""){
                        if($attr->validation == "decimal")
                            $rules[] = [[$attr->code], 'match', 'pattern'=>'/^[0-9]{1,12}(\.[0-9]{0,4})?$/'];
                        else
                            $rules[] = [[$attr->code], $attr->validation];
                    }
                }
            }
        }
        return array_merge([
            [['attributeSetId','brandId'], 'integer'],
            [['attributeSetId', 'typeId', 'sku', 'brandId', 'urlKey'], 'required'],
            [['urlKey'], 'match','pattern' => '/[a-zA-Z0-9]/'],
            [['recipientMessage', 'recipientName', 'recipientEmail','voucherValue'], 'required', 'when' => function($model){ return $model->typeId=="gift-voucher"; }],
            [['voucherValue'], 'number', 'max' => 1000],
            [['voucherValue'], 'number', 'min' => 10],
            [['voucherValue'], 'required', 'when' => function($model){ return $model->typeId=="gift-voucher"; },'message' => 'eGift Card Value cannot be blank.'],
            [['voucherValue'], 'number', 'message' => 'eGift Card Value must be a number.'],
            [['voucherValue'], 'number', 'max' => 1000,'tooBig' => 'eGift Card Value must be no greater than AU$1000.'],
            [['voucherValue'], 'number', 'min' => 10,'tooSmall' => 'eGift Card Value must be no less than  AU$10.'],
            [['voucherValue'], 'required', 'when' => function($model){ return $model->typeId=="gift-voucher"; },'message' => 'eGift Card Value cannot be blank.'],
            [['voucherValue'], 'number', 'message' => 'eGift Card Value must be a number.'],
            [['voucherValue'], 'number', 'max' => 1000,'tooBig' => 'eGift Card Value must be no greater than AU$1000.'],
            [['voucherValue'], 'number', 'min' => 10,'tooSmall' => 'eGift Card Value must be no less than  AU$10.'],
            [['recipientEmail'], 'email'],
            [['images','sku','dateAdded','brandId', 'urlKey', 'visibility', 'status'], 'safe'],
            [['sku'], 'unique', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Categories', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Products', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\CmsPages', 'targetAttribute' => 'slug', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Brands', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['f_size', 'length', 'qty'], 'required', 'when' => function($model){ return $model->scenario == "purchase"; }, 'whenClient' => "function(attribute, value){ return (jQuery('.nav-tabs-custom').length == 0); }"],
            [['qty'], 'integer', 'min' => 1],
            [['images'], 'file', 'extensions'=>'jpg, gif, png'],
        ], $rules);
    }
    public function extraFields(){
        $nativeAttributes = ['baseImage', 'thumbnailImage', 'smallImage', 'sellingPrice', 'attributeValuesAsArray', 'markup', 'brandName', 'supplierName', 'cataloguePageNumber', 'sellingPrice'];
        if(isset(Yii::$app->params['temp']['allAttributes']))
            return Yii::$app->params['temp']['allAttributes'];
        else{
            Yii::$app->params['temp']['allAttributes'] = array_merge(array_values(ArrayHelper::map(Attributes::find()->all(), 'code', 'code')), $nativeAttributes);
            return Yii::$app->params['temp']['allAttributes'];
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = [];
        if(isset($this->attributeSet)){
            foreach($this->attributeSet->attributeGroups as $group){
                foreach($group->productAttributes as $attr){
                    $labels[$attr->code] = $attr->title;
                }
            }
        }
        return array_merge([
            'id' => 'ID',
            'title' => 'Title',
            'typeId' => 'Type',
            'attributeSetId' => 'Attribute Set',
            'slug' => 'URL Key',
            'brandId' => 'Brand',
            'recipientMessage' => 'Message',
            'sku' => 'SKU',
            'f_size' => 'Finger Size'
        ], $labels);
    }
    public function getCartPosition($params = []){
        return \Yii::createObject([
            'class' => 'common\models\ProductCartPosition',
            'id' => $this->id,
        ]);
    }
    /**
     * Renders the fields in admin backend products create form
     */
    public function renderAttributes($group, $form){ 
        $renderer = \Yii::$app->attributeRenderer;
        $attributes = $group->productAttributes;
        if($group->title == "General"){
            if(Yii::$app->user->identity->roleId == "1")
                $suppliers = \common\models\Suppliers::find()->orderBy('firstname asc')->all();
            elseif(Yii::$app->user->identity->roleId == "3"){
                $suppliers = Yii::$app->user->identity->store->selectableSuppliers;
            }
            array_splice($attributes, 1, 0, ['sku', 'supplier', 'brandId']);
        }elseif($group->title == "Prices" && Yii::$app->user->identity->roleId == "3" && $this->createdBy == '0' && !$this->isNewRecord){ //to show 'Master Price' and 'Store Price'
            array_splice($attributes, 0, 0, ['masterPrice']);
        }
        $renderer->product = $this;
        $renderer->form = $form;
        //var_dump($renderer->form);die();
        if(count($attributes)>0){
            foreach($attributes as $attr){ //var_dump($attr); //die();
                if(!is_object($attr)){
                    if(in_array($attr, ['brandId', 'supplier']))
                        echo $form->field($this, $attr)->dropDownList(ArrayHelper::map($suppliers, 'id', 'fullName'));
                    else{ 
                        echo $form->field($this, $attr)->textInput();
                    }
                }else{
                    if($attr->code == "price" && Yii::$app->user->identity->roleId == "3" && !$this->isNewRecord && $this->createdBy == 0){
                        echo $form->field($this, $attr->code)->textInput(['value' => $this->getStorePrice(Yii::$app->params['storeId'])]);
                    }else
                        echo ($renderer->renderField($attr));
                }
            }
        }
    }
    public function renderConfigurableFields($form, $product = null){
        $return = '';
        foreach($this->superAttributes as $attr){ //var_dump($product->{$attr->attr->code}); die; 
            $return = $return . $form->field(is_object($product)? $product : $this, $attr->attr->code)->dropdownList(ArrayHelper::map($attr->attributeOptionPrices, is_object($product)? 'option.value' : 'id', 'option.value'), [(Yii::$app->id=="app-b2b" || 1==1)? '' : 'prompt' => 'Choose an Option...','disable'=>'disabled', 'data-attrid'=>$attr->attr->id,'class'=>'form-control attribute-select', ]);
        }
        //$return = $return . Html::activeHIddenInput($this, 'id', ['name' => 'Products[id]']);
        return $return;
    }
    public function renderVoucherFields($form){
        $return = '';        
        $return = $return . $form->field($this, 'voucherValue')->textInput(['class' => 'giftvoucher-text'])->label('eGift Card Value');
        $return = $return . $form->field($this, 'recipientName')->textInput(['class' => 'giftvoucher-text']);
        $return = $return . $form->field($this, 'recipientEmail')->textInput(['class' => 'giftvoucher-text','placeholder'=>'Your/Recipient’s Email'])->label('Email eGift Card to');
        $return = $return . $form->field($this, 'recipientMessage')->textArea()->label('Message');
        return $return;
    }
    public function renderBundleFields($form){
        $return = '';
        foreach($this->bundles as $bundle){
            $types = $bundle->inputType;
            $isUserDefinedforDD = Array();
            $isUserDefinedforRadio = Array();
            foreach ($bundle->bundleitems as  $bundleItem) {
                if(StoreProducts::find()->where(['productId' => $bundleItem->productId] and ['storeId' => Yii::$app->params['storeId']])) {
                    $isUserDefinedforDD[$bundleItem->id] =    ['data-isUserDefined' => $bundleItem->isUserDefinedQuantity, 'data-defaultQty'=> $bundleItem->defaultQty];
                    $isUserDefinedforRadio[$bundleItem->id] = ['data-isUserDefined' => $bundleItem->isUserDefinedQuantity, 'data-defaultQty'=> $bundleItem->defaultQty];
                }
            }
            switch ($types) {
                case 'select': 
                    $return .= $form->field($this, 'BundleItems')
                                ->dropDownList(
                                    ArrayHelper::map($bundle->bundleitems, 'id', 'product.name'),
                                    ['name'=>'Products[BundleItems][]', 'prompt'=>'Choose a Product','class'=>'bundle_dropdown','options'=>$isUserDefinedforDD]
                                )->label($bundle->title);
                break;
                case 'checkbox' :                
                    $return .= $form->field($this, 'BundleItems')
                                ->checkboxList(
                                    ArrayHelper::map($bundle->bundleitems, 'id', 'product.name'),
                                    ['itemOptions' => ['class'=>'bundle_checkbox'], 'unselect' => null
                                    ])->label($bundle->title);
                break;
                case 'radio':
                    $return .= $form->field($this, 'BundleItems')
                                ->radioList(
                                    ArrayHelper::map($bundle->bundleitems, 'id', 'product.name'),
                                    ['itemOptions' => ['class'=>'bundle_radio_'.'1','data-params'=>$isUserDefinedforRadio],
                                  'item' => function ($index, $label, $name, $checked, $value) use($isUserDefinedforRadio){ return "<div class='test'><input type='radio' name='Products[BundleItems][]' class='radios bundle_radio $value' value='$value' data-params='".json_encode($isUserDefinedforRadio)."' /><label>$label</label></div>"; }
                                  , 'unselect' => null])->label($bundle->title);
                break;
                case 'multi':
                    $return .= $form->field($this, 'BundleItems')
                                ->dropDownList(
                                    ArrayHelper::map($bundle->bundleitems, 'id', 'product.name'),['name'=>'Products[BundleItems][]','multiple' => true,'class' => 'bundle_multisel']
                                )->label($bundle->title);
                break;
            }
        }
        //$return = $return . Html::activeHIddenInput($this, 'id', ['name' => 'Products[id]']);
        return $return;
    }
    /**
     * Gets the value of the custom attribute
     * TODO: Get AttributeValues via relation
     */
    public function attrValue($attribute){
        if(isset(Yii::$app->params['temp']['attrValues'][$this->id][$attribute])){
            return Yii::$app->params['temp']['attrValues'][$this->id][$attribute];
        }
        if(isset(Yii::$app->params['temp']['attributes'][$attribute])){
            $attr = Yii::$app->params['temp']['attributes'][$attribute];
        }elseif(!$attr = Attributes::findOne(['code'=>$attribute])){
            return false;
        }
        // if($attr = Attributes::findOne(['code'=>$attribute])){
            Yii::$app->params['temp']['attributes'][$attribute] = $attr;
            if(Yii::$app->id != "app-backend" && $override = $this->hasOverride($attr)){  
                return $override;
            }
            else if($value = AttributeValues::findOne(['attributeId' => $attr->id, 'productId' => $this->id, 'storeId' => (!is_null($this->storeId)? $this->storeId : 0)])){
                    Yii::$app->params['temp']['attrValues'][$this->id][$attribute] = (!is_null($this->id) && isset($value->value))? $value->value : "";
                    return (!is_null($this->id) && isset($value->value))? $value->value : "";
            }
            elseif($value = AttributeValues::findOne(['attributeId' => $attr->id, 'productId' => $this->id, 'storeId' => '0'])){
                Yii::$app->params['temp']['attrValues'][$this->id][$attribute] = (!is_null($this->id) && isset($value->value))? $value->value : "";
                return (!is_null($this->id) && isset($value->value))? $value->value : "";
            }
        elseif(($value = AttributeValues::findOne(['attributeId' => $attr->id, 'productId' => $this->id])) && $this->createdBy != '0'){
            if(isset($_REQUEST['db1'])){
                var_dump(AttributeValues::findOne(['attributeId' => $attr->id, 'productId' => $this->id])); die;
            }
            Yii::$app->params['temp']['attrValues'][$this->id][$attribute] = (!is_null($this->id) && isset($value->value))? $value->value : "";
                return (!is_null($this->id) && isset($value->value))? $value->value : "";
        }
        else
                return false;
        // }
        // else
        //     return false;
    }
    public function hasOverride($attribute){ //var_dump($attribute);die();
        if(in_array($attribute->code, ['price'])){  //var_dump($attribute->code);die();
            $function = $attribute->code."Override";
            if(isset(Yii::$app->params['temp']['valueOverride'][$this->id][$attribute->code])){
                return Yii::$app->params['temp']['valueOverride'][$this->id][$attribute->code];
            }else{
                Yii::$app->params['temp']['valueOverride'][$this->id][$attribute->code] = $this->$function($attribute);
            }
            return Yii::$app->params['temp']['valueOverride'][$this->id][$attribute->code];     
        //return $this->$function($attribute);  
        }    
        else
            return false;
    }
    public function priceOverride($attribute,$type=NULL){  
        if(isset(Yii::$app->params['temp']['promotionProducts']))
                $promotionProducts = Yii::$app->params['temp']['promotionProducts'];
        else{ 
                $promotionProducts = ArrayHelper::map($this->getConsumerPromotionProducts()->all(), 'productId', 'promotionProduct');
                Yii::$app->params['temp']['promotionProducts'] = $promotionProducts;
        }
        if(isset(Yii::$app->params['temp']['conferenceProducts']))
                $conferenceProducts = Yii::$app->params['temp']['conferenceProducts'];
        else{
                $conferenceProducts = ArrayHelper::map($this->conferenceTrayProducts, 'productId', 'conferenceProduct');
                Yii::$app->params['temp']['conferenceProduct'] = $conferenceProducts;
        }
        if(Yii::$app->id=="app-b2b" || Yii::$app->id=="app-api" || Yii::$app->id=="app-suppliers") {
            if(isset($type) && $type == "b2c"){  //die();
                if($value = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => $attribute, 'productId' => $this->id, 'storeId' => '0'])->one()){  
                    return ((isset($value->value)) && !is_null($this->id)) ? $value->value : "" ;
                }    
            }
            else {
                if(in_array($this->id, array_keys($conferenceProducts))){
                    return $conferenceProducts[$this->id]->offerCostPrice;
                }
                else if(in_array($this->id, array_keys($promotionProducts))){
                    return $promotionProducts[$this->id]->offerCostPrice;
                }
                else{  //var_dump($promotionProducts);die();
                    if($value = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'cost_price', 'productId' => $this->id, 'storeId' => '0'])->one()){ 
                        //var_dump($value->value);
                        return (!is_null($this->id) && isset($value->value))? $value->value : "";
                    }
                }
            }    
        }
        else{  
            $todays = Helper::localDate(date("Y-m-d H:i:s"));
            if(isset(Yii::$app->session['studentByod'])) {
                $store = Stores::findOne(Yii::$app->params['storeId']);
                $byodProducts = ArrayHelper::map($store->getPortalProducts('studentByod')->all(), 'productId', 'portalProduct');
                if(in_array($this->id, array_keys($byodProducts))){ //var_dump($byodProducts[$this->id]->offerPrice);
                    return $byodProducts[$this->id]->offerPrice;
                }
            }
            elseif (isset(Yii::$app->session['schoolByod'])) {
                $store = Stores::findOne(Yii::$app->params['storeId']);
                $byodProducts = ArrayHelper::map($store->getPortalProducts('schoolByod')->all(), 'productId', 'portalProduct');
                if(in_array($this->id, array_keys($byodProducts))){
                    return $byodProducts[$this->id]->offerPrice;
                }
            }
            elseif(isset(Yii::$app->session['clientPortal'])){
                $store = Stores::findOne(Yii::$app->params['storeId']);
                $clientPortalProducts = ArrayHelper::map($store->getPortalProducts('clientPortal')->all(), 'productId', 'portalProduct');
                if(in_array($this->id, array_keys($clientPortalProducts))){
                    return $clientPortalProducts[$this->id]->offerPrice;
                }
            }
            if(in_array($this->id, array_keys($promotionProducts))){
                if(!\Yii::$app->getUser()->isGuest){
                    $user = User::findOne(['id'=>Yii::$app->user->id]);
                    if($user->groupDiscount != NULL){
                        $discPrice = ((100-$user->groupDiscount)*($promotionProducts[$this->id]->offerSellPrice));
                        return $discPrice/100;
                    }
                }
                return $promotionProducts[$this->id]->offerSellPrice;
            }
            elseif (($value = $this->special_price) != "" && $this->special_from_date != "" && $this->special_to_date != ""){ 
                if (!is_null($value)) {  
                    /*var_dump($todays);
                    var_dump(date("Y-m-d H:i:s", strtotime($this->special_from_date."00:00:00")));
                    var_dump(date("Y-m-d H:i:s", strtotime($this->special_to_date."23:59:59")));die(); */
                    $fromDate = Helper::localDate(date("Y-m-d H:i:s", strtotime($this->special_from_date."00:00:00")));
                    $toDate = Helper::localDate(date("Y-m-d H:i:s", strtotime($this->special_to_date."23:59:59")));
                    if ((strtotime($fromDate) <= strtotime($todays)) &&  (strtotime($toDate) >= strtotime($todays))){ 
                        if(!\Yii::$app->getUser()->isGuest){
                            $user = User::findOne(['id'=>Yii::$app->user->id]);
                            if($user->groupDiscount){
                                $discPrice = ((100-$user->groupDiscount)*$value);
                                return $discPrice/100;
                            }
                        }
                       return $value;
                    }
                }
                else{ 
                    return false;
                }
            }
        return false;
        }    
    }    
    public function getSellingPrice(){ 
        $price =  $this->priceOverride('price' , 'b2c');
        return $price;
    }
    public function getColumns(){ //isset($_GET['ProductsSearch']['brandName']) ? var_dump($_GET['ProductsSearch']['brandName']) : " ";
        $columns = [];
        return array_merge([
            'id',
            [
                'label'=>'Image',
                'value'=>function($model, $key){ 
                    return '<img src="'.Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail"."/".$model->thumbnailImage.'">';
                },
                'format'=>'html'
            ],    
            'name',
            'sku',
            [
                'label' => 'Brand',
                'attribute'=>'brandName',
                'filter' => Html::textInput("ProductsSearch[brandName]",isset($_GET['ProductsSearch']['brandName']) ? $_GET['ProductsSearch']['brandName'] : "",["class"=>"apply-autocomplete form-control",'data-url'=>Url::to(['brands/list'])])
            ],
            [
                'label' => 'Supplier',
                'attribute'=>'supplierName',
            ],
            [
                'attribute' => 'cost_price',
                'value' => function($model, $key, $index, $column){ return Helper::money($model->cost_price); },
                'format' => 'html'
            ],
            [
                'attribute' => 'price',
                'value' => function($model, $key, $index, $column){ return Helper::money($model->price); },
                'format' => 'html'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{copy}{discontinue}',
                'header' => 'Actions',
                'buttons' => [
                'update' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                },
                'copy' => function ($url, $model, $key) {
                    return '<a href="'.Url::to(['products/create', 'copyFrom' => $model->id]).'" title="Duplicate" data-pjax="0"><span class="fa fa-clone"></span></a>';
                },
                'discontinue' => function ($url, $model, $key) {
                    return (Yii::$app->user->identity->roleId == 1)? '<a href="'.$url.'" title="Discontinue" data-pjax="0" data-confirm="Are you sure you want to discontinue this product?",><span class="fa fa-ban"></span></a>' : '';
                },
                /*'delete' => function ($url, $model, $key) {
                    return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.$url.'" title="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?",><span class="fa fa-trash-o"></span></a>' : '';
                },*/
                ]
            ],
        ], $columns);
    }
    public function saveCustomAttributes($attributes, $storeId = 0){
        //$user = User::findOne(Yii::$app->user->id);
        //$userStoreId = $user->store->id;
        foreach($attributes as $code => $val){ //var_dump($code);die();
            if(!$attribute = Attributes::findOne(['code' => $code]))
                die($code);
            if(!$value = AttributeValues::findOne(['attributeId' => $attribute->id, 'productId' => $this->id, 'storeId' => $storeId]))
                $value = new AttributeValues;
            if(is_array($val))
                $val = json_encode(array_unique($val));
            $value->setAttributes([
                'attributeId' => $attribute->id,
                'productId' => $this->id,
                'value' => $val,
                'storeId' => $storeId,
            ]);
            $stat = true;
            if((!is_null($val) && $val != "") || ($val == "" && !$value->isNewRecord))
                $stat = $value->save(false);
        }
        return $stat;
    }
    public function search($params){
        $query = Products::find();
        $query->joinWith('attributeSet');
        $dataProvider->sort->attributes['attributeSet'] = [
            'asc' => ['attributeSet.title' => SORT_ASC],
            'desc' => ['attributeSet.title' => SORT_DESC],
        ];
        if (!($this->load($params) )) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            //... other searched attributes here
        ])
        ->andFilterWhere(['like', 'AttributeSets.title', 'fdsdf']);
        //->andFilterWhere(['like', 'attributeSet.title', $this->country]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
    public function findSuperAttributes(){
        $superAttributes = []; //var_dump($this->attributeSet->attributeGroups); die;
        if(!isset($this->id))
            $attributeSet = AttributeSets::findOne($_REQUEST['Products']['attributeSetId']);
        else
            $attributeSet = $this->attributeSet;
        foreach($attributeSet->attributeGroups as $group)
            foreach($group->productAttributes as $attr)
                if($attr->field=="dropdown" && $attr->configurable=="1" && $attr->scope=="global")
                    $superAttributes[] = $attr;
        return $superAttributes;
    }
    public function getOptionId($attributeId, $value){
        $option = AttributeOptions::find()->where(compact('attributeId', 'value'))->one();
        return ($option)? $option->id : false;
    }
    public function getAssignedProductsQuery(){
        $query = StoreProducts::find()->select('id', 'productId')->where(['storeId' => Yii::$app->params['storeId']]);
        if(Yii::$app->id=="app-b2b")
            return $query;
        else
            return $query->andWhere("type = 'b2c'");
    }
    public function getAttributeSetName(){
        return $this->attributeSet->title;
    }
    public function getBrandName(){
    return $this->brand->title;
    }
    public function getSupplierName(){ 
        if(isset($this->brand->supplier->title))
        return $this->brand->supplier->title;
    else
        return ""; //var_dump($this->id);die();
    }
    public function getSupplierId(){ 
        return $this->brand->supplier->id;
    }
    public function getSuperAttributes(){
        return $this->hasMany(ProductSuperAttributes::className(), ['productId' => 'id'])->orderBy('position');
    }
    public function getDetailViewAttributes(){
        $exclusions = ["'country_of_manufacture'","'meta_title'","'meta_keywords'","'meta_description'","'producttype'","'f_size'","'brandTitle'"];
        $customAttrs = Attributes::find()->where("system = '0' AND field != 'boolean' AND code not in (".implode(",", $exclusions).")")->all();
        //var_dump($customAttrs);die();
        $attrs = [];
        if((isset(Yii::$app->params['storeId']) && Yii::$app->id=="app-b2b") || Yii::$app->id=="app-api"){
            $attrs[] = ['label'=>'SKU', 'value'=>$this->sku];
            $attrs[] = ['label'=>'Vendor', 'value'=>$this->getSupplierName()];
        }    
        foreach($customAttrs as $attr){
            if($this->{$attr->code} != ""){
                if(Helper::isJson($this->{$attr->code})){
                    if(!is_array(json_decode($this->{$attr->code}, true))){
            $decoded = [$this->{$attr->code}];
            }else{
            $decoded = json_decode($this->{$attr->code}, true);
            }
            $attrs[] = ['label' => $attr->title, 'value' => implode(", ", $decoded)];
        }
                else
                    $attrs[] = ['label' => $attr->title, 'value' => $this->{$attr->code}];
            }
        }
        if((isset(Yii::$app->params['storeId']) && Yii::$app->id=="app-b2b") || Yii::$app->id=="app-api" || Yii::$app->id=="app-suppliers"){  //die('b2b');
            $attrs[] = ['label'=>'Recommended Retail', 'value'=>Helper::money($this->sellingPrice), 'format'=>'html'];
            $attrs[] = ['label'=>'Markup', 'value'=>$this->getMarkup()."%"];
        }    
        elseif($this->brand->isVirtual == "0"){
            $attrs[] = ['label'=>'Brand', 'value'=>$this->getBrandName()];
        }
        //var_dump($attrs); die;
        return $attrs;
    } 
    public function getAttributeValuesAsArray(){
        $values = [];
        foreach($this->detailViewAttributes as $attribute){
            $values[is_string($attribute)? $attribute : \backend\components\Helper::slugify($attribute['label'])] = is_string($attribute)? $this->$attribute : strip_tags($attribute['value']);
        }
        return $values;
    }
    public function getSuperAttributesValuesText(){ 
        foreach($this->config as $config => $details){
            if($this->product->typeId=="bundle"){
                echo "<p>".$details['name'].": ".$details['qty']."</p>";
            }elseif($this->product->typeId=="configurable"){
                echo "<p>".(isset($details['attr']['title'])? ($details['attr']['title'].": ".$details['value']) : "")."</p>";
            }
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConferenceTrayProducts()
    {
    $todays = date('Y-m-d H:i:s');
        return $this->hasMany(ConferenceTrayProducts::className(), ['productId' => 'id'])
                ->join('JOIN',
                        'ConferenceTrays as ct',
                        'ct.id = ConferenceTrayProducts.conferenceTrayId'
                    )
                 ->join('JOIN',
                        'Conferences as c',
                        'c.id = ct.conferenceId'
                    )
                    ->where("c.status= 1")
            ->andWhere("c.fromDate < '$todays' AND c.toDate > '$todays'");
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumerPromotionProducts()
    {
        $todays = date("Y-m-d H:i:s");
        $query =  ConsumerPromotionProducts::find()
                ->join('JOIN',
                        'ConsumerPromotions as cp',
                        'cp.id = ConsumerPromotionProducts.promotionId'
                    );
                    if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api" && Yii::$app->id != "app-suppliers"){
            $storeId = Yii::$app->params['storeId'];
                        $query->join('JOIN',
                        'StorePromotions as sp',
                        'sp.promotionId = ConsumerPromotionProducts.promotionId'
                        );
                        $query->where("sp.storeId=".$storeId." and sp.active=1 and startDate < '$todays' and endDate > '$todays'");
                    } 
                    elseif(Yii::$app->id == "app-b2b"){  //die('hai');
                        $query->where("cp.fromDate < '$todays' AND cp.costEndDate > '$todays'");
                    }
                $query->andWhere("cp.status='published'");      
        return $query;
    }
    public function getCataloguePageNumber($promotionId){
        if($promotionProduct = ConsumerPromotionProducts::find()->where(['productId' => $this->id, 'promotionId' => $promotionId])->one()){
            return $promotionProduct->pageId;
        }else{
            return "N/A";
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
   /* public function getCategories()
    {
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId != "3") { 
            return $this->hasMany(ProductCategories::className(), ['productId' => 'id','storeId'=>'0']);
        }
        else {
            //return $this->hasMany(ProductCategories::className(), ['productId' => 'id','storeId'=>'0'])->orWhere(['storeIds'=>$user->store->id]);
            return $this->hasMany(ProductCategories::className(), ['storeId'=>'0','storeIds'=>$user->store->id])->orWhere(['productId' => 'id']);
        }    
    }*/
    public function getCategories()
    {
        return $this->hasMany(ProductCategories::className(), ['productId' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(ProductPrices::className(), ['productId' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(ProductReviews::className(), ['productId' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeSet()
    {
        return $this->hasOne(AttributeSets::className(), ['id' => 'attributeSetId']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreProducts()
    {
        return $this->hasMany(StoreProducts::className(), ['productId' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWishlistItems()
    {
        return $this->hasMany(WishlistItems::className(), ['Products_id' => 'id']);
    }
    public function getBundles() 
    {
        return $this->hasMany(Bundles::className(),['productId' => 'id']);
    }
    public function getInventory(){ //var_dump($this->storeId);die();
        if(Yii::$app->id == "app-frontend") //it's the frontend
            $inventory = ($this->hasOne(ProductInventory::className(), ['productId' => 'id'])->andWhere(['storeId' => Yii::$app->params['storeId']])->exists())? $this->hasOne(ProductInventory::className(), ['productId' => 'id'])->andWhere(['storeId' => $this->storeId]) : $this->hasOne(ProductInventory::className(), ['productId' => 'id'])->andWhere(['storeId' => '0']);
        elseif(Yii::$app->id == "app-backend" && Yii::$app->user->identity->roleId == "3"){
            $inventory = $this->hasOne(ProductInventory::className(), ['productId' => 'id'])->andWhere(['storeId' => Yii::$app->user->identity->storeId]);
        }elseif(Yii::$app->id == "app-backend" && Yii::$app->user->identity->roleId == "1"){
            $inventory = $this->hasOne(ProductInventory::className(), ['productId' => 'id'])->andWhere(['storeId' => isset(Yii::$app->session['storeId'])? Yii::$app->session['storeId'] : '0']);
        }
        return (!$inventory->exists())? new ProductInventory : $inventory->one();
    }
    public function getStoreProduct(){
        return $this->hasOne(StoreProducts::className(), ['productId' => 'id'])->andWhere(['storeId' => Yii::$app->params['storeId']]);
    }
    public function getLinkedProducts(){
        return $this->hasMany(ProductLinkages::className(), ['parent' => 'id']);
    }
    public function getOptionPrices(){
        return $this->hasMany(AttributeOptionPrices::className(), ['productId' => 'id']);
    }
    public function getBrand(){
        return $this->hasOne(Brands::className(), ['id' => 'brandId']);
    }
    public function getParent(){
        return $this->hasOne(Products::className(), ['id' => 'parent'])->where('Products.status = 1')->viaTable('ProductLinkages', ['productId' => 'id']);
    }
    public function getAttributeValues($attributeId = null, $storeId = 0){ //die('sdkfjhskd');
        $query = $this->hasMany(AttributeValues::className(), ['productId' => 'id'])->andWhere("storeId = $storeId");
        if(isset($attributeId))
            $query->andWhere("attributeId = $attributeId");
        return $query;
    }
    public function getProductImages(){
        return $this->hasMany(ProductImages::className(), ['productId' => 'id']);
    }
    public function getThumbnailImage(){
        if($this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isThumbnail= 1 and exclude = 0")->exists()){
            $path = $this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isThumbnail= 1 and exclude = 0")->one()->path;
            /*$dir = substr(strrchr($path, "."), 1);
            $imageName = explode('.',$path);*/
            if($path != "" && file_exists('../..'.Yii::$app->params["productImagePath"]."thumbnail/".static::getImagePathCaseCorrected($path)))
                return static::getImagePathCaseCorrected($path);
            /*else if($dir == "png")
                return $imageName['0'].'.jpg';
            else if($dir == "jpg")
                return $imageName['0'].'.png';*/
            else
                return "/noimage.png";
            }
            else{
                return "/noimage.png";
            }
       /* $value = ProductImages::findOne(['isThumbnail' => 1, 'productId' => $this->id]);
        $path = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail/".$value['path'];
        return $path;*/
    }
    public function getBaseImage(){
        //var_dump(file_exists('../..'.Yii::$app->params["productImagePath"]."base/".$path)); die;
        if($this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isBase= 1 and exclude = 0")->exists()){
            $path = $this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isBase= 1 and exclude = 0")->one()->path;
            /*$dir = substr(strrchr($path, "."), 1);
            $imageName = explode('.',$path);*/
            if($path != "" && file_exists('../..'.Yii::$app->params["productImagePath"]."base/".static::getImagePathCaseCorrected($path)))
                return static::getImagePathCaseCorrected($path);
            /*else if($dir == "png")
                return $imageName['0'].'.jpg';
            else if($dir == "jpg")
                return $imageName['0'].'.png';*/
            else
                return "/noimage.png";
            }
            else{
                return "/noimage.png";
            }
    }
    public function getSmallImage(){
        //return $this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isSmall= 1");
        //var_dump(file_exists('../..'.Yii::$app->params["productImagePath"]."base/".$path)); die;
        if($this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isSmall= 1 and exclude = 0")->exists()){
            $path = $this->hasOne(ProductImages::className(), ['productId' => 'id'])->andWhere("isSmall= 1 and exclude = 0")->one()->path;
           /* $dir = substr(strrchr($path, "."), 1);
            $imageName = explode('.',$path);*/
            //var_dump(file_exists('../..'.Yii::$app->params["productImagePath"]."base/".$path)); die;
            if($path != "" && file_exists('../..'.Yii::$app->params["productImagePath"]."small/".static::getImagePathCaseCorrected($path)))
                return static::getImagePathCaseCorrected($path);
            /*else if($dir == "png")
                return $imageName['0'].'.jpg';
            else if($dir == "jpg")
                return $imageName['0'].'.png';*/
            else
                return "/noimage.png";
            }
            else{
                return "/noimage.png";
            }
    }
    public function getAbsoluteThumbImageUrl(){  
        /*$thumbImage = ProductImages::findOne(['isThumbnail' => 1, 'productId' => $this->id]);
        var_dump($thumbImage['path']);*/
       if($thumbImage = ProductImages::findOne(['isThumbnail' => 1, 'productId' => $this->id]))
        {
             return "hai"; 
           // return '<img alt="Thumbnail" src='.Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]' height="20" width="20" >';
        }
        else
        {
            return '1';
        }
    }
     public function getAbsoluteBaseImageUrl(){ 
        $baseImageUrl = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/base";
        return $baseImageUrl;
    }
     public function getAbsoluteSmallImageUrl(){
        $smallImageUrl = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/small";
        return $smallImageUrl;
    }
    public function isInConference(){
        if(isset(Yii::$app->params['temp']['conferenceTrayProducts'][$this->id])){
            $conferenceProducts = Yii::$app->params['temp']['conferenceTrayProducts'][$this->id];
        }else{
            $conferenceProducts = ArrayHelper::map($this->conferenceTrayProducts, 'productId', 'conferenceProduct');
            Yii::$app->params['temp']['conferenceTrayProducts'][$this->id] = $conferenceProducts;
        }
        if(in_array($this->id, array_keys($conferenceProducts))){
            return $conferenceProducts[$this->id]->conferenceTray->conference->id;
        }
        else
            return false;
    }
    public function isInPromotion(){
        $promotionProducts = ArrayHelper::map($this->getConsumerPromotionProducts()->all(), 'productId', 'promotionProduct');
        if(in_array($this->id, array_keys($promotionProducts)))
            return true;
        else
            return false;   
    }
    public function hasSpecialPrice(){
        if($this->special_price != "" && $this->special_price < $this->originalPrice){ //var_dump($this->originalPrice);die();
            $todays = date("d-m-Y");
            $fromDate = $this->special_from_date;
            $toDate = $this->special_to_date;
            if ((strtotime($fromDate) <= strtotime($todays)) &&  (strtotime($toDate) >= strtotime($todays))){ 
                return true;
            }
        }
        else{ 
            return false;
        }
    }
    public function getOriginalPrice()
    {
        /*
        if($value = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'price', 'productId' => $this->id, 'storeId' => 0])->one()){
                    return ((isset($value->value)) && !is_null($this->id)) ? $value->value : "" ;
        } */
        if($value = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'price', 'productId' => $this->id, 'storeId' => Yii::$app->params['storeId']])->one()){
            return ((isset($value->value)) && !is_null($this->id)) ? $value->value : "" ;
        }
        elseif ($value = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'price', 'productId' => $this->id, 'storeId' => 0])->one()) {
            return ((isset($value->value)) && !is_null($this->id)) ? $value->value : "" ;
        }
    }
    public function getMasterPrice()
    {
        if($value = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'price', 'productId' => $this->id, 'storeId' => 0])->one()){
                    return ((isset($value->value)) && !is_null($this->id)) ? $value->value : "" ;
        }
    }
    public function getMarkup(){ //var_dump(round(((($this->sellingPrice/1.1)-$this->price)/$this->price)*100)); die;
        return round(((($this->sellingPrice/1.1)-$this->cost_price)/(($this->cost_price==0)? 1 : $this->cost_price))*100);
    }
    public function getConferenceTrayProduct($conferenceId){
    $todays = date('Y-m-d H:i:s');
        return ConferenceTrayProducts::find()->join('JOIN', 'ConferenceTrays as ct', 'ct.id = ConferenceTrayProducts.conferenceTrayId')->join('JOIN', 'Conferences as c', 'c.id = ct.conferenceId')->where(['productId' => $this->id, 'ct.conferenceId' => $conferenceId, 'c.status'=>'1'])->andWhere("c.fromDate < '$todays' AND c.toDate > '$todays'")->one();
    }
    public static function getImagePathCaseCorrected($path){
        $parts = explode("/", $path);
        //unset($parts[count($parts)+1]);
        foreach($parts as $i => &$part)
            if($i != (count($parts)-1))
                $part = strtoupper($part);
        return implode("/", $parts);
    }
    public function getSelf(){
        return $this;
    }
    public function isCustomProduct(){
        if($product = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'made_to_order', 'productId' => $this->id, 'value'=>1])->andWhere("storeId = ".Yii::$app->params['storeId']." or storeId=0")->one()){
            return true;
        } 
        else
            return false;
    }
    public function isInByod($sessionVar){ 
        $store = Stores::findOne(Yii::$app->params['storeId']);
        $byodProducts = ArrayHelper::map($store->getPortalProducts($sessionVar)->all(), 'productId', 'portalProduct');
        if(in_array($this->id, array_keys($byodProducts))){
            return true;
        }
        else
            return false;
    }
    public function isPortalProduct(){
        if(isset(Yii::$app->session['studentByod']) || isset(Yii::$app->session['schoolByod']) || isset(Yii::$app->session['clientPortal'])) {
            if(isset(Yii::$app->session['studentByod']))
                $sessionVar = 'studentByod';
            elseif(isset(Yii::$app->session['schoolByod']))
                $sessionVar = 'schoolByod';
            elseif(isset(Yii::$app->session['clientPortal']))
                $sessionVar = 'clientPortal';
            $store = Stores::findOne(Yii::$app->params['storeId']);
            $portalProducts = ArrayHelper::map($store->getPortalProducts($sessionVar)->all(), 'productId', 'portalProduct');
            if(in_array($this->id, array_keys($portalProducts))){
                return true;
            }
            else
                return false;
        }
        else 
            return false;    
    }     
    public function isByodOnly(){ 
        if($this->byod_only == "Yes")
            return true;
        else
            return false;
    }
    public function assignBrandStores(){
    $storeBrands = $this->brand->getStoreBrands()->andWhere('enabled = 1')->all();
        foreach($storeBrands as $storeBrand){ //get this brand's stores
            if($storeProduct = \common\models\StoreProducts::find()->where(['storeId' => $storeBrand->storeId, 'productId' => $this->id, 'type' => 'b2c'])->one())
                continue;
            $storeProduct = new \common\models\StoreProducts;
            $storeProduct->storeId = $storeBrand->storeId;
            $storeProduct->productId = $this->id;
            $storeProduct->type = "b2c";
            $storeProduct->enabled = "1";
            $storeProduct->save(false);
        }
    }
    public function getRating(){
       /* if(Yii::$app->user->identity->roleId != "1"){
            $total = ProductReviews::find()->where(['productId'=>$this->id,'storeId'=>Yii::$app->params['storeId']])->sum('stars');
            $count = ProductReviews::find()->where(['productId'=>$this->id,'storeId'=>Yii::$app->params['storeId']])->count();
            $avg = $total/$count;
            return $avg;
        } */
        $query = ProductReviews::find()->where(['productId'=>$this->id]); 
        if (!\Yii::$app->user->isGuest) {
            if(Yii::$app->user->identity->roleId != "1"){
                $total = $query->andWhere(['storeId'=>Yii::$app->params['storeId']])->sum('stars');
                $count = $query->andWhere(['storeId'=>Yii::$app->params['storeId']])->count();
            }
            elseif(Yii::$app->user->identity->roleId == "1"){
                $total = $query->sum('stars');
                $count = $query->count();
            }
        }    
        else{
            $total = $query->andWhere(['storeId'=>Yii::$app->params['storeId']])->sum('stars');
            $count = $query->andWhere(['storeId'=>Yii::$app->params['storeId']])->count();
        }
        if($count > 0)
            $avg = $total/$count;
        else
            $avg = 0;
        return $avg;
    }
    public function getParentCategory(){
        $parent = ProductCategories::find()->join('JOIN','Categories as c','c.id = ProductCategories.categoryId')->where(['ProductCategories.productId'=>$this->id,'c.parent'=>0])->one();
        return $parent->categoryId;
    }
    public function getMeta() 
    {
        return $this->hasMany(ProductMetaDetails::className(),['productId' => 'id']);
    }
    public function getRelatedProducts() 
    {
        return $this->hasMany(RelatedProducts::className(),['relatedProductId' => 'id'])->andOnCondition(['storeId' => Yii::$app->params['storeId']]);
    }
    public function addActivity($type, $storeId = 0){
        $activity = new \common\models\ProductActivity;
        $activity->initiatorId = isset(Yii::$app->user->identity->id)? Yii::$app->user->identity->id : NULL;
        $activity->type = $type;
        $activity->storeId = $storeId;
        $activity->productId = $this->id;
        return $activity->save();
    }
    public function discontinue(){
        $storeProducts = StoreProducts::find()->where(['productId' => $this->id, 'type' => 'b2c'])->all();
        foreach($storeProducts as $storeProduct){
            $storeProduct->enabled = "0";
            $storeProduct->save(false);
        }
        $this->addActivity('discontinued');
        return true;
    }
    public function getStorePrice($storeId){
        return ($price = AttributeValues::find()->where(['storeId' => $storeId, 'attributeId' => '10', 'productId' => $this->id])->one())? $price->value : '';
    }

    public function extendDisableDate($storeId,$productId,$fromDate="",$extendDays=30){
        $fromDate = isset($fromDate)? $fromDate : date('Y-m-d');

        $StoreProducts = StoreProducts::find()->where(['storeId' => $storeId, 'productId' => $productId, 'enabled' => 1, 'type' => 'b2c'])->all();
       
        $saveCnt = 0;
        foreach ($StoreProducts as $key => $StoreProduct) {
            if(strtotime($StoreProduct->disableDate) < strtotime(date('Y-m-d', strtotime($fromDate."+".$extendDays." days")))) {
                $StoreProduct->disableDate = date('Y-m-d H:i:s', strtotime($fromDate."+".$extendDays." days"));
                $dateExtended = $StoreProduct->save(false);
                if($dateExtended) $saveCnt++;
            }
        }
        return ($saveCnt > 0)?true:false;
    }

}