<?php

namespace common\models;
use frontend\components\Helper;
use yii\helpers\ArrayHelper;
use Yii;
use common\models\Shipments;
/**
 * This is the model class for table "Shipments".
 *
 * @property integer $id
 * @property integer $orderId
 * @property string $comments
 * @property string $carrier
 * @property string $trackingNumber
 * @property string $createdDate
 */

class Shipments extends \yii\db\ActiveRecord
{
    public $reportFrom = null;
    public $reportTo = null;
    public $reportStoreId = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Shipments';
    }
    public $shippedItems;
    public $chkmail;
    public $carrierId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId','carrier','trackingNumber' ,'title'], 'required'],
            [['orderId'], 'integer'],
            [['comments', 'trackingNumber','title'], 'string'],
            [['createdDate','notify','chkmail'], 'safe'],
            [['carrier'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Shipment #',
            'orderId' => 'Order #',
            'title' => 'Title',
            'comments' => 'Comments',
            'shipmentPlacedDate' => 'Date Shipped',
            'orderShipToAddress' => 'Ship To Address',
            'orderItemCount' => 'Total Qty',
            'carrier' => 'Carrier',
            'title' => 'Reference Number',
            'trackingNumber' => 'Tracking Link',
            'createdDate' => 'Created Date',
        ];
    }
    public function sendSuppliersShipmentMail()
    {
        $shipment = Shipments::findOne($this->id);
        $template = \common\models\EmailTemplates::find()->where(['code' => 'supplierordershipment'])->one();
        $user = \common\models\User::find()->where(['id' => $shipment->order->customerId])->one();
        $store = \common\models\stores::find()->where(['id' => $shipment->order->storeId])->one();
        $order = \common\models\Orders::find()->where(['id' => $shipment->orderId])->one();
        //var_dump($template);die;
        $queue = new \common\models\EmailQueue;
        $queue->models = ['user'=>$user,'store'=>$store,'order'=>$order,'shipment'=>$shipment]; 
        $queue->replacements =[ 'Logo'=>$store->Logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
            'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,
            'emailLinkedinImage'=>$store->emailLinkedinImage];

        $queue->from = "no-reply";
        $queue->recipient = $user;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }
    public function sendUserShipmentMail()
    {
        if($this->order->fromRegisteredUser()==true)
        {
            $user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        }
        else
        {
            $userfullname=ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }
        $shipment = $this;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'userordershipment'])->one();
        //$user = \common\models\User::find()->where(['id' => $shipment->order->customerId])->one();
        $store = \common\models\Stores::find()->where(['id' => $shipment->order->storeId])->one();
        $order = \common\models\Orders::find()->where(['id' => $shipment->orderId])->one();
        //var_dump($shipment);die;
        $queue = new \common\models\EmailQueue;
        $queue->models = ['store'=>$store,'order'=>$order,'shipment'=>$shipment]; 
        $queue->replacements =[ 'logo'=>$store->logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
            'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,
            'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'carrier'=>$shipment->carriers->title];  

        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }
    public function getShipmentId()
    {
        return str_pad($this->id,5,"0",STR_PAD_LEFT);
    }
    public function getShipmentPlacedDate()
    {
        return Helper::date($this->createdDate, "j F, Y");
    }
    public function getShortGrid(){
        return \yii\grid\GridView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getShipmentItemsGrid(), 'sort' => false,'pagination'=> false,]),
            //['class' => 'yii\grid\SerialColumn'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                // /'id',
                [
                    'label'=> 'Product Name',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return $model->orderItem->product->name; 
                    }
                ],
                [
                    'label'=> 'SKU',
                    'attribute' => 'orderItem.sku',
                  
                ],
                // [
                //    'label' => 'Item Price',
                //     'format' => 'html',
                //     'value' => function ($model) {
                //         return Helper::money($model->orderItem->price / $model->orderItem->quantity );
                //     },
                // ],
                'quantityShipped',
                // [
                //    'label' => 'Item Total',
                //     'format' => 'html',
                //     'value' => function ($model) {
                //         return Helper::money(($model->orderItem->price / $model->orderItem->quantity) * $model->quantityShipped);
                //     },
                // ],
            ],
        ]);
    }
    public function getShipmentItemsGrid()
    {
        $query = new \yii\db\ActiveQuery('common\models\ShipmentItems');
        return $query->where("shipmentId=".$this->id."")->limit(10);
    }
    
    public function getShipmentItemsUrl()
    {
        $link=$this->order->storeUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/shipments/view', 'id'=>$this->id]).'">';
    }
    public function getAccountUrl()
    {
        $link=$this->order->storeUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/account']).'">';
    }
    public function getContactusUrl()
    {
        $link=$this->order->storeUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/contactus']).'">';
       
    }
    public function getHelpUrl()
    {
        $link=$this->order->storeUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/help']).'">';
       
    }
    public function getOrderDate()
    {
        return Helper::date($this->order->orderDate, "F jS Y h:i:s A");
        //return $this->order->orderDate;
    }
    public function getOrderItemCount()
    {
        return ShipmentItems::find()->where(['shipmentId' => $this->id])->count();
    }
    public function getOrderShipToAddress()
    {
        return $this->order->shippingAddressText;
    }
    public function getTrackingLink()
    {
        return '<a href='.$this->trackingNumber.'>'.$this->trackingNumber.'</a>';
    }

    public function getOrderCount(){
        $query = \common\models\Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->count(); 
    }
    public function getShipmentCount(){
        $query = \common\models\Shipments::find()
        ->join('LEFT JOIN', 'Orders o', 'Shipments.orderId = o.id')
        ->where('createdDate >= "'.$this->reportFrom.'" AND createdDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("o.storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getTotalSalesShipping(){
        $query = Shipments::find()
        ->join('LEFT JOIN', 'Orders o', 'Shipments.orderId = o.id')
        ->where('createdDate >= "'.$this->reportFrom.'" AND createdDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("o.storeId = {$this->reportStoreId}");
        $total_shipments= $query->all(); 
        $total_price=0;
        foreach ($total_shipments as $key => $shipment) {
            if(isset($shipment->order->grandTotal))
            {
                $a=$shipment->order->grandTotal;
                $total_price=$total_price+$a;
            }
        }
        return $total_price;
       
    }
    /* public function getTotalSalesShipping(){
        return Orders::find()
        ->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'"')->sum('grandTotal');
    } */

    public function getTotalCount(){
        $query = Shipments::find()
        ->join('LEFT JOIN', 'Orders o', 'Shipments.orderId = o.id')
        ->where('createdDate >= "'.$this->reportFrom.'" AND createdDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("o.storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getPeriodCarriers(){
        $query = Shipments::find()
        ->join('LEFT JOIN', 'Orders o', 'Shipments.orderId = o.id')
        ->where('createdDate >= "'.$this->reportFrom.'" AND createdDate <= "'.$this->reportTo.'"');
        if($this->reportStoreId != 0)
            $query->andWhere("o.storeId = {$this->reportStoreId}");
        $shipments = ArrayHelper::map($query->all(), 'carrier', 'carrier'); 
        //$shipments = ArrayHelper::map(Shipments::find()->where('createdDate >= "'.$this->reportFrom.'" AND createdDate <= "'.$this->reportTo.'"')->all(), 'carriers', 'carrier'); 
        return !empty($shipments)? implode(', ', $shipments) : null;
    }

    public function getCarriers()
    {        
        return $this->hasOne(Carriers::className(), ['id' => 'carrier']);        
    }
}
