<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "carriers".
 *
 * @property integer $id
 * @property string $title
 * @property string $trackingUrl
 * @property string $createdDate
 */
class Carriers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Carriers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'trackingUrl'], 'required'],
            [['trackingUrl'], 'string'],
            [['createdDate'], 'safe'],
            [['trackingUrl'],'url'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'trackingUrl' => 'Tracking URL',
            'createdDate' => 'Created Date',
        ];
    }
}
