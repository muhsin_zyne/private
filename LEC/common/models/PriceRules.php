<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "PriceRules".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $code
 * @property integer $couponUsage
 * @property integer $maxCouponUsage
 * @property string $fromDate
 * @property string $toDate
 * @property string $type
 * @property string $discount
 * @property string $conditions
 * @property integer $status
 */
class PriceRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PriceRules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code', 'type', 'discount', 'conditions', 'fromDate', 'toDate'], 'required'],
            [['description', 'type', 'conditions', 'status'], 'string'],
            [['couponUsage', 'maxCouponUsage', 'maxCouponUsagePerUser', 'createdBy'], 'integer'],
            [['fromDate', 'toDate'], 'safe'],
            [['discount'], 'number'],
            [['code'], 'unique', 'when' => function($model){
                if(Yii::$app->user->identity->roleId == "3"){
                    $query = StorePriceRules::find()->join('LEFT JOIN', 'PriceRules pr', 'StorePriceRules.ruleId = pr.id')->where(['storeId' => Yii::$app->user->identity->store->id, 'pr.code' => $model->code]);
                    if(!$model->isNewRecord)
                        $query->andWhere("pr.id != {$model->id}");
                    if($query->exists()){
                        return true;
                    }
                }elseif(Yii::$app->user->identity->roleId == "1"){
                    if(!isset($_POST['PriceRules']['stores']))
                        return true;
                    $query = StorePriceRules::find()->join('LEFT JOIN', 'PriceRules pr', 'StorePriceRules.ruleId = pr.id')->where(['storeId' => $_POST['PriceRules']['stores'], 'pr.code' => $model->code]);
                    if(!$model->isNewRecord)
                        $query->andWhere("pr.id != {$model->id}");
                    if($query->exists()){
                        return true;
                    }
                }
                return false;
            }, 'message' => 'This promo code already exists in an active/expired promo.'],
            [['title', 'code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'code' => 'Code',
            'couponUsage' => 'Coupon Usage',
            'maxCouponUsage' => 'Max Coupon Usage',
            'fromDate' => 'Active From',
            'toDate' => 'Expires On',
            'type' => 'Type',
            'discount' => 'Discount',
            'conditions' => 'Conditions',
            'status' => 'Status',
        ];
    }

    public function getStores(){
        return $this->hasMany(Stores::className(), ['id' => 'storeId'])->viaTable('StorePriceRules', ['ruleId' => 'id']);
    }

    public function isValidWith($entity){
        $conditions = unserialize($this->conditions);
        $expression = "1 != 1";
        if($entity instanceof \frontend\components\InteCart){  //var_dump(get_class($entity));die;
            
            $expressions = [];
            $positions = $entity->positions;
            //var_dump($conditions); die;
            if($conditions){
                foreach($positions as $position){
                    $expressions[] = $this->processConditions($conditions, $position, '||');
                }
                $expression = implode(" || ", $expressions);
            }else
                $expression = "1 == 1";
        }
        elseif($entity instanceof \common\models\ProductCartPosition){  
            $position = $entity;
            $expression = $this->processConditions($conditions, $position, '||');     
        }
        //var_dump($this->maxCouponUsage > $this->couponUsage); die;
        $maxCouponUsage = isset($this->maxCouponUsage)? $this->maxCouponUsage : 1;
        $couponUsage = isset($this->maxCouponUsage)? $this->couponUsage : 0;
        //var_dump(ArrayHelper::map($this->stores, 'id', 'id')); die;
        $userLimitCheck = true;
        if(!Yii::$app->user->isGuest && !is_null($this->maxCouponUsagePerUser)){
            $customerUsage = Yii::$app->user->identity->getCouponUsage($this);
            if($this->maxCouponUsagePerUser < $customerUsage)
                $userLimitCheck = false;
        }   //var_dump(ArrayHelper::map($this->stores, 'id', 'id')); die;
	
        return (eval("return $expression;") && $maxCouponUsage > $couponUsage && $userLimitCheck && in_array(Yii::$app->params['storeId'], ArrayHelper::map($this->stores, 'id', 'id')));
    }

    public function processConditions($conditions, $position, $aggregator){
        $expression = "(";
        if(is_array($conditions)){
            foreach($conditions as $condition){
                if($condition['type'] == "combination"){
	            if(!isset($condition['conditions'])){
                        $expression = "(1==1";
                        break;
                    }
                    $expression = $expression . $this->processConditions($condition['conditions'], $position, $condition['aggregator']);
                }else{
                    $i = 0;
                    $attribute = explode(".", $condition['attribute']);
                    //var_dump($attribute);die();
                    if($attribute[1] == "category"){
                        $catIds = explode(",", $condition['value']);
				//var_dump(ArrayHelper::map($position->product->categories, 'categoryId', 'categoryId')); die('ksjdfhksdf');
                        if(array_intersect($catIds, ArrayHelper::map($position->product->categories, 'categoryId', 'categoryId'))){
                            $expression = $expression ." 1==1";
			            }
                        else
                            $expression = $expression ." 1==0";
                    }
                    elseif ($attribute[1] == "brandid") {
                        $brandIds = explode(",", $condition['value']);
                        //var_dump($brandIds);die();
                        if(in_array($position->product->brandId,$brandIds))
                            $expression = $expression ." 1==1";
                        else
                            $expression = $expression ." 1==0"; 
                    }
                    else{
                        if($attribute[0] == "product")
                            $value = $position->product->$attribute[1];
                        elseif($attribute[0] == "position")
                            $value = $position->$attribute[1];
                        elseif($attribute[0] == "cart")
                            $value = Yii::$app->cart->$attribute[1];
                        $expression = $expression . "'".$value."' ".$condition['operator']." '".$condition['value']."' ".$aggregator." ";
                    }
                    $i++;
                }
            }
        }else{
            $expression = "(1==1";
        }
        return str_replace(["|| )", "&& )"], " )", $expression).")";
    }
}
