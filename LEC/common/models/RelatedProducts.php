<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "RelatedProducts".
 *
 * @property integer $id
 * @property integer $productId
 * @property integer $relatedProductId
 * @property integer $storeId
 */
class RelatedProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RelatedProducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'relatedProductId', 'storeId'], 'required'],
            [['productId', 'relatedProductId', 'storeId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Product ID',
            'relatedProductId' => 'Related Product ID',
            'storeId' => 'Store ID',
        ];
    }
    
    public function getProduct() {
        return $this->hasOne(Products::className(), ['id' => 'relatedProductId']);
    }
    
    public function getRelated($productId,$limit = 4) {
        
        if (!empty(Yii::$app->params['relatedProductsCount']))
            $limit = Yii::$app->params['relatedProductsCount'];

       
        $relatedProducts= Products::find()
                            ->joinWith('relatedProducts')
                            ->andwhere(['RelatedProducts.productId'=>$productId])
                            ->groupBy('Products.id')
                            ->limit($limit);

        $count = $relatedProducts->count();
        $id = [];
        
        foreach ($relatedProducts->all() as $relatedProduct) {
            $id[] = $relatedProduct->id;
        }
        
        
        $categories_array= [];
        
         $categories= Products::find()
                            ->joinWith('categories')
                            ->andwhere(['Products.id'=>$productId])->all();

        foreach($categories as $category) {
            $category_value= $category->categories;
            foreach($category_value as $value) {
                $categories_array[]=$value->categoryId;
                if($parent_category = Categories::findOne($value->categoryId)){
                    $parent_categories= $parent_category->parentCategories;
                    foreach($parent_categories as $par_cat_id)
                        $categories_array[]= $par_cat_id->id;
                //$categories_array[]= $parent_category->parent;
	        }
            }       
        }
        
      
        
         $category_products = Products::find()
                ->joinWith('categories')
                ->andwhere(['in','ProductCategories.categoryId',  array_unique($categories_array)])
                ->andwhere(['not in', 'Products.id', $id])
                ->groupBy('Products.id');
         
        
         //if(empty($id)) {
             $category_products->andWhere(['<>','Products.id', $productId]);
         //}
         
          $category_products->limit($limit - $count);
          
          $count1=$category_products->count();
         
          foreach ($category_products->all() as $category_product_id) {
             $id[]=$category_product_id->id;
        }
        
        
         $normal_products = Products::find()
                ->andwhere(['not in','Products.id',$id])
                ->andWhere(['<>','Products.id', $productId])
                ->groupBy('Products.id')
                ->limit($limit-$count-$count1);
        
        foreach ($normal_products->all() as $normal_product_id) {
             $id[]=$normal_product_id->id;
        }
        
        
        

        $related_products= Products::find()->andwhere(['IN', 'id', $id]);
        
        return $related_products;
       
        
    }
    


}
