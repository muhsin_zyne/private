<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "storebanners".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $bannerId
 */
class StoreBanners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StoreBanners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'bannerId'], 'required'],
            [['storeId', 'bannerId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'bannerId' => 'Banner ID',
        ];
    }
}
