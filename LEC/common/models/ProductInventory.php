<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductInventory".
 *
 * @property integer $id
 * @property integer $productId
 * @property integer $storeId
 * @property integer $quantity
 * @property integer $min_quantity
 * @property integer $manageStock
 * @property integer $isInStock
 */
class ProductInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductInventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', /*'quantity', 'minQuantity'*/], 'required'],
            [['productId', 'storeId', 'quantity', 'minQuantity', 'manageStock', 'isInStock'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Product ID',
            'storeId' => 'Store ID',
            'quantity' => 'Quantity',
            'minQuantity' => 'Minimum Quantity allowed in Shopping Cart',
            'manageStock' => 'Manage Stock',
            'isInStock' => 'Stock Availability',
        ];
    }
}
