<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "Attributes".
 *
 * @property integer $id
 * @property string $title
 * @property integer $applyToType
 * @property integer $required
 * @property integer $attributeGroupId
 * @property integer $configurable
 *
 * @property AttributeOptions[] $attributeOptions
 * @property AttributeValues[] $attributeValues
 * @property Attributegroups $attributeGroup
 * @property ConfigurableAttributes[] $configurableAttributes
 */
class Attributes extends \yii\db\ActiveRecord
{

    public $attr;
    public $productId;
    public $productTypeApplicable;
    public $productTypeApplicableUpdate;
    public $valueAttrOption;
    public $attributeGroupId;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applyToSimple', 'applyToBundle', 'applyToGrouped', 'applyToConfigurable', 'applyToVirtual', 'applyToDownloadable', 'required', 'configurable', 'applyToSimple', 'applyToGrouped', 'applyToConfigurable', 'applyToVirtual', 'applyToBundle', 'applyToDownloadable'], 'integer'],
            [['title','code'], 'required'],
            [['code'], 'unique'],
            [['title', 'field'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Attribute Title',
            'code' => 'Attribute Code',
            'field' => 'Catalog Input Type for Store Owner',
            
            'required' => 'Values Required',
            //'attributeGroupId' => 'Attribute Group ID',
            'attributeGroupId' => 'Apply To',
            'configurable' => 'Configurable',
        ];
    }


    public function getValue(){
        return $this->hasOne(AttributeValues::className(), ['attributeId' => 'id'])->where('productId', $this->productId);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(AttributeOptions::className(), ['attributeId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValues()
    {
        return $this->hasMany(AttributeValues::className(), ['attributeId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroup()
    {
        return $this->hasOne(AttributeGroups::className(), ['id' => 'attributeGroupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigurableAttributes()
    {
        return $this->hasMany(ConfigurableAttributes::className(), ['attributeId' => 'id']);
    }

    public function getAttrCode(){
        return "product." . $this->code;
    }

    public function getSelf(){
        return $this;
    }

    public static function getFilterAttributes($query){

        $exclusions = ['"country_of_manufacture"'];
        $raw = $query->select('Products.id')->limit(100)->createCommand()->rawsql;
        //var_dump($raw); die;
        $connection = Yii::$app->getDb();
        //var_dump("SELECT a.code, a.title, a.id FROM AttributeValues 
        //        INNER JOIN Attributes a ON a.id = AttributeValues.attributeId where productId in ($raw) AND a.system = 0 AND a.field = 'dropdown' AND AttributeValues.value != '' AND a.code not in (".implode(",", $exclusions).") Group By attributeId "); die;
        //$command = $connection->createCommand("SELECT a.code, a.title, a.id FROM AttributeValues 
        //        INNER JOIN Attributes a ON a.id = AttributeValues.attributeId where productId in ($raw) AND a.system = 0 AND a.field = 'dropdown' AND AttributeValues.value != '' AND a.code not in (".implode(",", $exclusions).") Group By attributeId ");
        $command = $connection->createCommand("SELECT a.code, a.title, a.id FROM AttributeValues 
                INNER JOIN Attributes a ON a.id = AttributeValues.attributeId where a.system = 0 AND a.field = 'dropdown' AND AttributeValues.value != '' AND a.code not in (".implode(",", $exclusions).") Group By attributeId ");
        $attributes = $command->queryAll();
        foreach ($attributes as $key=>$attr) {
                $filterAttributes[$key] = new \stdClass;
                $filterAttributes[$key]->code = $attr['code'];
                $filterAttributes[$key]->id = $attr['id'];
                $filterAttributes[$key]->title = $attr['title'];
        } 
        //var_dump($filterAttributes);die();
        return $filterAttributes;
    }

    public static function getFilterAttributeValues($query, $code){
        $raw = $query->select('Products.id')->createCommand()->rawsql;
        $connection = Yii::$app->getDb();
        //$command = $connection->createCommand("SELECT AttributeValues.value FROM AttributeValues JOIN Attributes a ON a.id = AttributeValues.attributeId where productId in ($raw) AND a.system = 0 AND a.field = 'dropdown' AND a.code = '$code' AND AttributeValues.value != '' Group By AttributeValues.id");
        $command = $connection->createCommand("SELECT value from AttributeValues av join Attributes a on a.id = av.attributeId where code = '$code'");
        $attributeValues = $command->queryAll();
        return $attributeValues;
    }
}