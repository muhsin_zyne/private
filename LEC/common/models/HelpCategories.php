<?php

namespace common\models;
use Yii;
use common\models\Stores;
use common\models\Categories;
use yii\helpers\Url;

class HelpCategories extends \yii\db\ActiveRecord
{

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'HelpCategories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['active', 'position','title'], 'safe'],
            [['position'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
    */


}
?>