<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "EmailTemplates".
 *
 * @property integer $id
 * @property string $displayName
 * @property string $description
 * @property string $module
 * @property string $code
 * @property string $subject
 * @property string $html
 * @property string $templatePath
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $layout = "html";
    
    public static function tableName()
    {
        return 'EmailTemplates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['displayName', 'description', 'module', 'code', 'subject', 'html', 'templatePath'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'displayName' => 'Display Name',
            'description' => 'Description',
            'module' => 'Module',
            'code' => 'Code',
            'subject' => 'Subject',
            'html' => 'Html',
            'templatePath' => 'Template Path',
        ];
    }

    public function getHtmlBody(){
        return !is_null($this->templatePath)? $this->renderTemplate() : $this->html;
    }

    public function renderTemplate(){
        $message = Yii::$app->controller->renderPartial("@common/mail/{$this->templatePath}");
        $body = Yii::$app->controller->renderPartial("@common/mail/layouts/{$this->layout}", ['content' => $message]);
        return $body;
    }
}
