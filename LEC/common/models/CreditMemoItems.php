<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CreditMemoItems".
 *
 * @property integer $id
 * @property integer $memoId
 * @property integer $orderItemId
 * @property integer $quantityRefunded
 */
class CreditMemoItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CreditMemoItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['memoId', 'orderItemId', 'quantityRefunded'], 'required'],
            [['memoId', 'orderItemId', 'quantityRefunded'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'memoId' => 'Memo ID',
            'orderItemId' => 'Order Item ID',
            'quantityRefunded' => 'Quantity Refunded',
        ];
    }
    public function getOrderItem()
    {
        return $this->hasOne(OrderItems::className(), ['id' => 'orderItemId']);
    }
}
