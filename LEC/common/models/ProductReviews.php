<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductReviews".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $stars
 * @property integer $productId
 */
class ProductReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductReviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['productId'], 'required'],
            [['productId'], 'integer'],
            [['title', 'stars'], 'string', 'max' => 45],
            [['stars'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'stars' => 'Stars',
            'productId' => 'Product ID',
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
