<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "CouponUsage".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class CouponUsage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CouponUsage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ruleId', 'userId', 'orderId',], 'integer'],
        ];
    }

}