<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Stores".
 *
 * @property integer $id
 * @property string $title
 * @property string $status
 * @property integer $adminId
 */

class Stores extends \yii\db\ActiveRecord

{

    /**
     * @inheritdoc
     */

    public static function tableName()

    {
        return 'Stores';

    }



    /**
     * @inheritdoc
     */

    public function rules()

    {
        // return [
        //     [['title','email'], 'required'],
        //     [['adminId'], 'integer'],
        //     [['title', 'status'], 'string', 'max' => 45]
        // ];
        return [
            [['title','email','abn','billingAddress','shippingAddress','status','adminId','phone','siteUrl','isVirtual'], 'required'],
            [['adminId','status','isVirtual'], 'integer'],
            [['email'], 'email'],
            [['siteUrl'], 'url'],
            [['logoPath'], 'safe'],
            //[['abn'],'string','min'=>11,'max'=>11],
            [['title'], 'string', 'max' => 45]
        ];
            
          

    }

     /**
     * @inheritdoc
     */

    public function attributeLabels()

    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'adminId' => 'Admin ID',
            'abn'=>'ABN',
            'siteUrl'=>'Website URL',
        ];

    }

    public function setBrand($brandId){
        if(!StoreBrands::find()->where(['storeId' => $this->id, 'brandId' => $brandId])->one()){
            $storeBrand = new StoreBrands;
            $storeBrand->storeId = $this->id;
            $storeBrand->brandId = $brandId;
            $storeBrand->save(false);
            $storeBrand->brand->setProducts($this->id);
        }

    }

    public function unsetBrand($brandId){
        $brand = Brands::findOne($brandId);
        if(StoreBrands::deleteAll(['storeId' => $this->id, 'brandId' => $brandId]))
            return $brand->unsetProducts($this->id);

    }


    public function getSuppliers(){
        return $this->hasMany(Suppliers::className(), ['id' => 'supplierUid'])
        ->joinWith('storeSuppliers', true, 'JOIN')
        ->where("StoreSuppliers.type = 'b2c' AND StoreSuppliers.storeId = ".$this->id)
        ->viaTable('StoreSuppliers', ['storeId' => 'id']);

    }

    public function getBrands(){  //var_dump($this->id);die();
        return $this->hasMany(Brands::className(), ['id' => 'brandId'])
        ->joinWith('storeBrands', true, 'JOIN')
        ->where("StoreBrands.type = 'b2c' AND storeId = ".$this->id)
        ->viaTable('StoreBrands', ['storeId' => 'id']);

    }

    public function getProducts(){
        $query = Products::find();
        // $query = $this->hasMany(Products::className(), ['Products.id' => 'sp.productId'])
        // ->join('LEFT JOIN', 'StoreProducts sp', 'sp.productId = Products.id');
        // if(Yii::$app->id != "app-b2b")
        //     $query->where("StoreProducts.type = 'b2c' AND storeId = ".$this->id);
        return $query;//->viaTable('StoreProducts', ['storeId' => 'id']);

    }

    public function getB2bSuppliers(){
        return $this->hasMany(Suppliers::className(), ['id' => 'supplierUid'])
        ->joinWith('storeSuppliers', true, 'JOIN')
        ->where("StoreSuppliers.type = 'b2b' AND StoreSuppliers.storeId = ".$this->id)
        ->viaTable('StoreSuppliers', ['storeId' => 'id']);

    }

    public function getB2bBrands(){
        return $this->hasMany(Brands::className(), ['id' => 'brandId'])
        ->joinWith('storeBrands', true, 'JOIN')
        ->where("StoreBrands.type = 'b2b' AND storeId = ".$this->id)
        ->viaTable('StoreBrands', ['storeId' => 'id']);

    }

    public function getB2bProducts(){
        return $this->hasMany(Products::className(), ['id' => 'productId'])
        ->joinWith('storeProducts', true, 'JOIN')
        ->where("StoreProducts.type = 'b2b' AND storeId = ".$this->id)
        ->viaTable('StoreProducts', ['storeId' => 'id'])
        ->orderBy('dateAdded');
    }

    public function getStoreBanners()

    {
        return $this->hasMany(Banners::className(), ['id' => 'bannerId'])->viaTable('StoreBanners', ['storeId' => 'id'], function($query){ $query->orderBy('StoreBanners.position asc'); });


        //var_dump($this->hasMany(Banners::className(), ['id' => 'bannerId'])->viaTable('Storebanners', ['storeId' => 'id']));die();

    }



    public function getEmailFacebookImage(){    
        if($this->getFacebookUrl()!=''){
            return '<a href="'.$this->getFacebookUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/facebook.png></a>';
        } else {
            return '';
        }
    }

    public function getEmailTwitterImage(){    
        if($this->getTwitterUrl()!=''){        
            return '<a href="'.$this->getTwitterUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/twitter.png></a>';
        } else { 
            return '';
        }
    }
   
    public function getEmailInstagramImage(){    
        if($this->getInstagramUrl()!=''){        
            return '<a href="'.$this->getInstagramUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/instagram.png ></a>';
        } else {
            return '';
        }
    }
    public function getEmailYoutubeImage(){    
        if($this->getYoutubeUrl()!=''){        
            return '<a href="'.$this->getYoutubeUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/youtube.png></a>';
        } else {
            return '';
        }
    }
    public function getEmailPinterestImage(){    
        if($this->getPinterestUrl()!=''){        
            return '<a href="'.$this->getPinterestUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/pinterest.png ></a>';
        } else {
            return '';
        }
    }
    
    public function getEmailGoogleplusImage() {   
        if($this->getGoogleplusUrl()!=''){        
            return '<a href="'.$this->getGoogleplusUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/google-plus.png ></a>';
        } else {  
            return '';
        }
    }
    public function getEmailLinkedinImage(){    
        if($this->getLinkedinUrl()!=''){        
            return '<a href="'.$this->getLinkedinUrl().'"><img src='.Yii::$app->params["rootUrl"].'/store/site/email/linkedin.png></a>';
        } else { 
            return '';
        }
    }
    public function getLogo(){    
        if($this->logoPath!=''){        
            return '<img  src='.Yii::$app->params["rootUrl"].$this->logoPath.'>';
        } else  {   
            return '';
        }
    }
    public function getMainLogo(){ 
        return '<img  src='.Yii::$app->params["rootUrl"].'/store/site/logo/logo.png'.'>';        
    }
    public function getStoreAdminName()

    {
        $user=User::find()->where(['id' => $this->adminId])->one();
        return $user->firstname .' '.$user->lastname;

    }

    public function getCategoryIds()

    {
        /*return $this->hasMany(ProductCategories::className(), ['storeId' => 'id'])
                    ->join('JOIN',
                        'StoreProducts as sp',
                        'sp.storeId = StoreProducts.storeId'
                    )
                    ->where("sp.storeId=".$this->id."");*/


                    //var_dump($categoryIds);die();


        $categoryIds = $this->hasMany(ProductCategories::className(), ['storeId' => 'id'])
                    ->join('JOIN',
                        'StoreProducts as sp',
                        'sp.storeId = StoreProducts.storeId'
                    )
                    ->where("sp.storeId=".$this->id."");


        //return $categoryIds;          
                    
        var_dump($categoryIds);die();            

    }



    public function getFacebookUrl(){ 
        $facebookUrl=Configuration::findSetting('facebook', $this->id);
        return $facebookUrl; 
    } 
    public function getYoutubeUrl(){      
        $youtubeUrl=Configuration::findSetting('youTube', $this->id);
        return $youtubeUrl; 
    }

    public function getPinterestUrl(){      
        $pinterestUrl=Configuration::findSetting('pinterest', $this->id);       
        return $pinterestUrl; 
    }
    public function getTwitterUrl(){ 
        $twitterUrl=Configuration::findSetting('twitter', $this->id);
        return $twitterUrl; 
    }
    public function getInstagramUrl(){
        $instagramUrl=Configuration::findSetting('instagram', $this->id);
        return $instagramUrl; 
    }   

    public function getGoogleplusUrl(){
        $googleplusUrl=Configuration::findSetting('google', $this->id);
        return $googleplusUrl; 
    } 

    public function getLinkedinUrl(){ 
        $linkedinUrl=Configuration::findSetting('linkedin', $this->id);
        return $linkedinUrl; 
    }   

    public function getContactusUrl() {
        $link=$this->siteUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/contact']).'"> Contact Us</a>';
    }
    public function getLoginUrl() { 
        $link=$this->siteUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/login']).'"> Login</a>';
    } 
    public function getMyAccountUrl(){
        $link=$this->siteUrl;
        return '<a href="'.$link.\yii\helpers\Url::to(['/site/account']).'"> My Account</a>';
    }       

    //returns IDs of categories which has products in this store
    public function getActiveCategoryIds(){
        // $categories = Yii::$app->db->createCommand("select pc.categoryId from ProductCategories pc join StoreProducts sp on sp.productId = pc.productId where sp.storeId = ".$this->id." group by pc.categoryId")
        // ->queryAll();
        $categories = Yii::$app->db->createCommand("select pc.categoryId,c.parent from ProductCategories pc join StoreProducts sp on sp.productId = pc.productId join Products p on p.id = sp.productId join Categories c on c.id = pc.categoryId where sp.storeId = ".$this->id." and (pc.storeId = '".$this->id."' OR pc.storeId = 0) and sp.enabled = 1 and p.status = 1 group by pc.categoryId")
        ->queryAll();       
        //$categories = ArrayHelper::map($categories, 'categoryId', 'categoryId');        
        $pcategories = ArrayHelper::map($categories, 'categoryId', 'parent');
        $parents = array_unique(array_values($pcategories));
        $categories = ArrayHelper::map($categories, 'categoryId', 'categoryId');
        //var_dump($categories + $parents);
        return $categories + $parents;
    }    

    public function getSelectableSuppliers(){
        $brands = ArrayHelper::map(StoreBrandsVisibility::find()->where(['storeId'=>$this->id])->all(),'id','brandId') + ArrayHelper::map($this->brands, 'id', 'id');
        return Suppliers::find()
        ->join('LEFT JOIN', 'BrandSuppliers bs', 'bs.supplierUid = Users.id')
        ->join('LEFT JOIN', 'StoreBrands sb', 'sb.brandId = bs.brandId')
        ->where('bs.brandId in ('.implode(",", $brands).') AND sb.storeId = '.$this->id)->orderBy('firstname asc')->all();
    }

    public function getAdmin(){
        return $this->hasOne(User::className(), ['id' => 'adminId']);
    }

    public function getBestSellers($limit = 10){
        $query = new \yii\db\ActiveQuery('\common\models\Products');
        $items = ArrayHelper::map(Yii::$app->db->createCommand("select p.id, count(p.id) as pCount from Products p join OrderItems oi on oi.productId = p.id join Orders o on oi.orderId = o.id where storeId=".$this->id." and p.status = 1 group by p.id order by pCount desc limit $limit")
        ->queryAll(), 'id', 'id');
        //var_dump($items); die;
        if(count($items) < $limit){
            $items = $items + ArrayHelper::map(\common\models\Products::find()->andWhere(['not in', 'id', $items])->limit($limit - count($items))->all(), 'id', 'id');
        }
        return $items;
        // return ArrayHelper::map($query
        //       ->select('p.id, count(p.id) as pCount')
        //       ->from('Products p')
        //       ->join('JOIN', 'OrderItems oi', 'oi.productId = p.id')
        //       ->join('JOIN', 'Orders o', 'o.id = oi.orderId')
        //       ->where(['storeId' => $this->id])
        //       ->groupBy('p.id')
        //       ->orderBy('pCount DESC')->all(), 'p.id', 'pCount');
    }


    public function getFeatured($limit = 3) {
        if (!empty(Yii::$app->params['homePageFeaturedProductsCount']))
            $limit = Yii::$app->params['homePageFeaturedProductsCount'];
        $featured = Products::find()
                ->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')
                ->join('JOIN', 'Attributes as a', 'a.id =av.attributeId')
                ->andWhere(['a.code' => 'featured', 'av.value' => 1, 'storeId' => $this->id])
                ->groupBy('Products.id')
                ->limit($limit);
        $count = $featured->count();
	//echo $count;
        $id=[];
        foreach ($featured->all() as $featured_id) {
             $id[]=$featured_id->id;
        }
        
        /*$featured_list = Products::find()
                ->select('Products.id')
                ->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')
                ->join('JOIN', 'Attributes as a', 'a.id =av.attributeId')
                ->andWhere(['a.code' => 'featured', 'av.value' => 1, 'storeId' => $this->id])
                ->groupBy('Products.id')
                ->limit($limit)->all(); */
        
        $normal_products = Products::find()
                ->andwhere(['not in','Products.id',$id])
                ->groupBy('Products.id')
                ->limit($limit-$count);
        
        foreach ($normal_products->all() as $normal_product_id) {
             $id[]=$normal_product_id->id;
        }
        
        
        $featured_products= Products::find()->andwhere(['IN', 'id', array_unique($id)]);
        
//print_r($normal_count);


//        if ($count == 0) {
//            $featured = Products::find()
//			->groupBy('Products.id')
//                    ->limit($limit);
//        }
        return $featured_products;
        //return Yii::$app->db->createCommand("select p.id from Products p join AttributeValues av on av.productId=p.id join Attributes a on a.id = av.attributeId where a.code = 'featured' and storeId = ".$this->id." group by p.id order by p.id limit $limit")->queryAll();

    }


    public function getPortalProducts($sessionVar){   //var_dump($sessionVar); die(); //var_dump(Yii::$app->session['studentByod']);die();
        $todays = date("Y-m-d H:i:s");
        
       /* if(!$portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['studentByod'],'storeId'=>$this->id])->one()){
            $portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session['schoolByod'],'storeId'=>$this->id])->one();
        }*/

        //$_REQUEST['overrideProductCheck'] = true;

        if($sessionVar == "studentByod")
            $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['studentByod'],'storeId'=>$this->id])->one();
        elseif($sessionVar == "schoolByod")
            $portal = ClientPortal::find()->where(['schoolCode'=>Yii::$app->session['schoolByod'],'storeId'=>$this->id])->one();
        else
            $portal = ClientPortal::find()->where(['studentCode'=>Yii::$app->session['clientPortal'],'storeId'=>$this->id])->one();

        $query =  ClientPortalProducts::find()
                ->join('JOIN',
                    'ClientPortal as cp',
                    'cp.id = ClientPortalProducts.portalId'
                );
                $query->where("cp.storeId=".$this->id." and cp.id=".$portal->id."");
        return $query;        
    }
    
    public function gethasGiftWrap(){
        $gift_wrap = Configuration::findSetting('gift_wrapping', Yii::$app->params['storeId']);
        $gift_wrap_check = false;
        if (isset($gift_wrap) && $gift_wrap != 'no')
            $gift_wrap_check = true;
        
        return $gift_wrap_check;
    }

    public function getPaymentMethod(){
        if(isset(Yii::$app->params['paymentGateway']) && Yii::$app->params['paymentGateway'] == "Eway")
            return "eway";
        elseif(isset(Yii::$app->params['paymentGateway']) && Yii::$app->params['paymentGateway'] == "Braintree")
            return "braintree";
        else
            return "paypal";
    }
    public function getStoreDetails(){
        return $this->billingAddress.' | '.$this->phone.' | abn : '.$this->abn;
    }   

    public function getMapUrl(){        
        $defaultAddress = B2bAddresses::find()->where(['ownerId'=>$this->adminId])->one();
        $mapaddress=isset($defaultAddress)?$defaultAddress->billingAddressAsText:'';
        return '<a href="https://maps.google.com/maps?&amp;q='.$mapaddress.'&amp">Show Store in Map</a>'; 
    }

    public function getAddressFormatted(){
        $defaultAddress = B2bAddresses::find()->where(['ownerId'=>$this->adminId])->one();
        return $defaultAddress->storeName.", ".$defaultAddress->streetAddress.", ".$defaultAddress->city." ".$defaultAddress->state." ".$defaultAddress->postCode;
    }
    //-------------- @ aaron 19/08/2016 site map ------------------------------
    public function getSiteMapMenus(){   //site map xml
        $storeId = $this->id;
        $menuPositions = ['home','dynamicmenus','gallery','advice','brands','promotion'];
        //var_dump($menuPositions);die;
        $storeMenuExclusions = [ ];
        $todays = date("Y-m-d H:i:s");
        //$store=Stores::findOne($storeId);
        $storePages = StorePages::find()->where(['storeId' => $this->id])->orderBy('id')->all();
        $gallerys = Gallery::find()->where(['storeId' => $this->id])->orderBy('position')->all(); 
        $storePromotions = StorePromotions::find()->where("storeId = $storeId and endDate > '$todays' and active = 1")->groupBy('promotionId')->all();
        $activeCategoryMenu=[];
        $menus = Menus::find()   //orderby position
            ->where(['storeId' => $this->id,'unAssigned'=>0])
            //->orderBy('position')
            ->orderBy('parent')
            ->asArray()
            ->all();
        foreach ($menus as $key => $menu) {
            $activeCategoryMenu[]=
            [ 
                'url' =>(preg_match("/http/", $menu['path']))?$menu['path']:$this['siteUrl']."/".$menu['path'],
                'categoriesId'=>$menu['categoriesId'],  
            ];
        }
        
        $nonCategoryMenu=[];

        foreach ($menuPositions as $menuItem) {
            if($menuItem != "dynamicmenus"){ 
                if($menuItem == "brands"){ //brands 
                    $nonCategoryMenu[]=
                    [ 
                        'url' =>$this['siteUrl']."/brands",
                        'categoriesId'=>0,  
                    ];
                }
                elseif($menuItem == "home"){ //home page
                    $nonCategoryMenu[]=
                    [
                        'url' =>$this['siteUrl']."/site/index",
                        'categoriesId'=>0,
                    ];
                }
                elseif($menuItem == "advice"){ // cms pages
                    foreach($storePages as $index => $storepage) {  
                        if(isset($storepage->cmsPage->status) && $storepage->cmsPage->status==1) {
                            $nonCategoryMenu[]=
                            [
                                'url' =>$this['siteUrl'].'/'.$storepage->cmsPage->slug.'.html',
                                'categoriesId'=>0,
                            ]; 
                        }    
                    }   
                }
                elseif($menuItem == "gallery"){ //gallerys
                    if(count($gallerys) >= 1) {
                        foreach($gallerys as $index => $gallery){
                            if(count($gallery->galleryImages)!=0){
                                $nonCategoryMenu[]=
                                [
                                    'url' =>$this['siteUrl'].'/gallery/view?id='.$gallery['id'],
                                    'categoriesId'=>0,
                                ]; 
                            }
                        }
                    }
                }
                elseif($menuItem == "promotion"){ //promotion
                    if(!empty($storePromotions)){
                        foreach ($storePromotions as $storePromotion) {
                            $promotion = ConsumerPromotions::find()->where("status = 'published' and id = $storePromotion->promotionId")->groupBy('id')->one();
                            $nonCategoryMenu[]=
                            [
                                'url' =>$this['siteUrl'].'/promotions/view?id='.$promotion->id,
                                'categoriesId'=>0,
                            ]; 
                        } 
                    }
                }
            }
        }
        $allMenus=array_merge($activeCategoryMenu,$nonCategoryMenu);
        return $allMenus;
    }
    public function getSiteMap(){
        //$store=Stores::findOne(Yii::$app->params['storeId']);
        //$store=Stores::findOne(1);
        $activeCategory=$this->activeCategoryIds;
        array_push($activeCategory,"0");
        $allMenus=$this->getSiteMapMenus();
        $priority = '0.5';        
        $changefreq = 'daily';      
        $lastmod = date("Y-m-d");//'1985-10-07';
        
        //---------------------------------- xml file creation -----------------------------
        //below array $url converted into required(sitemap.xml) structure       
        $xmldata = '<?xml version="1.0" encoding="utf-8"?>'; 
        $xmldata .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($allMenus as $url => $data)
        {
            if (in_array($data['categoriesId'], $activeCategory)) {
                $xmldata .= '<url>';
                $xmldata .= '<loc>'.$data['url'].'</loc>';
                $xmldata .= '<lastmod>'.$lastmod.'</lastmod>';
                $xmldata .= '<changefreq>'.$changefreq.'</changefreq>';
                $xmldata .= '<priority>'.$priority.'</priority>';
                $xmldata .= '</url>';
            }
        }   
        $xmldata .= '</urlset>'; 
        return $xmldata;
    }

    public function disableScheduledStoreProducts($storeId){
        $StoreProducts = StoreProducts::find()->where(['storeId' => $storeId, 'enabled' => 1, 'type' => 'b2c'])
                                              ->andWhere("disableDate != ''")
                                              ->all();
        $cnt = 0;
        foreach ($StoreProducts as $key => $StoreProduct) {
            if(strtotime(date('Y-m-d')) > strtotime($StoreProduct->disableDate)){
                $StoreProduct->disableDate = NULL;
                $dateExtended = $StoreProduct->save(false);
                $cnt++;
            }
        }
        return $cnt;
    }

}
