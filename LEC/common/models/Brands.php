<?php  

namespace common\models;

use Yii;
use common\models\TreeQuery;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use common\models\Products;
use common\models\Stores;

/**
 * This is the model class for table "Brands".
 *
 * @property integer $id
 * @property string $title
 * @property integer $enabled
 *
 * @property BrandSuppliers[] $brandSuppliers
 */
class Brands extends \yii\db\ActiveRecord
{
    use \common\traits\base\BeforeQueryTrait;
    public $width;
    public $height;
    public $image;
    public $contentImage;
    public $BEFORE_QUERY;
    //public $content;

    public static function tableName()
    {
        return 'Brands';
    }

    public function init(){
        if(Yii::$app->id == "app-frontend")
            $this->BEFORE_QUERY = "Brands.enabled = 1";
        parent::init();
    } 

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enabled'], 'integer'],
            [['title','path'], 'string', 'max' => 45],
            [['pageContent'],'string'],
            [['title','path','urlKey'], 'required'], 
            [['image','pageContent','contentImage'], 'safe'],
            [['image','contentImage'], 'file', 'extensions'=>'jpg, gif, png'],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Categories', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Products', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\CmsPages', 'targetAttribute' => 'slug', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Brands', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'enabled' => 'Enabled',
        ];
    }

    public function setProducts($storeId, $type = "b2c"){
        $store = Stores::findOne($storeId);
        $brandproducts = Products::find()->where("brandId='".$this->id."' and (createdBy=0 or createdBy=".$store->adminId.")")->all();
        foreach($brandproducts as $product){  
            if(!StoreProducts::find()->where(['storeId' => $storeId, 'productId' => $product->id, 'type' => $type])->one()){
                $storeProduct = new StoreProducts;
                $storeProduct->storeId = $storeId;
                $storeProduct->productId = $product->id;
                $storeProduct->type = $type;
                $storeProduct->save(false);
            }
        }
    }

    public function unsetProducts($storeId, $type = "b2c"){
        $brandproducts = Products::find()->where(['brandId'=>$this->id])->all();
        foreach($brandproducts as $product){
            StoreProducts::deleteAll(['storeId' => $storeId, 'productId' =>$product->id, 'type' => $type]);
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandSuppliers()
    {
        return $this->hasMany(BrandSuppliers::className(), ['brandId' => 'id']);
    }

    public function hasProducts()
    {
        if(isset(Yii::$app->params['temp']['brandsWithProducts'])){
            $brandsWithProducts = Yii::$app->params['temp']['brandsWithProducts'];
        }else{
            $brandsWithProducts = ArrayHelper::map(Brands::find()->where("EXISTS(SELECT 1 from Products p where Brands.id = p.brandId and status = 1 and createdBy = 0)")->all(), 'id', 'id');
            Yii::$app->params['temp']['brandsWithProducts'] = $brandsWithProducts;
        }
        return in_array($this->id, $brandsWithProducts);
    }

    public function getProducts(){
        return $this->hasMany(Products::className(), ['brandId' => 'id']);
    }

    public function getStoreBrands(){
        return $this->hasMany(StoreBrands::className(), ['brandId' => 'id']);
    }

    public function getStores(){
        return $this->hasMany(Stores::className(), ['id' => 'storeId'])

        ->joinWith('storeBrands', true, 'JOIN')

        ->where("StoreBrands.type = 'b2c' AND brandId = ".$this->id)

        ->viaTable('StoreBrands', ['brandId' => 'id']);
    }


    public function getStoreProducts($storeId,$type = null){

        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    ->from('Products')
                    ->join('JOIN',
                        'Brands as b',
                        'b.id = Products.brandId'
                    );
                    if(Yii::$app->id != "app-b2b")
                    $query->join('JOIN',
                        'StoreProducts as sp',
                        'sp.productId = Products.id'
                    );
                    $query->groupBy('Products.id')
                    ->where("Products.status = '1' AND Products.brandId = ".$this->id);
                    if(Yii::$app->id != "app-b2b")
                        $query->andWhere('sp.storeId = '.$storeId.'');

                    if(isset($type))
                        //$products->andWhere('ss.type="'.$type.'" AND sp.type="'.$type.'"');
                        $products->andWhere('sp.type="'.$type.'"');
                    return $products; 
    }


    public function getSupplier(){ //used in the case when a brand can have only one supplier.
        return $this->hasOne(Suppliers::className(), ['id' => 'supplierUid'])->viaTable('BrandSuppliers', ['brandId' => 'id']);
    }

    public function getSuppliers(){ //used in the case when a brand can have multiple suppliers.
        return $this->hasMany(Suppliers::className(), ['id' => 'supplierUid'])->viaTable('BrandSuppliers', ['brandId' => 'id']);
    }

    public function getFilterAttributes(){
        $query = Attributes::find()
        ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
        //->join('JOIN', 'ProductCategories pc', 'pc.productId = av.productId')
        ->join('JOIN', 'Products p', 'p.id = av.productId')
        ->join('JOIN', 'Brands b', 'b.id = p.brandId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = p.id')
        //->join('JOIN', 'Products p2', 'sp.productId = p2.id')
        ->where(['b.id' => $this->id, 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId'], 'sp.enabled' => '1',]);
	else
            $query->andWhere('createdBy = 0');
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND av.value != ''")
        ->groupBy('Attributes.id');
        $attributes = $query->all();
        $filterAttributes = [];
        foreach ($attributes as $key=>$attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        } 
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code){
        $query = AttributeValues::find()
        ->join('JOIN', 'Attributes a', 'a.id = AttributeValues.attributeId')
        //->join('JOIN', 'ProductCategories pc', 'pc.productId = AttributeValues.productId')
        ->join('JOIN', 'Products p', 'p.id = AttributeValues.productId')
        ->join('JOIN', 'Brands b', 'b.id = p.brandId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = p.id')
        //->join('JOIN', 'Products p2', 'sp.productId = p2.id')
        ->where(['b.id' => $this->id, 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId'], 'sp.enabled' => '1',]);
	else
            $query->andWhere('createdBy = 0');
        $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
        ->groupBy('`AttributeValues`.`value`');
        return $query->all();
    }

    public function getProductsMaximumPrice(){
        $priceAttr = (Yii::$app->id=="app-b2b" || Yii::$app->id=="app-api")? "22" : "10";
        if(Yii::$app->id=="app-frontend")
            $maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join Products p on av.productId = p.id join StoreProducts sp on sp.productId = p.id where attributeId = $priceAttr and p.brandId = ".$this->id." and p.status = 1 and sp.storeId = ".Yii::$app->params['storeId']." and sp.enabled = 1 and av.storeId = 0 order by av.value+0 desc limit 1")->one();
        else
            $maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join Products p on av.productId = p.id where attributeId = $priceAttr and p.brandId = ".$this->id." and p.status = 1 order by av.value+0 desc limit 1")->one();
        if($maxValue)
            return $maxValue->value;
        else
            return 14000;
    }

    public function getResolvedContent(){
        return str_replace('[!rootUrl]', Yii::$app->params['rootUrl'], $this->pageContent);
    }

    public function getSelf(){
        return $this;
    }

    public function getLogoUrl(){
        if(isset($this->path) && $this->path!=NULL)        
            return Yii::$app->params["rootUrl"].$this->path;
        else
            return Yii::$app->params["rootUrl"].'/store/brands/logo/nologo.jpg';
    }

    public function getProductsBrands(){
       return [$this];           
    }

    public function disableProducts(){
        foreach($this->products as $product){
            $product->status = '0';
            $product->save(false);
        }
        return true;
    }

}
