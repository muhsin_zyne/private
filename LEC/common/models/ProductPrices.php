<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductPrices".
 *
 * @property integer $id
 * @property string $value
 * @property string $type
 * @property string $from
 * @property string $to
 * @property integer $productId
 * @property integer $storeId
 * @property integer $supplierUid
 */
class ProductPrices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductPrices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'number'],
            [['from', 'to'], 'safe'],
            [['productId', 'storeId', 'supplierUid'], 'required'],
            [['productId', 'storeId', 'supplierUid'], 'integer'],
            [['type'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'type' => 'Type',
            'from' => 'From',
            'to' => 'To',
            'productId' => 'Product ID',
            'storeId' => 'Store ID',
            'supplierUid' => 'Supplier Uid',
        ];
    }
}
