<?php

namespace common\models;
use frontend\components\Helper;
use common\models\B2bAddresses;
use Yii;

/**
 * This is the model class for table "SalesComments".
 *
 * @property integer $id
 * @property string $type
 * @property integer $typeId
 * @property string $comment
 * @property string $createdDate
 * @property string $notifiedDate
 */
class SalesComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SalesComments';
    }

    /**
     * @inheritdoc
     */
    //public $notify;
    //public $orderId;
    public $readyComment;
    public function rules()
    {
        return [
            [['type', 'typeId', 'comment','orderId'], 'required'],
            [['type', 'comment'], 'string'],
            [['typeId','orderId'], 'integer'],
            [['createdDate', 'notifiedDate','notify'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'orderId' => 'Order Id',
            'typeId' => 'Type ID',
            'comment' => 'Comment',
            'createdDate' => 'Created Date',
            'notifiedDate' => 'Notified Date',
            'notify'=>'Notify Customer by Email',
        ];
    }
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }
    public function getInvoiceStatus()
    {
        return $this->hasOne(Invoices::className(), ['id' => 'typeId']);
    }
    public function getCreditmemoStatus()
    {
        return $this->hasOne(Creditmemos::className(), ['id' => 'typeId']);
    }
    public function getShipmentStatus()
    {
        return $this->hasOne(Creditmemos::className(), ['id' => 'typeId']);
    }
    public function getSalesCommentDate()
    {
        return Helper::date($this->createdDate, "F j, Y ");
    }
    public function sendSalesCommentNotificationMail() { // send to user   
        $template = \common\models\EmailTemplates::find()->where(['code' => 'salescommentnotification'])->one();
        $store = \common\models\Stores::find()->where(['id' => $this->order->storeId])->one();
        $salescomments=SalesComments::find()->where(['id' => $this->id])->one();
        
        if($this->order->fromRegisteredUser()==true){        
            $user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        } else {
            $userfullname=ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }
        
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store'=>$store,'salescomments'=>$salescomments]; 
        $queue->replacements =[ 'fromEmailAddress'=>'','siteUrl'=>'','logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userFullName'=>$userfullname,'useremail'=>$useremail,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->billingAddress];                                   

        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }
    public function sendOrderStatusUpdateMail($orderid=NULL) {        
        $orderId=isset($orderid) ? $orderid : $this->orderId;
        $order = \common\models\Orders::find()->where(['id' => $orderId])->one();
        $template = \common\models\EmailTemplates::find()->where(['code' => 'orderstatusupdate'])->one();
        $store = \common\models\Stores::find()->where(['id' => $order->storeId])->one();
        
       
        if($order->fromRegisteredUser()==true){        
            $user = \common\models\User::find()->where(['id' => $order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        } else {
            $userfullname=ucfirst($order->billing_firstname).' '.ucfirst($order->billing_lastname);
            $useremail=$order->email;
        }       
        if(!isset($orderid)){
            $salescomments=SalesComments::find()->where(['id' => $this->id])->one();
            $comment='<tr>
              <td bgcolor="#f6f6f6" style="padding:15px;border:1px solid #eeeeee;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                        <tr>
                            <td style="font-size:13px;color:#5e5e5e;padding-bottom:6px;font-weight:bold;">
                                Comment:
                            </td>
                        </tr>
                        <tr>
                            <td style="font-style:italic;color:#737373;font-size:13px;">
                                <i>
                                    '.$salescomments->comment.'
                                </i>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>'; 
        } else {
            $comment='';
        }
        
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store'=>$store,'order'=>$order]; 
        $queue->replacements =[ 'fromEmailAddress'=>'','siteUrl'=>'','logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userFullName'=>$userfullname,'useremail'=>$useremail,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->billingAddress,'statusUpdateBanner'=>$this->getStatusUpdateBanner(),'timeGrid'=>$this->getTimeGrid($order),'comment'=>$comment];
        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }
    public function sendSuppliersSalesCommentMail()  // send to user
    {
       
        $template = \common\models\EmailTemplates::find()->where(['code' => 'supplierssalescommentnotification'])->one();
        $store = \common\models\Stores::find()->where(['id' => $this->order->storeId])->one();
        //$order = \common\models\Orders::find()->where(['id' => $this->orderId])->one();
        if($this->order->fromRegisteredUser()==true)
        {
            $user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        }
        else
        {
            $userfullname=ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }
        $supplier = Suppliers::findOne(Yii::$app->user->Id);
        $suppliersName=ucfirst($supplier->firstname).' '.ucfirst($supplier->lastname);
        //var_dump($this);
        $queue = new \common\models\EmailQueue;
            $queue->models = ['salescomments'=>$this,'orders'=>$this->order]; 
            $queue->replacements =[ 'fromEmailAddress'=>'','siteUrl'=>'','logo'=>'<img  src='.Yii::$app->params["rootUrl"].'/store/site/logo/logo-legc.png>',
                'emailFacebookImage'=>$this->legjFacebookImage,'emailTwitterImage'=>'','emailInstagramImage'=>'',
                'emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'','suppliersName'=>$suppliersName,
                'emailLinkedinImage'=>'','userFullName'=>$userfullname,'useremail'=>'aaron@xtrastaff.com.au','orderDate'=>$this->order->orderPlacedDate];                                   

            $queue->from = "no-reply";
            //$queue->recipient = $user;//var_dump($template);die;
            $queue->recipients = $useremail;
            //$queue->recipients = 'aaron@xtrastaff.com.au';
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
            $queue->save();
    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/order-update.jpg></a>'; 
    }
    public function getMailToImage(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/to.png></a>'; 
    }
    public function getMailMessageBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/message.jpg></a>'; 
    }
    public function getStatusUpdateBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/order-status-update.jpg></a>'; 
    }
    public function getTimeGrid($order){        
        if(isset($order->collectFrom)){
            $defaultAddress = B2bAddresses::find()->where(['id'=>$order->collectFrom])->one();
        } else {
           $defaultAddress = B2bAddresses::find()->where(['ownerId'=>$order->store->adminId])->one();
        }
        return $defaultAddress->shortGrid;
    }

}
