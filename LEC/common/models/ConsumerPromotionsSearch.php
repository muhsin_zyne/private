<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ConsumerPromotions;

/**
 * ConsumerPromotionsSearch represents the model behind the search form about `common\models\ConsumerPromotions`.
 */
class ConsumerPromotionsSearch extends ConsumerPromotions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'shortTag', 'fromDate', 'toDate', 'createdOn', 'consumerStartDate', 'costEndDate', 'image', 'catalog', 'reminderDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConsumerPromotions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'fromDate' => $this->fromDate,
            'toDate' => $this->toDate,
            'createdOn' => $this->createdOn,
            'consumerStartDate' => $this->consumerStartDate,
            'costEndDate' => $this->costEndDate,
            'reminderDate' => $this->reminderDate,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'shortTag', $this->shortTag])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'catalog', $this->catalog]);

        return $dataProvider;
    }
}
