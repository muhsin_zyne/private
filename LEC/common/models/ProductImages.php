<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductImages".
 *
 * @property integer $id
 * @property string $path
 * @property integer $productId
 * @property integer $isThumbnail
 * @property integer $isSmall
 * @property integer $isBase
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductImages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'string'],
            [['productId', 'isThumbnail', 'isSmall', 'isBase'], 'required'],
            [['productId', 'isThumbnail', 'isSmall', 'isBase'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'productId' => 'Product ID',
            'isThumbnail' => 'Is Thumbnail',
            'isSmall' => 'Is Small',
            'isBase' => 'Is Base',
        ];
    }
}
