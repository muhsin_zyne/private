<?php

namespace common\models;

use Yii;

class AppointmentDefaultSettings extends \yii\db\ActiveRecord
{

	public static function tableName()
    {
        return 'AppointmentDefaultSettings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['	sun','mon','tue','wed','thu','fri','sat','storeId'], 'safe'],
            [[' sun','mon','tue','wed','thu','fri','sat'],'text'],
            [['id', 'storeId'], 'integer'],
        ];
    }
}
?>