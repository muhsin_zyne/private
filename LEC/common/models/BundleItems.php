<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "BundleItems".
 *
 * @property integer $id
 * @property string $price
 * @property string $priceType
 * @property string $defaultQty
 * @property integer $isUserDefinedQuantity
 * @property integer $position
 * @property integer $default
 * @property integer $bundleId
 * @property integer $productId
 */
class BundleItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BundleItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bundleId', 'productId'], 'required'],
            [['id', 'isUserDefinedQuantity', 'position', 'default', 'bundleId', 'productId'], 'integer'],
            [['price', 'defaultQty'], 'number'],
            [['priceType'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'priceType' => 'Price Type',
            'defaultQty' => 'Default Qty',
            'isUserDefinedQuantity' => 'Is User Defined Quantity',
            'position' => 'Position',
            'default' => 'Default',
            'bundleId' => 'Bundle ID',
            'productId' => 'Product ID',
        ];
    }

    public function getProduct() 
    {
        return $this->hasOne(Products::className(),['id' => 'productId']);
    }

    public function getBundle() 
    {
        return $this->hasOne(Bundles::className(),['id' => 'bundleId']);
    }

    
}
