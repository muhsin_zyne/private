<?php

namespace common\models;

use Yii;
use kartik\tree\TreeView;
use common\models\TreeQuery;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use common\models\Products;
use frontend\components\Helper;
/**
 * This is the model class for table "Users".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $firstname
 * @property string $lastname
 * @property integer $roleId
 */
class Suppliers extends \yii\db\ActiveRecord
{

    public $password;
    //public $status;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'email', 'status'], 'required'],
            [['password'], 'required', 'when' => function($model){ 
                return false; 
            }, 'whenClient' => "function(attribute, value){ return $('.brands-list').length < 1 }"
            ],
            [['email'], 'email'],
            [['firstname', 'lastname'], 'string', 'max' => 45],
            //[['phone'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'firstname' => 'Supplier Name',
            'lastname' => 'Lastname',
            'roleId' => 'Role ID',
        ];
    }

    public static function find(){
        return parent::find()->andWhere('roleId=2'); //only find suppliers
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    public function setPassword($password){
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getTitle(){
        return $this->firstname." ".$this->lastname;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return $this->hasMany(Brands::className(), ['id' => 'brandId'])->viaTable('BrandSuppliers', ['supplierUid' => 'id']);
    }

    public function getStores(){
        return $this->hasMany(Stores::className(), ['id' => 'storeId'])->viaTable('StoreSuppliers', ['supplierUid' => 'id']);
    }

    public function getStoreSuppliers(){
        return $this->hasMany(StoreSuppliers::className(), ['supplierUid' => 'id']);
    }

    public function getFullName(){
        return $this->firstname." ".$this->lastname;
    }

    public function getProducts($storeId = null, $type = null)
    { //die('getproducts');
       
        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    ->from('Products')
                    ->join('JOIN',
                        'Brands as b',
                        'Products.brandId = b.id'
                    )
                    ->join('JOIN',
                        'BrandSuppliers as bs',
                        'bs.brandId = b.id' 
                    );
                    if(Yii::$app->id != "app-b2b")
                        $query->join('JOIN',
                            'StoreProducts as sp',
                            'sp.productId = Products.id' 
                        );

                    $query->groupBy('Products.id')
                    ->where("bs.supplierUid = ".$this->id." AND Products.status = '1'");

        if(isset($storeId) && Yii::$app->id != "app-b2b")
            $products->andWhere('sp.storeId = "'.$storeId.'"');
        if(isset($type))
            $products->andWhere('ss.type="'.$type.'" AND sp.type="'.$type.'"');

        return $products; 

    }
    public function getDashboardProducts() // only for suppliers
    { //die('getproducts');
       
       $query = new ActiveQuery('common\models\Products');
        
        $products = $query
            ->from('Products')

            ->join('JOIN',
                'Brands as b',
                'Products.brandId = b.id'
            )
            ->join('JOIN',
                'BrandSuppliers as bs',
                'bs.brandId = b.id' 
            );
            
            $query->groupBy('Products.id')
            ->where("bs.supplierUid = ".$this->id."");
        
        return $products; 

    }

    

    public function getOrders()
    {
        $query = new \yii\db\ActiveQuery('common\models\Orders');
        return $query->where("EXISTS (SELECT 1 FROM OrderItems oi WHERE  oi.orderId = Orders.id and supplierUid = ".$this->id." and Orders.type = 'b2b')");
    }
    public function getOrdersSuppliers($condition)
    {
        $query = new \yii\db\ActiveQuery('common\models\Orders');
        return $query->where("EXISTS (SELECT 1 FROM OrderItems oi WHERE  oi.orderId = Orders.id and supplierUid = ".$this->id." and Orders.type = 'b2b' $condition)");
    }
    
    public function getOrderItems($orderId){
        
        return OrderItems::find()->where(['supplierUid' => $this->id, 'orderId' => $orderId]);
    }
    public function getOrderItemsMail($orderId){
        
        return OrderItems::find()->where(['supplierUid' => $this->id, 'orderId' => $orderId])->limit(10);
    }

    public function hasProducts($adminAddedOnly = false){
        if(isset(Yii::$app->params['temp']['suppliersWithProducts'])){
            $suppliersWithProducts = Yii::$app->params['temp']['suppliersWithProducts'];
        }else{
            $condition = ($adminAddedOnly)? " AND createdBy=0 " : "";
            $suppliersWithProducts = ArrayHelper::map(BrandSuppliers::find()->where("brandId in (SELECT id from Brands b WHERE EXISTS(SELECT 1 from Products p where b.id = p.brandId and status = 1 $condition))")->all(), 'supplierUid', 'supplierUid');
            Yii::$app->params['temp']['suppliersWithProducts'] = $suppliersWithProducts;
        }
        return in_array($this->id, $suppliersWithProducts);
    }

    public function getOrderItemsGrid($orderId){
        return \yii\grid\GridView::widget([
             'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getOrderItemsMail($orderId), 'sort' => false,'pagination'=> false,]),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
               [
                    'label'=> 'Product',
                    'attribute' => 'product.name',
                    'format' => 'html',
                ],
                [
                    'label' => 'Option(s)',
                    'value' => 'superAttributeValuesText',
                    'format' => 'html'
                ],
                [
                    'label'=> 'SKU',
                    'attribute' => 'product.sku',
                ],                
                [
                    'label'=> 'Item Price',
                    'format' => 'html',
                     'value' => function ($model) {
                        return Helper::money($model->price);
                    },
                ],
                [
                    'label' => 'Quantity',
                    'attribute' => 'quantity'
                ],
                [
                   'label' => 'Item Total',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return Helper::money($model->price * $model->quantity);
                    }
                   
                ],
            ],
        ]);
    }

    public function getFilterAttributes(){
        $query = Attributes::find()
        ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
        //->join('JOIN', 'ProductCategories pc', 'pc.productId = av.productId')
        ->join('JOIN', 'Products p', 'p.id = av.productId')
        ->join('JOIN', 'BrandSuppliers bs', 'bs.brandId = p.brandId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = p.id')
        //->join('JOIN', 'Products p2', 'sp.productId = p2.id')
        ->where(['bs.supplierUid' => $this->id, 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId'], 'sp.enabled' => '1']);
	else
            $query->andWhere('createdBy = 0');
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND av.value != ''")
        ->groupBy('Attributes.id');
        $attributes = $query->all();
        $filterAttributes = [];
        foreach ($attributes as $key=>$attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        } 
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code){
        $query = AttributeValues::find()
        ->join('JOIN', 'Attributes a', 'a.id = AttributeValues.attributeId')
        //->join('JOIN', 'ProductCategories pc', 'pc.productId = AttributeValues.productId')
        ->join('JOIN', 'Products p', 'p.id = AttributeValues.productId')
        ->join('JOIN', 'BrandSuppliers bs', 'bs.brandId = p.brandId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = p.id')
        //->join('JOIN', 'Products p2', 'sp.productId = p2.id')
        ->where(['bs.supplierUid' => $this->id, 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId'], 'sp.enabled' => '1',]);
	else
            $query->andWhere('createdBy = 0');
        $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
        ->groupBy('`AttributeValues`.`value`');
        return $query->all();
    }

    public function getProductsMaximumPrice(){
        $priceAttr = (Yii::$app->id=="app-b2b" || Yii::$app->id=="app-api")? "22" : "10";
        if($maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join Products p on av.productId = p.id join BrandSuppliers bs on bs.brandId = p.brandId where attributeId = $priceAttr and bs.supplierUid = ".$this->id." order by av.value+0 desc limit 1")->one())
            return $maxValue->value;
        else
            return 14000;
    }

    public function getProductsBrands(){
        $storeId = Yii::$app->params['storeId'];
        $query = \common\models\Brands::find();
        $brands = $query
                    ->from('Brands')
                    ->join('JOIN',
                        'Products as p',
                        'p.brandId = Brands.id'
                    )
                    ->join('JOIN',
                        'BrandSuppliers as bs',
                        'bs.brandId =  Brands.id'
                    );
                    if(Yii::$app->id != "app-b2b"){
                        $query->join('JOIN',
                            'StoreProducts as sp',
                            'sp.productId = p.id'
                        );
                    }    
            $query  ->groupBy('p.brandId')
                    ->andWhere("p.status = '1' and Brands.enabled = '1' and Brands.isVirtual = '0' and bs.supplierUid=".$this->id."");
                    if(Yii::$app->id != "app-b2b"){
                        $brands->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
                    }
                   // $brands->andWhere('(pc.storeId = "'.$storeId.'" OR pc.storeId = 0)');

        return $brands->all();            
    }

    public function disableBrands(){
        foreach($this->brands as $brand){
            if(count($brand->suppliers) == 1){ //only process if the brand has only this supplier
                $brand->enabled = '0';
                if($brand->save(false))
                    $brand->disableProducts();
            }
        }
    }

}
