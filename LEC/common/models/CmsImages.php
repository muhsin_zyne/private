<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cmsimages".
 *
 * @property integer $id
 * @property string $title
 * @property string $dateAdded
 */
class CmsImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CmsImages';
    }

    /**
     * @inheritdoc
     */
     public $image;
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['dateAdded'], 'safe'],
            [['image'], 'file','extensions'=>'jpg, gif, png'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'dateAdded' => 'Date Added',
        ];
    }
}
