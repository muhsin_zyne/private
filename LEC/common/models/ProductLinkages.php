<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductLinkages".
 *
 * @property integer $id
 * @property integer $productId
 * @property integer $parent
 * @property string $type
 */
class ProductLinkages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductLinkages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'parent', 'type'], 'required'],
            [['productId', 'parent'], 'integer'],
            [['type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Product ID',
            'parent' => 'Parent',
            'type' => 'Type',
        ];
    }

    public function getProduct(){
	$query = new \yii\db\ActiveQuery('\common\models\Products');
	$query->where(['id' => $this->productId, 'status' => '1']);
        return $query->one();
    }
}
