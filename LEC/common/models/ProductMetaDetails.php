<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "productmetadetails".
 *
 * @property integer $id
 * @property integer $productId
 * @property string $key
 * @property string $value
 * @property integer $position
 * @property integer $enabled
 */
class ProductMetaDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductMetaDetails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'productId', 'position', 'enabled'], 'safe'],
            [['key', 'value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Product ID',
            'key' => 'Key',
            'value' => 'Value',
            'position' => 'Position',
            'enabled' => 'Enabled',
        ];
    }
}
