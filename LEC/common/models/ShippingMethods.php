<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ShippingMethods".
 *
 * @property integer $id
 * @property string $title
 * @property string $normalPrice
 * @property string $threshold
 * @property string $aboveThreshold
 * @property integer $status
 */
class ShippingMethods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShippingMethods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'normalPrice', 'threshold', 'aboveThreshold'], 'required'],
            [['normalPrice', 'threshold', 'aboveThreshold'], 'number'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'normalPrice' => 'Normal Price',
            'threshold' => 'Threshold',
            'aboveThreshold' => 'Above Threshold',
            'status' => 'Status',
        ];
    }

    public function getCalculatedPrice($cartCost){
        if(isset(Yii::$app->session['appliedCoupons'])){
            if(in_array('free-shipping', array_values(Yii::$app->session['appliedCoupons']))){
                return 0;
            }
        }
        return ($cartCost >= $this->threshold)? $this->aboveThreshold : $this->normalPrice;
    }

    public function getDetail(){
        $threshold = Configuration::findSetting('shipping_discount_threshold');
        if(Yii::$app->cart->getCost(true) >= $this->threshold)
            $shippingCost = $this->aboveThreshold;
        else
            $shippingCost = $this->normalPrice;
        if(isset(Yii::$app->session['appliedCoupons'])){
            if(in_array('free-shipping', array_values(Yii::$app->session['appliedCoupons']))){
                $shippingCost = 0;
            }
        }
        return $this->title."(".\frontend\components\Helper::money($shippingCost)."):".money_format('%!n', $shippingCost);
    }

    public static function findShippingMethods(){
        if(ShippingMethods::find()->where(['storeId' => Yii::$app->params['storeId']])->exists()){
            return ShippingMethods::find()->where(['storeId' => Yii::$app->params['storeId']])->all();
        }else{
            return ShippingMethods::find()->where(['storeId' => '0'])->all();
        }
    }
}