<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "OrderItemChildren".
 *
 * @property integer $id
 * @property integer $parentProductId
 * @property integer $orderItemId
 * @property integer $productId
 * @property integer $quantity
 * @property string $type
 */
class OrderItemChildren extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderItemChildren';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parentProductId', 'orderItemId', 'productId', 'type'], 'required'],
            [['parentProductId', 'orderItemId', 'productId', 'quantity'], 'integer'],
            [['type'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parentProductId' => 'Parent Product ID',
            'orderItemId' => 'Order Item ID',
            'productId' => 'Product ID',
            'quantity' => 'Quantity',
            'type' => 'Type',
        ];
    }

    public function getProduct(){
        return $this->hasOne(\common\models\Products::className(), ['id' => 'productId']);
    }
}
