<?php

namespace common\models;
use frontend\components\Helper;
use Yii;
use yii\widgets\ListView;

/**
 * This is the model class for table "CreditMemos".
 *
 * @property integer $id
 * @property integer $orderId
 * @property string $comments
 * @property string $subTotal
 * @property string $shipping
 * @property string $grandTotal
 * @property string $createdDate
 */
class CreditMemos extends \yii\db\ActiveRecord
{
    public $reportFrom = null;
    public $reportTo = null;
    public $reportStoreId = 0;
    /**
     * @inheritdoc
     */
    public $chkmail;
    public static function tableName()
    {
        return 'CreditMemos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['orderId', 'subTotal', 'shipping', 'grandTotal'], 'required'],
            [['orderId'], 'integer'],
            [['comments'], 'string'],
            [['subTotal', 'shipping', 'grandTotal'], 'number'],
            [['createdDate', 'chkmail','notify'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Credit Memo #',
            'orderId' => 'Order #',
            'comments' => 'Comments',
            'subTotal' => 'Sub Total',
            'shipping' => 'Shipping',
            'grandTotal' => 'Grand Total',
            'createdDate' => 'Created Date',
        ];
    }

    public function getTotalCount(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND totalRefunded IS NOT NULL');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->count();
    }

    public function getTotalRefunded(){
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND totalRefunded IS NOT NULL');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        return $query->sum('totalRefunded');
    }

    public function getOrderIds()
    {  
        $query = Orders::find()->where('orderDate >= "'.$this->reportFrom.'" AND orderDate <= "'.$this->reportTo.'" AND totalRefunded IS NOT NULL');
        if($this->reportStoreId != 0)
            $query->andWhere("storeId = {$this->reportStoreId}");
        $orders = $query->all();
        return implode(", ", \yii\helpers\ArrayHelper::map($orders, 'id', 'id'));
        //return "hai";
    }       

    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }
    public function getCreditmemoId()
    {
        return str_pad($this->id,5,"0",STR_PAD_LEFT);
    }
    public function getCreditmemoPlacedDate()
    {
        return Helper::date($this->createdDate, "j F, Y");
    }
    public function sendCreditMemoMail(){    
        if($this->order->fromRegisteredUser()==true){        
            $user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        } else {             
            $userfullname=ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }        
        $creditmemo = \common\models\CreditMemos::findOne($this->id);  // only for date issue in email(time stamp).
        if(!empty($creditmemo->comments)) {
        $comments = '<tr>
              <td bgcolor="#f6f6f6" style="padding:15px;border:1px solid #eeeeee;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                        <tr>
                            <td style="font-size:13px;color:#5e5e5e;padding-bottom:6px;font-weight:bold;">
                                Comment:
                            </td>
                        </tr>
                        <tr>
                            <td style="font-style:italic;color:#737373;font-size:13px;">
                                <i>
                                    ' . $this->comments . '
                                </i>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>';
        }
        else {
            $comments='';
        }
        $template = \common\models\EmailTemplates::find()->where(['code' => 'userordercreditmemo'])->one();
        $store = \common\models\Stores::find()->where(['id' => $this->order->storeId])->one();
        $order = \common\models\Orders::find()->where(['id' => $this->orderId])->one();//var_dump($order);die;
        //$comments=($this->comments!='')?$this->comments:'N/A';
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['store'=>$store,'order'=>$order,'creditmemo'=>$creditmemo]; 
        $queue->replacements =[ 'logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage, 'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'comments'=>$comments,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$this->order->storeAddress,'comment'=>$comments,'grandTotal'=>number_format((float)$creditmemo->grandTotal, 2, '.', '')];            
            //var_dump($useremail);die;
        $queue->from = "no-reply";
        $queue->recipients =  $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }
    public function getCreditmemoItemsGrid()
    {
        $query = new \yii\db\ActiveQuery('common\models\CreditMemoItems');
        return $query->where("memoId=".$this->id."")->limit(20);
    }
    /*public function getShortGrid(){
        return \yii\grid\GridView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getInvoiceItemsGrid(), 'sort' => false,'pagination'=> false,]),
            //['class' => 'yii\grid\SerialColumn'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                //'orderItemId',
                [
                    'label'=> 'Product Name',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return $model->orderItem->product->name; 
                    }
                ],
                [
                    'label'=> 'SKU',
                    'attribute' => 'orderItem.sku',
                  
                ],
                [
                   'label' => 'Price',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money($model->orderItem->price);
                    },
                ],
                [
                    'label'=> 'Qty',
                    'attribute' => 'quantityRefunded',
                  
                ],
                // 'quantityRefunded',
                [
                   'label' => 'Item Total',
                    'format' => 'html',
                    'value' => function ($model) {
                        return Helper::money($model->orderItem->price * $model->quantityRefunded);
                    },
                ],
            ],
        ]);
    }*/
    public function getShortGrid(){
        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getCreditmemoItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/credit-memos/_maillistview',
        ]);
    }
    public function getCreditMemoItemTotalFormatted()
    {
        return Helper::money($this->subTotal);
    }
    public function getCreditMemoGrandTotalFormatted()
    {
        return Helper::money($this->grandTotal);
    }
    public function getCreditMemoShippingFormatted()
    {
        return Helper::money($this->shipping);
    }
    public function getPaymentDetails(){    
        $paymentgateway='';
        if($this->order->fromPaymentGateway!=NULL){        
            if($this->order->cardNumber!=NULL){            
                $paymentgateway='Credit Card Ending - '.$this->order->cardNumber;
            }            
        }        
        return $paymentgateway;
    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/credit-memo.jpg></a>'; 
    }
    public function getRefundItemCount(){
        $RefundItems=CreditMemoItems::find()->where(['memoId'=>$this->id])->all();
        $qty=0;
        foreach ($RefundItems as $item) {
           $qty=$qty+$item->quantityRefunded;
        }
        return $qty;
    }
    
    public function getRefundGrandTotal() {
        $RefundItems = CreditMemos::find()->where(['orderId' => $this->orderId])->all();
        if (!empty($RefundItems)) {
            $grandTotal1 = 0;
            foreach ($RefundItems as $item) {
                $grandTotal1 += $item->grandTotal;
                $grandTotal = ($this->order->grandTotal) - $grandTotal1;
            }
        } else
            $grandTotal = $this->order->grandTotal;

        return $grandTotal;
    }

    public function getRefundGrandTotalCheck() {
        $RefundItems = CreditMemos::find()->where(['orderId' => $this->orderId])->all();
        $grandTotal = 0;
        if (!empty($RefundItems)) {
            foreach ($RefundItems as $item) {
                $grandTotal += $item->grandTotal;
            }
        }

        return ($this->grandTotal) + $grandTotal;
    }

}
