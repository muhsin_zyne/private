<?php

namespace common\models;
use common\models\Stores;
use frontend\components\Helper;
use Yii;

/**
 * This is the model class for table "downloadpages".
 *
 * @property integer $id
 * @property string $title
 * @property string $filePath
 * @property integer $storeId
 * @property string $description
 * @property integer $status
 * @property integer $position
 * @property string $dateUpdated
 */
class Downloads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Downloads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'filePath', 'storeId', 'description'], 'required'],
            [['filePath', 'description'], 'string'],
            [['storeId', 'status', 'position'], 'integer'],
            [['dateUpdated'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['filePath'], 'file', 'extensions' => 'bmp, gif, jpg, jpeg, jpeg2000, raw, WebP, png, psd, pspimage, thm, tif, tiff, yuv, ai, drw, eps, ps, svg, pdf, xps, doc, docx, zip, rar, csv', 'maxSize' => 3145728, 'tooBig' => 'Image should be less than 3 MB.',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'filePath' => 'File Path',
            'storeId' => 'Store ID',
            'description' => 'Description',
            'status' => 'Status',
            'position' => 'Position',
            'dateUpdated' => 'Date Updated',
        ];
    }
    public function getStore(){
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }
    public function getFormatedDateUpdated(){
        return Helper::date($this->dateUpdated,'d-m-Y');
    }
    public function getFilePreview(){       
        if(preg_match("/\.(gif|png|jpg)$/", $this->filePath))
            return $this->filePath;
        else
            return '/store/download/file/file.png';
    }
    
}
