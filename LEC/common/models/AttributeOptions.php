<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "AttributeOptions".
 *
 * @property integer $id
 * @property string $value
 * @property integer $attributeId
 *
 * @property Attributes $attribute
 */
class AttributeOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AttributeOptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['attributeId'], 'required'],
            [['attributeId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'attributeId' => 'Attribute ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attributes::className(), ['id' => 'attributeId']);
    }

    public function getSelf(){
        return $this;
    }
}
