<?php

namespace common\models;
use frontend\components\Helper;
use Yii;
use common\models\ClientPortalProducts;
use common\models\Products;
use common\models\Stores;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

/**
 * This is the model class for table "OrderItems".
 *
 */

class ClientPortal extends \yii\db\ActiveRecord
{
    public $width;
    public $height;
    public $image;
    
	public static function tableName()
    {
        return 'ClientPortal';
    }

    public function rules()
    {
        return [
            [['schoolCode', 'payment_type','validFrom','expiresOn','phone','usage_count','organisation_name','address','shipment_type'], 'required'],
            [['studentCode'], 'required'],
            [['organisation_name','address','city','state'], 'required', 'when' => function ($model) {
              return $model->shipment_type == 'delivery-program';
            }, 'whenClient' => "function (attribute, value) {
              return $('#clientportal-shipment_type').val() == 'delivery-program';
            }"],
            [['schoolDeliverydate'], 'required', 'when' => function ($model) {
              return ($model->shipment_type == 'delivery-program');
            }, 'whenClient' => "function (attribute, value) {
              return ($('#clientportal-shipment_type').val() == 'delivery-program');
            }"],
            [['name', 'email','organisation_name','address','city','state','postcode'], 'string', 'max' => 255],
            [['id', 'storeId','phone'], 'integer'],
            [['studentCode','schoolCode'], 'string'],
            [['studentCode','schoolCode'], 'unique'],
            [['email'], 'email'],
            [['validFrom', 'expiresOn', 'schoolDeliverydate','name','studentAuthCode','schoolAuthCode','studentCode','schoolCode','quote'], 'safe'],
        ];
    }

    public function getProducts($storeId)
    {   
        //$product = new \common\models\Products(['byod' => true]);
        $product = new \common\models\Products;
        $query = $product::find();
        //var_dump($query->all());die();
        $products = $query
                    ->from('Products')
                    ->join('JOIN',
                        'ClientPortalProducts cpp',
                        'cpp.productId = Products.id'
                    )
                    ->join('JOIN',
                        'ClientPortal cp',
                        'cp.id = cpp.portalId'
                    );

       /* if(isset(Yii::$app->session['clientPortal'])){
            $products->join('JOIN',
                        'AttributeValues av',
                        'av.productId = Products.id'
                    )
                    ->join('JOIN',
                        'Attributes a',
                        'a.id = av.attributeId'
                    );
        }    */        

        $products->groupBy('cpp.productId')
                ->where("Products.status = '1' AND cpp.portalId={$this->id} AND cp.storeId = '$storeId'");
        
        /*if(isset(Yii::$app->session['clientPortal'])){
            $products->andWhere("a.code='byod_only'");
        }*/

        if(Yii::$app->id != "app-b2b")
            $query->andWhere("sp.storeId = '$storeId'"); 
        if(Yii::$app->id != "app-b2b"){
            $products->join('JOIN',
                        'StoreProducts as sp',
                        'sp.productId = Products.id'
                    );
            $products->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
            $products->andWhere('cpp.portalId = '.$this->id.'');
        }

        return $products;
    }

    public function getClientPortalProducts(){
        return $this->hasMany(ClientPortalProducts::className(), ['portalId' => 'id']);
        

        /*$query = \common\models\ClientPortalProducts::find();
        $products = $query
                    ->from('ClientPortalProducts')
                    ->join('JOIN',
                        'Products p',
                        'ClientPortalProducts.productId = p.id'
                    )
                    ->join('JOIN',
                        'ClientPortal cp',
                        'cp.id = ClientPortalProducts.portalId'
                    )
                    ->groupBy('ClientPortalProducts.productId')
                    ->where("p.status = '1' AND ClientPortalProducts.portalId=$this->id");

        return $products;    */        
    }

    public function getSelf(){
        return $this;
    }

    public function getValidFromFormatted(){
        return Helper::date($this->validFrom,"j F Y");
    }

    public function getExpiresOnFormatted(){
        return Helper::date($this->expiresOn,"j F Y");
    }

    public function getFilterAttributes(){
        $query = Attributes::find()
        ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
        ->join('JOIN', 'ClientPortalProducts cpp', 'cpp.productId = av.productId')
        ->join('JOIN', 'ClientPortal cp', 'cp.id = cpp.portalId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = cpp.productId')
        ->join('JOIN', 'Products p', 'sp.productId = p.id')
        ->where(['cp.id' => $this->id, 'sp.enabled' => '1', 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND av.value != ''")
        ->groupBy('Attributes.id');
        $attributes = $query->all();
        $filterAttributes = [];
        foreach ($attributes as $key=>$attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        } 
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code){
        $query = AttributeValues::find()
        ->join('JOIN', 'Attributes a', 'a.id = AttributeValues.attributeId')
        ->join('JOIN', 'ClientPortalProducts cpp', 'cpp.productId = AttributeValues.productId')
        ->join('JOIN', 'ClientPortal cp', 'cp.id = cpp.portalId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = cpp.productId')
        ->join('JOIN', 'Products p', 'sp.productId = p.id')
        ->where(['cp.id' => $this->id, 'sp.enabled' => '1', 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
        ->groupBy('`AttributeValues`.`value`');
        return $query->all();
    }

    public function getProductsMaximumPrice(){
        if($maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join ClientPortalProducts cpp on av.productId = cpp.productId where attributeId = 10 and cpp.portalId = ".$this->id." order by av.value+0 desc limit 1")->one())
            return $maxValue->value;
        else
            return 14000;
    }

    public function getProductsBrands(){
    	$storeId = Yii::$app->params['storeId'];
    	$portalType = (isset(Yii::$app->session['clientPortal'])) ? "clientPortal" : "byod";
        $query = \common\models\Brands::find();
        $brands = $query
                    ->from('Brands')
                    ->join('JOIN',
                        'Products as p',
                        'p.brandId = Brands.id'
                    )
                    ->join('JOIN',
                        'ClientPortalProducts as cpp',
                        'cpp.productId = p.id'
                    )
                    ->join('JOIN',
                        'ClientPortal as cp',
                        'cpp.portalId = cp.id'
                    );;
                    if(Yii::$app->id != "app-b2b"){
                        $query->join('JOIN',
                            'StoreProducts as sp',
                            'sp.productId = p.id'
                        );
                    }    
            $query  ->groupBy('p.brandId')
                    ->andWhere("p.status = '1' and Brands.enabled = '1' and Brands.isVirtual = '0'");
                    if(Yii::$app->id != "app-b2b"){
                        $brands->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
                    }

        return $brands->all();     
    }	

    public function sendCreationNotification(){
        $portalId = $this->id;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'portalcreationnotification'])->one();
        $portalProducts = \common\models\ClientPortalProducts::find()->where(['portalId' => $portalId])->one();
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $store = \common\models\Stores::find()->where(['id' => Yii::$app->user->identity->store->id])->one();
        elseif($user->roleId==1)
            $store = Stores::findOne($this->storeId);
        $useremail= $this->email;
        $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
        $queue = new \common\models\EmailQueue;
        $template->layout = "layout";
        $queue->models = ['portalProducts'=>$portalProducts,'portal'=>$this,'store'=>$store];

        $queue->replacements =['fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$store->logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->billingAddress,'clientPortalUrl'=>$store->siteUrl.'/client-portal/authenticate?token='.$this->studentAuthCode];

        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);

        //var_dump($queue->message);die();

        $queue->save(); 
    }

    public function getShortGrid(){ 
        /*return \yii\grid\GridView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getPortalProductsGrid(), 'sort' => false,'pagination'=> false,]),
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label'=> 'Product Name',
                    'attribute' => 'productId',
                    'headerOptions' => ['style' => 'width:50%'],
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return $model->product->name; 
                    }
                ],
                [
                    'label'=> 'SKU',
                    'value' => function($model, $attribute){
                        return $model->product->sku; 
                    }
                ],
                [
                    'label'=> 'Your Price',
                    'attribute' => 'offerPrice',
                ],
            ],
        ]);    */

        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getPortalProductsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => (Yii::$app->id == "frontend")?'/client-portal/_maillistview':'/client-portal/_maillistview',
        ]);
    }

    public function getPortalProductsGrid(){
        $storeId = Yii::$app->params['storeId'];
        $query = new \yii\db\ActiveQuery('common\models\ClientPortalProducts');
        $query->join('JOIN', 'StoreProducts sp', 'sp.productId = ClientPortalProducts.productId');
        $query->join('JOIN', 'Products p', 'p.id = ClientPortalProducts.productId');
        return $query->where("portalId=".$this->id." and sp.storeId=".$storeId." and p.status=1");
    }

    
    public function getProductTotalPrice(){
        $portalProducts = \common\models\ClientPortalProducts::find()->where(['portalId' => $this->id])->all();
        $total = 0;
        foreach ($portalProducts as $product) {
            $total = $total+$product->offerPrice;        
        }
        return Helper::money($total);
    }

    public function getPortalCreationBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/quote-client-portal-bnr.jpg></a>'; 
    }

    public function hasOrders(){
        if(\common\models\Orders::find()->where(['portalId' => $this->id])->one())
            return true;
        else
            return false;
    } 

    public function getSchoolAddress(){
        return '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">'.$this->organisation_name.'</p><p style="margin:0;margin-bottom:10px;color:#8c8c7b;">'.$this->address.'</p><p style="margin:0;color:#8c8c7b;margin-bottom:10px;">'.$this->city.' '.$this->state.' '.$this->postcode.'</p><p style="margin:0;color:#8c8c7b;margin-bottom:10px;">Phone: '.$this->phone;
    }

    public function isDeliveredToday()
    {
        if($this->schoolDeliverydate <= date('Y-m-d H:i:s'))
            return true;
        else
            return false;
    }        
}

?>
