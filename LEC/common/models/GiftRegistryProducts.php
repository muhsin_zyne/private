<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "giftregistryproducts".
 *
 * @property integer $id
 * @property integer $productId
 * @property integer $giftRegistryId
 * @property integer $purchased
 * @property integer $created_at
 */
class Giftregistryproducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GiftRegistryProducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'giftRegistryId', 'purchased'], 'integer'],
            ['created_at','safe'],
        ];
    }
    
    public function getProduct() {
        return $this->hasOne(Products::className(), ['id' => 'productId']);
    }
    
     public function getGiftregistry() {
        return $this->hasOne(GiftRegistry::className(), ['id' => 'giftRegistryId']);
    }

    
    public function getSelf() {
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Product ID',
            'giftRegistryId' => 'Gift Registry ID',
            'purchased' => 'Purchased',
            'created_at' => 'Created At',
        ];
    }
}
