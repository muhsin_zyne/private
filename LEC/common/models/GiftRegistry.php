<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
//use backend\components\SquareValidator;

/**
 * This is the model class for table "giftregistry".
 *
 * @property integer $id
 * @property string $brideName
 * @property string $groomName
 * @property string $contact
 * @property integer $email
 * @property integer $phone
 * @property string $address
 * @property integer $city
 * @property integer $state
 * @property integer $postalCode
 * @property string $blurb
 * @property string $weddingDate
 * @property string $activeFrom
 * @property string $expiresOn
 * @property integer $urlKey
 * @property integer $image
 * @property string $created_at
 * @property string $updated_at
 */
class GiftRegistry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GiftRegistry';
    }
    
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
               'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brideName', 'groomName', 'contact', 'email', 'phone', 'address', 'city', 'state', 'postalCode', 'blurb', 'urlKey','activeFrom','expiresOn','urlKey'], 'required'],
            [['phone', 'postalCode'], 'integer'],
            [['address', 'blurb','urlKey'], 'string'],
            ['email','email'],
            [['image'], 'image', 'extensions'=>'jpg, gif, png','checkExtensionByMimeType'=>false,'skipOnEmpty' => false, 'maxSize' => 1024 * 1024 * 2, 'on' => 'upload'],
            //'minWidth' => 224, 'maxWidth' => 224,'minHeight' => 224, 'maxHeight' => 224,
            ['activeFrom','validateDates'],
            ['urlKey', 'unique'],
            //['urlKey','url'],
            [['storeId','email','urlKey','weddingDate', 'activeFrom', 'expiresOn', 'created_at', 'updated_at','image'], 'safe'],
            [['brideName', 'groomName', 'contact'], 'string', 'max' => 255]
        ];
    }
    
    public function getGiftregistryProducts() {
        return $this->hasMany(Giftregistryproducts::className(), ['giftRegistryId' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brideName' => 'Name of Bride ',
            'groomName' => 'Name of Groom',
            'contact' => 'Primary Contact',
            'email' => 'Email',
            'phone' => 'Phone (No dash or space please)',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'postalCode' => 'Postal Code',
            'blurb' => 'Blurb',
            'weddingDate' => 'Wedding Date',
            'activeFrom' => 'Active From',
            'expiresOn' => 'Expires On',
            'urlKey' => 'Url (Only Tail)',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function validateDates() {
        if (strtotime($this->expiresOn) <= strtotime($this->activeFrom)) {
            $this->addError('activeFrom', 'Please give correct Start and End dates');
            $this->addError('expiresOn', 'Please give correct Start and End dates');
        }
    }
    
    public function getGiftProducts($storeId, $type = null) {   

        $query = new ActiveQuery('common\models\Products');
        $products = $query
                ->from('Products')
                ->join('JOIN', 'Brands as b', 'b.id = Products.brandId'
        );
            $query->join('JOIN', 'StoreProducts as sp', 'sp.productId = Products.id'
            );
        $query->groupBy('Products.id')
                ->where("Products.status = '1' AND Products.brandId = " . $this->id);
        if (Yii::$app->id != "app-b2b")
            $query->andWhere('sp.storeId = ' . $storeId . '');

        if (isset($type))
        //$products->andWhere('ss.type="'.$type.'" AND sp.type="'.$type.'"');
            $products->andWhere('sp.type="' . $type . '"');
        return $products;
    }
    
      public function getFilterAttributes() {
        $query = Attributes::find()
                ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
                //->join('JOIN', 'ProductCategories pc', 'pc.productId = av.productId')
                ->join('JOIN', 'Products p', 'p.id = av.productId')
                ->join('JOIN', 'GiftRegistryProducts gp', 'p.id = gp.productId');
                //->join('JOIN', 'GiftRegistry gr', 'gr.id = '.$this->id)
                //->join('JOIN', 'GiftRegistryProducts gp', 'gr.id = gp.giftRegistryId');
                //->join('JOIN', 'Products p2', 'sp.productId = p2.id')
                //->where(['gp.productId' => $this->id]);
//        if (Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
//            $query->andWhere(['gp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere(['gp.giftRegistryId'=>$this->id,'gp.purchased'=>0]);
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND av.value != ''")
                ->groupBy('Attributes.id');
        $attributes = $query->all();
        $filterAttributes = [];
        foreach ($attributes as $key => $attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        }
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code) {
        $query = AttributeValues::find()
                ->join('JOIN', 'Attributes a', 'a.id = AttributeValues.attributeId')
                //->join('JOIN', 'ProductCategories pc', 'pc.productId = AttributeValues.productId')
                ->join('JOIN', 'Products p', 'p.id = AttributeValues.productId')
                //->join('JOIN', 'Products p', 'p.id = av.productId')
                ->join('JOIN', 'GiftRegistryProducts gp', 'p.id = gp.productId');
        //->join('JOIN', 'Products p2', 'sp.productId = p2.id')
                //->where(['gp.productId' => $this->id]);
//        if (Yii::$app->id != "app-b2b" && Yii::$app->id != "app-api")
//            $query->andWhere(['gp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere(['gp.giftRegistryId'=>$this->id,'gp.purchased'=>0]);
        $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
                ->groupBy('`AttributeValues`.`value`');
        
        return $query->all();
    }

    public function getProductsMaximumPrice() {
        $priceAttr = (Yii::$app->id == "app-b2b" || Yii::$app->id == "app-api") ? "22" : "10";
        if ($maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join Products p on av.productId = p.id join GiftRegistryProducts gp on gp.productId = p.id where attributeId = $priceAttr and gp.giftRegistryId= $this->id and gp.purchased=0  order by av.value+0 desc limit 1")->one())
            return $maxValue->value;
        else
            return 14000;
    }

}
