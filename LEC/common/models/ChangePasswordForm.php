<?php 
  
namespace common\models;
use Yii;
use common\models\User;
use yii\base\Model;
    
    class ChangePasswordForm extends Model{
        public $oldpass;
        public $newpass;
        public $repeatnewpass;
        
        public function rules(){
            return [
                [['oldpass','newpass','repeatnewpass'],'required', 'whenClient' => "function (attribute, value) {
                    if( $('.change_password').length ){
                        return $('.change_password').val() == '1';
                    }    
                }"],
                ['oldpass','findPasswords'],
                ['repeatnewpass','compare','compareAttribute'=>'newpass'],
                [['newpass'],'string','min'=>6,'max'=>40],
            ];
        }
        
        public function findPasswords($attribute, $params){
            $user = User::find()->where([
                'id'=>Yii::$app->user->id])->one();
            if (!$user->validatePassword($this->oldpass)) {
                $this->addError($attribute, 'Old password is incorrect.');
            }
        }
        
        public function attributeLabels(){
            return [
                'oldpass'=>'Old Password',
                'newpass'=>'New Password',
                'repeatnewpass'=>'Repeat New Password',
            ];
        }
    }