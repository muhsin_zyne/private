<?php

namespace common\models;
use yii\widgets\ListView;
use Yii;

/**
 * This is the model class for table "b2baddresses".
 *
 * @property integer $id
 * @property integer $ownerId
 * @property string $storeName
 * @property string $streetAddress
 * @property string $city
 * @property string $state
 * @property string $country
 * @property integer $postCode
 * @property integer $phone
 * @property string $latitude
 * @property string $longitude
 * @property string $tradingHours
 * @property integer $defaultAddress
 */
class B2bAddresses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'B2bAddresses';
    }

    /**
     * @inheritdoc
     */
    public $mondayFrom;
    public $mondayTo;
    public $tuesdayFrom;
    public $tuesdayTo;
    public $wednesdayFrom;
    public $wednesdayTo;
    public $thursdayFrom;
    public $thursdayTo;
    public $fridayFrom;
    public $fridayTo;
    public $saturdayFrom;
    public $saturdayTo;
    public $sundayFrom;
    public $sundayTo;
    public function rules()
    {
        return [
            [['ownerId', 'postCode',  'defaultAddress'], 'integer'],
            [['storeName', 'streetAddress', 'city', 'state', 'country', 'postCode', 'phone', 'latitude', 'longitude','ownerId','defaultAddress'], 'required'],
            [['streetAddress', 'latitude', 'longitude', 'tradingHours'], 'string'],
            [['storeName', 'city', 'state', 'country'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() 
    {
        return [
            'id' => 'ID',
            'ownerId' => 'Owner ID',
            'storeName' => 'Store Name',
            'streetAddress' => 'Street Address',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'postCode' => 'Post Code',
            'phone' => 'Phone',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'tradingHours' => 'Trading Hours',
            'defaultAddress' => 'Default Address',
        ];
    }
    public function getBillingAddress(){    
        return $this->streetAddress."<br/>".$this->city.", ".$this->postCode."<br/>".$this->state.", ".$this->country;
    }

    public function getShippingAddress(){       
        return $this->streetAddress."<br/>".$this->city.", ".$this->postCode."<br/>".$this->state.", ".$this->country;
    }

    public function getAddressAsText(){
        return $this->streetAddress.", ".$this->city.", ".$this->postCode.", ".$this->state.", ".$this->country;
    }
    public function getBillingAddressAsText(){ // for email google map only @ aaron    
        return $this->storeName." ".$this->streetAddress." ".$this->city." ".$this->state."  ".$this->country;
    }
    public function getAddressForDropdown(){ // for email google map only @ aaron
        return $this->storeName.", ".$this->streetAddress.", ".$this->city." ".$this->state."  ".$this->postCode.", ".$this->country.", "."Phone number: ".$this->phone;
    }
    
    public function getB2bTradingHoursFormatted(){
        return  json_decode($this->tradingHours, true);        
    }
    public function getShortGrid(){
        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getTradingHoursGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/orders/_timeListView',
        ]);
    }
    public function getTradingHoursGrid(){    
        $query = new \yii\db\ActiveQuery('common\models\B2bAddresses');
        return $query->where("id=".$this->id."");
    }
}
