<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductSuperAttributes".
 *
 * @property integer $id
 * @property integer $productId
 * @property integer $attributeId
 * @property integer $position
 */
class ProductSuperAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductSuperAttributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'attributeId'], 'required'],
            [['productId', 'attributeId', 'position'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Product ID',
            'attributeId' => 'Attribute ID',
            'position' => 'Position',
        ];
    }

    public function getAttributeOptionPrices(){
        //return $this->hasMany(AttributeOptionPrices::className(), ['attributeId' => 'attributeId'])->andWhere('productId', $this->productId)->all();
        return $this->hasMany(AttributeOptionPrices::className(), ['attributeId' => 'attributeId'])->onCondition('productId = "'.$this->productId.'"');
    }

    public function getAttr(){
        return $this->hasOne(Attributes::className(), ['id' => 'attributeId']);
    }

}
