<?php

namespace common\models;
use frontend\components\Helper;
use yii\helpers\ArrayHelper;
use Yii;
use yii\widgets\ListView;

/**
 * This is the model class for table "deliveries".
 *
 * @property integer $id
 * @property integer $orderId
 * @property integer $notify
 * @property string $deliveryDate
 */
class Deliveries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Deliveries';
    }

    /**
     * @inheritdoc
     */

    public $shippedItems;
    public function rules()
    {
        return [
            [['orderId','deliveryDate','verifiedBy','collectedBy','phone','idProofNumber','comments'], 'required'],
            [['orderId', 'notify'], 'integer'],
            [['comments'], 'string'],
            [['deliveryDate','idProofNumber','verifiedBy','comments','collectedBy','phone','paymentType','amount'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderId' => 'Order ID',
            'notify' => 'Notify',
            'deliveryDate' => 'Collection Date',
            'idProofNumber'=>'License Number',
            'verifiedBy'=>'Verified By',
            'amount' => 'Amount Paid',
            'paymentType' => 'Payment Type',
            'comments'=>'Comments',
        ];
    }
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }

    public function getDeliveryItems(){
        return $this->hasMany(\common\models\DeliveryItems::className(), ['deliveryId' => 'id']);
    }

    public function sendDeliveryMail()
    {
        if($this->order->fromRegisteredUser()==true)
        {
            $user = \common\models\User::find()->where(['id' => $this->order->customerId])->one();
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        }
        else
        {
            $userfullname=ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }
        $delivery = $this;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'userorderdelivery'])->one();
        //$user = \common\models\User::find()->where(['id' => $shipment->order->customerId])->one();
        $store = \common\models\Stores::find()->where(['id' => $delivery->order->storeId])->one();
        $order = \common\models\Orders::find()->where(['id' => $delivery->orderId])->one();
        //var_dump($shipment);die;
        $queue = new \common\models\EmailQueue;
         $template->layout = "layout";
        $queue->models = ['store'=>$store,'order'=>$order,'delivery'=>$delivery]; 
        $queue->replacements =[ 'logo'=>$store->logo,'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$this->order->storeAddress];  

        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        $queue->save();
    }
    /*public function getShortGrid(){
        return \yii\grid\GridView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getDeliveryItemsGrid(), 'sort' => false,'pagination'=> false,]),
            //['class' => 'yii\grid\SerialColumn'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                // /'id',
                [
                    'label'=> 'Product Name',
                    'format' => 'html',
                    'value' => function($model, $attribute){
                        return $model->orderItem->product->name; 
                    }
                ],
                [
                    'label'=> 'SKU',
                    'attribute' => 'orderItem.sku',
                  
                ],
               
                'quantityDelivered',
               
            ],
        ]);
    }*/
    public function getShortGrid(){
        return ListView::widget([
             'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getDeliveryItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/delivery/_maillistview',
        ]);
    }

    public function getPdfShortGrid(){
        return ListView::widget([
             'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getDeliveryItemsGrid(), 'sort' => false,'pagination'=> false,]),
            'itemView' => '/delivery/_pdflistview',
            //'itemView' => (Yii::$app->id == "frontend")?'/orders/_invoicelistview':'/invoices/_maillistview',
        ]);
    }

    public function getDeliveryItemsGrid()
    {
        $query = new \yii\db\ActiveQuery('common\models\DeliveryItems');
        return $query->where("deliveryId=".$this->id."")->limit(10);
    }
    public function getDeliveryPlacedDate()
    {
        return Helper::date($this->deliveryDate, "j F, Y");
    }
    public function getDeliveryId()
    {
        return str_pad($this->id,5,"0",STR_PAD_LEFT);
    }
    public function getMailBanner(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/collection-advice.jpg></a>'; 
    }
    public function getDeliveryItemCount(){
        $DeliveryItems=DeliveryItems::find()->where(['deliveryId'=>$this->id])->all();
        $qty=0;
        foreach ($DeliveryItems as $item) {
           $qty=$qty+$item->quantityDelivered;
        }
        return $qty;
    }
}
