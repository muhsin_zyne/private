<?php 

namespace common\models;
use common\models\Stores;
use common\models\Categories;
use yii\helpers\Url;
use yii\helpers\Html;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property integer $storeId
 * @property integer $parent
 * @property integer $position
 */
class Menus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Menus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'path'], 'required'],
            [['path'], 'string'],
            [['storeId', 'position','type','htmlAttributes'], 'safe'],
            [['storeId', 'parent', 'position'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'path' => 'Path',
            'storeId' => 'Store Name',
            'parent' => 'Parent',
            'position' => 'Position',
        ];
    }
    public function getParentMenus(){
        return $this->hasMany(Menus::className(), ['parent' => $this->id]);
    }

    public static function menuDelete($menulist){  //insert default menu storeid to menu     
        $menu_delete_array=Menus::find()->where( [ 'lastInsertId' => $menulist['id']] )->all();
        foreach ($menu_delete_array as $index => $menu_delete) {
            $menu_pdelete=Menus::find()->where( [ 'id' => $menu_delete['id'] ])->one();
            $menu_pdelete->delete();
        }
    }
    public static function menuUpdateAllStores($menulist) {
        $allstores=Stores::find()->where(['isVirtual'=>0])->all();
        foreach ($allstores as $key => $store) 
        {
            // if($menulist->parent!=0)
            // {
            //     $menu_default_parent= Menus::find()->where(['storeId' => 0,'parent' => $menulist->parent])->one();
            //     var_dump($menu_default_parent);die;
            //     $menu_store_parent=Menus::find()->where(['storeId' => $store->id,'path' => $menu_default_parent->path])->one(); 
            //     //var_dump($menu_store_parent->id);die;
            // }
            $menu= Menus::find()->where(['storeId' => $store->id,'path' => $menulist->path])->one(); 
            if(isset($menu))
            {
               $menu->position=$menulist->position;
               $menu->save();
            }
        }
    }
    public static function menuInsert($menulist){  //insert default menu storeid to menu 
        //var_dump($menulist->parent);die;
        $allstores=Stores::find()->where(['isVirtual'=>0])->all();
        $menu_parent=Menus::find()->where(['id' => $menulist->parent,'storeId' => 0])->one();
        foreach ($allstores as $key => $store) { 
            $parent=0;
            if($menulist->parent==0){
                $parent=0;
            } else {
                $menus= Menus::find()->where(['storeId' => $store['id'],'path' => $menu_parent->path])->one(); 
                if($menus!=''){
                    $parent=$menus->id;
                } 
            }
            $menu_model = new Menus();
            $menu_model->title=$menulist['title'];
            $menu_model->path=$menulist['path'];
            $menu_model->storeId=$store['id'];
            $menu_model->parent=$parent;
            $menu_model->position=$menulist['position'];
            $menu_model->lastInsertId=$menulist['id'];
            $menu_model->save();
        }            
    }
    
    public static function createCategoryMenuItem($model_cat) { //categories is created insert that id to all stores and default
       //var_dump('hiii');die;
        $main_categories= Categories::find()->where(['id' => $model_cat->parent])->one();
        //$menus= Menus::find()->where(['storeId' => 0,'path' => $main_categories->urlKey])->one();
        if($main_categories->parent==0){ 
            $menus= Menus::find()->where(['storeId' => 0,'path' => $main_categories->urlKey])->one();  
            if($menus=='') { 
                $menu_model = new Menus();
                $menu_model->title=$main_categories->title;
                $menu_model->path=$main_categories->urlKey;
                $menu_model->storeId=0;
                $menu_model->parent=11;
                $menu_model->position=0;
                $menu_model->categoriesId=$main_categories->id;
                $menu_model->save(false);
                $parent=$menu_model->id;
                $position=0;                       
            } else {
                $menu_model = new Menus();
                $menu_model->title=$model_cat->title;
                $menu_model->path=$model_cat->urlKey;
                $menu_model->storeId=0;
                $menu_model->parent=12;
                $menu_model->position=0;
                $menu_model->categoriesId=$model_cat->id;
                $menu_model->save(false);
                $parent=$menu_model->id;
                $position=0;  
            }   
        } else {
            $categories= Categories::find()->where(['id' => $main_categories->parent])->one();
            $menus= Menus::find()->where(['storeId' => 0,'path' => $categories->urlKey])->one();
            if($menus=='') {  
                $menu_model = new Menus();
                $menu_model->title=$categories->title;
                $menu_model->path=$categories->urlKey;
                $menu_model->storeId=0;
                $menu_model->parent=13;
                $menu_model->position=0;
                $menu_model->categoriesId=$categories->id;
                $menu_model->save(false);
                $parent=$menu_model->id;
                $position=0;                       
            } 
        }
    }
    public static function deleteCategoryMenuItem($model_cat) {  //categories is deleted delete that id to menu tables  
        $menu_delete_catid_array=Menus::find()->where( [ 'categoriesId' => $model_cat->id] )->all();
        foreach ($menu_delete_catid_array as $index => $menu_delete) {
            //echo $menu_delete['id'];
            $menu_pdelete=Menus::find()->where( [ 'id' => $menu_delete['id'] ])->one();
            $menu_pdelete->delete();
        }
    }
    public static function demoMenucreate() // Create menu by all stores
    {
       /* $categories=Categories::find()->orderBy('parent ASC')->all();
        
        foreach ($categories as $key => $categorie)
        {
            $parent=0; 
            $store_default_lastinsert_id=0;
            if($categorie->parent==0)
            {
                $parent=0;
            }
            else
            {   
                $categories_p=Categories::find()->where(['id' => $categorie->parent])->one(); 
                $menus= Menus::find()->where(['storeId' => 0,'path' =>$categories_p->urlKey.'.html'])->one();
                $parent=$menus->id;
            }
            if($categorie->title!='Root')
            {
                //var_dump($categorie->urlKey);die;
                //s$menu_urlkey=explode('.html', $categorie->urlKey);
                $menus_check= Menus::find()->where(['storeId' => 0,'categoriesId' => $categorie->id,'path'=>$categorie->urlKey.'.html'])->one();
                //var_dump($menus_check);die;
                if(empty($menus_check))
                {
                    //var_dump($parent);die;
                    $menu_model = new Menus();
                    $menu_model->title=$categorie->title;
                    $menu_model->path=$categorie->urlKey.'.html';
                    $menu_model->storeId=0;
                    $menu_model->parent=$parent;
                    $menu_model->position=0;
                    $menu_model->categoriesId=$categorie->id;
                    $menu_model->save(false);
                    $store_default_lastinsert_id=$menu_model->id;
                }
            }  
            $stores=Stores::find()->where(['isVirtual'=>0])->all();
            foreach ($stores as $key => $store) 
            {

                $parent=0; 
                if($categorie->parent==0)
                {
                    $parent=0;
                }
                else
                {   
                    $categories_p=Categories::find()->where(['id' => $categorie->parent])->one();
                    //$menu_urlkey=explode('.html', $categories_p->urlKey);
                    $menus= Menus::find()->where(['storeId' =>$store->id,'path' => $categories_p->urlKey.'.html'])->one();
                    $parent=$menus->id;
                }
                if($categorie->title!='Root')
                {
                    $menus_check= Menus::find()->where(['storeId' =>$store->id,'categoriesId' => $categorie->id,'path'=>$categorie->urlKey.'.html'])->one();
                    if(empty($menus_check))
                    { 
                        //var_dump($store_default_lastinsert_id);die;
                        $menu_model = new Menus();
                        $menu_model->title=$categorie->title;
                        $menu_model->path=$categorie->urlKey.'.html';
                        $menu_model->storeId=$store->id;
                        $menu_model->parent=$parent;
                        $menu_model->position=0;
                        $menu_model->categoriesId=$categorie->id;
                        $menu_model->lastInsertId=$store_default_lastinsert_id;
                        $menu_model->save(false);
                    }
                }
            }
        }*/
    }
    public static function storeMenucreate($storeId){ // create menus dynamically by individual stores    
        $categories=Categories::find()->orderBy('parent ASC')->all();
        foreach ($categories as $key => $categorie){ 
            $parent=0; 
            $store_default_lastinsert_id=0;
            if($categorie->parent==0){
                $parent=0;
            } else {
                $categories_p=Categories::find()->where(['id' => $categorie->parent])->one(); 
                $menus= Menus::find()->where(['storeId' => $storeId,'path' =>$categories_p->urlKey.'.html'])->one();
                $parent=$menus->id;
            }
            if($categorie->title!='Root'){
                $menus_check= Menus::find()->where(['storeId' => $storeId,'categoriesId' => $categorie->id,'path'=>$categorie->urlKey.'.html'])->one();
                if(empty($menus_check)) {
                    $model = new Menus();
                    $model->title=trim($categorie->title);
                    $model->path=trim($categorie->urlKey).'.html';
                    $model->storeId=$storeId;
                    $model->parent=$parent;
                    $model->position=0;
                    $model->unAssigned=0;
                    $model->type='main';
                    $model->categoriesId=$categorie->id;
                    $model->save(false);
                }
            } 
        }
        // test unassigned
        /*$model = new Menus();
        $model->title='Testmenu';
        $model->path='test_menu.html';
        $model->storeId=$storeId;
        $model->parent=0;
        $model->position=0;
        $model->unAssigned=1;
        $model->categoriesId=0;
        $model->type='main';
        $model->save(false); */
    }
    public static function storeHeadermenucreate($storeId) { // create menus dynamically by individual stores    
        $menu_array=['Home','About Us','Promotions','Gift Vouchers','Brands'];
        $menu_array= array(
            array('title'=>'Home', 'path'=>'site/index'),
            array('title'=>'About Us', 'path'=>'about-us.html'),
            array('title'=>'Promotions', 'path'=>'#'),            
            array('title'=>'Gift Vouchers', 'path'=>'gift-vouchers.html'),
            array('title'=>'Brands', 'path'=>'brands'),           
        );        
        foreach ($menu_array as $value) {
            $menus_check=Menus::find()->where(['storeId' => $storeId,'title' =>$value['title'],'type'=>'header' ])->one();
            if(empty($menus_check)){
                $model = new Menus();
                $model->title=$value['title'];
                $model->path=$value['path'];
                $model->storeId=$storeId;
                $model->parent=0;
                $model->position=0;
                $model->unAssigned=0;
                $model->type='header';
                $model->categoriesId=0;
                $model->save(false);
            }           
        }       
    }
    public static function storeFootermenucreate($storeId) {// create menus dynamically by individual stores    
        $menu_array= array(
            array('title'=>'About Us', 'path'=>'#'),
            array('title'=>'Shop', 'path'=>'#'),
            array('title'=>'Support', 'path'=>'#'),
            array('title'=>'Brands', 'path'=>'brands'),
            array('title'=>'Our Story', 'path'=>'about-us.html'),
            array('title'=>'Services', 'path'=>'services.html'),
            array('title'=>'Login', 'path'=>'site/login'),
            array('title'=>'Online Gift Vouchers', 'path'=>'gift-vouchers.html'),
            array('title'=>'Sign Up', 'path'=>'site/signup'),
            array('title'=>'Contact Us', 'path'=>'site/contact'),
            array('title'=>'Delivery & Returns', 'path'=>'delivery-returns.html'),
            array('title'=>'FAQ', 'path'=>'faq.html'),
            array('title'=>'Privacy & Security', 'path'=>'privacy.html'),
            array('title'=>'Terms & Condition', 'path'=>'terms-condition.html'),
            array('title'=>'Warranty', 'path'=>'warranty.html'),
        );        
        //$model = new Menus();
        foreach ($menu_array as $value) {
            $menus_check=Menus::find()->where(['storeId' => $storeId,'title' =>$value['title'],'type'=>'footer' ])->one();
            if(empty($menus_check)){
                $model = new Menus();
                $model->title=$value['title'];
                $model->path=$value['path'];
                $model->storeId=$storeId;
                $model->parent=0;
                $model->position=0;
                $model->unAssigned=0;
                $model->type='footer';
                $model->categoriesId=0;
                $model->save(false);
            }          
        }       
    }
    public function getCategories(){
        return $this->hasOne(Categories::className(), ['id' => 'categoriesId']);
    }
    /* 
        @ aaron  20/09/2016
        menu list 
    */
    public function getItems($items_new,$parent){
        $newelements = array();
        foreach ($items_new as $newitem) {
            if($newitem['id'] != $parent && $newitem['parent'] == $parent) {
                $newelements[] =
                    [   'label' => $newitem['title'],
                        'url' => $newitem['path'],
                        'id' => $newitem['id'],
                        'type'=>$newitem['type'],                       
                        //'icon'=>($newitem['categoriesId']!=0)?$newitem->categories->icon:'',
                        'categoriesId'=>$newitem['categoriesId'],
                        'items' => $this->getItems($items_new,$newitem['id']),
                    ];
            }
        }
        return $newelements; 
    }   
    public function getMenuRecrusive($data,$child=FALSE){
        $html='<ol class="dd-list">';  
            foreach($data as $item){ //var_dump($item);
                if(isset($item['items'])) { //var_dump($item['categoriesId']);
                    $html.= '<li class="dd-item dd3-item" data-id="'.$item['id'].'">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content">'.$item['label'].'';
                        $html.='<u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/menus/updatemenu','id'=>$item['id'],'type'=>$item['type']],['class'=>'bootstrap-modal','data-title' => 'Update Menu',]).'</u>
                            <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/menus/delete', 'id' => $item['id']], ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]).'</u>';
                    $html.='</div>';
                    $html.= $this->getMenuRecrusive($item['items'],TRUE); 
                    $html.= '</li>';  
                }  
            }
        $html.='</ol>';                           
    return $html;
    }

    public function getMenuList($items){
       $elements = [];
        foreach ($items as $item) { 
            if($item['parent']=='0'){
                $elements[] =
                    [   'label' => $item['title'],
                        'url' => $item['path'],
                        'icon'=>(isset($item->categories->id))?$item->categories->icon:'',
                        'id' => $item['id'],
                        'type'=>$item['type'],
                        'categoriesId'=>$item['categoriesId'],
                        'items' => $this->getItems($items,$item['id']),
                    ];
            }
        }
        $result = $this->getMenuRecrusive($elements,FALSE);
        return $result;
    }
    /*
        @ aaron 21/09/2016 
        menu position change 
    */
    public function getMenuPositionChange($ass_menu_array,$storeId,$type,$status,$parent=NULL){
        $parent=(isset($parent))? $parent : 0;
        $position=0;
        foreach ($ass_menu_array as $key => $ass_menu) { 
            $menu = Menus::findOne($ass_menu['id']);
            $menu->storeId = $storeId;  
            $menu->parent = $parent;
            $menu->position = $position;
            $menu->unAssigned= $status;
            $menu->type=$type;
            $menu->save(false);
            $position++;
            if(isset($ass_menu['children']))
                $this->getMenuPositionChange($ass_menu['children'],$storeId,$type,$status,$menu['id']);
        }
        return true;
    }
    public static function categoryMenuCreate($categorie){ //individual category menu created
        $stores=Stores::find()->where(['isVirtual'=>0])->all();
        $storeArray="Menu created sites : ";
        foreach ($stores as  $store) {
            $menus_check=Menus::find()->where(['storeId' => $store->id,'categoriesId' =>$categorie->id])->one();
            //var_dump($menus_check);die;
            if(empty($menus_check)){
                $model = new Menus();
                $model->title=trim($categorie->title);
                $model->path=trim($categorie->urlKey).'.html';
                $model->storeId=$store->id;
                $model->parent=0;
                $model->position=0;
                $model->unAssigned=0;
                $model->type='main';
                $model->categoriesId=$categorie->id;
                $model->save(false);
                $storeArray=$storeArray.'<br/>'.$store->title;
            }
        }
        return $storeArray;
    }   
}
