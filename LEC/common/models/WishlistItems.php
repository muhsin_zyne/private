<?php

namespace common\models;
use Yii;
use yii\helpers\Html;
use frontend\components\Helper;

/**
 * This is the model class for table "WishlistItems".
 *
 * @property integer $id
 * @property integer $Products_id
 * @property integer $Stores_id
 */
class WishlistItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'WishlistItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productId', 'storeId','userId'], 'required'],
            [['productId', 'storeId','userId'], 'integer'],
            [['comments'], 'safe'],  
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productId' => 'Products ID',
            'storeId' => 'Stores',
            'userId' => 'User ID',
            'comments' => 'COMMENT',
            'createdDate' => 'Added on',
            'storeName' => Yii::t('app', 'STORE NAME'),
            'fullName' => Yii::t('app', 'Full Name'),
            'attributeSetName' => Yii::t('app', 'Product Name'),
            'wliDate' => Yii::t('app', 'Created Date')
        ];
    }
    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'productId']);
    }
    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
    public function getStore(){
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }
    public function getStoreName() {
        return $this->store->title;
    }
    public function getFullName() {
        return $this->user->firstname . ' ' . $this->user->lastname;
    }
    public function getAttributeSetName(){
        //return $this->product->name;
        return $this->product->attributeSet->title;
    }
    public function getWliDate(){
        return Helper::date($this->createdDate, " F jS, Y ");
    }
     public function getCartAdd(){
        if($this->product->typeId=='simple')
        {
        return '<input type="button" class="btn-primary btn cart" id="'.$this->productId.'" value="ADD TO CART">';  
        }
        else
        {
            return Html::a('<i class="fa fa-shopping-cart"></i> Add to cart',['wishlistitems/config','id' => $this->id],['class' => 'ajax-update ad-crt', 'data-title' => 'Create Product Settings']);
        }
    }
}
