<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Carts".
 *
 * @property integer $id
 * @property string $title
 * @property string $positionsData
 * @property integer $userId
 * @property string $createdDate
 */
class Carts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Carts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'positionsData', 'userId'], 'required'],
            [['positionsData'], 'string'],
            [['userId'], 'integer'],
            [['updatedDate'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'positionsData' => 'Positions Data',
            'userId' => 'User ID',
            'updatedDate' => 'Updated Date',
        ];
    }

    public function getItemCount(){
        $positions = unserialize($this->positionsData);
        return count($positions);
    }

    public function getItemQuantity(){
        $positions = unserialize($this->positionsData);
        $quantity = 0;
        if(!empty($positions)){
            foreach($positions as $position){
                $quantity += $position->quantity;
            }
        }
        return $quantity;
    }

    public function getSubTotal(){
        $positions = unserialize($this->positionsData);
        $total = 0;
        if(!empty($positions)){
            foreach($positions as $position){
                $total += $position->cost;
            }
        }
        return $total;
    }

    public function getCustomerName(){
        if(isset($this->customer))
            return $this->customer->fullName;
        else
            return $this->title;
    }

    public function getCustomerEmail(){
        if(isset($this->customer))
            return $this->customer->email;
        else
            return $this->email;
    }

    public function getCustomer(){
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
}
