<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "GiftVouchers".
 *
 * @property integer $id
 * @property string $code
 * @property string $initialAmount
 * @property string $balance
 * @property string $status
 * @property integer $creatorId
 * @property integer $orderId
 * @property string $recipientName
 * @property string $recipientEmail
 * @property string $createdDate
 * @property string $expiredDate
 * @property integer $storeId
 */
class GiftVouchers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GiftVouchers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'initialAmount', 'balance', 'creatorId', 'recipientName', 'recipientEmail', 'storeId', 'expiredDate'], 'required'],
            [['balance'], 'number', 'max' => $this->initialAmount],
            [['code', 'status'], 'string'],
            [['initialAmount', 'balance'], 'number'],
            [['creatorId', 'orderId', 'storeId'], 'integer'],
            [['createdDate', 'expiredDate','recipientMessage'], 'safe'],
            [['recipientName', 'recipientEmail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'initialAmount' => 'Initial Amount',
            'balance' => 'Balance',
            'status' => 'Status',
            'creatorId' => 'Creator ID',
            'orderId' => 'Order ID',
            'recipientName' => 'Recipient Name',
            'recipientEmail' => 'Recipient Email',
            'createdDate' => 'Created Date',
            'expiredDate' => 'Expiry Date',
            'storeId' => 'Store ID',
        ];
    }

    public function getOrder(){
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }

    public function getExpirationDateFormatted(){
        return \frontend\components\Helper::date($this->expiredDate);
    }

    public function getInitialAmountFormatted(){
        return \frontend\components\Helper::money($this->initialAmount);
    }

    public function getBalanceFormatted(){
        return \frontend\components\Helper::money($this->balance);
    }

    public function sendEmail($send = true){
        $template = \common\models\EmailTemplates::find()->where(['code' => 'giftvoucher'])->one();
        $store = \common\models\Stores::find()->where(['id' => $this->storeId])->one();
        if(!is_null($this->orderId) && $this->order->customerId==NULL){
            $userfullname = ucfirst($this->order->billing_firstname).' '.ucfirst($this->order->billing_lastname);
            $useremail=$this->order->email;
        }
        else{
            if(!is_null($this->orderId))
                $user = \common\models\User::findOne($this->order->customerId);
            else
                $user = Yii::$app->user->identity;
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
        }
        $queue = new \common\models\EmailQueue;
        $queue->models = ['store'=>$store,'voucher'=>$this, ]; 
        $queue->replacements =  [
                                'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'Logo'=>$store->Logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
            'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,
            'emailLinkedinImage'=>$store->emailLinkedinImage,
            'userfullname'=>$userfullname,'useremail'=>$useremail,
            ];

        $queue->from = "no-reply";
        //$queue->recipient = $user;//var_dump($template);die;
        $queue->recipients = $this->recipientEmail;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        //var_dump($queue->message);die;
        if($send)
            $queue->save();
        else{
            var_dump($queue->message);die;
        }
    }
}
