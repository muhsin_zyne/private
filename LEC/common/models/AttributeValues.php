<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "AttributeValues".
 *
 * @property integer $id
 * @property string $value
 * @property string $updatedOn
 * @property integer $attributeId
 * @property integer $StoreProductId
 *
 * @property Attributes $attribute
 * @property Storeproducts $storeProduct
 */
class AttributeValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AttributeValues';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updatedOn', 'value'], 'safe'],
            [['attributeId', 'productId', 'storeId'], 'required'],
            [['attributeId', 'productId', 'storeId'], 'integer'],
            [['value'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'updatedOn' => 'Updated On',
            'attributeId' => 'Attribute ID',
            'StoreProductId' => 'Store Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getAttribute()
    // {
    //     return $this->hasOne(Attributes::className(), ['id' => 'attributeId']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreProduct()
    {
        return $this->hasOne(Storeproducts::className(), ['id' => 'StoreProductId']);
    }

    public function getFormattedValue(){
        if(\frontend\components\Helper::isJson($this->value)){
            return implode(", ", json_decode($this->value, true));
        }
        return $this->value;
    }

    // public function getOption(){ //get the AttributeOptions model of the current value if it's of a dropdown
    //     return $this->hasOne(AttributeOptions::className(), ['value' => 'value'])->onCondition(['attributeId' => $this->attributeId, 'productId' => $this->productId]);
    // }
}
