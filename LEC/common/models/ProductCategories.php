<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ProductCategories".
 *
 * @property integer $id
 * @property integer $categoryId
 * @property integer $productId
 * @property integer $storeId
 */
class ProductCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductCategories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'productId', 'storeId'], 'required'],
            [['categoryId', 'productId', 'storeId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoryId' => 'Category ID',
            'productId' => 'Product ID',
            'storeId' => 'Store ID',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Products::className(), ['id' => 'productId']);
    }
}
