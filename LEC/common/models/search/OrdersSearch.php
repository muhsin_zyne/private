<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;
use common\models\User;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public $storeName;
    public $billingAddressText;
    public $shippingAddressText;

    public $orderDate_start;
    public $orderDate_end;
    public $userJenumber;
    public function rules()
    {
        return [
            [['id', 'storeId', 'quantity', 'customerId', 'billingAddressId', 'shippingAddressId', 'shipping_postcode', 'sameAsShipping', 'billing_postcode', 'shippingMethodId'], 'integer'],
            [['type', 'email', 'shipping_firstname', 'shipping_lastname', 'shipping_company', 'shipping_street', 'shipping_city', 'shipping_state', 'shipping_country', 'shipping_phone', 'shipping_fax', 
            'billing_firstname', 'billing_lastname', 'billing_company', 'billing_street', 'billing_city', 'billing_state', 'billing_country', 'billing_phone', 'billing_fax', 'discountDescription', 'vouchers',
             'customerNotes', 'orderDate', 'status', 'storeName', 'billingAddressText', 'shippingAddressText','userJenumber'], 'safe'],
            [['discountAmount', 'subTotal', 'shippingAmount', 'grandTotal', 'totalRefunded'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();
       
        // $user = User::findOne(Yii::$app->user->id);
        // if($user->roleId==3)
        // {
        //     $query =Orders::find()->where(['type' => 'b2c','storeId'=>$user->store->id])->orderBy(['id'=>SORT_DESC,]);
        // }
        // else
        // {
        //     $query = Orders::find()->where(['type' => 'b2c'])->orderBy(['id'=>SORT_DESC,]); 
        // }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $this->orderDate_start = isset($_REQUEST['OrdersSearch']['orderDate_start'])? date('Y-m-d 00:00:00', strtotime($_REQUEST['OrdersSearch']['orderDate_start'])) : null;
        $this->orderDate_end = isset($_REQUEST['OrdersSearch']['orderDate_end'])? date('Y-m-d 23:59:59', strtotime($_REQUEST['OrdersSearch']['orderDate_end'])) : null;
        

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
           // 'id' => $this->id,
             'storeId' => $this->storeId,
             'status' => $this->status,
            // 'quantity' => $this->quantity,
            // 'customerId' => $this->customerId,
            // 'billingAddressId' => $this->billingAddressId,
            // 'shippingAddressId' => $this->shippingAddressId,
            // 'shipping_postcode' => $this->shipping_postcode,
            // 'sameAsShipping' => $this->sameAsShipping,
            // 'billing_postcode' => $this->billing_postcode,
            // 'shippingMethodId' => $this->shippingMethodId,
            // 'discountAmount' => $this->discountAmount,
            // 'subTotal' => $this->subTotal,
            // 'shippingAmount' => $this->shippingAmount,
            // 'grandTotal' => $this->grandTotal,
             //'orderDate' => $this->orderDate,
            // 'totalRefunded' => $this->totalRefunded,

        ]);
        // $query->joinWith(['user' => function ($q) {
        //     $q->where('Users.jenumber LIKE "%' . $this->userJenumber . '%"');
        // }]);

        $query->andFilterWhere(['like', 'id', $this->id])
           // ->andFilterWhere(['like', 'orderDate', $this->orderDate])
            ->andFilterWhere(['like', 'grandTotal', $this->grandTotal])
            // ->andFilterWhere(['like', 'shipping_lastname', $this->shipping_lastname])
            // ->andFilterWhere(['like', 'shipping_company', $this->shipping_company])
            // ->andFilterWhere(['like', 'shipping_street', $this->shipping_street])
            // ->andFilterWhere(['like', 'shipping_city', $this->shipping_city])
            // ->andFilterWhere(['like', 'shipping_state', $this->shipping_state])
            // ->andFilterWhere(['like', 'shipping_country', $this->shipping_country])
            // ->andFilterWhere(['like', 'shipping_phone', $this->shipping_phone])
            // ->andFilterWhere(['like', 'shipping_fax', $this->shipping_fax])
            // ->andFilterWhere(['like', 'billing_firstname', $this->billing_firstname])
            // ->andFilterWhere(['like', 'billing_lastname', $this->billing_lastname])
            // ->andFilterWhere(['like', 'billing_company', $this->billing_company])
            // ->andFilterWhere(['like', 'billing_street', $this->billing_street])
            // ->andFilterWhere(['like', 'billing_city', $this->billing_city])
            // ->andFilterWhere(['like', 'billing_state', $this->billing_state])
            // ->andFilterWhere(['like', 'billing_country', $this->billing_country])
            // ->andFilterWhere(['like', 'billing_phone', $this->billing_phone])
            // ->andFilterWhere(['like', 'billing_fax', $this->billing_fax])
            // ->andFilterWhere(['like', 'discountDescription', $this->discountDescription])
            // ->andFilterWhere(['like', 'vouchers', $this->vouchers])
            // ->andFilterWhere(['like', 'customerNotes', $this->customerNotes])
            ->andFilterWhere(['like', 'status', $this->status]);

            if(isset($_REQUEST['OrdersSearch']['orderDate_start']) || isset($_REQUEST['OrdersSearch']['orderDate_end'] )){
                if($_REQUEST['OrdersSearch']['orderDate_start'] != NULL || $_REQUEST['OrdersSearch']['orderDate_end'] != NULL) { 
                    $query->andFilterWhere(['between', 'orderDate', $this->orderDate_start, $this->orderDate_end]);
                    $this->orderDate_start = date("m/d/Y", strtotime($this->orderDate_start));
                    $this->orderDate_end = date("m/d/Y", strtotime($this->orderDate_end));
                }else{
                    $this->orderDate_start = "";
                    $this->orderDate_end = "";
                }
            } 

            //->andFilterWhere(['between', 'orderDate', $this->orderDate_start, $this->orderDate_end]);

        return $dataProvider;
    }
}
