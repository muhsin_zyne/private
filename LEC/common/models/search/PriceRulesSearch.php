<?php
namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PriceRules;

class PriceRulesSearch extends PriceRules
{

	public $code;
	public $created_start;
	public $created_end;
	public $expired_start;
	public $expired_end;

    public $expcreated_start;
    public $expcreated_end;
    public $expexpired_start;
    public $expexpired_end;


	  public function rules()
    {
        return [
          	[['description', 'type'], 'string'],
            [['fromDate', 'toDate'], 'safe'],
            [['title', 'code'], 'string', 'max' => 255]
            
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {

        $query = PriceRules::find();

    	$dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $currentDate =  date("Y-m-d H:i:s", strtotime(date("Y-m-d") . date("H:i:s")));

		if(isset($_REQUEST['PriceRulesSearch']['created_start'])){
		    if($_REQUEST['PriceRulesSearch']['created_start'] != NULL){
		        $this->created_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['PriceRulesSearch']['created_start']));
		        $this->created_end = $currentDate;
		    }
		}

		if(isset($_REQUEST['PriceRulesSearch']['expired_start'])){
		    if($_REQUEST['PriceRulesSearch']['expired_start'] != NULL){
		        $this->expired_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['PriceRulesSearch']['expired_start']));
		        $this->expired_end = $currentDate;
		    }
		}

		if(isset($_REQUEST['PriceRulesSearch']['created_end'])){
		    if($_REQUEST['PriceRulesSearch']['created_end'] != NULL){
		        $this->created_end = date('Y-m-d 00:00:00', strtotime($_REQUEST['PriceRulesSearch']['created_end']));
		    }
		}

		if(isset($_REQUEST['PriceRulesSearch']['expired_end'])){
		    if($_REQUEST['PriceRulesSearch']['expired_end'] != NULL){
		        $this->expired_end = date('Y-m-d 00:00:00', strtotime($_REQUEST['PriceRulesSearch']['expired_end']));
		    }
		}

        if (!$this->validate()) {  die();
           return $dataProvider;
        }

        $query->andFilterWhere(['like', 'code', $this->code])
        	  ->andFilterWhere(['like','title',$this->title])
        	  ->andFilterWhere(['like','description',$this->description])
        	  ->andFilterWhere(['like','type',$this->type]);

        if(isset($_REQUEST['PriceRulesSearch']['created_start']) || isset($_REQUEST['PriceRulesSearch']['created_end'] )){
            if($_REQUEST['PriceRulesSearch']['created_start'] != NULL || $_REQUEST['PriceRulesSearch']['created_end'] != NULL) {
                $query->andFilterWhere(['between', 'fromDate', $this->created_start, $this->created_end]);
            }
        }

        if(isset($_REQUEST['PriceRulesSearch']['expired_start']) || isset($_REQUEST['PriceRulesSearch']['expired_end'] )){
            if($_REQUEST['PriceRulesSearch']['expired_start'] != NULL || $_REQUEST['PriceRulesSearch']['expired_end'] != NULL) {
                $query->andFilterWhere(['between', 'toDate', $this->expired_start, $this->expired_end]);
            }
        }	  


        return $dataProvider;

    }	
}	

?>