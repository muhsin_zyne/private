<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CmsBlocks;
use common\models\User;
use common\models\StoreCmsBlocks;
/**
 * BlockPagesSearch represents the model behind the search form about `common\models\CmsBlocks`.
 */
class BlockPagesSearch extends CmsBlocks
{
    /**
     * @inheritdoc
     */
    public $storeId;
    public function rules()
    {
        return [
            [['id', 'status', 'createdBy'], 'integer'],
            [['title', 'slug', 'content', 'type', 'dateUpdated','storeId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = User::findOne(Yii::$app->user->id);
        $query = CmsBlocks::find();
        if($user->roleId==1)
        {
            if(isset($params['BlockPagesSearch']['storeId']) && $params['BlockPagesSearch']['storeId']!=''){
            
                $sparray=[];
                $storePages=StoreCmsBlocks::find()->where(['storeId'=>$params['BlockPagesSearch']['storeId']])->all();
                //var_dump($storePages);die;
                foreach ($storePages as $key => $storePage) {
                   $sparray[] = $storePage->blockId;
                }
                //var_dump($sparray);die;
                $query = CmsBlocks::find()->where(['id'=>$sparray]);
            }
        }
        else
        {
            $sparray=[];
            $storePages=StoreCmsBlocks::find()->where(['storeId'=>Yii::$app->params['storeId']])->all();
            foreach ($storePages as $key => $storePage) {
               $sparray[] = $storePage->blockId;
            }
            $query = CmsBlocks::find()->where(['id'=>$sparray]);

        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'dateUpdated' => $this->dateUpdated,
            'createdBy' => $this->createdBy,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
