<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GiftRegistry;

/**
 * GiftRegistrySearch represents the model behind the search form about `common\models\GiftRegistry`.
 */
class GiftRegistrySearch extends GiftRegistry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'storeId', 'phone', 'postalCode'], 'integer'],
            [['brideName', 'groomName', 'contact', 'email', 'address', 'city', 'state', 'blurb', 'weddingDate', 'activeFrom', 'expiresOn', 'urlKey', 'image', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GiftRegistry::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'storeId' => $this->storeId,
            'phone' => $this->phone,
            'postalCode' => $this->postalCode,
            'weddingDate' => $this->weddingDate,
            'activeFrom' => $this->activeFrom,
            'expiresOn' => $this->expiresOn,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'brideName', $this->brideName])
            ->andFilterWhere(['like', 'groomName', $this->groomName])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'blurb', $this->blurb])
            ->andFilterWhere(['like', 'urlKey', $this->urlKey])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
