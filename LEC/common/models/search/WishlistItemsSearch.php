<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WishlistItems;
 use frontend\components\Helper;
/**
 * WishlistItemsSearch represents the model behind the search form about `common\models\WishlistItems`.
 */
class WishlistItemsSearch extends WishlistItems
{
    /**
     * @inheritdoc
     */
    public $storeName;
    public $fullName;
    public $attributeSetName;
    public $wliDate;
    public function rules()
    {
        return [
            [['id', 'productId', 'userId',  'createdDate', 'storeName','fullName','attributeSetName','wliDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WishlistItems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.title LIKE "%' . $this->storeName . '%"');
        }]);
        // $query->joinWith(['product' => function ($q) {
        //     $q->where('AttributeSets.title LIKE "%' . $this->attributeSetName . '%"');
        // }]);
        //STR_TO_DATE(datestring, '%d/%m/%Y')
       
        $query->joinWith(['user' => function ($q) {
            $q->andWhere('Users.firstname LIKE "%' . $this->fullName . '%"' .'OR Users.lastname LIKE "%' . $this->fullName . '%"');
        }]);
       
       //Yii::$app->formatter->asDatetime("2014-06-30 02:52:51.210201", "php:d-m-Y H:i:s");
        //$query->andWhere('WishlistItems.createdDate LIKE "%' . Yii::$app->formatter->asDatetime($this->wliDate, "php:d-m-Y H:i:s") . '%"');
        

        $query->andFilterWhere(['like', 'createdDate', $this->wliDate]);
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'productId' => $this->productId,
        //     'userId' => $this->userId,
        //     'storeId' => $this->storeId,
        //     'createdDate' => $this->createdDate,
        // ]);

        return $dataProvider;
    }
}
