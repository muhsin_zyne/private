<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Storebanners as StorebannersModel;

/**
 * Storebanners represents the model behind the search form about `common\models\Storebanners`.
 */
class Storebanners extends StorebannersModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'storeId', 'bannerId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StorebannersModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'storeId' => $this->storeId,
            'bannerId' => $this->bannerId,
        ]);

        return $dataProvider;
    }
}
