<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Downloads;
use common\models\User;
/**
 * DownloadPagesSaerch represents the model behind the search form about `common\models\DownloadPages`.
 */
class DownloadsSaerch extends Downloads
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'storeId', 'status', 'position'], 'integer'],
            [['title', 'filePath', 'description', 'dateUpdated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == 1)
            $query = Downloads::find()->orderBy(['id' => SORT_DESC]);
        else
           $query = Downloads::find()->where(['storeId'=>Yii::$app->params["storeId"]])->orderBy(['id' => SORT_DESC]); 

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'storeId' => $this->storeId,
            'status' => $this->status,
            'position' => $this->position,
            'dateUpdated' => $this->dateUpdated,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'filePath', $this->filePath])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
