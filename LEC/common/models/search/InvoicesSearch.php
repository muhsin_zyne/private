<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Invoices;
use common\models\User;
use common\models\Orders;

/**
 * InvoicesSearch represents the model behind the search form about `common\models\Invoices`.
 */
class InvoicesSearch extends Invoices
{
    /**
     * @inheritdoc
     */
    public $orderDate;
    public $orderBillToAddress;
    public function rules()
    {
        return [
            [['id', 'orderId', 'notify'], 'integer'],
            [['comments', 'status', 'createdDate','orderDate','orderBillToAddress'], 'safe'],
            [['subTotal', 'shipping', 'grandTotal'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoices::find()->orderBy(['id'=>SORT_DESC,]);
        // $user = User::findOne(Yii::$app->user->id);
        // if($user->roleId==3)
        // {
        //     $query = Invoices::find()->orderBy(['id'=>SORT_DESC,]);
                
        // }
        // else
        // {
        //     $query = Invoices::find()->orderBy(['id'=>SORT_DESC,]);
        // }

        //var_dump($this);die;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orderId' => $this->orderId,
            'subTotal' => $this->subTotal,
            'shipping' => $this->shipping,
            'grandTotal' => $this->grandTotal,
            'notify' => $this->notify,
            'createdDate' => $this->createdDate,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
