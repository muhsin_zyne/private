<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Brands;
use common\models\User;
use yii\helpers\ArrayHelper;
/**
 * BrandsSearch represents the model behind the search form about `common\models\Brands`.
 */
class BrandsSearch extends Brands
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'enabled', 'isVirtual'], 'integer'],
            [['title', 'enabled', 'isVirtual', 'path'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;
        $query = Brands::find();
        if($user->roleId == "3"){
            $visibleBrands = ArrayHelper::map(\common\models\StoreBrandsVisibility::find()->where(['storeId'=>$user->store->id])->all(), 'brandId', 'brandId');
            $selectableBrands = ArrayHelper::map($user->store->brands, 'id', 'id') + $visibleBrands;
            $query = Brands::find()->andWhere(['IN', 'id',$selectableBrands]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

       // var_dump($params);die();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->andFilterWhere([
        //     'id' => $this->id,
            
        //     'status' => $this->status,
        //     'dateUpdated' => $this->dateUpdated,
        // ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'enabled', $this->enabled]);
            // ->andFilterWhere(['like', 'slug', $this->slug])
            // ->andFilterWhere(['like', 'content', $this->content]);

        // if($user->roleId == "3"){
        //    $query->andFilterWhere(['storeId'=>$user->store->id]);
        // }    

        return $dataProvider;
    }
}
