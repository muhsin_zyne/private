<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;

/**
 * OrderItemsSearch represents the model behind the search form about `common\models\OrderItems`.
 */
class OrderItemsSearch extends OrderItems
{
    /**
     * @inheritdoc
     */
    public $productSku;
    public $totalPrice;
    public $storeName;
    public $productName;
    public $billingAddress;
    public $shippingAddress;
    public function rules()
    {
        return [
            [['id', 'orderId', 'productId', 'quantity', 'promotionId', 'storeAddressId', 'supplierUid', 'quantityRefunded', 'conferenceId'], 'integer'],
            [['config', 'comment', 'status','productSku','totalPrice','storeName','productName','billingAddress','shippingAddress'], 'safe'],
            [['discount', 'price', 'amountRefunded'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderItems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //var_dump($this);die;
        $query->andFilterWhere([
            //'id' => $this->id,
            //'orderId' => $this->orderId,
            //'productId' => $this->productId,
            //'quantity' => $this->quantity,
            //'discount' => $this->discount,
            //'price' => $this->price,
            //'promotionId' => $this->promotionId,
            //'storeAddressId' => $this->storeAddressId,
            //'supplierUid' => $this->supplierUid,
            //'quantityRefunded' => $this->quantityRefunded,
            //'amountRefunded' => $this->amountRefunded,
            //'conferenceId' => $this->conferenceId,
        ]);
        $query->joinWith(['product' => function ($q) {
            $q->where('Products.sku LIKE "%' . $this->productSku . '%"');
        }]);
        $query->joinWith(['b2baddress' => function ($q) {
            $q->where('B2bAddresses.storeName LIKE "%' . $this->storeName . '%"');
        }]);
        $query->joinWith(['b2baddress' => function ($q) {
            $q->where('B2bAddresses.billingAddress LIKE "%' . $this->billingAddress . '%"');
        }]);
        $query->joinWith(['b2baddress' => function ($q) {
            $q->where('B2bAddresses.shippingAddress LIKE "%' . $this->shippingAddress . '%"');
        }]);
        //$query->where('grandTotal LIKE "%' . $this->price * $this->quantity . '%" ' );
        

         $query->andFilterWhere(['like', 'price', $this->price])
            //->andFilterWhere(['like', 'grandTotal', $this->price*$this->quantity])
            ->andFilterWhere(['like', 'quantity', $this->quantity]);
        
        return $dataProvider;
    }
}
