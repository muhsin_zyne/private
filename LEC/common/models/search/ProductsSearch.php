<?php
/**
 * @author: Max
 */
namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\Attributes;
use yii\helpers\ArrayHelper;
/**
 * ProductsSearch represents the model behind the search form about `common\models\Products`.
 */ 
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */ 
    public $attributeSetName;
    public $brandName;
    public $supplierName;
    public $customAttributes;
    public $featured;
    public $filterByCategories = true;
    public function init(){
        $this->customAttributes = Attributes::find()->all();
        parent::init();
    }
    public function rules()
    {
        $rules = [];
        foreach($this->customAttributes as $attribute)
            $rules[] = [[$attribute->code], 'safe'];
        return array_merge([
            [['attributeSetId'], 'integer'],
            [['id', 'attributeSetName', 'brandName', 'supplierName', 'name', 'price', 'typeId', 'sku', 'status', 'dateAdded', 'brandId'], 'safe'],
        ], $rules);
    }

    public function attributes(){
        $attrs = array_values(ArrayHelper::map($this->customAttributes, 'id', 'code'));
        return array_merge(parent::attributes(), $attrs);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    { //var_dump(Yii::$app->params['storeId']); die;
        //$_REQUEST['Products']['attributeSetId'] = 20;
        $query = Products::find(); //var_dump($params);die;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) { var_dump($this->errors); die;
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // if (Yii::$app->id=="app-backend") {  //var_dump(Yii::$app->session["productsSearch"]);die();
        //     if(isset(Yii::$app->session["productsSearch"])){
        //         Yii::$app->request->queryParams = Yii::$app->session["productsSearch"];
        //     }
        //     if(isset(Yii::$app->request->queryParams["ProductsSearch"])){
        //         Yii::$app->session["productsSearch"] = Yii::$app->request->queryParams["ProductsSearch"];
        //     }
        // }

        //var_dump(Yii::$app->id);die();

        if(isset($_REQUEST['Categories']) && $this->filterByCategories){ //die('sdfwe');
            $cats = [];
            foreach($_REQUEST['Categories'] as $cat){
                if($category = \common\models\Categories::findOne($cat))
                    $cats = array_merge($cats, array_merge(ArrayHelper::map($category->getChildCategories(), 'id', 'id'), [$category->id]));
            }
            $query->andWhere("Products.id in (select productId from ProductCategories where categoryId in (".implode(",", $cats)."))");
        }
        $matches = [];
        if(isset($_REQUEST['Products']['priceRange'])){
            $min = $_REQUEST['Products']['priceRange']['min'];
            $max = $_REQUEST['Products']['priceRange']['max'];
            $priceAttr = (Yii::$app->id=="app-b2b" || Yii::$app->id=="app-api")? "cost_price" : "price";
            $match = ArrayHelper::map(Products::find()->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')->join('JOIN', 'Attributes as a', 'av.attributeId = a.id')->where("a.code='$priceAttr'")->andWhere("value <= ".$max." AND value >= ".$min)->all(), 'id', 'id');
            $matches[] = $match; //var_dump($match); die;
            if(isset($_REQUEST['Products']['Attributes'])){
                $i = 0;
                $options = [];
                $jsonOptions = [];
                foreach($_REQUEST['Products']['Attributes'] as $attrId => $value){  //var_dump($_REQUEST['Products']['Attributes']); die;
                    // foreach($value as $val){
                    //     if(\frontend\components\Helper::isJson($val))
                    //         $values = json_decode($val, true);
                    //     else
                    //         $values = [$value];
                    //     }
                    //     $jsonOptions[] = json_encode($values);
                    //     $options = array_merge($options, $values);
                    // }
                    //$match = ArrayHelper::map(Products::find()->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')->where('av.attributeId='.$attrId)->andWhere("value in ($values) OR value REGEXP '\"$values\"'")->all(), 'id', 'id');
                    //var_dump($options); die;
                    $matchQuery = Products::find()->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')->where('av.attributeId='.$attrId);
                    if(is_array($value)){
                        $matchCondition = "";
                        foreach($value as $i => $val){
                            $matchCondition = $matchCondition . "(value = '$val' OR value like '%\"$val\"%')";
                            if($i != count($value) - 1)
                                $matchCondition = $matchCondition . " OR ";
                        }
                        $matchQuery->andWhere($matchCondition);
                    }

                    $match = ArrayHelper::map($matchQuery->all(), 'id', 'id');
                    $matches[] = $match;
                }
            }
            if(isset($_REQUEST['Products']['Brands'])){
                //var_dump($_REQUEST['Products']['Brands']);die();
                /*foreach ($_REQUEST['Products']['Brands'] as $key => $brands) {
                    /*foreach ($brands as $brand) {
                        $query->joinWith(['brand' => function ($q) use($brand) {
                            $q->where('Brands.title LIKE "%' . $brand. '%"');
                        }]);
                    }*/
            $query = Products::find()->join('JOIN', 'Brands as b', 'b.id = Products.brandId');
            $query->andWhere('Products.brandId in ('.implode(",", $_REQUEST['Products']['Brands']).')');

            }
            if(isset($_REQUEST['keyword']) && $_REQUEST['keyword'] != ""){ 
                $q = \frontend\components\Helper::escape($_REQUEST['keyword']);
                $keywordQuery=Products::find();
                $keywordQuery->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id');
                $keywordQuery->join('JOIN', 'Attributes as a', 'a.id = av.attributeId');
                $keywordQuery->where("(a.code = 'name' AND av.value LIKE '%$q%')");
                $keywordQuery->orWhere("(a.code = 'ezcode' AND av.value LIKE '%$q%')");
                $keywordQuery->orWhere("(sku LIKE '%$q%')");
                $matches[] = ArrayHelper::map($keywordQuery->all(), 'id', 'id');
            }
            if(count($matches)<2)
                $matches = isset($matches[0])? [$matches[0], $matches[0]] : [[], []];
            //var_dump($matches); die;
            $matches = call_user_func_array('array_intersect', $matches);
            $matches = empty($matches)? [0] : $matches;
            $query->andWhere('Products.id in ('.implode(",", $matches).')');
        }
        
        if(isset($this->attributeSetName)){
            $query->joinWith(['attributeSet' => function ($q) {
                $q->where('AttributeSets.title LIKE "%' . $this->attributeSetName . '%"');
            }]);
        }
        if(isset($this->brandName)){
            $query->joinWith(['brand' => function ($q) {
                $q->where('Brands.title LIKE "%' . $this->brandName . '%"');
            }]);
        }
        if(isset($_REQUEST['ProductsSearch']['supplierName'])){
            $query->joinWith(['brand.brandSuppliers.supplier' => function ($q) {
                $q->where("concat_ws(' ',Users.firstname,Users.lastname) LIKE '%{$this->supplierName}%'");
            }]);
        }

        if(isset($this->featured)){
            if($this->featured !=0){ 
                $query->joinWith(['attributeValues' => function($q){
                    $q->where('AttributeValues.attributeId = 126 and AttributeValues.value =1 and AttributeValues.storeId = '.Yii::$app->user->identity->store->id.'');
                }]);
            }    
        }


        if(isset($_REQUEST['ProductsSearch'])){
            $attrs = array_values(ArrayHelper::map($this->customAttributes, 'id', 'code'));
            $attrIds = array_flip(ArrayHelper::map($this->customAttributes, 'id', 'code'));
            foreach($_REQUEST['ProductsSearch'] as $attribute => $value){
                if(in_array($attribute, $attrs)){
		    if($attribute == "featured" && $value == "0")
			continue;
                    $match = ArrayHelper::map(Products::find()->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')->where('av.attributeId='.$attrIds[$attribute])->andWhere("value LIKE '%".\frontend\components\Helper::escape($value)."%'")->all(), 'id', 'id');
                    $matches[] = $match;
                }
            }
            if(count($matches)==1)
                $matches[] = $match;

            $matches = call_user_func_array('array_intersect', $matches);
            //var_dump($matches); die;
            if(!empty($matches))
                $query->andWhere('Products.id in ('.implode(",", $matches).')');
            else
                $query->andWhere('1=0');
        }
        if(isset($_REQUEST['Products[id]']))
            $this->id = $_REQUEST['Products[id]'];
        if(strpos($this->id, ',') !== false)
            $this->id = explode(",", $this->id);
        //var_dump($this->id); die;
        if(!is_null($this->id) && $this->id != "")
            $query->andWhere(['Products.id' => $this->id]);
        $query->andFilterWhere(['like', 'typeId', $this->typeId]);
        $query->andFilterWhere(['like', 'sku', $this->sku]);
        //var_dump($this->sku);die;
        $query->andFilterWhere(['like', 'status', $this->status]);
        $query->groupBy('Products.id');
        $dataProvider = new \common\components\InteDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    public function getFilterAttributes(){
        $query = \common\models\Attributes::find();
        // ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
        // //->join('JOIN', 'ProductCategories pc', 'pc.productId = av.productId')
        // ->join('JOIN', 'Products p', 'p.id = av.productId')
        // ->join('JOIN', 'Brands b', 'b.id = p.brandId')
        // ->join('JOIN', 'StoreProducts sp', 'sp.productId = p.id')
        // ->where(['b.id' => $this->id]);
        // if(Yii::$app->id != "app-b2b")
        //     $query->andWhere(['sp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND code not in ('color', 'size')");
        // ->groupBy('Attributes.id');
        $attributes = $query->all();
        $filterAttributes = [];
        foreach ($attributes as $key=>$attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        } 
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code){
        $query = \common\models\AttributeOptions::find()
        ->join('JOIN', 'Attributes a', 'a.id = AttributeOptions.attributeId')
        // //->join('JOIN', 'ProductCategories pc', 'pc.productId = AttributeValues.productId')
        // ->join('JOIN', 'Products p', 'p.id = AttributeValues.productId')
        // ->join('JOIN', 'Brands b', 'b.id = p.brandId')
        // ->join('JOIN', 'StoreProducts sp', 'sp.productId = p.id')
        ->where(['a.code' => $code]);
        // if(Yii::$app->id != "app-b2b")
        //     $query->andWhere(['sp.storeId' => Yii::$app->params['storeId']]);
        // $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
        // ->groupBy('`AttributeValues`.`value`');
        return $query->all();
    }
}
