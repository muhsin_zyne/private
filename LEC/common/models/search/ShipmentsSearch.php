<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Shipments;

/**
 * ShipmentsSearch represents the model behind the search form about `common\models\Shipments`.
 */
class ShipmentsSearch extends Shipments
{
    /**
     * @inheritdoc
     */
    public $orderDate;
    public $orderShipToAddress;
    public function rules()
    {
        return [
            [['id', 'orderId', 'notify'], 'integer'],
            [['title', 'comments', 'carrier', 'trackingNumber', 'createdDate','orderDate','orderShipToAddress'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shipments::find()->orderBy(['id'=>SORT_DESC,]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'orderId' => $this->orderId,
            'notify' => $this->notify,
            'createdDate' => $this->createdDate,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'carrier', $this->carrier])
            ->andFilterWhere(['like', 'trackingNumber', $this->trackingNumber]);

        return $dataProvider;
    }
}
