<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Attributes;

/**
 * AttributesSearch represents the model behind the search form about `common\models\Attributes`.
 */
class AttributesSearch extends Attributes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'required', 'applyToSimple', 'applyToGrouped', 'applyToConfigurable', 'applyToVirtual', 'applyToBundle', 'applyToDownloadable', 'system', 'configurable'], 'integer'],
            [['title', 'scope', 'code', 'field', 'validation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attributes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'required' => $this->required,
            'applyToSimple' => $this->applyToSimple,
            'applyToGrouped' => $this->applyToGrouped,
            'applyToConfigurable' => $this->applyToConfigurable,
            'applyToVirtual' => $this->applyToVirtual,
            'applyToBundle' => $this->applyToBundle,
            'applyToDownloadable' => $this->applyToDownloadable,
            'system' => $this->system,
            'configurable' => $this->configurable,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'scope', $this->scope])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'field', $this->field])
            ->andFilterWhere(['like', 'validation', $this->validation]);

        return $dataProvider;
    }
}
