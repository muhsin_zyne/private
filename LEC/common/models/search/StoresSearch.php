<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Stores;

/**
 * StoresSearch represents the model behind the search form about `common\models\Stores`.
 */
class StoresSearch extends Stores
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'adminId', 'isVirtual'], 'integer'],
            [['title', 'abn', 'billingAddress', 'shippingAddress', 'email', 'status', 'phone', 'logoPath', 'siteUrl', 'facebookUrl', 'twitterUrl', 'instagramUrl'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stores::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'adminId' => $this->adminId,
            'isVirtual' => $this->isVirtual,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'abn', $this->abn])
            ->andFilterWhere(['like', 'billingAddress', $this->billingAddress])
            ->andFilterWhere(['like', 'shippingAddress', $this->shippingAddress])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'logoPath', $this->logoPath])
            ->andFilterWhere(['like', 'siteUrl', $this->siteUrl])
            ->andFilterWhere(['like', 'facebookUrl', $this->facebookUrl])
            ->andFilterWhere(['like', 'twitterUrl', $this->twitterUrl])
            ->andFilterWhere(['like', 'instagramUrl', $this->instagramUrl]);

        return $dataProvider;
    }
}
