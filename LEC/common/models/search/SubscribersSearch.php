<?php



namespace common\models\search;



use Yii;

use yii\base\Model;

use yii\data\ActiveDataProvider;

use common\models\Subscribers;



/**

 * Subscribersearch represents the model behind the search form about `common\models\Subscribers`.

 */

class SubscribersSearch extends Subscribers

{



    public $createdDate_start;

    public $createdDate_end;

    /**

     * @inheritdoc

     */

    public function rules()

    {

        return [

            [['id',], 'integer'],

            [['dateAdded', 'email', 'firstname', 'lastname','storeId'], 'safe'],

            //[['createdDate_start', 'createdDate_end'], 'required', 'on' => 'search'],

        ];

    }



    /**

     * @inheritdoc

     */

    public function scenarios()

    {

        // bypass scenarios() implementation in the parent class

        return Model::scenarios();

    }



    /**

     * Creates data provider instance with search query applied

     *

     * @param array $params

     *

     * @return ActiveDataProvider

     */

    public function search($params)

    {

        $query = Subscribers::find();



        $dataProvider = new ActiveDataProvider([

            'query' => $query,

        ]);



        $this->load($params);



        $currentDate =  date("Y-m-d H:i:s", strtotime(date("Y-m-d") . date("H:i:s")));



        if(isset($_REQUEST['SubscribersSearch']['createdDate_start'])){

            if($_REQUEST['SubscribersSearch']['createdDate_start'] != NULL){

                $this->createdDate_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['SubscribersSearch']['createdDate_start']));

                $this->createdDate_end = $currentDate;

            }

        }



        if(isset($_REQUEST['SubscribersSearch']['createdDate_end'])){

            if($_REQUEST['SubscribersSearch']['createdDate_end'] != NULL){

                $this->createdDate_end = date('Y-m-d 00:00:00', strtotime($_REQUEST['SubscribersSearch']['createdDate_end']));

            }

        }



       /* $this->createdDate_start = isset($_REQUEST['SubscribersSearch']['createdDate_start'])? date('Y-m-d 00:00:00', strtotime($_REQUEST['SubscribersSearch']['createdDate_start'])) : null;

        $this->createdDate_end = isset($_REQUEST['SubscribersSearch']['createdDate_end'])? date('Y-m-d 11:59:59', strtotime($_REQUEST['SubscribersSearch']['createdDate_end'])) : null;*/







        if (!$this->validate()) {

            // uncomment the following line if you do not want to any records when validation fails

            // $query->where('0=1');

            return $dataProvider;

        }



        $query->andFilterWhere([

            'id' => $this->id,
            'storeId' => $this->storeId,

            // 'status' => $this->status,

            // 'created_at' => $this->created_at,

            // 'updated_at' => $this->updated_at,

            // 'roleId' => $this->roleId,

        ]);



        $query->andFilterWhere(['like', 'firstname', $this->firstname])

            ->andFilterWhere(['like', 'lastname', $this->lastname])

            ->andFilterWhere(['like', 'email', $this->email]);





        if(isset($_REQUEST['SubscribersSearch']['createdDate_start']) || isset($_REQUEST['SubscribersSearch']['createdDate_end'] )){

            if($_REQUEST['SubscribersSearch']['createdDate_start'] != NULL || $_REQUEST['SubscribersSearch']['createdDate_end'] != NULL) {

                $query->andFilterWhere(['between', 'dateAdded', $this->createdDate_start, $this->createdDate_end]);

            }

        } 



           // ->andFilterWhere(['between', 'dateAdded', $this->createdDate_start, $this->createdDate_end]);

        return $dataProvider;

    }

}

