<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\Users`.
 */
class UserSearch extends User
{

    public $createdDate_start;
    public $createdDate_end;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'roleId'], 'integer'],
            [['auth_key', 'password_hash', 'password_reset_token', 'email', 'firstname', 'lastname'], 'safe'],
            //[['createdDate_start', 'createdDate_end'], 'required', 'on' => 'search'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $this->createdDate_start = isset($_REQUEST['UserSearch']['createdDate_start']) && ($_REQUEST['UserSearch']['createdDate_start'] != "") ? date('Y-m-d 00:00:00', strtotime($_REQUEST['UserSearch']['createdDate_start'])) : null;
        $this->createdDate_end = isset($_REQUEST['UserSearch']['createdDate_end']) && ($_REQUEST['UserSearch']['createdDate_end'] != "") ? date('Y-m-d 11:59:59', strtotime($_REQUEST['UserSearch']['createdDate_end'])) : null;
        
        $currentDate =  date("Y-m-d H:i:s", strtotime(date("Y-m-d") . date("H:i:s")));
        
        /*$this->createdDate_start = isset($_REQUEST['UserSearch']['createdDate_start']) ? date('Y-m-d 00:00:00', strtotime($_REQUEST['UserSearch']['createdDate_start'])) : null;
        $this->createdDate_end = isset($_REQUEST['UserSearch']['createdDate_end'])? date('Y-m-d 11:59:59', strtotime($_REQUEST['UserSearch']['createdDate_end'])) : null;*/

        if(isset($_REQUEST['UserSearch']['createdDate_start'])){
            if($_REQUEST['UserSearch']['createdDate_start'] != NULL){
                $this->createdDate_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['UserSearch']['createdDate_start']));
                $this->createdDate_end = $currentDate;
            }
        }

        if(isset($_REQUEST['UserSearch']['createdDate_end'])){
            if($_REQUEST['UserSearch']['createdDate_end'] != NULL){
                $this->createdDate_end = date('Y-m-d 00:00:00', strtotime($_REQUEST['UserSearch']['createdDate_end']));
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'roleId' => $this->roleId,
        ]);

        $query->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname]);
            //->andFilterWhere(['between', 'created_at', $this->createdDate_start, $this->createdDate_end]);


        if(isset($_REQUEST['UserSearch']['createdDate_start']) || isset($_REQUEST['UserSearch']['createdDate_end'] )){
            if($_REQUEST['UserSearch']['createdDate_start'] != NULL || $_REQUEST['UserSearch']['createdDate_end'] != NULL) {
                $query->andFilterWhere(['between', 'created_at', $this->createdDate_start, $this->createdDate_end]);
            }
        } 

        return $dataProvider;
    }
}
