<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CmsPages;
use common\models\User;
use common\models\StorePages;
/**
 * CmsPagesSearch represents the model behind the search form about `common\models\CmsPages`.
 */
class CmsPagesSearch extends CmsPages
{
    /**
     * @inheritdoc
     */ 
    public $storeId;
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['title', 'contentHeading', 'slug', 'content', 'dateUpdated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $user = User::findOne(Yii::$app->user->id);

        

        $query = CmsPages::find();
        if($user->roleId==1)
        {
            if(isset($params['CmsPagesSearch']['storeId']) && $params['CmsPagesSearch']['storeId']!=''){
            
                $sparray=[];
                $storePages=StorePages::find()->where(['storeId'=>$params['CmsPagesSearch']['storeId']])->all();
                //var_dump($storePages);die;
                foreach ($storePages as $key => $storePage) {
                   $sparray[] = $storePage->cmspageId;
                }
                //var_dump($sparray);die;
                $query = CmsPages::find()->where(['id'=>$sparray]);
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

       // var_dump($params);die();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            
            'status' => $this->status,
            'dateUpdated' => $this->dateUpdated,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'contentHeading', $this->contentHeading])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'content', $this->content]);

        // if($user->roleId == "3"){
        //    $query->andFilterWhere(['storeId'=>$user->store->id]);
        // }    

        return $dataProvider;
    }
}
