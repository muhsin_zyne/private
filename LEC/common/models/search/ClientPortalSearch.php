<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ClientPortal;

class ClientPortalSearch extends ClientPortal
{

    public $validFrom_start;
    public $validFrom_end;
    public $expiresOn_start;
    public $expiresOn_end;

	public function rules()
    {
        return [
            [['id','storeId'], 'integer'],
            [['studentCode','schoolCode', 'address', 'payment_type', 'shipment_type' , 'organisation_logopath'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ClientPortal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $currentDate =  date("Y-m-d H:i:s", strtotime(date("Y-m-d") . date("H:i:s")));

        if(isset($_REQUEST['ClientPortalSearch']['validFrom_start'])){
            if($_REQUEST['ClientPortalSearch']['validFrom_start'] != NULL){
                $this->validFrom_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['ClientPortalSearch']['validFrom_start']));
                $this->validFrom_end = $currentDate;
            }
        }

        if(isset($_REQUEST['ClientPortalSearch']['expiresOn_start'])){
            if($_REQUEST['ClientPortalSearch']['expiresOn_start'] != NULL){
                $this->expiresOn_start = date('Y-m-d 00:00:00', strtotime($_REQUEST['ClientPortalSearch']['expiresOn_start']));
                $this->expiresOn_end = $currentDate;
            }
        }

        if(isset($_REQUEST['ClientPortalSearch']['validFrom_end'])){
            if($_REQUEST['ClientPortalSearch']['validFrom_end'] != NULL){
                $this->validFrom_end = date('Y-m-d 00:00:00', strtotime($_REQUEST['ClientPortalSearch']['validFrom_end']));
            }
        }

        if(isset($_REQUEST['ClientPortalSearch']['expiresOn_end'])){
            if($_REQUEST['ClientPortalSearch']['expiresOn_end'] != NULL){
                $this->expiresOn_end = date('Y-m-d 00:00:00', strtotime($_REQUEST['ClientPortalSearch']['expiresOn_end']));
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'studentCode' => $this->studentCode,
            'schoolCode' => $this->schoolCode,
            'shipment_type' => $this->shipment_type,
            'payment_type' => $this->payment_type,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);
           // ->andFilterWhere(['like', 'image', $this->image]);

        if(isset($_REQUEST['ClientPortalSearch']['validFrom_start']) || isset($_REQUEST['ClientPortalSearch']['validFrom_end'] )){
            if($_REQUEST['ClientPortalSearch']['validFrom_start'] != NULL || $_REQUEST['ClientPortalSearch']['validFrom_end'] != NULL) {
                $query->andFilterWhere(['between', 'validFrom', $this->validFrom_start, $this->validFrom_end]);
            }
        }

        if(isset($_REQUEST['ClientPortalSearch']['expiresOn_start']) || isset($_REQUEST['ClientPortalSearch']['expiresOn_end'] )){
            if($_REQUEST['ClientPortalSearch']['expiresOn_start'] != NULL || $_REQUEST['ClientPortalSearch']['expiresOn_end'] != NULL) {
                $query->andFilterWhere(['between', 'expiresOn', $this->expiresOn_start, $this->expiresOn_end]);
            }
        }

        return $dataProvider;
    }    

}

?>	