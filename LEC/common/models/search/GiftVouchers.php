<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GiftVouchers as GiftVouchersModel;

/**
 * GiftVouchers represents the model behind the search form about `common\models\GiftVouchers`.
 */
class GiftVouchers extends GiftVouchersModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'creatorId', 'orderId', 'storeId'], 'integer'],
            [['code', 'status', 'recipientName', 'recipientEmail', 'recipientMessage', 'createdDate', 'expiredDate', 'lastUsedDate'], 'safe'],
            [['initialAmount', 'balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GiftVouchersModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'initialAmount' => $this->initialAmount,
            'balance' => $this->balance,
            'creatorId' => $this->creatorId,
            'orderId' => $this->orderId,
            'createdDate' => $this->createdDate,
            'expiredDate' => $this->expiredDate,
            'lastUsedDate' => $this->lastUsedDate,
            'storeId' => $this->storeId,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'recipientName', $this->recipientName])
            ->andFilterWhere(['like', 'recipientEmail', $this->recipientEmail])
            ->andFilterWhere(['like', 'recipientMessage', $this->recipientMessage]);

        return $dataProvider;
    }
}
