<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StoreBrands;

/**
 * StoreBrandsSearch represents the model behind the search form about `common\models\StoreBrands`.
 */
class StoreBrandsSearch extends StoreBrands
{
    /**
     * @inheritdoc
     */
    public $storeName;
    public $storeEmail;
    public $storeBillingAddress;
    public $storeShippingAddress;
    public $storePhone;
    public function rules()
    {
        return [
            [['id', 'storeId', 'brandId', 'enabled'], 'integer'],
            [['type','storeName','storeEmail','storeBillingAddress','storeShippingAddress','storePhone'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StoreBrands::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['store']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'storeId' => $this->storeId,
            'brandId' => $this->brandId,
            'enabled' => $this->enabled,
            //'storeName'=>$this->storeName,

        ]);

        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.title LIKE "%' . $this->storeName . '%"');
        }]);
        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.isVirtual LIKE "%' . 0 . '%"');
        }]);
        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.email LIKE "%' . $this->storeEmail . '%"');
        }]);
        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.billingAddress LIKE "%' . $this->storeBillingAddress . '%"');
        }]);
        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.shippingAddress LIKE "%' . $this->storeShippingAddress . '%"');
        }]);
        $query->joinWith(['store' => function ($q) {
            $q->where('Stores.phone LIKE "%' . $this->storePhone . '%"');
        }]);
        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
