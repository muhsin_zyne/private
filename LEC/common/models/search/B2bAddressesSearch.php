<?php

namespace common\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\B2bAddresses;

/**
 * B2bAddressesSearch represents the model behind the search form about `common\models\B2bAddresses`.
 */
class B2bAddressesSearch extends B2bAddresses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ownerId', 'postCode', 'phone', 'defaultAddress'], 'integer'],
            [['storeName', 'streetAddress', 'city', 'state', 'country', 'latitude', 'longitude', 'tradingHours'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = B2bAddresses::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ownerId' => $this->ownerId,
            'postCode' => $this->postCode,
            'phone' => $this->phone,
            'defaultAddress' => $this->defaultAddress,
        ]);

        $query->andFilterWhere(['like', 'storeName', $this->storeName])
            ->andFilterWhere(['like', 'streetAddress', $this->streetAddress])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'tradingHours', $this->tradingHours]);

        return $dataProvider;
    }
}
