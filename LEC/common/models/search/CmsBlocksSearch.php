<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CmsBlocks;
use common\models\User;
use common\models\StoreCmsBlocks;
/**
 * CmsblockPagesSearch represents the model behind the search form about `common\models\CmsblockPages`.
 */
class CmsBlocksSearch extends CmsBlocks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'status', 'createdBy'], 'integer'],
            [['title', 'content', 'dateUpdated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmsBlocks::find(); 
        $user = User::findOne(Yii::$app->user->id);      
        if($user->roleId==1)
        { //var_dump($params['CmsblockPagesSearch']['storeId']);die;
            if(isset($params['CmsblockPagesSearch']['storeId']) && $params['CmsblockPagesSearch']['storeId']!=''){
            
                $sparray=[];
                $storePages=StoreCmsBlocks::find()->where(['storeId'=>$params['CmsblockPagesSearch']['storeId']])->all();
                
                foreach ($storePages as $key => $storePage) {
                   $sparray[] = $storePage->cmsblockpageId;
                }
                //var_dump($sparray);die;
                $query = CmsBlocks::find()->where(['id'=>$sparray]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            
            'status' => $this->status,
            'dateUpdated' => $this->dateUpdated,
            'createdBy' => $this->createdBy,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
