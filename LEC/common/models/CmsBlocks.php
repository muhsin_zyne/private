<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cmsblockpages".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $storeId
 * @property integer $status
 * @property string $dateUpdated
 * @property integer $createdBy
 */
class CmsBlocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CmsBlocks';
    }

    /**
     * @inheritdoc
     */
    public $image;
    public $selected_store;
    public function rules()
    {
        return [
            [['title', 'content',  'status','selected_store', 'slug',], 'required'],
            [['content'], 'string'],
            [['status', 'createdBy','categoryId'], 'integer'],
            [['dateUpdated','selected_store',], 'safe'],
            [['title'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',            
            'status' => 'Status',
            'dateUpdated' => 'Date Updated',
            'createdBy' => 'Created By',
        ];
    }
    public function getStores(){
        return $this->hasMany(Stores::className(), ['id' => 'storeId'])->viaTable('StoreCmsBlocks', ['blockId' => 'id']);
    }
    public function getCategories(){
        return $this->hasOne(Categories::className(), ['id' => 'categoryId']);
    }
}
