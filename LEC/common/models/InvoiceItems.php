<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "InvoiceItems".
 *
 * @property integer $id
 * @property integer $invoiceId
 * @property integer $orderItemId
 * @property integer $quantityInvoiced
 */
class InvoiceItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'InvoiceItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoiceId', 'orderItemId', 'quantityInvoiced'], 'required'],
            [['invoiceId', 'orderItemId', 'quantityInvoiced'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoiceId' => 'Invoice ID',
            'orderItemId' => 'Order Item ID',
            'quantityInvoiced' => 'Quantity Invoiced',
        ];
    }
    public function getOrderItem()
    {
        return $this->hasOne(OrderItems::className(), ['id' => 'orderItemId']);
    }
}
