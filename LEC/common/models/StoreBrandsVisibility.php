<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StoreBrands".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $brandId
 */
class StoreBrandsVisibility extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StoreBrandsVisibility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'brandId'], 'required'],
            [['storeId', 'brandId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'brandId' => 'Brand ID',
        ];
    }
    
    
    }