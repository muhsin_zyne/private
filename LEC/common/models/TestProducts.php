<?php
/**
 * @author: Max
 */
namespace common\models;

use Yii;
use common\models\AttributeGroups;
use common\models\AttributeValues;
use yii\data\ActiveDataProvider;
use backend\components\Helper;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Stores;
use common\models\ProductInventory;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionProviderInterface;
use yz\shoppingcart\CartPositionTrait;
use yii\db\ActiveQuery;
use yii\base\Object;

class TestProducts extends \yii\db\ActiveRecord
{

	public function init(){
        
    }

    public static function tableName()
    {
        return 'TestProducts';
    }

    public function rules(){
        $rules = [];
        if(isset($_REQUEST['Products']['attributeSetId'])){ // set the rules for custom attributes
            if(isset(Yii::$app->params['temp']['attributeSet']))
                $attrSet = Yii::$app->params['temp']['attributeSet'];
            else{
                $attrSet = AttributeSets::findOne($_REQUEST['Products']['attributeSetId']);
                Yii::$app->params['temp']['attributeSet'] = $attrSet;
            }
            foreach($attrSet->attributeGroups as $group){
                foreach($group->productAttributes as $attr){
                    if($attr->required=="1")
                        $rules[] = [[$attr->code], 'required'];
                    else
                        $rules[] = [[$attr->code], 'safe'];
                    if($attr->validation!=""){
                        if($attr->validation == "decimal")
                            $rules[] = [[$attr->code], 'match', 'pattern'=>'/^[0-9]{1,12}(\.[0-9]{0,4})?$/'];
                        else
                            $rules[] = [[$attr->code], $attr->validation];
                    }
                }
            }
        }
        return array_merge([
            [['attributeSetId','brandId'], 'integer'],
            [['attributeSetId', 'typeId', 'sku'], 'required'],
            [['recipientMessage', 'recipientName', 'recipientEmail'], 'required', 'when' => function($model){ return $model->typeId=="gift-voucher"; }],
            [['recipientEmail'], 'email'],
            [['images','sku','dateAdded','brandId', 'urlKey', 'status'], 'safe'],
            [['sku'], 'unique', 'when' => function($model, $attribute){
                if(!$model->isNewRecord)
                    return $model->$attribute != $model->oldAttributes[$attribute];
                else
                    return true;
            }],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Categories'],
            [['urlKey'], 'unique', 'targetClass' => 'common\models\Products'],
            [['f_size', 'qty'], 'required', 'when' => function($model){ return $model->scenario == "purchase"; }, 'whenClient' => "function(attribute, value){ return ($('.nav-tabs-custom').length == 0); }"],
            [['qty'], 'integer', 'min' => 1],
            [['images'], 'file', 'extensions'=>'jpg, gif, png'],
        ], $rules);
    }

}

?>