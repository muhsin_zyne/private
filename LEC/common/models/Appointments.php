<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use common\models\Stores;
use common\models\User;
use common\components\ICSGenerator;

use frontend\components\Helper;

class Appointments extends \yii\db\ActiveRecord
{

	public $verifyCode;
    public static function tableName()
    {
        return 'Appointments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['	status','endTime','startTime','message'], 'safe'],
            [['email'], 'email'],
            [['name', 'email','phone','verifyCode'], 'required'],
            [['id', 'storeId','phone'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    public function isAvailable($timeslot){
        if(Yii::$app->id == "app-backend"){
            $user = User::findOne(Yii::$app->user->id);
            $storeId = $user->store->id;
        }
        elseif (Yii::$app->id == "app-frontend"){         
            $storeId = Yii::$app->params['storeId'];
        }    
        if(Appointments::find()->where(['storeId'=>$storeId,'startTime'=>$timeslot])->one())
            return false;
        else
            return true;
        
    }

    public function isBooked($timeslot){
        if(Yii::$app->id == "app-backend"){
            $user = User::findOne(Yii::$app->user->id);
            $storeId = $user->store->id;
        }
        elseif (Yii::$app->id == "app-frontend"){         
            $storeId = Yii::$app->params['storeId'];
        }
        if($appointment = Appointments::find()->where(['storeId'=>$storeId,'startTime'=>$timeslot,'status'=>'booked'])->one())
            return $appointment->id;
        else
            return false;

    }

    public function sendAppointmentConfirmation(){ 
    	$template = \common\models\EmailTemplates::find()->where(['code' => 'appointmentconfirmation'])->one();
    	$store = \common\models\Stores::find()->where(['id' => $this->storeId])->one();

        $icsGenerator = Yii::$app->ICSGenerator; 
        $icsGenerator->filename = "Appointment_".$store->title."_".rand(10000,20000).".ics";
        $icsGenerator->datestart = $this->startTime;
        $icsGenerator->dateend = $this->endTime;
        $icsGenerator->description = "Appointment";
        $icsGenerator->summary = "Book a Design Consultation - ".$store->title;
        $icsGenerator->location = $store->billingAddress;
        $attachment = $icsGenerator->save();

        $fileName = json_encode([\Yii::$app->basePath."/..".Yii::$app->params["attachmentspath"].$attachment => $attachment]);

        $queue = new \common\models\EmailQueue;
    	$queue->models = ['store'=>$store,'appointment'=>$this];
    	$useremail = $this->email;
    	$queue->replacements =[ 'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'Logo'=>$store->Logo,
                'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
                'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'useremail'=>$useremail]; 
        $queue->from = "no-reply";
        $queue->recipients = $useremail;
        //$queue->recipients = "dan.b@xtrastaff.com.au";
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        $queue->attachments = $fileName;
        $queue->save();
    }

     public function sendAppointmentNotification(){
    	$template = \common\models\EmailTemplates::find()->where(['code' => 'appointmentnotification'])->one();
    	$store = \common\models\Stores::find()->where(['id' => $this->storeId])->one();
    	$adminuser = \common\models\Stores::findOne(Yii::$app->params['storeId'])->admin;

        $icsGenerator = Yii::$app->ICSGenerator; 
        $icsGenerator->filename = "Appointment_".$store->title."_".rand(10000,20000).".ics";
        $icsGenerator->datestart = $this->startTime;
        $icsGenerator->dateend = $this->endTime;
        $icsGenerator->description = "Appointment";
        $icsGenerator->summary = "Book a Design Consultation - ".$store->title;
        $icsGenerator->location = $store->billingAddress;
        $attachment = $icsGenerator->save();

        $fileName = json_encode([\Yii::$app->basePath."/..".Yii::$app->params["attachmentspath"].$attachment => $attachment]);

    	$queue = new \common\models\EmailQueue;
    	$queue->models = ['store'=>$store,'appointment'=>$this];
    	$queue->replacements =[ 'fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'Logo'=>$store->Logo,
                'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,
                'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'useremail'=>$adminuser->email]; 
        $queue->from = "no-reply";
        $queue->recipients = $adminuser->email;
        //$queue->recipients = "dan.b@xtrastaff.com.au";
        $queue->attachments = $fileName;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);
        $queue->save();
    }

    public function getBookedSlot(){
    	$start = date('h:i a ', strtotime($this->startTime));
    	$end = date('h:i a ', strtotime($this->endTime));
    	return $start." - ". $end;
    }

    public function getBookedDate(){
    	return date('d-M-Y', strtotime($this->startTime));
    }
}
?>	
