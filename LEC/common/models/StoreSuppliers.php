<?php

namespace common\models;

use Yii;
use common\models\StoreBrands;
use common\models\BrandSuppliers;
/**
 * This is the model class for table "StoreSuppliers".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $supplierUid
 */
class StoreSuppliers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StoreSuppliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'supplierUid'], 'required'],
            [['storeId', 'supplierUid', 'enabled'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'supplierUid' => 'Supplier Uid',
        ];
    }

    public function setBrands($type){
        foreach($this->supplier->brands as $brand){
            if(!StoreBrands::find()->where(['storeId' => $this->storeId, 'brandId' => $brand->id, 'type' => $type])->one()){
                $storeBrand = new StoreBrands;
                $storeBrand->storeId = $this->storeId;
                $storeBrand->brandId = $brand->id;
                $storeBrand->type = $type;
                if($storeBrand->save(false))
                    $brand->setProducts($this->storeId, $type);
            }else
                 echo ($brand->id);
        }
        return true;
    }

    public function unsetBrands($type){
        $StoreSuppliers = [];
        $suppliersType = ($type=="b2c")? "suppliers" : "b2bSuppliers";
        foreach($this->store->$suppliersType as $supplier)
            $StoreSuppliers[] = $supplier->id;
        //var_dump($this->supplier->brands); die;
        foreach($this->supplier->brands as $brand){
            if(!$otherSupplier = BrandSuppliers::find()->where("brandId = '{$brand->id}' and supplierUid!='{$this->supplierUid}' and supplierUid in (".implode(",", $StoreSuppliers).")")->one()){
                StoreBrands::deleteAll(['storeId' => $this->storeId, 'brandId' => $brand->id, 'type' => $type]);
                $brand->unsetProducts($this->storeId, $type);
            }else{
                var_dump($otherSupplier); die;
            }
        }
    }

    public function getSupplier(){
        return $this->hasOne(Suppliers::className(), ['id' => 'supplierUid']);
    }

    public function getStore(){
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }
}
