<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ShipmentItems".
 *
 * @property integer $id
 * @property integer $shipmentId
 * @property integer $orderItemId
 * @property integer $quantityShipped
 */
class ShipmentItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShipmentItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipmentId', 'orderItemId', 'quantityShipped'], 'required'],
            [['shipmentId', 'orderItemId', 'quantityShipped'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shipmentId' => 'Shipment ID',
            'orderItemId' => 'Order Item ID',
            'quantityShipped' => 'Quantity',
        ];
    }
    public function getOrderItem()
    {
        return $this->hasOne(OrderItems::className(), ['id' => 'orderItemId']);
    }
    
}
