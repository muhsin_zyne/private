<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "storeblockpages".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $cmsblockpageId
 */
class StoreCmsBlocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StoreCmsBlocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'blockId'], 'required'],
            [['storeId', 'blockId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'cmsblockpageId' => 'Cmsblockpage ID',
        ];
    }
    public function getStore()
    {
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }
    public function getCmsblocks()
    {
        return $this->hasOne(CmsBlocks::className(), ['id' => 'blockId']);
    }
}
