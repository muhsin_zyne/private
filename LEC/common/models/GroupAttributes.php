<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "GroupAttributes".
 *
 * @property integer $id
 * @property integer $attributeGroupId
 * @property integer $attributeId
 */
class GroupAttributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GroupAttributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attributeGroupId', 'attributeId'], 'required'],
            [['attributeGroupId', 'attributeId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attributeGroupId' => 'Attribute Group ID',
            'attributeId' => 'Attribute ID',
        ];
    }
}
