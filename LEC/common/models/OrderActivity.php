<?php

namespace common\models;
use frontend\components\Helper;
use common\models\B2bAddresses;
use Yii;

/**
 * This is the model class for table "OrderActivity".
 *
 * @property integer $id
 * @property string $type
 * @property integer $typeId
 * @property string $comment
 * @property string $createdDate
 * @property string $notifiedDate
 */
class OrderActivity extends \yii\db\ActiveRecord
{
    public $entity;
    public $iconClassNames = [
        'order-placed' => 'flaticon-commerce',
        'ready-for-collection' => 'flaticon-verified-text-paper',
        'collected' => 'flaticon-check-1',
        'refund-initiated' => 'flaticon-money',
        'message-sent' => 'flaticon-multimedia',
        'email-resent' => 'flaticon-multimedia',
    ];
    public $iconBackgrounds = [
        'order-placed' => 'bg-brightred',
        'ready-for-collection' => 'bg-green',
        'collected' => 'bg-red',
        'refund-initiated' => 'bg-olive',
        'message-sent' => 'bg-orange',
        'email-resent' => 'bg-teal',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderActivity';
    }

    /**
     * @inheritdoc
     */
    //public $notify;
    //public $orderId;
    public function rules()
    {
        return [
            [['type', 'description', 'orderId'], 'required'],
            [['type', 'description'], 'string'],
            [['orderId'], 'integer'],
            [['date',], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'orderId' => 'Order Id',
            'typeId' => 'Type ID',
            'comment' => 'Comment',
            'createdDate' => 'Created Date',
            'notifiedDate' => 'Notified Date',
            'notify'=>'Notify Customer by Email',
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }

    public function generateDescription(){
        return $this->{"get".\yii\helpers\Inflector::id2camel($this->type)."Description"}();
    }

    public function getOrderPlacedDescription(){
        $items = $this->order->orderItems;
        $itemDetails = [];
        foreach($items as $item)
            $itemDetails[] = $item->product->name." x ".$item->quantity;
        if($customer = \common\models\User::findOne($this->order->customerId))
            $customerName = $customer->firstname;
        else
            $customerName = $this->order->billing_firstname;
        return "Order #{$this->order->id} placed by {$customerName} - ".implode(" | ", $itemDetails).".";
    }

    public function getReadyForCollectionDescription(){
        foreach($this->entity as $item){
            $itemDetails[] = $item['name']." x ".$item['qty'];
        }
        return "Item(s) marked as <b>Ready for Collection</b> - ".implode(" | ", $itemDetails).".";
    }

    public function getCollectedDescription(){
        if(!$this->entity instanceof \common\models\Deliveries)
            return false;
        foreach($this->entity->deliveryItems as $item)
            $itemDetails[] = $item->orderItem->product->name." x ".$item->quantityDelivered;
        return "Item(s) marked as <b>Collected</b> - ".implode(" | ", $itemDetails).". Collected by ".$this->entity->collectedBy.".";
    }

    public function getRefundInitiatedDescription(){
        if(!$this->entity instanceof \common\models\CreditMemos)
            return false;
        return "<b>Refund</b> initiated for ".Helper::money($this->entity->grandTotal).".";
    }

    public function getMessageSentDescription(){
        if($customer = \common\models\User::findOne($this->order->customerId))
            return "<b>Message sent</b> to {$customer->firstname}.";
        else
            return "<b>Message sent</b> to {$this->order->billing_firstname}.";
    }

    public function getEmailResentDescription(){
        return "<b>Order Confirmation</b> email resent.";
    }

}
