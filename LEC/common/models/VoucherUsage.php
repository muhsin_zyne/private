<?php

namespace common\models;

use Yii;
use common\models\GiftVouchers;
use frontend\components\Helper;
use yii\helpers\Html;
/**
 * This is the model class for table "VoucherUsage".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class VoucherUsage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'VoucherUsage';
    }

    /**
     * @inheritdoc
     */
    public $balance;
    public function rules()
    {
        return [
            [['id', 'voucherId', 'userId', 'orderId',], 'integer'],
            [['amountRedeemed','dateRedeemed','redemptionComment','redemptionAuthorizedBy'], 'required'],
            //[['amountRedeemed'], 'integer', 'max' => 10]
            [['amountRedeemed'], 'double', 'max' => $this->balance, ],
            [['balance'],'safe'],
            
        ];
    }
     public function attributeLabels() {
        return [
            'redemptionAuthorizedBy' => '	Redemption Authorised By',
        ];
    }

    public function getGiftVouchers()
    {
        return $this->hasOne(GiftVouchers::className(), ['id' => 'voucherId']);
    }
    public function getDateRedeemedFormated()
    {
        if(isset($this->dateRedeemed))        
           return Helper::date($this->dateRedeemed);
        else
            return '(not set)';
    }
    public function getOrderIdFormated()
    {
        if(isset($this->orderId))
            return Html::a('#'.$this->orderId,['orders/view','id' => $this->orderId]);        
        else       
            return  NULL; 
    }
}