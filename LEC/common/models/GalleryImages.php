<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "galleryimages".
 *
 * @property integer $id
 * @property string $title
 * @property integer $galleryId
 * @property integer $position
 * @property string $dateAdded
 */
class Galleryimages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $image;
    public static function tableName()
    {
        return 'GalleryImages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'galleryId'], 'required'],
            [['galleryId', 'position'], 'integer'],
            [['dateAdded','image'], 'safe'],           
            //[['image'], 'file', 'extensions'=>['jpg, gif, png'], 'maxSize' => 1024 * 1024 * 2],
            [['image'], 'file', 'extensions' => 'jpeg, jpg, png, gif', 'maxSize'=>2*1024*1024, 'maxFiles'=>1000],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'galleryId' => 'Gallery ID',
            'position' => 'Position',
            'dateAdded' => 'Date Added',
        ];
    }
}
