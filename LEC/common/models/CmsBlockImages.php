<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cmsblockimages".
 *
 * @property integer $id
 * @property string $path
 * @property string $dateAdded
 */
class CmsBlockImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CmsBlockImages';
    }

    /**
     * @inheritdoc
     */
     public $image;
    public function rules()
    {
        return [
            [['path'], 'required'],
            [['dateAdded'], 'safe'],
             [['image'], 'file','extensions'=>'jpg, gif, png'],
            [['path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'dateAdded' => 'Date Added',
        ];
    }
}
