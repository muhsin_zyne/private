<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "UserAddresses".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $company
 * @property string $telephone
 * @property string $fax
 * @property string $streetAddress
 * @property string $zip
 * @property string $country
 * @property integer $billingAddress
 * @property integer $shippingAdress
 */
class UserAddresses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserAddresses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['telephone', 'streetAddress','firstName','lastName', 'zip', 'country','city','state',], 'required'],
            //[['defaultBilling', 'defaultShipping'], 'string'],
            [['streetAddress','firstName','lastName'], 'string'],
            [['company', 'country'], 'string', 'max' => 100],
            [['telephone', 'fax'], 'string', 'max' => 48],
            [['zip'], 'string', 'max' => 20],
            [['city','state'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'company' => 'Company',
            'telephone' => 'Phone (Land Line)',
            'fax' => 'Mobile',
            'streetAddress' => 'Street Address',
            'zip' => 'Zip',
            'city'=>'Suburb/Town',
            'state'=>'State',
            'country' => 'Country',
            'defaultBilling' => 'Use as my default billing address',
            'defaultShipping' => 'Use as my default shipping address',
        ];
    }

    /*public function getBillingAddress()
    {
        return UserAddresses::find()->where(['type' => 'billing'])->all();
    }

    public function getShippingAddress()
    {
        return UserAddresses::find()->where(['type' => 'shipping'])->all();
    }*/

    public function getAddress()
    {
        return UserAddresses::findAll();
    }    

    public function getAddressAsText(){
        return $this->firstName." ".$this->lastName.'<br/> '.$this->streetAddress.'<br/>'.$this->city.', '.$this->state.', '.$this->zip.' <br/>'.'Phone : '.$this->telephone;
    }

    public function getDeliveryAddressAsText(){
        return '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">'.$this->firstName." ".$this->lastName.'</p><p style="margin:0;margin-bottom:10px;color:#8c8c7b;"> '.$this->streetAddress.'</p><p style="margin:0;color:#8c8c7b;margin-bottom:10px;">'.$this->city.' '.$this->state.' '.$this->zip.'</p><p style="margin:0;color:#8c8c7b;"> Phone : '.$this->telephone;
    }

    public function getAddressAsTextForDropdown(){
        return $this->firstName." ".$this->lastName.', '.$this->streetAddress.','.$this->city.', '.$this->state.', '.$this->zip.' ,'.'Phone : '.$this->telephone;
    }

    public function getAddressAsTextForMobile(){
        return $this->firstName." ".$this->lastName.', '.$this->streetAddress.','.$this->city.', '.$this->state.', '.$this->zip.' ,'.'Phone : '.$this->telephone;
    }
}
