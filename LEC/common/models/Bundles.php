<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Bundles".
 *
 * @property integer $id
 * @property string $title
 * @property string $inputType
 * @property integer $isRequired
 * @property integer $position
 * @property integer $productId
 */
class Bundles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Bundles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'productId'], 'required'],
            [['id', 'isRequired', 'position', 'productId'], 'integer'],
            [['title', 'inputType'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'inputType' => 'Input Type',
            'isRequired' => 'Is Required',
            'position' => 'Position',
            'productId' => 'Product ID',
        ];
    }

     public function getBundleitems() 
    {
        return $this->hasMany(BundleItems::className(),['bundleId' => 'id']);
    }
}
