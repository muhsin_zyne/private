<?php
namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\B2bAddresses;
use common\models\Stores;
use frontend\components\Helper; 

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class Subscribers extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Subscribers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge([
            // ['status', 'default', 'value' => self::STATUS_ACTIVE],
            // ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            //[['firstname', 'lastname', 'email'], 'required'],
            [['email'], 'required'],
            [['sessionId', 'storeId'],'safe'],
        ]);
    }

    public function attributeLables(){
        return [
            'firstname' => 'First Name',
            'lastname' => 'Last Name'
        ];
    }

    public function getStore(){
        return $this->hasOne(\common\models\Stores::className(), ['id' => 'storeId']);
    }
    
}
