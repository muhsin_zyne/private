<?php

namespace common\models;
use frontend\components\Helper;
use Yii;
use common\models\ClientPortal;
use common\models\Products;
use common\models\Stores;

/**
 * This is the model class for table "OrderItems".
 *
 */

class ClientPortalProducts extends \yii\db\ActiveRecord
{
	public static function tableName()
    {
        return 'ClientPortalProducts';
    }

     public function rules()
    {
        return [
            [['id', 'portalId', 'productId', 'offerPrice'], 'required'],
            [['id', 'productId'], 'integer'],
        ];
    }

     public function getPortal()
    {
        return $this->hasOne(ClientPortal::className(), ['id' => 'portalId']);
    }

    public function getProduct()
    {   
        $query = $this->hasOne(Products::className(), ['id' => 'productId']);
        $query->where = str_replace("Products.status = 1 AND ", "", $query->where);
        return $query;
    }
    public function getPortalProduct(){
        return $this;
    }

}


?>
