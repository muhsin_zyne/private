<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "EmailQueue".
 *
 * @property integer $id
 * @property integer $creatorUid
 * @property string $dateAdded
 * @property string $recipients
 * @property string $cc
 * @property string $bcc
 * @property string $from
 * @property string $subject
 * @property string $message
 * @property string $messageId
 * @property string $status
 * @property integer $lastRetryCount
 * @property string $lastRetry
 * @property string $dateSent
 */
class EmailQueue extends \yii\db\ActiveRecord
{
    var $models;
    var $recipient;
    var $replacements;
    var $defaultReplacements = ['footerText' => 'LEGJ'];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EmailQueue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creatorUid', 'lastRetryCount'], 'integer'],
            [['dateAdded', 'lastRetry', 'dateSent'], 'safe'],
            [['recipients', 'cc', 'bcc', 'from', 'subject', 'message', 'messageId', 'status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creatorUid' => 'Creator Uid',
            'dateAdded' => 'Date Added',
            'recipients' => 'Recipients',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'from' => 'From',
            'subject' => 'Subject',
            'message' => 'Message',
            'messageId' => 'Message ID',
            'status' => 'Status',
            'lastRetryCount' => 'Last Retry Count',
            'lastRetry' => 'Last Retry',
            'dateSent' => 'Date Sent',
        ];
    }

    public function send($recipientid, $variables = array(), $email=array(), $code = false){
        if(preg_match("/@/",$recipientid)){ // its to a user that is not in the system
            $this->recipient = $recipientid;
        //   $this->recipient = Users::model()->findByPk(10);
        } else { 
            $this->recipient = Users::model()->findByPk($recipientid);
        }
        $this->creatorUid = Yii::app()->user->getId();
        $this->status = 'Queued';   
        // need to detect from dynamically
         $mailDomain = Configuration::findSetting('email_domain');
        $this->from = $this->from."@".$mailDomain;
        if($code && $emailTemplate = EmailTemplates::find()->where(['code'=>$code])->one() && !empty($variables)){
            $this->models = $variables;
            $this->attributes = $this->fillTemplate($emailTemplate);
        } elseif(!empty($email)){
            $this->subject = $email['subject'];
            $this->message = $email['message'];
        }
        if(!$this->save()){
            return false;
        }
        return $this;
    }

    public function fillTemplate($template){
        /*$mailDomain = Configuration::findSetting('email_domain');
         if($this->from == 'no-reply'){ 
            $this->from .="@".$mailDomain;
        }*/
        //  from email @ aaron 12/07/2016
        if(Yii::$app->id=="app-b2b" || Yii::$app->id=="app-suppliers")   
            $this->from=Yii::$app->params['fromEmail'];                  
        else {  
            $fromEmail = Configuration::findSetting('fromEmail',Yii::$app->params['storeId']);
            if(isset($fromEmail) && $fromEmail!="")
                $this->from=$fromEmail; 
            else
                $this->from=Yii::$app->params['fromEmail'];    
        }
        //  Reply to Email @ aaron 12/07/2016
        if(Yii::$app->id=="app-b2b" || Yii::$app->id=="app-suppliers") {  
            $this->replyTo=Yii::$app->params['replyTo']; 
        }
        else {  
            $replyTo = Configuration::findSetting('replyTo',Yii::$app->params['storeId']);
            if(isset($replyTo) && $replyTo!="")
                $this->replyTo=$replyTo; 
            else
                $this->replyTo=Yii::$app->params['replyTo'];    
        }
       
        // if(!isset($this->recipient->email)){
        //     return false;
        // }


        // $this->recipients = $this->recipient->email;
        // now get placeholders 
        $placeholders['subject'] = $this->findPlaceholders($template->subject);
        $placeholders['html'] = $this->findPlaceholders($template->htmlBody);

        $find = array();
        $replace = array();
        //var_dump($template->htmlBody); die;
        foreach($placeholders as $count=>$type){
            foreach($type as $placeholder){
                    $find[]="[!$placeholder]";
                    $var = $this->resolveVar($placeholder);
                    $replace[]=$var;                                    
            }           
        }
        $this->subject = str_replace($find,$replace,$template->subject);
        $this->message = str_replace($find,$replace,$template->htmlBody);
        //var_dump($this->message);die;
        $this->status = 'Queued';       
    }

    public function findPlaceholders($string){
        if(preg_match_all("/\[!(.*?)\]/",$string,$matches)) // Checking wtheter there is a match for the pattern
            return $matches[1]; // returning the match
        else
            return [];
    }

    public function resolveVar($var){
        $parts = preg_split("/_/",$var);
        if(count($parts)>1){
            if(isset($this->models[$parts[0]])){
                $model = $this->models[$parts[0]];
                $item = $parts[1];
                if(isset($model->$item)){
                    $resolved = $model->$item;
                }elseif(method_exists($model, $item)){
                    $resolved = $model->$item();
                }else
                    return "[!unresolved: $var]";
            }else{
                die($var);
            }
        }elseif(count($parts)==1){
            if(isset($this->replacements[$parts[0]]))
                return $this->replacements[$parts[0]];
            elseif(isset($this->defaultReplacements[$parts[0]])){
                return $this->defaultReplacements[$parts[0]];
            }else
                return "[!unresolved: $var]";
        }else
            return "[!unresolved: $var]";
        return $resolved;
    }
}
