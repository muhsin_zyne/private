<?php

namespace common\models;

use Yii;
use common\models\TreeQuery;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;
use common\models\Products;

/**
 * This is the model class for table "ConferenceTrays".
 *
 * @property integer $id
 * @property string $title
 * @property string $trayId
 * @property string $image
 * @property integer $conferenceId
 */
class ConferenceTrays extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ConferenceTrays';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conferenceId','trayId'], 'required'],
            [['conferenceId'], 'integer'],
            [['title', 'image','trayId'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'trayId' => 'Tray Id',
            'conferenceId' => 'Conference ID',
        ];
    }

    public function getTrayProducts(){
        $user = User::findOne(Yii::$app->user->Id);
        if($user->roleId !=1)
            $storeId = User::findOne(Yii::$app->user->Id)->store->id;
        else
            $storeId = 0;
        return $this->hasMany(ConferenceTrayProducts::className(), ['conferencetrayId' => 'id'])
        ->join('JOIN', 'StoreProducts as sp', 'sp.productId = ConferenceTrayProducts.productId')
        ->where("conferencetrayId = ".$this->id." AND sp.storeId=".$storeId." AND sp.type='b2b'");
        
        // $query = new ActiveQuery('common\models\ConferenceTrayProducts');

        // $products = $query
        //             ->from('ConferenceTrayProducts as product')
        //             ->join('JOIN',
        //                 'StoreProducts as sp',
        //                 'sp.productId = product.productId'
        //             )
        //             ->where("conferencetrayId = ".$this->id." AND sp.storeId=".$storeId."")->all();
        // return $products;

    }

    public function getTest(){
        die('test');
    }

    public function getConference(){
        return $this->hasOne(Conferences::className(), ['id' => 'conferenceId']);
    }
}
