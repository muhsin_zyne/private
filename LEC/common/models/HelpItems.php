<?php

namespace common\models;
use Yii;
use common\models\Stores;
use common\models\Categories;
use yii\helpers\Url;

class HelpItems extends \yii\db\ActiveRecord
{

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'HelpItems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','content'], 'required'],
            [['screenshots', 'videoPath','content','title'], 'safe'],
            [['position','categoryId','enabled'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
    */


}
?>