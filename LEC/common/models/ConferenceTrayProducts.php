<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ConferenceTrayProducts".
 *
 * @property integer $id
 * @property integer $conferenceTrayId
 * @property integer $productId
 * @property string $offerCostPrice
 */
class ConferenceTrayProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ConferenceTrayProducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conferenceTrayId', 'productId'], 'required'],
            [['conferenceTrayId', 'productId'], 'integer'],
            [['offerCostPrice'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'conferenceTrayId' => 'Conference Tray ID',
            'productId' => 'Product ID',
            'offerCostPrice' => 'Offer Cost Price',
        ];
    }

    public function getProduct()
    {
         return $this->hasOne(Products::className(), ['id' => 'productId']);
    }

    public function getConferenceProduct(){
        return $this;
    }

    public function getConferenceTray(){
        return $this->hasOne(ConferenceTrays::className(), ['id' => 'conferenceTrayId']);
    }
}
