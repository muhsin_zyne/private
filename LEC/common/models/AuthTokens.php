<?php
namespace common\models;
class AuthTokens extends \yii\db\ActiveRecord
{
	public static function tableName()
    {
        return '{{%AuthTokens}}';
    }

    public function rules()
    {
        return [
        	[['token', 'type', 'userId',], 'required'],
        	[['token', 'type', 'userId',], 'safe'],
        ];
    }

    public function getUser(){
    	return $this->hasOne(\common\models\User::className(), ['id' => 'userId']);
    }
}