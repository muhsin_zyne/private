<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "AttributeGroups".
 *
 * @property integer $id
 * @property string $title
 * @property integer $attributeSetId
 *
 * @property Attributesets $attributeSet
 */
class AttributeGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AttributeGroups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attributeSetId'], 'required'],
            [['attributeSetId'], 'integer'],
            [['title'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'attributeSetId' => 'Attribute Set ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeSet()
    {
        return $this->hasOne(Attributesets::className(), ['id' => 'attributeSetId']);
    }

    public function getProductAttributes(){

        return $this
        ->hasMany(Attributes::className(), ['id' => 'attributeId'])
        ->join('JOIN', 'GroupAttributes as ga', 'ga.attributeId = Attributes.id')
        ->where(['attributeGroupId'=>$this->id])
        ->orderBy('ga.position')
        ->viaTable('GroupAttributes', ['attributeGroupId' => 'id']);

    }
}
