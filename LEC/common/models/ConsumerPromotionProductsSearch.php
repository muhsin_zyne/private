<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ConsumerPromotionProducts;

/**
 * ConsumerPromotionProductsSearch represents the model behind the search form about `common\models\ConsumerPromotionProducts`.
 */
class ConsumerPromotionProductsSearch extends ConsumerPromotionProducts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'promotionId', 'productId', 'pageId'], 'integer'],
            [['offerSellPrice', 'offerCostPrice'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConsumerPromotionProducts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'promotionId' => $this->promotionId,
            'productId' => $this->productId,
            'offerSellPrice' => $this->offerSellPrice,
            'offerCostPrice' => $this->offerCostPrice,
            'pageId' => $this->pageId,
        ]);

        return $dataProvider;
    }
}
