<?php

namespace common\models;

use Yii;
use frontend\components\Helper;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;

/**
 * This is the model class for table "ConsumerPromotions".
 *
 * @property integer $id
 * @property string $title
 * @property string $from
 * @property string $to
 * @property string $createdOn
 * @property string $consumerStartDate
 * @property string $costEndDate
 * @property string $image
 * @property string $catalog
 * @property string $reminderDate
 */
class ConsumerPromotions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $storeIds;

    public static function tableName()
    {
        return 'ConsumerPromotions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromDate', 'toDate', 'shortTag', 'createdOn', 'consumerStartDate', 'costEndDate', 'reminderDate'], 'safe'],
            [['catalog'], 'string'],
            [['title', 'shortTag'], 'string', 'max' => 45],
            [['image'], 'file'],
            [['title','shortTag','fromDate','toDate','costEndDate','consumerStartDate','catalog','reminderDate'],'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Name',
            'shortTag' => 'Short Tag',
            'fromDate' => 'Cost Price Start Date',
            'costEndDate' => 'Cost Price End Date',
            'consumerStartDate' => 'Consumer Promotion Start Date',
            'toDate' => 'Consumer Promotion End Date',
            'createdOn' => 'Created On',
            'image' => 'Image',
            'catalog' => 'Catalogue and Embed Code',
            'reminderDate' => 'Reminder Date',
        ];
    }

    public function getFromFormatted(){
        return Helper::date($this->fromDate);
    }
    

    public function getToFormatted(){
        return Helper::date($this->toDate);
    }

    public function getUrl(){
        //Yii::$app->request->baseUrl = 
        return \yii\helpers\Url::to(['b2b/promotions/view', 'id' => $this->id]);
    }

    public function getStores(){
        return $this->hasMany(StorePromotions::className(), ['promotionId' => 'id']);
    }

    public function getShortGrid(){
        /*return \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getPromotionProducts(), 'sort' => false]),
            'columns' => [
                [
                    'attribute' => 'product.name',
                    'label' => 'Item'
                ],
                'product.sku',
                [
                    'label' => 'Original Selling Price',
                    'attribute' => 'product.price'
                ],
                'offerSellPrice',
                [
                    'label' => 'Original Cost Price',
                    'attribute' => 'product.cost_price'
                ],
                'offerCostPrice',
                'pageId'
            ],
        ]);*/

        return ListView::widget([
            'summary'=>"",
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $this->getPromotionProducts(), 'sort' => false,'pagination'=> false,]),
            'itemView' => (Yii::$app->id == "frontend")?'/consumer-promotions/_maillistview':'/consumer-promotions/_maillistview',
        ]);
    }

    public function sendPublishNotification(){
        $template = \common\models\EmailTemplates::find()->where(['code' => 'promotionnotification'])->one();
        $b2bUsers = \common\models\User::find()->where(['roleId' => '3'])->all();
        
        $query = \common\models\User::find();

        $b2bUsers = $query
                    ->from('Users')
                    ->join('JOIN',
                        'Stores s',
                        's.adminId = Users.id'
                    )
                    ->join('JOIN',
                        'StorePromotions sp',
                        'sp.storeId = s.id'
                    )
                    ->join('JOIN',
                        'ConsumerPromotions cp',
                        'sp.promotionId = cp.id'
                    )
                    //->groupBy('Users.id')
                    ->orderBy('Users.id')
                    ->where("Users.roleId = 3 AND sp.promotionId=$this->id");   

        foreach($b2bUsers->all() as $user){
            $queue = new \common\models\EmailQueue;
            $template->layout = "layout";
            $store = \common\models\Stores::find()->where(['adminId'=>$user->id,'id'=>$user->storeId])->one();
            $promotion = $this;
            $userfullname=ucfirst($user->firstname).' '.ucfirst($user->lastname);
            $useremail=$user->email;
            //var_dump($promotion->products); die;
            $queue->models = ['promotion'=>$promotion];
            $queue->replacements =[ 'usertest'=>'test was success!','fromEmailAddress'=>$store->email,'siteUrl'=>$store->siteUrl,'logo'=>$store->logo,
            'emailFacebookImage'=>$store->emailFacebookImage,'emailTwitterImage'=>$store->emailTwitterImage,'emailInstagramImage'=>$store->emailInstagramImage,'emailYoutubeImage'=>$store->emailYoutubeImage,'emailPinterestImage'=>$store->emailPinterestImage,'emailGoogleplusImage'=>$store->emailGoogleplusImage,'emailLinkedinImage'=>$store->emailLinkedinImage,'userfullname'=>$userfullname,'useremail'=>$useremail,'appTitle'=>Yii::$app->params['app-title'],'date'=>date('Y'),'storeDetails'=>$store->storeDetails];
            $queue->from = "no-reply";
            $queue->recipient = $user;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);
            //var_dump($queue->message);die();
            $queue->save();
        }
    }

    public function getProducts($storeId, $type = null)
    {
        /*return Products::find()->join('JOIN','ConsumerPromotionProducts as cpp','cpp.productId = Products.id')->where(['cpp.promotionId' => $this->id]);*/

       // var_dump($this->id);die();

        //var_dump($this->id);die();

        $query = \common\models\Products::find();

        $products = $query
                    ->from('Products')
                    ->join('JOIN',
                        'ConsumerPromotionProducts cpp',
                        'cpp.productId = Products.id'
                    )
                    ->join('JOIN',
                        'ConsumerPromotions cp',
                        'cp.id = cpp.promotionId'
                    )
                    ->join('JOIN',
                        'StorePromotions stopro',
                        'stopro.promotionId = cpp.promotionId'
                    )
		    ->groupBy('cpp.productId')
                    ->orderBy('cpp.pageId')
                    ->where("Products.status = '1' AND cpp.promotionId=$this->id");
                    
        if(Yii::$app->id != "app-b2b")
            $query->andWhere("sp.storeId = '$storeId'"); 
        if(isset($type))
            $products->andWhere('sp.type="'.$type.'"');
        if(Yii::$app->id != "app-b2b"){
            $products->join('JOIN',
                        'StoreProducts as sp',
                        'sp.productId = Products.id'
                    );
            $products->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
            $products->andWhere('cpp.promotionId = '.$this->id.'');
        }

        //var_dump(count($products->all()));die();

        return $products;
        
    }

    public function getPromotionProducts(){
        return $this->hasMany(ConsumerPromotionProducts::className(), ['promotionId' => 'id']);
    }

    public function getStorePromotions(){
        return $this->hasMany(StorePromotions::className(), ['promotionId' => 'id']);
    }

     public function canPublish(){  //die("canPublish");
        //$promotion->status!="not-published";

        if($this->status == "not-published") {  //die("not-published");
            /*$products = ArrayHelper::map($this->promotionProducts, 'id', 'productId');
            $publishedPromotions = \common\models\ConsumerPromotions::find()->where('id != '.$this->id.'')->all();
            foreach ($publishedPromotions as $promotion) {  //var_dump($promotion->promotionProducts);die();
                if($product = ConsumerPromotionProducts::find()->where('productId in ('.implode(",", $products).') and promotionId = '.$promotion->id)->one())
                    {return false;}
                else if($product = ConferenceTrayProducts::find()->where('productId in ('.implode(",", $products).')')->one())
                    {return false;}
            }*/
            return true;
        }
    }

    public function adminAcceptPromotionMail()
    {
        $str_pro_id=1;

        $template = \common\models\EmailTemplates::find()->where(['code' => 'acceptpromotionadmin'])->one();
        $storepromotion = \common\models\StorePromotions::find()->where(['id' => $str_pro_id])->one();
        $consumerPromotion = \common\models\ConsumerPromotions::find()->where(['id' => $storepromotion->promotionId])->one();
        $store = \common\models\stores::find()->where(['id' => $storepromotion->storeId])->one();
        $user = \common\models\User::find()->where(['roleId' => 1])->one();
        //var_dump($template);die;
        $queue = new \common\models\EmailQueue;
            $queue->models = ['user'=>$user,'store'=>$store,'storepromotion'=>$storepromotion,'consumerPromotion'=>$consumerPromotion] ; 
            $queue->replacements =['siteUrl'=>'','Logo'=>'',
                                    'EmailFacebookImage'=>'','EmailTwitterImage'=>'','EmailInstagramImage'=>'',
                                    'facebookUrl'=>'','twitterUrl'=>'','instagramUrl'=>''];

            $queue->from = "no-reply";
            $queue->recipient = $user;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
            $queue->save();
    }
    public function b2buserAcceptPromotionMail()
    {
        $str_pro_id=1;
        
        $template = \common\models\EmailTemplates::find()->where(['code' => 'acceptpromotionb2buser'])->one();
        $storepromotion = \common\models\StorePromotions::find()->where(['id' => $str_pro_id])->one();
        $consumerPromotion = \common\models\ConsumerPromotions::find()->where(['id' => $storepromotion->promotionId])->one();
        $store = \common\models\stores::find()->where(['id' => $storepromotion->storeId])->one();
        $user = \common\models\User::find()->where(['id' => $storepromotion->store->adminId])->one();
        //var_dump($template);die;
        $queue = new \common\models\EmailQueue;
            $queue->models = ['user'=>$user,'store'=>$store,'storepromotion'=>$storepromotion,'consumerPromotion'=>$consumerPromotion] ; 
            $queue->replacements =['siteUrl'=>'','Logo'=>'',
                                    'EmailFacebookImage'=>'','EmailTwitterImage'=>'','EmailInstagramImage'=>'',
                                    'facebookUrl'=>'','twitterUrl'=>'','instagramUrl'=>''];
            $queue->from = "no-reply";
            $queue->recipient = $user;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
            $queue->save();
    }

    /*public function endPromotion()
    {
        $this
    }*/


    /*public function sendPromotionEndNotification()
    {
        $str_pro_id=1;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'promotionend'])->one();
        $storepromotion = \common\models\StorePromotions::find()->where(['id' => $str_pro_id])->one();
        $consumerPromotion = \common\models\ConsumerPromotions::find()->where(['id' => $storepromotion->promotionId])->one();
        $store = \common\models\stores::find()->where(['id' => $storepromotion->storeId])->one();
        $user = \common\models\User::find()->where(['id' => $storepromotion->store->adminId])->one();
        //var_dump($template);die;
        $queue = new \common\models\EmailQueue;
            $queue->models = ['user'=>$user,'store'=>$store,'storepromotion'=>$storepromotion,'consumerPromotion'=>$consumerPromotion] ; 
            $queue->replacements =['siteUrl'=>'','Logo'=>'',
                                    'EmailFacebookImage'=>'','EmailTwitterImage'=>'','EmailInstagramImage'=>'',
                                    'facebookUrl'=>'','twitterUrl'=>'','instagramUrl'=>''];

            $queue->from = "no-reply";
            $queue->recipient = $user;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
            $queue->save();
    }*/

    public function willEndToday()
    {
        foreach ($this->storePromotions as $promotion) {
            $promotion->willEndToday();
        }    

        if($this->toDate <= date('Y-m-d H:i:s')){
            $this->status = "ended";
            $this->save(false);
        }
    }

    public function getFilterAttributes(){
        $query = Attributes::find()
        ->join('JOIN', 'AttributeValues av', 'Attributes.id = av.attributeId')
        ->join('JOIN', 'ConsumerPromotionProducts cpp', 'cpp.productId = av.productId')
        ->join('JOIN', 'ConsumerPromotions cp', 'cp.id = cpp.promotionId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = cpp.productId')
        ->join('JOIN', 'Products p', 'sp.productId = p.id')
        ->where(['cp.id' => $this->id, 'sp.enabled' => '1', 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere("(Attributes.field='dropdown' OR Attributes.field='multiselect') AND Attributes.system = 0 AND av.value != ''")
        ->groupBy('Attributes.id');
        $attributes = $query->all();
	$filterAttributes = [];
        foreach ($attributes as $key=>$attr) {
            $filterAttributes[$key] = new \stdClass;
            $filterAttributes[$key]->code = $attr['code'];
            $filterAttributes[$key]->id = $attr['id'];
            $filterAttributes[$key]->title = $attr['title'];
        } 
        return $filterAttributes;
    }

    public function getFilterAttributeValues($code){
        $query = AttributeValues::find()
        ->join('JOIN', 'Attributes a', 'a.id = AttributeValues.attributeId')
        ->join('JOIN', 'ConsumerPromotionProducts cpp', 'cpp.productId = AttributeValues.productId')
        ->join('JOIN', 'ConsumerPromotions cp', 'cp.id = cpp.promotionId')
        ->join('JOIN', 'StoreProducts sp', 'sp.productId = cpp.productId')
        ->join('JOIN', 'Products p', 'sp.productId = p.id')
        ->where(['cp.id' => $this->id, 'sp.enabled' => '1', 'p.status' => '1']);
        if(Yii::$app->id != "app-b2b")
            $query->andWhere(['sp.storeId' => Yii::$app->params['storeId']]);
        $query->andWhere("(a.field='dropdown' OR a.field='multiselect') AND a.system = 0 AND AttributeValues.value != '' AND a.code='$code'")
        ->groupBy('`AttributeValues`.`value`');
        return $query->all();
    }

    public function getProductsBrands(){
        //$categoryIds = array_merge(ArrayHelper::map($this->getChildCategories(), 'id', 'id'), [$this->id]);
        $storeId = Yii::$app->params['storeId'];
        $query = \common\models\Brands::find();
        $brands = $query
                    ->from('Brands')
                    ->join('JOIN',
                        'Products as p',
                        'p.brandId = Brands.id'
                    )
                    ->join('JOIN',
                        'ProductCategories as pc',
                        'pc.productId = p.id'
                    )
                    ->join('JOIN',
                        'ConsumerPromotionProducts as cpp',
                        'cpp.productId = p.id'
                    )
                    ->join('JOIN',
                        'ConsumerPromotions as cp',
                        'cp.id = cpp.promotionId'
                    )
                    ;
                    if(Yii::$app->id != "app-b2b"){
                        $query->join('JOIN',
                            'StoreProducts as sp',
                            'sp.productId = p.id'
                        );
                    }    
            $query  ->groupBy('p.brandId')
                    ->andWhere("p.status = '1' and Brands.enabled = '1' and Brands.isVirtual = '0' and cp.id=".$this->id."");
                    if(Yii::$app->id != "app-b2b"){
                        $brands->andWhere("sp.enabled=1")->andWhere("sp.storeId = ".$storeId);
                    }
                    $brands->andWhere('(pc.storeId = "'.$storeId.'" OR pc.storeId = 0)');

        return $brands->all();            
    }

    public function getProductsMaximumPrice(){
        if($maxValue = AttributeValues::findBySql("SELECT av.value FROM AttributeValues av join ConsumerPromotionProducts cpp on av.productId = cpp.productId where attributeId = 10 and cpp.promotionId = ".$this->id." order by av.value+0 desc limit 1")->one())
            return $maxValue->value;
        else
            return 14000;
    }

    public function getNotification(){
        return '<a><img src='.Yii::$app->params["rootUrl"].'/store/site/email/promotion-notification.jpg></a>'; 
    }

}
