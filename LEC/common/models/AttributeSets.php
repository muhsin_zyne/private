<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "AttributeSets".
 *
 * @property integer $id
 * @property string $title
 * @property integer $basedOnSet
 */
class AttributeSets extends \yii\db\ActiveRecord
{

    public function search($params) {
        $query = AttributeSets::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //var_dump($params); die;
        $params = ['title'=>'sdfdsfsdfe'];
        $this->setAttributes($params);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        //var_dump($this->params); die;
        $this->addCondition($query, 'title');
        $this->addCondition($query, 'basedOnSet', true);
        // $this->addCondition($query, 'att2');
        // $this->addCondition($query, 'att2', true);

        return $dataProvider;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AttributeSets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['basedOnSet'], 'integer'],
            [['basedOnSet'], 'required'],
            [['title'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'basedOnSet' => 'Based On Set',
        ];
    }

    // public function getAvailableChildProducts($superAttributes){
    //     return Products::find()
    //     ->join('LEFT JOIN', 'AttributveValues as av', 'av.productId = Products.id')
    //     ->onCondition('attributeId in ('.implode(",", $superAttributes).')')
    //     //->where('attributeId in ('.implode(",", $superAttributes).')')
    //     ->andWhere('value!=""')
    //     ->andWhere('Products.typeId = "simple"')
    //     ->groupBy('Products.id');
    // }

    // TAKES TOO MUCH TIME FOR PROCESSING
    public function getAvailableChildProducts($superAttributes){ //var_dump($superAttributes); die;
        $products = $values = []; 
        $avalues = AttributeValues::find()
        ->join('JOIN', "Products as p", 'p.id = AttributeValues.productId')
        ->where('p.typeId = "simple" AND storeId = 0')
        ->andWhere('attributeId in ('.implode(",", $superAttributes).')')
        ->all();
        
        foreach($avalues as $avalue){
            $values[$avalue->productId."-".$avalue->attributeId] = $avalue;
        }
        $storeId = isset(Yii::$app->user->identity->store)? Yii::$app->user->identity->store->id : null;
        //var_dump(Yii::$app->params['storeId']); die;
        $searchModel = new \common\models\search\ProductsSearch();
        //var_dump($searchModel->BEFORE_QUERY); die;
        $query = $searchModel->search(Yii::$app->request->queryParams)->query;
        $productsQuery = $this->getProducts();
        $query->join = $productsQuery->join;
        $query->andWhere($productsQuery->where);
        $query->where = \backend\components\Helper::alterCondition($query->where, "(Products.visibility = 'everywhere')", " 1 = 1");
        //if(!is_null($storeId)){
        //    $setProducts = $query->join('JOIN', 'StoreProducts sp', 'sp.productId = Products.id')->andWhere('typeId = "simple"')->andWhere("sp.storeId = $storeId")->all();
        //}else
	//$_REQUEST['bypassStoreProductsCheck'] = true;
            $setProducts = $query->andWhere('typeId = "simple"')->all();
     	//unset($_REQUEST['bypassStoreProductsCheck']);
        foreach($setProducts as $product){
            if($product->typeId=="simple"){
                $prod = false;
                foreach($superAttributes as $attr){
                    if(is_array($attr))
                        continue;
                    if(isset($values[$product->id."-".$attr])){
                        if($values[$product->id."-".$attr]->value==""){
                            $prod = false;
                            break;
                        }else
                            $prod = $product;
                    }else{
                        $prod = false;
                        break;
                    }
                }
                if($prod)
                    $products[] = $prod;
                // if(count($products)>1)
                //     break;
            }
        }
        //var_dump($products); die;
        return $products;
    }

    public function getProducts(){
        return $this->hasMany(Products::className(), ['attributeSetId' => 'id']);
    }

    public function getAttributeGroups()
    {
        return $this->hasMany(AttributeGroups::className(), ['attributeSetId' => 'id'])->orderBy('position');
    }
    
}

