<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "StoreBrands".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $brandId
 */
class StoreBrands extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StoreBrands';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'brandId'], 'required'],
            [['storeId', 'brandId'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'brandId' => 'Brand ID',
        ];
    }

    public function setProducts(){
        $this->brand->setProducts($this->storeId);
        return true;
    }

    public function unsetProducts(){
        $this->brand->unsetProducts($this->storeId);
        return true;
    }

    public function getBrand(){
        return $this->hasOne(Brands::className(), ['id' => 'brandId']);
    }
    public function getStore(){
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }
    public function getStoreName() {
        return $this->store->title;
    }
    public function getStoreEmail() {
        return $this->store->email;
    }
    public function getStoreBillingAddress() {
        return $this->store->billingAddress;
    }
    public function getStoreShippingAddress() {
        return $this->store->shippingAddress;
    }
    public function getStorePhone() {
        return $this->store->phone;
    }
    public function getMemberNumber() {
        return $this->store->admin->jenumber;
    }
}
