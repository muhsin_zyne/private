<?php

namespace common\models;

use Yii;
use frontend\components\Helper;

/**
 * This is the model class for table "StorePromotions".
 *
 * @property integer $id
 * @property integer $storeId
 * @property integer $promotionId
 * @property string $startDate
 * @property string $endDate
 */
class StorePromotions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'StorePromotions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['storeId', 'promotionId'], 'required'],
            [['storeId', 'promotionId'], 'integer'],
            [['startDate', 'endDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'storeId' => 'Store ID',
            'promotionId' => 'Promotion ID',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
        ];
    }
    public function getPromotionStartDate()
    {
         return Helper::date($this->startDate);
    }
    public function getPromotionEndDate()
    {
        return Helper::date($this->endDate);
    }
    
    public function getStore()
    {
        return $this->hasOne(Stores::className(), ['id' => 'storeId']);
    }

    public function sendPromotionEndNotification()
    {
        $str_pro_id= $this->id;
        $template = \common\models\EmailTemplates::find()->where(['code' => 'promotionend'])->one();
        $storepromotion = \common\models\StorePromotions::find()->where(['id' => $str_pro_id])->one();
        $consumerPromotion = \common\models\ConsumerPromotions::find()->where(['id' => $storepromotion->promotionId])->one();
        $store = \common\models\stores::find()->where(['id' => $storepromotion->storeId])->one();
        $user = \common\models\User::find()->where(['id' => $storepromotion->store->adminId])->one();
        //var_dump($template);die;
        $queue = new \common\models\EmailQueue;
            $queue->models = ['user'=>$user,'store'=>$store,'storepromotion'=>$storepromotion,'consumerPromotion'=>$consumerPromotion] ; 
            $queue->replacements =['siteUrl'=>'','Logo'=>'',
                                    'EmailFacebookImage'=>'','EmailTwitterImage'=>'','EmailInstagramImage'=>'',
                                    'facebookUrl'=>'','twitterUrl'=>'','instagramUrl'=>''];

            $queue->from = "no-reply";
            $queue->recipient = $user;
            $queue->creatorUid = Yii::$app->user->id;
            $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
            $queue->save();
    }

    public function getPromotion()
    {
        return $this->hasOne(ConsumerPromotions::className(), ['id' => 'promotionId']);
    }

    public function getB2bUserName()
    {
        if($this->storeId!='')
        {
            $userid=$this->store->adminId;
        }
        else
        {
            $userid=$this->b2bUserId;
        }
        $user =\common\models\User::find()->where(['id'=>$userid])->one();
        return ucfirst($user->firstname).' '.ucfirst($user->lastname);
    }

    public function willEndToday()
    {
        if($this->endDate <= date('Y-m-d H:i:s')){
            $this->active = 0;
            //$this->sendPromotionEndNotification();
            $this->save(false); 
        }
    }

}
