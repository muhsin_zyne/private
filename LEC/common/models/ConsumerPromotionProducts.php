<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ConsumerPromotionProducts".
 *
 * @property integer $id
 * @property integer $promotionId
 * @property integer $productId
 * @property string $offerSellPrice
 * @property string $offerCostPrice
 * @property integer $pageId
 */
class ConsumerPromotionProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ConsumerPromotionProducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promotionId', 'productId', 'pageId'], 'integer'],
            [['offerSellPrice', 'offerCostPrice'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promotionId' => 'Promotion ID',
            'productId' => 'Product ID',
            'offerSellPrice' => 'Offer Sell Price',
            'offerCostPrice' => 'Offer Cost Price',
            'pageId' => 'Page ID',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'productId']);
    }

    public function getPromotionDetails(){
        return $this->hasOne(ConsumerPromotions::className(), ['id' => 'promotionId']);
    }

    public function getPromotionProduct(){
        return $this;
    }

    public function getSelf(){
         return $this;
    }

}
