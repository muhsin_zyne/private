<?php

namespace common\models;

use Yii;
use common\models\BrandSuppliers;
/**
 * This is the model class for table "BrandSuppliers".
 *
 * @property integer $id
 * @property integer $brandId
 * @property integer $supplierUid
 *
 * @property Brands $brand
 * @property Users $supplierU
 */
class BrandSuppliers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BrandSuppliers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brandId', 'supplierUid'], 'required'],
            [['brandId', 'supplierUid'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brandId' => 'Brand ID',
            'supplierUid' => 'Supplier Uid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brandId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(User::className(), ['id' => 'supplierUid']);
    }
}
