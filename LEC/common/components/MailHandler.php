<?php
namespace common\components;
use common\models\Configuration;
use Yii;
	class MailHandler extends \yii\base\Component {
		var $delivery = 'SwiftMailer'; // Valid are Postmark and 'PHPMailer.

		public function init(){ //var_dump(Configuration::findSetting('smtp_username')."|".Configuration::findSetting('smtp_password')); die;
			Yii::$app->mail->transport = [
				'class' => 'Swift_SmtpTransport',
	            'host' => Configuration::findSetting('smtp_host'),  // e.g. smtp.mandrillapp.com or smtp.gmail.com
	            'username' => Configuration::findSetting('smtp_username'),
	            'password' => Configuration::findSetting('smtp_password'),
	            'port' => Configuration::findSetting('smtp_port'), // Port 25 is a very common port too
	            'encryption' => Configuration::findSetting('smtp_encryption'),
			];
			//var_dump(Configuration::findSetting('smtp_username')); die;
			return parent::init();
		}
		public function sendMail($email){
			if($this->delivery=='SwiftMailer'){
				$recipients = explode(",", $email->recipients); //var_dump($recipients); die;
				if($recipients=="" || is_null($recipients) || $recipients[0] == "")
				    return;
				$mail = Yii::$app->mail->compose()
			    ->setFrom($email->from)
			    ->setReplyTo($email->replyTo)
			    ->setTo($recipients)
			    ->setBcc('mahesh.s@effidex.com')
			    ->setBcc('mark@xtrastaff.com.au')
			    ->setSubject($email->subject)
			    ->setHtmlBody($email->message);
			    if(!is_null($email->attachments) && $email->attachments != "[]" && $email->attachments != ""){
                                $paths = json_decode($email->attachments, true);
				//var_dump($email->attachments); die;
                                foreach($paths as $path => $fileName){
                                    if(file_exists($path)){
                                        $mail->attach($path, ['fileName' => $fileName]);
                                    }
                                }
                            }
			    // ->send();
				try {
					$sent = $mail->send();
					echo "\nSent '".$email->subject."' to ".$email->recipients;
					$email->save();
				}catch(Exception $e){
					$email->params = $e->getMessage();
					$email->status = 'error';
					$email->save();
				}
				return $sent;
			}
			return false;//$this->Send();
		}
	}

?>
