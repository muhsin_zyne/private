<?php
/**
 * Author: Dan
 */
namespace common\components;
use Yii;
use yii\base\Component;

class ICSGenerator  extends Component
{
	public $filename;
	public $datestart;
	public $dateend;
	public $description;
	public $summary;
	public $address;
	public $location;

	function dateToCal($timestamp) {
  		return date('Ymd\THis\Z', $timestamp);
	}
	function escapeString($string) {
  		return preg_replace('/([\,;])/','\\\$1', $string);
	}

	public function save(){
		$content = "BEGIN:VCALENDAR\nVERSION:2.0\nMETHOD:PUBLISH\nBEGIN:VEVENT\nDTSTART:".date("Ymd\THis\Z",strtotime($this->datestart))."\nDTEND:".date("Ymd\THis\Z",strtotime($this->dateend))."\nLOCATION:".$this->location."\nTRANSP: OPAQUE\nSEQUENCE:0\nUID:\nDTSTAMP:".date("Ymd\THis\Z")."\nSUMMARY:".$this->summary."\nDESCRIPTION:".$this->description."\nPRIORITY:1\nCLASS:PUBLIC\nBEGIN:VALARM\nTRIGGER:-PT10080M\nACTION:DISPLAY\nDESCRIPTION:Reminder\nEND:VALARM\nEND:VEVENT\nEND:VCALENDAR\n";
		
		$fp = fopen(\Yii::$app->basePath."/..".Yii::$app->params["attachmentspath"].$this->filename,"wb");
		fwrite($fp,$content);
		fclose($fp);
		return $this->filename;
	}
}	

?>