<?php
namespace common\components;
use Yii;

class MainController extends \yii\web\Controller
{	
	public function beforeAction($action){
		Yii::$app->log->traceLevel = 3;
		$this->view->registerJs('$(document).on("pjax:timeout", function(event) {
          // Prevent default timeout redirection behavior
          event.preventDefault()
        });');
        Yii::$app->session->timeout = 720000;
		Yii::$app->session->open(); //var_dump(Yii::$app->session->id); die;
        return true;
    }    

}