<?php
/**
 * Author: Max
 */
namespace common\components;
use Yii;
class CSVExport extends \yii\grid\GridView
{
	public $file;

	public function init(){
		$this->file = 'store/reports/'.time().'.csv';
		return parent::init();
	}
	public function renderTableHeader()
	{
	    $fp = fopen("../../".$this->file, 'w');
 	    $stack = array();
 	    foreach($this->columns as $column){
 	     	ob_start();
 	     	echo $column->renderHeaderCell();
 	     	$stack[] = trim(strip_tags(ob_get_clean()));
 	    } //var_dump($stack); die;
	    fputcsv($fp, $stack);
	}

	public function renderTableRow($model, $key, $index){
		$fp = fopen("../../".$this->file, 'a');
		$stack = array();
		foreach($this->columns as $column){
			ob_start();
			echo $column->renderDataCell($model, $key, $index);
			$stack[] = str_replace('&nbsp;', ' ', trim(strip_tags(ob_get_clean())));
		}
		fputcsv($fp, $stack);
	}

	public function renderItems(){
        $caption = $this->renderCaption();
        $columnGroup = $this->renderColumnGroup();
        $tableHeader = $this->showHeader ? $this->renderTableHeader() : false;
        $tableBody = $this->renderTableBody();
        $tableFooter = $this->showFooter ? $this->renderTableFooter() : false;
        $content = array_filter([
            $caption,
            $columnGroup,
            $tableHeader,
            $tableFooter,
            $tableBody,
        ]);
        return Yii::$app->controller->redirect(Yii::$app->params['rootUrl']."/".$this->file);
    }
}