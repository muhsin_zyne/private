<?php
    use yii\helpers\Html;
    use frontend\components\Helper;
   // $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->orderItem->product->getThumbnailImage();
?>
<tr>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$index +1?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
        <?=Helper::stripText($model->orderItem->product->name,30) ?><br>
        <span style="color:#e31837;"><?=Helper::money($model->orderItem->price)?></span>
    </td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$model->quantityInvoiced?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;"><?=Helper::money($model->orderItem->discount)?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;"><?=Helper::money($model->orderItem->giftWrapAmount* $model->orderItem->quantity)?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;"><?=Helper::money(($model->orderItem->price * $model->quantityInvoiced)-$model->orderItem->discount+($model->orderItem->giftWrapAmount*$model->quantityInvoiced))?></td>
</tr>
       
       

