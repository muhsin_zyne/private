<?php

use yii\helpers\Html;
use frontend\components\Helper;

?>

<!DOCTYPE html>
<head>
<!-- <style>a{color:#3a3138;}</style> -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="background:#eeeeee;margin:8px 0 0;">


<table bgcolor="#fff"  width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
        	<td style="padding:25px 0 25px 15px;">
            	<a href="" style="color:#3a3138;"><?=$invoice->order->store->logo?></a>
            </td>
            <td valign="middle" style="text-align:right;padding:15px;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;">
                	<tr>
                    	<td style="font-size:25px;color:#5b5b5b;font-weight:bold;padding-bottom:10px;">Tax Invoice</td>
                    </tr>
                    <tr>
                    	<td style="font-size:14px;color:#5b5b5b;color:#535353;padding-bottom:2px;">Invoice No: <span style="font-weight:bold;">#<?=$invoice->invoiceId?></span></td>
                    </tr>
                    <tr>
                    	<td style="font-size:14px;color:#5b5b5b;color:#535353;">ABN: <span style="font-weight:bold;font-size:15px;"><?=$invoice->order->store->abn?></span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td height="8" bgcolor="#e31837" colspan="2"></td>
        </tr>
    </tbody>    
</table>

<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        
        <tr>
        	<td style="padding:20px;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                    <tr>
                    	<td style="font-size:16px;">Thank you for your order. This is the Tax Invoice for Order #<?=$invoice->order->id?>.</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
        	<td valign="top" style="padding:15px 10px;border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                	<tr>
                    	<td width="284" valign="top"  style="padding:15px 3px 15px 15px;background:#f6f6f6;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                            	<tr>
                                	<td style="padding-bottom:8px;font-weight:bold;">Invoice Sent From:</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:3px;"><?=$invoice->order->orderDeliveredAddress?></td>
                                </tr>
                            </table>
                        </td>
                        <td width="12"></td>
                        <td width="284" valign="top"  style="padding:15px 3px 15px 15px;background:#f6f6f6;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                            	<tr>
                                	<td style="padding-bottom:8px;font-weight:bold;">Invoice To :</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:3px;"><?=$invoice->order->billingAddressText?></td>
                                </tr>
                            </table>
                        </td>
                        
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td style="padding:20px 10px 0;text-align:center;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                	<tr>
                    	<td colspan="3" style="font-size:16px;font-weight:bold;padding-bottom:8px;">Invoice Details</td>
                    </tr>
                    <tr>
                    	<td style="background:#f0f0f0;font-size:13px;padding:5px 3px;">
                        	Invoice No:<span style="font-weight:bold;">#<?=$invoice->invoiceId?></span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 3px;">
                        	Invoice Date:<span style="font-weight:bold;"><?=$invoice->invoicePlacedDate?></span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 3px;">
                        	Invoice Total:<span style="font-weight:bold;"><?=$invoice->invoiceGrandTotalFormatted?></span>
                        </td>
                    </tr>
                    
                    
                    <tr>
                    	<td colspan="3" style="padding-top:8px;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead  style="background:#5b5b5b;color:#fff;text-transform:uppercase;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Qty</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Discount</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Wrapping</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item Total</th>
                                  </tr>
                                </thead>
                                <tbody valign="top">
                                  <?=$invoice->pdfShortGrid?> 
                                  <tr>
                                  	<td colspan="5" style="padding:4px 3px;text-align:right;">Item Subtotal:</td>
                                    <td style="padding:4px 3px;text-align:right;"><?=$invoice->invoiceItemTotalFormatted?></td>
                                  </tr>
                                  <tr>
                                  	<td colspan="5" style="padding:4px 3px;text-align:right;">Gift Wrapping:</td>
                                    <td style="padding:4px 3px;text-align:right;"><?=$invoice->order->giftWrapTotalFormatted?></td>
                                  </tr>
                                  <tr>
                                  	<td colspan="5" style="padding:4px 3px;text-align:right;">Discount Applied:</td>
                                    <td style="padding:4px 3px;text-align:right;"><?=$invoice->order->formatedOrderDiscountAmount?></td>
                                  </tr>
                                  <tr>
                                  	<td colspan="2" style="padding:5px 3px;background:#f0f0f0;">The Invoice includes <span style="font-weight:bold;">GST of <?=$invoice->order->formattedAppliedTax?></span></td>
                                    <td colspan="3" style="padding:5px 3px;text-align:right;background:#f0f0f0;font-weight:bold;">Invoice Total (inc. GST):</td>
                                    <td style="padding:5px 3px;text-align:right;background:#f0f0f0;font-weight:bold;"><?=$invoice->invoiceGrandTotalFormatted?></td>
                                  </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding:10px 3px;color:#797979;font-size:13px;text-align:right;border-bottom:1px solid #eeeeee;">Paid using <?=$invoice->order->paymentDetails?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#fff;">
    <tbody>
        <tr>
          <td bgcolor="#1d1d1d" width="100%" style="">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="width:600px!important;padding:18px 0;font-family:Arial, Helvetica, sans-serif; font-size:11px;text-align:center;color:#211e21;">
                <tr>
                    <td style="text-align:center;">
                    	<a href="" style="color:#3a3138;"><img  alt="" src="<?=Yii::$app->params["rootUrl"]."/store/site/email/footer-logo.jpg"?>" alt=""></a>
                    </td> 
                </tr>
            </table>
          </td>
        </tr>
    </tbody>
</table>
</body>