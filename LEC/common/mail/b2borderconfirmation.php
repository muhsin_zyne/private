

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4" align="center"> <a href="[!siteUrl]"> [!Logo] </a></td></tr>
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Order Confirmation</h2> </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;"><b>Order : #[!order_OrderId]</b></td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!userfullname], </h2> </td>
        </tr>
            <tr>
                <td colspan="2" style="vertical-align:middle;font-size:12px;">
                    <p style="margin:5px 0;">Thank you for your order. This email confirms the order has been processed and forwarded to the relevant suppliers. The details of your order follow however in the case of large orders only the first ten lines are shown. To view all items click ‘See All Items’ for redirection to the website.</p>
                </td>
            </tr>
            <tr><td colspan="2"><b><h2>Order Details</h2></b></td></tr>
            <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr><td colspan="2">Order : #[!order_OrderId]</td></tr>
            <!-- <tr><td colspan="2">Placed on [!order_OrderPlacedDate]</td></tr> -->
            <tr>
                <td colspan="2" style="font-size:9px;">
                    [!order_b2bShortGrid]
                </td>
            </tr>
            <tr>
                <td style="padding:10px;vertical-align:middle;text-align:right;" colspan="8"> [!allorderurl]
                 </td>           
                </td>
            </tr>
            <!-- <tr><td></td></tr><p><br/></p> -->
            <tr style=" border-top: 1px solid #000; ">
                <td width="70%" style="padding: 10px;">
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;" >
                                <b>Order Total: </b>
                            </td>
                        </tr>
                    </table>
                </td><td width="30%">
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;">
                                <b>[!order_GrandTotalFormatted]</b>
                            </td>
                        </tr>
                    </table>         
                </td>
            </tr>
            <tr><td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td></tr>
            <tr>  <td colspan="2" style="padding: 8px; padding-left: 0px;"><p>[!customerNotes]</p></td>  </tr>
            <tr>
                <td colspan="2">
                    <p>This order has more than one locations as delivery addresses. If the order contains 10 items or more, please click 'See all items' to have a comprehensive view of the order.</p><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>If you have any questions or wish to make changes to this order please contact the relevant supplier direct. Goods will only be supplied if your LEG central billing account is in good standing otherwise the order will be treated as pending. Feedback on this platform and the order process can be sent to Leading Edge using the Contact Us form on the website.</p><br/>
                </td>
            </tr>
        </tbody>
    </table>
</div>