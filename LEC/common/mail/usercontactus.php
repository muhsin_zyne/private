<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td colspan="2" align="center">[!headerLogo]</td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding:30px 20px;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear Admin,</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;">The following request has been made </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
          <td valign="top" style="background:#f2f2f2;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                  <tr>
                      <td valign="top" style="padding:15px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                  <td style="padding-bottom:5px;font-weight:bold;width:175px;">Name:</td>
                                    <td style="padding-bottom:5px;">[!name]</td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:5px;font-weight:bold;width:175px;">Email:</td>
                                    <td style="padding-bottom:5px;">[!email]</td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:5px;font-weight:bold;width:175px;">Phone:</td>
                                    <td style="padding-bottom:5px;">[!phone]</td>
                                </tr>
                                [!address]
                                <tr>
                                  <td valign="top" style="padding-bottom:5px;font-weight:bold;width:175px;">Message:</td>
                                    <td valign="top" style="padding-bottom:5px;">[!body]</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
          <td style="padding:16px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Please respond to the customer at your earliest opportunity.
            </td>
        <tr> 
        <tr>
          <td style="padding:16px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Thank you from the Support Office !
            </td>
        <tr> 
        
    </tbody>
</table>


