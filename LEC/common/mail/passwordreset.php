<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
            <td colspan="2" align="center">[!headerLogo]</td>
        </tr>
        
        <tr>
            <td colspan="2" style="padding:30px 20px 0;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                    <tr>
                        <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!user_fullname],</td>
                    </tr>
                    <tr>
                        <td style="font-size:16px;">We received a request to reset the password associated with this email address. Please click on the link below to complete</td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                           [!user_passwordResetUrl]
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            If you did not request to have your password reset you can safely ignore this email. Rest assured your customer account is safe.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            If you repeatedly receive these password reset notifications or feel that your account may be subject to attempted fraudulent activity, please notify us as soon as posible using the <span style="text-decoration:underline;">[!storeContactus]</span> form on our website.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:16px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                            Thank you!
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
    </tbody>
</table>


