<div class="wrapper" style="width:600px;background:#fff;font-family:Arial, Helvetica, sans-serif;font-size:12px">
<div class="logo" style="background:#fff;padding:5px;text-align:center;">[!store_logo]</div>
<div class="con" style="padding:5px;color:#0C0C0C;">
<div class="info">
<b style="margin:0 0 5px 0;font-size:18px;">Hi Admin,</b><br>
[!takeaphoto_name] has sent us a picture of a piece he likes.
</div>
<div class="user">
<b style="margin:0 0 5px 0;font-size:18px;">Details</b>
<hr style="margin:3px 0;padding:0;" />
<p style="margin:5px 0;padding:0;"><b>Photo:</b>
<!--<a href="'.$madir.$new_file_name.'">[!takeaphoto_photo]</a>-->
[!takeaphoto_photoUrl]
</p>
<p style="margin:5px 0;padding:0;"><b>Price Range : </b> [!takeaphoto_priceRange]</p>
<p style="margin:5px 0;padding:0;"><b>Name : </b> [!takeaphoto_name]</p>
<p style="margin:5px 0;padding:0;"><b>Email : </b> [!takeaphoto_email]</p>
<p style="margin:5px 0 15px 0;padding:0;"><b>Phone : </b> [!takeaphoto_phone]</p>
</div>
<div class="msg">
<b style="margin:0 0 5px 0;font-size:18px;">Comments</b>
<hr style="margin:3px 0;" />
<p style="margin:5px 0;padding:0;">
[!takeaphoto_comment]
</p>
</div>
</div>
</div>