<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
            <td align="center">[!order_orderNotificationMailBanner]</td>
        </tr>
        
        <tr>
            <td width="100%" style="padding:30px 20px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                    <tr>
                        <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Congratulations!</td>
                    </tr>
                    <tr>
                        <td style="font-size:16px;padding-bottom:13px;"> Congratulations, you've just recieved a Client Portal order. The details of the order follow, however in the case of large orders only the first 10 lines are shown. Please click to <a href="" style="color:#5e5e5e;">[!order_orderurl]</a>.</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td valign="top" style="padding:15px 10px;border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                    <tr>
                        <td width="284" valign="top"  style="padding:15px 3px 15px 15px;background:#f6f6f6;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                    <td style="padding-bottom:8px;font-weight:bold;">Customer Details:</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:3px;">[!order_billingAddressText]</td>
                                </tr>
                                
                            </table>
                        </td>
                        <td width="12"></td>
                        <td width="284" valign="top" style="padding:15px 3px 15px 15px;background:#f6f6f6;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                    <td style="padding-bottom:8px;font-weight:bold;">Collection Address:</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:3px;">[!order_orderDeliveredAddress]</td>
                                </tr>
                                
                            </table>
                        </td>
                        
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="padding:15px 10px 0;text-align:center;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                    <tr>
                        <td colspan="2" style="font-size:16px;font-weight:bold;padding-bottom:8px;">Order Summary</td>
                    </tr>
                    
                    <tr>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;">
                            Order Id:<span style="font-weight:bold;color:#535353;"># [!order_OrderId]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                            Client Portal Code:<span style="font-weight:bold;">[!clientPortal_studentCode]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                            Placed On:<span style="font-weight:bold;color:#535353;">[!order_OrderPlacedDate]</span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="3" style="padding-top:8px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;">Qty</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Discount</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Gift Wrapping</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item Total</th>
                                  </tr>
                                </thead>
                                <tbody valign="top">
                                    [!order_shortGrid]
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td>
                            <a href="" style="text-decoration:none;"><span style="padding:12px 15px;border:1px solid #5b5b5b;font-size:13px;color:#5b5b5b;">See all items</span></a>
                        </td>
                        <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <tr>
                                    <td colspan="" style="padding:4px 3px;text-align:right;">Item Subtotal:</td>
                                    <td width="100" style="padding:4px 3px;text-align:right;">[!order_orderItemTotalFormatted]</td>
                                </tr>
                                <tr>
                                    <td colspan="" style="padding:4px 3px;text-align:right;">Gift Wrapping:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_giftWrapTotalFormatted]</td>
                                </tr>
                                <tr>
                                    <td colspan="" style="padding:4px 3px;text-align:right;">Discount Applied:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_formatedOrderDiscountAmount]</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="padding:0 10px 0;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;font-size:14px;">
                    <tr>
                        <td style="padding:5px 4px;text-align:right;background:#f0f0f0;font-weight:bold;">Order Total:</td>
                        <td width="100" style="padding:5px 4px;text-align:right;background:#f0f0f0;font-weight:bold;">[!order_GrandTotalFormatted]</td>
                    </tr>
                </table>
            </td>    
        </tr>
        
        <tr>
            <td style="padding:5px 10px 8px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;">
                    <tr>
                        <td style="font-size:13px;padding:5px 3px 8px;border-bottom:1px solid #eeeeee;color:#424242;">
                            Order Status:<span style="color:#6a8b50;">[!order_status]</span>
                        </td>
                        <td style="padding:5px 3px 8px;color:#797979;font-size:13px;text-align:right;border-bottom:1px solid #eeeeee;">[!order_paymentDetails]</td>
                    </tr>
                </table>
           </td>
        </tr>
        
        <tr>
            <td style="padding:20px 15px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                For your security we recommend that you confirm the payment by cross-checking with the payment advice from your payment gateway provider. In the case of gift voucher redemptions, you can access transaction history and live balances by logging into your admin dashboard.
            </td>
        <tr>
        
    </tbody>
</table>


