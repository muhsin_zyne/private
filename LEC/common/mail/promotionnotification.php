<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;
                line-height:20px; font-size:13px; color:#222;">
                <table style="width:100%; border-collapse:
                  collapse;border:none;font-size:12px;">
                  <tbody>
                    <tr><td colspan="4" align="center"> <a href="[!siteUrl]"> [!logo] </a></td></tr>
                    <tr>
                      <td colspan="2" style="vertical-align: middle;">
                        <h2 style="margin:0 0 5px 0;font-size:18px;">Consumer Promotion</h2>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" style="vertical-align:
                        middle;font-size:12px;">
                        <p style="margin:5px 0;">Leading Edge Jewellers has created a promotion, selling some of the products at a discounted price to boost sales.You may accept or reject this by clicking on the buttons in this email. To see the list of all products please click the `See All` link.</p>
                        <br>
                        <h2 style="margin:0;font-size:15px;margin:0 0
                          5px 0;">Promotion Details</h2>
                       <!--  <b>Name: </b> [!promotion_title]<br>
                        <b>Short tag: </b> [!promotion_shortTag]<br>
                        <b>Start Date: </b> [!promotion_fromFormatted]<br>
                        <b>End Date: </b> [!promotion_toFormatted]<br> -->
                        <b>Name: </b> Thanks Mum May -2016<br><br>
                        <b>Short Tag: </b> MDAY2016<br><br>
                        <b>Start Date: </b> 24/02/2016<br><br>
                        <b>End Date: </b> 05/08/2016<br><br>
                        <br>
                        <p style="margin:5px 0;">If you decide to 'Accept' this promotion, these products will have the revised price from 24/02/2016 to 05/08/2016 (or the custom date range which you can choose from the website backend). At 12:01 am 05/08/2016 (or your preferred end date), the price will revert back to normal.</p>
                        <p style="margin:5px
                          0;font-size:11px;font-style: italic;">* The "selling price" mentioned here may differ if you have set a separate/exclusive price for your store. If these products are sold at a different price on your sites, do not worry, at 12:01 am on 05/08/2016 (or your preferred end date), the price will change back to the price you currently sell them for.</p>
                        <p>To see the list of all products please click the <a style="color:#B30425;text-decoration:none;" href="http://admin.legj.com.au/b2b/promotions"> See All </a> link. </p>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <table class="product_table" style="width:100%;margin-top:10px;
                          border-collapse: collapse;font-size:12px" border="1" bordercolor="#E7E7E7">
                            <thead> 
                                <tr style="color:#191919;" bgcolor="#F8F8F8">
                                  <th style="padding:7px;vertical-align:middle;text-align: left;">Item(s)</th>
                                  <th style="padding:7px;vertical-align:center;">SKU</th>
                                  <!-- <th style="padding:7px;vertical-align:center;">EZcode</th> -->
                                  <th style="padding:7px;vertical-align:center;">Original Selling Price</th>
                                  <th style="padding:7px;vertical-align:center;">Offer Selling Price</th>
                                  <th style="padding:7px;vertical-align:center;">Original Cost Price</th>
                                  <th style="padding:7px;vertical-align:center;">Offer Cost Price</th>
                                  <th style="padding:7px;vertical-align:center;">Page No</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Cubic Zirconia Inside-Out Huggies </td>
                                  <td style="padding:10px;vertical-align:center;">CH10651</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">249.00</td>
                                  <td style="padding:10px;vertical-align:center;">239.00</td>
                                  <td style="padding:10px;vertical-align:center;">69.49</td>
                                  <td style="padding:10px;vertical-align:center;">65.60</td>
                                  <td style="padding:10px;vertical-align:center;">1</td>
                                </tr>
                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Diamond 0.30ct TDW Ribbon Crossover Ring</td>
                                  <td style="padding:10px;vertical-align:center;">CH12745</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">1499.00</td>
                                  <td style="padding:10px;vertical-align:center;">1499.00</td>
                                  <td style="padding:10px;vertical-align:center;">498.89</td>
                                  <td style="padding:10px;vertical-align:center;">470.95</td>
                                  <td style="padding:10px;vertical-align:center;">1</td>
                                </tr>
                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Gold 0.18ct TDW Diamond Bar Pendant on Chain</td>
                                  <td style="padding:10px;vertical-align:center;">CH12771</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">899.00</td>
                                  <td style="padding:10px;vertical-align:center;">899.00</td>
                                  <td style="padding:10px;vertical-align:center;">300.44</td>
                                  <td style="padding:10px;vertical-align:center;">283.62</td>
                                  <td style="padding:10px;vertical-align:center;">1</td>
                                </tr>
                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Diamond Heart Slider Pendant</td>
                                  <td style="padding:10px;vertical-align:center;">CH8570</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">149.00</td>
                                  <td style="padding:10px;vertical-align:center;">149.00</td>
                                  <td style="padding:10px;vertical-align:center;">47.72</td>
                                  <td style="padding:10px;vertical-align:center;">45.05</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>
                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Diamond Heart Studs</td>
                                  <td style="padding:10px;vertical-align:center;">CH8580</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">159.00</td>
                                  <td style="padding:10px;vertical-align:center;">159.00</td>
                                  <td style="padding:10px;vertical-align:center;">49.26</td>
                                  <td style="padding:10px;vertical-align:center;">46.50</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>

                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">Gold-Bonded Silver Hoops</td>
                                  <td style="padding:10px;vertical-align:center;">CH9125</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">89.95</td>
                                  <td style="padding:10px;vertical-align:center;">89.95</td>
                                  <td style="padding:10px;vertical-align:center;">31.38</td>
                                  <td style="padding:10px;vertical-align:center;">29.63</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>

                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Gold Diamond Scroll Ring</td>
                                  <td style="padding:10px;vertical-align:center;">CH12073</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">229.00</td>
                                  <td style="padding:10px;vertical-align:center;">199.00</td>
                                  <td style="padding:10px;vertical-align:center;">66.37</td>
                                  <td style="padding:10px;vertical-align:center;">62.65</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>

                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Diamond Heart Slider Pendant</td>
                                  <td style="padding:10px;vertical-align:center;">TN4156</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">69.95</td>
                                  <td style="padding:10px;vertical-align:center;">69.95</td>
                                  <td style="padding:10px;vertical-align:center;">21.28</td>
                                  <td style="padding:10px;vertical-align:center;">20.09</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>

                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">9ct Diamond Promise Ring</td>
                                  <td style="padding:10px;vertical-align:center;">CH6046</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">199.00</td>
                                  <td style="padding:10px;vertical-align:center;">189.00</td>
                                  <td style="padding:10px;vertical-align:center;">53.10</td>
                                  <td style="padding:10px;vertical-align:center;">50.13</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>

                                <tr>
                                  <td style="padding:10px;vertical-align:middle;text-align: left;">Gold-Bonded Silver 3-Tone Hoops</td>
                                  <td style="padding:10px;vertical-align:center;">CH10560</td>
                                  <!-- <td style="padding:10px;vertical-align:center;">XXX</td> -->
                                  <td style="padding:10px;vertical-align:center;">69.95</td>
                                  <td style="padding:10px;vertical-align:center;">69.95</td>
                                  <td style="padding:10px;vertical-align:center;">23.16</td>
                                  <td style="padding:10px;vertical-align:center;">21.87</td>
                                  <td style="padding:10px;vertical-align:center;">2</td>
                                </tr>
                            
                            <tr>
                              <td style="padding:10px;vertical-align:
                                middle;text-align:right;" colspan="8"><a moz-do-not-send="true" style="color:#B30425;text-decoration:none;" href="http://admin.legj.com.au/b2b/promotions">See All</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:10px;vertical-align:middle;text-align:center;"><a moz-do-not-send="true" href="http://admin.legj.com.au/b2b/view?id=9"><img moz-do-not-send="true" src="http://admin.inte.everybuy.com.au/images/accept.png" alt="accept"></a></td>
                      <td style="padding:10px;vertical-align:middle;text-align:center;"><a moz-do-not-send="true" href="http://admin.legj.com.au/b2b/view?id=9"><img moz-do-not-send="true" src="http://admin.inte.everybuy.com.au/images/reject.png" alt="reject"></a></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <p>Please click on the buttons above to accept/reject this promotion. You will be taken to the login/promotion page to confirm.</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>