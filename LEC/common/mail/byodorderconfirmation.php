<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td colspan="2" align="center"><a href="">[!order_byodMailBanner]</a></td>
        </tr>            
        <tr>
          <td colspan="2" style="padding:30px 20px;">
              <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!userfullname],</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;padding-bottom:13px;">Thank you for your order.</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;padding-bottom:13px;">This email confirms that we are in receipt of your order detail. In addition, you will shortly be receiving an official Tax Invoice for your records. <br/>
NB - This will be sent once we have confirmed payment. [!portalText]</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;font-weight:bold;">The detail of your order is furnished below</td>
                    </tr>
                </table>
            </td>
        </tr>
    
        <tr>
          <td valign="top" style="padding:15px 10px;border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;">              
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                  <tr>
                      <td valign="top" style="min-height:145px;display:block;padding:15px;background:#f6f6f6;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                              <tr>
                                  <td style="padding-bottom:8px;font-weight:bold;">Customer Details:</td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:3px;">[!order_billingAddressText]</td>
                                </tr>                                        
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            
            <td valign="top" style="padding:15px 10px;border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                  <tr>
                      <td valign="top" style="min-height:145px;display:block;padding:15px;background:#f6f6f6;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                              <tr>
                                  <td style="padding-bottom:8px;font-weight:bold;">Collection Address:</td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:3px;">[!order_orderDeliveredAddress]</td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
          <td colspan="2" style="padding:20px 10px 0;text-align:center;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                  <tr>
                      <td colspan="2" style="font-size:16px;font-weight:bold;padding-bottom:8px;">Order Summary</td>
                    </tr>
                    <tr>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;">
                            Order Id:<span style="font-weight:bold;"># [!order_orderId]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                          BYOD Code:<span style="font-weight:bold;">[!portal_studentCode]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                          Placed On:<span style="font-weight:bold;">[!order_orderPlacedDate]</span>
                        </td>
                    </tr>
                    <tr>
                      <td colspan="3" style="padding-top:8px;">                           
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;">Qty</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Discount</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Gift Wrapping</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item Total</th>
                                  </tr>
                                </thead>
                                <tbody valign="top">
                                    [!order_shortGrid] 
                                   
                                  <tr>
                                    <td colspan="6" style="padding:4px 3px;text-align:right;">Item Subtotal:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_orderItemTotalFormatted]</td>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:4px 3px;text-align:right;">Gift Wrapping:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_giftWrapTotalFormatted]</td>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:4px 3px;text-align:right;">Discount Amount:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_formatedOrderDiscountAmount]</td>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:5px 3px;text-align:right;background:#f0f0f0;font-weight:bold;">Order Total:</td>
                                    <td style="padding:5px 3px;text-align:right;background:#f0f0f0;font-weight:bold;">[!order_GrandTotalFormatted]</td>
                                  </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                      <td style="font-size:13px;padding:5px 3px;border-bottom:1px solid #eeeeee;">
                          Order Status:<span style="color:#6a8b50;">[!order_status]</span>
                        </td>
                        <td style="padding:5px 3px;color:#797979;font-size:13px;text-align:right;border-bottom:1px solid #eeeeee;">[!order_paymentDetails]</td>
                    </tr>
                </table>
            </td>
        </tr>
    
        <tr>
          <td colspan="2" style="padding:20px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              If you have any questions please donot hesitate to <a href="" style="text-decoration:underline;color:#5e5e5e;">[!order_contactusUrl]</a> via the website<br>
                OR simply call us in store on <span style="text-decoration:underline;"><a href="tel:[!store_phone]">[!store_phone]</a></span>
            </td>
        <tr>
        
        <tr>
          <td colspan="2" style="padding:0 0 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Thanks again for choosing to purchase from <span style="font-weight:bold;font-size:14px;">[!store_title]!</span>
            </td>
        <tr>
    </tbody>
</table>