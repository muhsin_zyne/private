<div class="wrapper" style="width:600px;background:#fff;font-family:Arial, Helvetica, sans-serif;font-size:12px">
<div class="logo" style="background:#fff;padding:5px;text-align:center;">[!store_logo]</div>
<div class="con" style="padding:5px;color:#0C0C0C;">
<div class="info">
<b style="margin:0 0 5px 0;font-size:18px;">Hi [!takeaphoto_name],</b><br/><br/>
Thanks for your interest in <b>[!store_title]</b>. We have received the photo you sent. We strive to do our best to meet your expectation. We shall get back to you shortly with further details.
<p>You can contact us anytime on:</p>
<b>Phone : </b> [!store_phone]<br/>
<b>Email : </b> [!store_email]<br/>
<b>Address : </b> [!store_billingAddress]
<br/>
<br/>
<hr>
Kind Regards,<br/>
<b style="margin:0 0 5px 0;font-size:18px;">[!store_title]</b>
</div>
</div>
</div>