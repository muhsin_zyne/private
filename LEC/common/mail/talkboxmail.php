<!--

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
            <tr><td colspan="4" align="center"> [!logo] </a></td></tr>
            <tr>
                <td colspan="7" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">New Talkbox Subscriber</h2> </td>
            </tr>        
            <tr>
                <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!store_storeAdminName],</h2> </td>
            </tr> 
            <tr>
                <td colspan="2" style="vertical-align:middle;font-size:12px;">
                    <p style="margin:5px 0;">A new subscriber has been added to your Talkbox database under the tag 'eCommerce Sign up'. The customer's email is [!email]</p>
                </td>
            </tr>
            
           
            <tr><td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;">        
                <p><b>Thank You.</b></p></td>
            </tr>
        </tbody>
    </table>
</div>-->
<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
        	<td align="center">[!headerLogo]</td>
        </tr>
        
        <tr>
        	<td style="padding:32px 20px;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                	<tr>
                    	<td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!store_storeAdminName],</td>
                    </tr>
                    <tr>
                    	<td style="font-size:15px;">A new subscriber has been added to your Talkbox database under the tag 
'eCommerce Sign up'. The customer's email is <b style="font-style:italic;">[!email]</b></td>
                    </tr>
                </table>
            </td>
        </tr> 
    </tbody>
</table>