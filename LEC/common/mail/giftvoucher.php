<style type="text/css">

.grid-view table {

  width: 100%;

  margin-top: 10px;

  border-collapse: collapse;

  font-size: 12px

}

.grid-view th {

  padding: 7px;

  vertical-align: middle;

  text-align: left;

}

.grid-view thead tr {

  color: #191919;

  background-color: #F8F8F8;

}

.grid-view tbody td {

  padding: 10px;

  vertical-align: middle;

  text-align: left;

}

</style>



<div style="background:#FFFFFF;  font-size:13px; color:#222;">

 <table style="width:600px !important;border-collapse:collapse;border:none;font-size:12px; font-family:Arial,Helvetica,sans-serif;">

  <tbody>

   <tr>

    <td colspan="4" style="text-align:center;"><a>[!Logo] </a></td>

   </tr>

   <tr>

    <td style="vertical-align:middle" colspan="2"><h2 style="margin:10px 0 5px 0;font-size:18px">Congratulations [!voucher_recipientName], </h2></td>

   </tr>

   <tr>

    <td style="vertical-align:middle;font-size:13px; line-height:19px;" colspan="2">

      <p style="font-size:15px;margin:5px 0">[!userfullname] has sent you an eGift Card worth [!voucher_balanceFormatted]! </p>

    </td>

   </tr>

   <tr>

    <td colspan="2">

      <table border="1" style="width:100%;margin-top:10px;border-collapse:collapse;font-size:12px; font-family:Arial,Helvetica,sans-serif;">

      <tbody>

       <tr>

        <td colspan="2">

          <table style="width:594px !important; background:rgb(238, 229, 196); border: 6px solid rgb(204, 204, 204); padding:10px; font-family:Arial,Helvetica,sans-serif;">

          <tbody>

           <tr>

            <td colspan="2" style="color: #93010b; font-family: Arial,Helvetica,sans-serif; font-size: 28px; font-weight: normal; text-transform: uppercase;">

            eGift Card

             </td>

           </tr>

           <tr>

            <td style="padding-top:20px; font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000000;text-transform:uppercase;"> To: </td>

            <td style="border-bottom:1px dashed #000000; padding-top:20px;line-height:13px;padding-left:5px;color:#93010b;font-size:16px"> [!voucher_recipientName] </td>

           </tr>

           <tr>

            <td style="padding-top:20px; font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000000;text-transform:uppercase;"> Message: </td>

            <td  style="border-bottom:1px dashed #000000; padding-top:20px;line-height:13px;padding-left:5px;color:#93010b;font-size:16px; text-transform:none;"><em>[!voucher_recipientMessage]</em></td>

           </tr>

           <tr>

            <td style="padding-top:20px; font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000000;text-transform:uppercase;"> From: </td>

            <td style="border-bottom:1px dashed #000000; padding-top:20px;line-height:13px;padding-left:5px;color:#93010b;font-size:16px"> [!userfullname] </td>

           </tr>

           <tr>

            <td style="padding-top:20px; width:150px; font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000000;text-transform:uppercase;"> Expiration Date: </td>

            <td style="border-bottom:1px dashed #000000; width:450px; padding-top:20px;line-height:13px;padding-left:5px;color:#93010b;font-size:16px"> [!voucher_expirationDateFormatted] </td>

           </tr>

           <tr>

            <td style="padding-top:20px; width:150px; font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000000;text-transform:uppercase;"> Voucher code: </td>

            <td style=" width:450px;">

              <table style="width:275px;border:1px solid #999999;color:#93010b; padding:7px; font-size:16px;font-weight:bold; background:white; font-family:Arial,Helvetica,sans-serif;">

                <tr>

                  <td>

                    [!voucher_code]

                  </td>

                </tr>

                </table>

            </td>

           </tr>

           <tr>

            <td style="padding-top:20px; width:150px; font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000000;text-transform:uppercase;"> To the value of: </td>

            <td style="border-bottom:1px dashed #000000; width:450px;font-weight:bold; line-height:13px;padding-left:5px; padding-bottom:5px;color:#93010b;font-size:18px"> [!voucher_balanceFormatted] </td>

           </tr>

           <tr>

            <td colspan="2">

              <b style="float:left;width:100%;font-size:12px;font-weight:bold;font-family:Arial,Helvetica,sans-serif;color:#424242;margin:10px 0 0 0">Notes:</b>

               <p style="float:left;width:100%;font-size:12px;font-weight:normal;font-family:Arial,Helvetica,sans-serif;margin:0;color:#686868"> Use before expiration date.Not redeemable for cash.This voucher can only be redeemed at <a target="_blank" href="[!store_siteUrl]">[!store_title]</a></p>

             </td>

           </tr>

          </tbody>

         </table></td>

       </tr>

      </tbody>

     </table></td>

   </tr>

   <tr>

    <td colspan="2"><p style="margin:5px 0; font-size:13px; line-height:19px; font-family:Arial,Helvetica,sans-serif;"> To redeem this eGift Card go to our online store at <strong>[!store_siteUrl]</strong>.

     

     <p> After adding products to the cart, checkout by entering the voucher code shown above. In the event that the cart total is higher than the eGift Card value, you will have the opportunity to pay

      

      the difference using a credit card, Paypal or by redeeming another eGift Card.

      

      Any unused balance will remain in your account for future use, up until the expiration date shown on the voucher ([!voucher_expirationDateFormatted]). </p>

     Should you have any queries, please feel free to call us on <strong>[!store_phone]</strong> or mail us at <strong>[!store_email]</strong><br>

     <br>

     Our Address is:<br>

     [!store_billingAddress] </td>

   </tr>

   <tr>

    <td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px; font-family:Arial,Helvetica,sans-serif;" colspan="2"><p><b>Congratulations!</b></p></td>

   </tr>

  </tbody>

 </table>

</div>

