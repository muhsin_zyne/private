<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td colspan="2" align="center">[!user_mailBanner]</td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding:30px 0;">
              <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                    <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!user_firstname] [!user_lastname],</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;padding-bottom:13px;"><b>Welcome to the world of [!store_title].</b></td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;padding-bottom:13px;">To log in at any time, simply click on either the <span style="text-decoration:underline;">[!store_loginUrl]</span> or <span style="text-decoration:underline;">[!store_myAccountUrl]</span> links at the top of the home page. Then enter your email address and nominated password.
                        Your registered email address is: <span style="text-decoration:underline;font-weight:bold;font-size:14px;">[!user_email]</span></td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;">Once you logged in, you will be able to do the following;</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
          <td valign="top" style="background:#f2f2f2;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                  <tr>
                      <td valign="top" style="padding:15px 0 15px 30px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                  <td style="padding-bottom:5px;">
                                      [!user_mailImageTick] Create a wishlist
                                    </td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:5px;">
                                      [!user_mailImageTick]Checkout faster
                                    </td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:5px;">
                                      [!user_mailImageTick] Check the status of an order
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            
            <td valign="top" style="background:#f2f2f2;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                  <tr>
                      <td valign="top"  style="padding:15px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                  <td style="padding-bottom:5px;">
                                      [!user_mailImageTick] View past orders
                                    </td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:5px;">
                                      [!user_mailImageTick] Update your profile
                                    </td>
                                </tr>
                                <tr>
                                  <td style="padding-bottom:5px;">
                                     [!user_mailImageTick] Change your password
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
          <td colspan="2" style="padding:20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                 We look forward to providing you with exemplary levels of service. For whatever reason, if you need assistance please don’t hesitate to reach out via our [!store_contactusUrl] form on our website
            </td>
        <tr>
        
        <tr>
          <td colspan="2" style="padding:0 0 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Thank you!
              <br>
              [!store_title]<br/>
              <a href="[!store_siteUrl]" style="text-decoration:underline;color:#5e5e5e!important;"> <b>Visit Store</b></a>
            </td>
        <tr>
        <tr>
          <td colspan="2" style="padding:0 0 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
             
            </td>
        <tr>
        
        
    </tbody>
</table>


