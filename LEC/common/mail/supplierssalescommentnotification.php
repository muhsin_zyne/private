



<style type="text/css">

  .grid-view table{

    width:100%;

    margin-top:10px;

    border-collapse: collapse;

    font-size:12px

  }

  .grid-view th{

    padding:7px;

    vertical-align:middle;

    text-align: left;

  }

  .grid-view thead tr{

    color:#191919;

    background-color: #F8F8F8;

  }

  .grid-view tbody td{

    padding:10px;

    vertical-align:middle;

    text-align: left;

  }

</style>

<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">

    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">

        <tbody>

        <tr><td colspan="4" align="center">  [!logo] </td></tr>

        

        <tr>

            <td style="text-align:right;"><b>Order Date : [!orderDate] </b></td>

        </tr>

        <tr>

            <td style="text-align:right;"><b>Order # : [!salescomments_orderId] </b></td>

        </tr>

        <tr>

            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!userFullName],</h2> </td>

        </tr>

        <tr>

            <td colspan="2">

                <table class="product_table" style="width:100%;margin-top:10px;border-collapse: collapse;font-size:12px" border="1" bordercolor="#E7E7E7">

                    <body> 

                        <tr style="color:#191919;" bgcolor="#F8F8F8">

                            <td style="padding:7px;vertical-align:top;text-align: left;" width="50%">

                                <table>

                                    <tr>

                                        <td>

                                            <p>[!suppliersName] has sent you a comment via the LEGC Supplier Portal. Please see the comment below:<br/></p>

                                            <p>[!salescomments_comment]</p>

                                        </td>

                                    </tr>

                                </table>

                            </td>

                        </tr>

                    </body> 

                </table>

            </td>

        </tr>

        <tr>

            <td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;" colspan="2">

                    <p><b>Thanks.</b></p>

            </td>

        </tr> 

        </tbody>

    </table>

</div>