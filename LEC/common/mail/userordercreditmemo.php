<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td align="center">[!creditmemo_mailBanner]</td>
        </tr>
        
        <tr>
          <td style="padding:30px 20px 0;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:16px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!userfullname],</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                          [!store_title], has processed a refund of AUD [!grandTotal] for your order #[!order_orderId] which was placed on [!order_orderPlacedDAte]. We apologise for any inconvenience caused. The refund will be credited to your credit card [!order_cardNumber] in 7-14 days.
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
        
        
        
        <tr>
          <td style="padding:0 10px 8px;text-align:center;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                
                    <tr>
                        <td style="font-size:13px;padding:5px 8px;text-align:right;">
                          Order Id:<span style="font-weight:bold;"># [!order_orderId] </span>
                        </td>
                    </tr>
                    
                    <tr>
                      <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;text-align:center;">Qty Ordered</th>
                                    <th style="padding:7px 3px;font-weight:normal;text-align:right;">Status</th>
                                  </tr>
                                </thead>
                                <tbody valign="top"> 
                                  [!order_statusUpdateShortGrid]
                                 
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        [!comment]
        
   
        
        
        <tr>
            <td style="padding:6px 20px 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
               Thanks again for choosing to purchase from Leading Edge Computers ! 
            </td>
        </tr>
        
    </tbody>
</table>