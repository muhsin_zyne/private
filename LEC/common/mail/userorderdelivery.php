<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td colspan="2" align="center">[!delivery_mailBanner]</td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding:30px 0;">
              <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!userfullname],</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;padding-bottom:8px;">Thank you for purchasing from [!appTitle].</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;">This email is to confirm the item/s collected.</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
          <td colspan="2" style="padding:15px 10px 0;text-align:center;border-top:1px solid #eaeaea;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                  <tr>
                      <td colspan="2" style="font-size:16px;font-weight:bold;padding-bottom:8px;">Package Contents</td>
                    </tr>
                    
                    
                    <tr>
                      <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;">
                          Order Id:<span style="font-weight:bold;"> # [!order_orderId]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                          The following items were collected on:<span style="font-weight:bold;"> [!delivery_deliveryPlacedDate]</span>
                        </td>
                    </tr>
                    
                    
                    <tr>
                      <td colspan="2" style="padding-top:8px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;">SKU</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Qty Puchased</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Qty Collected</th>
                                  </tr>
                                </thead>
                                <tbody valign="top">
                                  [!delivery_shortGrid]
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
          <td colspan="2" style="padding:20px 0 10px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              The goods were collected by [!userfullname].<br>
                <i style="font-style:italic;color:#959595;font-size:13px;">*The recipient’s Photo ID was verified at the time of collection</i>
            </td>
        <tr>
        
        <tr>
          <td colspan="2" style="padding:0 0 10px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              The above collection was processed by [!appTitle] staff member, <br>[!delivery_verifiedBy]
            </td>
        <tr>
        
        <tr>
          <td colspan="2" style="padding:0 0 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Thanks again for choosing to purchase from [!appTitle] !
            </td>
        <tr>

    </tbody>
</table>