

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4" align="center"> <a href="[!siteUrl]"> [!Logo] </a></td></tr>
        <tr>
            <td colspan="7" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">B2B Contact Us</h2> </td>
        </tr>
        
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear Master Admin,</h2> </td>

        </tr>
            
            <tr>
                <td colspan="4">
                    <table class="product_table" style="width:100%;margin-top:10px;border-collapse: collapse;font-size:12px" border="1" bordercolor="#E7E7E7">
                        <body> 
                            <tr style="color:#191919;" bgcolor="#F8F8F8">
                                <td style="padding:7px;vertical-align:top;text-align: left;" width="50%">
                                
                                  <p>Name : [!name] </p>
                                  <p>Email : [!email] </p>
                                  <p>Phone : [!phone] </p>
                                  <p>Enquiry : [!enquiry]</p>
                               </td>
                            </tr>
                        </body> 
                    </table>
                </td>
            </tr>
            
            
                </td>
            </tr>
           
            <tr>
                <td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;" colspan="2">
                    <p><b>Thanks for using the Leading Edge Web Platform.</b></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>