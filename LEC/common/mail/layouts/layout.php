<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<!DOCTYPE html>
    <head>
        <style>a{color:#3a3138;}</style>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body style="background:#eeeeee;margin:8px 0 0;">
        <table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
            <tbody>
                <tr>
                    <td colspan="2" align="center" style="text-align:cente;padding:25px 0;"><a href="">[!logo]</a></td>
                </tr>
            </tbody>
        </table>        
            <?= $content ?>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#fff;">
            <tbody>
                <tr>
                  <td bgcolor="#2f2f2f" width="100%" style="">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="padding:18px 0;font-family:Arial, Helvetica, sans-serif; font-size:11px;text-align:center;color:#211e21;">
                        <tr>
                            <td style="text-align:center;"><a href=""><img alt="" src="<?=Yii::$app->params["rootUrl"]."/store/site/email/footer-logo.jpg"?>"></a>
                            </td>
                            
                        </tr>
                    </table>
                  </td>
                </tr>
                
                <tr>
                  <td bgcolor="#5b5b5b" style="padding:5px 0;">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;color:#fff;">
                        <tr>
                            <td>[!storeDetails]</td>
                            <td style="text-align:right;">
                                <table align="center">
                                    <tr style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#3a3138; line-height:20px;text-align:right;">
                                        <td>[!emailFacebookImage]</td>
                                        <td>[!emailTwitterImage]</td>
                                        <td>[!emailLinkedinImage]</td>
                                        <td>[!emailInstagramImage]</td>
                                        <td>[!emailYoutubeImage]</td>
                                        <td>[!emailPinterestImage]</td>
                                        <td>[!emailGoogleplusImage]</td>                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="padding:12px 0;">
                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="width:600px!important;font-family:Arial, Helvetica, sans-serif; font-size:12px;text-align:center;color:#211e21;">
                        <tr>
                            <td style="padding:0 0 2px;">Copyright © [!appTitle] [!date] | This email was sent to 
                            <a href="" style="color:#211e21;text-decoration:none;">[!useremail]</a></td>
                        </tr>
                    </table>
                  </td>
                </tr>
                
            </tbody>
        </table>
    </body>
</html>