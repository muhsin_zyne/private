<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <table width="600" align="center" style="background:#fff; border:solid 1px #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#515151;">
        <tbody>
            <tr >
                <td>
                    <table cellspacing="0" cellpadding="0" border="0"  style=" padding:10px; color:#fff; text-align:center; width:600px;">
                        <tbody>
                            <tr style="width:600px;">
                                <td style="text-align:right; padding-top:15px;width:400px"  align="right" >
                                    <table align="right">
                                      	<tr style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#515151; line-height:20px;text-align:right;">
                                           <td>[!emailFacebookImage]</td>
                                            <td>[!emailTwitterImage]</td>
                                            <td>[!emailInstagramImage]</td>
                                            <td>[!emailYoutubeImage]</td>
                                            <td>[!emailPinterestImage]</td>
                                            <td>[!emailGoogleplusImage]</td>
                                            <td>[!emailLinkedinImage]</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="width:600px;">
                <td style="width:600px;">
                    <table width="600px" border="0" cellspacing="0" cellpadding="0" style="width:600px;text-align:center;">
                        <tr>
                            <td width="600" style="width:600px;text-align:right; border-top:solid 5px #dc2514;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <?=$content?>
                </td>
            </tr>
            <tr>
            	<td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;">
                  
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" background="#b50937">
                        <tbody>
                            <tr>
                                <td style="font-family: Arial, Helvetica, sans-serif; color: #a4b3bb; font-size: 12px; background:#dc2514; text-align:center; padding:20px; text-align:center; ">
                                    This message was sent to: [!useremail]
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
