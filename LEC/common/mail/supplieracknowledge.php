

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4" style="text-align:center;"> [!logo]</td></tr> 
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Order Acknowledgment</h2> </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;"><b>Order : #[!supplier_id]-[!order_OrderId]</b></td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!username],</h2> </td>
        </tr>
        <tr>
            <td colspan="2">
                This message confirms that your order has just been acknowledged. Please find the details below.
            </td>
        </tr>
        <tr><td colspan="2"><b><h2>Order Details</h2></b></td></tr>
        <td width="600" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
        <tr><td colspan="2">Order : #[!supplier_id]-[!order_OrderId]</td></tr>
        <tr><td colspan="2">Placed on [!order_OrderPlacedDate]</td></tr>
        <tr>
            <td colspan="2">
                [!itemsGrid]
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>  
        </tr>
        <tr>
            <td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;" colspan="2">
                <p><b>Thanks for using the Leading Edge Web Platform.</b></p>
            </td>
        </tr> 
       
        
             
        </tbody>
    </table>
</div>