

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4" align="center">  [!Logo] </td></tr>
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Shipment Tracking</h2> </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;"><b>Order Id: #[!order_OrderId]</b></td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!user_firstname] [!user_lastname],</h2> </td>
        </tr>
            <tr>
                <td colspan="2" style="vertical-align:middle;font-size:12px;">
                    <p style="margin:5px 0;">Your package was shipped via <b>[!store_title]</b>, Australia with the tracking ID <b>[!shipment_trackingNumber]</b>. </p> 
                    <p>This is to inform you that [!store_title], 
                        Australia has expressed inability to locate your address.
                        We request you to <a>[!shipment_ContactusUrl]contact us </a>with the necessary changes and/or additions to the address, 
                        including significant landmarks which may help the courier to locate your address.</p>
                    <p>If you need to change an existing delivery address for future orders, 
                        we recommend that you update your address book via <a>[!shipment_AccountUrl]Your Account</a>. </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="product_table" style="width:100%;margin-top:10px;border-collapse: collapse;font-size:12px" border="1" bordercolor="#E7E7E7">
                        <body> 
                            <tr style="color:#191919;" bgcolor="#F8F8F8">
                                <td style="padding:7px;vertical-align:top;text-align: left;" width="50%">
                                    <table >
                                        <tr>
                                            <td>
                                                <p><b>Track your package</b><br/></p>
                                                <p>
                                                <b>Courier Name : [!shipment_carrier]<br/>
                                                Tracking Id : [!shipment_trackingNumber]</b>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                               </td >
                               <td style="padding:7px;vertical-align:top;text-align: left;" width="50%">
                                    <table>
                                        <tr>
                                            <td>
                                                <p>Your Invoice will be sent to:<br/></p>
                                                <p><b>[!order_CustomerAddress]</b></p>
                                            </td>
                                        </tr>
                                    </table>
                               </td>
                            </tr>
                        </body> 
                    </table>
                </td>
            </tr><br/>
            <tr><td colspan="2"><b><h2>Package Contents</h2></b></td></tr>
            <td width="600" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr>
                <td colspan="2"> 
                    [!shipment_shortGrid]
                </td>
            </tr>
            <tr>
                <td style="padding:10px;vertical-align:middle;text-align:right;" colspan="8"> [!shipment_ShipmentItemsUrl]See All
                 </td>           
                </td>
            </tr>
            <tr><td></td></tr><p><br/></p>
             <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr>
                <td width="70%">
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;" >
                                <p>Item Subtotal:   <br/>
                                Shipping  Handling:<br/></p>
                                <b>Order Total: </b>
                            </td>
                        </tr>
                    </table>
                </td><td width="30%">
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;">
                                <p>[!order_OrderItemTotalFormatted]<br/>
                                [!order_ShippingAmount]<br/></p>
                                <b>[!order_GrandTotalFormatted]</b>
                            </td>  
                        </tr>
                    </table>         
                </td>
            </tr>
            <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr>
                <td colspan="2">
                    <p>Need to make changes to your invoice? Visit our<a >[!shipment_HelpUrl]Help Page</a>  for more information.</p><br/>
                </td>
            </tr>
            
        </tbody>
    </table>
</div>