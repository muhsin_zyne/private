<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
            <tr>
                <td colspan="4" align="center"> <a href="[!siteUrl]"> [!Logo] </a></td>
            </tr>
           <!--  <tr>
                <td colspan="2" style="text-align:right;"> 
                    <h2 style="margin:0 0 5px 0;font-size:18px;"> Design Consultation Booking Notification</h2> 
                </td>
            </tr> -->
            <tr>
                <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!store_title], </h2> </td>
            </tr>
            <tr>
                <td colspan="2" style="vertical-align:middle;font-size:12px;">
                    <p style="margin:5px 0;">A customer has just made a booking for a design consultation for [!appointment_bookedDate] via your website. Please see the details below:</p>
                </td>
            </tr>
            <tr><td colspan="2"><b><h2>Booking Details: </h2></b></td></tr>
            <td colspan="2" width="600" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr><td colspan="2">Date: [!appointment_bookedDate]</td></tr>
            <tr><td colspan="2">Time:  [!appointment_bookedSlot]</td></tr>
            <tr><td colspan="2">Customer Name: [!appointment_name]</td></tr>
            <tr><td colspan="2">Email: [!appointment_email]</td></tr>
            <tr><td colspan="2">Phone: [!appointment_phone]</td></tr>
            <tr><td colspan="2">Notes: [!appointment_message]</td></tr>
            <br/>
            <!-- <tr>
                <td colspan="2">
                    <p>If you have any questions or wish to make changes, please contact us directly on <b>[!store_phone] </b> or send an email to <b>[!store_email].</b></p>
                </td>
            </tr> -->
            <tr>
                <td colspan="2"><br/><p>Thanks,</p></td>
            </tr>
            <tr>
                <td colspan="2"><p><h2>LEJ Admin</h2></p></td>
                
            </tr>
            <!-- <tr>
                <td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;" colspan="2">
                    <!-- <p><b>We hope to see you again soon.</b></p> 
                </td>
            </tr> -->
        </tbody>
    </table>
</div>            
