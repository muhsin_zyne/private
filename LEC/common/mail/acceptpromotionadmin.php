

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
       
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Accept Promotions</h2> </td>
        </tr>
       
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!storepromotion_B2bUserName],</h2> </td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align:middle;font-size:12px;">
                <p style="margin:5px 0;">"<b>[!consumerPromotion_title]</b>" promotion has been accepted by <b>[!store_title]</b> .
                 The start date of promotion is <b>[!storepromotion_PromotionStartDate] 00:00:01 AM</b> and end date is <b>[!storepromotion_PromotionEndDate] 11:59:59 PM</b></p> 
                   
            </td>
        </tr>
        </tbody>
    </table>
</div>