<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="background:#fff;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td colspan="2" align="center">[!invoice_mailBanner]</td>
        </tr>
        
        <tr>
          <td colspan="2" style="padding:30px 0;">
              <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!userfullname],</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;padding-bottom:13px;">Thank you for purchasing from [!appTitle].</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;">This is the Tax Invoice for Order # [!order_orderId].</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding:15px 10px;border-top:1px solid #eaeaea;border-bottom:1px solid #eaeaea;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                    <tr>
                        <td width="284" valign="top"  style="padding:15px 3px 15px 15px;background:#f6f6f6;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                    <td style="padding-bottom:8px;font-weight:bold;">Invoice To:</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:3px;">[!order_billingAddressText]</td>
                                </tr>
                                
                            </table>
                        </td>
                        <td width="12"></td>
                        <td width="284" valign="top" style="padding:15px 3px 15px 15px;background:#f6f6f6;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:15px;color:#5e5e5e;line-height:21px;">
                                <tr>
                                    <td style="padding-bottom:8px;font-weight:bold;">Invoice From:</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom:3px;">[!order_orderDeliveredAddress]</td>
                                </tr>
                                <tr>                                    
                                    <td style="padding-bottom:3px;">ABN: [!store_abn]</td>
                                </tr>
                                
                            </table>
                        </td>
                        
                    </tr>
                </table>
            </td>
        </tr>
       
        
        <tr>
          <td colspan="2" style="padding:20px 10px 0;text-align:center;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                  <tr>
                      <td colspan="3" style="font-size:16px;font-weight:bold;padding-bottom:8px;">Invoice Details</td>
                    </tr>
                    
                    
                    <tr>
                      <td style="background:#f0f0f0;font-size:13px;padding:5px 3px;">
                          Invoice No:<span style="font-weight:bold;"> #[!invoice_invoiceId]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 3px;">
                          Invoice Date:<span style="font-weight:bold;"> [!invoice_invoicePlacedDate]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 3px;">
                          Invoice Total:<span style="font-weight:bold;"> [!invoice_invoiceGrandTotalFormatted]</span>
                        </td>
                    </tr>
                    
                    
                    <tr>
                      <td colspan="3" style="padding-top:8px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;">Qty</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Discount</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Gift Wrapping</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item Total</th>
                                  </tr>
                                </thead>
                                <tbody valign="top">
                                   [!invoice_shortGrid] 
                                   <tr>
                                    <td colspan="6" style="padding:4px 3px;text-align:right;">Item Subtotal:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!invoice_invoiceItemTotalFormatted]</td>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:4px 3px;text-align:right;">Gift Wrapping:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_giftWrapTotalFormatted]</td>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:4px 3px;text-align:right;">Discount Applied:</td>
                                    <td style="padding:4px 3px;text-align:right;">[!order_formatedOrderDiscountAmount]</td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" style="padding:5px 3px;background:#f0f0f0;">The Invoice includes <span style="font-weight:bold;">GST of [!order_formattedAppliedTax]</span></td>
                                    <td colspan="3" style="padding:5px 3px;text-align:right;background:#f0f0f0;font-weight:bold;">Invoice Total (inc. GST):</td>
                                    <td style="padding:5px 3px;text-align:right;background:#f0f0f0;font-weight:bold;">[!invoice_invoiceGrandTotalFormatted]</td>
                                  </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding:5px 3px;color:#797979;font-size:13px;text-align:right;border-bottom:1px solid #eeeeee;">Paid using [!order_paymentDetails]</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
          <td colspan="2" style="padding:20px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              If you have any questions please don’t hesitate to <a href="" style="text-decoration:underline;color:#5e5e5e;">[!order_contactusUrl]</a> via the website<br>
                OR simply call us in store on <span style="text-decoration:underline;"><a href="tel:[!store_phone]">[!store_phone]</a></span>
            </td>
        <tr>
        
        <tr>
          <td colspan="2" style="padding:0 0 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Thanks again for choosing to purchase from [!appTitle] !
            </td>
        <tr>
        
        
        
    </tbody>
</table>