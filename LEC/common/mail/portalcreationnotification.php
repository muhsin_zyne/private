<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td align="center">[!portal_portalCreationBanner]</td>
        </tr>
        
        <tr>
          <td width="100%" style="padding:30px 20px;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!portal_name],</td>
                    </tr>
                    <tr>
                      <td style="font-size:16px;">We have prepared the following quote for your consideration.<br/>Please refer to the details below. </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
          <td style="padding:15px 10px 0;text-align:center;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                  <tr>
                      <td colspan="3" style="font-size:16px;font-weight:bold;padding-bottom:8px;">Quote Contents</td>
                    </tr>
                    
                    <tr>
                      <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;">
                          Quote:<span style="font-weight:bold;">#[!portal_id]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                          Client Portal Code:<span style="font-weight:bold;">[!portal_studentCode]</span>
                        </td>
                        <td style="background:#f0f0f0;font-size:13px;padding:5px 8px;text-align:right;">
                          Date issued:<span style="font-weight:bold;">[!portal_getValidFromFormatted]</span>
                        </td>
                    </tr>

                    <tr>
                      <td colspan="3" style="padding-top:8px;">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;float: left;">SKU</th>
                                    <th style="padding:7px 3px;font-weight:normal;text-align:center;">Your Price</th>
                                  </tr>
                                </thead>
                                <tbody valign="top">
                                  [!portal_getShortGrid]
                                </tbody>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        
        <tr>
          <td style="padding:20px 15px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Please note that this quote is valid until <span style="font-weight:bold;">[!portal_getExpiresOnFormatted]</span>
            </td>
        <tr>
        
        <tr>
            <td style="text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                <table width="175" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;text-align:center;font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;">
                  <tr>
                      <td style="padding:12px 0;font-size:15px;font-weight:bold;border:1px solid #e31837;border-radius:5px;"><a href="[!clientPortalUrl]" style="text-decoration:none;color:#e31837;">Buy online now</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
          <td style="padding:20px 15px 30px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
              Or please visit us in store before this quote expires
            </td>
        <tr>
        
    </tbody>
</table>