<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }

</style>

<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">

    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">

        <tbody>
        <tr><td colspan="4" align="center"> <a href="[!siteUrl]"> [!logo] </a></td></tr>
        

        <tr>

            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Order Notification</h2> </td>

        </tr>

        <tr>

            <td colspan="2" style="text-align:right;"><b>Order : #[!supplier_id]-[!order_OrderId]</b></td>

        </tr>

        <tr>

            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!supplier_fullName],</h2> </td>

        </tr>

        <tr>

            <td colspan="2">

                You have got a new order from a LEGC B2B user. Please find the details below.

            </td>

        </tr>

        <tr><td colspan="2"><b><h2>Order Details</h2></b></td></tr>

        <td width="600" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>

        <tr><td colspan="2">Order : #[!supplier_id]-[!order_OrderId]</td></tr>

        <tr><td colspan="2">Placed on [!order_OrderPlacedDate]</td></tr>

        <tr>

            <td colspan="2">

                [!itemsGrid]

            </td>

        </tr>

        <tr>

            <td style="padding:10px;vertical-align:middle;text-align:right;" colspan="8">

                <a href="[!supplierDashboardUrl]">Click here to view all your orders</a>

            </td>

        </tr>

        

        <tr><td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td></tr>

        <tr>  <td colspan="2" style="padding: 8px; padding-left: 0px;"><p>[!customerNotes]</p></td>  </tr>
        <tr>
            <td colspan="2">
             Click <a href="[!supplierDashboardUrl]">here</a> to acknowledge receipt of the order, add comments or to send feedback to the customer. Please process the order at the earliest convenience.
            Do not reply to this email as it is from our automated message system. Feedback on this platform and the order process can be sent to Leading Edge using the Contact Us form on the website.
            </td>
        </tr>  

        </tbody>

    </table>

</div>