<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
            <td align="center">[!salescomments_mailMessageBanner]</a></td>
        </tr>
        
        <tr>
            <td style="padding:30px 20px 20px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;color:#5e5e5e;line-height:21px;text-align:center;">
                    <tr>
                        <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!userFullName],</td>
                    </tr>
                    <tr>
                        <td style="font-size:15px;padding-bottom:3px;">[!store_title] has sent you the following message</td>
                    </tr>
                    <tr>
                        <td style="font-size:14px;color:#9b9b9b;">Order Id: # [!salescomments_orderId]</td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td valign="top" style="background:#f2f2f2;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:13px;">
                    <tr>
                        <td valign="top" style="padding:15px 15px 6px;font-style:italic;color:#737373;line-height:21px;">
                            <i>[!salescomments_comment]</i>
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#919191;padding:0 15px 15px;font-style:italic;"><i>- Posted on [!salescomments_salesCommentDate]</i></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="padding:20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                If you have any questions please don’t hesitate to <span style="text-decoration:underline;">[!store_contactusUrl]</span> via the website OR simply call us in store on <span style="text-decoration:underline;"><a href="tel:[!store_phone]">[!store_phone]</a></span>
            </td>
        </tr>
    </tbody>
</table>