<!-- 

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4"> <a href="[!siteUrl]"> [!logo] </a></td></tr>
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Order from Supplier</h2> </td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear Supplier,</h2> </td>
        </tr>
        <tr>
            <td colspan="2">
                You have got a new order from [!userfullname]. Please find the details below.
            </td>
        </tr>
        <tr><td colspan="2"><b><h2>Order Details</h2></b></td></tr>
        <tr>
            <td colspan="2">
                An online sale has been made via Our Shop. So that we may fulfill our delivery commitment to the customer, kindly send the following items(s) to the shipping address noted below as soon as possible. In case of any issues/concerns, please contact us immediately on (07) 54824335 

                Products Details: 

                Product Name: [!product_name] 
                SKU: [!product_sku]
                Qty: 1 

                Shipping address: 50 Mary Street,    
                GYMPIE, QLD, 4570 
                P: (07) 54824335 
                E: [!store_email] 

                Email: sales@miabellajewellery.com.au 

                Thank you! [!store_title]

            </td>
        </tr>
        <tr>
            <td style="padding:10px;vertical-align:middle;text-align:right;" colspan="8">
                <a href="[!supplierDashboardUrl]">Click here to view all your orders</a>
            </td>
        </tr>
        <tr><td></td></tr><p><br/></p>
        <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>   
        </tbody>
    </table>
</div> -->





<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>

<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4" align="center"> <a href="[!siteUrl]"> [!logo] </a></td></tr>
        <tr><td><p></p></td></tr>
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">URGENT ORDER</h2> </td>
        </tr>
        <tr>
        	<td>
            [!message]
            </td>
        </tr>
        
        <!-- <tr>
            <td style="padding:10px;vertical-align:middle;text-align:right;" colspan="8">
                <a href="[!supplierDashboardUrl]">Click here to view all your orders</a>
            </td>
        </tr> -->
        <tr><td></td></tr>
        <p><br/></p>
        <!-- <tr>
        	<td colspan="2">
                <p>[!footerText].</p><br/>
            </td>
        </tr> -->
        <!-- <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td> -->
           
        </tbody>
        <td colspan="2" style="width:600px;text-align:center; border-top:solid 1px #014961;"><p>[!footerText]</p></td>
    </table>
</div> 