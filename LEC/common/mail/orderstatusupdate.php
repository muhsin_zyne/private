<table bgcolor="#fff" width="600" border="0" cellspacing="0" cellpadding="0" align="center"  style="width:600px!important;background:white;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <tbody>
        <tr>
          <td align="center">[!statusUpdateBanner]</td>
        </tr>
        
        <tr>
          <td style="padding:30px 20px 0;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;font-size:16px;color:#5e5e5e;line-height:21px;text-align:center;">
                  <tr>
                      <td style="font-size:16px;font-weight:bold;padding-bottom:5px;">Dear [!userFullName],</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                           There has been an update in the status of your order item(s). 
                            Please see the details below:
                        </td>
                    </tr>
                </table>
            </td>
        </tr> 
        
        
        
        <tr>
          <td style="padding:0 10px 8px;text-align:center;">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;text-align:left;">
                
                    <tr>
                        <td style="font-size:13px;padding:5px 8px;text-align:right;">
                          Order Id:<span style="font-weight:bold;"># [!order_orderId] </span>
                        </td>
                    </tr>
                    
                    <tr>
                      <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
                                <thead style="background:#5b5b5b;color:#fff;">
                                  <tr>
                                    <th style="padding:7px 5px;font-weight:normal;text-align:center;">#</th>
                                    <th style="padding:7px 3px;font-weight:normal;">Item</th>
                                    <th style="padding:7px 3px;"></th>
                                    <th style="padding:7px 3px;font-weight:normal;text-align:center;">Qty Ordered</th>
                                    <th style="padding:7px 3px;font-weight:normal;text-align:right;">Status</th>
                                  </tr>
                                </thead>
                                <tbody valign="top"> 
                                  [!order_statusUpdateShortGrid]
                                 
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        [!comment]
        
        <tr>
            <td style="padding:8px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;font-weight:bold;">
                Here are our normal trading hours
            </td>
        </tr>
        
        
        <tr>
          <td>
              [!timeGrid]
            </td>
        </tr>
        
        <tr>
            <td style="padding:20px 0 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                <table width="175" border="0" cellspacing="0" cellpadding="0" align="center" style="margin:0 auto;text-align:center;font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;">
                  <tr>
                      <td style="padding:12px 0;font-size:15px;font-weight:bold;border:1px solid #e31837;border-radius:5px;"><a href="" style="text-decoration:none;">[!store_mapUrl]</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td style="padding:20px 20px 0;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                If you are coming to our store to collect an item, please be sure to bring with you a copy of your Tax Invoice and apprpriate Photo ID so that we can verify you quickly.
            </td>
        </tr>
        <tr>
            <td style="padding:6px 20px 20px;text-align:center;line-height:21px;font-size:15px;color:#5e5e5e;">
                If you have any questions please do not hesitate to <span style="text-decoration:underline;">[!store_contactusUrl]</span> via the website OR simply call us in store on <span style="text-decoration:underline;">[!store_phone]</span>
            </td>
        </tr>
        
    </tbody>
</table>