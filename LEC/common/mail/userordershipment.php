



<style type="text/css">

  .grid-view table{

    width:100%;

    margin-top:10px;

    border-collapse: collapse;

    font-size:12px

  }

  .grid-view th{

    padding:7px;

    vertical-align:middle;

    text-align: left;

  }

  .grid-view thead tr{

    color:#191919;

    background-color: #F8F8F8;

  }

  .grid-view tbody td{

    padding:10px;

    vertical-align:middle;

    text-align: left;

  }

</style>

<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">

    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">

        <tbody>

        <tr><td colspan="4" align="center">  [!logo] </td></tr>

        <tr>

            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Shipment Tracking</h2> </td>

        </tr>

        <tr>

            <td colspan="2" style="text-align:right;"><b>Order Id: #[!order_orderId]</b></td>

        </tr>

        <tr>

            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!userfullname],</h2> </td>

        </tr>

        <tr>

            <td colspan="2" style="vertical-align:middle;font-size:12px;">

                <p style="margin:5px 0;">This message confirms that your order has been shipped today.</b></p> 

                

                

            </td>

        </tr>

        <tr>

            <td colspan="2">

                <table class="product_table" style="width:100%;margin-top:10px;border-collapse: collapse;font-size:12px" border="1" bordercolor="#E7E7E7">

                    <body> 

                        <tr style="color:#191919;" bgcolor="#F8F8F8">

                            <td style="padding:7px;vertical-align:top;text-align: left;" width="50%">

                                <table >

                                    <tr>

                                        <td>

                                            <p>Track your package<br/></p>

                                            <p>

                                            Courier Name : [!carrier]<br/>

                                            Tracking Reference : [!shipment_title]<br/>

                                            Tracking Link : [!shipment_trackingLink]

                                            </p>

                                        </td>

                                    </tr>

                                </table>

                           </td >

                           <td style="padding:7px;vertical-align:top;text-align: left;" width="50%">

                                <table>

                                    <tr>

                                        <td>

                                            <p>Your Shipment will be delivered to :<br/></p>

                                            <p>[!order_orderDeliveredAddress]</p>

                                        </td>

                                    </tr>

                                </table>

                           </td>

                        </tr>

                    </body> 

                </table>

            </td>

        </tr>

        <tr><td colspan="2"><b><h2>Package Contents</h2></b></td></tr>

        <td width="600" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>

        <tr>

            <td colspan="2"> 

                [!shipment_shortGrid]

            </td>

        </tr>

           

        <tr><td></td></tr>

        <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>

        

        <tr>

            <td colspan="2">

                <p>Please allow 1-2 days for delivery to metropolitan addresses and 2-3 days for delivery to regional areas.

                    In the event that you have questions or concerns, please contact us directly on <b>[!store_phone] </b> or send an email to <b>[!store_email]</b> </p>



                <p>We want your order to arrive in perfect condition. 

                    Please advise us if you are not happy with any aspect of your order within 48 hours of receipt. </p>



                <p>If you need to change an existing delivery address for future orders, 

                    we recommend that you update your address book at any time via <a>[!shipment_accountUrl]Your Account</a> on our website. </p>



            </td>

        </tr>

        <tr><td></td></tr>

        <tr>

            <td colspan="2"><br/>

                <p><h2>[!store_title]</h2></p>

            </td>

        </tr>

        <tr>

            <td style=" text-align:center; border-top:solid 1px #ccc; padding-top:10px; padding-bottom:5px;">

                    <p><b>Thank You.</b></p>

        </tr>    

        </tbody>

    </table>

</div>