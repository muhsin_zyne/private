

<style type="text/css">
  .grid-view table{
    width:100%;
    margin-top:10px;
    border-collapse: collapse;
    font-size:12px
  }
  .grid-view th{
    padding:7px;
    vertical-align:middle;
    text-align: left;
  }
  .grid-view thead tr{
    color:#191919;
    background-color: #F8F8F8;
  }
  .grid-view tbody td{
    padding:10px;
    vertical-align:middle;
    text-align: left;
  }
</style>
<div style="background:#FFFFFF; padding:10px;  font-size:13px; color:#222;">
    <table style="width:100%; border-collapse:collapse;border:none;font-size:12px;">
        <tbody>
        <tr><td colspan="4" align="center"> <a href="[!siteUrl]"> [!logo] </a></td></tr>
        <tr>
            <td colspan="2" style="text-align:right;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Order Cancellation</h2> </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;"><b>Order : #[!order_OrderId]</b></td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: middle;"> <h2 style="margin:0 0 5px 0;font-size:18px;">Dear [!userfullname],</h2> </td>
        </tr>
            <tr>
                <td colspan="2" style="vertical-align:middle;font-size:12px;">
                    <p style="margin:5px 0;">Your order has been cancelled. 
                    For your reference, here’s a summary of your order:You just cancelled order #[!order_OrderId] placed on [!order_OrderPlacedDate].</p>
                </td>
            </tr>
            <tr><td colspan="2"><b><h2>Order Details</h2></b></td></tr>
            <td width="600" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr><td colspan="2">Order : #[!order_OrderId]</td></tr>
               <tr><td colspan="2">Status : [!order_OrdersStatus]</td></tr>
            <tr><td colspan="2">Placed on [!order_OrderPlacedDate]</td></tr>
            <tr>
                <td colspan="2">
                    [!order_shortGrid]
                </td>
            </tr>
            <tr>
                <td style="padding:10px;vertical-align:middle;text-align:right;" colspan="8"> [!allorderurl]
                 </td>           
                </td>
            </tr>
            <tr><td></td></tr><p><br/></p>
             <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr>
                <td width="70%">
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;" >
                                <p>Item Subtotal:   <br/>
                                Shipping  Handling:<br/></p>
                                <b>Order Total: </b>
                            </td>
                        </tr>
                    </table>
                </td><td width="30%">
                    <table width="100%">
                        <tr>
                            <td style="text-align:right;">
                                <p>[!order_OrderItemTotalFormatted]<br/>
                                [!order_ShippingAmount]<br/></p>
                                <b>[!order_GrandTotalFormatted]</b>
                            </td>  
                        </tr>
                    </table>         
                </td>
            </tr>
            <td colspan="2" style="width:600px;text-align:right; border-top:solid 1px #014961;">&nbsp;</td>
            <tr>
                <td colspan="2">
                    <p>Need to make changes to your order? Visit our<a >[!order_helpUrl]Help Page</a>  for more information.</p><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p>We hope to see you again soon.</p><br/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p><h2>[!store_title]</h2></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>