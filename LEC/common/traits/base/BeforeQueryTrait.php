<?php
namespace common\traits\base;

trait BeforeQueryTrait{
    public static function find() { 
        $obj = new static;
        $class = new \ReflectionClass($obj);
        $condition = "";
        foreach ($class->getProperties() as $property) {
            if(strpos($property->getName(),'BEFORE_QUERY') !== false && is_string($property->getValue($obj))){ 
                $condition = $property->getValue($obj);
            }
        }
        //var_dump($condition); die;
        $query = parent::find()->andWhere($condition);
        return $query;
    }
}