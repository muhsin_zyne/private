<?php
 
namespace console\controllers;
 
use yii\console\Controller;
use common\models\EmailQueue;
use common\models\Stores;
use common\models\Orders;
 
/**
 * Test controller
 */
class CronController extends Controller {
 
    public function actionMinute() {
        $this->sendEmails();
    }
 
 	public function actionDaily(){
 		
 	}

 	public function actionNightly(){
 		/*$promotions = ConsumerPromotions::find()->all();
 		foreach($promotions as $promotion){
 			if($promotion->willEndToday())
 				$promotion->endPromotion();
 		}
 		
 		$conference = Conferences::find()->where(['status'=>'1'])->one();
 		if($conference->willEndToday())
 			$conference->endConference();*/	

 		$this->doOrderStatusUpdate();
 		$this->disableScheduledProducts();
 	}
    public function sendEmails(){
	    $emails = EmailQueue::find()->where("status='Queued' OR status = 'Retry'")->all(); // Finding EmailQueue model
	    
	    $i = 1; // Setting the counter.
		//$template = file_get_contents(pathinfo(__FILE__, PATHINFO_DIRNAME) . '/../../themes/infopoint/emailTemplate.html');
	    if(empty($emails)){ // No emails in queue.
	        echo("Send: No Emails In Queue\n");
	    }else{
	        echo "Send: Sending ".count($emails)." email(s) via SwiftMailer"; // Counting email,Converting first letter uppercase and displaying
	        foreach($emails as $email){
	        	$mail = new \common\components\MailHandler(); // Starting new class.
	        	if($mail->sendMail($email)){ // Sending the mail
					$email->dateSent = date('Y-m-d H:i:s');
		        	$email->status = 'Sent'; // Setting status as sent
	          	}else{
					$email->lastRetryCount = ($email->lastRetryCount + 1);
					$email->lastRetry = date('Y-m-d H:i:s');
		            $email->status = 'Retry'; // Setting status queued
	        	}
	            $i++; // increasing counter by 1.
	          	$email->save(); // Saving the model 
	        }
	    }
  	}
  	public function generateSiteMap(){
		$stores=Stores::find()->where(['isVirtual'=>0])->all();
		foreach ($stores as $key => $store) {
			$testxmlData = $store->getSiteMap();			
			if(file_put_contents('frontend/web/sitemaps/store_'.$store->id.'.xml',$testxmlData)){        	
            	echo $store->id.$store->title.'   '; 
        	}
		}
	}
	public function actionTest() {
		$this->generateSiteMap();		
	}

	public function doOrderStatusUpdate()
	{
		$orders = Orders::find()->where(['type'=>'byod'])->all();
 		foreach ($orders as $order) {
 			if($order->portal->shipment_type == "delivery-program"){
 				if($order->portal->isDeliveredToday()){
 					$order->status = "fully-collected";
 					$order->save(false);
 				}	
 			}
 		}
	}
 	
 	public function disableScheduledProducts(){
		$stores=Stores::find()->where(['isVirtual'=>0])->all();
		$cnt = 0;
		foreach ($stores as $key => $store) {
			$tempCnt = $store->disableScheduledStoreProducts($store->id);
			$cnt +=  $tempCnt;
		}
        echo $cnt." scheduled products disabled";
 	}

}