<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class ContactForm extends \yii\base\Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;
    public $phone;
    public $enquiry;
    public $uploadFile;

    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body','subject'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            [['uploadFile'], 'file','extensions' => 'png,jpg,JPG,jpeg,JPEG,gif,tiff,psd,doc,pdf,xls,docx,Ai,eps,xlsx,csv,txt,rtf,html,zip', 'maxFiles' => 10],
            [['subject'], 'safe'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],

            //['verifyCode', 'captcha','captchaAction'=>'/b2b/site/captcha'],
        ];
    }

    public function attributeLabels(){
    return ['body' => 'Enquiry', 'uploadFile'=>'Upload Files'];
    }
    
    public function getLogo()
    {
        return '<img  src='.Yii::$app->params["rootUrl"].'/store/site/logo/logo.png>';
       
    }

   

    public function sendEmail($storeId,$links) // send to admin (normal user)
    {
        //var_dump($this->subject);die;
        $name=$this->name;
        $email=$this->email;
        $subject=$this->subject;        
        $body=$this->body;         
            //var_dump($subject) ;die; 
        $template = \common\models\EmailTemplates::find()->where(['code' => 'admincontactus'])->one();
        $store=\common\models\Stores::find()->where(['id' => $storeId])->one();
        $user= \common\models\User::find()->where(['id' => $store->adminId])->one();        
        $queue = new \common\models\EmailQueue;
        $queue->models = ['store'=>$store,'user'=>$user,'addressId'=>''];
//        $links="";
//        if (!empty($model->uploadFile)) {
//            foreach ($model->uploadFile as $file) {
//                $random_name = $file->baseName . '_' . Yii::$app->security->generateRandomString() . '.' . $file->extension;
//                $filename = \Yii::$app->basePath . "/../store/attachments/" . $random_name; # i'd suggest adding an absolute path here, not a relative.
//                $file->saveAs($filename);
//                $files[$filename] = $random_name;
//                
//                $url=Yii::$app->params["rootUrl"]."/store/attachments/".$random_name;
//                $links.="<a href='$url' target='_blank'>$file->name</a><br/><br/>";
//            }
//            //$queue->attachments = json_encode($files);
//        }
        
        
//        
//        print_r($queue->attachments);
//        exit;
        //$queue->recipients =Yii::$app->params['adminEmail']; 
        //$queue->recipients ='anto@xtrastaff.com.au,sreedev@xtrastaff.com.au';
        $queue->recipients ='anto@xtrastaff.com.au,joy.j@xtrastaff.com.au,vicky.s@xtrastaff.com.au';
        $queue->cc = 'mark@xtrastaff.com.au,'.Yii::$app->params['adminEmail'];
        //$queue->recipients=isset(Yii::$app->params['tempcontactusemail']) ? Yii::$app->params['tempcontactusemail'] : $queue->recipients;   
        $queue->replacements =['emailFacebookImage'=>'','emailTwitterImage'=>'','emailInstagramImage'=>'',
            'emailYoutubeImage'=>'','emailPinterestImage'=>'','emailGoogleplusImage'=>'',
            'emailLinkedinImage'=>'','logo'=>$this->logo,'useremail'=>$queue->recipients,'name'=>$name,'email'=>$email,'phone'=>'','body'=>$body,'store_title'=>$store->title,'store_email'=>$store->email,'links'=>$links];
        $queue->from = $email;
        $queue->creatorUid = Yii::$app->user->id;
        $queue->attributes = $queue->fillTemplate($template);//var_dump($queue->message);die;
        $queue->subject= $subject ."- New Support Request from ".$store->title ;
        return $queue->save();
    }
    
    // ================================================================================
    // store_token, load_token, delete_token are SAMPLE functions! please replace with your own!
    public function store_token($token, $name) {
        file_put_contents(\Yii::$app->basePath . "/../store/tokens/$name.token", serialize($token));
    }

    public function load_token($name) {
        if (!file_exists(\Yii::$app->basePath . "/../store/tokens/$name.token"))
            return null;
        return @unserialize(@file_get_contents(\Yii::$app->basePath . "/../store/tokens/$name.token"));
    }

    public function delete_token($name) {
        @unlink(\Yii::$app->basePath . "/../store/tokens/$name.token");
    }

    // ================================================================================

    public function handle_dropbox_auth($dropbox) {
        // first try to load existing access token
        $access_token = $this->load_token("access");
        if (!empty($access_token)) {
            $dropbox->SetAccessToken($access_token);
        } elseif (!empty($_GET['auth_callback'])) { // are we coming from dropbox's auth page?
            // then load our previosly created request token
            $request_token = $this->load_token($_GET['oauth_token']);
            if (empty($request_token))
                die('Request token not found!');

            // get & store access token, the request token is not needed anymore
            $access_token = $dropbox->GetAccessToken($request_token);
            $this->store_token($access_token, "access");
            $this->delete_token($_GET['oauth_token']);
        }

        // checks if access token is required
        if (!$dropbox->IsAuthorized()) {
            // redirect user to dropbox auth page
            $return_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?auth_callback=1";
            $auth_url = $dropbox->BuildAuthorizeUrl($return_url);
            $request_token = $dropbox->GetRequestToken();
            $this->store_token($request_token, $request_token['t']);
            die("Authentication required. <a href='$auth_url'>Click here.</a>");
        }
    }

}
