<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * CheckoutForm is the model behind the checkout form.
 */
class ConfigurationForm extends Model
{

    public $general_sender_name, $general_sender_email, $email_domain, $smtp_host, $smtp_username, $smtp_password, $smtp_port, $smtp_encryption, $mainbanner_resolution,$sidebanner_resolution,$stripbanner_resolution,$mobilebanner_resolution,
    $minibanner_resolution, $shipping_discount_threshold, $is_sidebanner_facebook,$admin_email,$facebook,$twitter,$instagram,$youTube,$pinterest,$linkedin,$google,
    $meta_title,$meta_keywords,$meta_description,$order_confirmation_recipients,$storeId,$gift_wrapping,$contact_form_recipients,$google_analytics,$replyTo,$fromEmail,$gst_percentage;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['general_sender_name', 'general_sender_email', 'smtp_host', 'smtp_username', 'smtp_password', 'smtp_port', 'order_confirmation_recipients','storeId','gift_wrapping','contact_form_recipients','google_analytics','replyTo','fromEmail','gst_percentage'], 'safe'],
            //[['general_sender_name', 'general_sender_email', 'smtp_host', 'smtp_username', 'smtp_password', 'smtp_port','meta_title','meta_keywords','meta_description'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'general_sender_name' => 'Sender Name',
            'general_sender_email' => 'Sender Email',
            'order_confirmation_recipients' => 'Order Notification Recipients'
        ];
    }
}
