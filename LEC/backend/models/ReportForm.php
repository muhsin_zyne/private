<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * CheckoutForm is the model behind the checkout form.
 */
class ReportForm extends Model
{
    public $start_date, $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date'], 'safe'],
            [['start_date', 'end_date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entityType' => 'Entity Type',
        ];
    }
}