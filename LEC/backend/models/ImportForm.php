<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * CheckoutForm is the model behind the checkout form.
 */
class ImportForm extends Model
{
    public $entityType, $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entityType', 'file'], 'safe'],
            [['entityType', 'file'], 'required'],
            [['file'], 'file', 'extensions' => ['csv']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entityType' => 'Entity Type',
        ];
    }
}