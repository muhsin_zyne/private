<?php

namespace common\models;

use yii\base\Model;
use common\models\images;

/**
 * UploadForm is the model behind the upload form.
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file'],
        ];
    }
}


?>