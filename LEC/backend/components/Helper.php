<?php

/**
 * @author: Max
 */

namespace backend\components;
use yii\base\Component;

class Helper extends Component{
	public static function slugify($text){
	    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
	    $text = trim($text, '-');
	    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	    $text = strtolower($text);
	    $text = preg_replace('~[^-\w]+~', '', $text);
	    if (empty($text))
	      	return 'n-a';
	  	return $text;
	}

	public static function money($string){
		setlocale(LC_MONETARY, 'en_AU');
		$symbol = 'AU$';
		$color = (preg_match("/-/",$string)) ? 'red' : '';
		$string = ($string == null) ? 0 : $string;
		return "<span class='$color'>$symbol".money_format("%!n", trim($string))."</span>";
	}

	public static function date($string, $format = "d/m/Y"){
		return date($format, strtotime($string));
	}

	public static function convertDate($string){  //var_dump($string);die();
		$recieved_date = explode('-', $string);
		//var_dump($recieved_date);die();
        $date = $recieved_date[1] . '/' . $recieved_date[0] . '/' . $recieved_date[2];
        $new_date = date('Y-m-d H:i:s', strtotime($date));
		return $new_date;
	}

	public static function convertPromotionDate($string){  //var_dump($string);die();
		$recieved_string = explode(' ', $string);
		$timestamp = date('Y-m-d H:i:s',strtotime($recieved_string[0]));
		return $timestamp;
		//var_dump($timestamp);die();
		//$recieved_date = explode('-', $recieved_string[0]);
		//var_dump($recieved_date);die();
        /*$date = $recieved_date[1] . '/' . $recieved_date[0] . '/' . $recieved_date[2];
        $new_date = date('Y-m-d H:i:s', strtotime($date));
        //var_dump($new_date);die();
		return $new_date;*/
	}

	public static function isJson($string) {
	    $decoded = json_decode($string);
	    return (json_last_error() == JSON_ERROR_NONE && !is_float($decoded));
	}

	public static function clientIp(){
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
		{
		    if (array_key_exists($key, $_SERVER) === true)
		    {
		        foreach (explode(',', $_SERVER[$key]) as $ip)
		        {
		            if (filter_var($ip, FILTER_VALIDATE_IP) !== false)
		            {
		                return $ip;
		            }
		        }
		    }
		}
	}

	public static function alterCondition($where, $find, $replace){
		foreach($where as $i => $condition){
			if(is_array($condition)){
				$where[$i] = self::alterCondition($condition, $find, $replace);
			}elseif(is_string($condition)){
				if(strpos($condition, $find) !== false)
					$where[$i] = str_replace($find, $replace, $condition);
			}
		}
		return $where;
	}

	public static function getPriceExclGST($price,$gstPercentage){
		$exclPrice = ($price*100)/(100+$gstPercentage);
		return round($exclPrice,2);
	}

	public static function localDate($date, $format = 'Y-m-d H:i:s', $defaultTimezone = 'UTC', $toTimezone = 'Australia/Sydney'){
		date_default_timezone_set($defaultTimezone);
	    $new_date = new \DateTime($date);
	    $new_date->setTimeZone(new \DateTimeZone($toTimezone));
	    return $new_date->format($format);
	}

}