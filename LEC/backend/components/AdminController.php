<?php
namespace app\components;
use Yii;
use yii\helpers\Url;
use common\models\User;
use common\models\Configuration;

class AdminController extends \common\components\MainController
{
	public $title;
	public $mainMenu;
  	public $siteAdminPermissions = [
      'Index' => ['site/index', 'site/error', 'site/logout', 'site/loginas','site/contact','site/print-mail'],
      'Products' => ['reports/products'],
      'ProductImages' => ['product-images/upload'],
      'Dashboard' => ['site/index'],
      //'Suppliers' => ['suppliers/index','suppliers/view','suppliers/enable'],
      'System' => [],
      'Configurations' => ['configuration/index', 'configuration/update'],
      //'Manage Featured Products' => ['products/featured','products/list'],
      // 'Store Management' =>['stores/manage','stores/dummyproducts'],
      'Brands' => ['brands/index','brands/view','brands/enable','brands/test','brands/list'],
      'Sales' => ['sales/create', 'sales/view'],
      'Orders' => ['orders/create', 'orders/view','orders/index' , 'orders/update','orders/changestatus','orders/sendmail','orders/delete','orders/giftregistry-orders','orders/failed-transactions'],
      'Invoices' => ['invoices/create', 'invoices/view','invoices/index', 'invoices/update', 'invoices/sendmail', 'invoices/print'],
      'Reviews and Ratings' => ['reviews/view', 'ratings/view'],
      'Shipments' => ['shipments/create', 'shipments/view', 'shipments/index', 'shipments/update', 'shipments/sendmail', 'shipments/carrier'],
      'Deliveries' => ['delivery/create', 'delivery/view', 'delivery/index', 'delivery/update', 'delivery/sendmail', 'delivery/pdf'],
      'Credit Memos' => ['credit-memos/create', 'credit-memos/view','credit-memos/index','credit-memos/sendmail'],
      'Transactions' => ['transactions/create', 'transactions/view'],
      'Manage Products' => ['products/create', 'products/view', 'products/index', 'products/update', 'products/enable','products/delete', 'products/getsupplierbrands','products/export','products/refreshdisabledate'],
      'Customer Reviews' => ['customerReviews/create','customerReviews/view'],
      'Pending Reviews' => ['pendingReviews/create','pendingReviews/view'],
      'All Reviews' => ['allReviews/create','allReviews/view'],
      'Reports' => ['reports/orders', 'reports/invoiced', 'reports/refunds', 'reports/shipped', 'reports/orders', 'reports/carts', 'reports/abandonedcarts'],
      'Manage Ratings' => ['manageReviews/create','manageReviews/view'],
      'Users' => ['users/create','users/view', 'users/index','users/export','users/delete'],
      'Subscribers' => ['subscribers/create','subscribers/view', 'subscribers/index','subscribers/export','subscribers/delete'],
      'Manage Users' => ['manageCustomers/create','manageCustomers/view'],
      'CMS' => ['cms/index','cms/create','cms/view','cms/update','cms/delete','cms/image-browse','cms/image-upload'],
      'CmsImages' => ['cms-images/index','cms-images/create','cms-images/view','cms-images/getimages'],
      'CMS Blocks' => ['cms-blocks/index','cms-blocks/create','cms-blocks/view','cms-blocks/update','cms-blocks/delete','cms-blocks/getimages','cms-blocks/imageupload','cms-blocks/image-browse','cms-blocks/image-upload'],
      'CMS Category' => ['category-blocks/index','category-blocks/create','category-blocks/view','category-blocks/update','category-blocks/delete','category-blocks/categoryname','category-blocks/image-browse','category-blocks/image-upload'],
      'Gift Vouchers' => ['gift-vouchers/index', 'gift-vouchers/create', 'gift-vouchers/view', 'gift-vouchers/update', 'gift-vouchers/delete','gift-vouchers/redeem'],
      'Coupon Codes' => ['price-rules/index', 'price-rules/create', 'price-rules/update', 'price-rules/getoptions', 'price-rules/delete','price-rules/view'],
      'Reviews' => ['reviews/create','reviews/view'],
      //'Shopping Cart' => ['shoppingCart/create','shoppingCart/view'],
      'Pending Reviews' => ['pendingReviews/create','pendingReviews/view'],
      'Invoiced' => ['invoiced/create','invoiced/view'],
      'Shipping' => ['shipping/create','shipping/view'],
      'Refunds' => ['refunds/create','refunds/view'],
      'Products Carts' => ['productsCarts/create','productsCarts/view'],
     // 'Abandoned Carts' => ['abandonedCarts/create','abandonedCarts/view'],
      'Bestsellers' => ['bestsellers/create','bestsellers/view'],
      'Products Ordered' => ['productsOrdered/create','productsOrdered/view'],
      'Most Viewed' => ['mostViewed/create','mostViewed/view'],
      'New Accounts' => ['newAccounts/create','newAccounts/view'],
      'Customers by Orders Total' => ['customersByOrdersTotal/create','customersByOrdersTotal/view'],
      'Customers by Number of Orders' => ['customersbyNumberofOrders/create','customersbyNumberofOrders/view'],
      'Customers Reviews' => ['customersReviews/create','customersReviews/view'],
      'Products Reviews' => ['productsReviews/create','productsReviews/view'],
      'Banners' => ['banners/create','banners/view','banners/index','banners/assignbanners','banners/configuration','banners/delete','banners/update','banners/sortbanners','banners/minibannersave'],
      'Menus' => ['menus/create','menus/view','menus/index','menus/update','menus/updatemenu','menus/delete'],
      'Add Banners' => ['addBanners/create','addBanners/view'],
      'Gallery' => ['gallery/create','gallery/view','gallery/index','gallery/update','gallery/delete','gallery/sortable'],
      'GalleryImages' => ['gallery-images/create','gallery-images/view','gallery-images/index','gallery-images/delete','gallery-images/update','gallery-images/sortable'],
      'Coupon Code Management' => ['couponCodeManagement/create','couponCodeManagement/view'],
      'Menu' => ['menus/create', 'menus/index', 'menus/update', 'menus/delete'],
      'Social Media Management' => ['social-media/index'],
      'Sales Comments' => ['sales-comments/create'],
      'Opt into Promotion' => ['b2b/promotions','b2b/view','b2b/accept','b2b/cancel'],
      'Order from Supplier' => ['orders/order'],
      'GiftRegistry' => ['gift-registry/index','gift-registry/create','gift-registry/update','gift-registry/view','gift-registry/delete','gift-registry/getproducts'],
      'GiftRegistryProducts' => ['giftregistryproducts/index'],
       'BYOD' => ['byod/index','byod/create','byod/update','byod/view','byod/getproducts','byod/delete','byod/report','byod/assign'],
       'BYOD Orders' => ['byod-orders/index','byod-orders/view','byod-orders/update','byod-orders/pdf'],
       'Downloads'=>['downloads/create','downloads/view','downloads/index','downloads/delete','downloads/update'],
      'Client Portal' => ['client-portal/index','client-portal/create','client-portal/update','client-portal/view','client-portal/getproducts','client-portal/orders','client-portal/order-detail','client-portal/delete','client-portal/report','client-portal/send-notification'],
      'Appointments' => ['appointments/index','appointments/update','appointments/delete','appointments/view','appointments/create','appointment-settings/index'],
      'Help' => ['help/index', 'help/view', 'help/list'],
  ];
  public $b2cOnlyMenus = [
    'Opt into Promotion', 'Social Media Management','Order from Supplier','Manage Featured Products','Appointments','GiftRegistry','BYOD', 'Client Portal','BYOD Orders'
  ];

  public $globalAccessUrls;

	public function init(){
		$this->globalAccessUrls = [Url::to(['/site/request-password-reset']), Url::to(['/site/reset-password']), Url::to(['/site/login']), Url::to(["/site/clientip"]), Url::to(['/site/loginas'])];
    parent::init();
  }
	public function beforeAction($action){ //if(isset($_REQUEST['debuug'])){ var_dump(\Yii::$app->getUser()->loginUrl); die; }
		if (\Yii::$app->getUser()->isGuest && !in_array("/".\Yii::$app->getRequest()->resolve()[0], $this->globalAccessUrls)){
            return \Yii::$app->getResponse()->redirect(\Yii::$app->getUser()->loginUrl);
        }
        if(!$this->hasPermission($action->controller->id."/".$action->id)){
            die('403: Not allowed');
        }
        //var_dump(Yii::$app->params['storeId']);die;
        if(!isset(Yii::$app->params['storeId']))
            Yii::$app->params['storeId'] = isset(Yii::$app->user->identity->store)? Yii::$app->user->identity->store->id : null;
        if(isset(Yii::$app->user->identity)){
          $user = User::findOne(Yii::$app->user->id);
          if($user->roleId == "3") {
            Yii::$app->params['temp']['siteConfig'] = require(__DIR__ . '/../../frontend/config/'.trim(Yii::$app->user->identity->store->configPath).'');
         
            //var_dump(Yii::$app->params['temp']['siteConfig']['components']);die();  
          }
        }   

         $this->mainMenu= require('../config/menu.php');

		return parent::beforeAction($action);
	}


  //Yii::$app->controller->id;
  //Yii::$app->controller->action->id;
  //Yii::$app->controller->getRoute()

    public function isVisible($key,$type=NULL){
        if(isset(Yii::$app->user->id)) {
            $user = Yii::$app->user->identity;
            if($user->roleId == "3") {
                if(array_key_exists($key, $this->siteAdminPermissions))
                {
                    if($key=='Banners') // only for site admin banner managment 
                    { 
                       if(Configuration::findSetting($type, Yii::$app->params['storeId'])!=NULL)
                            return true;
                        else
                            return false;                        
                    }
                    return true;
                }
                else 
                    return false;
            }elseif($user->roleId == "1"){
              if(!in_array($key, $this->b2cOnlyMenus))
                return true;
              else
                return false;
            }       
        }           
    }

    public function hasPermission($action){
        if(isset(Yii::$app->user->id)) {
            $user = User::findOne(Yii::$app->user->id);
            if($user->roleId == "3") {
                foreach($this->siteAdminPermissions as $permission){
                    if(in_array($action, $permission)){  //var_dump($permission); die;
                        return true;
                    }
                }
                return false;
            } 
            else {
                return true;
            }
        } 
        return true;
    }
}
