<?php 
namespace backend\components;
use Yii;
class SquareValidator extends \yii\validators\ImageValidator
{
    /**
     * @inheritdoc
     */
    protected function validateImage($image)
    {
        if (false === ($imageInfo = getimagesize($image->tempName))) {
            return [$this->notImage, ['file' => $image->name]];
        }

        list($width, $height) = $imageInfo;

        if ($width !== $height) {
            return [Yii::t('yii', 'The image "{file}" is not square.'), ['file' => $image->name]];
        }

        return parent::validateImage($image);
    }
}
?>