<?php
namespace backend\components;
use Yii;
use yii\bootstrap\Widget;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use common\models\Categories;

class TreeView extends Widget{
    public $category = null;
    public $readOnly = true;

    public function init()
    {

        $categories = Categories::find()
            ->where(['active' => '1'])
            ->andWhere('id != 0')
            ->asArray()
            ->all();

        $category_tree = [];                

        foreach ($categories as $category) {
            if($category['parent'] == 0){
                $category_tree[] = [
                    'label'=> $category['title'],
                    'id'=> $category['id'],
                    'position' => $category['position'],
                    'items' => $this->getChild($categories,$category['id'])
                ];
            }
        }

        //var_dump($category_tree);die();

        echo ' <!--<div class="searchbox">
                   <span class="tree_keywordtext"> Search Here </span> <input type="text" class="tree_keyword" placeholder="search here" name="keyword">
                </div>-->
                <div class="dd easy-tree" id="nestable">
                <ol class="dd-list">';

        $tree = $this->renderTree($category_tree);
        echo $tree;

        echo "</ol></div>";
        echo "<script type=\"text/javascript\">
                $(document).ready(function(){
                    var updateOutput = function(e)
                    {
                        var list   = e.length ? e : $(e.target),
                            output = list.data('output');
                        if (window.JSON) {
                            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                        } else {
                            output.val('JSON browser support required for this demo.');
                        }
                    };

                    var treeOrder = $('.nestable-output').val();
                    var url = $(location).attr('href');

                    $('#nestable').nestable({
                        group: 1, 
                    })
                    .on('change', updateOutput);

                    updateOutput($('#nestable').data('output', $('#nestable-output')));

                   /* $('.easy-tree').EasyTree({
                        addable: true,
                        editable: true,
                        deletable: true,
                        collapse:false
                    });*/

                    /*if($('.selectcat').is(':checked')){
                        $(this).parent().find('ol').toggle();
                    }*/
                });
                
                $('.categories-index .dd').on('change', function() {
                    setTimeout(function(){  
                    var treeOrder = $('.nestable-output').val();
                    $.ajax({
                                url: '".Url::to(['categories/changeorder'])."', 
                                dataType: 'json',
                                type : 'post',  
                                data: {treeOrder: treeOrder}, 
                                success: function(data){
                                       
                                }
                            });
                    }, 1000);    
                });
                
                $('.dd3-content .fa-plus').click(function(){
                    $(this).toggleClass('fa-plus fa-minus');
                    /*$(this).parent().next().css('display','block');*/
                    $(this).parent().next().toggle();
                });

                $('.dd3-content span').click(function(){
                    $(this).siblings('.fa').toggleClass('fa-plus fa-minus');
                    $(this).parent().next().toggle();
                });

                </script>";
    } 

    public function getChild($categories,$parent){
        $child_elements = [];
        foreach ($categories as $category) {
            if($category['parent'] == $parent){
                $child_elements[] = [
                    'label'=> $category['title'],
                    'id'=> $category['id'],
                    'position'=>$category['position'],
                    'items' => $this->getChild($categories,$category['id'])
                ];
            }   
        }

        return $child_elements;
    }

    public function sorts($x, $y) {
        return $x['id'] - $y['id'];
    }

    public function renderTree($category_tree){  
        $html ='';

        usort($category_tree, function($x, $y){return $x['position'] - $y['position'];});

        foreach ($category_tree as $category) {  

            if($this->readOnly){
                if(!empty($category['items'])){
                    $html .="<li data-id='".$category['id']."'><input type='checkbox' name='selectcat' class='selectcat simple' value='".$category['id']."'><div class='dd3-content'><i class='fa fa-plus'></i><span class='category-text'>".$category['label']."</span></div>";
                    $html .="<ol class='dd-list' style='display:none'>";
                    $html .=$this->renderTree($category['items']);
                    $html .="</ol>";
                    $html .="</li>";
                }
                else{
                    $html .="<li data-id='".$category['id']."'><input type='checkbox' name='selectcat' class='selectcat simple' value='".$category['id']."'><div class='dd3-content'><span class='category-text'>".$category['label']."</span></div>";

                    $html .="</li>";
                }
            }
                
            else{
                if(!empty($category['items'])){
                    $html .="<li class='dd-item' data-id='".$category['id']."'><div class='dd-handle dd3-handle'></div><div class='dd3-content'><i class='fa fa-plus'></i><span class='category-text'>".$category['label']."</span></div>";
                    $html .="<ol class='dd-list' style='display:none'>";
                    $html .=$this->renderTree($category['items']);
                    $html .="</ol>";
                    $html .="</li>";
                }
                else{
                    $html .="<li class='dd-item' data-id='".$category['id']."'><div class='dd-handle dd3-handle'></div><div class='dd3-content'><span class='category-text'>".$category['label']."</span></div>";

                    $html .="</li>";
                }
            }    
        }
        return $html;
    }
}

?>