<?php 

namespace backend\components;

/**
 * @author: Max
 **/

use Yii;
use yii\base\Component;
use backend\models\ImportForm;
use common\models\Products;
use common\models\User;
use common\models\Attributes;
use yii\helpers\ArrayHelper;
use common\models\Brands;
use common\models\Stores;
use backend\components\Helper;

class Importer extends Component{

    public $renewUrlKeys = false;
    public $defaultBrandId = 210;
    public $aliases = [
        'attribute_set' => 'attributeSetId',
        'product_type' => 'typeId',
    ];
    public $attributeValuesToBeVerified = [
        'attributeSet.title', 'product_type',
    ];
    public $attributeExceptions = [
        'configurable_attributes', 'sub_products', 'brand', 'base_image', 'small_image', 'thumbnail_image', 'category', 'quantity', 'isInStock', 'jenumber', 'parent'
    ];
    public $fileHandle;

    public function init(){
        ini_set('max_execution_time', 10000);
        ini_set('memory_limit', '5000M');
        $ddAttributes = Attributes::find()->where(['field' => 'dropdown'])->all();
        foreach($ddAttributes as $attribute)
            $this->attributeValuesToBeVerified[] = $attribute->code;
        return parent::init();
    }

    public function importProducts($rows, $skipExisting = true){ //var_dump($skipExisting); die;
        //$productExclusions = ['16419','16418','16417','16416','16415','16414','16412','16411','16410','16386','16385','16358','16361','16360','16359','16357','16353','16356','16355','16354','16413','16420','16395','16394','16393','16408','16396','16392','16388','16387','16391','16409','16425','16430','16429','16428','16421','16427','16426','16424','16423','16422','16352','16351','16368','16369','16373','16372','16364','16365','16374','16367','16366','16371','16370','16123','16127','16128','16131','16124','16125','16248','16247','16126','16375','16382','16346','16350','16349','16348','16345','16341','16344','16343','16342','16347','16362','16380','16381','16384','16383','16376','16377','16363','16379','16378','16389','16390','16293','16264','16263','16262','16294','16324','16327','16326','16325','16261','16265','16259','16252','16251','16250','16260','16267','16266','16269','16268','16323','16322','16333','16332','16331','16330','16317','16316','16303','16302','16301','16334','16335','16321','16320','16319','16318','16328','16329','16338','16337','16336','16249','16253','16279','16278','16339','16340','16280','16281','16272','16276','16277','16402','16401','16406','16405','16398','16397','16407','16404','16400','16399','16403','16273','16274','16270','16271','16287','16286','16255','16256','16254','16258','16257','16285','16284','16283','16282','16275','16290','16291','1628'];
        $sets = ArrayHelper::map(\common\models\AttributeSets::find()->all(), 'title', 'id');
        $brands = ArrayHelper::map(\common\models\Brands::find()->all(), 'title', 'id');
        $owners = ArrayHelper::map(\common\models\User::find()->where('roleId=3')->all(), 'jenumber', 'self');
        $attrs = Attributes::find()->all();
        $cats = ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title');
        $attributeIds = ArrayHelper::map($attrs, 'code', 'id');
        //var_dump($rows); die;
        $status = [];
        foreach($rows as $i => $row){
            //$row = ['attribute_set' => 'Jewellery'] + $row;
            $product = Products::find()->where(['sku' => $row['sku']])->one();
            if(($product && $skipExisting)/* || in_array($product->id, $productExclusions)*/){
                //skip this iteration
                continue;
            }elseif(!$product){
                $product = new Products;
            }elseif($product && !$skipExisting){
                //continue this iteration with the loaded product
            }
            foreach($row as $attr => $value){
                if($attr=="attribute_set"){
                    $_REQUEST['Products']['attributeSetId'] = $sets[$value];
                    $product->attributeSetId = $sets[$value];
                    //var_dump($product->attributeSetId); die;
                }
                if($attr=="brand"){
                    $product->brandId = isset($brands[$value])? $brands[$value] : $this->defaultBrandId;
                }
                // if($attr == "parent" && $value != ""){
                //     $product->visibility = "not-visible-individually";
                // }
                if(isset($this->aliases[$attr])){
                    $attribute = $this->aliases[$attr];
                    if($attribute!="attributeSetId")
                        $product->$attribute = $value;
                }
                elseif(($product->hasAttribute($attr))){ //echo $attr;
                    $attribute = $attr;
                    if($attribute == "urlKey" && Products::find()->where(['urlKey' => $value])->exists() && $product->isNewRecord && $value != ""){
                        $value = $value . "-" . $row['sku'];
                    }
                    $product->$attribute = $value;
                }
                else
                    if($i == 0 && !in_array($attr, $this->attributeExceptions)){
                        $result['attributeNotFound'][] = "Attribute: \"$attr\" does not exist.";
                        continue;
                    }
                    elseif(in_array($attr, $this->attributeExceptions))
                        $attribute = $attr;
                
            } //$product->validate(); var_dump($product->getErrors());die;
            //var_dump($product->typeId); var_dump($row); var_dump($product->hasAttribute('f_size')); die;
            if($product->urlKey == "")
                $product->urlKey = $this->generateUrlKey($product);
            if($product->validate() || $row['parent'] != ""){ // bypass validation if it's a sub product
                $customAttrs = array_diff($product->attributes(), $product->productAttrs); // filtering the custom attributes
                foreach($customAttrs as $customAttr)
                    $customAttributes[$customAttr] = $product->getAttributes()[$customAttr];
                $product->createdBy = "0";
                if(!$product->save(false, $product->productAttrs))
                    { var_dump($product->getErrors()); var_dump(" - urlKey: ".$product->urlKey); continue;}
                if(isset($customAttributes))
                    $product->saveCustomAttributes($customAttributes);
                    var_dump($customAttributes); die;
                if(isset($row['jenumber']) && $row['jenumber']!=""){ 
                    $product->createdBy = $owners[$row['jenumber']]->id;
                    $product->save(false, $product->productAttrs);
                    if(!\common\models\StoreProducts::find()->where(['storeId' => $owners[$row['jenumber']]->store->id, 'productId' => $product->id, 'type' => 'b2c'])->exists()){
                        $storeProduct = new \common\models\StoreProducts;
                        if(!is_object($owners[$row['jenumber']]->store)){
                            var_dump($row['jenumber']." has no store!"); die;
                        }
                        $storeProduct->storeId = $owners[$row['jenumber']]->store->id;
                        $storeProduct->productId = $product->id;
                        $storeProduct->type = "b2c"; 
                        $storeProduct->save(false);
                    }
                }
                if(isset($row['category']) && $row['category']!=""){
                    $categories = explode(",", $row['category']);
                    \common\models\ProductCategories::deleteAll("productId='".$product->id."' AND storeId = 0");
                    foreach($categories as $category){
                        $category = trim($category);
                        if(isset($cats[$category])){
                            $prodCat = new \common\models\ProductCategories;
                            $prodCat->productId = $product->id;
                            $prodCat->categoryId = $category;
                            $prodCat->storeId = '0';
                            if(!$prodCat->save(false)){
                                var_dump($prodCat->getErrors()); die;
                            }
                        }
                    }
                }
                if(isset($row['quantity']) && $row['quantity']!=""){
                    $inventory = new \common\models\ProductInventory;
                    $inventory->productId = $product->id;
                    $inventory->storeId = 0;
                    $inventory->quantity = $row['quantity'];
                    $inventory->manageStock = '1';
                    $inventory->isInStock = isset($row['isInStock'])? $row['isInStock'] : '1';
                    if(!$inventory->save(false)){
                        var_dump($inventory->getErrors()); die;
                    }
              	}
                if(isset($row['small_image'])){
		    \common\models\ProductImages::deleteAll("productId='".$product->id."' AND isSmall = '1'");
                    $image = new \common\models\ProductImages;
                    $image->path = $row['small_image'];
                    $image->productId = $product->id;
                    $image->isSmall = "1";
                    $image->save(false);
                }
                if(isset($row['thumbnail_image'])){
		    \common\models\ProductImages::deleteAll("productId='".$product->id."' AND isThumbnail = '1'");
                    $image = new \common\models\ProductImages;
                    $image->path = $row['thumbnail_image'];
                    $image->productId = $product->id;
                    $image->isThumbnail = "1";
                    $image->save(false);
                }
                if(isset($row['base_image'])){
	            \common\models\ProductImages::deleteAll("productId='".$product->id."' AND isBase = '1'");
                    $image = new \common\models\ProductImages;
                    $image->path = $row['base_image'];
                    $image->productId = $product->id;
                    $image->isBase = "1";
                    $image->save(false);
                }
                if(isset($row['configurable_attributes']) && $row['configurable_attributes']!=""){ 
                    $configAttrs = explode(",", $row['configurable_attributes']);
                    \common\models\ProductSuperAttributes::deleteAll("productId='".$product->id."'");
                    \common\models\AttributeOptionPrices::deleteAll("productId='".$product->id."'");
                    $subProducts = explode(",", $row['sub_products']);
                    \common\models\ProductLinkages::deleteAll("parent='".$product->id."'");
                    foreach($subProducts as $sku){
                        $sku = trim($sku);
                        if($subProductRow = $this->findRowBy('sku', $sku, $rows)){
                            if($subProduct = Products::find()->where(compact('sku'))->one())
                            	$subProductCache[$product->id][$subProduct->id] = $subProduct;
			    else{ var_dump(compact('sku')); die; }
                            $linkage = new \common\models\ProductLinkages;
                            $linkage->parent = $product->id;
                            $linkage->productId = $subProduct->id;
                            $linkage->type = "configurable";
                            $linkage->save(false);
                            // $subProduct->visibility = "not-visible-individually";
                            // $subProduct->status = '1';
                            // $subProduct->save(false);
                        }else{
                            die('subProduct with sku: '.$sku.' not found');
                        }
                    } 
                    foreach($configAttrs as $attrib){
                        $attrib = trim($attrib);
                        $superAttr = new \common\models\ProductSuperAttributes;
                        $superAttr->productId = $product->id;
                        $superAttr->attributeId = $attributeIds[$attrib];
                        $superAttr->save(false);
                        $optionCache = []; 
                        foreach($subProductCache[$product->id] as $subProductId => $subProductFromCache){
                            $optionPrice = new \common\models\AttributeOptionPrices;
                            $optionPrice->productId = $product->id;
                            $optionPrice->attributeId = $attributeIds[$attrib];
                            //$optionId = 0;
                            $optionId = $subProduct->getOptionId($attributeIds[$attrib], $subProductFromCache->$attrib);
                            if(!$optionId || in_array($optionId, $optionCache)){
                                var_dump("Option '".$subProductFromCache->$attrib."'for attribute $attrib not found in it's AttributeOptions");
                                continue;
                            }
                            $optionCache[] = $optionId;
                            $optionPrice->optionId = $optionId;
                            $optionPrice->price = '0.00';
                            $optionPrice->type = 'fixed';
                            $optionPrice->save(false);
                        }
                    }
                }
                $product->assignBrandStores();
            }else{ var_dump($row);echo "-".$product->urlKey."-"; var_dump($product->getErrors()); die;
                $status = $status + $product->getErrors();
            }
        echo ($product->sku." updated.\n"); //die;
        }
        echo "status-";
        var_dump($status); die;
        return $status;
    }

    public function importCustomers($rows, $skipExisting = true){
        $stores = ArrayHelper::map(Stores::find()->where(['isVirtual' => 0])->all(), 'title', 'id');
        //var_dump($stores['Mia Bella Jewellers']); die;
        foreach($rows as $i => $row){
            $customer = User::find()->where(['email' => $row['email']])->one();
            if($customer && $skipExisting){
                //skip this iteration
                continue;
            }elseif(!$customer){
                $customer = new User;
            }elseif($customer && !$skipExisting){
                //continue this iteration with the loaded product
            }
            //var_dump($customer->isNewRecord); die;
            foreach($row as $attr => $value){
                $parts = explode(" ", trim($row['name']));
                //var_dump($parts); //die;
                $customer->email = $row['email'];
                $customer->setPassword('whatis01');
                $customer->firstname = $parts[0];
                if(isset($parts[1]))
                    $customer->lastname = $parts[2];
                $customer->status = 1;
                $customer->roleId = 4;
                //var_dump($stores[$row['store']]); die;
                $customer->storeId = $stores[$row['store']];
                $customer->save(false);
            }
        }
    }

	public function verifyProducts($rows, $scenario = "create"){
    	$product = new Products;
    	$sets = ArrayHelper::map(\common\models\AttributeSets::find()->all(), 'title', 'id');
    	$attrs = Attributes::find()->all(); //var_dump($attributes); die;
        $brands = ArrayHelper::map(\common\models\Brands::find()->all(), 'title', 'id');
        $cats = ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title');
        $owners = ArrayHelper::map(\common\models\User::find()->where('roleId=3')->all(), 'jenumber', 'id');
        $result = [];
        if(isset($rows[0]['product_type']))
            $rows = $this->sortBy('product_type', $rows);
        //var_dump($rows); die;
    	foreach($attrs as $a){
    		$attributes[$a->code] = ['required' => $a->required, 'validation' => $a->validation];

        }
        //var_dump($attributes); die;
        $urlKeysInCSV = $skuInCSV = [];
    	foreach($rows as $i => $row){
            //$row = ['attribute_set' => 'Jewellery'] + $row;
            $requiredAttributes = []; //var_dump($row); die;
    		foreach($row as $attr => $value){
                if($attr == "jenumber"){
                    if(!isset($owners[$value]))
                        $result['storeNotFound'][] = "Store with JE Number: \"$value\" does not exist.";
                }
    			if($attr=="attribute_set"){
    				$_REQUEST['Products']['attributeSetId'] = $sets[$value]; //var_dump($_REQUEST['Products']['attributeSetId']); die;
                    foreach($attrs as $a)
                        if(isset($a->attributeGroup))
                            if($a->attributeGroup->attributeSet->id == $sets[$value] && $a->required == "1")
                                $requiredAttributes[] = $a->code;
                }

    			if(isset($this->aliases[$attr]))
    				$attribute = $this->aliases[$attr];
    			elseif(($product->hasAttribute($attr)))
    				$attribute = $attr;
                elseif(isset($attributes[$attr]))
                    continue; //this attribute is not present in the current product's attribute set
    			else
    				if($i == 0 && !in_array($attr, $this->attributeExceptions)){ var_dump($attr); die('missing');
    					$result['attributeNotFound'][] = "Attribute: \"$attr\" doest not exist.";
    					continue;
    				}
    				elseif(in_array($attr, $this->attributeExceptions))
    					$attribute = $attr;
    			if($value=="" && !in_array($attribute, $this->attributeExceptions) && isset($attributes[$attribute]) && $attributes[$attribute]['required']==1 && $row['parent'] == ""){ // no parent = sub product of a config product and hence, no validation required.
    				$result['valueNotValid'][] = "Attribute: \"$attr\" is required at line#".($i+2);
    				continue;
    			}

    			//TODO:
    			//value validation
                if($attribute=="sku"){
                    if((Products::find()->where(['sku' => $value])->exists() && $scenario == "create") || in_array($value, $skuInCSV)){
                        $result['valueNotValid'][] = "SKU: \"$value\" already exists at line#".($i+2);
                    }else
                    $skuInCSV[] = $value;
                }

                // if($attribute=="urlKey"){
                //     if(($value != "" && (Products::find()->where(['urlKey' => $value])->exists() && !$this->renewUrlKeys) && $scenario == "create") || in_array($value, $urlKeysInCSV)){
                //         $result['valueNotValid'][] = "URL Key: \"$value\" already exists at line#".($i+2);
                //     }elseif($value != "")
                //         $urlKeysInCSV[] = $value; 
                // }

                if($attribute=="brand"){
                    if(!isset($brands[$value]) && $value != "")
                        $result['valueNotValid'][] = "Brand: \"$value\" does not exist at line#".($i+2);
                }
    			if(in_array($attribute, ($this->attributeValuesToBeVerified))){
    				if($attribute=="typeId"){
    					if(!in_array($value, ['simple', 'configurable']))
    						$result['valueNotValid'][] = "Product type: \"$value\" is not valid at line#".($i+2);
    					continue;
    				}
					if($attribute=="attributeSet.title"){
						if(!in_array($value, array_keys($sets)))
							$result['valueNotValid'][] = "Attribute Set: \"$value\" does not exists at line#".($i+2);
						continue;
					}
    				if(!isset(Yii::$app->params['temp']['attributeOptions'][$attribute]))
    					Yii::$app->params['temp']['attributeOptions'][$attribute] = ArrayHelper::map(\common\models\AttributeOptions::find()->join('JOIN', 'Attributes', 'Attributes.id = AttributeOptions.attributeId')->where(['Attributes.code'=>$attribute])->all(), 'id', 'value');
    				if(!in_array($attribute, $this->attributeExceptions) && !in_array($value, Yii::$app->params['temp']['attributeOptions'][$attribute]) && $value!="")
    					$result['valueNotValid'][] = "Value: \"$value\" for attribute: \"$attribute\" is not valid at line#".($i+2);
    			}
			}
            foreach($requiredAttributes as $reqAttr){
                if(!isset($rows[1][$reqAttr]))
                    $result['columnNotFound'][] = "Column: \"$reqAttr\" is required at line#".($i+2);
            }
            if(isset($row['category']) && $row['category']!=""){
                $categories = explode(",", $row['category']);
                foreach($categories as $category){
                    $category = trim($category);
                    if(!isset($cats[$category])){
                        $result['valueNotValid'][] = "Category with ID: \"$category\" does not exist at line#".($i+2);
                    }
                }
            }
			if(isset($row['sub_products']) && $row['sub_products']!=""){
				$subProducts = explode(",", $row['sub_products']);
				foreach($subProducts as $sku){
					$sku = trim($sku);
					if($subProductRow = $this->findRowBy('sku', $sku, $rows)){
						if($subProductRow['product_type'] != "simple"){
							$result['configurationNotValid'][] = "Sub Product with sku: \"$sku\" specified for configurable product[".$row['sku']."] at line#".($i+2)." is not a simple product.";
							continue;
						}
						$configAttrs = explode(",", $row['configurable_attributes']);
						foreach($configAttrs as $attrib){
							$attrib = trim($attrib);
							if(!isset($attributes[$attrib])){
								$result['configurationNotFound'][] = "Configurable attribute: \"$attrib\" specified at line#".($i+2)." not found.";
								continue;
							}
							if(!isset($subProductRow[$attrib]) || $subProductRow[$attrib]==""){ var_dump($subProductRow); die;
								$result['configurationNotValid'][] = "Sub Product with sku: \"$sku\" specified for configurable product at line#".($i+2)." has configurable attribute: \"$attrib\" as blank.";
								continue;
							}else{
								//var_dump($subProductRow[$attrib]); die;
							}
						}
					}else
						$result['configurationNotFound'][] = "Sub Product with sku: \"$sku\" specified for configurable product[".$row['sku']."] at line#".($i+2)." not found.";
				}
			}
    	}
        $status = true;
        foreach($result as $res){
            if(empty($res))
                $status = true;
            else{
                $status = false;
                break;
            }
        }
        $result['status'] = $status;
    	return $result;
    }

    public function verifyCustomers($rows, $scenario = "create"){
        $result['status'] = true;
        return $result;
    }

    public function processCsv($handle){
    	$i = 0;
        $header = null;
        $rows = array();
        while($row = fgetcsv($handle,0,",",'"')){
			if ($header === null) {
		        $header = $row;
		        continue;
		    }
		    if(count($header)!=count($row))
		    	{var_dump($header); var_dump($row); die;}
		    $rows[] = array_combine($header, $row);
        }
        //var_dump($rows); die;
        return $rows;
    }

    public function findRowBy($attribute, $value, $rows){ //var_dump($rows); die;
    	foreach($rows as $k => $row)
    		if($row[$attribute] == $value)
    			return $row;
    	return false;
    }

    public function sortBy($attribute, $rows){
        usort($rows,function($a, $b) use($attribute){
            return $a[$attribute] < $b[$attribute];
        });
        return $rows;
    }

    public function generateUrlKey($product){
        if($product->visibility == "not-visible-individually")
            return $product->sku;
        return Helper::slugify($product->name."-".$product->sku);
    }

}
