<?php

/**
 * @author: Max
 */

namespace backend\components;
use Yii;
use yii\base\Component;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\AttributeOptions;
use backend\components\Helper;

class AttributeRenderer extends Component{
	public $product;
	public $form;

	public function renderField($attr){
		$attr->productId = $this->product->id;
		return $this->{$attr->field}($attr);
	}

	public function text($attr){
		return $this->form->field($this->product, $attr->code)->textInput();
	}

	public function textarea($attr){
		return $this->form->field($this->product, $attr->code)->textArea();
	}

	public function date($attr){
		return DatePicker::widget([
			'form' => $this->form,
			'model' => $this->product,
			'attribute' => $attr->code,
			'pluginOptions' => [
				'format' => 'dd-mm-yyyy',
			],
		]);
	}

	public function boolean($attr){ // Yes/No dropdown
		return $this->form->field($this->product, $attr->code)->dropDownList(['0' => 'No', '1' => 'Yes']);
	}

	public function dropdown($attr){
		return $this->form->field($this->product, $attr->code)->dropDownList(ArrayHelper::map($attr->options, 'value', 'value'), ['prompt' => '-- Please Select --']);
	}

	public function multiselect($attr){
		$selected = [];
		if(!$this->product->isNewRecord){ 
			if(Helper::isJson($this->product->{$attr->code}) && $this->product->{$attr->code} != ""){
				$values = json_decode($this->product->{$attr->code}, true);	//var_dump($values); die;
				$options = ArrayHelper::map($attr->options, 'value', 'value');
				foreach($options as $option){
					if(in_array($option, $values))
						$selected[$option] = ['checked' => 'checked'];
				}
				//var_dump($selected);die;
			}
			
		}
		return $this->form->field($this->product, $attr->code)->dropDownList(ArrayHelper::map($attr->options, 'value', 'value'), ['multiple' => 'multiple', 'options' => $selected]);
	}

	public function price($attr){
		return $this->form->field($this->product, $attr->code)->textInput();
	}
}
