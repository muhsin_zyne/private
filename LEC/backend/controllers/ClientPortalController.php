<?php

namespace backend\controllers;

use Yii;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\ClientPortal;
use common\models\ClientPortalProducts;
use common\models\search\ClientPortalSearch;
use common\models\AttributeValues;
use common\models\Attributes;
use common\models\search\OrdersSearch;
use common\models\OrderItems;
use common\models\User;
use common\models\Invoices;
use common\models\OrderComment;
use common\models\CreditMemos;
use common\models\Shipments;
use common\models\SalesComments;
use common\models\Deliveries;
use common\models\Stores;
use common\models\search\DeliveriesSearch;
use common\models\search\ShipmentsSearch;
use common\models\search\InvoicesSearch;
use common\models\search\CreditMemosSearch;
use common\models\Orders;
use frontend\components\Helper;
use yii\web\UploadedFile;
use yii\db\ActiveQuery;
use kartik\mpdf\Pdf;

class ClientPortalController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	//var_dump(md5(Yii::$app->user->identity->store->id."testcode".microtime()));die();   

        $searchModel = new ClientPortalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $dataProvider->query->andWhere(['type'=>'client-portal','storeId'=>$user->store->id]);
        else
            $dataProvider->query->andWhere(['type'=>'client-portal']);
        $dataProvider->query->orderBy(['id'=>SORT_DESC,]);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    public function actionCreate(){
        $clientPortal = new ClientPortal();
        $clientPortalProducts = new ClientPortalProducts();
        $stores = Stores::find()->all();
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == "3")
            $storeId = Yii::$app->user->identity->store->id;
        else
            $storeId = $_POST["listname"];

        if ($clientPortal->load(Yii::$app->request->post())) {

            $clientPortal->validFrom =date('Y-m-d H:i:s', strtotime($clientPortal->validFrom));
            $clientPortal->expiresOn =date('Y-m-d H:i:s', strtotime($clientPortal->expiresOn));
            $clientPortal->storeId = $storeId;
            $clientPortal->type = "client-portal"; 
            $clientPortal->shipment_type = $_POST['ClientPortal']['shipment_type'];
            $clientPortal->studentAuthCode = md5($clientPortal->studentCode.$storeId.microtime());

            if(UploadedFile::getInstance($clientPortal,'organisation_logopath')!= '')
                {
                    $image = UploadedFile::getInstance($clientPortal, 'organisation_logopath');  
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/client-portal/logo/";    
                    $size = filesize($image->tempName);            
                    $ext = end((explode(".", $image->name)));
                    $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                    $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                    $uploadPath="/store/client-portal/logo/".$avatar;
                    $clientPortal->organisation_logopath = $uploadPath; 

                    $fileDimensions = getimagesize($image->tempName);
                    $imageType = strtolower($fileDimensions['mime']); 

                    switch(strtolower($imageType))
                    {
                        case 'image/png': 
                        $img = imagecreatefrompng($image->tempName);
                        break;

                        case 'image/gif':
                        $img = imagecreatefromgif($image->tempName);
                        break;
                        
                        case 'image/jpeg':
                        $img = imagecreatefromjpeg($image->tempName);
                        break;
                        
                        default:
                        die('Unsupported File!'); //output error
                    }  
                    
                    $clientPortal->width=imagesx($img);
                    $clientPortal->height=imagesy($img);

                    if($clientPortal->width!= 260 & $clientPortal->height!= 120)
                        {                
                            Yii::$app->getSession()->setFlash('error', 'Logo Size should be 260px X 120px');
                            return $this->render('create', compact('clientPortal'));
                        }
                    else{
                        $image->saveAs($savepath);    
                    }
                }
            
            if ($clientPortal->save(false)) {
                 if(isset($_POST['Products'])){
                    foreach ($_POST['Products']['id'] as  $id) {  
                        $clientPortalProduct = new ClientPortalProducts;
                        $clientPortalProduct->portalId = $clientPortal->id;
                        $clientPortalProduct->productId = $id;
                        $clientPortalProduct->offerPrice = $_POST['Products'][$id]['offerPrice'];
                        $clientPortalProduct->save(false);
                    }
                }
                $clientPortal->sendCreationNotification();
            } 
            $searchModel = new ClientPortalSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            Yii::$app->session->setFlash('success', 'Client Code Created Successfully !'); 
            return $this->redirect('index', compact('searchModel','dataProvider'));  
        }
        else{
            return $this->render('create', compact('clientPortal','clientPortalProducts','stores'));
        }    
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Client Code Deleted Successfully !');
        return $this->redirect(['index']);
    }

    public function actionUpdate($id){  
        $clientPortal = $this->findModel($id);
        $clientPortalProducts = $clientPortal->clientPortalProducts;
        //var_dump($clientPortalProducts);die();
        $logopath = $clientPortal->organisation_logopath;
        $stores = Stores::find()->all();

        if ($clientPortal->load(Yii::$app->request->post())){ 
            
            if(isset($_POST['Products']['deletedItems'])) {  
                ClientPortalProducts::deleteAll('id in ('.implode(",",$_POST['Products']['deletedItems']).')');
            }

                if(UploadedFile::getInstance($clientPortal,'organisation_logopath')!= ''){
                    $image = UploadedFile::getInstance($clientPortal, 'organisation_logopath');  
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/client-portal/logo/";    
                    $size = filesize($image->tempName);            
                    $ext = end((explode(".", $image->name)));
                    $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                    $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                    $uploadPath="/store/client-portal/logo/".$avatar;
                    $clientPortal->organisation_logopath = $uploadPath; 

                    $fileDimensions = getimagesize($image->tempName);
                    $imageType = strtolower($fileDimensions['mime']); 

                    switch(strtolower($imageType))
                    {
                        case 'image/png':
                        $img = imagecreatefrompng($image->tempName);
                        break;

                        case 'image/gif':
                        $img = imagecreatefromgif($image->tempName);
                        break;
                        
                        case 'image/jpeg':
                        $img = imagecreatefromjpeg($image->tempName);
                        break;
                        
                        default:
                        die('Unsupported File!'); //output error
                    }    
                    
                    $clientPortal->width=imagesx($img);
                    $clientPortal->height=imagesy($img);
                    if($clientPortal->width!= "260" & $byod->height!= "120")
                        {                
                            Yii::$app->getSession()->setFlash('error', 'Brand Logo Size should be 260px X 120px');
                            return $this->render('create', compact('clientPortal'));
                        }
                    else{
                            $image->saveAs($savepath);        
                    }
                }
                else
                    $clientPortal->organisation_logopath = $logopath;

            $clientPortal->shipment_type = $_POST['ClientPortal']['shipment_type'];
            $clientPortal->validFrom  = date('Y-m-d H:i:s', strtotime($clientPortal->validFrom));
            $clientPortal->expiresOn =date('Y-m-d H:i:s', strtotime($clientPortal->expiresOn));

            if($clientPortal->save(false)) {
                if(isset($_POST['Products']['id'])){ 
                    foreach ($_POST['Products']['id'] as  $id) { 

                        if(isset($_POST['Products'][$id]["newitem"])){ 
                            if(!ClientPortalProducts::find()->where(['productId'=>$id,'portalId'=>$clientPortal->id])->one()){
                                $clientPortalProduct = new ClientPortalProducts;
                                $clientPortalProduct->portalId = $clientPortal->id;
                                $clientPortalProduct->productId = $id;
                                $clientPortalProduct->offerPrice = $_POST['Products'][$id]['offerPrice'];
                                $clientPortalProduct->save(false); 
                            }       
                        }
                        else{ 
                            $clientPortalProduct = ClientPortalProducts::find()->where('id = '.$id.' and portalId = '.$clientPortal->id.'')->one();
                            $clientPortalProduct->offerPrice = $_POST['Products'][$id]['offerPrice'];
                            $clientPortalProduct->save(false);
                        }
                    }
                } 
            } 
            
            $searchModel = new ClientPortalSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            if(isset($_POST['update_email'])){
                $clientPortal->sendCreationNotification();
                Yii::$app->session->setFlash('success', 'Client Code Updated Successfully and Quote email send Successfully !');
            }
            else{    
                Yii::$app->session->setFlash('success', 'Client Code Updated Successfully !'); 
            }    
            return $this->redirect('index', compact('searchModel','dataProvider'));   
        }
        else{
             return $this->render('update',compact('clientPortal','clientPortalProducts','stores'));
        }    
    }

    public function actionView($id)
    {
        
        /*$clientPortalProducts = new ActiveDataProvider([
            'query' => ClientPortalProducts::find()->where(['portalId'=>$id]),
        ]);*/

        $query = \common\models\ClientPortalProducts::find();
        $products = $query
                    ->from('ClientPortalProducts')
                    ->join('JOIN',
                        'Products p',
                        'ClientPortalProducts.productId = p.id'
                    )
                    ->where("p.status = '1' AND ClientPortalProducts.portalId=$id");

        $clientPortalProducts = new ActiveDataProvider([
            'query' => $query,
        ]);            

        $clientPortal = $this->findModel($id);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $store = Stores::findOne(Yii::$app->user->identity->store->id);
        elseif($user->roleId==1)
            $store = Stores::findOne($clientPortal->storeId);
        return $this->render('view', compact('clientPortal','clientPortalProducts','store'));
    }

    public function actionSendNotification($id){
        $portal = ClientPortal::find()->where(['id'=>$id])->one();
        $portal->sendCreationNotification();
        Yii::$app->session->setFlash('success', 'Quote email send Successfully !'); 
        return $this->redirect(['view','id'=>$id]);
    }

    public function actionReport() {   

    	$content = "Test Content";
        
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Krajee Report Header'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        /*$response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_RAW;
            $headers = Yii::$app->response->headers;
            $headers->add('Content-Type', 'application/pdf');*/
        
        // return the pdf output as per the destination setting
        $res =  $pdf->render(); 
    }
        
    public function actionGetproducts(){
        $keyword = $_REQUEST['keyword'];
        $storeId = Yii::$app->params['storeId'];
        $data = [];
        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    ->from('Products as p')
                    ->join('JOIN',
                        'AttributeValues as av',
                        'av.productId = p.id'
                    )
                    ->join('JOIN',
                        'Attributes as a',
                        'a.id = av.attributeId'
                    )
                    ->join('JOIN',
                        'StoreProducts as sp',
                        'p.id = sp.productId'
                    )
                    ->groupBy('p.id')
                    ->where("(p.sku = '".$keyword."' OR av.value like '%".$keyword."%') AND a.code='name' AND sp.storeId='".$storeId."' AND p.status = 1 AND sp.enabled = 1");
                    
                    if(empty($keyword))
                        $products->andWhere("1!=1");

                    $products->all();

        foreach($products->all() as $product)
            $data[$product->id] = ['id'=>$product->id,'name'=>$product->name, 'sku'=>$product->sku, 'price'=>$product->price];
        return json_encode($data);      
    }

    public function actionOrders()
    {
        $searchModel = new OrdersSearch();
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $dataProvider->query->andWhere('type = "client-portal" and storeId = '.$user->store->id.'')->orderBy(['id'=>SORT_DESC,]);
        else
            $dataProvider->query->andWhere(['type' => 'client-portal'])->orderBy(['id'=>SORT_DESC,]);
        
        return $this->render('orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrderDetail($id){  
        
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId' => $id]),
        ]);
        $searchModel_invoices=new InvoicesSearch();
        $dataProvider_invoices = new ActiveDataProvider([
            'query' => Invoices::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);

        //$searchModel_ordercomments = new OrderCommentSearch();
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'order','orderId' => $id]),
        ]);
        $dataProvider_salescomments_all = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['orderId' => $id]),
        ]);
        
        $dataProvider_creditmemos = new ActiveDataProvider([
            'query' => CreditMemos::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_creditmemos=new CreditMemosSearch();

        $dataProvider_shipment = new ActiveDataProvider([
            'query' => Shipments::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_shipment=new ShipmentsSearch();

         $dataProvider_deliveries = new ActiveDataProvider([
            'query' => Deliveries::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_deliveries=new DeliveriesSearch();

         return $this->render('order-detail', [
            'model' => Orders::findOne($id),
            'searchModel_invoices' => $searchModel_invoices,
            'dataProvider_invoices'=>$dataProvider_invoices,
            
            'dataProvider_salescomments'=>$dataProvider_salescomments,
            'dataProvider_salescomments_all'=>$dataProvider_salescomments_all,

            'searchModel_creditmemos' => $searchModel_creditmemos,
            'dataProvider_creditmemos'=>$dataProvider_creditmemos,

            'searchModel_shipment' => $searchModel_shipment,
            'dataProvider_shipment'=>$dataProvider_shipment,

            'searchModel_deliveries' => $searchModel_deliveries,
            'dataProvider_deliveries'=>$dataProvider_deliveries,
            
            'dataProvider' => $dataProvider,
        ]);
    }


    protected function findModel($id)
    {
        if (($clientPortal = ClientPortal::findOne($id)) !== null) {
            return $clientPortal;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*public function actionTestssssss(){
        $portal = ClientPortal::find()->where(['id'=>3])->one();
        $portal->sendCreationNotification();
    }*/
}
?>    