<?php

namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\GroupAttributes;
use common\models\AttributeGroups;
use common\models\AttributeSets;
use common\models\Attributes;
use common\models\search\AttributesSearch;

/**
 * AttributeSetsController implements the CRUD actions for AttributeSets model.
 */
class AttributeSetsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AttributeSets models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AttributeSets::find(),
        ]);
        $searchModel = new AttributesSearch;
        //$searchModel->search(Yii::$app->request->post());
        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    /**
     * Displays a single AttributeSets model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AttributeSets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttributeSets();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $basedOn_attributeSets=$_POST['AttributeSets']['basedOnSet'];
            $basedOn_attributeSets_array=AttributeGroups::find()->where( [ 'attributeSetId' => $basedOn_attributeSets ] )->all();
            //echo $model->id;
            $pos_att_group=0;
            foreach ($basedOn_attributeSets_array as $k=>$value) {
                //print_r($value);
                $atr_groups_id=$value['id'];
                $model_att_group= new AttributeGroups();
                $model_att_group->title=$value['title'];
                $model_att_group->attributeSetId=$model->id;
                $model_att_group->position=$pos_att_group;
                $model_att_group->save();
                $pos_att_group++;
                $groupattributes_array=GroupAttributes::find()->where( [ 'attributeGroupId' => $atr_groups_id ] )->all();
                $pos_group_attr=0;
                foreach ($groupattributes_array as $k=>$value1) {
                    
                    $model_group_attr= new GroupAttributes();
                    $model_group_attr->attributeId=$value1['attributeId'];
                    $model_group_attr->attributeGroupId=$model_att_group->id;
                    $model_group_attr->position=$pos_group_attr;
                    $model_group_attr->save();
                    $pos_group_attr++;
                }
            }

        }
        if(\Yii::$app->request->isAjax){
            return $this->renderPartial('_form',compact('model'));
        }
        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Updates an existing AttributeSets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            echo "Attribute Set updated successfully!";
        } else {
            
             //echo 'hai';exit;
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }
     public function actionUpdategroupattributes()//------------------------aaron
    {

        //echo 1;exit;
        //$attributsets_id= $_REQUEST['id'];exit;
        $ass_attr_JSON=$_POST["order1"];
        $unass_attr_JSON=$_POST["order2"];
        $atr_set_id=$_POST["atr_set_id"];
       //echo  $someJSON;exit;
        //var_dump($_POST["order1"]);exit;
        $ass_attr_array = json_decode($ass_attr_JSON, true);

        $pos_group_attr=0;
        foreach ($ass_attr_array as $index => $group_attr_list) 
        { 
        $model_group = AttributeGroups::findOne($group_attr_list['id']); 
        //echo  $group_attr_list['id'];exit;          
        $model_group->position = $pos_group_attr;
        $model_group->save();
        $pos_group_attr++;
        
        $attri_no=GroupAttributes::find()->where( [ 'attributeGroupId' =>$group_attr_list['id']])->all();
        //print_r($attri_no);
        //echo $glist['id'];echo '</br>';
            $pos_attr=0;
            if(!isset($group_attr_list['children']))
            {
              
               GroupAttributes::deleteAll(['attributeGroupId' => $group_attr_list['id']]);
            }
            else
            {

            

            foreach ($group_attr_list['children'] as $index => $attr_list) 
            { 
                
                $a_list=$attr_list['id'];
                $g_list=$group_attr_list['id'];
                //-------------------------------------------delete moving item------------------
                $c_array=$group_attr_list['children'];
                $dbarray_new;
                $carray_new;
                $dbarray=GroupAttributes::find()->where( [ 'attributeGroupId' =>$group_attr_list['id']])->all();
                foreach ($dbarray as $index => $a) 
                { 
                    $dbarray_new[] =$a['attributeId'];
                }
                foreach ($c_array as $index => $b) 
                { 
                    $carray_new[] =$b['id'];
                }
                if(isset($dbarray_new))
                {   
                    $result=array_diff($dbarray_new,$carray_new);
                    //var_dump($result);exit;
                    if (!empty($result)) {                
                        foreach ($result as $index => $c) 
                        {                     
                            $c_p=GroupAttributes::find()->where( [ 'attributeId' => $c, 'attributeGroupId' =>$group_attr_list['id']])->one();
                            $tmodel = GroupAttributes::find()->where( [ 'id' => $c_p['id'] ] )->one();
                            $tmodel->delete();
                        }
                    }
                    $dbarray_new=array();
                    $carray_new=array();
                }
                //-------------------------------------------------delete moving item end-----------------------
                //var_dump($g_list);exit;
                $attri_p=GroupAttributes::find()->where( [ 'attributeId' => $a_list, 'attributeGroupId' =>$g_list])->one();
                if($attri_p)
                {
                    
                    $cust = GroupAttributes::findOne($attri_p['id']);            
                    $cust->position = $pos_attr;
                    //echo $attri_p['id'];exit;
                    $cust->save();
                    $pos_attr++;
                  //echo "yes";echo $pos_attr++;  echo " att ".$attri_update;
                }
                else
                {                    
                    $model1 = new GroupAttributes();
                    $model1->attributeGroupId=$group_attr_list['id'];
                    $model1->attributeId=$attr_list['id'];                    
                    $model1->position= $pos_attr;
                    $model1->save();                   
                    $pos_attr++;            
               }
            }
            }
        }
        $unass_attr_array = json_decode($unass_attr_JSON, true);
        
        foreach ($unass_attr_array as $index => $unass_attr_list) 
        { 
            //var_dump($unass_attr_list);exit;
            $unass_attr_array_list[]=$unass_attr_list['id'];
        //echo '</br>';echo $unass_attr_list['id'];echo '</br>';           
            
        }
        //print_r($unass_attr_array_list);
        
        return $this->redirect(['view', 'id' => $atr_set_id]);
    }
    public function actionDeleteattributeset()
    {
        //echo 1;exit;
        $atr_set_id=$_POST["atr_set_id"];
        $attributeGroupId=$_POST["atr_group_id"];//exit;
        $gro_atr_array=GroupAttributes::find()->where( [ 'attributeGroupId' => $attributeGroupId ] )->all();
        
        foreach ($gro_atr_array as $k=>$value) {
            //print_r($value);
            $id=$value['id'];
            $user = GroupAttributes::find()->where( [ 'id' => $id ] )->one();
            $user->delete();
            
        }
        //echo  $attributeGroupId;exit;
         if($attributeGroupId!='')
        {
            $aatrdelete=AttributeGroups::find()->where( [ 'id' => $attributeGroupId ])->one();
            $aatrdelete->delete();
        }
        return $this->redirect(['view', 'id' => $atr_set_id]);

    }
    public function actionCreateattributegroup()
    {
        $name=$_POST["person"]; 
        $atr_set_id=$_POST["atr_set_id"]; 
        if($name!='')
        {
            $model_asc = new AttributeGroups;
            $model_asc->title = $name;
            $model_asc->attributeSetId = $atr_set_id;
            $atr_group_count = AttributeGroups::find()->where(['attributeSetId' => $atr_set_id])->count(); 
            $model_asc->position = $atr_group_count-1;
            $model_asc->save();  
        }

        return $this->redirect(['view', 'id' => $atr_set_id]);
        
    }

    /**
     * Deletes an existing AttributeSets model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AttributeSets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttributeSets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttributeSets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    //return $refresh;
    

}
