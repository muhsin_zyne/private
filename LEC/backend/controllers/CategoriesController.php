<?php

namespace backend\controllers;

use Yii;
use common\models\Categories;
use common\models\ProductCategories;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
/**
 * CategoriesController implements the CRUD actions for Categories model.
 */


class CategoriesController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductCategories::find(),
        ]);

        $model = new Categories();
        return $this->render('index', compact('model','dataProvider'));
    }
    
    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionCreate()
    {   

        $model = new Categories();  
        $dataProvider = new ActiveDataProvider([
            'query' => ProductCategories::find(),
        ]);
        if ($model->load(Yii::$app->request->post())){  //var_dump($_POST);die();
            if(\Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($model);
            }
	    if($model->save(false)) {   //die('saved');
                return $this->redirect('index',compact('model','dataProvider'));
            }    
        } 
        else {  
            if(\Yii::$app->request->isAjax)
                return $this->renderAjax('create', compact('model','dataProvider'));
            else
                return $this->render('create', compact('model','dataProvider'));
        }
    }

    public function actionChangeorder(){
        $treeOrder = json_decode($_POST ["treeOrder"],true);

        foreach ($treeOrder as $key => $tree) {
            if(!$category = Categories::find()->where(['parent'=>'0','id'=>$tree['id']])->one()){ 
                $category = $this->findModel($tree['id']);
                $category->parent = '0';
                $category->position = $key+1;
                $category->save(false);
            }
            else{   
                $category->position = $key+1;
                $category->save(false);
            }

            if(isset($tree['children'])){
                foreach ($tree['children'] as $childpos => $children) {
                    if(!$category = Categories::find()->where(['parent'=>$tree['id'],'id'=>$children['id']])->one()){
                        $category = $this->findModel($children['id']);
                        $category->parent = $tree['id'];
                        $category->position = $childpos+1;
                        $category->save(false);
                    }
                    else{
                        $category->position = $childpos+1;
                        $category->save(false);
                    }
                }
            }
        }
    }

    public function actionGeneral()
    {
        $model = new Categories();
        echo "hai";exit;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('general', [
                'model' => $model,
            ]);
        }
    }

    public function actionDisplaySettings()
    {
        $model = new Categories();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('display_settings', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($selected)
    {
        $model = $this->findModel($selected);
        $dataProvider = new ActiveDataProvider([
            'query' => ProductCategories::find(),
        ]);

        if ($model->load(Yii::$app->request->post())){   //var_dump($_POST);die();
	    if(\Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($model);
            }
            if($model->save(false)){
                return $this->redirect('index',compact('model','dataProvider'));
            }
        }
        else{
            if(\Yii::$app->request->isAjax)
                return $this->renderAjax('update', compact('model','dataProvider'));
            else
                return $this->render('update', compact('model','dataProvider'));
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($selected)
    {
        // $this->findModel($id)->delete();
        // return $this->redirect(['index']);
        $this->findModel($selected)->delete();
        if($subCategories = Categories::find()->where(['parent'=>$selected])->all()){
            foreach ($subCategories as $category) {
                $this->findModel($category['id'])->delete();
            }
        }
        return $this->redirect(['index']);
    }

    public function actionSearch($keyword)
    {
        $categories = Categories::find()->where('title like "%'.$keyword.'%"')->all();
        $cat_ids = Array();
        foreach ($categories as $category) {
            $cat_ids['id'][] = $category->id;
          
        }
        //var_dump($cat_ids);die();
        return json_encode($cat_ids);
    }    

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

   public function actionTest(){
    	echo \yii\helpers\Url::to(['/treemanager/node/manage']);
    }


}
