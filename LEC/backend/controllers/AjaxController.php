<?php



namespace backend\controllers;



use Yii;

use common\models\Products;

use common\models\AttributeGroups;

use common\models\ProductCategories;

use common\models\ProductSuperAttributes;

use common\models\ProductLinkages;

use common\models\AttributeOptionPrices;

use app\components\AdminController;

use yii\grid\GridView;

use yii\data\ActiveDataProvider;

use yii\helpers\Html;

use yii\helpers\ArrayHelper;





class AjaxController extends AdminController

{

	public function actionGetbundleitems(){

		

		$grid = GridView::widget([

	    	//'filterModel' => $search,

	        'dataProvider' => new ActiveDataProvider([

            	'query' => Products::find()->where('typeId = "simple"'), 

            ]),	//$search->search(Yii::$app->request->queryParams);

            'columns' => array_merge([

							        	[

							        		'class' => 'yii\grid\CheckboxColumn',

							        		'contentOptions' => ['class' => 'check'],

							        		'checkboxOptions' => function($model, $key, $index, $column) {

																    return ['data-name' => $model->name, 'class' => 'bundlePdtId', 'data-sku' => $model->sku, 'value' => $model->id];

																}

							        	],	

							        	'name',

							        	'typeId',

							        	'sku',

										'price',



        ]),

        ]);    

		\yii\widgets\Pjax::begin(['id' => 'product-sets']); 

		echo $grid;

	    \yii\widgets\Pjax::end();

	}

}	





?>