<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

use common\models\Orders;
use common\models\OrderItems;
use common\models\Invoices;
use common\models\OrderComment;
use common\models\CreditMemos;
use common\models\Shipments;
use common\models\SalesComments;
use common\models\EmailQueue;
use common\models\User;
use common\models\Deliveries;
use common\models\search\DeliveriesSearch;


use app\components\AdminController;
use common\models\search\OrdersSearch;
use common\models\search\ShipmentsSearch;
use common\models\search\InvoicesSearch;
use common\models\search\CreditMemosSearch;
//use common\models\search\OrderCommentSearch;

use bryglen\braintree\braintree;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends AdminController
{
    

    private $_prefix = 'Braintree';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex(){
        $searchModel = new OrdersSearch();       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3){        
          $dataProvider->query->andWhere(['type' => 'b2c','storeId'=>$user->store->id])->andFilterWhere( ['!=', 'status', 'payment-pending'])->orderBy(['id'=>SORT_DESC,]);
          //$dataProvider->query->andWhere('(type = "b2c" or type = "byod" or type="client-portal") and storeId = '.$user->store->id.'')->orderBy(['id'=>SORT_DESC,]);
        } else {
          $dataProvider->query->andWhere(['type' => 'b2c'])->andFilterWhere( ['!=', 'status', 'failed'])->orderBy(['id'=>SORT_DESC,]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionFailedTransactions(){
        $searchModel = new OrdersSearch();       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3){        
          $dataProvider->query->andWhere(['type' => 'b2c','storeId'=>$user->store->id,'status'=>'failed'])->orderBy(['id'=>SORT_DESC,]);
        } else {
          $dataProvider->query->andWhere(['type' => 'b2c','status'=>'failed'])->orderBy(['id'=>SORT_DESC,]);
        }
        return $this->render('failed-orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionUpdate($id,$type=NULL){
        $model = $this->findModel($id); 
        $orderItems= OrderItems::find()->select('id')->where(['orderId'=>$id])->all(); 
        if (Yii::$app->request->post()) {
            //var_dump($_POST);die;
            $entity = [ ];
            foreach ($_POST['OrderItems'] as $key => $orderItem) { // order items upadte  Qty Ready For Delivery
                $model_orderItems=OrderItems::findOne($key);
                $model_orderItems->qtyReadyForDelivery=$model_orderItems->qtyReadyForDelivery+$orderItem['qrfd'];
                $model_orderItems->save(false);
                $orderItemEntity[$key]['name']=$model_orderItems->product->name;
                $orderItemEntity[$key]['qty']=$orderItem['qrfd'];
                $entity= $entity+$orderItemEntity;                
            }
            //var_dump($entity);die;
            $model->addActivity('ready-for-collection', $entity);
            $model->status=$model->ordersStatus;
            $model->save(false);          
            if($_POST['SalesComments']['readyComment']!=''){ 
                $model_salesComments = new SalesComments();
                $model_salesComments->type='order';
                $model_salesComments->orderId=$id;
                $model_salesComments->comment=$_POST['SalesComments']['readyComment']; 
                $model_salesComments->save(false); 
                if(isset($_POST['SalesComments']['notify']) && $_POST['SalesComments']['notify']=='true'){
                    $model_salesComments->notify=1;
                    $model_salesComments->sendOrderStatusUpdateMail();
                    $model_salesComments->save(false); 
                }
                //$model->addActivity('message-sent', $model_salesComments);
            } else {  
                if(isset($_POST['SalesComments']['notify']) && $_POST['SalesComments']['notify']=='true'){
                    $model_sc = new SalesComments();              
                    $model_sc->sendOrderStatusUpdateMail($model->id);
                }
            } 
            echo "<script> location.reload(); </script>";
            die('Order Status Updated Successfully!');  
        }
        
        if(\Yii::$app->request->isAjax){ 
            //var_dump($_REQUEST); die;
            $oi_array[]=0;$oi_qrfd[0]=0;
            foreach ($orderItems as $orderItem) {
                $oi_id=$orderItem['id'];
                if(isset($_REQUEST['chk_'.$oi_id])){
                    //echo $_REQUEST['chk_'.$oi_id];
                    if($_REQUEST['qrfd_'.$oi_id]!=0){
                        $oi_array[]=$oi_id;
                        $oi_qrfd[$oi_id]=$_REQUEST['qrfd_'.$oi_id];
                    }
                }                  
            } 
            if($type=='fc'){
                $dataProvider = new ActiveDataProvider([ 
                    'query' => OrderItems::find()->where(['orderId' => $id])
                        ->andWhere(['id' =>$model->getRemainingOrderItemIds()]),
                    ]);
            } else {
                $dataProvider = new ActiveDataProvider([
                'query' => OrderItems::find()->where(['orderId' => $id])->andWhere(['id' =>$oi_array]),
            ]); 
            }            
            return $this->renderAjax('ready-for-collection',compact('model','dataProvider','oi_qrfd','type'));
        }
        //return $this->renderPartial('update', [ 'model' => $model, ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);

        //$orderItems=OrderItems::find()->where(['orderId' => $id])->all();           
        $dataProvider = new ActiveDataProvider([          
            'query' => OrderItems::find()->where(['orderId' => $id])->andWhere(['id' =>$model->getRemainingOrderItemIds()]),
        ]);
        $searchModel_invoices=new InvoicesSearch();
        $dataProvider_invoices = new ActiveDataProvider([
            'query' => Invoices::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);

        //$searchModel_ordercomments = new OrderCommentSearch();
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'order','orderId' => $id]),
        ]);
        $dataProvider_salescomments_all = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['orderId' => $id]),
        ]);
        
        $dataProvider_creditmemos = new ActiveDataProvider([
            'query' => CreditMemos::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_creditmemos=new CreditMemosSearch();

        $dataProvider_shipment = new ActiveDataProvider([
            'query' => Shipments::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_shipment=new ShipmentsSearch();

        $dataProvider_deliveries = new ActiveDataProvider([
            'query' => Deliveries::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);

        $deliveries = Deliveries::find()->where(['orderId'=>$id])->all();
        $invoice = Invoices::find()->where(['orderId' => $id])->one();

        $searchModel_deliveries=new DeliveriesSearch();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel_invoices' => $searchModel_invoices,
            'dataProvider_invoices'=>$dataProvider_invoices,
            
            'dataProvider_salescomments'=>$dataProvider_salescomments,
            'dataProvider_salescomments_all'=>$dataProvider_salescomments_all,

            'searchModel_creditmemos' => $searchModel_creditmemos,
            'dataProvider_creditmemos'=>$dataProvider_creditmemos,

            'searchModel_shipment' => $searchModel_shipment,
            'dataProvider_shipment'=>$dataProvider_shipment,

            'searchModel_deliveries' => $searchModel_deliveries,
            'dataProvider_deliveries'=>$dataProvider_deliveries,
            
            'dataProvider' => $dataProvider,
            'deliveries' => $deliveries,
            'invoice' => $invoice,
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/
    public function actionChangestatus()
    {
       
      $model = $this->findModel($_POST['order_id']);
      //var_dump($_POST['order_id']);die;
      $model->status=$_POST['status'];

      if($model->save(false))
      {
        Yii::$app->getSession()->setFlash('success', 'Status Changed successfully.');
        return $this->redirect(['view', 'id' => $model->id]);  
      }
      
    }

    public function actionSendmail($id)
    {
        $model=$this->findModel($id);
        $model->sendOrderConfirmation();
        $model->addActivity('email-resent', $model);
        Yii::$app->getSession()->setFlash('success', 'The order email has been sent.');
        return $this->redirect(['view', 'id' => $model->id]);

    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->status='cancelled';
        $model->save(false);
        //var_dump($this->findModel($id));die;
        Yii::$app->getSession()->setFlash('success', 'The order has been cancelled.');
        return $this->redirect(['view', 'id' => $model->id]);
    }
    
    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionTest($id)
    {
         //$id=329;
         $model=$this->findModel($id);
         $model->sendB2bOrderConfirmation();
         //$model->sendSupplierNotification();
        // $model->invoicesCreate();
        // $model->cancelUserOrderMail();
         //$EmailQueue = EmailQueue::find()->where(['id' => '669'])->one();
         //echo $EmailQueue->message;exit;
    }

    public function actionInvoice($id){    
        $model=$this->findModel($id);
        $model->invoicesCreate();
        Yii::$app->getSession()->setFlash('success', 'Invoice Created successfully.');
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionOrder(){
      return false;
      $user = User::findOne(Yii::$app->user->id);
      if(\Yii::$app->request->isAjax){
          $orderItem = OrderItems::find()->where(['id'=>$_REQUEST['orderItemId']])->one();
          if (isset($_POST['OrderItems'])) {
              $model = OrderItems::findOne($_POST['OrderItems']['orderItemId']);
              $model->orderFromSupplier();
              die('Order email sent successfully');
          }
          else          
            return $this->renderAjax('_orderProduct',compact('orderItem'));  
      }
      
      $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->join('JOIN','Orders as o','o.id = OrderItems.orderId')->where('o.type = "b2c" and o.storeId = '.$user->store->id.'')->orderBy(['o.id'=>SORT_DESC]),
        ]);
      return $this->render('order', ['dataProvider' => $dataProvider,
        ]);
    }


    public function actionRefund($id)
    {
      $order = $this->findModel($id);
      $braintree = Yii::$app->braintree;
      $method = "refund";
      
      $class = strtr("{class}_{command}",array(
                '{class}' => $this->_prefix,
                '{command}' => 'Transaction',
            ));

      call_user_func(array($class, $method), $order->transactionId,10);

       $searchModel = new OrdersSearch();
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
        {
          $dataProvider->query->andWhere(['type' => 'b2c','storeId'=>$user->store->id])->orderBy(['id'=>SORT_DESC,]);
        }
        else
        {
          $dataProvider->query->andWhere(['type' => 'b2c'])->orderBy(['id'=>SORT_DESC,]);
        }
        return $this->redirect('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
      
      Yii::$app->getSession()->setFlash('success', 'Refund processed successfully.');
    }
    
    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionGiftregistryOrders() {

        //var_dump(Yii::$app->request->queryParams);die();

        $searchModel = new OrdersSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->join('JOIN', 'GiftRegistryProducts as gp', 'gp.order_id = Orders.id');
        $user = User::findOne(Yii::$app->user->id);
        if ($user->roleId == 3) {
            $dataProvider->query->andWhere(['type' => 'b2c', 'storeId' => $user->store->id, 'gp.purchased' => 1])->orderBy(['id' => SORT_DESC,]);
        } else {
            $dataProvider->query->andWhere(['type' => 'b2c', 'gp.purchased' => 1])->orderBy(['id' => SORT_DESC,]);
        }
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    public function actionTestMail($id) {
    $salescomments=SalesComments::find()->where(['id' => $id])->one();
    $salescomments->sendOrderStatusUpdateMail();
    }
    

   
}
