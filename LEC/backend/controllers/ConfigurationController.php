<?php

namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\data\ActiveDataProvider;
use backend\models\ConfigurationForm;
use common\models\Configuration;
/**
 * SalesCommentsController implements the CRUD actions for SalesComments model.
 */
class ConfigurationController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SalesComments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $config = new ConfigurationForm;
        if(Yii::$app->user->identity->roleId == "3")
            $settings = Configuration::find()->where(['storeId' => Yii::$app->user->identity->store->id])->all();
        else
            $settings = Configuration::find()->where(['storeId' => (isset(Yii::$app->session['storeId'])? Yii::$app->session['storeId'] : 0)])->all();
        foreach($settings as $setting){
            $config->{$setting->key} = $setting->value;
        }
        return $this->render('index', compact('config'));
    }

    /**
     * Displays a single SalesComments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing SalesComments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        if(isset($_POST['ConfigurationForm'])){  //var_dump($_POST['ConfigurationForm']['storeId']);die();
            $session = Yii::$app->session;
            Yii::$app->session['storeId'] = $_POST['ConfigurationForm']['storeId'];
            $storeId = (Yii::$app->user->identity->roleId == "3")? Yii::$app->user->identity->store->id : (isset(Yii::$app->session['storeId'])? Yii::$app->session['storeId'] : 0);
            foreach($_POST['ConfigurationForm'] as $key => $value){
                Configuration::setConfig($key, $value, $storeId);
            }
            Yii::$app->session->setFlash('success', 'Settings saved successfully');
        }
        $this->redirect(\yii\helpers\Url::to(['configuration/index']));
    }

    /**
     * Finds the SalesComments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SalesComments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SalesComments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
