<?php

namespace backend\controllers;

use Yii;
use common\models\Conferences;
use common\models\Products;
use common\models\ConferenceTrays;
use common\models\ConferenceTrayProducts;
use common\models\search\ConferencesSearch;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;
//use common\models\search\OrderItems;

/**
 * ConferencesController implements the CRUD actions for Conferences model.
 */
class ConferencesController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Conferences models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConferencesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conferences model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'conference' => $this->findModel($id),
        ]);
    }

    public function actionList()
    {
        $searchModel = new ConferencesSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Conferences model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $conference = new Conferences();
        

        if ($conference->load(Yii::$app->request->post())) {

           // var_dump(Yii::$app->request->post());die();

            $conference->fromDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["Conferences"]['fromDate']));
            $conference->toDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["Conferences"]['toDate']));
            $conference->status = Yii::$app->request->post()["Conferences"]['status'];
            if ($conference->save(false)) { //var_dump($conference->id);die();
                foreach ($_POST['Products']['trays'] as  $trays) {   //var_dump($trays['trayProduct']);die();
                    $tray = new ConferenceTrays();
                    $tray->title = $trays['title'];
                    //$tray->image = $trays['image'];
                    $tray->trayId = $trays['trayId'];
                    $tray->conferenceId = $conference->id;
                    if($tray->save()){
                        if(isset($trays['trayProduct'])){
                            foreach ($trays['trayProduct'] as  $id => $item) {
                                $trayProducts = new ConferenceTrayProducts;
                                $trayProducts->conferenceTrayId  = $tray->id;
                                $trayProducts->productId  = $id;
                                $trayProducts->offerCostPrice  = $item["offerCostPrice"];    
                                $trayProducts->save(false);
                            }
                        }    
                    }
                }   
            }
            return $this->redirect(['view', 'id' => $conference->id]);
        }      
       
        else {
            return $this->render('create', [
                'conference' => $conference,
            ]);
        }


    }

    /**
     * Updates an existing Conferences model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $conference = $this->findModel($id);
        $conferenceTrays = $conference->conferenceTrays;
        if ($conference->load(Yii::$app->request->post())){
            $conference->fromDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["Conferences"]['fromDate']));
            $conference->toDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["Conferences"]['toDate']));
            if ($conference->save()) {
                if (isset($_POST['Products']['deletedProducts'])) {
                    ConferenceTrayProducts::deleteAll('id in ('.implode($_POST['Products']['deletedProducts']).')');
                }
                if (isset($_POST['Products']['deletedTrays'])) {
                    ConferenceTrays::deleteAll('id in ('.implode($_POST['Products']['deletedTrays']).')');
                    ConferenceTrayProducts::deleteAll('conferenceTrayId in ('.implode($_POST['Products']['deletedTrays']).')');
                }

                return $this->redirect(['view', 'id' => $conference->id]);
            } 
        }    
        else {

                return $this->render('update',compact('conference','conferenceTrays'));
            }
    }

    /**
     * Deletes an existing Conferences model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionOrders($id){
        $conference = Conferences::findOne($id);

        //var_dump($conference);die();

        $orderItems = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['conferenceId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        return $this->render('orders', compact('orderItems', 'conference'));
    }

    /**
     * Finds the Conferences model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Conferences the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($conference = Conferences::findOne($id)) !== null) {
            return $conference;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
