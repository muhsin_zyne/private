<?php

namespace backend\controllers;

use Yii;
use common\models\Deliveries;
use common\models\DeliveryItems;
use common\models\search\DeliveriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;
use common\models\User;
use common\models\Orders;
use yii\helpers\ArrayHelper;
use common\models\SalesComments;
use kartik\mpdf\Pdf;
use yii\web\UploadedFile;

/**
 * DeliveriesController implements the CRUD actions for Deliveries model.
 */
class DeliveryController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Deliveries models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliveriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == "3") 
        {
            $orders = Orders::findAll(['type'=>'b2c','storeId'=>$user->store->id]);
            $store_order = implode(",", ArrayHelper::map($orders, 'id', 'id'));
            $dataProvider->query->andWhere("Deliveries.orderid in (".(($store_order=="")? "0" : $store_order).")");
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Deliveries model.
     * @param integer $id
     * @return mixed
     */
   
    public function actionView($id)
    {       
        $dataProvider = new ActiveDataProvider([
            'query' => DeliveryItems::find()->where(['deliveryId' => $id]),
        ]);   
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'delivery']),
        ]);    
        return $this->render('view', [
            'model' => $this->findModel($id),           
            'dataProvider'=>$dataProvider,  
            'dataProvider_salescomments'=>$dataProvider_salescomments,         
        ]);
    }

    /**
     * Creates a new Deliveries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($orderid)
    {
        $model = new Deliveries();
        $model->orderId=$orderid;
        $order=Orders::findOne($orderid);        
        $itemsToShow = [];
	    $items = OrderItems::find()->where(['orderId' => $orderid])->all();
	    foreach($items as $item)
	        if($item->qtyToDelivered)
		       $itemsToShow[] = $item->id;
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['id' => $itemsToShow]),
        ]);
        
        if ($model->load(Yii::$app->request->post())) {
            //var_dump(Yii::$app->request->post());die;        
            $model->deliveryDate= date("Y-m-d",strtotime($_POST['deliveryDate']));
            $model->idProofNumber=$_POST['Deliveries']['idProofNumber'];
            $model->verifiedBy=$_POST['Deliveries']['verifiedBy'];
            $model->comments=$_POST['Deliveries']['comments'];
            $model->collectedBy=$_POST['Deliveries']['collectedBy'];
            $model->phone=$_POST['Deliveries']['phone'];
            if($order->type == "byod"){
	            if(isset($_POST['Deliveries']['amount']))
	            	$model->amount=$_POST['Deliveries']['amount'];
	            if(isset($_POST['Deliveries']['paymentType']))
	            	$model->paymentType=$_POST['Deliveries']['paymentType'];	
	        }    	           
            if ($model->save()){                            
                foreach ($_POST['DeliveryItems']['orderItemId'] as  $value){ 
                    $model_di= new DeliveryItems();
                    $delivery_order_item=explode(',', $value);
                    $model_di->deliveryId = $model->id;
                    $model_di->orderItemId =$delivery_order_item[0];
                    $model_di->quantityDelivered =$delivery_order_item[1];
                    if($delivery_order_item[1]!=0){                    
                       $model_di->save();
                    }                  
                }
                $model->order->status=$model->order->ordersStatus;
                $model->order->save(false);
                $model->sendDeliveryMail();    
                if(isset($_POST['Delivery']['chkmail'])) {// email send and status change to notify                
                    $model->notify=1;
                    $model->save(false);                    
                }
            }
            $order->addActivity('collected', $model);
            Yii::$app->getSession()->setFlash('success', 'Collection Advice Generated Successfully! See the Collection Advice Tab');
            if($model->order->type == "byod")
            	return $this->redirect(['byod-orders/view', 'id' => $model->orderId]);
            else
            	return $this->redirect(['orders/view', 'id' => $model->orderId]);	           
        } else {
            return $this->render('create', ['model' => $model,'dataProvider'=>$dataProvider,]);
        }
    }

    /**
     * Updates an existing Deliveries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ['model' => $model,]);
        }
    }

    /**
     * Deletes an existing Deliveries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionPdf($id) { 
        $delivery = $this->findModel($id);
        $content = Yii::$app->controller->renderPartial('_pdfContent',compact('delivery'));
        $filename = "Collection_".$delivery->id."_".$delivery->order->id.".pdf";
        $footer = "<div style='color:#5b5b5b;font-weight:normal;'>".$delivery->order->storeAddressFormatted."</div>";
        //$footer = "<div style='color:#5b5b5b;font-weight:normal;'>".$delivery->order->storeAddressFormatted."|Ph: ".$delivery->order->store->phone."|ABN: ".$delivery->order->store->abn."</div>";
        $header = "<div style='color:#5b5b5b;font-weight:normal;'>Instore Collection";
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'filename' => $filename, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            //'destination' => Pdf::DEST_FILE, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            //'cssFile' => '@backend/web/css/deliverypdf.css',
            // any css to be embedded if required
            'cssInline' => '', 
             // set mPDF properties on the fly
            'options' => ['title' => 'In-Store Collection'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>[$header], 
                'SetFooter'=>[$footer],
            ]
        ]);

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        
        // return the pdf output as per the destination setting
        return $pdf->render(); 


    }

    /**
     * Finds the Deliveries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deliveries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deliveries::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSendmail($id)
    {
        $model=$this->findModel($id);
        $model->sendDeliveryMail();
        $model->notify=1;
        $model->save();
        Yii::$app->getSession()->setFlash('success', 'The Delivery email has been sent.');
        return $this->redirect(['view', 'id' => $model->id]);

    }
}
