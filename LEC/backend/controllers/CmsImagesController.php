<?php

namespace backend\controllers;

use Yii;
use common\models\CmsImages;
use common\models\search\CmsImagesSearch;
use app\components\AdminController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\CmsPages;

/**
 * CmsImagesController implements the CRUD actions for CmsImages model.
 */
class CmsImagesController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsImages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CmsImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    public function actionView()
    {
         $ass_attr_JSON=$_POST["imageid"];
       echo $ass_attr_JSON;exit;

    }
   
    
    public function actionCreate()
     {
        //die('dsjkajshkda');
        $model = new CmsPages();
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        //Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/ckeditorimage/';
        //exit;
        $image = UploadedFile::getInstance($model, 'image');
        //echo $image;exit;
        $ext = end((explode(".", $image->name)));
        $avatar = Yii::$app->security->generateRandomString().".{$ext}";
        $path = Yii::$app->params['uploadPath'] . $avatar;
        $model_image = new CmsImages();
        $model_image->title = $avatar;
        $model_image->save();
        $image->saveAs($path);
        return true;
                       
    }
    public function actionGetimages()
    { 
        $html='<ol id="selectable">';
        $ckimage_all= CmsImages::find()->orderBy(['id' => SORT_DESC])->all();
        foreach ($ckimage_all as $index => $ckimage) 
        {
           //$html.='<tr> <td>'.$ckimage['title'].'</td> </tr>';
            $html.='<li class="ui-state-default" id="'.$ckimage['id'].'">
                <img src ="'.Yii::$app->params["rootUrl"].'/store/cms/images/'.$ckimage['title'].'" height="110" width="140">
            </li>';
  
              
        }                     
        $html.='</ol> ';   
        echo  $html; 
    }

   
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CmsImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->delete();
        $delete_path=Yii::$app->basePath."/../".$model['title'];
        if (file_exists($delete_path)) {
            unlink($delete_path);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the CmsImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsImages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
