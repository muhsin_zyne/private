<?php

namespace backend\controllers;

use Yii;
use common\models\CmsBlocks;
use common\models\search\BlockPagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\StoreCmsBlocks;
use common\models\User;
use yii\data\ActiveDataProvider;
use common\models\CategoryCmsBlocks;
use yii\helpers\ArrayHelper;
use common\models\Stores;
use common\models\StoreCategoryBlocks;
use common\models\Categories;
use common\models\CmsBlockImages;
use yii\web\UploadedFile;

/**
 * CategoryBlockController implements the CRUD actions for CmsBlocks model.
 */
class CategoryBlocksController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmsBlocks models.
     * @return mixed
     */

    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->id);
        $searchModel = new BlockPagesSearch(); 

        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
        $queryParams["BlockPagesSearch"]["type"] ='category_content';                       
        $dataProvider = $searchModel->search($queryParams); 
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CmsBlocks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CmsBlocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmsBlocks();
        $user = User::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) )
        {                      
            $model->type="category_content";
            if($user->roleId==1)
            {
                $stores_array=$_POST['CmsBlocks']['selected_store'];
            }
            elseif ($user->roleId==3) {
                $stores_array[] = Yii::$app->params['storeId'];
            }    
           
            $model->createdBy=Yii::$app->user->id;           
            if ($model->save(false))
            { 
                foreach ($stores_array as  $storeId) {
                    $storecmsblocks=StoreCmsBlocks::find()->where(['storeId'=>$storeId])->all();
                    //var_dump($storeblocks);die;
                    foreach ($storecmsblocks as $key => $storecmsblock) {
                        if(isset($storecmsblock->cmsblocks->type) && $storecmsblock->cmsblocks->type=="category_content"){
                            if($storecmsblock->cmsblocks->categoryId==$_POST['CmsBlocks']['categoryId']){
                                $storecmsblock->delete();
                            }
                        }                        
                    }
                    $model_sp=new StoreCmsBlocks();
                    $model_sp->storeId=$storeId;
                    $model_sp->blockId=$model->id;
                    $model_sp->save(false);                                       
                }
                Yii::$app->session->setFlash('success', 'Page created successfully');
                return $this->redirect(['index']);          
            }
            else{var_dump($model->getErrors());die;}
        }
        else {                  
            return $this->render('create', compact('model'));           
        }
    }

    /**
     * Updates an existing CmsBlocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $stores=Stores::find()->where(['isVirtual'=>0])->all();
        $categories=Categories::find()->all(); 
        if(isset($_POST['CmsBlocks']))  
        {
            $stores_array=$_POST['CmsBlocks']['selected_store'];            
            StoreCmsBlocks::deleteAll(['blockId' => $id]);
            if ($model->load(Yii::$app->request->post()) && $model->save())
            { 
                foreach ($stores_array as  $storeId) { 
                    $storecmsblocks=StoreCmsBlocks::find()->where(['storeId'=>$storeId])->all();                    
                    foreach ($storecmsblocks as $key => $storecmsblock) {
                        if(isset($storecmsblock->cmsblocks->type) && $storecmsblock->cmsblocks->type=="category_content"){
                            if($storecmsblock->cmsblocks->categoryId==$_POST['CmsBlocks']['categoryId']){
                                $storecmsblock->delete();
                            }
                        }                        
                    }
                    $model_sp=new StoreCmsBlocks();
                    $model_sp->storeId=$storeId;
                    $model_sp->blockId=$model->id;
                    $model_sp->save(false);
                }
                Yii::$app->session->setFlash('success', 'Page updated successfully');
                return $this->redirect(['index']);
            }
            else{ 
                return $this->render('update', compact('model')); 
            }
        }
        else 
        { return $this->render('update', compact('model'));  } 
    }

    /**
     * Deletes an existing CmsBlocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCategoryname()
    {
        //var_dump($_POST['id']);die;
        $categories=Categories::find()->where(['id'=>$_POST['id']])->one(); 
        if(isset($categories))        
            return $categories->title;
        else
            return null;
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CmsBlocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsBlocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsBlocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionImageBrowse()// ckeditor image browse 
    { 
        $storeId=(isset(Yii::$app->params['storeId']))?Yii::$app->params['storeId']:0;
        $cmsImages=CmsBlockImages::find()->where(['storeId'=>$storeId])->orderBy(['id' => SORT_DESC])->all();        
        return $this->renderPartial('image',compact('cmsImages'));
    }
    public function actionImageUpload()// ckeditor image upload
    {    
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        $uploadedFile = UploadedFile::getInstanceByName('upload'); 
        //$mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);
        $file = time()."_".$uploadedFile->name;
        $url=Yii::$app->params["rootUrl"]."/../store/cms/images/".$file; 
        $uploadPath = Yii::$app->params['uploadPath'].$file;        
        $check = getimagesize($uploadedFile->tempName);
        $storeId=(isset(Yii::$app->params['storeId']))?Yii::$app->params['storeId']:0;
        if ($uploadedFile==null)
        {
           $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
           $message = "The file is of zero length.";
        }
        else if($check['mime']!='image/jpeg' && $check['mime']!='image/jpg' && $check['mime']!='image/png'){
            $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
            $url='';
        }
        /*else if ($mime!="image/jpeg" && $mime!="image/png" && $mime!="image/jpg")
        {
           $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
        }*/
        else if ($uploadedFile->tempName==null)
        {
           $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
          $message = "";
            $model_image = new CmsBlockImages();
            $model_image->path = $file;
            $model_image->storeId=$storeId;
            $model_image->save(); 
          $move = $uploadedFile->saveAs($uploadPath);
          if(!$move)
          {
             $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
          } 
        }        
        $funcNum = $_GET['CKEditorFuncNum'] ;
        //var_dump($_GET['CKEditorFuncNum']);die;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";        
    }
}
