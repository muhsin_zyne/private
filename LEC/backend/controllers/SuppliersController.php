<?php

namespace backend\controllers;

use Yii;
use common\models\Suppliers;
use common\models\search\SupplierSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\Brands;
use yii\helpers\ArrayHelper;
use common\models\BrandSuppliers;

/**
 * SuppliersController implements the CRUD actions for Suppliers model.
 */
class SuppliersController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Suppliers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupplierSearch();
        $query = Suppliers::find();
        $adminUser = Yii::$app->user->identity;
        if($adminUser->roleId == "3")
            $query = $adminUser->store->getSuppliers();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $query);
        $dataProvider->query->andWhere("roleId = '2'"); //fetch suppliers only
        $adminUser = Yii::$app->user->identity;
        $enabledSuppliers = [];
        if($adminUser->roleId == "3") {
            $enabledSuppliers = \common\models\StoreSuppliers::findAll(['enabled'=>'1','type'=>'b2c','storeId'=>$adminUser->store->id]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'adminUser' => $adminUser,
            'enabledSuppliers' => $enabledSuppliers
        ]);
    }

    /**
     * Displays a single Suppliers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $brands = Brands::find()->all();
        $supplier = $this->findModel($id);
        if(isset($_POST['Brands'])){
            $_POST['Brands'] = is_array($_POST['Brands'])? $_POST['Brands'] : array();
            $previousBrands = ArrayHelper::map(BrandSuppliers::find()->where(['supplierUid' => $id])->all(), 'id', 'brandId');
            $removedBrands = array_diff(array_values($previousBrands), $_POST['Brands']);
            if(!empty($removedBrands)){
                BrandSuppliers::deleteAll("supplierUid='$id' and brandId in (".implode(",", $removedBrands).")");
                foreach($supplier->stores as $store)
                    foreach($removedBrands as $brand)
                        if(!$otherSupplier = BrandSuppliers::find()->where("brandId = '$brand' and supplierUid!='{$supplier->id}'")->one())
                            $store->unsetBrand($brand);
            }
            foreach($_POST['Brands'] as $brand){
                if(!BrandSuppliers::find()->where(['supplierUid' => $id, 'brandId' => $brand])->one()){
                    $brandSupplier = new BrandSuppliers;
                    $brandSupplier->supplierUid = $id;
                    $brandSupplier->brandId = $brand;
                    $brandSupplier->save(false);
                    if($brandSupplier->save(false)){
                        foreach($supplier->stores as $store){
                            $store->setBrand($brand);
                        }
                    }
                }
            }
        }
        return $this->render('view', compact('brands', 'supplier'));
    }

    /**
     * Creates a new Suppliers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Suppliers();

        if ($model->load(Yii::$app->request->post())) {
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            $model->roleId = '2';
            $model->status = '1';
            $model->save(false);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Suppliers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->password == "")
                $attributes = ['firstname', 'email', 'status'];
            else{
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                $attributes = ['firstname', 'email', 'password_hash', 'status'];
            }
            // var_dump($attributes); die;
            // var_dump($model->status); die;
            $justDisabled = false;
            if($model->oldAttributes['status'] == "1" && $model->status == "0")
                $justDisabled = true;
            $model->save(true, $attributes);
            if($justDisabled)
                $model->disableBrands();
            if(isset($_POST['Brands'])){
                $_POST['Brands'] = is_array($_POST['Brands'])? $_POST['Brands'] : array();
                $previousBrands = ArrayHelper::map(BrandSuppliers::find()->where(['supplierUid' => $id])->all(), 'id', 'brandId');
                $removedBrands = array_diff(array_values($previousBrands), $_POST['Brands']);
                if(!empty($removedBrands)){
                    BrandSuppliers::deleteAll("supplierUid='$id' and brandId in (".implode(",", $removedBrands).")");
                    foreach($model->stores as $store)
                        foreach($removedBrands as $brand)
                            if(!$otherSupplier = BrandSuppliers::find()->where("brandId = '$brand' and supplierUid!='{$model->id}'")->one())
                                $store->unsetBrand($brand);
                }
                foreach($_POST['Brands'] as $brand){
                    if(!BrandSuppliers::find()->where(['supplierUid' => $id, 'brandId' => $brand])->one()){
                        $brandSupplier = new BrandSuppliers;
                        $brandSupplier->supplierUid = $id;
                        $brandSupplier->brandId = $brand;
                        $brandSupplier->save(false);
                        if($brandSupplier->save(false)){
                            foreach($model->stores as $store){
                                $store->setBrand($brand);
                            }
                        }
                    }
                }
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Suppliers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        throw new \yii\web\NotFoundHttpException('Page Not Found');
        die;
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionEnable(){
        $user = \common\models\User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $_POST['currentEnabledSuppliers'] = isset($_POST['currentEnabledSuppliers'])? $_POST['currentEnabledSuppliers'] : [];
        $_POST['enabledSuppliers'] = isset($_POST['enabledSuppliers'])? $_POST['enabledSuppliers'] : [];
        $disabledSuppliers = array_diff(array_values($_POST['enabledSuppliers']), $_POST['currentEnabledSuppliers']);
        //var_dump($disabledProducts); die;
        //var_dump($_POST['currentEnabledSuppliers']); die;
        foreach ($_POST['currentEnabledSuppliers'] as $supplier) {
            if ($existingSupplier = \common\models\StoreSuppliers::find()->where(['storeId' => $storeId, 'supplierUid' => $supplier, 'type' => 'b2c', 'enabled'=>'0'])->one()) {
                $existingSupplier->enabled = "1";
                $existingSupplier->save(false);
                $existingSupplier->setBrands('b2c');
            }
        }    
        if(!empty($disabledSuppliers)){ 
            foreach ($disabledSuppliers as $supplier) { 
                $existingSupplier = \common\models\StoreSuppliers::find()->where(['storeId' => $storeId, 'supplierUid' => $supplier, 'type' => 'b2c'])->one();
                $existingSupplier->enabled = "0";
                $existingSupplier->save(false);
                $existingSupplier->unsetBrands('b2c');
            }   
        }    

        return true;
    }

    /**
     * Finds the Suppliers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Suppliers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Suppliers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}