<?php

namespace backend\controllers;
use yii\data\ActiveDataProvider;
use common\models\PriceRules;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use common\models\StorePriceRules;
use common\models\User;
use common\models\search\PriceRulesSearch;

class PriceRulesController extends \app\components\AdminController
{
    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->id);
        $today = date("Y:m:d H:i:s");

        $activeSearchModel = new PriceRulesSearch();
        $expiredSearchModel = new PriceRulesSearch();

        $queryParam1 = isset($_GET['PriceRulesSearch']) ? $_GET['PriceRulesSearch'] : "";
        $queryParam2 = isset($_GET['PriceRulesExpiredSearch']) ? $_GET['PriceRulesExpiredSearch'] : "";

        $activeDataProvider = $activeSearchModel->search(Yii::$app->request->queryParams);
        $expiredDataProvider = $expiredSearchModel->search(Yii::$app->request->queryParams);

        if($user->roleId == 3){
            $activeDataProvider->query->andWhere("toDate > '".$today."' and createdBy='".Yii::$app->user->id."'")->orderBy(['id'=>SORT_DESC]);
            $expiredDataProvider->query->andWhere("toDate < '".$today."' and createdBy='".Yii::$app->user->id."'")->orderBy(['id'=>SORT_DESC]);
        }
        elseif($user->roleId == 1){
            $activeDataProvider->query->andWhere("toDate > '".$today."'")->orderBy(['id'=>SORT_DESC]);
            $expiredDataProvider->query->andWhere("toDate < '".$today."'")->orderBy(['id'=>SORT_DESC]);
        }    

        return $this->render('index',compact('activeDataProvider','expiredDataProvider','activeSearchModel','expiredSearchModel'));
    }

    public function actionCreate(){
    	$priceRule = new PriceRules();
        if(isset($_POST['PriceRules'])){   //var_dump($_POST['PriceRules']);die();
            if($priceRule->load(Yii::$app->request->post())){ 

                $priceRule->fromDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["PriceRules"]['fromDate']));
                $priceRule->toDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["PriceRules"]['toDate']));

                $conditions = [['type' => 'combination', 'aggregator' => $_POST['PriceRules']['aggregator'], 'value' => 'true']];
                if(isset($_POST['PriceRules']['conditions'])){
                    foreach($_POST['PriceRules']['conditions'] as $i=>$condition){
                        $conditions[0]['conditions'][$i]['type'] = 'single';
                        $conditions[0]['conditions'][$i]['operator'] = $condition['operator'];
                        $conditions[0]['conditions'][$i]['attribute'] = strtolower($condition['attribute']);
                        $conditions[0]['conditions'][$i]['value'] = isset($condition['value'])? $condition['value'] : "";
                    }
                }
                if(Yii::$app->user->identity->roleId == "3")
                    $priceRule->createdBy = Yii::$app->user->id;
                $priceRule->conditions = serialize($conditions);
                if(Yii::$app->request->isAjax){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($priceRule);
                }
                if($priceRule->save()){
                    if(isset($_POST['PriceRules']['stores']) && is_array($_POST['PriceRules']['stores'])){
                        foreach($_POST['PriceRules']['stores'] as $storeId){
                            $storeRule = new StorePriceRules;
                            $storeRule->storeId = $storeId;
                            $storeRule->ruleId = $priceRule->id;
                            $storeRule->save();
                        }
                    }
                    $this->redirect(\yii\helpers\Url::to(['price-rules/index']));
                }
            }
        }
    	return $this->render('create', compact('priceRule'));
    }

    public function actionUpdate($id){
        if($priceRule = PriceRules::findOne($id)){
           // if(Yii::$app->user->identity->roleId == 1 || $priceRule->createdBy != Yii::$app->user->id)
            if($priceRule->createdBy != Yii::$app->user->id || Yii::$app->user->identity->roleId != 1)
                //die('403: Not Allowed');
            if(isset($_POST['PriceRules'])){
                if($priceRule->load(Yii::$app->request->post())){

                    $priceRule->fromDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["PriceRules"]['fromDate']));
                    $priceRule->toDate = date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["PriceRules"]['toDate']));

                    $conditions = [['type' => 'combination', 'aggregator' => $_POST['PriceRules']['aggregator'], 'value' => 'true']];
                    if(isset($_POST['PriceRules']['conditions'])){
                        foreach($_POST['PriceRules']['conditions'] as $i=>$condition){
                            if(isset($condition['value'])){ //process this condition only if it has a value set
                                $conditions[0]['conditions'][$i]['type'] = 'single';
                                $conditions[0]['conditions'][$i]['operator'] = $condition['operator'];
                                $conditions[0]['conditions'][$i]['attribute'] = strtolower($condition['attribute']);
                                $conditions[0]['conditions'][$i]['value'] = isset($condition['value'])? $condition['value'] : "";
                            }
                        }
                    }
                    $priceRule->conditions = serialize($conditions);
                    if(Yii::$app->request->isAjax){
                        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        return \yii\widgets\ActiveForm::validate($priceRule);
                    }
                    if($priceRule->save()){
                        if(isset($_POST['PriceRules']['stores']) && is_array($_POST['PriceRules']['stores'])){
                            StorePriceRules::deleteAll(['ruleId' => $priceRule->id]);
                            foreach($_POST['PriceRules']['stores'] as $storeId){
                                $storeRule = new StorePriceRules;
                                $storeRule->storeId = $storeId;
                                $storeRule->ruleId = $id;
                                $storeRule->save();
                            }
                        }
                        Yii::$app->session->setFlash('success', 'Price RUle updated successfully');
                        $this->redirect(\yii\helpers\Url::to(['index']));
                    }
                }
            }
            return $this->render('update', compact('priceRule'));
        }
    }

    public function actionGetoptions($code, $ruleid = NULL){
        $code = explode(".", $code);

        //var_dump($code);die();

    	if($attribute = \common\models\Attributes::find()->where("code='".strtolower($code[1])."' AND (field='dropdown' OR field='text')")->one()){
    		if($attribute->field=="dropdown")
    			return "<div class='testclass'>".Html::dropDownList("PriceRules[conditions][$ruleid][value]", [], ArrayHelper::map($attribute->options, 'value', 'value'), ['style' => 'width: 156px;','class'=>'pricerule_value form-control']);
    		else
    			return Html::textInput("PriceRules[conditions][$ruleid][value]", ['style' => 'width: 156px;','class'=>'pricerule_value form-control']);
    	}elseif($code[0]=="cart" || $code[0]=="position"){
    		return Html::textInput("PriceRules[conditions][$ruleid][value]", null, ['style' => 'width: 156px;','class'=>'pricerule_value form-control']);
    	}elseif($code[1] == "category"){
            //return Html::textInput("PriceRules[conditions][$ruleid][value]", null, ['placeholder' => 'comma separated IDs', 'style' => 'width: 156px;']);
            //return '<button name="category" class="pick_cat">Select categories</button>';

            return Html::textInput("PriceRules[conditions][$ruleid][value]", null, ['placeholder' => 'comma separated IDs', 'style' => 'width: 156px;', 'class'=>'categoryids form-control']);

            //$html .='<button name="category" class="pick_cat">Select categories</button>';

            //return $html;
        }
        elseif ($code[1] == "brandId") {
            return Html::textInput("PriceRules[conditions][$ruleid][value]", null, ['placeholder' => 'comma separated IDs', 'style' => 'width: 156px;', 'class'=>'brandids']);
        }
    }

    public function actionDelete($id){
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'Item deleted successfully.');
        return $this->redirect(['index']);
    }

    public function actionView($id){
        $rule = PriceRules::findOne($id);
        //var_dump(unserialize($rule->conditions)); die;
        return $this->render('view', compact('rule'));
    }

    protected function findModel($id)
    {
        if (($pricerule = PriceRules::findOne($id)) !== null) {
            return $pricerule;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
