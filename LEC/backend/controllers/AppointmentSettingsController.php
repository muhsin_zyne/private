<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;

class AppointmentSettingsController extends AdminController
{
	public function actionIndex(){
		$appointmentSettings = new AppointmentDefaultSettings();
		$currentDate = date('Y-m-d');
		$weekDay = lcfirst(date('D',strtotime($currentDate)));
		$user = User::findOne(Yii::$app->user->id);
		$storeId = $user->store->id;
		$timeSlots = ['9:00 am - 10:00 am','10:00 am - 11:00 am','11:00 am - 12:00 pm','12:00 pm - 1:00 pm','1:00 pm - 2:00 pm','2:00 pm - 3:00 pm','3:00 pm - 4:00 pm','4:00 pm - 5:00 pm','5:00 pm - 6:00 pm'];
		$currentDateSetting = AppointmentDefaultSettings::find()->select($weekDay)->where(['storeId'=>$storeId])->one();
		
		if(!$defaultSetting = AppointmentDefaultSettings::find()->where(['storeId'=>$storeId])->one()) {
			
			$defaultSetting =  ['sun'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]],'mon'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]],'tue'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]],'wed'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]],'thu'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]],'fri'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]],'sat'=>["workingHours"=>["9:00 am - 10:00 am","10:00 am - 11:00 am","11:00 am - 12:00 pm","12:00 pm - 1:00 pm","1:00 pm - 2:00 pm","2:00 pm - 3:00 pm","3:00 pm - 4:00 pm","4:00 pm - 5:00 pm","5:00 pm - 6:00 pm"]]];

		}
		
		if(isset($_POST['settings'])){  //var_dump($_POST['settings']);die();
			foreach ($_POST['settings']['timeslots'] as $key => $slots) {  
				$timeSlotArray['workingHours'] = [];
				foreach ($slots as $slot) {
					$timeSlotArray['workingHours'][] = $slot;
				}
				if($settings = AppointmentDefaultSettings::find()->where(['storeId'=>$storeId])->one()){
					$settings->$key = json_encode($timeSlotArray);
					$settings->updatedOn = date('Y-m-d h:i:s');
					$settings->save(false);
				}
				else{
					$settings = new AppointmentDefaultSettings();
					$settings->$key = json_encode($timeSlotArray);
					$settings->updatedOn = date('Y-m-d h:i:s');
					$settings->storeId = $storeId;
					$settings->save(false);
				}
			}
			return $this->redirect('index',compact('defaultSetting','currentDate','weekDay','currentDateSetting','appointmentSettings','timeSlots'));
		}
		return $this->render('index',compact('defaultSetting','currentDate','weekDay','currentDateSetting','appointmentSettings','timeSlots'));
	}
}		