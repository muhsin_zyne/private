<?php
namespace backend\controllers;
use Yii;
use common\models\CmsBlocks;
use common\models\search\BlockPagesSearch;
use yii\web\Controller;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\CmsBlockImages;
use common\models\StoreCmsBlocks;
use common\models\User;
use yii\data\ActiveDataProvider;

/**

 * CmsblockController implements the CRUD actions for CmsblockPages model.

 */

class CmsBlocksController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Lists all CmsblockPages models.
     * @return mixed
     */

    public function actionIndex()
    {  
        $user = User::findOne(Yii::$app->user->id);
        $searchModel = new BlockPagesSearch(); 
        $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
        $queryParams["BlockPagesSearch"]["type"] ='block_content' ;       
        $dataProvider = $searchModel->search($queryParams);         
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**     * Displays a single CmsblockPages model.

     * @param integer $id     * @return mixed

     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /*public function actionImageupload()
    {
        //die('hiii');
        $model = new CmsBlocks();
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        //Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/ckeditorimage/';    //exit;
        $image = UploadedFile::getInstance($model, 'image');
        //echo $image;exit;
        $ext = end((explode(".", $image->name)));
        $avatar = Yii::$app->security->generateRandomString().".{$ext}";
        $path = Yii::$app->params['uploadPath'] . $avatar;
        $model_image = new CmsBlockImages();
        $model_image->path = $avatar;
        $model_image->save();
        $image->saveAs($path);
        return true;
    } */

    public function actionGetimages()
    { 
        $html='<ol id="selectable">';
        $ckimage_all= CmsBlockImages::find()->orderBy(['id' => SORT_DESC])->all();
        foreach ($ckimage_all as $index => $ckimage) 
        {
           //$html.='<tr> <td>'.$ckimage['title'].'</td> </tr>';
            $html.='<li class="ui-state-default" id="'.$ckimage['id'].'">
                <img src ="'.Yii::$app->params["rootUrl"].'/store/cms/images/'.$ckimage['path'].'" >
            </li>'; 
        }                    
        $html.='</ol> ';   
        echo  $html; 
    }

    /**

     * Creates a new CmsblockPages model.

     * If creation is successful, the browser will be redirected to the 'view' page.

     * @return mixed

     */

    public function actionCreate()
    {
        $model = new CmsBlocks();
        $user = User::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post())) {
            //var_dump($_POST);die;
            if($user->roleId==1)
            {
                $stores_array=$_POST['CmsBlocks']['selected_store'];
            }
            elseif ($user->roleId==3) {
                $stores_array[] = Yii::$app->params['storeId'];
            } 
            $model->createdBy=Yii::$app->user->id;
            if ($model->save())
            {    
                foreach ($stores_array as  $value) { 
                   $model_sp=new StoreCmsBlocks();
                   $model_sp->storeId=$value;
                   $model_sp->blockId=$model->id;
                   $model_sp->save();
                }
                Yii::$app->session->setFlash('success', 'Page created successfully');
                return $this->redirect(['index']);   
            }
            else{ 
                var_dump($model->geterrors());die;
                return $this->render('create', [ 'model' => $model,]); 
            } 
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**

     * Updates an existing CmsblockPages model.

     * If update is successful, the browser will be redirected to the 'view' page.

     * @param integer $id

     * @return mixed

     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(isset($_POST['CmsBlocks']))  
        {
            $stores_array=$_POST['CmsBlocks']['selected_store'];
            $model->content=$_POST['CmsBlocks']['content'];
            StoreCmsBlocks::deleteAll(['blockId' => $id]);
            if ($model->load(Yii::$app->request->post()) && $model->save())
            {      //var_dump(Yii::$app->request->post());die;

                foreach ($stores_array as  $value) { 
                    $model_sp=new StoreCmsBlocks();
                    $model_sp->storeId=$value;
                    $model_sp->blockId=$model->id;
                    $model_sp->save();
                }
                Yii::$app->session->setFlash('success', 'Page updated successfully');
                return $this->redirect(['index']);
            }
            else{ 
                return $this->render('update', [ 'model' => $model,]); 
            }
        }
        else 
        { return $this->render('update', ['model' => $model,]);  }          

    }



    /**

     * Deletes an existing CmsblockPages model.

     * If deletion is successful, the browser will be redirected to the 'index' page.

     * @param integer $id

     * @return mixed

     */

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }



    /**

     * Finds the CmsblockPages model based on its primary key value.

     * If the model is not found, a 404 HTTP exception will be thrown.

     * @param integer $id

     * @return CmsblockPages the loaded model

     * @throws NotFoundHttpException if the model cannot be found

     */

    protected function findModel($id)
    {
        if (($model = CmsBlocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionImageBrowse()// ckeditor image browse 
    { 
        $storeId=(isset(Yii::$app->params['storeId']))?Yii::$app->params['storeId']:0;
        $cmsImages=CmsBlockImages::find()->where(['storeId'=>$storeId])->orderBy(['id' => SORT_DESC])->all();        
        return $this->renderPartial('image',compact('cmsImages'));
    }
    public function actionImageUpload()// ckeditor image upload
    {    
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        $uploadedFile = UploadedFile::getInstanceByName('upload'); 
        //$mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);
        $file = time()."_".$uploadedFile->name;
        $url=Yii::$app->params["rootUrl"]."/../store/cms/images/".$file; 
        $uploadPath = Yii::$app->params['uploadPath'].$file;  
        $storeId=(isset(Yii::$app->params['storeId']))?Yii::$app->params['storeId']:0;      
        $check = getimagesize($uploadedFile->tempName);
        if ($uploadedFile==null)
        {
           $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
           $message = "The file is of zero length.";
        }
        else if($check['mime']!='image/jpeg' && $check['mime']!='image/jpg' && $check['mime']!='image/png'){
            $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
            $url='';
        }
        /*else if ($mime!="image/jpeg" && $mime!="image/png" && $mime!="image/jpg")
        {
           $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
        }*/
        else if ($uploadedFile->tempName==null)
        {
           $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
          $message = "";
            $model_image = new CmsBlockImages();
            $model_image->path = $file;
            $model_image->storeId=$storeId;
            $model_image->save(); 
          $move = $uploadedFile->saveAs($uploadPath);
          if(!$move)
          {
             $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
          } 
        }        
        $funcNum = $_GET['CKEditorFuncNum'] ;
        //var_dump($_GET['CKEditorFuncNum']);die;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";        
    }

}

