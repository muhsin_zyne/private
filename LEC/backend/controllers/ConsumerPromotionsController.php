<?php

namespace backend\controllers;

use Yii;
use common\models\ConsumerPromotions;
use common\models\ConsumerPromotionsSearch;
use common\models\ConsumerPromotionProducts;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\models\Products;
use common\models\Stores;
use common\models\ProductCategories;
use common\models\AttributeValues;
use common\models\Attributes;
use common\models\StorePromotions;
use common\models\StoreProducts;
use yii\data\ActiveDataProvider;
use frontend\components\Helper;
use common\models\EmailTemplates;

/**
 * ConsumerPromotionsController implements the CRUD actions for ConsumerPromotions model.
 */
class ConsumerPromotionsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ConsumerPromotions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConsumerPromotionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ConsumerPromotions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $promotionpdts = new ActiveDataProvider([
            'query' => ConsumerPromotionProducts::find()->where(['promotionId'=>$id]),
        ]);

        // return $this->render('view', [
        //     'promotion' => $this->findModel($id),
        // ]);

        $promotion = $this->findModel($id);

        return $this->render('view', compact('promotion','promotionpdts'));
    }

    /**
     * Creates a new ConsumerPromotions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $promotion = new ConsumerPromotions();
        $stores = Stores::find()->where(['isVirtual'=>0])->all();
        $products = new Products();


        if ($promotion->load(Yii::$app->request->post())){  //die('hi');


            //var_dump($_POST["ConsumerPromotions"]["storeIds"]);die();

            $promotion->fromDate =date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["ConsumerPromotions"]['fromDate']));
            $promotion->toDate =date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["ConsumerPromotions"]['toDate']));
            $promotion->consumerStartDate =date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["ConsumerPromotions"]['consumerStartDate']));
            //$promotion->reminderDate =Helper::convertdate(Yii::$app->request->post()["ConsumerPromotions"]['reminderDate']);
            $promotion->costEndDate =date('Y-m-d H:i:s', strtotime(Yii::$app->request->post()["ConsumerPromotions"]['costEndDate']));
            if ($promotion->save(false)) {
                if(isset($_POST['Products']['id']) && !empty($_POST['Products']['id'])){
                    foreach ($_POST['Products']['id'] as  $id) { //var_dump($_POST['Products'][$id]['offerSellPrice']);die();
                        $promotionProducts = new ConsumerPromotionProducts;
                        $promotionProducts->promotionId = $promotion->id;
                        $promotionProducts->productId = $id;
                        $promotionProducts->offerSellPrice = $_POST['Products'][$id]['offerSellPrice'];
                        $promotionProducts->offerCostPrice = $_POST['Products'][$id]['offerCostPrice'];
                        if(isset($_POST['Products'][$id]['pageId']) && $_POST['Products'][$id]['pageId'] !=0)
                            $promotionProducts->pageId = $_POST['Products'][$id]['pageId'];
                        else
                            $promotionProducts->pageId = 1;
                        
                        $promotionProducts->save(false);
                    }
                }    
                if(isset($_POST["ConsumerPromotions"]["stores"]) && !empty($_POST["ConsumerPromotions"]["stores"])){
                    foreach ($_POST["ConsumerPromotions"]["stores"] as $storeId) {
                        $storePromotion = new StorePromotions;
                        $storePromotion->storeId = $storeId;
                        $storePromotion->promotionId = $promotion->id;
                        $storePromotion->startDate = date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['fromDate']));
                        $storePromotion->endDate = date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['toDate']));
                        $storePromotion->active = 1;
                        $storePromotion->save(false);

                        if(isset($_POST['Products']['id']) && !empty($_POST['Products']['id'])){
                            foreach ($_POST['Products']['id'] as  $productId) { 
                                $products->extendDisableDate($storeId,$productId,$_POST["ConsumerPromotions"]['fromDate']);
                            }
                        }
                    }
                }    
                return $this->redirect(['view', 'id' => $promotion->id]);
            } 
        }
        else {
                return $this->render('create', compact('promotion','stores'));
        }
        
    }    

    /**
     * Updates an existing ConsumerPromotions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $promotion = $this->findModel($id);
        $stores = Stores::find()->where(['isVirtual'=>0])->all();
        $promotionProducts = $promotion->promotionProducts;
        

        $products = new Products();

        if ($promotion->load(Yii::$app->request->post())){

            $promotion->fromDate =date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['fromDate']));
            $promotion->toDate =date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['toDate']));
            $promotion->consumerStartDate =date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['consumerStartDate']));
            //$promotion->reminderDate =date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['reminderDate']));
            $promotion->costEndDate =date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['costEndDate']));

            if(isset($_POST['Products']['deletedItems'])) {
                ConsumerPromotionProducts::deleteAll('id in ('.implode($_POST['Products']['deletedItems']).')');
            }

            StorePromotions::deleteAll('promotionId ='.$promotion->id);
            if(isset($_POST["ConsumerPromotions"]["stores"]) && !empty($_POST["ConsumerPromotions"]["stores"])){
                foreach ($_POST["ConsumerPromotions"]["stores"] as $storeId) {
                    if(!$storePromotion = StorePromotions::find()->where(['promotionId'=>$promotion->id,'storeId'=>$storeId])->one()) {
                        $storePromotion = new StorePromotions;
                        $storePromotion->storeId = $storeId;
                        $storePromotion->promotionId = $promotion->id;
                        $storePromotion->startDate = date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['fromDate']));
                        $storePromotion->endDate = date('Y-m-d H:i:s', strtotime($_POST["ConsumerPromotions"]['toDate']));
                        $storePromotion->active = 1;
                        $storePromotion->save(false);
                    }
                }    
            }    
          
            if($promotion->save(false)) {
                //----- StoreProducts -> disableDate change to NULL -------------------------------
                foreach ($promotion->stores as $store) {
                    $storeId =  $store->storeId;
                    foreach ($promotionProducts as $product) {
                        $productId = $product->productId;
                        $StoreProducts = StoreProducts::find()
                                            ->where(['storeId' => $storeId, 'productId' => $productId, 'enabled' => 1, 'type' => 'b2c'])
                                            ->andWhere("disableDate != ''")
                                            ->all();
                        foreach ($StoreProducts as $key => $StoreProduct) {
                            $StoreProduct->disableDate = NULL;
                            $dateExtended = $StoreProduct->save(false);
                        }
                    }
                }
                //--------------------------------------------------------------------------------------

                if(isset($_POST['Products']['id'])){
                    foreach ($_POST['Products']['id'] as  $id) {
                        if(isset($_POST['Products'][$id]['newitem'])){
                            $promotionProduct = new ConsumerPromotionProducts;
                            $promotionProduct->promotionId = $promotion->id;
                            $promotionProduct->productId = $id;
                            $promotionProduct->offerSellPrice = $_POST['Products'][$id]['offerSellPrice'];
                            $promotionProduct->offerCostPrice = $_POST['Products'][$id]['offerCostPrice'];
                            $promotionProduct->pageId = $_POST['Products'][$id]['pageId'];
                            $promotionProduct->save(false);  
                            $productId = $id;
                        }
                        else{
                            if($promotionProduct = ConsumerPromotionProducts::find()->where('id = '.$id.' and promotionId = '.$promotion->id.'')->one())
                            {
                                $promotionProduct->offerSellPrice = $_POST['Products'][$id]['offerSellPrice'];
                                $promotionProduct->offerCostPrice = $_POST['Products'][$id]['offerCostPrice'];
                                $promotionProduct->pageId = $_POST['Products'][$id]['pageId'];
                                $promotionProduct->save(false);
                                $productId = $promotionProduct->productId;
                            }    
                        }

                        foreach ($_POST["ConsumerPromotions"]["stores"] as $storeId) {
                            $products->extendDisableDate($storeId,$productId,$_POST["ConsumerPromotions"]['fromDate']);
                        }
                    }

                    
                }
                return $this->redirect(['view', 'id' => $promotion->id]);
            }
        } 
        else {
            return $this->render('update', compact('promotion','stores','promotionProducts'));
        }
    }

    /**
     * Deletes an existing ConsumerPromotions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetproducts(){
        $keyword = $_REQUEST['keyword'];
        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    //->select('p.id, av.value')
                    ->from('Products as p')
                    ->join('JOIN',
                        'AttributeValues as av',
                        'av.productId = p.id'
                    )
                    ->join('JOIN',
                        'Attributes as a',
                        'a.id = av.attributeId'
                    )
                    ->groupBy('p.id')
                    
                    ->where("(p.sku = '".$keyword."' OR av.value like '%".$keyword."%') AND a.code='name'");
                    
                    if(empty($keyword))
                        $products->andWhere("1!=1");

                    $products->all();

        foreach($products->all() as $product)
                $data[$product->id] = ['id'=>$product->id,'name'=>$product->name, 'sku'=>$product->sku, 'price'=>$product->price, 'cost_price'=>$product->cost_price];
        return json_encode($data);      
    }  

    public function actionGetcategoryproducts(){

        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    ->from('Products as p')
                    ->join('JOIN',
                        'ProductCategories as pc',
                        'pc.productId = p.id'
                    )
                    ->groupBy('p.id')
                    ->where("(pc.categoryId in (".$_REQUEST['categoryIds']."))");
                    $products->all();

        foreach($products->all() as $product)
            $data[$product->id] = ['id'=>$product->id, 'name'=>$product->name, 'sku'=>$product->sku, 'price'=>$product->price, 'cost_price'=>$product->cost_price];
        return json_encode($data);     
    }    

    public function actionPublish($id){  
        $promotion = ConsumerPromotions::findOne($id);

        //var_dump($promotion->status);die();

       /* if($promotion->canPublish())
            Yii::$app->getSession()->setFlash('error', 'Promotion is already published or has ended!');*/

        if($promotion->status!="not-published"){ //die('hai');
            Yii::$app->getSession()->setFlash('success', 'Promotion is already published or has ended!');
            $this->redirect(\yii\helpers\Url::to(['consumerpromotions/view', 'id' => $id]));
        }


        // write publishing logic here

        //var_dump($promotion->canPublish());die();
        if($promotion->canPublish()) {   //die('hai'); 
            $promotion->status = "published";
            if($promotion->save(false)) {  
                Yii::$app->getSession()->setFlash('check', 'Promotion published successfully !');
                //$promotion->sendPublishNotification();
                //die('published');
                return $this->redirect(['view', 'id' => $id]);
            }
        }    

        
        
    }

    public function actionTest(){  
        if(isset($_FILES['csv'])){ 
    if ($_FILES['csv']['size'] > 0) {  
        $file = $_FILES['csv']['tmp_name'];
        $handle = fopen($file,"r");
        $data = fgetcsv($handle,1000,",","'");
        $uploadedProducts = Array();
        $notFound = Array();
        do {
                if ($data[0]) {
                    if ($product = Products::find()->where(['sku'=>$data[0]])->one()) {
                        $uploadedProducts[] = 
                            [
                                'id' =>$product->id,
                                'sku'=>$data[0],
                                'name'=>$product->name,
                                'price'=>$product->price,
                                'costprice'=>$product->cost_price,
                                'offerSellPrice' => $data[1],
                                'offerCostPrice' => $data[2],
                                'pageId' => $data[3],
                            ];
                    }   
                    else{
                        $notFound[] = [
                            'sku'=>$data[0],
                        ];
                    } 
                }
        }
        while ($data = fgetcsv($handle,1000,",","'"));
    }

    return json_encode(['notfound' => $notFound, 'uploadedProducts'=> $uploadedProducts]);
}
    }    

    /**
     * Finds the ConsumerPromotions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConsumerPromotions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($promotion = ConsumerPromotions::findOne($id)) !== null) {
            return $promotion;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
