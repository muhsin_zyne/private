<?php

namespace backend\controllers;

use Yii;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\ClientPortal;
use common\models\ClientPortalProducts;
use common\models\search\ClientPortalSearch;
use common\models\AttributeValues;
use yii\widgets\ActiveForm;
use common\models\Attributes;
use common\models\search\OrdersSearch;
use common\models\OrderItems;
use common\models\User;
use common\models\Invoices;
use common\models\OrderComment;
use common\models\CreditMemos;
use common\models\Shipments;
use common\models\SalesComments;
use common\models\Deliveries;
use common\models\Stores;
use common\models\search\DeliveriesSearch;
use common\models\search\ShipmentsSearch;
use common\models\search\InvoicesSearch;
use common\models\search\CreditMemosSearch;
use common\models\Orders;
use frontend\components\Helper;
use yii\web\UploadedFile;
use yii\db\ActiveQuery;
use yii\web\Response;

class ByodController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	//var_dump(md5(Yii::$app->user->identity->store->id."testcode".microtime()));die();   

        $searchModel = new ClientPortalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $dataProvider->query->andWhere(['type'=>'byod','storeId'=>$user->store->id]);
        else
            $dataProvider->query->andWhere(['type'=>'byod']);
        $dataProvider->query->orderBy(['id'=>SORT_DESC,]);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    public function actionCreate($duplicateFrom=null){
    	if(!$byod = ClientPortal::find()->where(['id'=>$duplicateFrom])->one())
			$byod = new ClientPortal();
		else
			$byod->isNewRecord = true;

        if (Yii::$app->request->isAjax && $byod->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($byod);
        }

		if ($byod->load(Yii::$app->request->post())) {  

            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($byod);
            }

            $byod->validFrom = date('Y-m-d H:i:s', strtotime($byod->validFrom));
            $byod->expiresOn = date('Y-m-d H:i:s', strtotime($byod->expiresOn));

            if($byod->schoolDeliverydate != "0000-00-00 00:00:00" && !empty($byod->schoolDeliverydate)) {
                $byod->schoolDeliverydate = date('Y-m-d H:i:s', strtotime($byod->schoolDeliverydate));
            }
            else{
                $byod->schoolDeliverydate = "0000-00-00 00:00:00";
            }    

            $byod->storeId = Yii::$app->user->identity->store->id;
            $byod->type = "byod"; 
            $byod->shipment_type = $_POST['ClientPortal']['shipment_type'];
            $byod->studentAuthCode = md5($byod->studentCode.Yii::$app->user->identity->store->id.microtime());
            $byod->schoolAuthCode = md5($byod->schoolCode.Yii::$app->user->identity->store->id.microtime());

            if(UploadedFile::getInstance($byod,'organisation_logopath')!= '')
                {
                    $image = UploadedFile::getInstance($byod, 'organisation_logopath');  
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/byod/logo/";    
                    $size = filesize($image->tempName);            
                    $ext = end((explode(".", $image->name)));
                    $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                    $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                    $uploadPath="/store/byod/logo/".$avatar;
                    $byod->organisation_logopath = $uploadPath; 

                    $fileDimensions = getimagesize($image->tempName);
                    $imageType = strtolower($fileDimensions['mime']); 

                    switch(strtolower($imageType))
                    {
                        case 'image/png': 
                        $img = imagecreatefrompng($image->tempName);
                        break;

                        case 'image/gif':
                        $img = imagecreatefromgif($image->tempName);
                        break;
                        
                        case 'image/jpeg':
                        $img = imagecreatefromjpeg($image->tempName);
                        break;
                        
                        default:
                        die('Unsupported File!'); //output error
                    }  
                    
                    $byod->width=imagesx($img);
                    $byod->height=imagesy($img);

                    if($byod->width!= 260 & $byod->height!= 120)
                        {                
                            Yii::$app->getSession()->setFlash('error', 'Logo Size should be 260px X 120px');
                            return $this->render('create', compact('byod'));
                        }
                    else{
                        $image->saveAs($savepath);    
                    }
                }
            $byod->id = null;
            if ($byod->save()) {
                 if(isset($_POST['Products'])){
                    foreach ($_POST['Products']['id'] as  $id) {
                        $byodProduct = new ClientPortalProducts;
                        $byodProduct->portalId = $byod->id;
                        $byodProduct->productId = $id;
                        $byodProduct->offerPrice = $_POST['Products'][$id]['offerPrice'];
                        $byodProduct->save(false);
                    }
                }
            } 
            $searchModel = new ClientPortalSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            Yii::$app->session->setFlash('success', 'BYOD Created Successfully !'); 
            return $this->redirect('index', compact('searchModel','dataProvider'));  
        }
        else{
        	//$this->view->registerJsFile('/js/datepicker-kv.js', ['position' => \yii\web\View::POS_END]);
            return $this->render('create', compact('byod'));
        }    
    }

    public function actionAssign()
    {
        $user = User::findOne(Yii::$app->user->id);
        $byodlist = ClientPortal::find()->where(['storeId'=>$user->store->id,'type'=>'byod'])->all();
        if(isset($_POST['Products'])){
            foreach ($_POST['Products'] as $productId => $values) {
                foreach ($_POST['Byod']['id'] as $key => $byodId) {
                    if(!$product = ClientPortalProducts::find()->where(['portalId'=>$byodId,'productId'=>$productId])->one()){
                        $product = new ClientPortalProducts();
                        $product->portalId = $byodId;
                        $product->productId = $productId;
                        $product->offerPrice = $values['offerPrice'];
                        $product->save(false);
                    }
                }
            }
            Yii::$app->session->setFlash('success','Products are assigned to the selected BYOD successfully !'); 
            return $this->redirect('index');
        }
        return $this->render('assign',compact('byodlist'));
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'BYOD Deleted Successfully !');
        return $this->redirect(['index']);
    }

    public function actionUpdate($id){  
        $byod = $this->findModel($id);
        //$byodProducts = $byod->clientPortalProducts;
        $logopath = $byod->organisation_logopath;

        if (Yii::$app->request->isAjax && $byod->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($byod);
        }

        if ($byod->load(Yii::$app->request->post())){  //var_dump(UploadedFile::getInstance($byod,'organisation_logopath'));die();
            
            if(isset($_POST['Products']['deletedItems'])) { 
                ClientPortalProducts::deleteAll('id in ('.implode(',',$_POST['Products']['deletedItems']).')');
            }

                if(UploadedFile::getInstance($byod,'organisation_logopath')!= ''){
                    $image = UploadedFile::getInstance($byod, 'organisation_logopath');  
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/byod/logo/";    
                    $size = filesize($image->tempName);            
                    $ext = end((explode(".", $image->name)));
                    $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                    $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                    $uploadPath="/store/byod/logo/".$avatar;
                    $byod->organisation_logopath = $uploadPath; 

                    $fileDimensions = getimagesize($image->tempName);
                    $imageType = strtolower($fileDimensions['mime']); 

                    switch(strtolower($imageType))
                    {
                        case 'image/png':
                        $img = imagecreatefrompng($image->tempName);
                        break;

                        case 'image/gif':
                        $img = imagecreatefromgif($image->tempName);
                        break;
                        
                        case 'image/jpeg':
                        $img = imagecreatefromjpeg($image->tempName);
                        break;
                        
                        default:
                        die('Unsupported File!'); //output error
                    }    
                    
                    $byod->width=imagesx($img);
                    $byod->height=imagesy($img);
                    if($byod->width!= "260" & $byod->height!= "120")
                        {                
                            Yii::$app->getSession()->setFlash('error', 'Brand Logo Size should be 260px X 120px');
                            return $this->render('create', compact('byod'));
                        }
                    else{
                            // if (file_exists($logoImage =  Yii::$app->params['uploadPath'].$logopath)) { //die('present');
                            //     unlink($logoImage);
                            //     $image->saveAs($savepath);
                            // }
                            // else
                                $image->saveAs($savepath);        
                    }
                }
                else
                    $byod->organisation_logopath = $logopath;

            $byod->shipment_type = $_POST['ClientPortal']['shipment_type'];
            $byod->validFrom =date('Y-m-d H:i:s', strtotime($byod->validFrom));
            $byod->expiresOn =date('Y-m-d H:i:s', strtotime($byod->expiresOn));
            if(!empty($byod->schoolDeliverydate) && $byod->schoolDeliverydate != "0000-00-00 00:00:00"){ //var_dump($byod->schoolDeliverydate);die('date');
                $byod->schoolDeliverydate = date('Y-m-d H:i:s', strtotime($byod->schoolDeliverydate));
            }
            else
                $byod->schoolDeliverydate = "0000-00-00 00:00:00";

            if($byod->save()) {
                if(isset($_POST['Products']['id'])){
                    foreach ($_POST['Products']['id'] as  $id) { //var_dump($id);die();   
                        if(isset($_POST['Products'][$id]['newitem'])){  
                            $byodProduct = new ClientPortalProducts;
                            $byodProduct->portalId = $byod->id;
                            $byodProduct->productId = $id;
                            $byodProduct->offerPrice = $_POST['Products'][$id]['offerPrice'];
                            $byodProduct->save(false);    
                        }
                        else{ 
                            $byodProduct = ClientPortalProducts::find()->where('productId = '.$id.' and portalId = '.$byod->id.'')->one();
                            $byodProduct->offerPrice = $_POST['Products'][$id]['offerPrice'];
                            $byodProduct->save(false);
                        }
                    }
                } 
            } 
            $searchModel = new ClientPortalSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            Yii::$app->session->setFlash('success', 'BYOD Updated Successfully !'); 
            return $this->redirect('index', compact('searchModel','dataProvider'));   
        }
        else{
             return $this->render('update',compact('byod'));
        }    
    }

    public function actionView($id)
    {
        
        $query = \common\models\ClientPortalProducts::find();
        $products = $query
                    ->from('ClientPortalProducts')
                    ->join('JOIN',
                        'Products p',
                        'ClientPortalProducts.productId = p.id'
                    )
                    ->where("p.status = '1' AND ClientPortalProducts.portalId=$id");

        $byodProducts = new ActiveDataProvider([
            'query' => $query,
        ]);

        $byod = $this->findModel($id);
        /*$byodProducts = new ActiveDataProvider([
            'query' => ClientPortalProducts::find()->where(['portalId'=>$id]),
        ]);*/
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $store = Stores::findOne(Yii::$app->user->identity->store->id);
        elseif($user->roleId==1)
            $store = Stores::findOne($byod->storeId);
        
        return $this->render('view', compact('byod','byodProducts','store'));
    }

    public function actionGetproducts(){
        $keyword = $_REQUEST['keyword'];
        $storeId = Yii::$app->params['storeId'];
        $data = [];
        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    ->from('Products as p')
                    ->join('JOIN',
                        'AttributeValues as av',
                        'av.productId = p.id'
                    )
                    ->join('JOIN',
                        'Attributes as a',
                        'a.id = av.attributeId'
                    )
                    ->join('JOIN',
                        'StoreProducts as sp',
                        'p.id = sp.productId'
                    )
                    ->groupBy('p.id')
                     ->where("(p.sku = '".$keyword."' OR av.value like '%".$keyword."%') AND a.code='name' AND sp.storeId='".$storeId."' AND p.status = 1 AND sp.enabled = 1");
                    
                    if(empty($keyword))
                        $products->andWhere("1!=1");

                    $products->all();

        foreach($products->all() as $product)
            $data[$product->id] = ['id'=>$product->id,'name'=>$product->name, 'sku'=>$product->sku, 'price'=>$product->price];
        return json_encode($data);      
    }

    public function actionReport($id) {   

    	$byodId = $id;

    	/*$byod = ClientPortal::find()->where(['id'=>$id,'type'=>'byod'])->one();

    	$searchModel = new OrdersSearch();
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $dataProvider->query->andWhere('type = "byod" and storeId = '.$user->store->id.' and portalId= '.$byod->id.'')->orderBy(['id'=>SORT_DESC,]);
        else
            $dataProvider->query->andWhere(['type' => 'byod','portalId' => $byod->id])->orderBy(['id'=>SORT_DESC,]);*/


        
        return $this->render('reportView',compact('byodId'));


        //$content = $this->renderPartial('_reportView',compact('byod','dataProvider','searchModel'));
    	//$content = "Test Content";
        
        // setup kartik\mpdf\Pdf component
        /*$pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Byod Orders Report'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['$byod->id'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        /*$response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_RAW;
            $headers = Yii::$app->response->headers;
            $headers->add('Content-Type', 'application/pdf');*/
        
        // return the pdf output as per the destination setting
        //$res =  $pdf->render(); 
    }


    protected function findModel($id)
    {
        if (($byod = ClientPortal::findOne($id)) !== null) {
            return $byod;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
?>    
