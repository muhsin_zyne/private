<?php

namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\helpers\Html;
use common\models\HelpCategories;
use common\models\HelpItems;
use yii\web\UploadedFile;

class HelpController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$helpCategories = HelpCategories::find()->where(['active'=>1])->orderBy('position')->all();
        return $this->render('index',compact('helpCategories'));
    }

    public function actionCreate()
    {
    	$helpItem = new HelpItems();
    	if ($helpItem->load(Yii::$app->request->post())) {
    		$helpItem->position = 1;
            $helpItem->save(false);
            return $this->redirect('list');	
    	} 
    	else {
    		if(\Yii::$app->request->isAjax){
                return $this->renderAjax('itemform',compact('helpItem'));
            }
            else {    
                return $this->render('itemform', compact('helpItem'));
            } 
    	}
    }

    public function actionUpdate($id)
    {
        $helpItem = $this->findModel($id);
        if ($helpItem->load(Yii::$app->request->post())){
            $helpItem->save(false);
            return $this->redirect('list'); 
        }
        else{
             return $this->render('itemform',compact('helpItem'));
        }   
    }

    public function actionView($id)
    {
        $helpItem = $this->findModel($id);
        return $this->render('view',compact('helpItem'));
    }

    public function actionList()
    {
        $helpCategories = HelpCategories::find()->where(['active'=>1])->orderBy('position')->all();
        $activeHelpItems = HelpItems::find()->where(['enabled'=>1])->orderBy('position')->all();

        return $this->render('list',compact('helpCategories','helpItems'));
    }

    public function actionScreenshot($id)
    {
        $helpItem = $this->findModel($id);
        return $this->render('screenshot',compact('helpItem'));
    }

    public function actionChangeorder(){
        $treeOrder = json_decode($_POST ["treeOrder"],true);

        foreach ($treeOrder as $key => $tree) {
            if($helpCategories = HelpCategories::find()->where(['id'=>$tree['id'],'active'=>1])->one()){
            	$helpCategories->position = $key+1;
            	$helpCategories->save(false);
            }

            if(isset($tree['children'])){
                foreach ($tree['children'] as $childpos => $children) {
                	if(!$helpItem = HelpItems::find()->where(['id'=>$children['id'],'categoryId'=>$tree['id']])->one()){
                		$helpItem = $this->findModel($children['id']);
                		$helpItem->categoryId = $tree['id'];
                        $helpItem->position = $childpos+1;
                        $helpItem->save(false);
                	}
                	else{
                		$helpItem->position = $childpos+1;
                        $helpItem->save(false);
                	}
                }
            }
        }
    }

    public function actionSearch()
    {
        
    }

    public function actionImageUpload()// ckeditor image upload
    {
        $uploadedFile = UploadedFile::getInstanceByName('upload'); 
        $file = time()."_".$uploadedFile->name;
        $url=Yii::$app->params["rootUrl"]."/../store/help/images/".$file; 
        $uploadPath = Yii::$app->basePath."/../store/help/images/".$file;
        $check = getimagesize($uploadedFile->tempName);

        if ($uploadedFile==null)
        {
           $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
           $message = "The file is of zero length.";
        }
        else if($check['mime']!='image/jpeg' && $check['mime']!='image/jpg' && $check['mime']!='image/png'){
            $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
            $url='';
        }
        /*else if ($mime!="image/jpeg" && $mime!="image/png")
        {
           $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        }*/
        else if ($uploadedFile->tempName==null)
        {
           $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
          $message = "";
          $move = $uploadedFile->saveAs($uploadPath);
          if(!$move)
          {
             $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
          } 
        }        
        $funcNum = $_GET['CKEditorFuncNum'] ;
        //var_dump($_GET['CKEditorFuncNum']);die;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>"; 
    }    

    protected function findModel($id)
    {
        if (($helpItem = HelpItems::findOne($id)) !== null) {
            return $helpItem;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}    