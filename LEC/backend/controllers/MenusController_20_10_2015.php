<?php

namespace backend\controllers;

use Yii;
use common\models\Menus;
use common\models\User;
use common\models\search\MenusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\Stores;
use common\models\Categories;
use yii\helpers\Html;
/**
 * MenusController implements the CRUD actions for Menus model.
 */
class MenusController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menus models.
     * @return mixed
     */
    public function actionIndex($sid= NULL)
    {
        $searchModel = new MenusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId != 1)
        {
           $sid=$user->store->id;
        }
        $storeid = ($sid==NULL ? 0 : $sid );
        //var_dump($test);die;
        $menuall=Menus::find()->where(['storeId'=>$storeid,'unAssigned'=>0])->orderBy('position')->all();
        return $this->render('index',compact('menuall','storeid'));
        
    }
    // public function actionIn() //----------------check categories create
    // {
    //     $model = Categories::find()->where(['id' => 19])->one();
    //     //print_r($model);
    //     Menus::createCategoryMenuItem($model);echo "haiiiiiiiii";
    // }

    /**
     * Displays a single Menus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           die('Menu has been Created Successfully.'); 
        } 
        if(\Yii::$app->request->isAjax){
            return $this->renderPartial('_form',compact('model'));
        }
        // return $this->renderPartial('create', [
        //     'model' => $model,
        // ]);
        //return $this->redirect(['index']); 

    }
     public function actionStoremenu()
    {
        //echo "1" ;exit;
        //echo $_POST['store_id'];exit;
        $storeid=$_POST['store_id'];
        $menuall=Menus::find()->where(['storeId'=>$storeid,'unAssigned'=>0])->orderBy('position')->all();
        $html='
        <div id="row_main">
            <div class="tab-content responsive">

                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header"> <h3 class="box-title">Menu List</h3>
                            <div class="dd box-body" id="nestable">
                                <ol class="dd-list"> ';
                                    
                                    //$parent0=Menus::find()->where(['storeId' => $storeid,'parent' => 0])->orderBy('position')->all();
                                    foreach ($menuall as $key => $par0) 
                                    { 
                                        if($par0['parent']==0)
                                        {                          
                                            $html.='                    
                                            <li class="dd-item dd3-item" data-id="'.$par0['id'].'"><div class="dd-handle dd3-handle"></div>
                                                <div class="dd3-content">'.$par0['title'].'
                                                    <u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/menus/updatemenu','id'=>$par0['id']],
                                                        ['class'=>'ajax-update','data-title' => 'Update Menu',]) .'</u>
                                                    <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/menus/delete', 'id' => $par0['id']], 
                                                        ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) .'</u>
                                                </div>                  
                                                <ol class="dd-list">';
                                                    //$parent1=Menus::find()->where(['storeId' => $storeid,'parent' => $par0['id']])->orderBy('position')->all();
                                                    foreach ($menuall as $index => $par1) 
                                                    {  
                                                        if($par1['parent']==$par0['id'])
                                                        {                          
                                                            $html.='
                                                            <li class="dd-item dd3-item" data-id="'.$par1['id'].'"><div class="dd-handle dd3-handle"></div>
                                                                <div class="dd3-content">'.$par1['title'].'
                                                                    <u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/menus/updatemenu','id'=>$par1['id']],
                                                                        ['class'=>'ajax-update','data-title' => 'Update Menu',]) .'</u>
                                                                    <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/menus/delete', 'id' => $par1['id']], 
                                                                        ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) .'</u> 
                                                                </div>
                                                                <ol class="dd-list">';
                                                                    //$parent2=Menus::find()->where(['storeId' => $storeid,'parent' => $par1['id']])->orderBy('position')->all();
                                                                    foreach ($menuall as $index => $par2) 
                                                                    {  
                                                                        if($par2['parent']==$par1['id'])
                                                                        {
                                                                            $html.='
                                                                            <li class="dd-item dd3-item" data-id="'.$par2['id'].'"><div class="dd-handle dd3-handle"></div>
                                                                                <div class="dd3-content">'.$par2['title'].'
                                                                                    <u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/menus/updatemenu','id'=>$par2['id']],
                                                                                        ['class'=>'ajax-update','data-title' => 'Update Menu',]).'</u>
                                                                                    <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/menus/delete', 'id' => $par2['id']], 
                                                                                        ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]).'</u>
                                                                                </div>
                                                                           </li>';
                                                                        }                                        
                                                                    }   
                                                                $html.='</ol>
                                                            </li>'; 
                                                        }
                                                    } 
                                                $html.='</ol>
                                            </li>';
                                        } 
                                    }              
                                $html.='</ol>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header"><h3 class="box-title">Unassigned Menu</h3>
                            <div class="dd" id="nestable2">
                                <ol class="dd-list">';
                                    //$storeid=-1;
                                    $unassign=Menus::find()->where(['storeId' => $storeid,'unAssigned'=>1])->all();
                                    if(empty($unassign))
                                    {
                                        $html.='<div class="dd-empty"></div>';
                                    }
                                    foreach ($unassign as $index => $unassignmenu) 
                                    { 
                                        $html.=' <li class="dd-item dd3-item" data-id="'.$unassignmenu['id'].'"><div class="dd-handle dd3-handle"></div>
                                            <div class="dd3-content">'.$unassignmenu['title'].'
                                                <u class="menu-icon-set">'.Html::a('<i class="fa fa-pencil"></i> Edit', ['/menus/updatemenu','id'=>$unassignmenu['id']],
                                                    ['class'=>'ajax-update','data-title' => 'Update Menu',]) .'</u>
                                                <u class="menu-icon-set">'.Html::a('<i class="fa fa-trash"></i> Delete', ['/menus/delete', 'id' => $unassignmenu['id']], 
                                                    ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) .'</u>

                                            </div>
                                        </li>'; 
                                    }                        
                                $html.='</ol>
                            </div>
                            <textarea id="nestable-output" style="display:none;"></textarea> 
                            <textarea id="nestable2-output" style="display:none;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>';

    echo $html;
    }

    
    
    public function actionUpdate() // menu save to order
    {
        //echo "hai";exit;
        $model = new Menus();
        $storeid=$_POST['store_id'];
        //var_dump($_POST['store_id']);die;
        $assigned_menu=$_POST['assigned_menu'];
        $unassigned_menu=$_POST['unassigned_menu'];
        $ass_menu_array = json_decode($assigned_menu, true);    //assigned menu save function       
        //print_r($ass_menu_array);exit;
        if($storeid==0)                        //default menu case
        {
            $new_parent0=0;
            $new_positionp0=0;
            foreach ($ass_menu_array as $index => $ass_menu_parent0) 
            {             
                $menu_model_p0 = Menus::findOne($ass_menu_parent0['id']);
                //print_r($menu_model_p0);
                if($menu_model_p0->position!=$new_positionp0)
                {
                    $menu_model_p0->parent = $new_parent0;
                    $menu_model_p0->position = $new_positionp0;
                    $menu_model_p0->unAssigned= 0;
                    Menus::menuUpdateAllStores($menu_model_p0);
                }
                $menu_model_p0->parent = $new_parent0;
                $menu_model_p0->position = $new_positionp0;
                $menu_model_p0->unAssigned= 0;
                if($menu_model_p0['storeId']==-1) 
                {
                    Menus::menuInsert($menu_model_p0);
                }                        
                $menu_model_p0->storeId = $storeid; 
                $menu_model_p0->save();
                $new_positionp0++;
            
                $new_parent1=$ass_menu_parent0['id'];
                $new_positionp1=0;
                if(isset($ass_menu_parent0['children']))
                {
                    foreach ($ass_menu_parent0['children'] as $index => $ass_menu_parent1) 
                    {                 
                        $menu_model_p1 = Menus::findOne($ass_menu_parent1['id']);//var_dump($menu_model_p1);die;
                        if($menu_model_p1->position!=$new_positionp1)
                        {
                            $menu_model_p1->parent = $new_parent1;
                            $menu_model_p1->position = $new_positionp1; 
                            $menu_model_p1->unAssigned= 0;    
                            Menus::menuUpdateAllStores($menu_model_p1);
                        }
                        $menu_model_p1->parent = $new_parent1;
                        $menu_model_p1->position = $new_positionp1;
                        $menu_model_p1->unAssigned= 0;
                        if($menu_model_p1['storeId']==-1) 
                        {
                            Menus::menuInsert($menu_model_p1);
                        }
                        $menu_model_p1->storeId = $storeid; 
                        $menu_model_p1->save();

                        $new_positionp1++;                
                        $new_parent2=$ass_menu_parent1['id'];
                        $new_positionp2=0;
                        if(isset($ass_menu_parent1['children']))
                        {
                            foreach ($ass_menu_parent1['children'] as $index => $ass_menu_parent2) 
                            { 
                                $menu_model_p2 = Menus::findOne($ass_menu_parent2['id']); 
                                if($menu_model_p2->position!=$new_positionp2)
                                {
                                    $menu_model_p2->parent = $new_parent2;
                                    $menu_model_p2->position = $new_positionp2;
                                    $menu_model_p2->unAssigned= 0;
                                    //die('piii');
                                    Menus::menuUpdateAllStores($menu_model_p2);
                                }
                                $menu_model_p2->parent = $new_parent2;
                                $menu_model_p2->position = $new_positionp2;
                                $menu_model_p2->unAssigned= 0;
                                if($menu_model_p2['storeId']==-1) 
                                {
                                    Menus::menuInsert($menu_model_p2);
                                } 
                                
                                $menu_model_p2->storeId = $storeid;
                                $menu_model_p2->save();                            
                                $new_positionp2++;
                            }
                        }
                    }
                }

                $unass_menu_array = json_decode($unassigned_menu, true);    //unassigned menu save function       
                foreach ($unass_menu_array as $index => $unass_menu) 
                {
                    $menu_model_unass = Menus::findOne($unass_menu['id']);
                    //print_r($menu_model_unass);
                    if($menu_model_unass['storeId']==0) 
                        {
                            //$this->menuDelete($menu_model_unass);
                            Menus::menuDelete($menu_model_unass);
                        } 
                    //$menu_model_unass->storeId = -1;  
                    $menu_model_unass->parent = 0;
                    $menu_model_unass->position = 0;
                    $menu_model_unass->unAssigned =1;
                    $menu_model_unass->save();
                    if(isset($unass_menu['children']))
                    {
                        foreach ($unass_menu['children'] as $index => $unass_menu1) 
                        {
                            $menu_model_unass1 = Menus::findOne($unass_menu1['id']);
                            if($menu_model_unass1['storeId']==0) 
                            {
                                Menus::menuDelete($menu_model_unass1);
                            } 
                            //$menu_model_unass1->storeId = -1;  
                            $menu_model_unass1->parent = 0;
                            $menu_model_unass->position = 0;
                            $menu_model_unass->unAssigned =1;
                            $menu_model_unass1->save();
                            if(isset($unass_menu1['children']))
                            {
                                foreach ($unass_menu1['children'] as $index => $unass_menu2) 
                                {
                                    $menu_model_unass2 = Menus::findOne($unass_menu2['id']);
                                    if($menu_model_unass2['storeId']==0) 
                                    {
                                        Menus::menuDelete($menu_model_unass2);
                                    } 
                                    //$menu_model_unass2->storeId = -1;  
                                    $menu_model_unass2->parent = 0;
                                    $menu_model_unass->position = 0;
                                    $menu_model_unass->unAssigned =1;
                                    $menu_model_unass2->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            //var_dump($ass_menu_parent0);die();
            $new_parent0=0;
            $new_positionp0=0;
            foreach ($ass_menu_array as $index => $ass_menu_parent0)  //1st level menu
            {             
                $menu_model_p0 = Menus::findOne($ass_menu_parent0['id']);
                $menu_model_p0->storeId = $storeid;  
                $menu_model_p0->parent = $new_parent0;
                $menu_model_p0->position = $new_positionp0;
                $menu_model_p0->unAssigned= 0;
                $menu_model_p0->save();
                $new_positionp0++;
            
                $new_parent1=$ass_menu_parent0['id'];
                $new_positionp1=0;
                if(isset($ass_menu_parent0['children']))     //2nd level menu
                {
                    foreach ($ass_menu_parent0['children'] as $index => $ass_menu_parent1) 
                    {                 
                        $menu_model_p1 = Menus::findOne($ass_menu_parent1['id']);  
                        $menu_model_p1->storeId = $storeid;  
                        $menu_model_p1->parent = $new_parent1;
                        $menu_model_p1->position = $new_positionp1;
                        $menu_model_p1->unAssigned= 0;
                        $menu_model_p1->save();
                        $new_positionp1++;                
                        $new_parent2=$ass_menu_parent1['id'];
                        $new_positionp2=0;
                        if(isset($ass_menu_parent1['children']))    //3rd level menu
                        {
                            foreach ($ass_menu_parent1['children'] as $index => $ass_menu_parent2) 
                            { 
                                $menu_model_p2 = Menus::findOne($ass_menu_parent2['id']); 
                                $menu_model_p2->storeId = $storeid;   
                                $menu_model_p2->parent = $new_parent2;
                                $menu_model_p2->position = $new_positionp2;
                                $menu_model_p2->unAssigned= 0;
                                $menu_model_p2->save();                            
                                $new_positionp2++;
                            }
                        }
                    }
                }
                $unass_menu_array = json_decode($unassigned_menu, true); 
                   //unassigned menu save function   

                foreach ($unass_menu_array as $index => $unass_menu)  //1st level menu
                {
                    //echo $unass_menu;
                    $menu_model_unass = Menus::findOne($unass_menu['id']); 
                    //$menu_model_unass->storeId = -1;  
                    $menu_model_unass->parent = 0;
                    $menu_model_unass->position= 0;
                    $menu_model_unass->unAssigned= 1;
                    $menu_model_unass->save();
                    //var_dump($unass_menu['children']);die;
                    if(isset($unass_menu['children']))          //2nd level menu
                    {
                        foreach ($unass_menu['children'] as $index => $unass_menu1) 
                        { 
                            $menu_model_unass1 = Menus::findOne($unass_menu1['id']);
                            //var_dump($menu_model_unass1->id) ;die;
                            //$menu_model_unass1->storeId = -1;  
                            $menu_model_unass1->parent = 0;
                            $menu_model_unass1->position = 0;
                            $menu_model_unass1->unAssigned= 1;
                            $menu_model_unass1->save();
                            if(isset($unass_menu1['children']))     //3rd level menu
                            {
                                foreach ($unass_menu1['children'] as $index => $unass_menu2) 
                                { 
                                    $menu_model_unass2 = Menus::findOne($unass_menu2['id']);
                                    //var_dump($menu_model_unass1->id) ;die;
                                    $menu_model_unass2->storeId = -1;  
                                    $menu_model_unass2->parent = 0;
                                    $menu_model_unass2->position = 0;
                                    $menu_model_unass2->unAssigned= 1;
                                    $menu_model_unass2->save();
                                }
                            }
                        }
                    }
                }
            }
        } 
        //var_dump($storeid);die;  
        Yii::$app->getSession()->setFlash('success', 'The Menu has been Updated.');
        return $this->redirect(['index','sid'=>$storeid]);
        
    }
    
    

    /**
     * Deletes an existing Menus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        
        // if($model->storeId!=0)
        // { 
            $menus=Menus::find()->where(['parent'=>$model->id])->all();//var_dump($menus);die;
            if(isset($menus))
            {
                foreach ($menus as $key => $menu) {
                    if($menu->parent!=0)
                    {
                        $menus1=Menus::find()->where(['parent'=>$menu->id])->all();
                        if(isset($menus1))
                        {
                            foreach ($menus1 as $key => $menu1) {
                                $this->findModel($menu1->id)->delete();
                            }
                        }
                    }
                    $this->findModel($menu->id)->delete();
                }
            }
            $model->delete();
        //}
        Yii::$app->getSession()->setFlash('success', 'The Menu has been Deleted.');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Menus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCreatemenu()
    {
       Menus::demoMenucreate();
    }
    public function actionCreatesitemenu($id)
    {
        $store=Stores::find()->where(['id'=>$id,'isVirtual'=>0])->one();
        if(!empty($store))
        {
            Menus::storeMenucreate($id);
            Yii::$app->getSession()->setFlash('success', $store->title.' Menu has been Created Successfully!......');
            return $this->redirect(['index']);
        }
        else
        {
            Yii::$app->getSession()->setFlash('error', 'Sorry!....Please Check Your Store Id!......');
            return $this->redirect(['index']);
        }
    }
    public function actionUpdatemenu($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            die('Menu has been Updated Successfully.');
            // Yii::$app->getSession()->setFlash('success', 'The Menu has been Updated.');
            // return $this->redirect(['index']);
        } 
        if(\Yii::$app->request->isAjax){
            return $this->renderPartial('_form',compact('model'));
        }
        return $this->renderPartial('updatemenu', [ 'model' => $model, ]);
    }
}

