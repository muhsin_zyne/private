<?php

namespace backend\controllers;

use Yii;
use yii\db\Query;
use common\models\Banners;
use common\models\User;
use common\models\Stores;
use common\models\search\Banners as BannersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\web\UploadedFile;
use common\models\StoreBanners;
use common\models\Configuration;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
/**
 * BannersController implements the CRUD actions for Banners model.
 */
class BannersController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionIndex($id=NULL,$type=NULL)
    {
        $id=($id=='b2b') ? 'b2b' : 'b2c';
        $type=($type==NULL) ? 'b2c-main' : $type;
        $user = User::findOne(Yii::$app->user->id);
        $stores = [];
            if($user->roleId != "3") { 
                $stores = Stores::find()->all();
            }   
        $store_id=($user->roleId==1)?'0':$user->store->id;
        if($user->roleId == 1) {
            $banners_all=Banners::find()->where(['type' => $type,'isFacebook' => 0,'storeId'=>0])->all();
                  
        }
        else if($user->roleId == 3) {
            
            $hasMasterId = Banners::find()->where('masterId>0 and (storeId =0 or storeId = '.$store_id.')')->all();
            $masterIds = [];
            foreach ($hasMasterId as $master) {
               $masterIds[] = $master->masterId;
            }
            if(!empty($masterIds)){
                $banners_all=Banners::find()->where('type = "'.$type.'" and isFacebook=0 and (storeId =0 or storeId = '.$store_id.') and id NOT IN ('.implode($masterIds,',').')')->all();
            }
            else{
                $banners_all=Banners::find()->where('type = "'.$type.'" and isFacebook=0 and (storeId =0 or storeId = '.$store_id.')')->all();                
            }
            
            $store = ArrayHelper::map($user->store->storeBanners,'id','id');

        }  
        //var_dump($banner_all);die;
        $model = new Banners();
        //---- banners save
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/banner/images/"; 
        if (Yii::$app->request->post())
        {
            $face=(isset($_POST['Banners']['isFacebook']))?$_POST['Banners']['isFacebook']:0;
            if($face==1)
            { 
                $model_facebook= Banners::find()->where( ['isFacebook' => 1,'storeId' => Yii::$app->params['storeId']] )->one();
                if(isset($model_facebook))
                {
                    $model_facebook->path =$_POST['Banners']['facebook_link'];
                    $model_facebook->isFacebook=1;
                    $model_facebook->main_image="0";
                    $model_facebook->url_type="0";
                    $model_facebook->category="0";
                    $model_facebook->type="b2c-side";
                    $model_facebook->save();
                }
                else
                {
                    $model->path =$_POST['Banners']['facebook_link'];
                    $model->isFacebook=1;
                    $model->storeId=$store_id;              
                    $model->main_image="0";                    
                    $model->url_type="0";
                    $model->category="0";
                    $model->type="b2c-side";
                    $model->save();
                }
            }
            else
            {     
                if(UploadedFile::getInstance($model,'main_image')!= '')
                {
                    $image = UploadedFile::getInstance($model, 'main_image');                
                }        
                $model->path = $image->name;
                $size = filesize($image->tempName);            
                $ext = end((explode(".", $image->name)));
                $avatar = Yii::$app->security->generateRandomString().date('ymdhis').".{$ext}";
                $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                $path="/store/banner/images/".$avatar;
                //-- url type 
                if($_POST['Banners']['url_type']==1)
                    $model->html=$_POST['Banners']['link'];
                else     
                    $model->url=$_POST['Banners']['link'];
                //-- url type  end
                //-- ext check
                if($ext=='png')            
                    $img =imagecreatefrompng($image->tempName);  
                else if($ext=='jpg')
                    $img =imagecreatefromjpeg($image->tempName); 
                else
                    $img =imagecreatefromgif($image->tempName);
                $model->x=imagesx($img);
                $model->y=imagesy($img);
                //-- ext check end
                //-- dimension check
                $dimention=explode("X",$_POST['Banners']['category']);            
                $x=$dimention['0']; 
                $y=$dimention['1']; 

                //-- dimension check  
                //var_dump($x);var_dump($y); var_dump($x);var_dump($y);die;
                if($_POST['Banners']['type']!='' )
                {
                    if($model->x!=$x || $model->y!=$y)
                    { 
                        Yii::$app->getSession()->setFlash('error', 'Banner Image Size Exactly '.$_POST['Banners']['category']);
                        //return $this->render('index', [ 'model' => $model]);
                         return $this->redirect(['index','type'=>$type,'id'=>$id]);  
                    }

                }                  
                $model->type=$_POST['Banners']['type'];
                $model->storeId=$_POST['Banners']['storeId']; 
                $model->link=$_POST['Banners']['link'];
                $model->main_image=0;
                $model->category=0;
                $model->path=$path;
                if($model->save())
                {
                    $image->saveAs($savepath); 
                }
                else
                {
                    var_dump($model->geterrors());die;
                }
                Yii::$app->getSession()->setFlash('success', 'Banner Added Successfully!.....');
                return $this->redirect(['index','type'=>$type,'id'=>$id]);
            }
        }
        // banners save end

        return $this->render('index', compact('model','id','type','banners_all','store')); 

    }
    
    public function actionAssignbanners() 
    {   
        //var_dump($_POST['site']);die;
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == 1)
            $store_id = -1;
        else
            $store_id = $user->store->id;
        $type=$_POST['type'];
        $data_db = [];
        $sit_admin_array=$_POST['site']; 
            $query = new Query;
            $query  ->select(['StoreBanners.bannerId AS sbid'])->orderBy(['rand()' => SORT_ASC])->where(['storeId' => $store_id])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where(['type' => $type]);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $j=0;
            foreach ($data as $index => $banner) 
            {
                $data_db[$j]=$banner['sbid'];$j++;
            }
        $site_admin=explode(",",$sit_admin_array) ;
        $total=count($site_admin);
        //print_r($site_admin);print_r($data_db);exit;
        //var_dump($data_db);
        //var_dump($site_admin);
        $delete_array=array_diff($data_db,$site_admin);
        //var_dump($delete_array);
        foreach ($delete_array as $index => $delete_id) 
        {
           $store_delete= StoreBanners::find()->where( ['storeId' => $store_id,'bannerId' => $delete_id] )->one();  
           if(!empty($store_delete))
                $store_delete->delete();
        }
        for($i=0;$i<$total;$i++)
        {
            $storeadmin= StoreBanners::find()->where( ['storeId' => $store_id,'bannerId' => $site_admin[$i]] )->one(); 
            //var_dump($storeadmin);die();
            if($storeadmin=='')
            {
                $model= new StoreBanners();
                $model->storeId=$store_id;
                $model->bannerId=$site_admin[$i];
                $model->save();
            }
        }
        //var_dump($type);die;
        
        if( $store_id==-1)
            $id='b2b';
        else
            $id='b2c';
        //var_dump($type_tab);die;
        $model1= new Banners();
        Yii::$app->getSession()->setFlash('success', 'Data Saved Successfully!.....');
        return $this->redirect(['index','type'=>$type,'id'=>$id]);
        
    }

    /**
     * Displays a single Banners model.
     * @param integer $id
     * @return mixed
     */

    public function actionConfiguration()
    {
        //echo $_POST['store_id'];exit;
        $conf=Configuration::find()->where(['storeId' => $_POST['store_id'],'key' => 'is_sidebanner_facebook'])->one();       
        if($conf['id']!='')
        {
            $conf_model=Configuration::find()->where(['id' => $conf['id']])->one();
            $conf_model->value=$_POST['fac'];
            $conf_model->storeId=$_POST['store_id'];
            $conf_model->save();
        }
        else
        {
            $conf_model=new Configuration();
            $conf_model->key='is_sidebanner_facebook';
            $conf_model->value=$_POST['fac'];
            $conf_model->storeId=$_POST['store_id'];
            $conf_model->save();
        }
        Yii::$app->getSession()->setFlash('success', 'Defaut Side Banner Type Change Successfully!.....');
        return $this->redirect(['index','type'=>'b2c-side','id'=>'b2c']);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionMinibannersave()
    {
        //echo $_POST['bannerId'];
        $query = new Query;  // main banner
        $query->select(['StoreBanners.id AS sbid'])->where(['storeId' =>$_POST['store_id'],'position'=>$_POST['position'] ])->from('StoreBanners')
            ->join('LEFT OUTER JOIN', 'Banners','Banners.id=StoreBanners.bannerId')->where("type ='b2c-mini' and StoreBanners.position = ".$_POST['position']." and StoreBanners.storeId = ".$_POST['store_id']."");
        $command = $query->createCommand();
        $selected_banners = $command->queryAll();
        foreach ($selected_banners as $key => $minibanner) {
            //$minibanner['sbid'];
            $storebanner=StoreBanners::findOne($minibanner['sbid']);
            $storebanner->delete();
        }
        $model= new StoreBanners;
        $model->storeId=$_POST['store_id'];
        $model->position=$_POST['position'];
        $model->bannerId=$_POST['bannerId'];
        $model->save();
       
        Yii::$app->getSession()->setFlash('success', 'Saved!.....');
        return $this->redirect(['index','type'=>'mini']);

    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banners();
       

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
          
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$type) 
    {
        
        $user = User::findOne(Yii::$app->user->id);        
        $model = $this->findModel($id);      
        $oldPath=$model->path;              
        $x=$model->x;$y=$model->y;
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/banner/images/";
        if (Yii::$app->request->post())
        {
            if($user->roleId == "3") { //var_dump($id);
                if(!$model = Banners::find()->where(['id'=>$id,'storeId'=>$user->store->id])->one()){  //die('hai');
                    $old = $this->findModel($id);
                    $model = new Banners;
                    $model->storeId = $user->store->id;
                    $model->x = $old->x;
                    $model->y = $old->y;
                    $model->masterId = $old->id;
                    $model->target = Yii::$app->request->post()['Banners']['target'];
                }
            } 

            $image = UploadedFile::getInstance($model, 'path');
            if(!empty($image))
            {
                $ext = end((explode(".", $image->name)));
                $avatar = Yii::$app->security->generateRandomString().date('ymdhis').".{$ext}";
                $savepath = Yii::$app->params['uploadPath'] . $avatar;
                $path="/store/banner/images/".$avatar;                
                $model->path = $path;                   
                if($ext=='png')  //------------------check image type
                {
                    $img =imagecreatefrompng($image->tempName);   
                }
                else if($ext=='jpg')
                {
                    $img =imagecreatefromjpeg($image->tempName);   
                }
                else 
                {
                    $img =imagecreatefromgif($image->tempName);   
                }
                //var_dump($img);die;
                $model->x=imagesx($img);
                $model->y=imagesy($img);                
                if($model->x!=$x & $model->y!=$y)
                {   
                    $model->path=$oldPath;             
                    Yii::$app->getSession()->setFlash('error', 'Banner Image Size Exactly '.$x.'px'.' X '.$y.'px');
                    return $this->render('update',compact('model','type'));     
                }                
                
            } 
            else
            {    
                $model->path = $_POST['Banners']['demo_path'];
                //$model->thumb=$model->path;
            }
            if($_POST['Banners']['url_type']==1) 
            { 
                $model->url='';
                $model->html=$_POST['Banners']['link']; 
            }            
            else 
            { 
                $model->html=''; 
                $model->url=$_POST['Banners']['link'];
            }
            $model->type=$_POST['Banners']['type'];
            $model->image="0";$model->main_image="0"; 
            if(!empty($image))
            {
                if($model->save(false))
                {
                    $image->saveAs($savepath);
                    //die('hiii');
                }
            }
            else
            {
                $model->target = Yii::$app->request->post()['Banners']['target'];
                if($model->save(false)){
                    if (isset($old)) {
                        if($storeBanners = StoreBanners::find()->where(['bannerId'=>$old->id,'storeId'=>$user->store->id])->one()){
                            $storeBanners->bannerId = $model->id;
                            $storeBanners->save(false); 
                        }
                    }
                }
            }

            if ($user->roleId == "1") {  //var_dump($id);die();
                if($model = Banners::find()->where(['id'=>$id,'storeId'=>0])->one()){   
                    $siteBanners = Banners::find()->where(['masterId'=>$id])->all();
                    foreach ($siteBanners as $banner) {
                        if($subBanner = Banners::find()->where('id = "'.$banner["id"].'" and masterId = "'.$id.'"')->one()){ 
                            $subBanner->path = $model->path;
                            $subBanner->save(false);
                        }
                    }

                }        
            }   
            $id=(preg_match("/b2c/", $type))?'b2c':'b2b';           
            
            Yii::$app->getSession()->setFlash('success', 'Banner has been updated successfully!.....');
            return $this->redirect(['index','type'=>$type,'id'=>$id]);
             
        }     

         //return $this->render('update', ['model' => $model]);
        return $this->render('update',compact('model','type'));
    }

    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {        
        $delete_banner=Banners::find()->where( ['id' => $id] )->one();
        $store_banner_array=StoreBanners::find()->where( ['bannerId' => $delete_banner['id']] )->all();
        foreach ($store_banner_array as $index => $store_banner) 
        {
            $model_sb=StoreBanners::find()->where( ['id' => $store_banner['id']] )->one();
            $model_sb->delete();
        }
        $delete_path=Yii::$app->basePath."/../".$delete_banner['path'];
        if (file_exists($delete_path)) {
            //unlink($delete_path);
        }
        $this->findModel($id)->delete();
        
        $id=($delete_banner->type=='b2b-side' || $delete_banner->type=='b2b-mini' || $delete_banner->type=='b2b-main')?'b2b':'b2c';
        return $this->redirect(['index','id'=>$id,'type'=>$delete_banner->type]);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSortbanners(){
        $store_banners = $_POST['banners'];
        $store_id = $_POST['store_id'];
        foreach ($store_banners as $key => $value) {
            $banner= StoreBanners::find()->where( ['storeId' => $store_id,'bannerId' => $value])->one(); 
            $banner->position = $key;
           if($banner->update() != false)
                echo "Update successful". $banner->id;
            else
                echo "Unable to update";
        } 
    }
    }