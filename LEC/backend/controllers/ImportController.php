<?php

namespace backend\controllers;
use Yii;
use backend\models\ImportForm;
use common\models\Products;
use common\models\Attributes;
use yii\helpers\ArrayHelper;

class ImportController extends \app\components\AdminController
{

    public function actionIndex()
    {
    	$import = new ImportForm;
        return $this->render('index', compact('import'));
    }

    public function actionUpdate()
    {
        $import = new ImportForm;
        return $this->render('update', compact('import'));
    }

    public function actionCheck($scenario = "create"){
    	if(isset($_FILES['ImportForm'])){
    		$import = new ImportForm;
    		if($_FILES['ImportForm']['size']['file'] > 0){
    			$importer = Yii::$app->importer;
                $importer->renewUrlKeys = true;
    			$file = $_FILES['ImportForm']['tmp_name']['file'];
		        $importer->fileHandle = fopen($file,"r");
		        $rows = $importer->processCsv($importer->fileHandle);
                if($_POST['ImportForm']['entityType'] == "products")
		            $result = $importer->verifyProducts($rows, $scenario);
                elseif($_POST['ImportForm']['entityType'] == "customers")
                    $result = $importer->verifyCustomers($rows, $scenario);
		        $filePath = "";
                //var_dump($result); die;
                if($result['status']){ 
                    $time = date('YmdHis');
                    move_uploaded_file($file, "../data/csv/".$_POST['ImportForm']['entityType']."/".$time.".csv");
		        	//var_dump($file); die;
		        	$filePath = "../data/csv/".$_POST['ImportForm']['entityType']."/".$time.".csv";
		        }
		        return $this->render('validation', compact('result', 'import', 'filePath', 'scenario'));
    		}
    		else{
    			Yii::$app->getSession()->setFlash('error', 'CSV file is invalid or empty');
        		return $this->render('index', compact('import', 'scenario'));
    		}
    	}
    }

    public function actionProcess($filePath, $scenario = "create", $entityType = "products"){ //var_dump($entityType); die;
        $import = new ImportForm;
    	$importer = Yii::$app->importer;
        $importer->renewUrlKeys = true;
        $importer->fileHandle = fopen($filePath,"r");
        $rows = $importer->processCsv($importer->fileHandle);
        $result = [];
        // if($entityType == "products")
        //     $result = $importer->verifyProducts($rows, $scenario);
        if($entityType == "customers")
            $result = $importer->verifyCustomers($rows, $scenario);
        $importResult = [];
        $result['status'] = true;
    	if($result['status'] == true){
            if($entityType == "products"){
                if(isset($rows[0]['product_type']))
                    $rows = $importer->sortBy('product_type', $rows);
                $importResult = $importer->importProducts($rows, ($scenario=="update")? false : true);
            }elseif($entityType == "customers"){
                $importResult = $importer->importCustomers($rows, ($scenario=="update")? false : true);
            }die;
            if(empty($importResult)){
                Yii::$app->getSession()->setFlash('success', 'CSV file imported successfully');
            }
        }else{
            return $this->render('validation', compact('result', 'import', 'filePath', 'scenario'));
        }
        return $this->render('result', compact('importResult', 'import', 'scenario'));
    }

    public function actionFixconfigproducts(){
        $importer = Yii::$app->importer;
        $skus = "'ARFYF','CH10075','1818','1808PC','1822','ARGBN','1812','1604','1804','1802','1742','CH10079','CH10081','CH10078','CH10080','CH10077','WD406-C7','WD433-C7','WD443-7','WD10BNA-4','WD15-6','WD182-7','WD15-4','WD10BGP-6','WD10BGP-4','8805','8513','8391','8798','8384','8405','8357','8715','8514','8515','8605','8569','8616','8630','8624','8568','8559','8539','8532','8540','8555','8556','8774','5280','8808','8625','8771','8768','8772','8776','8779','8641','8644','8716','8708','8737','8750','8739','8705','8645','8660','8680','8694','8682','8764'";
        $products = Products::find()->where("sku in ($skus)")->all();
        $importer->fileHandle = fopen("../data/csv/products/configurable_rings_28092015.csv", "r");
        $rows = $importer->processCsv($importer->fileHandle);
        foreach($products as $product){
            $row = $importer->findRowBy('sku', $product->sku, $rows);
            $children = explode(",", $row['sub_products']);
            //var_dump($children); die;
            $optionCache = []; 
            foreach($children as $sku){
                $sku = trim($sku);
                if($subProductRow = $importer->findRowBy('sku', $sku, $rows)){
                    if(!$subProduct = Products::find()->where(compact('sku'))->one()){
                        $subProduct = new Products;
                        $subProduct->sku = $sku;
                        $subProduct->typeId = "simple";
                        $subProduct->brandId = $product->brandId;
                        $subProduct->attributeSetId = $product->attributeSetId;
                        $subProduct->visibility = "not-visible-individually";
                        $subProduct->createdBy = "0";
                        $subProduct->save(false);
                        $value = new \common\models\AttributeValues;
                        $value->value = $subProductRow["f_size"];
                        $value->productId = $product->id;
                        $value->attributeId = "35";
                        $value->storeId = "0";
                        $value->save(false);
                    }
                    $subProductCache[$product->id][$subProduct->id] = $subProduct;
                    $linkage = new \common\models\ProductLinkages;
                    $linkage->parent = $product->id;
                    $linkage->productId = $subProduct->id;
                    $linkage->type = "configurable";
                    $linkage->save(false);
                    $subProduct->visibility = "not-visible-individually";
                    $subProduct->status = '1';
                    $subProduct->save(false);
                    $optionPrice = new \common\models\AttributeOptionPrices;
                    $optionPrice->productId = $product->id;
                    $optionPrice->attributeId = "35";
                    //$optionId = 0;
                    $optionId = $subProduct->getOptionId(35, $subProductRow["f_size"]);
                    if(!$optionId || in_array($optionId, $optionCache))
                        continue;
                    $optionCache[] = $optionId;
                    $optionPrice->optionId = $optionId;
                    $optionPrice->price = '0.00';
                    $optionPrice->type = 'fixed';
                    $optionPrice->save(false);
                }else{
                    die('subProduct with sku: '.$sku.' not found');
                }
            }
            $superAttr = new \common\models\ProductSuperAttributes;
            $superAttr->productId = $product->id;
            $superAttr->attributeId = '35';
            $superAttr->save(false);
        }
    }
}
