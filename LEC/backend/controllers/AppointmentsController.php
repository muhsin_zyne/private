<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;
use kartik\date\DatePicker;

class AppointmentsController extends AdminController
{
	public function actionIndex(){
		if(isset($_GET['date'])){
			$date = new \DateTime($_GET['date']);
			$currentDate = $date->format('Y-m-d');
		}
		else
  			$currentDate = date('Y-m-d');

  		$user = User::findOne(Yii::$app->user->id);
		$appointment = new Appointments(); 
		$storeId = $user->store->id;
		$weekDay = lcfirst(date('D',strtotime($currentDate)));
		//$currentDateSetting = AppointmentDefaultSettings::find()->select($weekDay)->where(['storeId'=>$storeId])->one();
		if($currentDateSetting = AppointmentDefaultSettings::find()->where(['storeId'=>$storeId])->one()) {
			$workingHours = json_decode($currentDateSetting->$weekDay,true);
			foreach ($workingHours as $key => $slots) {
				$timeSlots = $slots;
			}
			if($currentDate < $currentDateSetting->updatedOn){

			}
		}
		else{
			$timeSlots = ['9:00 am - 10:00 am','10:00 am - 11:00 am','11:00 am - 12:00 pm','12:00 pm - 1:00 pm','1:00 pm - 2:00 pm','2:00 pm - 3:00 pm','3:00 pm - 4:00 pm','4:00 pm - 5:00 pm','5:00 pm - 6:00 pm'];
		}	
		
		$currentDateAppointments = Appointments::find()->where('storeId = "'.$storeId.'" and startTime like "%'.$currentDate.'%"')->all();
		return $this->render('index',compact('currentDate','currentDateSetting','timeSlots','currentDateAppointments','appointment'));
	}

	public function actionUpdate(){  
		if(isset($_GET['date'])){
			$selectedDate = $_GET['date'];
			$date = new \DateTime($_GET['date']);
			$currentDate = $date->format('Y-m-d');
		}
		else{
			$selectedDate = date('d-M-Y');
  			$currentDate = date('Y-m-d');
		}

		

		$user = User::findOne(Yii::$app->user->id);
		$storeId = $user->store->id;

		/*$user = User::findOne(Yii::$app->user->id);
		$storeId = $user->store->id;
		$weekDay = lcfirst(date('D',strtotime($currentDate)));
		$currentDateSetting = AppointmentDefaultSettings::find()->select($weekDay)->where(['storeId'=>$storeId])->one();
		$workingHours = json_decode($currentDateSetting->$weekDay,true);
		foreach ($workingHours as $key => $slots) {	
			$timeSlots = $slots;
		}
		$currentDateAppointments = Appointments::find()->where('storeId = "'.$storeId.'" and startTime like "%'.$currentDate.'%"')->all();*/
		if(isset($_POST['appointments'])){
			foreach ($_POST['appointments']["slots"] as $key => $value) {
				$splitSlots = explode(" - ", $key);
				$startDate = new \DateTime($_POST['appointments']['bookingDate'].$splitSlots[0]);
		        $endDate = new \DateTime($_POST['appointments']['bookingDate'].$splitSlots[1]);
		        $startTime = $startDate->format('Y-m-d H:i:s');
		        $endTime = $endDate->format('Y-m-d H:i:s');
		        if($value == "unavailable"){
			        if(!$appointment=Appointments::find()->where(['storeId'=>$storeId,'startTime'=>$startTime,'status'=>$value])->one()){
			            $appointment = new Appointments();
				        $appointment->startTime = $startTime;
				        $appointment->endTime = $endTime;
						$appointment->status = $value;
						$appointment->storeId = $storeId;
						$appointment->save(false);
					}
				}	
				elseif($value == "available"){
					if($appointment=Appointments::find()->where(['storeId'=>$storeId,'startTime'=>$startTime,'status'=>'unavailable'])->one()){
						$appointment->delete();
					}	
				}	
			}	
			return $this->redirect(['appointments/index','date'=>$selectedDate]);
		}
	}
	

	public function actionCreate(){
		$appointment = new Appointments();
		$user = User::findOne(Yii::$app->user->id);
		$storeId = $user->store->id;
		/*$currentDate = date('Y-m-d');
		$weekDay = lcfirst(date('D',strtotime($currentDate)));
		$user = User::findOne(Yii::$app->user->id);
		$storeId = $user->store->id;
		$currentDateSetting = AppointmentDefaultSettings::find()->select($weekDay)->where(['storeId'=>$storeId])->one();
		$defaultSetting = AppointmentDefaultSettings::find()->where(['storeId'=>$storeId])->one();
		$timeSlots = ['9:00 am - 10:00 am','10:00 am - 11:00 am','11:00 am - 12:00 pm','12:00 pm - 1:00 pm','1:00 pm - 2:00 pm','2:00 pm - 3:00 pm','3:00 pm - 4:00 pm','4:00 pm - 5:00 pm','5:00 pm - 6:00 pm'];
		$currentAppointments = Appointments::find()->where(['storeId'=>$storeId])->all(); */
		if ($appointment->load(Yii::$app->request->post())) {
			if(\Yii::$app->request->isAjax){  
				$splitSlots = explode(" - ", $_POST['Appointments']['bookingSlot']);
				$startTime = new \DateTime($_POST['Appointments']['bookingDate'].$splitSlots[0]);
	            $endTime = new \DateTime($_POST['Appointments']['bookingDate'].$splitSlots[1]);
	            $appointment->startTime = $startTime->format('Y-m-d H:i:s');
	            $appointment->endTime = $endTime->format('Y-m-d H:i:s');
	            $appointment->status = "booked";
				$appointment->storeId = $storeId;
				if($appointment->save(false)) {
					$appointment->sendAppointmentConfirmation();
                	$appointment->sendAppointmentNotification();
                	die('The slot is booked successfully.');
                }	
				//if($appointment->save(false)) { 
				//return $this->render('index',compact('defaultSetting','currentDate','weekDay','currentDateSetting','timeSlots','currentAppointments'));
				//}
			}
			else{
				$date = $_POST['appointments']["bookingDate"];
				$timeSlot = $_POST['appointments']["timeslot"];
				var_dump($_POST['appointments']["slots"]);die(); 
			}		
		}
		else {

            if(\Yii::$app->request->isAjax){
                return $this->renderAjax('_form',compact('appointment'));
            }
            else {    
                return $this->render('create', [
                'appointment' => $appointment,
                ]);
            } 
        }
			
	}

	public function actionDelete($id){
		$appointment = $this->findModel($id);
		$appointment->delete();
		return $this->redirect(['appointments/index']);

	}

	public function actionView($id){
		$appointment = $this->findModel($id);
		if(\Yii::$app->request->isAjax){
			return $this->renderAjax('_view',compact('appointment'));
		}	
	}

	protected function findModel($id)
    {
        if (($appointment = Appointments::findOne($id)) !== null) {
            return $appointment;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}	
?>

