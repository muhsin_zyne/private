<?php

namespace backend\controllers;

use Yii;
use app\components\AdminController;
use common\models\User;
use common\models\Stores;
use common\models\Configuration;

class SocialMediaController extends AdminController
{
    public function actionIndex()
    {
    	$session = Yii::$app->session;
        $social = new Configuration();
        $user = User::findOne(Yii::$app->user->id);
		$stores = [];
        if($user->roleId != "3") 
            $stores = Stores::find()->all(); 
        if($user->roleId == "3") 
            $storeId = $user->store->id;
        else { 
            if(isset($session['storeId'])){
                   $storeId = $session['storeId'];
            }  
            else { $storeId = 1; } 
        }

        if ($social->load(Yii::$app->request->post())) {

            if(isset($_POST["Configuration"]["key"])){
				foreach ($_POST["Configuration"]["key"] as $key=>$socialmedia) { 

                    if (!Configuration::find()->where(['storeId' => $storeId, 'key' => $key])->one()) {
	    				$socialMedia = new Configuration();
	    				$socialMedia->key = $key;
	    				$socialMedia->value = $socialmedia;
	    				$socialMedia->storeId = $storeId;
	    				$socialMedia->save(false);	    			
    				}
    				else{
    					$socialMedia = Configuration::findOne(['key' => $key, 'storeId' => $storeId]);
	    				$socialMedia->key = $key;
	    				$socialMedia->value = $socialmedia;
	    				$socialMedia->storeId = $storeId;
	    				$socialMedia->save(false);	 	
    				}
    			}

            }

            $facebook = Configuration::findSetting('facebook',$storeId);
            $twitter = Configuration::findSetting('twitter',$storeId);
            $linkedin = Configuration::findSetting('linkedin',$storeId);
            $instagram = Configuration::findSetting('instagram',$storeId);
            $google = Configuration::findSetting('google',$storeId);
            $youTube = Configuration::findSetting('youTube',$storeId);
            $pinterest = Configuration::findSetting('pinterest',$storeId);   

            return $this->render('index',compact('facebook','twitter','linkedin','instagram','google','youTube','pinterest','user','stores','storeId'));

        }
        else{

            $facebook = Configuration::findSetting('facebook',$storeId);
            $twitter = Configuration::findSetting('twitter',$storeId);
            $linkedin = Configuration::findSetting('linkedin',$storeId);
            $instagram = Configuration::findSetting('instagram',$storeId);
            $google = Configuration::findSetting('google',$storeId);
            $youTube = Configuration::findSetting('youTube',$storeId);
            $pinterest = Configuration::findSetting('pinterest',$storeId);
        	
        	return $this->render('index',compact('facebook','twitter','linkedin','instagram','google','youTube','pinterest','user','stores','social','storeId'));
		}	
    }    
		
    

}
