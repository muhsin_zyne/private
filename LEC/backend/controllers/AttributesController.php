<?php

namespace backend\controllers;

use Yii;
use common\models\Attributes;
use common\models\search\AttributesSearch;
use common\models\AttributeOptions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
//use common\models\search\AttributesSearch;

/**
 * AttributesController implements the CRUD actions for Attributes model.
 */
class AttributesController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attributes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Attributes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Attributes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     
    public function actionCreate(){  
        $model = new Attributes();
        $input_type_dropdown=0;   


        if(isset($_POST['Attributes'])){
            if($_POST['Attributes']['attributeGroupId']=='1'){
                $model->applyToSimple = 1;
                $model->applyToBundle = 1;
                $model->applyToGrouped = 1;
                $model->applyToConfigurable = 1;
                $model->applyToVirtual = 1;
                $model->applyToDownloadable = 1;
            }
            else {
                $model->applyToSimple = 0;
                $model->applyToBundle = 0;
                $model->applyToGrouped = 0;
                $model->applyToConfigurable = 0;
                $model->applyToVirtual = 0;
                $model->applyToDownloadable = 0;
                $sel_pro_type=$_POST['Attributes']['productTypeApplicable'];
                if($sel_pro_type!=''){
                    $sel_pro_type_count=count($sel_pro_type)-1;
                    for($i=0;$i<=$sel_pro_type_count;$i++){                    
                        if($sel_pro_type[$i]=='Simple'){$model->applyToSimple = 1;}
                        if($sel_pro_type[$i]=='Bundle'){$model->applyToBundle = 1;} 
                        if($sel_pro_type[$i]=='Grouped'){$model->applyToGrouped = 1;}
                        if($sel_pro_type[$i]=='Configurable'){ $model->applyToConfigurable = 1;}
                        if($sel_pro_type[$i]=='Virtual'){$model->applyToVirtual = 1;}
                        if($sel_pro_type[$i]=='Downloadable'){$model->applyToDownloadable = 1;}             
                    }
                }   
            }
            $model->required=$_POST['Attributes']['required'];
            $model->configurable=$_POST['Attributes']['configurable'];
            $model->scope=$_POST['Attributes']['scope'];
            $model->validation=$_POST['Attributes']['validation'];            
            if($_POST['Attributes']['field']=='dropdown' || $_POST['Attributes']['field']=='multiselect'){
                $input_type_dropdown=1;
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           
            if($input_type_dropdown==1 && isset($_POST['AttributeOptions']['value'])){
                foreach ($_POST['AttributeOptions']['value'] as $key => $attr_value) {
                    //echo $_POST['AttributeOptions']['value'][$i-1];
                    $model1= new AttributeOptions();
                    $model1->value=$attr_value;
                    $model1->attributeId=$model->id;
                    $model1->save();
                }
                // exit;
            }
            return $this->redirect(['index']);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
         
    }

    /**
     * Updates an existing Attributes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $input_type_dropdown=0;  
        if(isset($_POST['Attributes'])){         
            if($_POST['Attributes']['attributeGroupId']=='1'){
                $model->applyToSimple = 1;
                $model->applyToBundle = 1;
                $model->applyToGrouped = 1;
                $model->applyToConfigurable = 1;
                $model->applyToVirtual = 1;
                $model->applyToDownloadable = 1;
            }
            else {
                $sel_pro_type=$_POST['Attributes']['productTypeApplicableUpdate'];          
                if($sel_pro_type!=''){
                    $model->applyToSimple = 0;
                    $model->applyToBundle = 0;
                    $model->applyToGrouped = 0;
                    $model->applyToConfigurable = 0;
                    $model->applyToVirtual = 0;
                    $model->applyToDownloadable = 0;
                    $sel_pro_type_count=count($sel_pro_type)-1;
                    for($i=0;$i<=$sel_pro_type_count;$i++){
                        if($sel_pro_type[$i]=='Simple'){$model->applyToSimple = 1; }
                        if($sel_pro_type[$i]=='Bundle'){$model->applyToBundle = 1; }
                        if($sel_pro_type[$i]=='Grouped'){$model->applyToGrouped = 1; }
                        if($sel_pro_type[$i]=='Configurable'){$model->applyToConfigurable = 1;}
                        if($sel_pro_type[$i]=='Virtual'){$model->applyToVirtual = 1;}
                        if($sel_pro_type[$i]=='Downloadable'){$model->applyToDownloadable = 1;}
                    }
                }   
            }
            $model->required=$_POST['Attributes']['required'];
            $model->configurable=$_POST['Attributes']['configurable'];
            $model->scope=$_POST['Attributes']['scope'];
            $model->validation=$_POST['Attributes']['validation']; 
            if($_POST['Attributes']['field']=='dropdown' || $_POST['Attributes']['field']=='multiselect'){
                $input_type_dropdown=1;
            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()){
            if($input_type_dropdown==1){ 
                $att_op_values=$_POST['AttributeOptions']['value']; 
                //var_dump($att_op_values);die;
                foreach ($att_op_values as $index => $option){ 
                    if(!$attributeOption = AttributeOptions::find()->where(['value' => $option, 'attributeId' => $model->id])->one()){ //die($option);
                        $attributeOption = new AttributeOptions;
                    }
                    $attributeOption->value=$option;
                    if($attributeOption->value!=''){
                        $attributeOption->attributeId=$model->id;
                        $attributeOption->save();
                    }
                }
            }
            return $this->redirect(['index']);
        } 
        else {
            return $this->render('update', ['model' => $model,]);                    
        }
    }

    /**
     * Deletes an existing Attributes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id){    
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    public function actionDeleteAttributeOption(){    
        $id=$_POST['id'];
        $attibutesoption=AttributeOptions::findOne($_POST['id']);
        $attibutesoption->delete(false);
        return $id;
    }

    /**
     * Finds the Attributes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attributes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attributes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
