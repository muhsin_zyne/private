<?php

namespace backend\controllers;

use Yii;
use common\models\SalesComments;
use common\models\Orders;
use common\models\search\SalesCommentsPagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\data\ActiveDataProvider;
/**
 * SalesCommentsController implements the CRUD actions for SalesComments model.
 */
class SalesCommentsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SalesComments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SalesCommentsPagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SalesComments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SalesComments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new SalesComments();        
        //var_dump(Yii::$app->request->post());die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $order=Orders::findOne($model->orderId);  
            $order->addActivity('message-sent', $model);
            Yii::$app->getSession()->setFlash('success', 'Message Successfully Sent!.');           
            if($_POST['SalesComments']['notify']==1){
               $model->sendSalesCommentNotificationMail();
            }
            if($model->type=='order'){            
                return $this->redirect(['orders/view', 'id' => $model->typeId]);
            }
            /*else if($model->type=='invoice')
            {
                return $this->redirect(['invoices/view', 'id' => $model->typeId]);
            }
            else if($model->type=='shipping')
            {
                return $this->redirect(['shipments/view', 'id' => $model->typeId]);
            }
            else if($model->type=='creditmemo')
            {
                return $this->redirect(['credit-memos/view', 'id' => $model->typeId]);
            }
            else if($model->type=='delivery')
            {
                return $this->redirect(['delivery/view', 'id' => $model->typeId]);
            }*/
            else{
                echo 'error';
            }
        } else {
            //var_dump($model->geterrors());die;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SalesComments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SalesComments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SalesComments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SalesComments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SalesComments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
