<?php

namespace backend\controllers;

use Yii;
use common\models\Downloads;
use common\models\search\DownloadsSaerch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\web\UploadedFile;
/**
 * DownloadPageController implements the CRUD actions for DownloadPages model.
 */
class DownloadsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DownloadPages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DownloadsSaerch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DownloadPages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DownloadPages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Downloads();
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/download/file/";
        if ($model->load(Yii::$app->request->post())) {
            $file=UploadedFile::getInstance($model, 'filePath');
            $ext1 = explode(".", $file->name);
            $ext=$ext1['1'];
            //$ext = end((explode(".", $file->name)));
            $avatar = Yii::$app->security->generateRandomString().".{$ext}";
            $savepath = Yii::$app->params['uploadPath'] . $avatar; 
            $filepath="/store/download/file/".$avatar;
            $model->filePath=$filepath;            
            if($model->save())
            {
                $file->saveAs($savepath);
            }
            Yii::$app->getSession()->setFlash('success', 'The Page Created Successfully!.');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DownloadPages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //var_dump($model->filePath);die;
        $oldFilePath=$model->filePath;
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/download/file/";
        if ($model->load(Yii::$app->request->post())) {
            //var_dump($_POST);die;
            $file=UploadedFile::getInstance($model, 'filePath');
            $model->filePath=$oldFilePath;
            //var_dump($file);die;
            if(!empty($file)){               
                //$ext1 = explode(".", $file->name);
                //$ext=$ext1['1'];
                $ext = end((explode(".", $file->name)));
                $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                $filepath="/store/download/file/".$avatar; 
                $model->filePath=$filepath;
            }   
            if($model->save()){
                if(!empty($file)){ 
                    $file->saveAs($savepath);
                }
                Yii::$app->getSession()->setFlash('success', 'The Page Updated Successfully.');
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DownloadPages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DownloadPages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DownloadPages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Downloads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
