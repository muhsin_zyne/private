<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {   $searchModel = new \common\models\search\UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $condition = (Yii::$app->user->identity->roleId == '3')? ['storeId' => Yii::$app->user->identity->store->id, 'roleId' => 4] : ['roleId' => 4];
        $dataProvider->query->andWhere($condition);
        $user = new \common\models\User;
        return $this->render('index', compact('dataProvider', 'searchModel', 'user'));
    }
    

public function actionExport(){
        $searchModel = new \common\models\search\UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $condition = (Yii::$app->user->identity->roleId == '3')? ['storeId' => Yii::$app->user->identity->store->id, 'roleId' => 4] : ['roleId' => 4];
        $dataProvider->query->andWhere($condition);
        $columns = [['class' => 'yii\grid\SerialColumn'],'firstname','lastname','email:email',['attribute' => 'customerStore.title', 'label' => 'Store', 'visible' => Yii::$app->user->identity->roleId != 3],['attribute' => 'created_at','value' => function($model){ return \backend\components\Helper::date($model->created_at); }]];
        $dataProvider->pagination = false;
	return \common\components\CSVExport::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]);
    }



    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);
        if($user->roleId == "4" && (Yii::$app->user->identity->roleId == "1" || $user->storeId == Yii::$app->user->identity->store->id)){
            $user->delete();
            Yii::$app->session->setFlash('success', 'User successfully deleted');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetstore(){
        $session = Yii::$app->session;

        /*$session['storeId'] = $_REQUEST["storeId"];
        return $session['storeId'];*/

        Yii::$app->session['storeId'] = $_REQUEST["storeId"];
        return $_REQUEST["storeId"];

    }
}
