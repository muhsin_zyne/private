<?php

namespace backend\controllers;

use Yii;
use common\models\Products;
use common\models\TestProducts;
use common\models\AttributeGroups;
use common\models\ProductCategories;
use common\models\ProductInventory;
use common\models\ProductImages;
use common\models\ProductSuperAttributes;
use common\models\ProductLinkages;
use common\models\Bundles;
use common\models\Stores;
use common\models\StoreProducts;
use common\models\User;
use common\models\Brands;
use common\models\BundleItems;
use common\models\AttributeValues;
use common\models\Attributes;
use common\models\AttributeOptionPrices;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\ProductMetaDetails;
use backend\models\Model;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends AdminController
{
    public $uploadPath;
    public $productAttrs = ['attributeSetId', 'typeId', 'id', 'brandId', 'sku', 'dateAdded', 'createdBy', 'urlKey', 'status', 'visibility'];
    public $b2cAdminEditableAttrs = ["price","special_price","special_from_date","special_to_date","byod_only"];
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'test', 'createuser'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function init(){
        $this->uploadPath = \Yii::$app->basePath . '/uploads/';
        parent::init();
    }

    public function actionIndex()
    {
        $searchModel = new \common\models\search\ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);//var_dump($dataProvider->query->where); die;
        $this->title = "Products";
        //$featured = Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->join('JOIN','StoreProducts as sp','sp.productId = Products.id')->where('av.storeId = '.Yii::$app->user->identity->store->id.' and a.code = "featured"')->all();
        $enabledProducts = [];
        $user = Yii::$app->user->identity;
        if($user->roleId == "3") {
            $store = $user->store;
            $enabledProducts = StoreProducts::findAll(['enabled'=>'1','type'=>'b2c','storeId'=>$store->id]);
            $storeProducts = implode(",", ArrayHelper::map(Products::find()->all(), 'id', 'id'));
            $featured = ArrayHelper::map(Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->join('JOIN','StoreProducts as sp','sp.productId = Products.id')->where('av.storeId = '.Yii::$app->user->identity->store->id.' and a.code = "featured" and av.value=1')->all(), 'id', 'id');
            //var_dump($storeProducts); die;
            $dataProvider->query->andWhere("Products.id in (".(($storeProducts=="")? "0" : $storeProducts).")");
        }
        
        return $this->render('index', compact('searchModel', 'dataProvider','enabledProducts','featured'));

    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($copyFrom = false)
    {

        $stores = $storeProducts = $productImages = $categoryIds = [];
        $product_meta= [new ProductMetaDetails()];
        $user = Yii::$app->user->identity;
        if($user->roleId == "3") { 
            $storeId = $user->store->id;
        }  
        else { 
            $storeId = 0; 
        } 
        if(!$oldProduct = Products::findOne($copyFrom))
            $product = new Products();
        else{ //duplication
            $product = clone $oldProduct;
            $product->sku = $product->urlKey = "";
            $_REQUEST['Products']['typeId'] = $product->typeId;
            $_REQUEST['Products']['attributeSetId'] = $product->attributeSetId;
            if($user->roleId != "3") { 
                $storeProducts = ArrayHelper::map(StoreProducts::findAll(['productId'=>$product->id,'type'=>'b2c']),'id','storeId');
            }
            $product_meta = $product->meta;
            $product_meta =(empty($product_meta)) ? [new ProductMetaDetails()] : $product_meta;
            $productImages = $product->productImages;
            $product->duplicate = true;
            $categoryIds = ArrayHelper::map(ProductCategories::find()->andWhere("(storeId = '0' or StoreId = $storeId) and productId = $product->id")->all(),'categoryId','storeId');
            if($product->typeId=="configurable"){ 
                $_REQUEST['Products']['ProductSuperAttributes'] = ArrayHelper::map($product->superAttributes, 'id', 'attributeId');
                $availableChildProducts = new ArrayDataProvider([
                    'allModels' => $product->attributeSet->getAvailableChildProducts(ArrayHelper::map($product->superAttributes,'attributeId','attributeId')),
                    'pagination'=> ['defaultPageSize' => 10]
                ]);
            }
        }
        
        //$productImages = new ProductImages();
        $dataProvider = new ActiveDataProvider([
            'query' => Products::find() //$search->search(Yii::$app->request->queryParams);

            ]);


        if($user->roleId != "3") { 
            $stores = Stores::find()->all();
        }

        if((\Yii::$app->request->isAjax && isset($_GET['validate'])) || $copyFrom){
            // continue till validation
        }elseif(!\Yii::$app->request->isAjax && !isset($_REQUEST['Products']['typeId']))
            $this->redirect(\yii\helpers\Url::to(['/products']));
        elseif(\Yii::$app->request->isAjax && !isset($_REQUEST['Products']['typeId'])){
            return $this->renderAjax('_settings',compact('product'));
        }elseif(\Yii::$app->request->isAjax && $_REQUEST['Products']['typeId']=="configurable" && !isset($_REQUEST['_pjax'])){
            $product->load(Yii::$app->request->post());
            return $this->renderAjax('_configattributes',compact('product'));

        }
        $productDataPosted = isset($_POST['Products'])? $_POST['Products'] : [];
        unset($productDataPosted['attributeSetId'], $productDataPosted['typeId']);

        if ($product->load(Yii::$app->request->post()) && !empty($productDataPosted)) {  //var_dump(Yii::$app->request->post());die();
                //var_dump($this->productAttrs);die('hai');// submission of the actual product form
           
            $productAttrs = $this->productAttrs;

            $customAttrs = array_diff($product->attributes(), $productAttrs); // filtering the custom attributes
            foreach($customAttrs as $attr)
                $attributes[$attr] = $product->getAttributes()[$attr];
            $product->createdBy = (Yii::$app->user->identity->roleId == "1")? "0" : Yii::$app->user->id;
            $_REQUEST['bypassStoreProductsCheck'] = true;
	    if(\Yii::$app->request->isAjax && isset($_GET['validate'])){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($product);
            }
            if($product->save(true, $productAttrs)){// saving only the columns in Products table
		        $storeId = (Yii::$app->user->identity->roleId=="3")? $user->store->id : "0";
                if($product->saveCustomAttributes($attributes, $storeId)){
                    $product_meta = Model::createMultiple(ProductMetaDetails::classname()); // product meta starts here
                    Model::loadMultiple($product_meta, Yii::$app->request->post());
                    // validate all models
                    $valid = Model::validateMultiple($product_meta);
                    if ($valid && !empty($product_meta)) {
                         //   $transaction = \Yii::$app->db->beginTransaction();
                        try {
                                foreach ($product_meta as $meta) {
                                    if(!empty($meta->key) || !empty($meta->value)) {
                                        $meta->id = null;
                                        $meta->productId = $product->id;
                                        $meta->save(false);
                                          // $transaction->rollBack();
                                          //break;
                                    }
                                }   
                        } catch (Exception $e) {
                            //$transaction->rollBack();
                        }
                    }
                    if(isset($_POST['kv-products'])){ //var_dump($_POST['kv-products']);die();
                        if(!empty($_POST['kv-products'])) { //var_dump($_POST['kv-products']);die();
                            $catids = ArrayHelper::map($product->categories, 'id', 'categoryId');
                            foreach(explode(",", $_POST['kv-products']) as $category){
                                if($category != NULL){
                                    if(!in_array($category, array_values($catids))){  //var_dump($category);die('category');
                                        $productCat = new ProductCategories;
                                        $productCat->categoryId = $category;
                                        $productCat->productId = $product->id;
                                        $productCat->storeId = $storeId;
                                        $productCat->save(false);
                                    }
                                }    
                            }
                            $removed = array_diff(array_values($catids), explode(",", $_POST['kv-products']));
                            if(!empty($removed))
                                ProductCategories::deleteAll("productId='".$id."' and categoryId in (".implode(",", $removed).")");
                        }    
                    }
                  
                    if (isset($_POST['Products']['rel_product_id'])) {
                        foreach ($_POST['Products']['rel_product_id'] as $rel_product_id) {
                            $Related_product = new \common\models\RelatedProducts;
                            $Related_product->relatedProductId = $rel_product_id;
                            $Related_product->productId = $product->id;
                            $Related_product->storeId = $storeId;
                            $Related_product->save(false);
                        }
                    }

                    if(isset($_POST['Products']['stores'])){ 
                        foreach ($_POST['Products']['stores'] as $store) {
                            $storeProduct = new StoreProducts;
                            $storeProduct->storeId = $store;
                            $storeProduct->productId = $product->id;
                            $storeProduct->type = 'b2c';
                            $storeProduct->save(false);
                        }
                    }
                    elseif(isset($user->store)) {
                        $store = $user->store;
                        $storeProduct = new StoreProducts;
                        $storeProduct->storeId = $store->id;
                        $storeProduct->productId = $product->id;
                        $storeProduct->type = 'b2c';
                        $storeProduct->save(false);

                    }  
                    

                    if(isset($_POST['ProductInventory'])){

                        $productInventory = new ProductInventory;
                        $productInventory->productId = $product->id;
                        $productInventory->storeId = $storeId;
                        $productInventory->quantity = $_POST['ProductInventory']['quantity'];
                        $productInventory->minQuantity = $_POST['ProductInventory']['minQuantity'];
                        $productInventory->manageStock =  $_POST['ProductInventory']['manageStock']; 
                        $productInventory->isInStock = $_POST['ProductInventory']['isInStock'];
                        $productInventory->save(false);

                    }

                    if(isset($_POST['Products']['images'])){ 

                        if(isset($_POST['Products']['images']['thumbnail'])){
                            $thumbnailImageIndex =$_POST['Products']['images']['thumbnail'];
                        } 

                        if(isset($_POST['Products']['images']['small_image'])){
                            $smallImageIndex =$_POST['Products']['images']['small_image'];
                        } 

                        if(isset($_POST['Products']['images']['base_image'])){
                            $baseImageIndex =$_POST['Products']['images']['base_image'];
                        } 

                        foreach ($_POST['Products']['images'] as $key => $store) {

                            if ($key === "old"){
                                foreach ($store as $index => $image) { 
                                        
                                    if($existingImage = ProductImages::find()->where(['id'=>$image['imageId']])->one()){
                                        
                                        $existingThumbImagePath = \Yii::$app->basePath."/../store/products/images/thumbnail/".$existingImage->path;
                                        $existingSmallImagePath = \Yii::$app->basePath."/../store/products/images/small/".$existingImage->path;
                                        $existingBaseImagePath = \Yii::$app->basePath."/../store/products/images/base/".$existingImage->path;

                                        $newName = "dup_".$image['imageId'].$existingImage->path;

                                        $newThumbImagePath=\Yii::$app->basePath."/../store/products/images/thumbnail/".$newName;
                                        $newSmallImagePath=\Yii::$app->basePath."/../store/products/images/small/".$newName;
                                        $newBaseImagePath=\Yii::$app->basePath."/../store/products/images/base/".$newName;

                                        $thumbFile = \Yii::$app->basePath."/../store/products/images/thumbnail/".$existingImage->path;
                                        $smallFile = \Yii::$app->basePath."/../store/products/images/small/".$existingImage->path;
                                        $baseFile = \Yii::$app->basePath."/../store/products/images/base/".$existingImage->path;

                                        if(file_exists($thumbFile)){
                                            copy($existingThumbImagePath, $newThumbImagePath);
                                        }
                                        if(file_exists($smallFile)){
                                            copy($existingSmallImagePath, $newSmallImagePath);
                                        }
                                        if(file_exists($baseFile)){
                                            copy($existingBaseImagePath, $newBaseImagePath);
                                        }

                                        $productImage = new ProductImages();
                                        $productImage->sortOrder = $image['sort_order'];
                                        $productImage->path = $newName;
                                        if(isset($_POST['Products']['images']['thumbnail'])) {
                                            if($index == $thumbnailImageIndex)
                                                $productImage->isThumbnail = 1;
                                            else
                                                $productImage->isThumbnail = 0;
                                        }  
                                        if(isset($_POST['Products']['images']['small_image'])) { 
                                            if($index == $smallImageIndex)
                                                $productImage->isSmall =1;
                                            else
                                                $productImage->isSmall = 0;
                                        }
                                        if(isset($_POST['Products']['images']['base_image'])) {    
                                            if($index == $baseImageIndex)
                                                $productImage->isBase = 1;
                                            else
                                                $productImage->isBase = 0;
                                        }
                                        if(isset($image['exclude']))
                                            $productImage->exclude = 1;
                                        else
                                            $productImage->exclude = 0; 

                                        $productImage->productId = $product->id;
                                        
                                        $productImage->save(false);
                                    }    
                                    
                                }        
                            } 


                            if ($key !== "thumbnail" && $key !== "small_image" && $key !== "base_image" && $key !== "old") { 
                                $productImage = new ProductImages;
                                $productImage->productId = $product->id;

                                if(isset($store['title']))
                                    $productImage->path = $store['title'];
                                if(isset($store['sort_order']))
                                    $productImage->sortOrder = $store['sort_order'];
                                if(isset($_POST['Products']['images']['thumbnail'])) {
                                    if($key == $thumbnailImageIndex)
                                        $productImage->isThumbnail = 1;
                                }   
                                if(isset($_POST['Products']['images']['small_image'])) { 
                                    if($key == $smallImageIndex)
                                        $productImage->isSmall =1;
                                }
                                if(isset($_POST['Products']['images']['base_image'])) {    
                                    if($key == $baseImageIndex)
                                        $productImage->isBase = 1;
                                }
                                if(isset($store['exclude']))
                                    $productImage->exclude = 1;
                                else
                                    $productImage->exclude = 0;

                                $productImage->save(false);
                            }    
                        }    
                    }  
                    
                    if($product->typeId=="configurable"){ // process the product configurations
                        foreach(json_decode($_POST['Products']['ProductSuperAttributes']['ids']) as $superAttr){
                            $attr = new ProductSuperAttributes;
                            $attr->productId = $product->id;
                            $attr->attributeId = $superAttr;
                            $attr->save(false);
                        }
                        unset($_POST['Products']['ProductSuperAttributes']['ids']);
                        foreach($_POST['Products']['ProductSuperAttributes'] as $superAttrId => $superAttr){
                            foreach($superAttr as $optionId => $option){
                                $optionPrice = new AttributeOptionPrices;
                                $optionPrice->attributeId = $superAttrId;
                                $optionPrice->productId = $product->id;
                                $optionPrice->optionId = $optionId;
                                $optionPrice->price = $option['price'];
                                $optionPrice->type = $option['type'];
                                $optionPrice->save(false);
                            }
                        }
                        if(isset($_POST['Products']['ProductLinkages'])){
                            $linkages = explode(",", $_POST['Products']['ProductLinkages']);
                            foreach($linkages as $prod){
                                $linkage = new ProductLinkages;
                                $linkage->productId = $prod;
                                $linkage->parent = $product->id;
                                $linkage->type = "configurable";
                                if($prod != "")
                                    $linkage->save(false);
                            }
                        }
                    }
                    if($product->typeId=="bundle"){  
                        foreach($_POST['Products']['Bundles'] as $id => $bundle){ 
                            $newbundle = new Bundles;
                            $newbundle->productId = $product->id;
                            $newbundle->title = $bundle['title']; 
                            $newbundle->inputType = $bundle['inputType'];   
                            $newbundle->isRequired = $bundle['isRequired']; 
                            $newbundle->position = $bundle['position']; 
                            $newbundle->save(false);
                            foreach($bundle['BundleItems'] as $id => $item){ //var_dump($item);die();
                                $newbundleItem = new BundleItems;
                                $newbundleItem->price = $item['price'];
                                $newbundleItem->priceType = $item['priceType'];
                                $newbundleItem->defaultQty =  $item['defaultQty'];
                                $newbundleItem->isUserDefinedQuantity = $item['isUserDefinedQuantity'];
                                $newbundleItem->position = $item['position'];
                                if(isset($item['default'])){
                                $newbundleItem->default = $item['default'];
                                }
                                $newbundleItem->bundleId = $newbundle->id;
                                $newbundleItem->productId = $item['productId'];
                                $newbundleItem->save(false);

                            }    
                        }
                    }    
                    //die('created.');
                    Yii::$app->getSession()->setFlash('success', 'Product created successfully.');
                    return $this->redirect(['index', 'id' => $product->id]);
                }
            }
        //else{ var_dump($product->getErrors()); die;}
        }
        // if($product->typeId=="configurable"){ //var_dump(ArrayHelper::map($product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes'])->limit(10)->all(), 'id', 'id'));
        //     $availableChildProducts = new ArrayDataProvider([
        //         'allModels' => $product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes']),
        //         //'query' => $product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes']),
        //         'pagination'=> ['defaultPageSize' => 10]
        //     ]);
        // }else
        //     $availableChildProducts = [];
        $product->load($_REQUEST); 
        $groups = $product->attributeSet->attributeGroups;
        //var_dump($product->attributeSetId); die;
        if(!isset($_REQUEST['createForm'])){ // submission of the initial popup or the config attrs popup
            //var_dump($_REQUEST); die;
            if($product->typeId=="configurable"){ //var_dump(ArrayHelper::map($product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes'])->limit(10)->all(), 'id', 'id'));
		if(!isset($_REQUEST['Products']['ProductSuperAttributes']) && !$copyFrom)
			return $this->redirect(['index', 'id' => $product->id]);
                $availableChildProducts = new ArrayDataProvider([
                    'allModels' => $product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes']),
                    //'query' => $product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes']),
                    'pagination'=> ['defaultPageSize' => 10]
                ]);
            }elseif(!$copyFrom)
                $availableChildProducts = [];
            $product->createValidators();
            //var_dump($stores);die();    
            return $this->render('create', compact('product', 'groups', 'availableChildProducts','dataProvider','searchModel','stores','storeProducts','productImages','product_meta','categoryIds'));
        }
        return $this->render('create', compact('product', 'groups','productImages', 'availableChildProducts', 'stores','storeProducts','product_meta','categoryIds'));

    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        //var_dump($_POST['kv-products']);die();
        $session = Yii::$app->session;
        $product = $this->findModel($id);
        $product_meta = $product->meta;
        $product_meta =(empty($product_meta)) ? [new ProductMetaDetails()] : $product_meta;

        $productImages = $product->productImages;
        $thumbnailImage = $product->thumbnailImage;
        $smallImage = $product->smallImage;
        $baseImage = $product->baseImage;

        //var_dump($product->brand->supplier->id);die();


        //var_dump($session['storeId']); die;
        if (isset($session['storeId'])) { //die($session['storeId']);

            $product->storeId = $session['storeId'];  
        } 

        $_REQUEST['Products']['attributeSetId'] = $product->attributeSetId;
        if($product->typeId=="configurable"){ 
            $_REQUEST['Products']['ProductSuperAttributes'] = ArrayHelper::map($product->superAttributes, 'id', 'attributeId');
            $availableChildProducts = new ArrayDataProvider([
                'allModels' => $product->attributeSet->getAvailableChildProducts($_REQUEST['Products']['ProductSuperAttributes']),
                'pagination'=> ['defaultPageSize' => 10]
            ]);
        }


        $user = Yii::$app->user->identity;
        
        if($user->roleId == "3") { 
            $storeId = $user->store->id;
        }  
        else { 
                if(isset($session['storeId'])){
                    $storeId = $session['storeId'];
                }  
                else { $storeId = 0; }
        } 
        
        $stores = [];
        if($user->roleId != "3") { 
            $stores = Stores::find()->all(); 
            $storeProducts = ArrayHelper::map(StoreProducts::findAll(['productId'=>$product->id,'type'=>'b2c']),'id','storeId');
        } 
        else{
            $storeProducts = StoreProducts::findOne(['productId'=>$product->id,'storeId'=>$storeId,'type'=>'b2c','enabled'=>1]);
        }
        //$categoryIds = ArrayHelper::map($product->categories, 'id', 'categoryId');
        //$categoryIds = ArrayHelper::map($product->categories, 'id', 'categoryId','storeId');

        if($user->roleId != "3") { 
            $categoryIds = ArrayHelper::map(ProductCategories::find()->andWhere("(storeId = '0' or StoreId = $storeId) and productId = $product->id")->all(),'categoryId','storeId');
        } 
        else{
            $categoryIds = ArrayHelper::map(ProductCategories::find()->andWhere("(storeId = '0' or StoreId = $storeId) and productId = $product->id")->all(), 'categoryId','storeId');
        }    

        
        if ($product->load(Yii::$app->request->post())) { 

            /*if(isset($_POST['Products']['images'])){
                    if(isset($_POST['Products']['images']['old'])){ 
                        die('old');
                    }
                    else{
                        die('new');
                    }
            }*/

            //die('Not old');

            $product->scene = "update"; // for the Products::__get() to fetch the currently loaded attributes
            $product->createValidators();
            $productAttrs = $this->productAttrs;
            if($user->roleId == "3" && $product->createdBy != $user->id) {
                $customAttrs = $this->b2cAdminEditableAttrs;
                //$customAttrs = ['price',/*'special_price','special_from_date','special_to_date','pricetype','cost_price'*/]; //filtering the custom attributes if it's a B2C admin
            }
            else {
                $customAttrs = array_diff($product->attributes(), $productAttrs); //filtering the custom attributes
            }    
            //var_dump($customAttrs);die();
            foreach($customAttrs as $attr)
                $attributes[$attr] = $product->getAttributes()[$attr];
                $_REQUEST['bypassStoreProductsCheck'] = true;
                if(\Yii::$app->request->isAjax){ // it's a validation request from ajaxValidation
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ($user->roleId == "3")? \yii\widgets\ActiveForm::validate($product, $this->b2cAdminEditableAttrs) : \yii\widgets\ActiveForm::validate($product);
                }
                if($product->save(true, $productAttrs)){ //saving only the columns in Products s
                    
                    $oldIDs = ArrayHelper::map($product_meta, 'id', 'id');//product meta details starts
                    $product_meta = Model::createMultiple(ProductMetaDetails::classname(), $product_meta);
                    Model::loadMultiple($product_meta, Yii::$app->request->post());
                    $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($product_meta, 'id', 'id')));
                    
                    $valid = Model::validateMultiple($product_meta);
                    
                    if (!empty($deletedIDs) && (Yii::$app->user->identity->roleId == "1" || $product->createdBy == Yii::$app->user->identity->id)) {
                                ProductMetaDetails::deleteAll(['id' => $deletedIDs]);
                            }

                    if ($valid && !empty($product_meta)) {
                        //   $transaction = \Yii::$app->db->beginTransaction();
                        try {

                            foreach ($product_meta as $key=>$meta) {
                                //if(!empty($meta->key) || !empty($meta->value)) {
                                    $meta->productId = $product->id;
                                    $meta->position = $key+1;
                                    $meta->save(false);
                                //}    
                            }
                        } catch (Exception $e) {
                            //$transaction->rollBack();
                        }
                    }

                    if($product->saveCustomAttributes($attributes,$storeId)){
                        if(isset($_POST['kv-products'])){  //var_dump($_POST['kv-products']);var_dump($catids);die();
                            $catids = ArrayHelper::map(ProductCategories::find()->andWhere("storeId = $storeId and productId = $product->id")->all(),'id','categoryId');
                            if(!empty($_POST['kv-products'])) { //var_dump(explode(",", $_POST['kv-products']));die();
                                foreach(explode(",", $_POST['kv-products']) as $category){ 
                                    if($category != NULL){
                                        if($user->roleId != "3") {
                                            if(!in_array($category, array_values($catids))){
                                                $productCat = new ProductCategories;
                                                $productCat->categoryId = $category;
                                                $productCat->productId = $product->id;
                                                $productCat->storeId = $storeId;
                                                $productCat->save(false);
                                            }
                                        }
                                        else{  
                                            if (!ProductCategories::find()->where(['categoryId' => $category, 'productId' => $product->id])->one()) { 
                                                $productCat = new ProductCategories;
                                                $productCat->categoryId = $category;
                                                $productCat->productId = $product->id;
                                                $productCat->storeId = $user->store->id;
                                                $productCat->save(false);
                                            }
                                        }
                                    }      
                                }
                            }
                                $removed = array_diff(array_values($catids), explode(",", $_POST['kv-products']));
                                //var_dump($removed);die();
                                if(!empty($removed))
                                    ProductCategories::deleteAll("productId='".$id."' and categoryId in (".implode(",", $removed).")");
                            }
                       // }

                        if (isset($_POST['Products']['deletedItems'])) {
                           //print_r($_POST['Products']['deletedItems']);
                          //exit;
                            \common\models\RelatedProducts::deleteAll('RelatedProducts.id in (' . implode(",", $_POST['Products']['deletedItems']) . ')');
                        }
                        
                        
                            
                        if (isset($_POST['Products']['rel_product_id'])) {
                            foreach ($_POST['Products']['rel_product_id'] as $rel_product_id) {
                                if (isset($_POST['Products'][$rel_product_id]['newitem'])) {
                                    $Related_product = new \common\models\RelatedProducts;
                                    $Related_product->relatedProductId = $rel_product_id;
                                    $Related_product->productId = $product->id;
                                    $Related_product->storeId = $storeId;
                                    $Related_product->save(false);
                                } else {
                                    
                                }
                            }
                        }
                        

                        if(isset($user->store)){ 
                            if(!StoreProducts::find()->where(['storeId' => $user->store->id, 'productId' => $product->id, 'type' => 'b2c'])){
                                $storeProduct = new StoreProducts;
                                $storeProduct->storeId = $user->store->id;
                                $storeProduct->productId = $product->id;
                                $storeProduct->type = 'b2c';
                                $storeProduct->enabled = '1';
                                $storeProduct->save(false);
                            }
                        }elseif(isset($_POST['Products']['stores'])){ 
                            $previousStores = ArrayHelper::map(StoreProducts::findAll(['productId'=>$product->id,'type'=>'b2c']),'id','storeId');
                            $removedStores = array_diff(array_values($previousStores), $_POST['Products']['stores']);
                            if(!empty($removedStores)){
                                StoreProducts::deleteAll("productId='$product->id' AND storeId in (".implode(",", $removedStores).") AND type='b2c'"); 
                            }    
                            foreach ($_POST['Products']['stores'] as $store) { 
                                if (!StoreProducts::find()->where(['storeId' => $store, 'productId' => $product->id, 'type' => 'b2c'])->one()) {
                                    $storeProduct = new StoreProducts;
                                    $storeProduct->storeId = $store;
                                    $storeProduct->productId = $product->id;
                                    $storeProduct->type = 'b2c';
                                    $storeProduct->save(false);
                                } 
                            }
                        }
                        else{
                            StoreProducts::deleteAll("productId='$product->id' AND type='b2c'");
                        }

                        
                    if(isset($_POST['Products']['deletedImages'])) {  
                        foreach ($_POST['Products']['deletedImages'] as $key => $value) {   
                            $model_sb=ProductImages::find()->where(['id' => $value])->one();
                            $model_sb->delete();
                            $uploadThumbnailPath = \Yii::$app->basePath."/../store/products/images/thumbnail/";
                            $uploadSmallPath = \Yii::$app->basePath."/../store/products/images/small/";
                            $uploadBasePath = \Yii::$app->basePath."/../store/products/images/base/";
                            if (file_exists($thumbImage =  $uploadThumbnailPath.$value['name'])) {
                                unlink($thumbImage);
                            }    
                            if (file_exists($smallImage =  $uploadSmallPath.$value['name'])) {
                                unlink($smallImage);
                            }
                            if (file_exists($baseImage =  $uploadBasePath.$value['name'])) {
                                unlink($baseImage);
                            }    
                        }
                }

            if(isset($_POST['Products']['images'])){
                
                if(isset($_POST['Products']['images']['thumbnail'])){
                    $thumbnailImageIndex = (int) $_POST['Products']['images']['thumbnail'];
                } 
                if(isset($_POST['Products']['images']['small_image'])){
                    $smallImageIndex = (int) $_POST['Products']['images']['small_image'];
                } 
                if(isset($_POST['Products']['images']['base_image'])){
                    $baseImageIndex = (int) $_POST['Products']['images']['base_image'];
                } 

                foreach ($_POST['Products']['images'] as $key => $value) {
                    if ($key === "old"){
                        foreach ($value as $index => $image) { 
                            if($productImage = ProductImages::find()->where(['id' => $image['imageId'], 'productId' => $product->id])->one()){
                                $productImage->sortOrder = $image['sort_order'];
                                if(isset($_POST['Products']['images']['thumbnail'])) {
                                    if($index == $thumbnailImageIndex)
                                        $productImage->isThumbnail = 1;
                                    else
                                        $productImage->isThumbnail = 0;
                                }  
                                if(isset($_POST['Products']['images']['small_image'])) { 
                                    if($index == $smallImageIndex)
                                        $productImage->isSmall =1;
                                    else
                                        $productImage->isSmall = 0;
                                }
                                if(isset($_POST['Products']['images']['base_image'])) {    
                                    if($index == $baseImageIndex)
                                        $productImage->isBase = 1;
                                    else
                                        $productImage->isBase = 0;
                                }
                                if(isset($image['exclude']))
                                    $productImage->exclude = 1;
                                else
                                    $productImage->exclude = 0; 
                
                                $productImage->save(false);
                            }
                        }        
                    }  

                    else{
                        if($key !== "thumbnail" && $key !== "small_image" && $key !== "base_image"){
                            $productImage = new ProductImages;
                            $productImage->productId = $product->id;
                            if(isset($value['title']))
                                $productImage->path = $value['title'];
                            if(isset($value['sort_order']))
                                $productImage->sortOrder = $value['sort_order'];
                            if(isset($value['exclude']))
                                $productImage->exclude = 1;
                            if(isset($_POST['Products']['images']['thumbnail'])){ 
                                $productImage->isThumbnail = 1;
                            }
                            if(isset($_POST['Products']['images']['base_image'])){
                                $productImage->isBase =1;
                            }
                            if(isset($_POST['Products']['images']['small_image'])){
                                $productImage->isSmall =1;
                            }
            
                            $productImage->save(false); 
                        }           
                    } 
                }    
            }     

                    if($product->typeId=="configurable"){
                        unset($_POST['Products']['ProductSuperAttributes']['ids']);
                        if(isset($_POST['Products']['ProductSuperAttributes'])){
                            foreach($_POST['Products']['ProductSuperAttributes'] as $superAttrId => $superAttr){
                                AttributeOptionPrices::deleteAll(['attributeId' => $superAttrId, 'productId' => $product->id]);
                                foreach($superAttr as $optionId => $option){
                                    $optionPrice = new AttributeOptionPrices;
                                    $optionPrice->attributeId = $superAttrId;
                                    $optionPrice->productId = $product->id;
                                    $optionPrice->optionId = $optionId;
                                    $optionPrice->price = $option['price'];
                                    $optionPrice->type = $option['type'];
                                    $optionPrice->save(false);
                                }
                            }
                        }
                        if(isset($_POST['Products']['ProductLinkages'])){
                            $linkages = explode(",", $_POST['Products']['ProductLinkages']);
                            ProductLinkages::deleteAll(['type' => 'configurable', 'parent' => $product->id]);
                            foreach($linkages as $prod){
                                $linkage = new ProductLinkages;
                                $linkage->productId = $prod;
                                $linkage->parent = $product->id;
                                $linkage->type = "configurable";
                                if($prod != "")
                                    $linkage->save(false);
                            }
                        }
                    }


                   if(isset($_POST['ProductInventory'])){ 
                        if (!$productInventory = ProductInventory::find()->where(['storeId' => $storeId, 'productId' => $product->id])->one())
                            $productInventory = new ProductInventory;
                        $productInventory->productId = $product->id;
                        $productInventory->storeId = $storeId;
                        $productInventory->quantity = $_POST['ProductInventory']['quantity'];
                        $productInventory->minQuantity = $_POST['ProductInventory']['minQuantity'];
                        $productInventory->manageStock =  $_POST['ProductInventory']['manageStock']; 
                        $productInventory->isInStock = $_POST['ProductInventory']['isInStock'];
                        $productInventory->save(false);   
                    }     

                  /* if(isset($_POST['ProductInventory'])){ //var_dump($_POST['ProductInventory']); die;
                        if (!$productInventory = ProductInventory::find()->where(['storeId' => $storeId, 'productId' => $product->id])->one())
                            $productInventory = new ProductInventory;
                        $productInventory->productId = $product->id;
                        $productInventory->storeId = $storeId;
                        $productInventory->quantity = $_POST['ProductInventory']['quantity'];
                        $productInventory->minQuantity = $_POST['ProductInventory']['minQuantity'];
                        $productInventory->manageStock =  $_POST['ProductInventory']['manageStock']; 
                        $productInventory->isInStock = $_POST['ProductInventory']['isInStock'];
                        $productInventory->save(false);
                    }*/




                    if($product->typeId=="bundle") {

                        if(isset($_POST['Products']['deletedBundles'])){

                            Bundles::deleteAll('id in ('.implode($_POST['Products']['deletedBundles']).')');
                            BundleItems::deleteAll('bundleId in ('.implode($_POST['Products']['deletedBundles']).')');
                        }
                        if(isset($_POST['Products']['deletedBundleItems'])) {
                            //var_dump($_POST['Products']['deletedBundleItems']);die();

                            BundleItems::deleteAll('id in ('.implode($_POST['Products']['deletedBundleItems']).')');
                        }
                        foreach($_POST['Products']['Bundles'] as $id => $bundle){ 
                           if(isset($bundle['newBundle'])){
                                $newbundle = new Bundles;
                                $newbundle->productId = $product->id;
                                $newbundle->title = $bundle['title']; 
                                $newbundle->inputType = $bundle['inputType'];   
                                $newbundle->isRequired = $bundle['isRequired']; 
                                $newbundle->position = $bundle['position']; 
                                $newbundle->save(false);
                           }
                           else {
                                if(isset($_POST['Products']['deletedBundles'])){
                                    if(!in_array($bundle['bundle_id'], $_POST['Products']['deletedBundles'])){ 
                                    $existingBundle = Bundles::findOne($bundle['bundle_id']); 
                                    $existingBundle->productId = $bundle['productId'];
                                    $existingBundle->title = $bundle['title']; 
                                    $existingBundle->inputType = $bundle['inputType'];   
                                    $existingBundle->isRequired = $bundle['isRequired'];
                                    $existingBundle->position = $bundle['position']; 
                                    $existingBundle->save(false);
                                    }
                                }
                                else {
                                    $existingBundle = Bundles::findOne($bundle['bundle_id']); 
                                    $existingBundle->productId = $bundle['productId'];
                                    $existingBundle->title = $bundle['title']; 
                                    $existingBundle->inputType = $bundle['inputType'];   
                                    $existingBundle->isRequired = $bundle['isRequired'];
                                    $existingBundle->position = $bundle['position']; 
                                    $existingBundle->save(false);    
                                }
                            }
                            foreach($bundle['BundleItems'] as $id => $item){
                                if(isset($item['newItem'])){
                                    $newbundleItem = new BundleItems;
                                    $newbundleItem->price = $item['price'];
                                    $newbundleItem->priceType = $item['priceType'];
                                    $newbundleItem->defaultQty =  $item['defaultQty'];
                                    $newbundleItem->isUserDefinedQuantity = $item['isUserDefinedQuantity'];
                                    $newbundleItem->position = $item['position'];
                                    if(isset($item['default'])){
                                        $newbundleItem->default = $item['default'];
                                    }
                                    if(isset($bundle['newBundle'])){
                                    $newbundleItem->bundleId = $newbundle->id;
                                    }
                                    else {
                                        $newbundleItem->bundleId = $bundle['bundle_id'];
                                    }
                                    $newbundleItem->productId = $item['productId'];
                                    $newbundleItem->save(false);
                                }    
                                else {
                                    $existingBundleItem = BundleItems::findOne($item['bundleItemId']); 
                                    $existingBundleItem->price = $item['price'];
                                    $existingBundleItem->priceType = $item['priceType'];
                                    $existingBundleItem->defaultQty =  $item['defaultQty'];
                                    $existingBundleItem->isUserDefinedQuantity = $item['isUserDefinedQuantity'];
                                    $existingBundleItem->position = $item['position'];
                                    if(isset($item['default'])){
                                        $existingBundleItem->default = $item['default'];
                                    }
                                    $existingBundleItem->bundleId = $item['bundleId'];
                                    $existingBundleItem->productId = $item['productId'];
                                    $existingBundleItem->save(false);    
                                }
                            }    
                        }
                    }

                    if($user->roleId == "3" && strtotime($storeProducts->disableDate) >= strtotime(date('Y-m-d'))) { 
                       $product->extendDisableDate($storeId,$id,date('Y-m-d'));//Extend disable date in store products table
                    }

                    Yii::$app->getSession()->setFlash('success', 'Product updated successfully.');
                    return $this->redirect(['index', 'id' => $product->id]);
                }

            }    
        } else {
            $groups = $product->attributeSet->attributeGroups;
            return $this->render('update', compact('product', 'groups', 'categoryIds', 'availableChildProducts','stores','storeProducts','productImages','thumbImageUrl','product_meta'));

            //}    //var_dump($product->getErrors());die();

        }
        $groups = $product->attributeSet->attributeGroups;
        return $this->render('update', compact('product', 'groups', 'categoryIds', 'availableChildProducts','stores','storeProducts','productImages','product_meta'));

    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $product = $this->findModel($id);
        $delete = false;
        if(Yii::$app->user->identity->roleId == 3){
            if($product->createdBy == Yii::$app->user->id)
                $delete = true;
            else{
                $delete = false;
            }
        }elseif(Yii::$app->user->identity->roleId == 1)
            $delete = true;
        else
            $delete = false;
        if($delete)
            $product->delete();
        else{
            echo "403: Not allowed"; die;
        }        
        Yii::$app->getSession()->setFlash('success', 'Product deleted successfully.');
        return $this->redirect(['index']);
    }

    public function actionCopy($id){
        $oldProduct = $this->findModel($id);
        $product = clone $oldProduct;
    }

    public function actionExport(){
    	$searchModel = new \common\models\search\ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('export', compact('searchModel', 'dataProvider'));
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($product = Products::findOne($id)) !== null) {
            return $product;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionTest()
    {

        $products = Products::find()->where(['visibility'=>'not-visible-individually'])->all();
        
        foreach ($products as $product) {
            if($testProduct = TestProducts::find()->where("sku='".$product->sku."'")->one()){
                if($testProduct->brandId == '210'){ //var_dump($testProduct);die();
                    $testProduct->brandId = NULL;
                    $testProduct->save(false);
                    //var_dump($testProduct->save(false));die();
                }
            }
           //}
        }
 
    }


    public function actionEnable(){

        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $currentEnabledPdts = isset($_POST['currentEnabledPdts'])? $_POST['currentEnabledPdts'] : [];
        $enabledProducts = isset($_POST['enabledProducts'])? $_POST['enabledProducts'] : [];
        $disabledProducts = array_diff($enabledProducts, $currentEnabledPdts);

        $currentFeaturedPdts = isset($_POST['currentFeaturedPdts'])? $_POST['currentFeaturedPdts'] : [];
        $featuredProducts = isset($_POST['featuredProducts'])? $_POST['featuredProducts'] : [];
        $disabledFeatured = array_diff($featuredProducts, $currentFeaturedPdts);

        //var_dump($currentFeaturedPdts);die();

        foreach ($currentFeaturedPdts as $product) {
            if ($featured = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where('AttributeValues.storeId = '.Yii::$app->user->identity->store->id.' and AttributeValues.productId = '.$product.' and AttributeValues.value = 1 and a.code = "featured"')->one()) {
                $featured->value = 1;
                $featured->save(false);
            }

            else{

                $featured = new AttributeValues();
                $attribute = Attributes::findOne(['code'=>'featured']);
                $featured->value = 1;
                $featured->storeId = Yii::$app->user->identity->store->id;
                $featured->productId = $product;
                $featured->attributeId = $attribute->id;
                $featured->save(false);
            }
        }

        if(!empty($disabledFeatured)){
            foreach ($disabledFeatured as $product) {
               $featured = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where('AttributeValues.storeId = '.Yii::$app->user->identity->store->id.' and AttributeValues.productId = '.$product.' and AttributeValues.value = 1 and a.code = "featured"')->one();
               $featured->value = 0;
               $featured->save(false); 
            }
        }
        
        foreach ($currentEnabledPdts as $product) {
            if ($existingProduct = StoreProducts::find()->where(['storeId' => $storeId, 'productId' => $product, 'type' => 'b2c', 'enabled'=>'0'])->one()) {
                $existingProduct->enabled = "1";
                $existingProduct->save(false);
            }
        }    
        if(!empty($disabledProducts)){ 
            foreach ($disabledProducts as $product) { 
                $existingProduct = StoreProducts::find()->where(['storeId' => $storeId, 'productId' => $product, 'type' => 'b2c'])->one();
                $existingProduct->enabled = "0";
                $existingProduct->save(false);
            }   
        }



        return true;
    }

    public function actionGetsupplierbrands($supplierUid){
        if($supplier = \common\models\Suppliers::findOne($supplierUid)){
            $query = $supplier->getBrands();
            if(Yii::$app->user->identity->roleId == "3"){
                $query->join('LEFT JOIN', 'StoreBrands sb', 'sb.brandId = Brands.id')
                ->where(['sb.storeId' => Yii::$app->user->identity->store->id]);
            }
            $results = $query->all();
            $options = [];
            foreach($results as $result)
                $options[] = ['id' => $result->id, 'title' => $result->title];
            return json_encode($options);
        }
        return false;
    }

    
    public function actionFeatured(){
        if($_POST){
            var_dump($_POST);
        }
        else{
            //$product = AttributeValues::find()->join('JOIN','Attributes as a','a.id = AttributeValues.attributeId')->where(['a.code' => 'made_to_order', 'productId' => $this->id, 'value'=>1])->andWhere("storeId = ".Yii::$app->params['storeId']." or storeId=0")->one()

            $products = Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeId')->where(['a.code'=>'featured','value'=>1])->andWhere("storeId=0");

           $dataProvider = new ActiveDataProvider([
            'query' => $products //$search->search(Yii::$app->request->queryParams);
            ]);

            return $this->render('featuredlist',compact('dataProvider'));
            
        }
    }

    public function actionList($keyword){
        $products = Products::find()->join('JOIN','AttributeValues as av','av.productId = Products.id')->join('JOIN','Attributes as a','a.id = av.attributeteId')->join('JOIN','StoreProducts as sp','sp.productId = Products.id')->where('(a.code = "name" and av.value like "%'.$keyword.'%") or Products.sku like "%'.$keyword.'%" and av.storeId = Yii::$app->user->identity->store->id')->all();

        foreach($products as $product)
            $options[] = ['sku' => $product->sku, 'title' => $product->name];

        return json_encode($options);
    }
   
    public function actionRefreshdisabledate($id){
        $product = new Products();
        $user = User::findOne(Yii::$app->user->Id);
       
        $storeId = $user->store->id;
        $productId = $id;

        $res = false;
        if($user->roleId == "3" && $productId) { 
           $res = $product->extendDisableDate($storeId,$productId,date('Y-m-d'));
        }
        if($res) Yii::$app->getSession()->setFlash('success', 'Disable date extended Successfully.');
        else     Yii::$app->getSession()->setFlash('error', 'Please check again, some errors occurred.');
        return $this->redirect(['update', 'id' => $productId]);
    }


    // public function actionSetstore(){
    //     $session = Yii::$app->session;
    //     $session['storeId'] = $_REQUEST["storeId"];
    //     return $session['storeId'];
    //     //var_dump($session['user_id']);die();
    // }


}
