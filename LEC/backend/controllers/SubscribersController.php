<?php

namespace backend\controllers;

use Yii;
use common\models\Subscribers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;

/**
 * SubscribersController implements the CRUD actions for Subscribers model.
 */
class SubscribersController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subscribers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new \common\models\search\SubscribersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $condition = (Yii::$app->user->identity->roleId == '3')? ['storeId' => Yii::$app->user->identity->store->id] : [];
        $dataProvider->query->andWhere($condition);
        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    /**
     * Displays a single Subscribers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subscribers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscribers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Subscribers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Subscribers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $subscriber = $this->findModel($id);
        if(Yii::$app->user->identity->roleId == "1" || $subscriber->storeId == Yii::$app->user->identity->store->id){
            $subscriber->delete();
            Yii::$app->session->setFlash('success', 'Subscriber successfully deleted');
        }
        return $this->redirect(['index']);
    }

    public function actionExport(){
        $dataProvider = new ActiveDataProvider([
            'query' => Subscribers::find(),
        ]);
        if(Yii::$app->user->identity->roleId != "3")
            $columns = [['class' => 'yii\grid\SerialColumn'],'email:email',['attribute' => 'store.title', 'label' => 'Store'],['attribute' => 'Date Created','value' => function($model){ return \backend\components\Helper::date($model->dateAdded); }]];
        else
            $columns = [['class' => 'yii\grid\SerialColumn'],'email:email',['attribute' => 'Date Created','value' => function($model){ return \backend\components\Helper::date($model->dateAdded); }]];
        return \common\components\CSVExport::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ]);
    }

    /**
     * Finds the Subscribers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscribers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscribers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
