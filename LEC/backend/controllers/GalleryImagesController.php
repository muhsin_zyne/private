<?php

namespace backend\controllers;
use Yii;
use yii\helpers\Html;
use common\models\GalleryImages;
use common\models\search\GalleryImagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\components\AdminController;

/**
 * GalleryimagesController implements the CRUD actions for Galleryimages model.
 */
class GalleryimagesController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Galleryimages models.
     * @return mixedallery
     */
    public function actionIndex($id)
    {
        // /var_dump($id);die;
        $galleryid=$id;
        $searchModel = new GalleryImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'galleryid' => $galleryid,

        ]);
    }

    /**
     * Displays a single Galleryimages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Galleryimages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        //die('hiiii');
        $model = new GalleryImages();        
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/gallery/images/";        
        $images = UploadedFile::getInstances($model, 'title');
        foreach ($images as $key => $image) 
        { 
            $ext = end((explode(".", $image->name)));
            $avatar = Yii::$app->security->generateRandomString().".{$ext}";
            $path = Yii::$app->params['uploadPath'] . $avatar; 
            $model->title = $avatar;
            $model->galleryId = $id;
            $model->position = 0;
            //var_dump($model->galleryId);die;
            $model->save();
            $image->saveAs($path);
            
        }
        // $image->saveAs($path);
        //return true;
        Yii::$app->getSession()->setFlash('success', 'The image has been successfully uploaded.');  
        return $this->redirect(['index', 'id' => $id]);
    }
    public function actionSortable()
    {
        $pos=0;
        $sort_category=explode(",",$_POST['list']);                        
        foreach ($sort_category as  $category) 
        {
            $gallery_model= GalleryImages::find()->where( [ 'id' => $category] )->one();   
            $gallery_model->position= $pos;      
            $gallery_model->save();
            $pos++;
        }
      
    }

    /**
     * Updates an existing Galleryimages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $description=$_POST['dec'];
        $image_id=$_POST['image_id'];
        $gallery_id=$_POST['gallery_id'];
        $model= GalleryImages::find()->where( [ 'id' => $image_id] )->one();   
        $model->description =$description;      
        $model->save();
        Yii::$app->getSession()->setFlash('success', 'The Description has been Updated.');  
        return $this->redirect(['index', 'id' => $gallery_id]);
        //return $this->redirect(['index', 'id' => $gallery_id]);
        // $html='';
        //     $gallery_image_all= GalleryImages::find()->where( ['galleryId' => $gallery_id] )->orderBy('position')->all(); 
        //     if(empty($gallery_image_all))
        //     { 
        //          $html.='No Images Found';
        //     }
        //     else
        //     {           
        //         foreach ($gallery_image_all as $index => $galleryimage) 
        //         {
                
        //             $html.='<li class="ui-state-default" id="'.$galleryimage['id'].'">
        //                 <div style="width:100%;  height:175px; overflow: hidden;">
        //                     <a href=""><img src ='.Yii::$app->params["rootUrl"].'/store/gallery/images/'.$galleryimage['title'].' height="100%" width="auto" ></a>
        //                 </div>';     
        //             $html.=''. Html::a('Delete', ['delete', 'id' => $galleryimage['id'],'galleryId' => $gallery_id], 
        //                 ['class' => 'btn btn-info btn-sm','data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]).'';             
        //             $html.='<button id="'.$galleryimage['id'].'" class="btn btn-info btn-sm btn1" >Description</button>';
        //             $html.='<div class="clear"></div>
        //                 <span style="font-size:14px">'.substr($galleryimage['description'],0,30).' </span>';
        //              $html.='<span style="color:blue;font-weight:bold;font-size:14px">';                     
                        
        //                 $more=strlen($galleryimage['description']);
        //                 if($more>20)
        //                 {
        //                      $html.=''. Html::a('More...', ['view', 'id' => $galleryimage['id'],'galleryId' => $gallery_id]).'';
        //                     //$html.=''.Html::a('Update', ['update','id' => $galleryimage['id']], ['class'=>'btn btn-info btn-sm']).'';
        //                 }
                        
        //                 $html.='</span>
                        
        //              </li>';  
        //          } 
        //       } 
        //     $html.='
        // ';
      
        // echo $html;
    }

    /**
     * Deletes an existing Galleryimages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$galleryId)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'The Image has been Deleted.');  
        return $this->redirect(['index', 'id' => $galleryId]);
    }

    /**
     * Finds the Galleryimages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Galleryimages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GalleryImages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
