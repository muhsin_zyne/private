<?php

namespace backend\controllers;
use Yii;
use common\models\Stores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\StoreProducts;
use common\models\Products;
use common\models\search\StoresSearch;

/**
 * StoresController implements the CRUD actions for Stores model.
 */
class StoresController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StoresSearch();
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Stores::find(),
        // ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stores model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stores();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Stores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Stores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionManage(){


        $searchModel = new StoresSearch();
        // $dataProvider = new ActiveDataProvider([
        //     'query' => Stores::find(),
        // ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(isset($_POST['Stores'])){  //var_dump($_POST['Stores']);die();
            $session = Yii::$app->session;
            Yii::$app->session['storeId'] = $_POST['Stores']['storeId'];
            $storeId = (Yii::$app->user->identity->roleId == "3") ? Yii::$app->user->identity->store->id : (isset(Yii::$app->session['storeId']) ? Yii::$app->session['storeId'] : 1);

            $store = $this->findModel($storeId);
            if ($store->load(Yii::$app->request->post()) && $store->save())
            {
                Yii::$app->session->setFlash('success', 'Details updated successfully');
                return $this->render('index', compact('store'));
            }
            return $this->render('index', compact('store'));
                
        }
        else{
            $session = Yii::$app->session;
            if (Yii::$app->user->identity->roleId == "1") {  //var_dump($session['storeId']);die();
                if (isset($session['storeId']) && $session['storeId'] != "0"){
                    $storeId = $session['storeId'];
                }
                else{  //var_dump($session['storeId']);die();
                    Yii::$app->session['storeId'] = 1;
                    $storeId = 1;
                }
            }
            $store = $this->findModel($storeId);
            return $this->render('index',compact('store','dataProvider','searchModel'));
        }


        /*$user = Yii::$app->user->identity;
        $session = Yii::$app->session;
        if($user->roleId == "1"){
            if (isset($session['storeId'])) 
                $storeId = $session['storeId'];
            else
                $storeId = 0;      
        }
        elseif ($user->roleId == "3") {
            $storeId = $user->store->id;
        }

        $store = $this->findModel($storeId);
        return $this->render('index',compact('store'));*/
    }

    public function actionDummyproducts(){
        /*$dummyProducts = StoreProducts::find()->join('JOIN', 'AttributeValues av', 'av.productId = StoreProducts.productId')->join('JOIN', 'Products p', 'p.id = StoreProducts.productId')->where('StoreProducts.type="b2c" and p.createdBys !=0')->all();*/

        /*$dummyProducts = Products::find()->join('JOIN','StoreBrands b','Products.brandId = b.brandId')->join('JOIN','StoreProducts sp','Products.id=StoreProducts.productId')->where('');*/
    }

    /**
     * Finds the Stores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stores::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
