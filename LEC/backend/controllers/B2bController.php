<?php

namespace backend\controllers;
use Yii;
use yii\data\ActiveDataProvider;
use common\models\Stores;
use common\models\Brands;
use common\models\Products;
use common\models\StoreSuppliers;
use common\models\StoreBrands;
use common\models\StoreBrandsVisibility;
use common\models\StoreProducts;
use yii\helpers\ArrayHelper;
use common\models\search\OrdersSearch;
use common\models\Orders;
use common\models\OrderItems;
use common\models\User;
use common\models\StorePromotions;
use common\models\ConsumerPromotions;
use common\models\ConsumerPromotionProducts;
use backend\components\Helper;

class B2bController extends \app\components\AdminController
{
    public function actionIndex()
    {
    	$stores = new ActiveDataProvider([
            'query' => Stores::find() //$search->search(Yii::$app->request->queryParams);
        ]);
        return $this->render('index', compact('stores'));
    }


    public function actionSuppliers($id, $type="b2c"){
        $store = Stores::findOne($id);
        if(isset($_POST['Suppliers'])){
            $_POST['Suppliers'] = is_array($_POST['Suppliers'])? $_POST['Suppliers'] : array();
            $previousSuppliers = ArrayHelper::map(StoreSuppliers::find()->where(['storeId' => $id, 'type' => $type])->all(), 'id', 'supplierUid');
            $removedSuppliers = array_diff(array_values($previousSuppliers), $_POST['Suppliers']);
            if(!empty($removedSuppliers)){
                foreach(StoreSuppliers::find()->where("storeId='$id' AND supplierUid in (".implode(",", $removedSuppliers).") AND type='".$type."'")->all() as $storeSupplier)
                    $storeSupplier->unsetBrands($type);
                StoreSuppliers::deleteAll("storeId='$id' AND supplierUid in (".implode(",", $removedSuppliers).") AND type='".$type."'");
            }
            foreach($_POST['Suppliers'] as $supplier){
                if(!StoreSuppliers::find()->where(['storeId' => $id, 'supplierUid' => $supplier, 'type' => $type])->one()){
                    $storeSupplier = new StoreSuppliers;
                    $storeSupplier->storeId = $id;
                    $storeSupplier->supplierUid = $supplier;
                    $storeSupplier->type = $type;
                    $storeSupplier->save(false);
                    $storeSupplier->setBrands($type);
                }
            }
        }
        return $this->render('suppliers', compact('store', 'type'));
    }

    public function actionBrands($id, $type="b2c"){
    	$store = Stores::findOne($id);
        if(isset($_POST['Brands'])){
            $_POST['Brands'] = is_array($_POST['Brands'])? $_POST['Brands'] : array();
            $previousBrands = ArrayHelper::map(StoreBrands::find()->where(['storeId' => $id, 'type' => $type])->all(), 'id', 'brandId');
            $removedBrands = array_diff(array_values($previousBrands), $_POST['Brands']);
            if(!empty($removedBrands)){
                StoreBrands::deleteAll("storeId='$id' AND brandId in (".implode(",", $removedBrands).") AND type='".$type."'");
                foreach(Brands::find()->where(['id' => $removedBrands])->all() as $brand)
                    $brand->unsetProducts($id, $type);
            }
            foreach($_POST['Brands'] as $brand){
                if(!StoreBrands::find()->where(['storeId' => $id, 'brandId' => $brand, 'type' => $type])->one()){
                    $storeBrand = new StoreBrands;
                    $storeBrand->storeId = $id;
                    $storeBrand->brandId = $brand;
                    $storeBrand->type = $type;
                    if($storeBrand->save(false))
                        Brands::findOne($brand)->setProducts($id, $type);
                }
            }
        }
    	return $this->render('brands', compact('store', 'type'));
    }

public function actionAllowedbrands($id,$type='b2c'){
    $store = Stores::findOne($id);
    if(isset($_POST['Brands'])){
        StoreBrandsVisibility::deleteAll("storeId='$id'");
        foreach($_POST['Brands'] as $brand){
            $storeBrandsVisibility = new StoreBrandsVisibility;
            $storeBrandsVisibility->brandId = $brand;
            $storeBrandsVisibility->storeId = $id;
            $storeBrandsVisibility->save(false);
        }
    }
    return $this->render('brands-visibility', compact('store', 'type','id'));
    
}
    public function actionProducts($id, $type="b2c"){ 
        $store = Stores::findOne($id);
        if(isset($_REQUEST['Products'])){ 
            $_REQUEST['Products'] = is_array($_REQUEST['Products'])? $_REQUEST['Products'] : array();
            $products = array_flip($_REQUEST['Products']);
            $sortedProducts = [];
            foreach($products as $selection => $index)
                $sortedProducts[(int)$selection] = (is_numeric($selection))? 'checked' : 'unchecked';
            // $previousProducts = ArrayHelper::map(StoreProducts::find()->where(['storeId' => $id, 'type' => $type])->all(), 'id', 'productId');
            // $removedProducts = array_diff(array_values($previousProducts), $_POST['Products']);
            // if(!empty($removedProducts))
            //     StoreProducts::deleteAll("storeId='$id' AND productId in (".implode(",", $removedProducts).") AND type='".$type."'");

            foreach($sortedProducts as $productId => $status){
                if($status=="unchecked"){
                    StoreProducts::deleteAll("storeId='$id' AND productId ='$productId' AND type='".$type."'");
                }else{
                    if(!StoreProducts::find()->where(['storeId' => $id, 'productId' => $productId, 'type' => $type])->one()){
                        $storeProduct = new StoreProducts;
                        $storeProduct->storeId = $id;
                        $storeProduct->productId = $productId;
                        $storeProduct->type = $type;
                        $storeProduct->enabled = '1';
                        $storeProduct->save(false);
                    }
                }
            }
        }
        //var_dump($store->products); die;
        return $this->render('products', compact('store', 'type'));
    }

    public function actionOrders()
    {
        
        /*$dataProvider = new ActiveDataProvider([
            'query' => Orders::find()->where(['type' => 'b2b'])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel = new OrdersSearch();
        return $this->render('orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/

        $searchModel = new \common\models\search\OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['type'=>'b2b'])->orderBy(['id'=>SORT_DESC,]);
        return $this->render('orders', compact('dataProvider', 'searchModel'));

    }

    public function actionExport()
    {
        
        $this->render('csvexport');
    }

    public function actionOrderdetail($id)
    {
             
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId' => $id]),
        ]);
        return $this->render('order-detail', [
            'model' => \common\models\Orders::findOne($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCancel($id){
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        if($selectedPromotion = StorePromotions::find()->where(['storeId'=>$storeId,'promotionId'=>$id,'active'=>1])->one()){
            $selectedPromotion->active = 0;
            $selectedPromotion->save(false);
        }    
        $this->redirect(['promotions']);
    }

     public function actionPromotions()
    {
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $search = new ConsumerPromotions;
        $dataProvider = new ActiveDataProvider([
            'query' => ConsumerPromotions::find()->where('id not in (SELECT promotionId from StorePromotions where storeId = "'.$storeId.'") and status="published"')
            ]);

        return $this->render('promotions',compact('dataProvider'));
    } 
    public function actionView($id)
    {
        $promotion = ConsumerPromotions::findOne($id);
        $user = User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $search = new ConsumerPromotionProducts;
        $dataProvider = new ActiveDataProvider([
            'query' => ConsumerPromotionProducts::find()->join('JOIN','StoreProducts as sp','ConsumerPromotionProducts.productId = sp.productId')->where('promotionId = '.$id.' and sp.storeId = '.$storeId.''),
            'pagination' => [
                'pageSize' => 10,
            ],    
            ]);

        if(StorePromotions::find()->where(['promotionId'=>$id,'storeId'=>$storeId,'active'=>1])->one())
            $selected = 1;
        else
            $selected = 0;

        return $this->render('promotion-view',compact('dataProvider','promotion','selected'));
    }

    public function actionAccept($id)
    {
        $user = User::findOne(Yii::$app->user->Id);
        $storeid = $user->store->id;
        if (isset($_POST['ConsumerPromotions']))  {
            $storePromotions = new StorePromotions();
            $storePromotions->storeId =  $storeid;
            $storePromotions->promotionId = $id;
            $storePromotions->startDate = Helper::convertpromotiondate($_POST['ConsumerPromotions']['fromDate']);
            $storePromotions->endDate = Helper::convertpromotiondate($_POST['ConsumerPromotions']['toDate']);

            //var_dump($storePromotions->startDate);
            //var_dump($storePromotions->endDate);die();

            $storePromotions->active = "1";
            $storePromotions->save(false);
            return $this->redirect(\yii\helpers\Url::to(['b2b/promotions']));
        }
    }   
}
