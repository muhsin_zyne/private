<?php

namespace backend\controllers;

use Yii;
use common\models\ConferenceTrays;
use common\models\Categories;
use common\models\ConferenceTrayProducts;
use common\models\search\ConferencesTraysSearch;
use common\models\Products;
use common\models\ProductCategories;
use common\models\AttributeValues;
use common\models\Attributes;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
/**
 * ConferenceTraysController implements the CRUD actions for ConferenceTrays model.
 */
class ConferenceTraysController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ConferenceTrays models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConferencesTraysSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ConferenceTrays model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ConferenceTrays model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tray = new ConferenceTrays();

        if ($tray->load(Yii::$app->request->post())) {

            //var_dump(Yii::$app->request->post());die();

            $tray->title = Yii::$app->request->post()["ConferenceTrays"]["title"];
            $tray->image = Yii::$app->request->post()["ConferenceTrays"]["image"];
            $tray->trayId = Yii::$app->request->post()["ConferenceTrays"]["trayId"];
            $tray->conferenceId = Yii::$app->request->post()["ConferenceTrays"]["conferenceId"];
            if ($tray->save()) {
                foreach ($_POST['Products']['id'] as  $id) {
                    $trayProducts = new ConferenceTrayProducts;
                    $trayProducts->conferenceTrayId  = $tray->id;
                    $trayProducts->productId  = $id;
                    $trayProducts->offerCostPrice  = $_POST['Products'][$id]['offerCostPrice'];    
                    $trayProducts->save(false);
                }
            } 

            return $this->redirect(['view', 'id' => $tray->id]);    
        }

        else {

            if(\Yii::$app->request->isAjax){
                return $this->renderAjax('_form',compact('tray'));
            }
            else {    
                return $this->render('create', [
                'tray' => $tray,
                ]);
            } 
        }
    }

    /**
     * Updates an existing ConferenceTrays model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $tray = $this->findModel($id);

        if ($tray->load(Yii::$app->request->post())){

            if(isset($_POST['Products']['deletedItems'])) {
                //var_dump($_POST['Products']['deletedItems']);die();
                ConferenceTrayProducts::deleteAll('id in ('.implode($_POST['Products']['deletedItems']).')');
            }

            if($tray->save()) {

                foreach ($_POST['Products']['id'] as  $id) {
                    if(isset($_POST['Products'][$id]['newitem'])){

                        $trayProducts = new ConferenceTrayProducts;
                        $trayProducts->conferenceTrayId  = $tray->id;
                        $trayProducts->productId  = $id;
                        $trayProducts->offerCostPrice  = $_POST['Products'][$id]['offerCostPrice'];    
                        $trayProducts->save(false);
                    }
                }        
                return $this->redirect(['view', 'id' => $tray->id]);
            } 
        }

        else {
            return $this->render('update', [
                'tray' => $tray,
            ]);
        }
    }

    /**
     * Deletes an existing ConferenceTrays model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetproducts(){
        $keyword = $_REQUEST['keyword'];
        //var_dump($keyword);die();
        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    //->select('p.id, av.value')
                    ->from('Products as p')
                    ->join('JOIN',
                        'AttributeValues as av',
                        'av.productId = p.id'
                    )
                    ->join('JOIN',
                        'Attributes as a',
                        'a.id = av.attributeId'
                    )
                    ->join('JOIN',
                        'StoreProducts as sp',
                        'sp.productId = p.id'
                    )
                    ->groupBy('p.id')
                    
                    ->where("(p.sku = '".$keyword."' OR av.value like '%".$keyword."%') AND a.code='name' AND sp.type='b2b'");
                    
                    if(empty($keyword))
                        $products->andWhere("1!=1");

                    $products->all();

                    

                    //var_dump(json_encode(ArrayHelper::map($products->all(),'id','name','sku')));
            $data = [];
            foreach($products->all() as $product)
                $data[$product->id] = ['id'=>$product->id,'name'=>$product->name, 'sku'=>$product->sku, 'price'=>$product->price,'cost_price'=>$product->cost_price];
        return json_encode($data);      
    }  

    public function actionGetcategoryproducts(){

        //var_dump($_REQUEST['selected']);die();
       // var_dump($_REQUEST['keyword']);die();
        $query = new ActiveQuery('common\models\Products');
        $products = $query
                    ->from('Products as p')
                    ->join('JOIN',
                        'ProductCategories as pc',
                        'pc.productId = p.id'
                    )
                    ->join('JOIN',
                        'StoreProducts as sp',
                        'sp.productId = p.id'
                    )
                    ->groupBy('p.id')
                    ->where("(pc.categoryId in (".implode(',', $_REQUEST['categoryIds']).") AND sp.type='b2b')");
                    $products->all();
            $data = [];        
            foreach($products->all() as $product)
                $data[$product->id] = ['id'=>$product->id, 'name'=>$product->name, 'sku'=>$product->sku, 'price'=>$product->price,'cost_price'=>$product->cost_price];
            return json_encode($data);     
            //var_dump(json_encode($data));die();
    }    

    public function actionGetcategories(){ 
        $keyword = $_REQUEST['keyword'];
        $categories = ArrayHelper::map(Categories::find()->where("title like '%".$keyword."%'")->all(),'id','title');  
        foreach($categories as $key=>$category) 
            $data[$key] = ['id'=>$key, 'name'=>$category];
           
        return json_encode($data);
            //$data[$category->id] = ['id'=>$category->id, 'name'=>$category->title];
        //var_dump(json_encode($data));die();
        //return json_encode($categories);
    }

    public function actionTest(){  
        
        $file = $_FILES['csv']['tmp_name'];
        $data=fopen($file,'r');
        $tray = Array();
        $products = Array();
        $notFound = Array();
        $t = 0;

        /*while($row = fgets($data)) {
            if (strpos($row, 'Tray:') !== false){
                $t++;
                $trayDetails = explode(",", $row);
                var_dump($trayDetails);
            } 

            var_dump($row);   
        }die();*/



        while($row = fgets($data)) { 
            if (strpos($row, 'Tray:') !== false){
                $t++;
                $trayDetails = explode(",", $row);
                //var_dump($trayDetails);die();
                $tray[$t] = [ 
                          'name'=>$trayDetails[0], 
                          'trayId'=>$trayDetails[1],
                        ];

                //var_dump($tray[$t]['products']);die();        
            }

            else {
                if(strcmp($row,",,")==1){
                    $product_details = explode(",", $row);
                    if ($product = Products::find()->where(['sku'=>$product_details[0]])->one()) {
                        $tray[$t]['products'][] = [
                                        'id' =>$product->id,
                                        'name'=>$product->name,
                                        'sku'=>$product_details[0], 
                                        'costprice'=>$product->cost_price,
                                        'offerCostPrice'=>$product_details[1],
                                    ];
                    }
                    else{
                        $notFound[] = [
                            'sku'=>$product_details[0],
                        ];
                    }                
                }
            }
        }        

        return json_encode(['notfound' => $notFound, 'uploadedTrays'=> $tray]);
    }    

/**
     * Finds the ConferenceTrays model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ConferenceTrays the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($tray = ConferenceTrays::findOne($id)) !== null) {
            return $tray;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
