<?php

namespace backend\controllers;

use Yii;
use common\models\GiftRegistry;
use common\models\search\GiftRegistrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\db\ActiveQuery;
/**
 * GiftRegistryController implements the CRUD actions for GiftRegistry model.
 */
class GiftRegistryController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GiftRegistry models.
     * @return mixed
     */
    public function actionIndex()
    {
//        print_r(\common\models\Products::find() ->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')
//                ->join('JOIN', 'Attributes as a', 'a.id = av.attributeId')->all());
        //exit;
        $searchModel = new GiftRegistrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GiftRegistry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $giftpdts = new \yii\data\ActiveDataProvider([
            'query' => \common\models\GiftRegistryProducts::find()->where(['giftRegistryId'=>$id])->orderBy('id'),
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'giftpdts'=>$giftpdts,
        ]);
    }

    /**
     * Creates a new GiftRegistry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GiftRegistry();
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/gift-registry/";    
        $image = \yii\web\UploadedFile::getInstance($model, 'image');
        $model->scenario = 'upload';
        if ($model->load(Yii::$app->request->post())) {
            if(!empty($image)) {
                $array= explode(".", $image->name);
                $ext = end($array);
                $image_name = Yii::$app->security->generateRandomString() . ".{$ext}";
                $savepath = Yii::$app->params['uploadPath'] . $image_name; 
            }    
            $model->weddingDate = Yii::$app->formatter->asDatetime($model->weddingDate);
            $model->activeFrom = Yii::$app->formatter->asDatetime($model->activeFrom);
            $model->expiresOn = Yii::$app->formatter->asDatetime($model->expiresOn);
            $model->image=\yii\web\UploadedFile::getInstance($model, 'image');
            if($model->validate(false)) {
                if(!empty($image)) {
                 $image->saveAs($savepath);
                 $model->image= "/store/gift-registry/".$image_name;
                }
                 $model->save(false);
                 if(isset($_POST['Products']['id'])) {
                    foreach ($_POST['Products']['id'] as $product_id) {
                        $gift_registry_product=new \common\models\GiftRegistryProducts;
                        $gift_registry_product->productId=$product_id;
                        $gift_registry_product->giftRegistryId= $model->id;
                        $gift_registry_product->save(false);     
                    }
                 }    
                 
                return $this->redirect(['view', 'id' => $model->id]);
            } 
            else {
            return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GiftRegistry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/gift-registry/";    
        $image = \yii\web\UploadedFile::getInstance($model, 'image');
        $old_image= $model->image;
        if ($model->load(Yii::$app->request->post())) {
            if (isset($_POST['Products']['deletedItems'])) {
                \common\models\GiftRegistryProducts::deleteAll('id in (' . implode($_POST['Products']['deletedItems']) . ')');
            }
            if(!empty($image->size)) {
                $model->scenario = 'upload';
                $type = $image->type;
                $size = $image->size;
                $array = explode(".", $image->name);
                $ext = end($array);
                $image_name = Yii::$app->security->generateRandomString() . ".{$ext}";
                $savepath = Yii::$app->params['uploadPath'] . $image_name;
                $model->image=\yii\web\UploadedFile::getInstance($model, 'image');
            }
            $model->weddingDate = Yii::$app->formatter->asDatetime($model->weddingDate);
            $model->activeFrom = Yii::$app->formatter->asDatetime($model->activeFrom);
            $model->expiresOn = Yii::$app->formatter->asDatetime($model->expiresOn);
            if($model->validate()) {
                if(!empty($image)) {
                    $model->image= "/store/gift-registry/".$image_name;
                    $image->saveAs($savepath);
                }
                else {
                   $model->image=$old_image;
                }
                $model->save(false);
                if(isset($_POST['Products']['id'])) {
                    foreach ($_POST['Products']['id'] as $product_id) {
                        if (isset($_POST['Products'][$product_id]['newitem'])) {
                            $gift_registry_product = new \common\models\GiftRegistryProducts;
                            $gift_registry_product->productId = $product_id;
                            $gift_registry_product->giftRegistryId = $model->id;
                            $gift_registry_product->save(false);
                        } else {
                           
                        }
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } 
            else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GiftRegistry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GiftRegistry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GiftRegistry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GiftRegistry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetproducts() {
        set_time_limit(0);
        $keyword = $_REQUEST['keyword'];
        $products = \common\models\Products::find()
                ->join('JOIN', 'AttributeValues as av', 'av.productId = Products.id')
                ->join('JOIN', 'Attributes as a', 'a.id = av.attributeId')
                ->join('JOIN','StoreProducts as sp','sp.productId = Products.id')
                ->andwhere("(Products.sku = '" . $keyword . "' OR av.value like '%" . $keyword . "%') AND a.code='name' AND sp.enabled=1 AND sp.storeId=".Yii::$app->user->identity->store->id)
                //->andwhere("a.code = 'byod_only' and av.value='Yes'")
                ->groupBy('Products.id')
                ->all();
                
//        print_r($products);
//        exit;
//        if (empty($keyword))
//            $products->andWhere("1!=1");

        //$products=$products->all();
        //print_r($products->attributes);
       // exit;
        \Yii::$app->response->format= 'json';
        $data=[];
        foreach ($products as $product) {
            if(isset($product->byod_only) && $product->byod_only=='Yes')
                break;
            $data[$product->id] = ['id' => $product->id,'storeId'=>$product->storeId, 'name' => $product->name, 'sku' => $product->sku, 'price' => $product->price, 'cost_price' => $product->cost_price];
        }
        return $data;
    }

}
