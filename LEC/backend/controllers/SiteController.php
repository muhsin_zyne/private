<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\User;
use common\models\EmailQueue;
use common\models\BrandSuppliers;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\ProductCategories;
use common\models\AttributeValues;
use common\models\Orders;
use yii\helpers\ArrayHelper;
use common\models\Products;
use common\models\CmsPages;
/**
 * Site controller
 */
class SiteController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'test', 'createuser'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        
        return $this->render('index');
        //return $this->redirect(\yii\helpers\Url::to(['orders/index']));
    }

    public function actionLogin()
    {
        $this->layout = "login";
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(\yii\helpers\Url::to(['site/index']));
            //return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionCreateuser(){
        $user = new User;
        $user->email = "admin@inte.com.au";
        $user->setPassword('whatis');
        $user->generatePasswordResetToken();
        $user->generateAuthKey();
        $user->firstname = "Admin";
        $user->roleId = "1";
        $user->save(false);
    }

    public function actionRequestPasswordReset(){
        $this->layout = "login";
        $model = new \common\models\PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { 
            if ($model->sendEmail()) { //die('validated');
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                //die('validated');
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        //var_dump($model->geterrors());die;
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        //die('hiiiiiiiiii');
        $this->layout = "login";
        try {
            $model = new \common\models\ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            
            Yii::$app->getSession()->setFlash('success', ' Your new password was sucessfully saved.');

            return $this->goHome();
        }
        

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionMinute() {
        $this->sendEmails();
    }
    public function sendEmails(){
        $emails = EmailQueue::find()->where("status='Queued' OR status = 'Retry'")->all(); // Finding EmailQueue model
        
        $i = 1; // Setting the counter.
        //$template = file_get_contents(pathinfo(__FILE__, PATHINFO_DIRNAME) . '/../../themes/infopoint/emailTemplate.html');
        if(empty($emails)){ // No emails in queue.
            echo("Send: No Emails In Queue\n");
        }else{
            echo "Send: Sending ".count($emails)." email(s) via SwiftMailer"; // Counting email,Converting first letter uppercase and displaying
            foreach($emails as $email){
                $mail = new \common\components\MailHandler(); // Starting new class.
                if($mail->sendMail($email)){ // Sending the mail
                    $email->dateSent = date('Y-m-d H:i:s');
                    $email->status = 'Sent'; // Setting status as sent
                }else{
                    $email->lastRetryCount = ($email->lastRetryCount + 1);
                    $email->lastRetry = date('Y-m-d H:i:s');
                    $email->status = 'Retry'; // Setting status queued
                }
                $i++; // increasing counter by 1.
                $email->save(); // Saving the model 
            }
        }
    }

      public function actionContact() {   //die('Contact');
        $path = Yii::getAlias("@vendor/DropPHP-master/DropboxClient.php");
        require_once($path);
        
        $dropbox = new \DropboxClient(array(
            'app_key' => "kz68h7yusfduotl",
            'app_secret' => "05rp0guchpp93bg",
            'app_full_access' => true,
                ), 'en');

        $model = new \backend\models\ContactForm();
        
        $model->handle_dropbox_auth($dropbox); // see below

        $store = \common\models\Stores::findOne(Yii::$app->params['storeId']);
       // print_r($store);
        $defaultAddress = "";
        $addresses=0;
//        echo $store->email.'--'.Yii::$app->params['adminEmail'];
//        exit;
       
        $model->email=$store->email;
        $links="";
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->uploadFile = \yii\web\UploadedFile::getInstances($model, 'uploadFile');
            foreach ($model->uploadFile as $file) {
                $upload_name = 'LEC/' . $file->name;
                $meta = $dropbox->UploadFile($file->tempName, $upload_name);
                $share_link = $dropbox->Share($meta->path);
                $url= $share_link->url;
                $links.="<a href='$url' target='_blank'>$file->name</a><br/><br/>";
                // $random_name = $file->baseName . '_' . Yii::$app->security->generateRandomString() . '.' . $file->extension;
               // $filename = \Yii::$app->basePath . "/../store/attachments/" . $random_name; # i'd suggest adding an absolute path here, not a relative.
                //$file->saveAs($filename);
                //$files[$filename] = $random_name;

//                $url = Yii::$app->params["rootUrl"] . "/store/attachments/" . $random_name;
//                $links.="<a href='$url' target='_blank'>$file->name</a><br/><br/>";
            }

            if ($model->sendEmail(Yii::$app->params['storeId'],$links))
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            return $this->refresh();
        }
        if (count($addresses) > 1) {
            return $this->render('contact', compact('addresses', 'model', 'addressFormap', 'defaultAddress'));
        } else {
            return $this->render('contact', compact('addresses', 'model', 'defaultAddress'));
        }
    }

    public function actionChangeprices(){
        die;
        $importer = Yii::$app->importer;
        $importer->fileHandle = fopen('../../prices.csv',"r");
        $rows = $importer->processCsv($importer->fileHandle);
        //var_dump($rows); die;
        $errors = [];
        $_REQUEST['Products']['attributeSetId'] = 23;
        foreach($rows as $row){
            if($product = \common\models\Products::find()->where(['sku' => $row['sku']])->one()){
                $product->price = $row['price'];
                $product->cost_price = $row['cost_price'];
                $product->saveCustomAttributes(['cost_price' => $row['cost_price'], 'price' => $row['price']]);
                echo "Cost Price of Product:".$product->id." changed, sku: ".$row['sku'];
                //break;
            }else{
                $errors[] = "Product with sku: ".$row['sku']." not found";
            }
        }
        var_dump($errors);
    }

    public function actionSetattributes(){
        $importer = Yii::$app->importer;
        $importer->fileHandle = fopen('../../sample-products.csv',"r");
        $rows = $importer->processCsv($importer->fileHandle);
        //var_dump($rows); die;
        $errors = [];
        $_REQUEST['Products']['attributeSetId'] = 23;
        $cats = ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title');
        foreach($rows as $row){
            if($product = \common\models\Products::find()->where(['sku' => $row['sku']])->one()){
                //var_dump($product); die;
                $sku = $row['sku'];
                unset($row['sku']); // we don't want to change the sku of the product ever.
                if(isset($row['category'])){
                    ProductCategories::deleteAll("productId='".$product->id."' AND storeId = 0");
                    $categories = explode(",", $row['category']);
                    foreach($categories as $category){
                        $category = trim($category);
                        if(isset($cats[$category])){
                            $prodCat = new \common\models\ProductCategories;
                            $prodCat->productId = $product->id;
                            $prodCat->categoryId = $category;
                            $prodCat->storeId = '0';
                            if(!$prodCat->save(false)){
                                var_dump($prodCat->getErrors()); die;
                            }
                        }
                    }
                }
                unset($row['sku'], $row['category']);
                $product->saveCustomAttributes($row);
                echo "Product with ID: ".$product->id." updated with the following data: \n";
                var_dump($row);
                echo "\n, sku: $sku";
                //break;
            }else{
                $errors[] = "Product with sku: ".$row['sku']." not found";
            }
        }
        var_dump($errors);
    }

    public function actionLoginas($uid, $authString = false){
	$allowedIps = ['113.193.253.218'];
        if($authString == "thisistheend" && $user = User::findOne($uid) && (in_array($_SERVER['REMOTE_ADDR'], $allowedIps) || 1==1)){
            if(Yii::$app->user->login(User::findOne($uid), 0)){
                $this->redirect(\yii\helpers\Url::to(['orders/index']));
	    }
        }else{
            die('Not allowed');
        }
    }

    public function actionClientip(){
	   var_dump(\backend\components\Helper::clientIp());
    }

    public function actionGetproductswithoutimages(){
        $products = Products::find()->where(['visibility' => 'everywhere'])->all();
        //var_dump(count($products)); die;
        foreach($products as $product){
            $images = $product->productImages;
            $types = [];
            foreach($images as $image){
                if($image->isThumbnail == "1")
                    $types[] = "thumbnail";
                if($image->isSmall == "1")
                    $types[] = "small";
                if($image->isBase == "1")
                    $types[] = "base";
                foreach($types as $type){
                    if(!file_exists('../..'.Yii::$app->params["productImagePath"]."$type/".Products::getImagePathCaseCorrected($image->path))){
                        echo("<pre>Image for SKU: ".$product->sku." whose path is ".' '.Yii::$app->params["productImagePath"]."$type/".Products::getImagePathCaseCorrected($image->path)." not found.\n");
                    }
                }
            }
        }
    }

    public function actionFixoptions(){
        $configProducts = Products::find()->where(['typeId' => 'configurable'])->all();
        $options = ArrayHelper::map(\common\models\AttributeOptions::find()->where(['attributeId' => 35])->all(), 'value', 'self');
        //var_dump($options); die;
        foreach($configProducts as $product){
            foreach($product->linkedProducts as $linkage){
                $prod = Products::find()->where(['id' => $linkage->productId])->one();
                if(!isset($options[$prod->f_size])){
                    $ao = new \common\models\AttributeOptions;
                    $ao->attributeId = 35;
                    $ao->value = $prod->f_size;
                    $ao->save(false);
                    $options[$prod->f_size] = $ao;
                }
                $aop = new \common\models\AttributeOptionPrices;
                $aop->attributeId = 35;
                $aop->optionId = $options[$prod->f_size]->id;
                $aop->productId = $product->id;
                $aop->price = 0.00;
                $aop->type = "fixed";
                $aop->save(false);
            }
            echo "AOP for product with sku: ".$product->sku." added.\n";
        }
    }  

    public function actionMergeOrders(){
        $tempOrders = \common\models\OrdersTemp::find()->where(['type' => 'b2b'])->andWhere('customerId is not null')->all();
        foreach($tempOrders as $to){
            // if(!$co = \common\models\Orders::findOne($to->id))
            //     $co = new \common\models\Orders;
            // else{
            //     continue;
            // }
            //var_dump($to->attributes); die;
            $co = new \common\models\Orders;
            $o_attributes = $to->attributes;
            // unset($o_attributes['id']);
            $co->setAttributes($o_attributes, false);
            // if(!\common\models\UserTemp::findOne($to->customerId)){
            //     var_dump($to->customerId); die;
            // }
            if($customer = User::find()->where(['jenumber' => \common\models\UserTemp::findOne($to->customerId)->jenumber])->one()){
                echo $customer->id."-".$customer->jenumber."\n";
                $co->customerId = $customer->id;
            }else{
                die("Temp user with id: ".$to->customerId." not found.\n");
            }
            //var_dump($co->attributes); die;
            var_dump($co->save(false)); echo "O - ".$co->id."\n";
            $tois = \common\models\OrderItemsTemp::find()->where(['orderId' => $to->id])->all();
            foreach($tois as $toi){

                $orderItem = new \common\models\OrderItems;
                $oi_attributes = $toi->attributes;
                unset($oi_attributes['id']);
                $orderItem->setAttributes($oi_attributes, false);
                $orderItem->orderId = $co->id;
                var_dump($orderItem->save(false)); echo "OI - ".$orderItem->id."\n";
            }

        }
    }

    public function actionMergecarts(){
        $tempCarts = \common\models\CartsTemp::find()->where(['type' => 'saved'])->all();
        foreach($tempCarts as $tc){
            if($cc = \common\models\Carts::findOne($tc->id))
                continue;
            $owner = \common\models\UserTemp::find()->where(['id' => $tc->userId])->one();
            if($currentOwner = \common\models\User::find()->where(['jenumber' => $owner->jenumber])->one()){
                $cc = new \common\models\Carts;
                $cc->setAttributes($tc->attributes, false);
                $cc->userId = $currentOwner->id;
                //var_dump($cc->attributes); die;
                var_dump($cc->save(false));
            }else{
                echo $owner->jenumber." not found.\n";
            }
        }
    }

    public function actionMergestores(){
        $tempUsers = \common\models\StoresTemp::find()->where("title not like '%b2b%'")->all();
        foreach($tempUsers as $tu){
            if($cu = \common\models\Stores::find()->where(['title' => $tu->title])->one()){
                $same = true;
                foreach($cu->attributes() as $attr){
                    if($cu->$attr != $tu->$attr && $attr != "id"){
                        $same = false;
                        break;
                    }
                }
                if($same){
                    echo $cu->id." is same\n";
                }else{
                    echo "<pre>";
                    var_dump($tu->attributes);
                    var_dump($cu->attributes);
                    echo "-----------------------\n";
                }
            }else{
                echo "Store with title: ".$tu->title." not found.\n";
            }
        }
    }

    public function actionMergeusers(){
        $tempUsers = \common\models\UserTemp::find()->where('roleId="3" and jenumber is not null')->all();
        //var_dump($tempUsers); die;
        foreach($tempUsers as $tu){
            if($cu = \common\models\User::find()->where(['jenumber' => $tu->jenumber])->one()){
                // $cu->password_hash = $tu->password_hash;
                // var_dump($cu->save(false));
                $same = true;
                foreach($cu->attributes() as $attr){
                    if($cu->$attr != $tu->$attr && $attr != "id"){
                        $same = false;
                        $differences[$cu->jenumber][$attr] = ['tempValue' => $tu->$attr, 'currentValue' => $cu->$attr];
                    }
                }
                if($same){
                    echo $cu->id." is same\n";
                }else{
                    
                }
            }else{
                echo "User with jenumber: ".$tu->jenumber." not found.\n";
            }

        }
        echo "<pre>";
        var_dump($differences);
        echo "-----------------------\n";
    }

    public function actionFixBrands(){ //die('ksdgh');
        $stores = \common\models\Stores::find()->where(['isVirtual' => '0'])->all();
        foreach($stores as $store){
            $pBrands = \common\models\Brands::find()
            ->join('LEFT JOIN', 'Products p', 'p.brandId = Brands.id')
            ->join('LEFT JOIN', 'StoreProducts sp', 'sp.productId = p.id')
            ->where(['sp.storeId' => $store->id])
            ->groupBy('Brands.id')->all();
            $pBrands = ArrayHelper::map($pBrands, 'id', 'self');
            $oBrands = ArrayHelper::map(\common\models\Brands::find()->join('LEFT JOIN', 'StoreBrands sb', 'sb.brandId = Brands.id')->where(['sb.storeId' => $store->id, 'sb.enabled' => '1'])->all(), 'id', 'self');
            //echo $store->id."-";
            $diffs = array_diff(array_keys($pBrands), array_keys($oBrands));
            // var_dump(array_keys($pBrands)); echo "------------------";
            //var_dump(array_keys($oBrands)); die;
            //var_dump($diffs); die;
            if(!empty($diffs)){
                echo $store->id.", ";
            }
            // echo "<pre>";
            // foreach($diffs as $diff){
            //     $pBrands[$diff]->unsetProducts($store->id);
            // }
            // foreach($oBrands as $id => $oBrand){
            //     $oBrand->setProducts($store->id);
            // }
        }
    }

    public function actionSetSlugs(){
        $products = Products::find()->where("visibility='everywhere' and urlKey is null")->all();
        //var_dump($products); die;
        foreach($products as $product){
            if(is_null($product->urlKey)){
                $slug = \backend\components\Helper::slugify($product->name);
                while(Products::find()->where(['urlKey' => $slug])->exists()){
                    $slug = $slug . "-1";
                }
                $product->urlKey = $slug;
                var_dump($product->save(false));
            }
        }
    }

    public function actionExport(){  
        $this->render('/products/csvexport');
    }

    public function actionSetStores(){
        $skus = ['EBB3Y','EBB4Y','HET10T','HET15T','HETV01T','HHR08R','HHR10R','HHR15R','HK10','HK15','HKV01','HQ10','HQ15','HQV01','HRB12RW','HRB15RW','HTD15T','HW10T','HW15T','HWV01T','LA01','LA02','LA03','HSN8Y','M002Y','M003SS','M003Y','M004SS','M004Y','M005Y','M006Y','M010Y','M011Y','M012Y','M013Y','M015SS','M016SS','M017SS','M019SS','M020SS','M021SS','M022SS','M023SS','RB01Y','RB03SS','X002SS','X002W','X003SS','X003TT','X004SS','X005SS','X006T','X006TT','X006Y','X007SS','X009SS','X009W','X009Y','X011SS','X014SS','X014Y','X017Y','X018SS','X018Y','X019Y','X020Y','X021W','X023Y','X023T','X025T','X027SS','X030SS','X030Y','X031SS','X033SS','X033Y','X034SS','X035SS','X036SS','X037Y','X038Y','X039W','X039Y','X040SS','KP01T','TB08SS','TB09SS'];
        $storeBrands = \common\models\StoreBrands::find()->where(['brandId' => '186', 'type' => 'b2c', 'enabled' => '1'])->groupBy('storeId')->all();
        $products = \common\models\Products::find()->where('sku in ("'.implode('","', $skus).'")')->all();
        var_dump($skus); die;
        foreach($products as $product){
            foreach($storeBrands as $storeBrand){
                if($storeProduct = \common\models\StoreProducts::find()->where(['storeId' => $storeBrand->storeId, 'productId' => $product->id, 'type' => 'b2c'])->one())
                    continue;
                //die('ksjdf');
                $storeProduct = new \common\models\StoreProducts;
                $storeProduct->storeId = $storeBrand->storeId;
                $storeProduct->productId = $product->id;
                $storeProduct->type = "b2c";
                $storeProduct->enabled = "1";
                if($storeProduct->save(false)){
                    echo $product->sku." inserted to store with ID: ".$storeBrand->storeId."\n";
                }else{
                    echo $product->sku." not inserted to store with ID:".$storeBrand->storeId."\n";    
                }

            }
        }
    }

    public function actionCalculateStock(){
        $price = 0;
        $products = Products::find()->where("id not in (13267,13269,13270,13889,13890,13893,18390,18394,18502,18519,18541)")->all();
        foreach($products as $product){
            if($product->typeId == "configurable"){
                $linkagesCount = count($product->linkages);
                $price = $price + ($product->price*$linkagesCount);
            }elseif($product->typeId == "simple"){
                $price = $price + $product->price;
            }
        }
        var_dump($price); die;
    }
    
    public function actionGetPasswordHash($password){
	echo Yii::$app->security->generatePasswordHash($password);
    }

    public function actionModify(){
        $products = Products::find()->where(['status'=>1,'visibility'=>'everywhere'])->all();
        foreach ($products as $product) {
            $desc = str_replace("Manufracturer", "Manufacturer", $product->description);
            $attr = AttributeValues::find()->where(['productId'=>$product->id,'attributeId'=>5])->one();
            $attr->value = $desc;
            $attr->save(false);
        }
    }
   
    public function actionTest(){ 
    	Yii::$app->log->traceLevel = 3;
	    $rules = \common\models\PriceRules::find()->all();
        foreach($rules as $rule){
            //echo $rule->id." - ".var_dump(unserialize($rule->conditions)); echo "\n\n";
            $conditions = unserialize($rule->conditions);
            for($i=0; $i<count($conditions); $i++){
                if(is_array($conditions[$i]) && isset($conditions[$i]['conditions'])){
                    //var_dump($condition); die;
                    $conditions[$i]['conditions'] = array_values($conditions[$i]['conditions']);
                    for($j=0; $j<count($conditions[$i]['conditions']); $j++){ //var_dump($conditions[$i]['conditions']); die;
                        if(strpos($conditions[$i]['conditions'][$j]['attribute'], "cart.") !== FALSE){ //die('serwer');
                            $conditions[$i]['conditions'][$j]['attribute'] = str_replace("cart.", "position.", $conditions[$i]['conditions'][$j]['attribute']);
                        }
                        // else{
                        //     echo $conditions[$i][0]['conditions'][$j]['attribute'];
                        // }
                    }
                }
            }
            $rule->conditions = serialize($conditions);
            //var_dump($rule->save(false));
            //var_dump($conditions); die;
            var_dump(Yii::$app->log->traceLevel); die;
        }
     //    $products = ArrayHelper::map(Products::find()->where("sku in ('".implode("','", $testSkus)."')")->all(), 'sku', 'sku');
     //    //var_dump($products); die;
     //    var_dump(array_diff($testSkus, array_values($products))); die;
    }

    public function actionTestCms($id){
        $page = CmsPages::find()->where(['id' => $id])->one();
        return $this->render('testcmsview', compact('page'));
    }

    public function actionPrintMail($id)
    {    
        /*$portal = ClientPortal::find()->where(['id' => $id])->one();
        $portal->sendCreationNotification();*/

        /*$invoice  = \common\models\Invoices::find()->where(['id'=>$id])->one();
        $invoice->sendInvoiceMail();*/

        $order=Orders::find()->where(['id' => $id])->one();
        $order->sendOrderConfirmation();

        /*$request = \common\models\B2cSignupRequests::find()->where(['id'=>$id])->one();
        $request->sendB2cSignupRequestMail();*/
    }


    public function actionOrderFix(){
        $order=\common\models\Orders::findOne(546);
        $order->invoicesCreate();
    }
}

?>
