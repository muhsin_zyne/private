<?php

namespace backend\controllers;

use Yii;
use common\models\CmsPages;
use common\models\StorePages;
use common\models\User;
use common\models\search\CmsPagesSearch;
use app\components\AdminController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\CmsImages;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * CmsController implements the CRUD actions for CmsPages model.
 */
class CmsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all CmsPages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user = User::findOne(Yii::$app->user->id);
        $searchModel = new CmsPagesSearch();
       
        $sparray  = [];
        //var_dump(Yii::$app->params['storeId']);die;
        if($user->roleId==3)
        {
            Yii::$app->params['storeId']=$user->store->id;
            $sparray=[];
            $storePages=StorePages::find()->where(['storeId'=>Yii::$app->params['storeId']])->all();
            foreach ($storePages as $key => $storePage) {
               $sparray[] = $storePage->cmspageId;
            }
            //var_dump($sparray);
            $dataProvider = new ActiveDataProvider([
                'query' => CmsPages::find()->where(['id'=>$sparray]),
            ]);
        }
        else
        {
           $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
        }
       
        if($user->roleId==3)
        {
            $sparray=[];
            $storePages=StorePages::find()->where(['storeId'=>Yii::$app->params['storeId']])->all();
            foreach ($storePages as $key => $storePage) {
               $sparray[] = $storePage->cmspageId;
            }
            //var_dump($sparray);die;
            $dataProvider = new ActiveDataProvider([
                'query' => CmsPages::find()->where(['id'=>$sparray]),
            ]);
        }
        else
        {
           $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
        }
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CmsPages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new CmsPages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionUrl()
    // {  
    //     $model = new CmsPages();
    //     Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/ckeditorimage/';
    //     $image = UploadedFile::getInstance($model, 'image');
    //     $ext = end((explode(".", $image->name)));
    //     $avatar = Yii::$app->security->generateRandomString().".{$ext}";
    //     $path = Yii::$app->params['uploadPath'] . $avatar;
    //     $model_image = new CmsImages();
    //     $model_image->title = $avatar;
    //     $model_image->save();
    //     $image->saveAs($path);
    // } 
    public function actionCreate()
    {

        $user = User::findOne(Yii::$app->user->id);
        $model = new CmsPages();
        if(isset($_POST['CmsPages']))
        { 
            //var_dump($_POST['CmsPages']) ;die;    
            if($user->roleId==1)
            {
                $stores_array=$_POST['CmsPages']['selected_store'];
            }
            elseif ($user->roleId==3) {
                //var_dump($user->store->id);die;
                $stores_array[] = $user->store->id;
            }    
            $model->content=$_POST['CmsPages']['resolvedContent'];
            $model->createdBy=Yii::$app->user->id;
            //var_dump($stores_array);die;
            //var_dump(Yii::$app->request->post());die();
            if ($model->load(Yii::$app->request->post()))
            {      //var_dump(Yii::$app->request->post());die;
                if(\Yii::$app->request->isAjax){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($model);
                }
                $model->save();
                foreach ($stores_array as  $value) {                    
                   $model_sp=new StorePages();
                   $model_sp->storeId=$value;
                   $model_sp->cmspageId=$model->id;
                   $model_sp->save();
                }
                Yii::$app->session->setFlash('success', 'Page created successfully');
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => $model->id]);

            }
            else{ 
                return $this->render('create', [ 'model' => $model,]); 
            }
        }
        
        else {
            return $this->render('create', [ 'model' => $model,]);                
            }
    }
    
    /**
     * Updates an existing CmsPages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(isset($_POST['CmsPages']))       
        {
            $stores_array=$_POST['CmsPages']['selected_store'];
             $model->content=$_POST['CmsPages']['resolvedContent'];
            StorePages::deleteAll(['cmspageId' => $id]);
            if ($model->load(Yii::$app->request->post()))
            {      //var_dump(Yii::$app->request->post());die;
                if(\Yii::$app->request->isAjax){
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($model);
                }
                $model->save();
                foreach ($stores_array as  $value) {                    
                   $model_sp=new StorePages();
                   $model_sp->storeId=$value;
                   $model_sp->cmspageId=$model->id;
                   $model_sp->save();
                }
                Yii::$app->session->setFlash('success', 'Page updated successfully');
                return $this->redirect(['index']);

            }
            else{ 
                return $this->render('update', [ 'model' => $model,]); 
            }
        }
        else {
            return $this->render('update', [ 'model' => $model, ]);
        }
    }

    /**
     * Deletes an existing CmsPages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Page deleted successfully');
        return $this->redirect(['index']);
    }

    /**
     * Finds the CmsPages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsPages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmsPages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionImageBrowse()// ckeditor image browse 
    { 
        $storeId=(isset(Yii::$app->params['storeId']))?Yii::$app->params['storeId']:0;
        $cmsImages=CmsImages::find()->where(['storeId'=>$storeId])->orderBy(['id' => SORT_DESC])->all();        
        return $this->renderPartial('image',compact('cmsImages'));
    }
    public function actionImageUpload()// ckeditor image upload
    {    
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        $uploadedFile = UploadedFile::getInstanceByName('upload'); 
        //$mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);
        $file = time()."_".$uploadedFile->name;
        $url=Yii::$app->params["rootUrl"]."/../store/cms/images/".$file; 
        $uploadPath = Yii::$app->params['uploadPath'].$file;
        $check = getimagesize($uploadedFile->tempName);
        $storeId=(isset(Yii::$app->params['storeId']))?Yii::$app->params['storeId']:0;
        $model_image = new CmsImages();
        $model_image->title = $file;
        $model_image->storeId=$storeId;
        $model_image->save();  

        //var_dump($uploadPath);die;
        //extensive suitability check before doing anything with the file…
        if ($uploadedFile==null)
        {
           $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
           $message = "The file is of zero length.";
        }
        else if($check['mime']!='image/jpeg' && $check['mime']!='image/jpg' && $check['mime']!='image/png'){
            $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
            $url='';
        }
        /*else if ($mime!="image/jpeg" && $mime!="image/png")
        {
           $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        }*/
        else if ($uploadedFile->tempName==null)
        {
           $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
          $message = "";
          $move = $uploadedFile->saveAs($uploadPath);
          if(!$move)
          {
             $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
          } 
        }        
        $funcNum = $_GET['CKEditorFuncNum'] ;
        //var_dump($_GET['CKEditorFuncNum']);die;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";        
    }
}
