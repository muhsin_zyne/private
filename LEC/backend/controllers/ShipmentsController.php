<?php

namespace backend\controllers;

use Yii;
use common\models\Shipments;
use common\models\search\ShipmentsSearch;
use common\models\ShipmentItems;
use common\models\search\ShipmentItemsSearch;
use common\models\SalesComments;
use common\models\User;
use common\models\Orders;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;
use yii\helpers\ArrayHelper;
use common\models\Carriers;

/**
 * ShipmentsController implements the CRUD actions for Shipments model.
 */
class ShipmentsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shipments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShipmentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == "3") 
        {
            $orders = Orders::findAll(['type'=>'b2c','storeId'=>$user->store->id]);
            $store_order = implode(",", ArrayHelper::map($orders, 'id', 'id'));
            $dataProvider->query->andWhere("Shipments.orderid in (".(($store_order=="")? "0" : $store_order).")");
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shipments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new ShipmentItemsSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => ShipmentItems::find()->where(['shipmentId' => $id]),
        ]);
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'shipping']),
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider'=>$dataProvider,
            'dataProvider_salescomments'=>$dataProvider_salescomments,
        ]);
    }

    /**
     * Creates a new Shipments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($orderid)
    {
        $model = new Shipments();
        $model->orderId=$orderid;
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId' => $orderid,'status' => 'pending'])
            ->orWhere(['orderId' => $orderid,'status' => 'invoiced']),
        ]);
        // if ($model->load(Yii::$app->request->post()) ) 
        // {
        //     var_dump(Yii::$app->request->post());die;
        // }
        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
           //var_dump($model->order->orderItemsStatus);die;
            
           foreach ($_POST['ShipmentItems']['orderItemId'] as  $value) 
           { 

                $model_si= new ShipmentItems();
                $ship_order_item=explode(',', $value);
                $model_si->shipmentId = $model->id;
                $model_si->orderItemId =$ship_order_item[0];
                $model_si->quantityShipped =$ship_order_item[1];
                $qts=$model_si->orderItem->qtyToShip;
                $orderitem_qty=$qts-$ship_order_item[1];
                if($model_si->orderItem->qtyToShip<$ship_order_item[1])
                {
                   $model_si->quantityShipped =$model_si->orderItem->qtyToShip; 
                }
                if($ship_order_item[1]!=0)
                {
                    $model_si->save();
                }
                
                if($model_si->orderItem->qtyToShip==0)
                {
                    $model_si->orderItem->status='shipped';
                    $model_si->orderItem->save(false);
                }            
            }
            //var_dump($model->order->shippingOrderStatus);die;
            if($model->order->shippingOrderStatus==0)
            {
                $model->order->status='complete';
                $model->order->save(false);
            }
            if(isset($_POST['Shipments']['chkmail']))
            {
                $model->notify=1;
                $model->save(false);
                $model->sendUserShipmentMail();
            }
            //$model->sendUserShipmentMail();
            Yii::$app->getSession()->setFlash('success', 'The Shipment has been created.');
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Shipments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //var_dump(Yii::$app->request->post());die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Shipments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionSendmail($id)
    {
        $model=$this->findModel($id);
        $model->sendUserShipmentMail();
        $model->notify=1;
        $model->save();
        Yii::$app->getSession()->setFlash('success', 'The Shipment email has been sent.');
        return $this->redirect(['view', 'id' => $model->id]);

    }
    public function actionTestmail()
    {
        $id=15;
        $model=$this->findModel($id);
        $model->sendUserShipmentMail();
    }

    /**
     * Finds the Shipments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shipments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shipments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
     public function actionCarrier($id){
       
        $carriers=Carriers::find()->where(['id'=>$id])->one();
        return $carriers->trackingUrl;        
    }
}
