<?php

namespace backend\controllers;

use Yii;
use common\models\CreditMemos;
use common\models\search\CreditMemosSearch;
use common\models\CreditMemoItems;
use common\models\search\CreditMemoItemsSearch;
use common\models\SalesComments;
use common\models\User;
use common\models\Orders;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;
use common\models\ProductInventory;
use yii\helpers\ArrayHelper;
use frontend\components\eway;
//use bryglen\braintree\braintree;

/**
 * CreditMemosController implements the CRUD actions for CreditMemos model.
 */
class CreditMemosController extends AdminController
{
    
    private $_prefix = 'Braintree';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CreditMemos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CreditMemosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == "3") 
        {
            $orders = Orders::findAll(['type'=>'b2c','storeId'=>$user->store->id]);
            $store_order = implode(",", ArrayHelper::map($orders, 'id', 'id'));
            $dataProvider->query->andWhere("CreditMemos.orderid in (".(($store_order=="")? "0" : $store_order).")");
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CreditMemos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new CreditMemoItemsSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => CreditMemoItems::find()->where(['memoId' => $id]),
        ]);
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'creditmemo']),
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider'=>$dataProvider,
            'dataProvider_salescomments'=>$dataProvider_salescomments,

        ]);
    }

    /**
     * Creates a new CreditMemos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
      public function actionCreate($orderid) {
        $model = new CreditMemos();
        $model->orderId = $orderid;
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId' => $orderid]),
                //'query' => OrderItems::find()->where(['orderId' => $orderid,'status' => 'shipped'])
                //->orWhere(['orderId' => $orderid,'status' => 'invoiced']),
        ]);
//        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//            return \yii\widgets\ActiveForm::validate($model);
//        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->RefundGrandTotalCheck > $model->order->grandTotal) {
                Yii::$app->getSession()->setFlash('error', "Sorry, refund amount must be less than order total.");
                //Yii::$app->getSession()->setFlash('error', "Sorry145555, ");
                //return $this->refresh();
                //return $this->redirect(['create', 'id' => $orderid]);
                die("<script>window.location.reload()</script>");
            }
            
            

            // Yii::$app->getSession()->setFlash('error', "Sorry145555, ");
            //var_dump($_POST);die();
            //var_dump(Yii::$app->request->post());die;
            //var_dump($_POST['CreditMemos']);die;
            //$model->subTotal=$_POST['CreditMemos']['subTotal'];
            //$model->grandTotal=$_POST['CreditMemos']['grandTotal'];
            //var_dump($model);die;
            //print_r($_POST['refund']);
            //exit;
            $model->grandTotal = $_POST['CreditMemos']['grandTotal'];
            $model->shipping = 0;
            $model->status = 'refunded';

            $order = $model->order;
            $model->subTotal = $order->subTotal;
            if($model->notify=='true')
                $model->notify=1;
            else
                $model->notify=0;
            
            $eway = Yii::$app->set('eway', Yii::$app->params['temp']['siteConfig']['components']['eway']);

            $response = Yii::$app->eway->refundAmount($order->transactionId, Yii::$app->eway->convertAmount($model->grandTotal));
            
            if(empty($response->Errors)) {
                if ($model->save()) {
                    if (isset($_POST['refund'])) {
                        foreach ($_POST['refund'] as $key => $refund_item) {
                            $order_item = OrderItems::findOne($key);
                            $model_cmi = new CreditMemoItems();
                            $model_cmi->memoId = $model->id;
                            $model_cmi->amount = $refund_item;
                            $model_cmi->orderItemId = $key;
                            $model_cmi->quantityRefunded = $order_item->quantity;
                            $model_cmi->save(false);
                        }
                    }
                    
                    if ($model->order->CreditMemoCheck)
                        $model->order->status = "partially-refunded";
                    else
                        $model->order->status = "fully-refunded";
                    $model->order->save(false);
                    
                    if ($model->notify == 1)
                        $model->sendCreditMemoMail();
                    Yii::$app->getSession()->setFlash('success', 'The credit memo has been created.');
                    Yii::$app->getSession()->setFlash('success', 'Refund processed successfully.');
                    die("<script>window.location.reload()</script>");
                    //die("The credit memo has been created.");
                }
            }
            else {
                Yii::$app->getSession()->setFlash('error', "Some error occured!!");
                die("<script>window.location.reload()</script>");
            }

        } //else { var_dump($model->geterrors());die;}
        /* else {
          return $this->render('create', [
          'model' => $model,
          'dataProvider'=>$dataProvider,
          ]);
          } */
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('create', compact('model', 'dataProvider'));
        }
    }

    /**
     * Updates an existing CreditMemos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CreditMemos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionSendmail($id)
    {
        $model=$this->findModel($id);
        $model->sendCreditMemoMail();
        $model->notify=1;
        $model->save();
        Yii::$app->getSession()->setFlash('success', 'The credit memo email has been sent.');
        return $this->redirect(['view', 'id' => $model->id]);

    }
    public function actionTestmail()
    {
        $id=20;
        $model=$this->findModel($id);
        $model->sendCreditMemoMail();
    }


    /**
     * Finds the CreditMemos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CreditMemos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CreditMemos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
