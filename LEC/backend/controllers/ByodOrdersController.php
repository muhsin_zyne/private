<?php

namespace backend\controllers;

use Yii;
use app\components\AdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\Products;
use common\models\ClientPortal;
use common\models\ClientPortalProducts;
use common\models\search\ClientPortalSearch;
use common\models\AttributeValues;
use yii\widgets\ActiveForm;
use common\models\Attributes;
use common\models\search\OrdersSearch;
use common\models\OrderItems;
use common\models\User;
use common\models\Invoices;
use common\models\OrderComment;
use common\models\CreditMemos;
use common\models\Shipments;
use common\models\SalesComments;
use common\models\Deliveries;
use common\models\Stores;
use common\models\search\DeliveriesSearch;
use common\models\search\ShipmentsSearch;
use common\models\search\InvoicesSearch;
use common\models\search\CreditMemosSearch;
use common\models\Orders;
use frontend\components\Helper;
use yii\web\UploadedFile;
use yii\db\ActiveQuery;
use yii\web\Response;
use kartik\mpdf\Pdf;

class ByodOrdersController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$searchModel = new OrdersSearch();
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId==3)
            $dataProvider->query->andWhere('type = "byod" and storeId = '.$user->store->id.'')->orderBy(['id'=>SORT_DESC,]);
        else
            $dataProvider->query->andWhere(['type' => 'byod'])->orderBy(['id'=>SORT_DESC,]);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id){  

        $byodOrder = Orders::find()->where(['id'=>$id])->one();
        
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId' => $id])->andWhere(['id' =>$byodOrder->getRemainingOrderItemIds()]),
        ]);
        $searchModel_invoices=new InvoicesSearch();
        $dataProvider_invoices = new ActiveDataProvider([
            'query' => Invoices::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);

        //$searchModel_ordercomments = new OrderCommentSearch();
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'order','orderId' => $id]),
        ]);
        $dataProvider_salescomments_all = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['orderId' => $id]),
        ]);
        
        $dataProvider_creditmemos = new ActiveDataProvider([
            'query' => CreditMemos::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_creditmemos=new CreditMemosSearch();

        $dataProvider_shipment = new ActiveDataProvider([
            'query' => Shipments::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_shipment=new ShipmentsSearch();

        $dataProvider_deliveries = new ActiveDataProvider([
            'query' => Deliveries::find()->where(['orderId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);
        $searchModel_deliveries=new DeliveriesSearch();

        $deliveries = Deliveries::find()->where(['orderId'=>$id])->all();
        $invoice = Invoices::find()->where(['orderId' => $id])->one();

         return $this->render('view', [
            'model' => Orders::findOne($id),
            'searchModel_invoices' => $searchModel_invoices,
            'dataProvider_invoices'=>$dataProvider_invoices,
            
            'dataProvider_salescomments'=>$dataProvider_salescomments,
            'dataProvider_salescomments_all'=>$dataProvider_salescomments_all,

            'searchModel_creditmemos' => $searchModel_creditmemos,
            'dataProvider_creditmemos'=>$dataProvider_creditmemos,

            'searchModel_shipment' => $searchModel_shipment,
            'dataProvider_shipment'=>$dataProvider_shipment,

            'searchModel_deliveries' => $searchModel_deliveries,
            'dataProvider_deliveries'=>$dataProvider_deliveries,
            
            'dataProvider' => $dataProvider,
            'deliveries' => $deliveries,
            'invoice' => $invoice,
        ]);
    }

    public function actionUpdate($id,$type=NULL){
        $model = Orders::find()->where(['id'=>$id])->one();
        $orderItems= OrderItems::find()->select('id')->where(['orderId'=>$id])->all(); 
        if (Yii::$app->request->post()) {
            $entity = [ ];
            foreach ($_POST['OrderItems'] as $key => $orderItem) { // order items upadte  Qty Ready For Delivery
                $model_orderItems=OrderItems::findOne($key);
                $model_orderItems->qtyReadyForDelivery=$model_orderItems->qtyReadyForDelivery+$orderItem['qrfd'];
                $model_orderItems->save(false);
                $orderItemEntity[$key]['name']=$model_orderItems->product->name;
                $orderItemEntity[$key]['qty']=$orderItem['qrfd'];
                $entity= $entity+$orderItemEntity;                
            }
            $model->addActivity('ready-for-collection', $entity);
            $model->status=$model->ordersStatus;
            $model->save(false);          
            if($_POST['SalesComments']['readyComment']!=''){ 
                $model_salesComments = new SalesComments();
                $model_salesComments->type='order';
                $model_salesComments->orderId=$id;
                $model_salesComments->comment=$_POST['SalesComments']['readyComment']; 
                $model_salesComments->save(false); 
                if(isset($_POST['SalesComments']['notify']) && $_POST['SalesComments']['notify']=='true'){
                    $model_salesComments->notify=1;
                    $model_salesComments->sendOrderStatusUpdateMail();
                    $model_salesComments->save(false); 
                }
            } else {  
                if(isset($_POST['SalesComments']['notify']) && $_POST['SalesComments']['notify']=='true'){
                    $model_sc = new SalesComments();              
                    $model_sc->sendOrderStatusUpdateMail($model->id);
                }
            } 
            echo "<script> location.reload(); </script>";
            die('Order Status Updated Successfully!');  
        }
        
        if(\Yii::$app->request->isAjax){ 
            $oi_array[]=0;$oi_qrfd[0]=0;
            foreach ($orderItems as $orderItem) {
                $oi_id=$orderItem['id'];
                if(isset($_REQUEST['chk_'.$oi_id])){
                    if($_REQUEST['qrfd_'.$oi_id]!=0){
                        $oi_array[]=$oi_id;
                        $oi_qrfd[$oi_id]=$_REQUEST['qrfd_'.$oi_id];
                    }
                }                  
            } 
            if($type=='fc'){
                $dataProvider = new ActiveDataProvider([ 
                    'query' => OrderItems::find()->where(['orderId' => $id])
                        ->andWhere(['id' =>$model->getRemainingOrderItemIds()]),
                    ]);
            } 
            else {
                $dataProvider = new ActiveDataProvider([
                    'query' => OrderItems::find()->where(['orderId' => $id])->andWhere(['id' =>$oi_array]),
                ]); 
            }            
            return $this->renderAjax('/orders/ready-for-collection',compact('model','dataProvider','oi_qrfd','type'));
        }
    }

    public function actionPdf($id) {
        $order = Orders::find()->where(['id'=>$id])->one();
        $invoice = Invoices::find()->where(['orderId'=>$order->id])->one();
        $portal = ClientPortal::find()->where(['id'=>$order->portalId])->one();
        $content = Yii::$app->controller->renderPartial('_pdfContent',compact('order','invoice','portal'));
        $filename = "BYOD Invoice Summary - Order #".$order->id.".pdf";
        $footer = "<div style='color:#5b5b5b;font-weight:normal;'>".$order->storeAddressFormatted."</div>";
        $header = "<div style='color:#5b5b5b;font-weight:normal;'>BYOD Invoice Summary";

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'filename' => $filename, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            //'destination' => Pdf::DEST_FILE, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            //'cssFile' => '@backend/web/css/deliverypdf.css',
            // any css to be embedded if required
            'cssInline' => '', 
             // set mPDF properties on the fly
            'options' => ['title' => 'In-Store Collection'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>[$header], 
                'SetFooter'=>[$footer],
            ]
        ]);

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        return $pdf->render(); 
    }    
}    	