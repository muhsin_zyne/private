<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\Gallery;
use common\models\search\GallerySearch;
use common\models\user;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex($sid=1)
    {
        //var_dump($sid);die;
        $model = new Gallery();
        $dataProvider = Yii::$app->request->queryParams;
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId != 1)
        {
           $sid=$user->store->id;
        }
        $storeid = ($sid==NULL ? 1 : $sid );
        return $this->render('index',compact('storeid'));
        // return $this->render('index', [
        //     //'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
     public function actionView()
    {
        //echo "hai";
        $store_id=$_POST['store_id'];

        $html='<div id="catageries_list">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List of Categories</h3><br>
                <h5 class="sub-head">Drag and drop the title to sort the order. It will automatically saved once you drop the item. </h5>
            </div>
            <div class="box-body">
                <div class="cf nestable-lists">
                    <div class="dd" id="nestable">
                        <ul id="sortable">';                   
                    $gallery_storeid=Gallery::find()->where(['storeId' => $store_id])->all();
                    //var_dump($gallery_storeid);die;
                    if(!empty($gallery_storeid))                  
                    {
                        foreach ($gallery_storeid as $index => $item) 
                        {
                                        
                        $html.='<li class="ui-state-default" id="item-'.$item['id'].'"><div class="dd-handle">';
                        $html.=''.$item['title'].'';
                        $html.='<div style="float:right;">';               
                        $html.=''. Html::a('Add Images', ['gallery-images/index','id' => $item['id']], ['class'=>'btn btn-info btn-sm']) .'';
                        $html.=''. Html::a('Update', ['update','id' => $item['id']], ['class'=>'btn btn-info btn-sm']).'';
                         $html.=''. Html::a('Delete', ['delete', 'id' => $item['id']], 
                                ['class' => 'btn btn-info btn-sm','data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]).'';
                            
                        $html.='</div></li>';  
                    
                        }
                    }
                    else
                    {
                         $html.='No Category Found';
                     }
                    
                   
                    $html.='</ul>
                    </div>       
                </div>
            </div>
        </div>
           
    </div>';
        echo $html;
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSortable()
    {
        $pos=0;
        $sort_category=explode(",",$_POST['list']);                        
        foreach ($sort_category as  $category) 
        {
            $gallery_model= Gallery::find()->where( [ 'id' => $category] )->one();   
            $gallery_model->position= $pos;      
            $gallery_model->save();
            $pos++;
        }
      
    }
    public function actionCreate()
    {
        
        $model = new Gallery();
        $model->title= $_POST['title'];
        $model->storeId= $_POST['store_id'];
        $model->position=0;
        $model->save();
        //return $this->render('index'); 
        Yii::$app->getSession()->setFlash('success', 'The Gallery Category has been Created.');  
        return $this->redirect(['index','sid'=>$model->storeId]); 
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           Yii::$app->getSession()->setFlash('success', 'The Gallery Category has been Updated.');   
           return $this->redirect(['index','sid'=>$model->storeId]);   
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //echo $_REQUEST['id'];
        $model=$this->findModel($id);
        $model->delete();
        Yii::$app->getSession()->setFlash('success', 'The Gallery Category has been Deleted.');
        return $this->redirect(['index','sid'=>$model->storeId]);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
