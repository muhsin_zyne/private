<?php

namespace backend\controllers;

use Yii;
use common\models\Menus;
use common\models\User;
use common\models\search\MenusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\Stores;
use common\models\Categories;
use yii\helpers\Html;
/**
 * MenusController implements the CRUD actions for Menus model.
 */
class MenusController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menus models.
     * @return mixed
     */
    public function actionIndex($sid= NULL,$type)
    {
        $searchModel = new MenusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        $sid=($user->roleId != 1) ? $user->store->id : $sid ;        
        $storeid = ($sid==NULL ? 0 : $sid );
        //var_dump($type);die;
        $menuall_position=Menus::find()->where(['storeId'=>$storeid,'unAssigned'=>0,'type'=>$type])->orderBy('position')->all();
        //$menuall_title=Menus::find()->where(['storeId'=>$storeid,'unAssigned'=>0,'type'=>$type])->orderBy('title')->all();
        $menuall_unAssigned=Menus::find()->where(['storeId'=>$storeid,'unAssigned'=>1,'type'=>$type])->orderBy('title')->all();
        return $this->render('index',compact('menuall_position','storeid','menuall_title','menuall_unAssigned'));
        
    }
    
    /**
     * Displays a single Menus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type)
    {
        $model = new Menus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //Yii::$app->getSession()->setFlash('success', 'Menu has been Created Successfully!.');
            echo "<script> location.reload(); </script>";
            die('Menu has been Created Successfully!.'); 
            //return $this->redirect(['index']);
        } 
        if(\Yii::$app->request->isAjax){
            return $this->renderAjax('_form',compact('model','type'));
        }
        // return $this->renderPartial('create', [
        //     'model' => $model,
        // ]);
        //return $this->redirect(['index']); 

    }
    public function actionStoremenu() // menu change by store id
    {
       $storeid=$_POST['store_id'];
       ($storeid=='' ? $storeid=0 : $storeid=$storeid );
        return $this->redirect(['index','sid'=>$storeid,'type'=>$_POST['type']]); 
    }
    public function actionUpdate() // menu save to order
    {
        //echo "hai";exit;
        $model = new Menus();
        $storeid=$_POST['store_id'];
        $type=$_POST['type'];
        $assigned_menu=$_POST['assigned_menu'];
        $unassigned_menu=$_POST['unassigned_menu'];
        $ass_status=0;
        $unass_status=1;
        $ass_menu_array = json_decode($assigned_menu, true);    //assigned menu save function  
        $unass_menu_array = json_decode($unassigned_menu, true);  
        $model->getMenuPositionChange($ass_menu_array,$storeid,$type,$ass_status);   // Assigned Menu List 
        $model->getMenuPositionChange($unass_menu_array,$storeid,$type,$unass_status);   // Unassigned Menu List     
        Yii::$app->getSession()->setFlash('success', 'The Menu has been Updated.');
        return $this->redirect(['index','sid'=>$storeid,'type'=>$type]);
    }
    
    

    /**
     * Deletes an existing Menus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);         
        $menus=Menus::find()->where(['parent'=>$model->id])->all();
        if(isset($menus)){
            foreach ($menus as $key => $menu) {
                if($menu->parent!=0){
                    $menus1=Menus::find()->where(['parent'=>$menu->id])->all();
                    if(isset($menus1)){
                        foreach ($menus1 as $key => $menu1) {
                            $this->findModel($menu1->id)->delete();
                        }
                    }
                }
                $this->findModel($menu->id)->delete();
            }
        }
        $model->delete();
        Yii::$app->getSession()->setFlash('success', 'The Menu has been Deleted.');
        return $this->redirect(['index','type'=>$model->type]);
    }

    /**
     * Finds the Menus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCreatemenu() {    
       //Menus::demoMenucreate(); // create menu for all sites
    }
    //--------------------------------------------- site menu create  ------
    public function actionCreatesitemenu($id) { // create menu for subsite    
        $store=Stores::find()->where(['id'=>$id,'isVirtual'=>0])->one();
        if(!empty($store)){
            Menus::storeMenucreate($id);  // menu create 
            Yii::$app->getSession()->setFlash('success', $store->title.' Menu has been Created Successfully!......');
            return $this->redirect(['index','type'=>'main']);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Sorry!....Please Check Your Store Id!......');
            return $this->redirect(['index','type'=>'main']);
        }
    }
    public function actionCreateheadermenu($id) {// create  header menu for subsite    
        $store=Stores::find()->where(['id'=>$id,'isVirtual'=>0])->one();
        if(!empty($store)){
            Menus::storeHeadermenucreate($id);
            Yii::$app->getSession()->setFlash('success', $store->title.'  Header Menu has been Created Successfully!......');
            return $this->redirect(['index','type'=>'header']);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Sorry!....Please Check Your Store Id!......');
            return $this->redirect(['index','type'=>'header']);
        }
    }
    public function actionCreatefootermenu($id){ // create  footer menu for subsite
        $store=Stores::find()->where(['id'=>$id,'isVirtual'=>0])->one();
        if(!empty($store)){
            Menus::storeFootermenucreate($id);
            Yii::$app->getSession()->setFlash('success', $store->title.'  Header Menu has been Created Successfully!......');
            return $this->redirect(['index','type'=>'footer']);
        } else {
            Yii::$app->getSession()->setFlash('error', 'Sorry!....Please Check Your Store Id!......');
            return $this->redirect(['index','type'=>'footer']);
        }
    }
    //------------------------------------------------------------------------- site menu create end ------
    public function actionUpdatemenu($id,$type){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            echo "<script> location.reload(); </script>";
            die('Menu has been Updated Successfully.');
            // Yii::$app->getSession()->setFlash('success', 'The Menu has been Updated.');
            // return $this->redirect(['index']);
        } 
        if(\Yii::$app->request->isAjax){
            return $this->renderAjax('_form',compact('model','type'));
        }
        return $this->renderPartial('updatemenu', [ 'model' => $model, ]);
    }
    public function actionCategorymenu($id){
        $categorie=Categories::findOne($id);
        if(isset($categorie)){
           echo  Menus::categoryMenuCreate($categorie);
        } else {
            echo "No Category Found";
        }
    }

}

