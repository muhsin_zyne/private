<?php

namespace backend\controllers;
use yii\data\ArrayDataProvider;
use common\models\Orders;
use common\models\Invoices;
use common\models\CreditMemos;
use backend\components\Helper;
use Yii;

class ReportsController extends \app\components\AdminController
{

    public function actionIndex(){
        return $this->render('index');
    }

    public function actionExport(){
        return $this->render('export');
    }

    public function actionOrders(){
        $report = new \backend\models\ReportForm;
        if($firstOrder = Orders::find()->orderBy('orderDate asc')->one())
            $startDate = strtotime(date('d-m-Y 00:00:00', strtotime($firstOrder->orderDate)));
        else
            $startDate = time();

        if(isset($_POST['ReportForm'])){
            $report->load($_POST);
            $startDate = strtotime(date('d-m-Y 00:00:00', strtotime($_POST['ReportForm']['start_date'])));
            $endDate = strtotime(date('d-m-Y 23:59:59', strtotime($_POST['ReportForm']['end_date'])));
        }
        $order = new Orders;
    	$orders = $this->createReportProvider($startDate, isset($endDate)? $endDate : null, "day", $order, ['totalCount', 'salesItemsCount', 'salesTotal', 'invoiced', 'shipped', 'refunded', 'discount']);// die;
    	if(isset($_POST['ReportForm']['export'])){
            return \common\components\CSVExport::widget([
                'dataProvider' => $orders,
                'summary' => '',
                'columns' => [
                    [
                       'label'=>'Date',
                        'value'=>'period',
                    ],    
                   [
                        'label' => 'Orders',
                        'value' => 'totalCount',
                    ],
                    [
                        'label' => 'Sales Items',
                        'value' => 'salesItemsCount'
                    ],
                    [
                        'label' => 'Sales Total',
                        'value' => function($row){ return Helper::money($row['salesTotal']); },
                        'format' => 'raw'
                    ],
                    [
                        'label' => 'Refunded',
                        'value' => function($row){ return Helper::money($row['invoiced']); },
                        'format' => 'raw'
                    ],
                    [
                        'label' => 'Shipped',
                        'value' => function($row){ return Helper::money($row['shipped']); },
                        'format' => 'raw'
                    ],
                    [
                        'label' => 'Discount',
                        'value' => function($row){ return Helper::money($row['discount']); },
                        'format' => 'raw'
                    ]
                ],
            ]);
        }
        return $this->render('orders', compact('orders', 'report'));
    }

    public function actionInvoiced(){
        $report = new \backend\models\ReportForm;
    	if($firstOrder = Orders::find()->orderBy('orderDate asc')->one()){
	    	$startDate = strtotime(date('d-m-Y 00:00:00', strtotime($firstOrder->orderDate)));
            if(isset($_POST['ReportForm'])){
                $report->load($_POST);
                $startDate = strtotime(date('d-m-Y 00:00:00', strtotime($_POST['ReportForm']['start_date'])));
                $endDate = strtotime(date('d-m-Y 23:59:59', strtotime($_POST['ReportForm']['end_date'])));
            }
	    	$invoice = new Invoices;
	    	$invoices = $this->createReportProvider($startDate, isset($endDate)? $endDate : null, "day", $invoice, ['orderCount', 'invoicedOrdersCount', 'invoiced']);// die;
    	}else
    		$invoices = [];
            if(isset($_POST['ReportForm']['export'])){
            return \common\components\CSVExport::widget([
                'dataProvider' => $invoices,
                'summary' => '',
                'columns' => [
                    [
                       'label'=>'Date',
                        'value'=>'period',
                    ],
                    [
                        'label' => 'Number of Orders',
                        'value' => 'orderCount',
                    ],
                    [
                        'label' => 'Number of Invoiced Orders',
                        'value' => 'invoicedOrdersCount'
                    ],
                    [
                        'label' => 'Total Invoiced',
                        'value' => function($row){ return Helper::money($row['invoiced']); },
                        'format' => 'raw'
                    ]
                ],
            ]);
        }
    	return $this->render('invoices', compact('invoices', 'report'));
    }

    public function actionShipped(){

        $report = new \backend\models\ReportForm;
        if($firstOrder = Orders::find()->orderBy('orderDate asc')->one()){ 
            $startDate = strtotime(date('d-m-Y 00:00:00', strtotime($firstOrder->orderDate)));
            if(isset($_POST['ReportForm'])){
                $report->load($_POST);
                $startDate = strtotime(date('d-m-Y 00:00:00', strtotime($_POST['ReportForm']['start_date'])));
                $endDate = strtotime(date('d-m-Y 23:59:59', strtotime($_POST['ReportForm']['end_date'])));
            }
            $shipments = $this->createReportProvider($startDate, isset($endDate)? $endDate : null, "day", new \common\models\Shipments, ['shipmentCount', 'carrier', 'totalSalesShipping', 'periodCarriers']);// die;
            //var_dump($shipments);die;
        }else
            //die('hiiii');
            $shipments = [];
            if(isset($_POST['ReportForm']['export'])){
            return \common\components\CSVExport::widget([
                'dataProvider' => $shipments,
                'summary' => '',
                'columns' => [
                    [
                        'label'=>'Date',
                        'value'=>'period',
                    ],
                    [
                        'label' => 'Carrier(s)',
                        'value' => 'periodCarriers'
                    ],
                    [
                        'label' => 'Number of Orders',
                        'value' => 'shipmentCount',
                    ],
                    [
                        'label' => 'Total Sales Shipping',
                        'value' => 'totalSalesShipping',
                        'format' => 'html'
                    ],
                ],
            ]);
        }
        return $this->render('shipments', compact('shipments', 'report'));
    }

    public function actionRefunds(){
        $report = new \backend\models\ReportForm;
    	if($firstOrder = Orders::find()->where('totalRefunded IS NOT NULL')->orderBy('orderDate asc')->one()){
	    	$startDate = strtotime(date('d-m-Y 00:00:00', strtotime($firstOrder->orderDate)));
            if(isset($_POST['ReportForm'])){
                $report->load($_POST);
                $startDate = strtotime(date('d-m-Y 00:00:00', strtotime($_POST['ReportForm']['start_date'])));
                $endDate = strtotime(date('d-m-Y 23:59:59', strtotime($_POST['ReportForm']['end_date'])));
            }
	    	$refund = new CreditMemos;
	    	$refunds = $this->createReportProvider($startDate, isset($endDate)? $endDate : null, "day", $refund, ['totalCount', 'totalRefunded','orderIds']);
            //var_dump($refunds);die();
    	}else
    		$refunds = [];
        if(isset($_POST['ReportForm']['export'])){
            return \common\components\CSVExport::widget([
                'dataProvider' => $refunds,
                'summary' => '',
                'columns' => [
                    [
                       'label'=>'Date',
                        'value'=>'period',
                    ],
		    [
		        'label'=>'Order IDs',
		        'attribute'=>'orderIds',
		    ],
                    [
                        'label' => 'Number of Refunded Orders',
                        'value' => 'totalCount',
                    ],
                    [
                        'label' => 'Total Refunded',
                        'value' => function($row){ return Helper::money($row['totalRefunded']); },
                        'format' => 'raw'
                    ],
                ],
            ]);
        }
    	return $this->render('refunds', compact('refunds', 'report'));
    }

    public function actionCarts(){
        $report = new \backend\models\ReportForm;
        
    }

    public function actionAbandonedcarts(){
        $rows = [];
        $report = new \backend\models\ReportForm;
    	$carts = \common\models\Carts::find()->where('updatedDate <= "'.date('Y-m-d H:i:s', strtotime("-5 days")).'" AND type="abandoned" AND (email IS NOT NULL OR userId!=0)')->all();
        foreach($carts as $i => $cart){
            $rows[$i]['id'] = $cart->id;
            $rows[$i]['name'] = $cart->customerName;
            $rows[$i]['email'] = $cart->customerEmail;
            $rows[$i]['itemCount'] = $cart->itemCount;
            $rows[$i]['itemQuantity'] = $cart->itemQuantity;
            $rows[$i]['subTotal'] = Helper::money($cart->subTotal);
            $rows[$i]['updatedAt'] = Helper::date($cart->updatedDate);
        }

        $abandonedCarts = new ArrayDataProvider([
            'allModels' => $rows //$search->search(Yii::$app->request->queryParams);
        ]);
        if(isset($_POST['ReportForm']['export'])){
            return \common\components\CSVExport::widget([
                'dataProvider' => $abandonedCarts,
                'summary' => '',
                'columns' => [
                    [
                        'label' => 'Customer Name',
                        'value' => 'name',
                    ],
                    [
                        'label' => 'Customer Email',
                        'value' => 'email'
                    ],
                    [
                        'label' => 'Number of Items',
                        'value' => 'itemCount',
                    ],
                    [
                        'label' => 'Quantity of Items',
                        'value' => 'itemQuantity',
                    ],
                    [
                        'label' => 'Updated at',
                        'value' => 'updatedAt'
                    ]
                ],
            ]);
        }
        return $this->render('abandonedcarts', compact('abandonedCarts', 'report'));
    }

    public function actionItemsales(){
            
    }

    public function createReportProvider($startDate, $endDate = null, $period = "month", $model, $attributes = []){
    	$rows = [];
    	$endDate = (is_null($endDate))? strtotime(date('d-m-Y 00:00:00')) : $endDate;
    	$date = $endDate;
    	//$model->reportFrom = $period;

        /*var_dump($attributes);
        var_dump($model->attributes); die();*/

        $i = 0;
        $model->reportStoreId = Yii::$app->user->identity->roleId == "3"? Yii::$app->user->identity->store->id : 0;
    	while($startDate <= $date){
    		if($period=="month"){
	            $model->reportFrom = date("Y-m-01 00:00:00", ($date));
	            $model->reportTo = date("Y-m-31 23:59:59", ($date));
	        }elseif($period=="year"){
	            $model->reportFrom = date("Y-01-01 00:00:00", ($date));
	            $model->reportTo = date("Y-12-31 23:59:59", ($date));
	        }else{
	            $model->reportFrom = date("Y-m-d 00:00:00", ($date));
	            $model->reportTo = date("Y-m-d 23:59:59", ($date));
	        }
    		foreach($attributes as $key => $attribute){
    			if($model->totalCount == 0)
    				continue;
    			else{
    				$rows[$i]['id'] = $date;
    				$rows[$i]['period'] = ($period=="month")? date('Y/m', $date) : (($period=="year")? date('Y', $date) : date('d/m/Y', $date));
    				$rows[$i][$attribute] = $model->$attribute;
                   // var_dump($model->$attribute);
    			}
    		}
    		$date = strtotime("-1 $period", $date); //adding one day/month/year
    		$i++;
    		//echo date('d-m-Y', $date)."\n";
    	}
    	//$rows[] =
        //var_dump($rows);die(); 
        //die();
    	return new ArrayDataProvider([
            'allModels' => $rows //$search->search(Yii::$app->request->queryParams);
        ]);
    }
}
