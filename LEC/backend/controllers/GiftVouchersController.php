<?php

namespace backend\controllers;

use Yii;
use common\models\GiftVouchers;
use common\models\search\GiftVouchers as GiftVouchersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\VoucherUsage;
use yii\data\ActiveDataProvider;
/**
 * GiftVouchersController implements the CRUD actions for GiftVouchers model.
 */
class GiftVouchersController extends \app\components\AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GiftVouchers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GiftVouchersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(Yii::$app->user->identity->roleId == "3")
            $dataProvider->query->andWhere('storeId = "'.Yii::$app->user->identity->store->id.'"');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GiftVouchers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GiftVouchers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $voucher = new GiftVouchers();
        if ($voucher->load(Yii::$app->request->post())) { //die('test');
            $voucher->expiredDate = date('Y-m-d H:i:s', strtotime($voucher->expiredDate));
            $voucher->initialAmount = $voucher->balance;
            if($voucher->save(false))
                $voucher->sendEmail(true);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $voucher,
            ]);
        }
    }
    public function actionRedeem($id)
    {
        $model = new VoucherUsage();
       
        if ($model->load(Yii::$app->request->post())) {       
            $model->dateRedeemed= date("Y-m-d",strtotime($_POST['VoucherUsage']['dateRedeemed']));
            if($model->save()){
                $voucher =GiftVouchers::findOne($id);
                $voucher->balance=$voucher->balance - $model->amountRedeemed;
                $voucher->lastUsedDate=date('Y-m-d H:i:s');
                $voucher->save(false);
                Yii::$app->session->setFlash('success', 'Redemption Confirmed.');
                return $this->redirect(['update','id'=>$id]);
            }           
        } 
        if(\Yii::$app->request->isAjax){
            return $this->renderPartial('redeem',compact('model','id'));
        }
        else {
            return $this->render('redeem',compact('model','id'));
        }
       
    }
    

    /**
     * Updates an existing GiftVouchers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $voucher = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => VoucherUsage::find()->where(['voucherId' => $id])->orderBy(['id'=>SORT_DESC,]),
        ]);

        //var_dump($_POST['update']);die();

        /*if ($voucher->load(Yii::$app->request->post())) {
            $voucher->expiredDate = date('Y-m-d H:i:s', strtotime($voucher->expiredDate));
            $voucher->save(false);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $voucher,
            ]);
        }*/

        if (isset($_POST['update'])) {  //var_dump($voucher->balance);die();
            if ($voucher->load(Yii::$app->request->post())) {
                $voucher->expiredDate = date('Y-m-d H:i:s', strtotime($voucher->expiredDate));
                $voucher->save(false);
                return $this->redirect(['index']);
            }    
        }
        elseif (isset($_POST['update_email'])) {
            if ($voucher->load(Yii::$app->request->post())) {
                $voucher->expiredDate = date('Y-m-d H:i:s', strtotime($voucher->expiredDate));
                if($voucher->save(false))
                    $voucher->sendEmail(true);
                return $this->redirect(['index']);
            }  
        }
        else {
            return $this->render('update', [
                'model' => $voucher,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing GiftVouchers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GiftVouchers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GiftVouchers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GiftVouchers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
