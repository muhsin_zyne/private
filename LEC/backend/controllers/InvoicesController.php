<?php

namespace backend\controllers;

use Yii;
use common\models\Invoices;
use common\models\search\InvoicesSearch;
use common\models\InvoiceItems;
use common\models\search\InvoiceItemsSearch;
use common\models\SalesComments;
use common\models\User;
use common\models\Orders;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\data\ActiveDataProvider;
use common\models\OrderItems;
use yii\helpers\ArrayHelper;

/**
 * InvoicesController implements the CRUD actions for Invoices model.
 */
class InvoicesController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = User::findOne(Yii::$app->user->id);
        if($user->roleId == "3") 
        {
            $orders = Orders::findAll(['type'=>'b2c','storeId'=>$user->store->id]);
            $store_order = implode(",", ArrayHelper::map($orders, 'id', 'id'));
            $dataProvider->query->andWhere("Invoices.orderid in (".(($store_order=="")? "0" : $store_order).")");
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        //var_dump($model->order->id);die;
        $searchModel = new InvoiceItemsSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => InvoiceItems::find()->where(['invoiceId' => $id]),
        ]);
        $dataProvider_salescomments = new ActiveDataProvider([
            'query' => SalesComments::find()->where(['typeId' => $id,'type' => 'invoice','orderId' => $model->order->id]),
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider'=>$dataProvider,
            'dataProvider_salescomments'=>$dataProvider_salescomments,

        ]);
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
    }

    /**
     * Creates a new Invoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($orderid)
    {
        //var_dump('hi');die;
        $model = new Invoices();
        $model->orderId=$orderid;
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['orderId' => $orderid,'status' => 'pending']),
        ]);
        $model->status='paid';
        if ($model->load(Yii::$app->request->post()) ) 
        {
            
           // var_dump(Yii::$app->request->post());die;
          
        }
        if (isset($_POST['InvoiceItems']) && $model->load(Yii::$app->request->post()) && $model->save()) 
        {
            $subtotal=0;
            foreach ($_POST['InvoiceItems']['orderItemId'] as  $value) 
            {
                $model_ii= new InvoiceItems();
                $invo_orderitem_exp=explode(',', $value);
                $model_ii->invoiceId = $model->id;
                $model_ii->orderItemId = $invo_orderitem_exp[0];
                $model_ii->quantityInvoiced =$invo_orderitem_exp[1];
                //var_dump($invo_orderitem_exp[1]);die;
                if($model_ii->orderItem->qtyToInvoiced<$invo_orderitem_exp[1])
                {
                   $model_ii->quantityInvoiced =$model_ii->orderItem->qtyToInvoiced; 
                }
                if($model_ii->quantityInvoiced!=0)
                {
                    $subtotal=$subtotal+($model_ii->orderItem->price * $model_ii->quantityInvoiced);
                    $model_ii->save();
                }
                if($model_ii->orderItem->qtyToInvoiced==0)
                {
                    $model_ii->orderItem->status='invoiced';
                    $model_ii->orderItem->save(false);
                }
            }
            //var_dump($model->order->invoiceOrderStatus);die;
            if($model->order->invoiceOrderStatus==1)
            {
                $model->order->status='processing';
                $model->order->save(false);
            }
            if(isset($_POST['Invoices']['chkmail']))
            {
                $model->notify=1;
                $model->save(false);
                $model->sendInvoiceMail();
            }
            Yii::$app->getSession()->setFlash('success', 'The Invoice has been created.');
            //------------------------------------------------- shipment created ----------------------------------------------
            if(isset($_POST['Invoices']['shipment_chk']))
            {

                $model_s = new \common\models\Shipments();
                $model_s->orderId=$_POST['Invoices']['orderId'];
                $model_s->title=$_POST['Invoices']['shipment_title'];
                $model_s->carrier=$_POST['Invoices']['shipment_carrier'];
                $model_s->trackingNumber=$_POST['Invoices']['shipment_trackingNumber'];
                if($model_s->save(false))
                {
                    foreach ($_POST['InvoiceItems']['orderItemId'] as  $value) 
                    {
                        $model_si= new \common\models\ShipmentItems();
                        $ship_orderitem_exp=explode(',', $value);
                        $model_si->shipmentId = $model_s->id;
                        $model_si->orderItemId = $ship_orderitem_exp[0];
                        $model_si->quantityShipped =$ship_orderitem_exp[1];
                        if($model_si->orderItem->qtyToShip<$ship_orderitem_exp[1])
                        {
                            $model_si->quantityShipped =$model_si->orderItem->qtyToShip; 
                        }
                        $model_si->save(false);
                        if($model_si->orderItem->qtyToShip==0)
                        {
                            $model_si->orderItem->status='shipped';
                            $model_si->orderItem->save(false);
                        }
                        if(isset($_POST['Invoices']['ship_chkmail']))
                        {
                            $model_s->notify=1;
                            $model_s->save(false);
                            $model_s->sendUserShipmentMail();
                        }
                        if($model_s->order->shippingOrderStatus==0)
                        {
                            $model_s->order->status='complete';
                            $model_s->order->save(false);
                        }
                    }
                }
                Yii::$app->getSession()->setFlash('success', 'The invoice and shipment have been created.');
            } 
            //---------------------------------------------------------------shipment end---------------------------------------------------------------------

           
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Invoices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Invoices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionSendmail($id)
    {
        $model=$this->findModel($id);
        $model->sendInvoiceMail();
        $model->notify=1;
        $model->save();
        Yii::$app->getSession()->setFlash('success', 'The invoice email has been sent.');
        return $this->redirect(['view', 'id' => $model->id]);

    }
    public function actionTestmail()
    {
        $id=6;
        $model=$this->findModel($id);
        $model->sendInvoiceMail();
    }
   
    /**
     * Finds the Invoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
