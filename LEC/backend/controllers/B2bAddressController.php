<?php

namespace backend\controllers;

use Yii;
use common\models\B2bAddresses;
use common\models\search\B2bAddressesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\helpers\ArrayHelper;
use common\models\User;
/**
 * B2bAddressController implements the CRUD actions for B2bAddresses model.
 */
class B2bAddressController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all B2bAddresses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new B2bAddressesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single B2bAddresses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new B2bAddresses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new B2bAddresses();
        $users=User::find()->where(['roleId'=>3])->all();
        $listData=ArrayHelper::map($users,'id','email');        
        if ($model->load(Yii::$app->request->post())) { 
            $tradingHoursArray = array(
                array(
                    'monday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayTo']
                    ),
                    'tuesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayTo']
                    ),
                    'wednesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayTo']
                    ),
                    'thursday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayTo']
                    ),
                    'friday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayTo']
                    ),
                    'saturday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayTo']
                    ),
                    'sunday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayTo']
                    ),
                ),
            );
            $model->tradingHours=json_encode(array_values($tradingHoursArray));
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } */ else {
            return $this->render('create', [
                'model' => $model,'listData'=>$listData,
            ]);
        }
    }

    /**
     * Updates an existing B2bAddresses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $users=User::find()->where(['roleId'=>3])->all();
        $listData=ArrayHelper::map($users,'id','email');
        if ($model->load(Yii::$app->request->post())) {           
            $tradingHoursArray = array(
                array(
                    'monday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkMon']) ? 'closed' : $_POST['B2bAddresses']['mondayTo']
                    ),
                    'tuesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSTue']) ? 'closed' : $_POST['B2bAddresses']['tuesdayTo']
                    ),
                    'wednesday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkWed']) ? 'closed' : $_POST['B2bAddresses']['wednesdayTo']
                    ),
                    'thursday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkThu']) ? 'closed' : $_POST['B2bAddresses']['thursdayTo']
                    ),
                    'friday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkFri']) ? 'closed' : $_POST['B2bAddresses']['fridayTo']
                    ),
                    'saturday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSat']) ? 'closed' : $_POST['B2bAddresses']['saturdayTo']
                    ),
                    'sunday' =>array(
                        'from'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayFrom'],
                        'to'=>isset($_POST['B2bAddresses']['chkSun']) ? 'closed' : $_POST['B2bAddresses']['sundayTo']
                    ),
                ),
            );
            $model->tradingHours=json_encode(array_values($tradingHoursArray));
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
               'model' => $model,'listData'=>$listData,
            ]);
        }
    }

    /**
     * Deletes an existing B2bAddresses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the B2bAddresses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return B2bAddresses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = B2bAddresses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
