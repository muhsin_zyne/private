<?php

namespace backend\controllers;

use Yii;
use common\models\Brands;
use common\models\StoreBrandsVisibility;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use common\models\BrandSuppliers;
use yii\web\UploadedFile;
use common\models\search\BrandsSearch;
use common\models\StoreBrands;
use common\models\CmsImages;
use common\models\search\CmsImagesSearch;
/**
 * BrandsController implements the CRUD actions for Brands model.
 */
class BrandsController extends AdminController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brands models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $query = Brands::find();
        $adminUser = Yii::$app->user->identity;
        $searchModel = new BrandsSearch();
        if($adminUser->roleId == "3")
            $query = $adminUser->store->getBrands();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
        $user = Yii::$app->user->identity;
        $enabledBrands = [];
        $visibleBrands = [];
        if($user->roleId == "3") {
            $enabledBrands = \common\models\StoreBrands::findAll(['enabled'=>'1','type'=>'b2c','storeId'=>$user->store->id]);
            $visibleBrands = StoreBrandsVisibility::find()->where(['storeId'=>$user->store->id])->all();
        }
        return $this->render('index', compact('dataProvider', 'adminUser', 'enabledBrands', 'searchModel','visibleBrands'));
    }

    /**
     * Displays a single Brands model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $brand = new Brands();
        if ($brand->load(Yii::$app->request->post())){   //var_dump(Yii::$app->request->post());die();
            if(\Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($brand);
            }
            $brand->title = Yii::$app->request->post()['Brands']['title'];
            $brand->enabled = '1';
            $brand->urlKey = Yii::$app->request->post()['Brands']['urlKey']; 
            $brand->pageContent = Yii::$app->request->post()['Brands']['resolvedContent'];
            if(UploadedFile::getInstance($brand,'path')!= '')
                {
                    $image = UploadedFile::getInstance($brand, 'path');                
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/brands/logo/";    
                    $size = filesize($image->tempName);            
                    $ext = end((explode(".", $image->name)));
                    $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                    $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                    $uploadPath="/store/brands/logo/".$avatar;
                    $brand->path = $uploadPath; 

                    $fileDimensions = getimagesize($image->tempName);
                    $imageType = strtolower($fileDimensions['mime']); 

                    switch(strtolower($imageType))
                    {
                        case 'image/png':
                        $img = imagecreatefrompng($image->tempName);
                        break;

                        case 'image/gif':
                        $img = imagecreatefromgif($image->tempName);
                        break;
                        
                        case 'image/jpeg':
                        $img = imagecreatefromjpeg($image->tempName);
                        break;
                        
                        default:
                        die('Unsupported File!'); //output error
                    }  
                    
                    $brand->width=imagesx($img);
                    $brand->height=imagesy($img);

                    /*var_dump(imagesx($img));
                    var_dump(imagesy($img));
                    die();*/

                    if($brand->width!= 260 & $brand->height!= 120)
                        {                
                            Yii::$app->getSession()->setFlash('error', 'Brand Logo Size should be 260px X 120px');
                            return $this->render('create', compact('brand'));
                        }
                    else{
                        $image->saveAs($savepath);    
                    }
                }

            if($brand->save(false)){

                if(isset(Yii::$app->request->post()['Brands']['suppliers']) && is_array(Yii::$app->request->post()['Brands']['suppliers'])){
                    foreach(Yii::$app->request->post()['Brands']['suppliers'] as $supplier){
                        $brandSupplier = new BrandSuppliers;
                        $brandSupplier->brandId = $brand->id;
                        $brandSupplier->supplierUid = $supplier;
                        $brandSupplier->save(false);
                    }
                }
            }   
            Yii::$app->getSession()->setFlash('success', 'Brand created successfully');
            return $this->redirect(['index', 'id' => $brand->id]); 
        }           
        else {  
            return $this->render('create', compact('brand'));
        }
    }

    /**
     * Updates an existing Brands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $brand = $this->findModel($id);

        $logopath = $brand->path;
        
        if ($brand->load(Yii::$app->request->post())){  //var_dump(Yii::$app->request->post());die();
            if(\Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($brand);
            }
           /* if($brand->save(false)) {
                return $this->redirect(['view', 'id' => $brand->id]);
            }*/
	        $brand->urlKey = Yii::$app->request->post()['Brands']['urlKey']; 
                $brand->title = Yii::$app->request->post()['Brands']['title'];
                $brand->pageContent = Yii::$app->request->post()['Brands']['resolvedContent'];
                
                if(UploadedFile::getInstance($brand,'path')!= ''){
                    $image = UploadedFile::getInstance($brand, 'path');                
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/brands/logo/";    
                    $size = filesize($image->tempName);            
                    $ext = end((explode(".", $image->name)));
                    $avatar = Yii::$app->security->generateRandomString().".{$ext}";
                    $savepath = Yii::$app->params['uploadPath'] . $avatar; 
                    $uploadPath="/store/brands/logo/".$avatar;
                    $brand->path = $uploadPath; 

                    $fileDimensions = getimagesize($image->tempName);
                    $imageType = strtolower($fileDimensions['mime']); 

                    switch(strtolower($imageType))
                    {
                        case 'image/png':
                        $img = imagecreatefrompng($image->tempName);
                        break;

                        case 'image/gif':
                        $img = imagecreatefromgif($image->tempName);
                        break;
                        
                        case 'image/jpeg':
                        $img = imagecreatefromjpeg($image->tempName);
                        break;
                        
                        default:
                        die('Unsupported File!'); //output error
                    }    
                    
                    $brand->width=imagesx($img);
                    $brand->height=imagesy($img);
                    if($brand->width!= "260" & $brand->height!= "120")
                        {                
                            Yii::$app->getSession()->setFlash('error', 'Brand Logo Size should be 260px X 120px');
                            return $this->render('update', compact('brand'));
                        }
                    else{
                            // if (file_exists($logoImage =  Yii::$app->params['uploadPath'].$logopath)) { //die('present');
                            //     unlink($logoImage);
                            //     $image->saveAs($savepath);
                            // }
                            // else
                                $image->saveAs($savepath);        
                    }
                }
                else
                    $brand->path = $logopath;
                $justDisabled = false;
                if($brand->oldAttributes['enabled'] == "1" && $brand->enabled == "0")
                    $justDisabled = true;
                if($brand->save(false)){ 
                    if($justDisabled)
                        $brand->disableProducts();
                    Yii::$app->getSession()->setFlash('success', 'Brand updated successfully');
                    return $this->redirect(['index', 'id' => $brand->id]);
                }
            }
            else {
                return $this->render('update', compact('brand'));
            }
    }

    /**
     * Deletes an existing Brands model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        throw new \yii\web\NotFoundHttpException('Page Not Found');
        die;
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionEnable(){
        $user = \common\models\User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;
        $currentEnabledBrands = isset($_POST['currentEnabledBrands'])? $_POST['currentEnabledBrands'] : [];
        $enabledBrands = isset($_POST['enabledBrands'])? $_POST['enabledBrands'] : [];
        $disabledBrands = array_diff(array_values($enabledBrands), $currentEnabledBrands);
        //var_dump($disabledBrands); die;
        foreach ($currentEnabledBrands as $brand) {
            if (!$existingBrands = \common\models\StoreBrands::find()->where(['storeId' => $storeId, 'brandId' => $brand, 'type' => 'b2c'])->one()){
                $existingBrands = new \common\models\StoreBrands;
                $existingBrands->storeId = $storeId;
                $existingBrands->brandId = $brand;
            }
            $existingBrands->enabled = "1";
            if($existingBrands->save(false)){
                Brands::findOne($existingBrands->brandId)->setProducts($storeId, 'b2c');
            }
        }    
        if(!empty($disabledBrands)){ //var_dump($disabledBrands);die();
            foreach ($disabledBrands as $brand) { 
                $brand = (int)$brand;
                $existingBrands = \common\models\StoreBrands::find()->where(['storeId' => $storeId, 'brandId' => $brand, 'type' => 'b2c'])->one();
                //var_dump($existingBrands);die();
                $existingBrands->enabled = "0";
                if($existingBrands->save(false)){  //die('saved');
                    Brands::findOne($existingBrands->brandId)->unsetProducts($storeId, 'b2c');
                }
            }   
        }    

        die("Updated Successfully");
    }

    public function actionContentsaveimages(){   //die('save image');
        $model = new Brands();
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        $image = UploadedFile::getInstance($model, 'contentImage');
        $ext = end((explode(".", $image->name)));
        $avatar = Yii::$app->security->generateRandomString().".{$ext}";
        $path = Yii::$app->params['uploadPath'] . $avatar;
        $model_image = new CmsImages();
        $model_image->title = $avatar;
        $model_image->save();
        $image->saveAs($path);
        return true;
    }

    public function actionGetimages()
    { 
        $html='<ol id="selectable">';
        $ckimage_all= CmsImages::find()->all();
        foreach ($ckimage_all as $index => $ckimage) 
        {
           //$html.='<tr> <td>'.$ckimage['title'].'</td> </tr>';
            $html.='<li class="ui-state-default" id="'.$ckimage['id'].'">
                <img src ="'.Yii::$app->params["rootUrl"].'/store/cms/images/'.$ckimage['title'].'" height="110" width="140">
            </li>';
  
              
        }                     
        $html.='</ol> ';   
        echo  $html; 
    }    

    public function actionTest()
    {
        $storeBrands = StoreBrands::find()->where(['type' => 'b2c'])->all();
        foreach($storeBrands as $sb){
            if($storeBrand = StoreBrands::find()->where(['storeId' => $sb->storeId, 'brandId' => $sb->brandId, 'type' => $sb->type, 'enabled' => $sb->enabled])->andWhere("id != ".$sb->id)->one()){
                StoreBrands::deleteAll("storeId = ".$sb->storeId." AND brandId = ".$sb->brandId." AND type = '".$sb->type."' AND enabled = ".$sb->enabled." AND id != ".$sb->id);
            }
        }
    }

    public function actionList($keyword){
        if(Yii::$app->user->identity->roleId == "3"){
            $brands = Brands::find()->join('LEFT JOIN', 'StoreBrands sb', 'sb.brandId = Brands.id')
                ->where("title like '%".$keyword."%' and sb.storeId =  '".Yii::$app->user->identity->store->id."'")->all();
        }
        else
            $brands = Brands::find()->where("title like '%".$keyword."%'")->all();   

        foreach($brands as $brand)
            $options[] = ['id' => $brand->id, 'title' => $brand->title];
        
        return json_encode($options);
    }

    /*public function actionDummyproducts(){
        $user = \common\models\User::findOne(Yii::$app->user->Id);
        $storeId = $user->store->id;

        $storeBrands = StoreBrands::find()->where(['type' => 'b2c','storeId'=>$storeId])->all();

    }*/

    /**
     * Finds the Brands model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brands the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brands::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionImageBrowse()// ckeditor image browse 
    { 
        $cmsImages=CmsImages::find()->orderBy(['id' => SORT_DESC])->all();        
        return $this->renderPartial('image',compact('cmsImages'));
    }
    public function actionImageUpload()// ckeditor image upload
    {    
        Yii::$app->params['uploadPath'] = Yii::$app->basePath."/../store/cms/images/";
        $uploadedFile = UploadedFile::getInstanceByName('upload'); 
        //$mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);
        $file = time()."_".$uploadedFile->name;
        $url=Yii::$app->params["rootUrl"]."/../store/cms/images/".$file; 
        $uploadPath = Yii::$app->params['uploadPath'].$file;
        $check = getimagesize($uploadedFile->tempName);
        $model_image = new CmsImages();
        $model_image->title = $file;
        $model_image->save();  

        //var_dump($uploadPath);die;
        //extensive suitability check before doing anything with the file…
        if ($uploadedFile==null)
        {
           $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
           $message = "The file is of zero length.";
        }
        else if($check['mime']!='image/jpeg' && $check['mime']!='image/jpg' && $check['mime']!='image/png'){
            $message = "The image must be in either JPG, JPEG or PNG format. Please upload a JPG or PNG instead.";
            $url='';
        }
        /*else if ($mime!="image/jpeg" && $mime!="image/png")
        {
           $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        }*/
        else if ($uploadedFile->tempName==null)
        {
           $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
          $message = "";
          $move = $uploadedFile->saveAs($uploadPath);
          if(!$move)
          {
             $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
          } 
        }        
        $funcNum = $_GET['CKEditorFuncNum'] ;
        //var_dump($_GET['CKEditorFuncNum']);die;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";        
    }
}
