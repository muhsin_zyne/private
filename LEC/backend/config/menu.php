<?php

use yii\helpers\Url;

return $menus= 

	[

	    [



        'url' => Url::to(['site/index']),

        'label' => 'Dashboard',

        'icon' => 'tachometer',

        'visible' => $this->isVisible('Dashboard'),

        'options' => ['class' => 'tachometer'],

      ],  



            

      [

	      'url' => '#',

	      'label' => 'Sales',

	      'icon' => 'shopping-cart',

	      'visible' => $this->isVisible('Sales'),

        'options' => ['class' => 'shopping-cart'],

	      'items' => [

	        [ 

	          'label' => 'Orders', 

	          'icon'=>'', 

	          'url'=>Url::to(['orders/index']),

	          'visible' => $this->isVisible('Orders'),

	         // 'visible' => (array_key_exists("Orders",$this->siteAdminPermissions)) ? true : false,

	         ],

           [ 

                  'label' => 'Failed Transactions', 

                  'icon'=>'', 

                  'url'=>Url::to(['orders/failed-transactions']),

                  'visible' => $this->isVisible('Orders'),

                 // 'visible' => (array_key_exists("Orders",$this->siteAdminPermissions)) ? true : false,

            ],



	        [

	          'label' => 'Invoices', 

	          'icon'=>'', 

	          'url'=>Url::to(['invoices/index']),

	          'visible' => $this->isVisible('Invoices'),

	        ],



	        [

	          'label' => 'Shipments', 

	          'icon'=>'', 

	          'url'=>Url::to(['shipments/index']),

	          'visible' => $this->isVisible('Shipments'),

	        ],

          [

              'label' => 'Deliveries', 

              'icon'=>'', 

              'url'=>Url::to(['delivery/index']),

              'visible' => $this->isVisible('Shipments'),

          ],



	        [

	          'label' => 'Credit Memos', 

	          'icon'=>'', 

	          'url'=>Url::to(['credit-memos/index']),

	          'visible' => $this->isVisible('Credit Memos'),

	        ],



	    ],

	],



    [



        'url' => Url::to(['orders/order']),

        'label' => 'Order from Supplier',

        'icon' => 'truck',

        'visible' => $this->isVisible('Order from Supplier'),

        'options' => ['class' => 'truck'],



    ],





    [

        'url' => '#',

        'label' => 'Catalogue',

        'icon' => 'diamond',

        'options' => ['class' => 'diamond'],

        'items' => [

                    ['url' => \yii\helpers\Url::to(['/products']),

                       'label' => 'Manage Products',

                       'icon' => '',

                       'visible' => $this->isVisible('Manage Products'),

                    ],



                    ['url' => Url::to(['/categories']),

                       'label' => 'Manage Categories',

                       'icon' => '',

                       'visible' => $this->isVisible('Manage Categories'),

                    ],



                    ['url' => '#',

                       'label' => 'Attributes',

                       'icon' => '',

                       'visible' => $this->isVisible('Attributes'),

                       'items' => [['label' => 'Manage Attributes', 'icon'=>'', 'url'=>Url::to(['/attributes']),'visible' => $this->isVisible('Manage Attributes')],

                                   ['label' => 'Manage Attributes Sets', 'icon'=>'', 'url'=>Url::to(['/attribute-sets']),'visible' => $this->isVisible('Manage Attributes Sets')]]  

                    ],



                   ],

    ],



    [

        'url' => '#',

        'label' => 'Product Positioning',

        'icon' => 'th-large',

        'options' => ['class' => 'th-large'],

        'visible' => $this->isVisible('Product Positioning'),

        'items' => [

            [ 

              'url' => \yii\helpers\Url::to(['brand-product-position/update']),

              'label' => 'By Brand',

              'icon' => '',

              'visible' => $this->isVisible('By Brand'),

            ],

            [

              'url' => Url::to(['category-product-position/update']),

              'label' => 'By Category',

              'icon' => '',

              'visible' => $this->isVisible('By Category'),

            ],

        ],

    ],    



    [



        'url' => '#',

        'label' => 'All Users',

        'icon' => 'users',

        'options' => ['class' => 'users'],

        'visible' => $this->isVisible('Users'),

        'items' => [

          ['label' => 'Customers', 'icon'=>'', 'url'=>Url::to(['/users']),'visible' => $this->isVisible('Customers')],

          ['label' => 'Subscribers', 'icon'=>'', 'url'=>Url::to(['/subscribers']),'visible' => $this->isVisible('Subscribers')]

        ]

    ],



    [



        'url' => Url::to(['/price-rules']),

        'label' => 'Promo Codes',

        'visible' => $this->isVisible('Coupon Codes'),

        'icon' => 'tags',

        'options' => ['class' => 'tags'],

    ],

    

     [



          //'url' => Url::to(['/cms']),

          'label' => 'CMS',

          'icon' => 'clipboard',

          'options' => ['class' => 'clipboard'],

          //'visible' => $this->isVisible('CMS'),

          'items' => [

              ['url' => Url::to(['/cms']),

                 'label' => 'CMS Pages',

                 'icon' => '',

                 'visible' => $this->isVisible('CMS'),

              ],



              ['url' => Url::to(['/cms-blocks']),

                 'label' => 'CMS Blocks',

                 'icon' => '',

                 'visible' => $this->isVisible('CMS Blocks'),

              ],

              ['url' => Url::to(['/category-blocks']),

                 'label' => 'Category Blocks',

                 'icon' => '',

                 'visible' => $this->isVisible('CMS Blocks'),

              ],

          ],

      ],



    [



        'url' => Url::to(['/gift-vouchers']),

        'label' => 'Gift Vouchers',

        'icon' => 'gift',

        'visible' => $this->isVisible('Gift Vouchers'),

         'options' => ['class' => 'gift'],

    ],



    [



        'url' => '#',

        'label' => 'My Concierge',

        'icon' => 'shopping-bag',

        'options' => ['class' => 'shopping-bag'],

        'visible' => $this->isVisible('Client Portal'),

        'items' => [

          ['label' => 'Manage My Concierge', 'icon'=>'', 'url'=>Url::to(['/my-concierge']),'visible' => $this->isVisible('Client Portal')],

          ['label' => 'My Concierge Orders', 'icon'=>'', 'url'=>Url::to(['my-concierge-orders/index']),'visible' => $this->isVisible('Client Portal')]

        ]

    ],



    [



        'url' => '#',

        'label' => 'Reports',

        'visible' => $this->isVisible('Reports'),

        'icon' => 'download',

        'options' => ['class' => 'download'],

        'items' => [['label' => 'Sales', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Sales'),

                    'items' => [['label' => 'Orders', 'icon'=>'', 'url'=>Url::to(['reports/orders']),'visible' => $this->isVisible('Orders')],

                                //['label' => 'Invoiced', 'icon'=>'', 'url'=>Url::to(['reports/invoiced']),'visible' => $this->isVisible('Invoiced')],

                                ['label' => 'Shipping', 'icon'=>'', 'url'=>Url::to(['reports/shipped']),'visible' => $this->isVisible('Shipping')],

                                ['label' => 'Refunds', 'icon'=>'', 'url'=>Url::to(['reports/refunds']),'visible' => $this->isVisible('Refunds')],

                                //['label' => 'Paypal Settlement Reports', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Paypal Settlement Reports')]

                                ]],

                    ['label' => 'Shopping Cart', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Shopping Cart'),

                    'items' => [

                                ['label' => 'Abandoned Carts', 'icon'=>'', 'url'=>Url::to(['reports/abandonedcarts']),'visible' => $this->isVisible('Abandoned Carts')]]

                            ],

            ],



    ],

            

     [

                'url' => '#',

                'label' => 'Gift Registry',

                'icon' => 'heart-o',

                'options' => ['class' => 'heart-o'],

                'visible' => $this->isVisible('GiftRegistry'),

                'items' => [

                    [

                        'label' => 'Manage Gift Registry',

                        'icon' => '',

                        'url' => Url::to(['gift-registry/index']),

                        'visible' => $this->isVisible('GiftRegistry'),

                    // 'visible' => (array_key_exists("Orders",$this->siteAdminPermissions)) ? true : false,

                    ],

                    [

                        'label' => 'Gift Registry Orders',

                        'icon' => '',

                        'url' => Url::to(['orders/giftregistry-orders']),

                        'visible' => $this->isVisible('GiftRegistry'),

                    // 'visible' => (array_key_exists("Orders",$this->siteAdminPermissions)) ? true : false,

                    ],

                ],

            ],

            [



          //'url' => Url::to(['/byod']),

          'url' => '#',

          'label' => 'Appointments',

          'icon' => 'book',

          'options' => ['class' => 'book'],

          'visible' => $this->isVisible('Appointments'),

          'items' => [

                ['url' => Url::to(['appointments/index']),

                   'label' => 'Manage Appointments',

                   'icon' => '',

                   'visible' => $this->isVisible('Appointments'),

                ],



                ['url' => Url::to(['appointment-settings/index']),

                   'label' => 'Default Settings',

                   'icon' => '',

                   'visible' => $this->isVisible('Appointments'),

                ],

          ],



    ],



    [



        'url' => '#',

        'label' => 'System',

        'icon' => 'cogs',

        'visible' => $this->isVisible('System'),

        'options' => ['class' => 'cogs'],

        'items' => [



                    // [  'url' => '#',

                    //    'label' => 'My Account',

                    //    'icon' => '',

                    //    'visible' => $this->isVisible('My Account'),

                    // ],



                    // [   'url' => '#',

                    //    'label' => 'Notifications',

                    //    'icon' => '',

                    //    'visible' => $this->isVisible('Notifications'),

                    // ],         



                    ['label' => 'Import / Export', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Import / Export'),

                    'items' => [['label' => 'Import', 'icon'=>'', 'url'=>Url::to(['import/index']),'visible' => $this->isVisible('Import')],

                                // ['label' => 'Export', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Export')],

                                // ['label' => 'Dataflow Profiles', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Dataflow Profiles')],

                                // ['label' => 'Dataflow Advanced Profiles', 'icon'=>'', 'url'=>'#','visible' => $this->isVisible('Dataflow Advanced Profiles')]]],

                    ],

                    ],



                  ['url' => Url::to(['stores/manage']),

                       'label' => 'Store Management',

                       'visible' => $this->isVisible('Store Management'),

                       'icon' => '',

                  ],
                    



                  ['url' => Url::to(['configuration/index']),

                       'label' => 'Configurations',

                       'icon' => '',

                       'visible' => $this->isVisible('Configurations'),

                  ],
                    ['url' => Url::to(['b2b-address/index']),
                       'label' => 'B2b Address',
                       'visible' => $this->isVisible('B2b Address'),
                       'icon' => '',
                    ],




    		],

    ],

    [

        'url' => '#',

        'label' => 'B2B Management',

        'visible' => $this->isVisible('B2B Management'),

        'options' => ['class' => 'list-alt'],

        'icon' => 'list-alt',

        'items' => [

                    ['url' => Url::to(['/b2b']),

                       'label' => 'B2B Dashboard',

                       'icon' => '',

                       'visible' => $this->isVisible('B2B Dashboard'),

                    ],



                    ['url' => Url::to(['b2b/orders']),

                       'label' => 'B2B Orders',

                       'icon' => '',

                       'visible' => $this->isVisible('B2B Orders'),

                    ],



                    ['url' => '#',

                       'label' => 'B2B Banners',

                       'icon' => '',

                       'visible' => $this->isVisible('Banners'),

                       'items' => [

                          ['url' => Url::to(['/banners','id'=>'b2b','type'=>'b2b-main']),

                             'label' => 'Main Banners',

                             'icon' => '',

                            // 'visible' =>  $this->isVisible('Banners','mainbanner_resolution'),

                          ],



                          ['url' => Url::to(['/banners','id'=>'b2b','type'=>'b2b-mini']),

                             'label' => 'Mini Banners',

                             'icon' => '',

                            // 'visible' =>  $this->isVisible('Banners','minibanner_resolution'),

                          ],



                          ['url' => Url::to(['/banners','id'=>'b2b','type'=>'b2b-side']),

                             'label' => 'Side Banners',

                             'icon' => '',

                             //'visible' => $this->isVisible('Banners','sidebanner_resolution'),

                          ],

                              

                      ],

                    ],

                    ['url' => Url::to(['/consumer-promotions']),

                       'label' => 'Consumer Promotions',

                       'icon' => '',

                      'visible' => $this->isVisible('Consumer Promotions') ,

                    ],



                  



                    ['url' => Url::to(['/conferences']),

                       'label' => 'Conferences',

                       'icon' => '',

                      'visible' => $this->isVisible('Conferences') ,

                    ], 



                    ['url' => Url::to(['conferences/list']),

                       'label' => 'Conference Orders',

                       'icon' => '',

                       'visible' => $this->isVisible('Conference Orders'),

                    ], 



                    

                ],

    ],

    [

        'url' => Url::to(['/brands']),

        'label' => 'Brands',

        'visible' => $this->isVisible('Brands'),

        'icon' => 'cubes',

        'options' => ['class' => 'cubes'],

    ], 

    [

        'url' => Url::to(['/suppliers']),

        'label' => 'Suppliers',

        'visible' => $this->isVisible('Suppliers'),

        'icon' => 'users',

        'options' => ['class' => 'users'],

    ], 

    [

          'url' => '#',

          'label' => 'B2C Banners',

          'visible' => $this->isVisible('Menu'),

          'icon' => 'newspaper-o',

          'options' => ['class' => 'newspaper-o'],

          'items' => [

              ['url' => Url::to(['/banners','id'=>'b2c','type'=>'b2c-main']),

                 'label' => 'Main Banners',

                 'icon' => '',

                 'visible' =>  $this->isVisible('Banners','mainbanner_resolution'),

              ],



              ['url' => Url::to(['/banners','id'=>'b2c','type'=>'b2c-mini']),

                 'label' => 'Mini Banners',

                 'icon' => '',

                 'visible' =>  $this->isVisible('Banners','minibanner_resolution'),

              ],



              ['url' => Url::to(['/banners','id'=>'b2c','type'=>'b2c-side']),

                 'label' => 'Side Banners',

                 'icon' => '',

                 'visible' => $this->isVisible('Banners','sidebanner_resolution'),

              ],

                  

          ],

    ],

    [

        'url' => Url::to(['/b2b/promotions']),

        'label' => 'Opt into Promotion',

        'visible' => $this->isVisible('Opt into Promotion'),

        'icon' => 'suitcase',

        'options' => ['class' => 'suitcase'],

    ],  

    [

        'url' => Url::to(['/gallery']),

        'label' => 'Gallery',

        'visible' => $this->isVisible('Gallery'),

        'icon' => 'picture-o',

        'options' => ['class' => 'picture-o'],

    ],

    [

        'url' => Url::to(['/menus']),

        'label' => 'Menu Management',

        'visible' => $this->isVisible('Menu'),

        'icon' => 'sitemap',

        'options' => ['class' => 'sitemap'],

    ],

    [

        'url' => Url::to(['/social-media']),

        'label' => 'Social Media Management',

        'visible' => $this->isVisible('Social Media Management'),

        'icon' => 'share-alt',

        'options' => ['class' => 'share-alt'],

    ],



      /*[

        'url' => '#',

        'label' => 'Facebook Feed',

        'visible' => $this->isVisible('Facebook Feed'),

        'icon' => 'home',

      ],



      [

        'url' => \yii\helpers\Url::to(['/products/create']),

        'label' => 'Add Product',

        //'options' =>['class'=>'ajax-update'],

        'visible' => $this->isVisible('Add Product'),

        'icon' => 'home',

      ],  */



    ];

