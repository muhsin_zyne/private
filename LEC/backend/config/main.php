<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'treemanager' => [
            'class' => '\kartik\tree\Module',

            'treeStructure' => [ 
                    'treeAttribute' => 'root',
                    'leftAttribute' => 'lft',
                    'rightAttribute' => 'rgt',
                    'depthAttribute' => 'lvl',
                 ],
            
            'dataStructure' => [
                    'keyAttribute' => 'id',
                    'nameAttribute' => 'title',
                    'iconAttribute' => 'icon',
                    'iconTypeAttribute' => 'icon_type'
                ],

           /* 'treeViewSettings'=> [

                        'nodeActions' => [
                
                // \kartik\tree\Module::NODE_MANAGE => \dirname(__DIR__)\Url::to(['/treemanager/node/manage']),
                // \kartik\tree\Module::NODE_SAVE => \dirname(__DIR__)\Url::to(['/treemanager/node/save']),
                // \kartik\tree\Module::NODE_REMOVE => \dirname(__DIR__)\Url::to(['/treemanager/node/remove']),
                // \kartik\tree\Module::NODE_MOVE => \dirname(__DIR__)\Url::to(['/treemanager/node/move']),



                   // \kartik\tree\Module::NODE_MANAGE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%2Fmanage',
                   //  \kartik\tree\Module::NODE_SAVE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%save',
                   //  \kartik\tree\Module::NODE_REMOVE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%remove',
                   //  \kartik\tree\Module::NODE_MOVE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%move',
                                        


                    \kartik\tree\Module::NODE_MANAGE => '/projects/inte/backend/web/index.php?r=debug',
                    \kartik\tree\Module::NODE_SAVE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%save',
                    \kartik\tree\Module::NODE_REMOVE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%remove',
                    \kartik\tree\Module::NODE_MOVE => '/projects/inte/backend/web/index.php?r=treemanager%2Fnode%move',
                                            

                                        ] ,

                    'nodeView' => '_form'                ]     */

        ],

      
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
    ],
        'view' => [
          'theme' => [
             'pathMap' => [ 
                '@app/views' => [ 
                    '@webroot/themes/adminlte',

                 ]
             ],
             'baseUrl' => '@web/',
           ],
        ],
        'AdminNavBar' => [
            'class' => 'backend/components/AdminNavBar'
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
            ],
        ],
        'attributeRenderer' => [
            'class' => 'backend\components\AttributeRenderer'
        ],
        'importer' => [
            'class' => 'backend\components\Importer'
        ],
    ],
    'params' => $params,
];
