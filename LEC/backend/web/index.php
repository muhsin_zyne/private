<?php
error_reporting(-1);
ini_set('display_errors', 'On');
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
    //require(__DIR__ . '/../../frontend/config/site1.inte.everybuy.com.au-config.php')
);

$application = new yii\web\Application($config);
//Yii::$app->params['temp']['siteConfig'] = require(__DIR__ . '/../../frontend/config/site1.inte.everybuy.com.au-config.php');
$application->run();
