$(document).ready(function(){
	$('.nav-tabs-custom').on('afterValidate', 'form', function(){
		$('.tab-pane').each(function(){
		    if($(this).find('.has-error').length > 0){
		        $('.nav-tabs li a[href="#'+$(this).attr("id")+'"]').click();
		    }
		})
	})

	$('.report-export').click(function(){
		$(this).parents('form').append('<input type="hidden" class="export-report" name="ReportForm[export]" value="1"/>');
		$(this).parents('form').submit();
	})

	$('.report-generate').click(function(){
		$(this).parents('form').find('.export-report').remove();
		$(this).parents('form').submit();
	})

	/*$('.grid-view tbody td').click(function (e) { 
		$(this).siblings(":last").find('a').get(0).click();
	});*/
	
	
	var availableTags = [];
	
  	$('body').on('keyup','.apply-autocomplete',function(){
		$.ajax({
			type: "GET",
			url: $(this).attr('data-url'),
			data:'keyword='+$(this).val(),
	    	success: function(data){
	    		$.each($.parseJSON(data), function(key, item) {
	    			var idx = $.inArray(item['title'], availableTags);
	    			if (idx == -1) {  
	    				availableTags.push(item['title']);
	    			}
	    			else {
  						availableTags.splice(idx, 1);
					}	
	    			//console.log(availableTags);
	    		});	
	    	}	
	    });	

	    $( ".apply-autocomplete" ).autocomplete({
	      source: availableTags
	    });
	});

	$( "ul.nav-stacked li a" ).each(function() {
  		if($( this ).attr("href") == window.location.pathname){
     		$(this).addClass('cat-active');
     		$(this).parent().parent().parent().addClass('active');
  		}
	});

	/*$('#nextbutton').click(function(e){
	    var next = $('.active').next().find('a').attr('href');
	    e.preventDefault();
	    $('#toggle-mytab a[href="'+next+'"]').tab('show');
	    $('#backbutton').show();
		if($('ul#toggle-mytab li:last').hasClass('active')){ 
			$('#nextbutton').hide(); 
		}
	})
	
	$('#backbutton').click(function(e){
	    var prev = $('ul .active').prev().find('a').attr('href'); console.log(prev);
	    e.preventDefault();
	    $('#toggle-mytab a[href="'+prev+'"]').tab('show');
	    if($('ul#toggle-mytab li:first').hasClass('active')){ 
			$('#backbutton').hide(); 
		}
		$("#nextbutton").show();
	})

	if($('ul#toggle-mytab li:first').hasClass('active')){ 
			$('#backbutton').hide(); 
		} */
	$('#nextbutton').click(function(e){
	    var next = $('.active').next().find('a').attr('href');
	    e.preventDefault();
	    $('#toggle-mytab a[href="'+next+'"]').tab('show');
	    $('#backbutton').show();
		if($('ul#toggle-mytab li:last').hasClass('active')){ 
			$('#nextbutton').hide(); 
		}
	})
	
	$('#backbutton').click(function(e){
	    var prev = $('ul .active').prev().find('a').attr('href'); console.log(prev);
	    e.preventDefault();
	    $('#toggle-mytab a[href="'+prev+'"]').tab('show');
	    if($('ul#toggle-mytab li:first').hasClass('active')){ 
			$('#backbutton').hide(); 
		}
		$("#nextbutton").show();
	})
	
  $('#toggle-mytab li').click(function(){

	  		$('#nextbutton').show(); 
	  	    $('#backbutton').show();
   })

    $('#backbutton').hide(); 
});	