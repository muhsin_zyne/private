function updatePage(updateIds){
    if (updateIds) {
        myUpdateIds = updateIds.split(',');
        for(var n=0; n<myUpdateIds.length; n++){
            updateId = myUpdateIds[n];
            //console.log(updateId);
            var $e = $("#" + updateId);
            $.pjax.reload({container:"#" + updateId});
        }
    }
}

function validateForm(id){
    $("#" + id + " form").find('input, select, textarea').each(function(){
        $("#" + id + " form").yiiActiveForm('validateAttribute', $(this).attr("id"));
    })
}

function validateAndSubmit(id){
    setTimeout(function(){ //MS: doing this because yii has 200ms delay for the validation to be reflected.
        if($("#" + id + " form").find('.has-error').contents().length < 1){
            window.formValidated = true;
            $("#" + id + " form").wl_Form('submit');
        }
    }, 300);
}

$ = jQuery;
$(document).ready(function(){
    window.formValidated = false;
    $('body').on('click','.bootstrap-modal',function(e){
        "use strict";
        e.preventDefault();
        if($(this).attr("type")=="checkbox"){
            if($(this).attr("data-configurable")!="true" || !$(this).is(':checked')){
                throw new Error('Not an error');
            }
        }
        if($('div#my-modal').length>0) {
            $('div#my-modal').remove(); 
        }
        $('body').append('<div class="modal fade bs-example-modal-lg" id="my-modal" role="dialog">');
        if($(this).hasClass("editFieldButton")){
            var dataid = $('#AvailableFields option:selected, #Available option:selected').eq(0).attr('data-id');
            if(dataid)
                var Data = {ajax:true, id:dataid};
            else{
                var Data={};
                $.msg('You must select a field before you can edit it!', {header: 'Warning!'});
                return false;
            }
        }
        var myUpdateIds= [];
        var updateId = null;
        var $a = $(this),
                myUrl = '',
                $content = '',
        urls = $a.attr('href'),
        id = 'my-modal',
        myTitle = $a.attr('data-title') || $a.attr('title')  || $a.attr('original-title'),
        myWidth = $a.attr('data-width'),
        $dialog = $("#" + id),
        updateIds = $a.attr('data-update'),
        //refreshDiv = $a.attr('data-refresh'),
        js = $a.attr('data-ajax'),
        myData=Data,
        stop = false,
        jsloaded = $a.attr('data-ajax-loaded'),
        alertonly = $a.attr('data-alert'),
        oncompleted = $a.attr('data-complete');
        var beforesubmit = $a.attr('data-before-submit');
        var dataparameters = $a.attr('data-parameters');
        var dataparameters = (typeof $a.attr('data-parameters') != 'undefined' && $a.attr('data-parameters') != "") ? $.parseJSON($a.attr('data-parameters')) : '';
        if (urls) { 
            if (js) {
                eval(js);
                if (stop === true) {  
                    return false;
                }
            }

            if(alertonly){
                myData = $a.parent().parent().find('input,textarea,select').serializeArray();
                console.log(myData);
                $.ajax({
                    type: "POST",
                    url: urls,
                    data: myData,
                    success:function(data,responseText,jqXHR){
                        updatePage(updateIds);
                        $.gritter.add({
                            title: 'Success!',
                            text: jqXHR.responseText
                        });
                        if(jsloaded){
                            eval(jsloaded);
                        }
                    },
                    error:function(jqXHR,responseText){
                        $.gritter.add({
                            title: 'Warning!',
                            text: jqXHR.responseText
                        });
                    }
                });
                        
            }

            else{
                $('.modal').load(urls,function(result,responseText, jqXHR){ 
                    if (responseText === 'error') {
                        $.gritter.add({
                            title: 'Warning!',
                            text: jqXHR.responseText
                        });
                        return false;
                    } 
                    else if (responseText === 'success') {
                        var $inputs = $("#" + id + " select");
                        $("#" + id + " form").not('.nobind').wl_Form({
                            ajax: true,
                            url: urls,
                            method: 'post',
                            onBeforeSubmit: function (data) { console.log(data);
                                eval(beforesubmit);
                                validateForm(id); //return false;
                                e.preventDefault();
                                if(!window.formValidated){
                                    validateAndSubmit(id);
                                    return false;
                                }
                                if($("#" + id + " form").find('.has-error').contents().length>0){ 
                                    return false;
                                };
                                // alert('beforeTimeout');
                            },
                            onValidError: function (element) {
                            
                            },
                            onError: function (textStatus, error, jqXHR) {
                                $.gritter.add({
                                    title: 'Warning!',
                                    text: jqXHR.responseText
                                });
                                fixButtonColumn();
                                return false;
                            },
                            onSuccess: function (data, textStatus, jqXHR) {  //console.log(data); console.log(textStatus); console.log(jqXHR); 
                                
                                //$.pjax.reload({container:'#address-block'});
                                updatePage(updateIds);
                                $('#my-modal').modal('hide');
                                console.log(dataparameters.noFlash);
                                if (typeof dataparameters != 'object') {
                                    $.gritter.add({
                                        title: 'Success!',
                                        text: jqXHR.responseText
                                    });
                                } else if (dataparameters.noFlash == true) {
                                     window.location.reload();
                                }
                                
                            },
                            onComplete: function (textStatus, jqXHR) {
                                eval(oncompleted);
                            },
                            onRequireError: function (element) {},
                            onBeforePrepare: function (element) {},
                        });

                        if (jsloaded) {
                            eval(jsloaded);
                        }

                        $('#my-modal').modal({show:true});
                    }
                });        
            }    
        }
    });
});     