/**
 * centralizes the actual page update fir data-update
 */
function updatePage(updateIds){
    if (updateIds) {
        myUpdateIds = updateIds.split(',');
        for(var n=0; n<myUpdateIds.length; n++){
            updateId = myUpdateIds[n];
            var $e = $("#" + updateId);
            $.pjax.reload({container:"#" + updateId});
        }
    }
}
$ = jQuery;
$(document).ready(function(){
$('body').on('click','.ajax-update, .delete, a.ajax-update span',function(e){  
    "use strict";
    if($(this).attr("type")=="checkbox"){
        if($(this).attr("data-configurable")!="true" || !$(this).is(':checked')){
            throw new Error('Not an error');
        }
    }
    
    if($('div#ajax-update').length>0) 
        $('div#ajax-update').remove(); //return false;
    $('body').append($('<div>').attr('id', 'ajax-update'));
    if($(this).hasClass("editFieldButton")){
        var dataid = $('#AvailableFields option:selected, #Available option:selected').eq(0).attr('data-id');
        if(dataid)
            var Data = {ajax:true, id:dataid};
        else{
            var Data={};
            $.msg('You must select a field before you can edit it!', {header: 'Warning!'});
            return false;
        }
    }
        var myUpdateIds= [];
        var updateId = null;
        var $a = $(this),
                myUrl = '',
                $content = '',
        urls = $a.attr('href'),
        id = 'ajax-update',
        myTitle = $a.attr('data-title') || $a.attr('title')  || $a.attr('original-title'),
        myWidth = $a.attr('data-width'),
        $dialog = $("#" + id),
                //$dialog = $('<div></div>');
        updateIds = $a.attr('data-update'),
        js = $a.attr('data-ajax'),
        myData=Data,
        stop = false,
        jsloaded = $a.attr('data-ajax-loaded'),
        alertonly = $a.attr('data-alert'),
        oncompleted = $a.attr('data-complete');
        var beforesubmit = $a.attr('data-before-submit');
        var dataparameters = $a.attr('data-parameters');
        console.log(urls);

	    var dataparameters = (typeof $a.attr('data-parameters') != 'undefined' && $a.attr('data-parameters') != "") ? $.parseJSON($a.attr('data-parameters')) : '';

            if (urls) { 
        // do ajax jquery to get the info from the page selected
            if (js) {
                eval(js);
            if (stop === true) {  
                return false;
            }
            }
                    if(alertonly){
                        //if(!js){
                            myData = $a.parent().parent().find('input,textarea,select').serializeArray();
                        //}
                        console.log(myData);
                        $.ajax({
                            type: "POST",
                            url: urls,
                            data: myData,
                            success:function(data,responseText,jqXHR){
                                updatePage(updateIds);
                                $.gritter.add({
                                    title: 'Success!',
                                    text: jqXHR.responseText
                                });
                if(jsloaded){
                    eval(jsloaded);
                                }
                            },
                            error:function(jqXHR,responseText){
                                $.gritter.add({
                                    title: 'Warning!',
                                    text: jqXHR.responseText
                                });
                            }
                            });
                        
                    } else {
                    showPleaseWait($dialog, myTitle, myWidth);

            $dialog.load(urls, myData, function (data, responseText, jqXHR) { 
                closePleaseWait($dialog);
                $dialog.dialog('option', 'title', myTitle);
            if (responseText === 'error') {
                $.gritter.add({
                    title: 'Warning!',
                    text: jqXHR.responseText
                });
                return false;
            } else if (responseText === 'success') {
                var $inputs = $("#" + id + " select");
                $("#" + id + " form").not('.nobind').wl_Form({
                    ajax: true,
                    url: urls,
                    method: 'post',
                    onBeforeSubmit: function (data) {
                        eval(beforesubmit);
                    },
                    onValidError: function (element) {
                        //              $.msg("One or more fields are not valid!",{header: "Warning!"});
                    },
                    onError: function (textStatus, error, jqXHR) {
                        //$dialog.dialog('close');
                        $.gritter.add({
                            title: 'Warning!',
                            text: jqXHR.responseText
                        });
                                                fixButtonColumn();
                        return false;
                    },
                    onSuccess: function (data, textStatus, jqXHR) {
                        updatePage(updateIds);  
                        console.log(dataparameters.noFlash);
                        if(typeof dataparameters != 'object'){
                            $.gritter.add({
                                title: 'Success!',
                                text: jqXHR.responseText
                            });
                        }else if(dataparameters.noFlash == false){
                            $.gritter.add({
                                title: 'Success!',
                                text: jqXHR.responseText
                            });
                        }
                        $dialog.dialog('close');
                    },
                    onComplete: function (textStatus, jqXHR) {
                                            
                                             eval(oncompleted);
                                        },
                    onRequireError: function (element) {}
                });

                

                                /**
                                 * Post run default scripts
                                 *
                                 **/
                // $dialog.find('input[type=file]').wl_File();
                // // setup all the fancy whitelabel options
                // $dialog.find('input[type=number].integer').wl_Number();
                // $dialog.find('input[type=number].decimal').wl_Number({
                //     decimals: 2,
                //     step: 0.5
                // });
                // $dialog.find('input.date, div.date').wl_Date();
                // $dialog.find('input.time').wl_Time();
                // $dialog.find('textarea[data-autogrow]').elastic();
                // $dialog.find('textarea.html').wl_Editor();
                // $dialog.find('input[data-regex]').wl_Valid();
                // $dialog.find('input[type=url]').wl_URL();
                // $dialog.find('input[type=email]').wl_Mail();
                // $dialog.find('input[type=password]').wl_Password();
                // $dialog.find('div.alert').wl_Alert();
                // $dialog.find('ul.breadcrumb').wl_Breadcrumb();
                // $dialog.find("textarea, input, select").not('input[type=submit], input[name*="rating-"], input[type=radio].stars, input[type=checkbox], input[type=radio].starSearch, input[name=rating], .noUniform, .noUniform *').uniform();
                // $dialog.find('div.calendar').wl_Calendar();

                if (jsloaded) {
                    eval(jsloaded);
                }
                                //doTipsy($dialog);
                                /**
                                 * open the dialog finally :/
                                 */
                $dialog.dialog({
                    width: myWidth,
                    title: myTitle,
                    resizable: false,
                                        modal: true,
                                        close: function(event, id){ //alert('fhdsjkhdi');
                                            //$(this).dialog('destroy');
                                        }
                }).dialog("open");
            }
        });
            } // end if
    }
    if($(this).attr("type")!="checkbox")
        return false;
});
    
});

function showPleaseWait($dialog, myTitle, myWidth){ //console.log('show');
    $dialog.html("<h3 style='width:350px; margin: 0 auto;'>We are currently processing your request</h3><br><center><img src='/images/loading.gif' alt='Please Wait' /></center>");
    $dialog.html("<form><fieldset><label><img src='/images/loading.gif' alt='Please Wait' width='20px' height='20px'/>&nbsp;&nbsp;Processing, Please Wait</label></fieldset></form>");
    $dialog.dialog({
        title: "Opening Form",
        resizable: false,
        modal: false,
        width: myWidth,
    }).dialog('open');
}

function closePleaseWait($dialog){ console.log('close');
    //$dialog.dialog('destroy');
    $dialog.html();
}
