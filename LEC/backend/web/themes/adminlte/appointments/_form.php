<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;
use yii\grid\GridView;
use backend\components\Helper;

use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;
?>

<div class="conference-trays-form">

	<?php $form = ActiveForm::begin(['id'=>'appointment-booking']); ?>
		<div class="booking-date-desc"></div>
		<?= $form->field($appointment, 'name')->textInput(['maxlength' => 45,'placeholder'=>'Full Name'])->label('') ?>
		<?= $form->field($appointment, 'email')->textInput(['maxlength' => 45,'placeholder'=>'Email Address'])->label('') ?>
		<?= $form->field($appointment, 'phone')->textInput(['maxlength' => 45,'placeholder'=>'Phone number'])->label('') ?>
		<?= $form->field($appointment, 'message')->textarea(['rows' => 5,'placeholder'=>'Any notes or special requests'])->label('') ?>
		<input type="hidden" name="Appointments[bookingDate]" value="" class="appo-booking-date">
		<input type="hidden" name="Appointments[bookingSlot]" value="" class="appo-booking-slot">
		<div class="form-group">
            <?= Html::submitButton('Reserve this time', ['class' => 'btn btn-success']) ?>
        </div>
	<?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function() {
    	var date = $('#w1').val();
    	var timeslot = $('.selected-timeslot').val();
    	$('.booking-date-desc').append(date+', '+timeslot);
    	$('.appo-booking-date').val(date);
    	$('.appo-booking-slot').val(timeslot);
	});
</script>    	
