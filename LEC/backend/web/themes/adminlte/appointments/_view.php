<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;
use yii\grid\GridView;
use backend\components\Helper;

use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;

?>

<div class="conference-trays-form">
	<div class="appo-details-field">
		<div class="appo-det-title">Date:</div>
		<div class="appo-det-value"><?=date('d-M-Y', strtotime($appointment->startTime))?></div>
	</div>
	<div class="appo-details-field">
		<div class="appo-det-title">Time:</div>
		<div class="appo-det-value"><?=date('h:i a', strtotime($appointment->startTime))?> to <?=date('h:i a', strtotime($appointment->endTime))?></div>
	</div>
	<div class="appo-details-field">
		<div class="appo-det-title">Name:</div>
		<div class="appo-det-value"><?=$appointment->name?></div>
	</div>
	<div class="appo-details-field">
		<div class="appo-det-title">Email:</div>
		<div class="appo-det-value"><?=$appointment->email?></div>
	</div>
	<div class="appo-details-field">
		<div class="appo-det-title">Phone Number:</div>
		<div class="appo-det-value"><?=$appointment->phone?></div>
	</div>
	<div class="appo-details-field">
		<div class="appo-det-title">Notes or Special:</div>
		<div class="appo-det-value"><?=$appointment->message?></div>
	</div>
</div>