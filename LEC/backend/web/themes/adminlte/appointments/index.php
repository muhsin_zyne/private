<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use backend\components\Helper;
use backend\assets\AppAsset;
use yii\grid\GridView;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;

$this->title = 'Manage Design Consultation Bookings';
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(isset($_GET['date']))
  $selectedDate = $_GET['date'];
else
  $selectedDate = date('d-M-Y');

?>

<div class="consumer-promotions-create">
    <div class="box">
        <div class="box-body">
            <div class="bk-head">
    		      <div class="booking-text">Booking Schedule for <span><?=$selectedDate?></span></div>
    		      <?php $form = ActiveForm::begin(['action' => ['appointments/update','date'=>$selectedDate],'options'=>['enctype'=>'multipart/form-data','class'=>'booking-fields']]); ?>
      		    <div class="booking-calendar-save">
                      <div class="booking-save">
                          <div class="form-group">
                              <?= Html::submitButton('<i class="fa fa-save"></i> Save Changes', ['class' => 'btn btn-success' ]) ?>
                          </div>
                      </div>
                      <div class="booking-calendar">
                          <i class="fa fa-calendar"></i>
                          <?=DatePicker::widget([
                              'name' => 'appointments[bookingDate]',
                              'type' => DatePicker::TYPE_INPUT,
                              'value' => $selectedDate,
                              'pluginOptions' => [
                                  'autoclose'=>true,
                                  'format' => 'dd-M-yyyy'
                              ]
                          ]);?>
                      </div>
                  </div>
                <div class="clearfix"></div>
                <div class="book-table">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                              <?php
                                if(!empty($timeSlots) && count(array_filter($timeSlots)) != 0){ 
                                  foreach ($timeSlots as $key => $slots) { 
                                    $splitSlots = explode(" - ", $slots);
                                    if(count(array_filter($splitSlots)) != 0){
                                      $startDate = new DateTime($selectedDate.$splitSlots[0]);
                                      $startTime = $startDate->format('Y-m-d H:i:s');
                              ?>
                              <td>
                                <div class="tl-book">
                                    <input type="hidden" class="slots-inputs" value="<?=$slots?>">
                                    <div class="tl-one"><?=$splitSlots[0]?></div>
                                    <div class="tl-two"><span>to</span></div>
                                    <div class="tl-one"><?=$splitSlots[1]?></div>
                                </div>
                                <div class="drop-wrap">
                                  <?php if($appointment->isAvailable($startTime)){  ?>
                                    <select class="booking-action" class="" name="appointments[slots][<?=$slots?>]">
                                      <option value="available" selected="selected">Available</option>
                                      <option value="unavailable">Unavailable</option>
                                      <option value="booked">Booked</option>
                                    </select>
                                  <?php } else { 
                                            if($id = $appointment->isBooked($startTime)){ ?>
                                                <a href="<?=\yii\helpers\Url::to(['appointments/view','id'=>$id])?>" class="ajax-update view-booking-class" data-title="Booking Details" data-width="400px">
                                                    <!-- <button class="btn btn-primary">View Booking</button> -->
                                                    <i class="fa fa-eye" title="View Booking"></i></a>
                                                    <a href="<?=\yii\helpers\Url::to(['appointments/delete','id'=>$id])?>"><i class="fa fa-trash" title="Remove Booking"></i></a>
                                                </a>
                                  <?php }
                                      else{
                                  ?>      <select class="booking-action" class="" name="appointments[slots][<?=$slots?>]">
                                            <option value="available">Available</option>
                                            <option value="unavailable"  selected="selected">Unavailable</option>
                                            <option value="booked">Booked</option>
                                          </select> 
                                    
                                  <?php } } ?>  
                                </div>
                              </td>    
                              <?php } } } else{ ?>
                                Booking is disabled for this day.
                              <?php } ?>  
                              <div style="display:none">
                                <a href="<?=\yii\helpers\Url::to(['appointments/create'])?>" class="ajax-update booking-class" data-title="Add a Booking" data-width="400px" data-complete="location.reload();">Booking</a>
                                <input type="hidden" class="selected-timeslot" value="" name="appointments[timeslot]">
                              </div> 
                            </tr>
                        </table>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.booking-action').change(function(){ 
            var timeslot = $(this).parent().parent().find('.slots-inputs').val();
            $('.selected-timeslot').val(timeslot);  
            if($(this).val() == "booked") { 
              $('.booking-class').click();
            }  
        });

        $("#w1").change(function(){
          var selecteddate = $("#w1").val();
          url = location.href.split('?')[0];
          location.href = url + "?date="+selecteddate
        });

        $('.fa-trash').click(function(){
          if (confirm("Booking details will be lost. Are you sure?")) {
            return true;
          }
          else{
            return false;
          }
          
        });
    });   
</script>         