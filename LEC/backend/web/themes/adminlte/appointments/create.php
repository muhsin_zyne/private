<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConferenceTrays */

$this->title = 'Add a booking';
$this->params['breadcrumbs'][] = ['label' => 'Manage Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conference-trays-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'appointment' => $appointment,
    ]) ?>

</div>
