<?php
use yii\helpers\Inflector;
$this->title = "Import Data - Results";
?>
<p>
    <div class="results">
    	<div class="col-md-12">
    		<div class="box box-default">
                <?php if(!empty($importResult)){ ?>
        			<div class="box-header with-border">
                      <i class="fa fa-warning"></i>
                      <h3 class="box-title">Alerts</h3>
                    </div>
                <?php } ?>
                <div class="box-body">
                	<?php //var_dump($importResult); die;
                	if(!empty($importResult)){
	                	foreach($importResult as $type => $typeResult){ ?>
		                	<div class="callout callout-danger">
			                    <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
			                    <!-- <h4><?=Inflector::titleize($type)?>!</h4> -->
			                    <?php 
                                foreach($typeResult as $result){ ?>
			                    		<?=$result?><br>
			                    <?php } ?>
		                  	</div>
	                  	<?php }
	                  	echo $this->render('_form', compact('import', 'success', 'scenario'));
                  	}else{ ?>
                  		<?=$this->render('_form', compact('import', 'success', 'scenario'));?>
                <?php }
                ?>
                </div>
    		</div>
    	</div>
    </div>
</p>