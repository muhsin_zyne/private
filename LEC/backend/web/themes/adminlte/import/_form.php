<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use common\models\Suppliers;

/* @var $this yii\web\View */

/* @var $model common\models\Brands */

/* @var $form yii\widgets\ActiveForm */

?>


<div class="box">
<div class="box-body">
<div class="import-form">
    <?php $form = ActiveForm::begin([
    	'action' => \yii\helpers\Url::to(['import/check', 'scenario' => $scenario]),
    	'options' => ['enctype'=>'multipart/form-data']
    ]); ?>
    <?= $form->field($import, 'entityType')->dropDownlist(['products' => 'Products', 'customers' => 'Customers']) ?>
    <?= $form->field($import, 'file')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Check Data', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
</div>