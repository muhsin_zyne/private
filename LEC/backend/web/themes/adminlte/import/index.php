<?php
/* @var $this yii\web\View */
$this->title = "Import Data";
$scenario = "create";
?>

<p>
    <?=$this->render('_form', compact('import', 'scenario'))?>
</p>
