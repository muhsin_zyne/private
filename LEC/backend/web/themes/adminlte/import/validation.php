<?php
use yii\helpers\Inflector;
/* @var $this yii\web\View */
$this->title = "Import Data - Results";
?>

<p>
    <?php
    	//var_dump($results);
    ?>
    <div class="results">
    	<div class="col-md-12">
    		<div class="box box-default">
    			<div class="box-header with-border">
                  <i class="fa fa-info"></i>
                  <h3 class="box-title">Information</h3>
                </div>
                <div class="box-body">
                	<?php 
                	if(!$result['status']){
                		unset($result['status']);

	                	foreach($result as $type => $typeResult){ ?>
		                	<div class="callout callout-danger">
			                    <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
			                    <h4><?=Inflector::titleize($type)?>!</h4>
			                    <?php 
                                foreach($typeResult as $result){ ?>
			                    		<?=$result?><br>
			                    <?php } ?>
		                  	</div>
	                  	<?php }
	                  	echo $this->render('_form', compact('import', 'scenario'));
                  	}else{ ?>
                  		<div class="callout callout-success">
		                    <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
		                    <h4>Success!</h4>
		                    The uploaded file has no errors and is ready to be imported.
		                </div>
		                <button type="submit" class="btn btn-primary import">Import</button>
                <?php }
                ?>
                </div>
    		</div>
    	</div>
    </div>
</p>
<script type="text/javascript">
$(document).ready(function(){
	$('.import').click(function(){
		location.href = "<?=htmlspecialchars_decode(\yii\helpers\Url::to(['import/process', 'filePath' => $filePath, 'scenario' => $scenario, 'entityType' => $import->entityType]))?>";
	})
})
</script>