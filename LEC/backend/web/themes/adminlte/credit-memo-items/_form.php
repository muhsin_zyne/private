<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CreditMemoItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="credit-memo-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'memoId')->textInput() ?>

    <?= $form->field($model, 'orderItemId')->textInput() ?>

    <?= $form->field($model, 'quantityRefunded')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
