<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CreditMemoItems */

$this->title = 'Update Credit Memo Items: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Credit Memo Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="credit-memo-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
