<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CreditMemoItems */

$this->title = 'Create Credit Memo Items';
$this->params['breadcrumbs'][] = ['label' => 'Credit Memo Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-memo-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
