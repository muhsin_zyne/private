<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CreditMemoItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Credit Memo Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-memo-items-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Credit Memo Items', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'memoId',
            'orderItemId',
            'quantityRefunded',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
