<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Shipments */

//$this->title = 'Create Shipments';
$this->title = 'New Shipment for Order#'.$model->order->orderId;
$this->params['breadcrumbs'][] = ['label' => 'Shipments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipments-create">

    
    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider'=>$dataProvider,
    ]) ?>

</div>
