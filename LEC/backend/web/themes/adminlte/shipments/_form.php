<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;

use frontend\components\Helper;

use yii\widgets\ListView;

use yii\helpers\url;

use yii\grid\GridView;

use common\models\SalesComments;

use common\models\Shipments;

use common\models\OrderItems;

use yii\helpers\ArrayHelper;


?>

        <div class="shipments-form">
          <div class="row">
            <div class="col-xs-12">
              <div style="float:right; margin-bottom:15px;">
                <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/view' ,'id'=>$model->order->id], ['class' => 'btn btn-default']) ?>
              </div>
            </div>
          </div>
          <div class="box">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
                    <h3 class="box-title">Order #
                      <?= $model->order->orderId ?>
                      (Order confirmation email was sent)</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <p>Order Date :
                      <?= $model->order->orderPlacedDate ?>
                    </p>
                    <p>Order Status :
                      <?= ucfirst($model->order->status) ?>
                    </p>
                    <p>Purchased From :
                      <?= $model->order->storeName ?>
                    </p>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-user"></i>
                    <h3 class="box-title">Account Information</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <p> Customer Name :
                      <?= ucfirst($model->order->customer->firstname).' '.ucfirst($model->order->customer->lastname) ?>
                    </p>
                    <p> Email :
                      <?= $model->order->customer->email ?>
                    </p>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-home"></i>
                    <h3 class="box-title">Billing Address</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <p>
                      <?= $model->order->billingAddressText ?>
                    </p>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-building-o"></i>
                    <h3 class="box-title">Shipping Address</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <p>
                      <?= $model->order->shippingAddressText ?>
                    </p>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-usd"></i>
                    <h3 class="box-title">Payment Information</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <?= $model->order->paymentDetails?>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-truck"></i>
                    <h3 class="box-title">Shipping & Handling Information</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <?php
        
                            if(isset($model->order->shippingMethodId)){ ?>
                    <?= $model->order->shippingMethod->title ?>
                    | Total Shipping Charges:
                    <?= Helper::money($model->order->shippingMethod->normalPrice) ?>
                    <br/>
                    <?php }
        
                            else
        
                            {?>
                    N/A | Total Shipping Charges:
                    <?= Helper::money(0) ?>
                    <br/>
                    <?php }  ?>
                    <br/>
                    <div class="shipping-form" id="shipping-form">
                      <?php $form = ActiveForm::begin(); ?>
                      <div class="row">
                        <?php $Carriers=common\models\Carriers::find()->all(); 
                        $listData=ArrayHelper::map($Carriers,'id','title'); ?>
                        <div class="col-md-6">
                          <?= $form->field($model, 'carrier')->dropDownList($listData, ['prompt'=>'Select Carrier...']); ?>
                        </div>
                        <div class="col-md-6">
                          <?= $form->field($model, 'title')->textInput() ?>
                        </div>
                      </div>
                      <?= $form->field($model, 'trackingNumber')->textarea() ?>
                    </div>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
                    <h3 class="box-title">Items to Ship</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <?= GridView::widget([
        
                            'id' => 'order-items',
        
                            'dataProvider' => $dataProvider,
        
                            //'filterModel' => $searchModel,
        
                            'columns' => [
        
                                ['class' => 'yii\grid\SerialColumn'],
        
                                
        
                                'id',
        
                                [
        
                                    'label'=> 'Product Name',
        
                                    'attribute' => 'orderId',
        
                                    'value' => 'product.name'
        
                                ],
        
                                [
        
                                    'label'=> 'SKU',
        
                                    'attribute' => 'productId',
        
                                    'value' => 'sku'
        
                                ],
        
                                
        
                                [
        
                                    'label' => 'Qty',
        
                                    'attribute' => 'id',
        
                                    'format' => 'html',
        
                                    'value' => function ($model) {
        
                                        return $model->qtyStatus;
        
                                    },
        
                                ],
        
                                [
        
                                    'label' => 'Qty to Ship',
        
                                    'format' => 'raw',
        
                                    'value' => function ($model) { //var_dump(ArrayHelper::map($enabledProducts, 'id', 'productId'));die();
        
                                         return '<input type="text" class="qts form-control" value="'.$model->qtyToShip.'" id="'.$model->id.'"  >
        
                                         <input type="hidden" id="qts'.$model->id.'" name="ShipmentItems[orderItemId][]" value="'.$model->id.','.$model->qtyToShip.'">' ;
        
                                     },
        
                                     
        
                                ],
        
                            ],
        
        
        
                        ]); ?>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-header with-border"><i class="fa fa-comments-o"></i>
                    <h3 class="box-title">Shipment Comments</h3>
                  </div>
                  <!-- /.box-header -->
                  
                  <div class="box-body">
                    <div class="order-comment-form">
                      <?= $form->field($model, 'comments')->textarea(['rows' => 4]) ?>
                    </div>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
              <div class="col-md-6">
                <div class="box box-default">
                  <div class="box-body" >
                    <p class="ship-check"><input type="checkBox" class="enabled_checkbox" id="shipments-chkmail" name="Shipments[chkmail]" checked>Email Copy of Shipment</p>
                    <div class="form-group">
                      <?= Html::submitButton('Submit shipment', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                  </div>
                  <!-- /.box-body --> 
                  
                </div>
                <!-- /.box --> 
                
              </div>
            </div>
          </div>
        </div>
        </div>
<script>

$(document).ready(function(){

    $(".qts").change(function(){  

        var text = $( this ).val();

        var oi_id=$(this).attr('id');

        $('#qts'+oi_id).val(oi_id+','+text);

        //alert($('#qts'+oi_id).val());

    }); 
    $('#shipments-carrier').on('change',function(){
       
        $.ajax({
            type: "GET",
            url: "<?=Yii::$app->urlManager->createUrl(['shipments/carrier'])?>",
            data: {id: $(this).val()},
            dataType: "html", 
            success: function (data) {
                //alert(data);  
                $('#shipments-trackingnumber').val(data);              
            }                
        });
    });

});

</script>