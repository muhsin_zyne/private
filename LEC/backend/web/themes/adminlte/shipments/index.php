<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ShipmentItems;
use frontend\components\Helper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ShipmentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shipments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">
<div class="shipments-index">
    <?php \yii\widgets\Pjax::begin(['id' => 'shipments-index',]); ?>

    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
           [
                'label' => 'INVOICE DATE',
                'attribute' => 'createdDate',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::date($model->createdDate);
                },
            ],
            //'createdDate',
            'orderId',
            
            //'orderDate',
            //'orderShipToAddress',
            
            [
                'label' => 'Order Date',
                'attribute' => 'orderDate',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::date($model->order->orderDate);
                },
            ],
            [
                'label'=> 'Ship to Address',
                'format' => 'html',
                'attribute' => 'orderShipToAddress',
                'value' => 'order.shippingAddressText'
            ],
            
            'orderItemCount',
                     
           [
                'label'=> 'View',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('View',['view','id' => $model->id]);
                }
            ],
            
        ],
    ]); ?>
    <?php  \yii\widgets\Pjax::end();  ?> 

     <?php
    $this->registerJs("
    $('td').click(function (e) {
        var id = $(this).closest('tr').data('key');
        if(e.target == this)
            location.href = '" . Url::to(['shipments/view']) . "?id=' + id;
        });
    ");
?>

</div>
</div>
</div>
