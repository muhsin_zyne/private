<?php



use yii\helpers\Html;

use yii\widgets\DetailView;

use yii\grid\GridView;

use frontend\components\Helper;

use yii\widgets\ActiveForm;

use yii\widgets\ListView;

use yii\helpers\url;

use common\models\SalesComments;

use common\models\Shipments;

use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */

/* @var $model common\models\Shipments */

if($model->notify==1)

{

    $message='(the shipment email was sent)';

}

else

{

    $message='(the shipment email was not sent)';

}

$this->title = 'Shipment #'.$model->shipmentId.'|'.$model->shipmentplacedDate.$message;



?>

    <div class="row">
      <div class="col-md-12">
      <div style="float:right; margin-bottom:15px;">
        <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/view' ,'id'=>$model->order->id], ['class' => 'btn btn-default ']) ?>
        <?= Html::a('<i class="fa fa-envelope"></i> Send Email', ['sendmail','id'=>$model->id],['class' => 'btn btn-primary hold',
        'data' => ['confirm' => 'Are you sure you want to send shipment email to customer?','method' => 'post',]]) ?>
      </div>
      </div>
    </div>
    <div class="box box-default">
    <div class="box-body">
    <div class="shipments-view">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
              <h3 class="box-title">Order #
                <?= $model->order->orderId ?>
                (Order confirmation email was sent)</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <p>Order Date :
                <?= $model->order->orderPlacedDate ?>
              </p>
              <p>Order Status :
                <?= ucfirst($model->order->status) ?>
              </p>
              <p>Purchased From :
                <?= $model->order->storeName ?>
              </p>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-user"></i>
              <h3 class="box-title">Account Information</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <p> Customer Name :
                <?= ucfirst($model->order->customer->firstname).' '.ucfirst($model->order->customer->lastname) ?>
              </p>
              <p> Email :
                <?= $model->order->customer->email ?>
              </p>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-home"></i>
              <h3 class="box-title">Billing Address</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <p>
                <?= $model->order->billingAddressText ?>
              </p>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-building-o"></i>
              <h3 class="box-title">Shipping Address</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <p>
                <?= $model->order->shippingAddressText ?>
              </p>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-usd"></i>
              <h3 class="box-title">Payment Information</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <?= $model->order->paymentDetails?>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-truck"></i>
              <h3 class="box-title">Shipping and Tracking Information</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body"> <a href='<?=$model->trackingNumber?>' target="_blank">Track this shipment</a><br/>
              <?php
    
                        if(isset($model->order->shippingMethodId)){ ?>
              <?= $model->order->shippingMethod->title ?>
              | Total Shipping Charges:
              <?= Helper::money($model->order->shippingMethod->normalPrice) ?>
              <br/>
              <?php }
    
                        else
    
                        {?>
              N/A | Total Shipping Charges:
              <?= Helper::money(0) ?>
              <br/>
              <?php }  ?>
              <br/>
              <div class="shipping-form">
                <?php $form = ActiveForm::begin(['action' =>  Url::to(['shipments/update','id' => $model->id])]); 
    
                                    $model_sc= new Shipments(); 
    
                                    //$model_oc->orderId=$model->id;  ?>
                <?php $Carriers=common\models\Carriers::find()->all(); 
                $listData=ArrayHelper::map($Carriers,'id','title'); ?>                   
                <?= $form->field($model, 'carrier')->dropDownList($listData, ['prompt'=>'Select Carrier...']); ?>
                <?= $form->field($model, 'title')->textInput() ?>
                <?= $form->field($model, 'trackingNumber')->textarea() ?>
                <div class="form-group">
                  <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
              </div>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
              <h3 class="box-title">Items Shipped</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <?= GridView::widget([
    
                        'dataProvider' => $dataProvider,
    
                        //'filterModel' => $searchModel,
    
                        'columns' => [
    
                        ['class' => 'yii\grid\SerialColumn'],
    
    
    
                        //'id',
    
                        [
    
                            'label'=> 'Product Name',
    
                            'attribute' => 'orderId',
    
                            'value' => 'orderItem.product.name'
    
                        ],
    
                        [
    
                            'label'=> 'SKU',
    
                            'attribute' => 'productId',
    
                            'value' => 'orderItem.sku'
    
                        ],
    
                        'quantityShipped',
    
                        ],
    
                    ]); ?>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-comments-o"></i>
              <h3 class="box-title">Shipment Comments History</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <div class="order-comment-form">
                <?php $form = ActiveForm::begin(['action' =>  Url::to(['sales-comments/create'])]); 
    
                                $model_sc= new SalesComments(); 
    
                                $model_sc->typeId=$model->id; $model_sc->type='shipping';$model_sc->orderId=$model->order->id; ?>
                <?= $form->field($model_sc, 'type')->hiddenInput()->label(false)  ; ?>
                <?= $form->field($model_sc, 'typeId')->hiddenInput()->label(false)  ; ?>
                <?= $form->field($model_sc, 'orderId')->hiddenInput()->label(false)  ; ?>
                <?= $form->field($model_sc, 'comment')->textarea(['rows' => 4])-> label('Add Shipment Comments'); ?>
                <?= $form->field($model_sc, 'notify')->checkbox(); ?>
                <div class="form-group">
                  <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <?= ListView::widget([
    
                                'dataProvider' => $dataProvider_salescomments,
    
                                //'filterModel' => $searchModel_ordercomment,
    
                                'itemView' => '_listview',
    
                            ]); ?>
              </div>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
        </div>
      </div>
    </div>
    
