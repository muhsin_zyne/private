<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\tree\TreeViewInput;
use common\models\Categories;
use common\models\ProductCategories;
//use kartik\tree\models\Tree;
//use kartik\tree\controllers\NodeController;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'method' => 'post',
    'action' => ['site/save'],
    //'action' => '',
    'id' => 'pro1'
]);

?>

<?php

  $catIds=explode(',',$cats);

  if(!is_null($catIds)){ ?>

       <input type="hidden"  value='<?=$cats?>' name="kv-product" class="form-control hide" id="w0">

<?php  }

?>
    
<?php  echo \kartik\tree\TreeViewInput::widget([
    // single query fetch to render the tree
    'query' => \common\models\Categories::find()->addOrderBy('root, lft'),
    'headingOptions'=>['label'=>'Categories'],
    'name' => 'kv-product', // input name
   // 'value' => '1,2,3', // values selected (comma separated for multiple select)
    'asDropdown' => false, // will render the tree input widget as a dropdown.
    'multiple' => true, // set to false if you do not need multiple selection
    'fontAwesome' => true, // render font awesome icons
    'rootOptions' => [
    'label'=>'<i class="fa fa-tree"></i>', // custom root label
    'class'=>'text-success'
    ],
    //'options'=>['disabled' => true],
    ]);

?> 

<?php

  $catIds=explode(',',$cats);

  if(!is_null($catIds)){ 


        foreach ($catIds as $value) {
          

         // var_dump($value);die();
       

    ?>

       <input type="hidden"  value='<?=$cats?>' name="kv-product" class="form-control hide" id="w0">

       <script type="text/javascript">


        $('.kv-node-key:contains(<?php echo json_encode($catIds); ?>)').parent().parent().parent().addClass("kv-selected");

            

       </script>

<?php  } }

?>






 <!-- <input type="hidden" name="Products[Categories][]" id="ids" value="">   -->

<button class="btn btn-primary" id="#submitfn" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>   

<?php ActiveForm::end() ?>

<script type="text/javascript">

$(document).ready(function () {
    
$(".fa").click(function(){

    setTimeout(function() {

    var favorite = [];

    var key=$(".kv-selected .kv-node-label").text();

    $.each($(".kv-selected .kv-node-key"), function(){            
                favorite.push($(this).text());
              //  console.log(favorite);
            });

   
    
}, 1000);

});

});

</script>
  