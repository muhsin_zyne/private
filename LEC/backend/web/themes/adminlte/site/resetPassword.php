<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-box" id="login-box">
    <div class="header">Reset Password</div>
    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
    <div class="body bg-gray">
        <p>Please choose your new password:</p>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn bg-olive btn-block']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
