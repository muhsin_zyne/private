<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
<style type="text/css">

*{ margin:0; padding:0; box-sizing:border-box; font-family: 'Open Sans', sans-serif;}
  .container{ width:1000px; margin:0 auto;}
  .yike-wrapper{ width:100%; float:left; border:1px solid #ededed; padding:80px 58px;}
  .yike-right{ width:25%; float:right; text-align:center;}
  .yike-right img{ max-width:75%;}
  .yike-left{ width:75%; float:left; color:#7c7c7c; font-size:15px; line-height:30px; padding-right:50px;}
  .yike-left span{ width:100%; float:left; font-weight:600; color:#565656; font-size:31px; margin-bottom:20px;}
  .yike-left a{ color:#565656; text-decoration:none;}
  .yike-left a:hover{ text-decoration: underline;}


/*  *{ margin:0; padding:0; box-sizing:border-box; font-family: 'Open Sans', sans-serif;}
  .container{ width:100%;}
  .yike-wrapper{ width:100%; float:left; border:1px solid #ededed; padding:50px 25px; text-align:center;}
  .yike-right{ width:100%; float:right;}
  .yike-right img{ max-width:75%;}
  .yike-left{ width:100%; float:left; color:#7c7c7c; font-size:13px; line-height:30px; padding-top:20px;}
  .yike-left span{ width:100%; float:left; font-weight:600; color:#565656; font-size:31px; margin-bottom:20px;}
  .yike-left a{ color:#565656; text-decoration:none;}
  .yike-left a:hover{ text-decoration: underline;}
*/
</style>
<div class="box">
<div class="box-body">
<div class="container">
	<div class="yike-wrapper">
  <?php if($exception->statusCode == 404){ ?>
      <div class="yike-right">
        <img src="/images/icon-error.png" alt=""/>
      </div>
      <div class="yike-left">
        <span>Oops!</span>

Looks like you have hit a 404! You are looking for a page/URL that does not exist. The page could have expired or you are here by mistake. Kindly go to the home page and continue browsing.</a>
     </div>
  <?php }else{ ?>
              <div class="yike-right">
              <img src="/images/tool.jpg" alt=""/>
            </div>
            <div class="yike-left">
                    <span>Yikes!!</span>
              <?php
                //var_dump($exception); die;
              ?>
                Looks like something totally went wrong! If you email us the link or a brief of what happened, we will beat the webmaster and get it resolved. <br>
                Email: <a href="mailto:admin@leadingedgegroup.com.au">admin@leadingedgegroup.com.au</a>
            </div>
  <?php } ?>
	</div>
</div>
</div>
</div>
