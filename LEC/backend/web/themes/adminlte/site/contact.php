
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contact Support Team';
$this->params['breadcrumbs'][] = $this->title;
$listData = ['Customise My Website' => 'Customise My Website', 
'Request a New Feature' => 'Request a New Feature',
'Upload New Products' => 'Upload New Products',
'Edit Existing Products'=> 'Edit Existing Products',
'Correct Product Photos' => 'Correct Product Photos',
'Report a Bug' => 'Report a Bug',
'Other'=> 'Other'];
?>
<div class="box">
    <div class="box-body">
        <div class="orders-index sm-tb">
            <div class="contact_area">
                <h1><?= Html::encode($this->title) ?></h1>
                <div class="site-contact">
                    <p>
                        If you have any queries or customisation requests, please contact the development team using the following form. We will get back to you as soon as possible.
                    </p>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $form = ActiveForm::begin(['id' => 'contact-form','options'=>['enctype' => 'multipart/form-data']]); ?>
                            <?= $form->field($model, 'name') ?>
                            <?= $form->field($model, 'subject')->dropDownList($listData, ['prompt'=>'Please select'])?>
                            <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                            <?php //$form->field($model, 'uploadFile')->fileInput() ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?= $form->field($model, 'uploadFile[]')->fileInput(['multiple' => true]) ?>
                                </div> 
                                <div class="col-sm-6">
                                    <img id="loader" src="/images/loading.gif" style="display:none;"/>
                                </div> 
                            </div>    
                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                                <span style="font-size: 14px;font-style: italic;display: block;float: right;"> If you are looking for bulk product upload, here is a <a href="http://root.lec.inte.com.au/Simple-Product-Data.xlsx" target="_blank"> <b>sample CSV</b></a> for you to fill and send.</span> 
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs("$('form#contact-form').on('beforeSubmit', function(e) {
       $('#loader').show(); 
       $(':submit').attr('disabled', true);
    });");
?>



