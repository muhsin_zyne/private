<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Request Password Reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-box" id="login-box">
    <div class="header">Reset Password</div>
    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
            <div class="body bg-gray">
            <p>Please fill out your email. A link to reset password will be sent there.</p>
                <?= $form->field($model, 'email') ?>
                </div>
                <div class="footer">
                    <?= Html::submitButton('Send', ['class' => 'btn bg-olive btn-block']) ?>
                </div>
            
            <?php ActiveForm::end(); ?>

</div>