<?php
/* @var $this yii\web\View */

$this->title = 'Dashboard';
use yii\helpers\Url;
use common\models\User;

$user = User::findOne(Yii::$app->user->id);
?>
<div class="site-index">
    
    <!--main-page-bg-->
    <div class="main-page-bg">
    <div class="row">
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color1">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <p>Sales</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["orders/index"])?>">Orders</a></li>
                        <li><a href="<?=Url::to(["invoices/index"])?>">Invoices</a></li>
                       <!--  <li><a href="<?=Url::to(["shipmets/index"])?>">Shipments</a></li> -->
                        <li><a href="<?=Url::to(["credit-memos/index"])?>">Credit Memos</a></li>
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color2">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                        <p>Catalogue</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["products/index"])?>">Manage Products</a></li>
                        <?php if($user->roleId == "1") {  ?>
                            <li><a href="<?=Url::to(["categories/index"])?>">Manage Categories</a></li>
                            <li><a href="<?=Url::to(["attributes/index"])?>">Attributes</a></li>
                        <?php } ?>  
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color3">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <p>Site Content</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["cms/index"])?>">CMS Pages</a></li>
                        <li><a href="<?=Url::to(["cms-blocks/index"])?>">CMS Blocks</a></li>
                        <li><a href="<?=Url::to(["category-blocks/index"])?>">Category Blocks</a></li>
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->

        <?php if($user->roleId == "1") {  ?>

            <div class="col-xs-4">
            <div class="box-bg no-hover bg-color4">
                <div class="box-inner">
                    <div class="icon-bg">
                        <a href="<?=Url::to(["suppliers/index"])?>">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <p>Suppliers</p>
                        </a>    
                    </div>
                    <!--//icon-bg-->
                    <!-- <ul>
                        <li class="active"><a href="">Customers</a></li>
                        <li><a href="">Subscribers</a></li>
                    </ul> -->
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg no-hover bg-color5">
                <div class="box-inner">
                    <div class="icon-bg">
                        <a href="<?=Url::to(["brands/index"])?>">
                            <i class="fa fa-cubes" aria-hidden="true"></i>
                            <p>Brands</p>
                        </a>
                    </div>
                    <!--//icon-bg-->
                    <!-- <ul>
                        <li class="active"><a href="">Orders</a></li>
                        <li><a href="">Invoices</a></li>
                        <li><a href="">Shipments</a></li>
                        <li><a href="">Credit Memos</a></li>
                    </ul> -->
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->

        <?php } else { ?>   

        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color4">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <p>Users</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["users/index"])?>">Customers</a></li>
                        <li><a href="https://talkbox.impactapp.com.au/" target="_blank">Subscribers</a></li>
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg  no-hover bg-color5">
                <div class="box-inner">
                    <div class="icon-bg">
                        <a href="<?=Url::to(["brands/index"])?>">
                            <i class="fa fa-cubes" aria-hidden="true"></i>
                            <p>Brands</p>
                        </a> 
                    </div>
                    <!--//icon-bg-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        
        <?php } ?>  
        
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color6">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        <p>Reports</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["reports/orders"])?>">Orders</a></li>
                       <!--  <li><a href="<?=Url::to(["reports/shipped"])?>">Shipping</a></li> -->
                        <li><a href="<?=Url::to(["reports/refunds"])?>">Refunds</a></li>
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color7">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        <p>Banners</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["banners/index",'id'=>'b2c','type'=>'b2c-main'])?>">Main Banners</a></li>
                        <li><a href="<?=Url::to(["banners/index",'id'=>'b2c','type'=>'b2c-mini'])?>">Mini Banners</a></li>
                        <li><a href="<?=Url::to(["banners/index",'id'=>'b2c','type'=>'b2c-side'])?>">Side Banners</a></li>
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->

        <?php if($user->roleId == "1") {  ?>

        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color8">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-list-alt" aria-hidden="true"></i>
                        <p>B2B Management</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["b2b/index"])?>">B2B Dashboard</a></li>
                        <li><a href="<?=Url::to(["b2b/orders"])?>">B2B Orders</a></li>
                        <li><a href="<?=Url::to(["banners/index",'id'=>'b2b'])?>">B2B Banners</a></li>
                        <li><a href="<?=Url::to(["consumer-promotions/index"])?>">Consumer Promotions</a></li>
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->

        <?php } else { ?>   
        
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color8">
                <div class="box-inner">
                    <div class="icon-bg">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <p>BYOD</p>
                        </a>    
                    </div>
                   <ul>
                        <li class="active"><a href="<?=Url::to(["byod/index"])?>">Manage BYOD</a></li>
                        <li><a href="<?=Url::to(["byod-orders/index"])?>">BYOD Orders</a></li>
                    </ul> 
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        
        <?php } ?>
        
        <!--col-xs-4-->
        <div class="col-xs-4">
            <div class="box-bg bg-color9">
                <div class="box-inner">
                    <div class="icon-bg">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <p>Settings</p>
                    </div>
                    <!--//icon-bg-->
                    <ul>
                        <li class="active"><a href="<?=Url::to(["configuration/index"])?>">Configurations</a></li>
                        <?php if($user->roleId == "1") {  ?>
                        <li><a href="<?=Url::to(["stores/manage"])?>">Store Management</a></li> 
                        <?php } ?>  
                    </ul>
                    <!--//ul-->
                </div>
                <!--//box-inner-->
            </div>
        </div>
        <!--//col-xs-4-->
        </div>
    </div>
    <!--//main-page-bg-->
    
</div>
<script>
    $('.box-inner li').click(function(e) {
        //e.preventDefault(); //prevent the link from being followed
        $('.box-inner li').removeClass('active');
        $(this).addClass('active');
    });
</script>
