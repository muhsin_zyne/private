<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\AttributeGroups;
use common\models\GroupAttributes;
use common\models\Attributes;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Attributes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row" id="1">
  <div class="col-md-12" id="buttons">
  <div class="buttons-smrow">
  <button id="create_button" class="btn btn-success">
  <i class="fa fa-plus"></i> Create Group
  </input>
  <button id="delete_button" class="btn btn-danger">
  <i class="fa fa-trash"></i> Delete Group
  </input>
  <button id="update_button" class="btn btn-primary">
  <i class="fa fa-save"></i> Save Changes
  </input>
  </div>
  </div>
  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Attribute Set Details</h3>
      </div>
      <?php $form = ActiveForm::begin(); ?>
      <div class="box-body">
        <?= $form->field($model, 'title')->textInput(['maxlength' => 45, 'required' => true]) ?>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
    <!-- /.box --> 
  </div>
  <div class="col-md-4" id="attr">
  <div class="box box-primary">
  <div class="box-body">
  <h3 class="box-title">Attributes</h3>
  <div class="dd" id="nestable">
  <ol class="dd-list">
  <?php
                    $attributeSetId_r=$_REQUEST['id'];
                    $atr_group=AttributeGroups::find()->where(['attributeSetId' => $attributeSetId_r])->orderBy('position')->all();
                    foreach ($atr_group as $index => $file) 
                    {
                    ?>
  <li class="dd-item" data-id="<?= $file['id'] ?>">
    <div class="dd-handle">
  <input type="checkbox" id="ch_<?= $file['id'] ?>" >
  <b>
  <?= $file['title'] ?>
  </b></div>
<ol class="dd-list">
  <?php
                            $attribute_group_id=$file['id'];
                            //$attribute_group_id=1;
                            $atr_group_id=GroupAttributes::find()->where(['attributeGroupId' => $attribute_group_id])->orderBy('position')->all();
                            foreach ($atr_group_id as $index => $glist) 
                            {
                                $att_final = Attributes::find()->where(['id' => $glist['attributeId']])->one();
                                if(isset($att_final['id']))
                                { ?>
  <li class="dd-item" data-id="<?= $glist['attributeId']?>">
    <div class="dd-handle">
      <?php
                                        echo $att_final['title']; 
                                    ?>
    </div>
  </li>
  <?php } ?>
  <?php } ?>
</ol>
</li>
<?php } ?>
</ol>
</div>
</div>
</div>
</div>
<div class="col-md-4">
  <div class="box box-primary">
    <div class="box-body">
      <h3 class="box-title">Unassigned Attributes</h3>
      <div class="dd" id="nestable2">
        <ol class="dd-list">
          <?php
                    $arr1[]=0;
                    $arr2[]=0;
                    $all_assigned_attributes_main = Attributes::find()->all();
                    $i=0;
                    foreach ($all_assigned_attributes_main as $index => $assigned_attribute) 
                    {
                        $arr1[$i]= $assigned_attribute['id'];
                        $i++;
                    }
                    $j=0;
                    $atr_group=AttributeGroups::find()->where(['attributeSetId' => $attributeSetId_r])->all();
                    foreach ($atr_group as $index => $glist) 
                    {                        
                        $assigned_group_ataributes= GroupAttributes::find()->where(['attributeGroupId' => $glist['id']])->select('attributeId')->distinct()->all();
                        foreach ($assigned_group_ataributes as $index => $file2) 
                        {
                            $arr2[$j]= $file2['attributeId'];
                            $j++;
                        }
                    }
                    //var_dump($arr2);exit;
                    $unassigned_attributes = array_diff($arr1, $arr2);//var_dump($unassigned_attributes);exit;
                    if(empty($unassigned_attributes))
                    {?>
          <div class="dd-empty"></div>
          <?php }
                    foreach ($unassigned_attributes as $index => $attribute_id) 
                    {
                    ?>
          <li class="dd-item" data-id="<?= $attribute_id ?>">
            <div class="dd-handle">
              <?php
                                //$atri_id=$attribute_id; 
                                $att_final_unassigned = Attributes::find()->where(['id' => $attribute_id])->one();
                                echo $att_final_unassigned['title'];//echo '    '.$atri_id;
                                ?>
            </div>
          </li>
          <?php
                    }
                    ?>
        </ol>
        <textarea id="nestable-output" style="display:none;"></textarea>
        <textarea id="nestable2-output" style="display:none;"></textarea>
      </div>
    </div>
  </div>
  <!-- /.box --> 
</div>
</div>
<link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/adminlte/css/main_nestedSortable.css" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/adminlte/js/AdminLTE/jquery.nestable.attr.js"></script> 
<script>

$(document).ready(function()
{   
    //-------nestable-------------
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {

            var a1=output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));

        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // activate Nestable for list 2
    $('#nestable2').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

    updateOutput($('#nestable2').data('output', $('#nestable2-output')));
    //----update attribute-sets-----
    $("#update_button").click(function(){
        var order1= document.getElementById("nestable-output").value;
        var order2= document.getElementById("nestable2-output").value;
        var atr_set_id=<?= $attributeSetId_r ?>;
        $.ajax({
            type: "POST",
            url: "<?=Yii::$app->urlManager->createUrl(['attribute-sets/updategroupattributes'])?>",                
            data: "&order1=" + order1 + "&order2=" + order2 + "&atr_set_id=" + atr_set_id ,
            dataType: "html", 
            success: function (data) {
                alert(data);
                //alert("hai");
            }        
        });
    });
    //----create new  attribute-sets-------
    $("#create_button").click(function(){
        var atr_set_id=<?= $attributeSetId_r ?>;
        //alert(atr_set_id);exit;
        var person = prompt("Please Enter Attribute Groups Name", "");
        if (person != null) {
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['attribute-sets/createattributegroup'])?>",
                data: "&person=" + person +"&atr_set_id=" + atr_set_id ,
                //dataType: "json",
                dataType: "html", 
                success: function (data) {
                //alert(data);
                //$("#attr").html(data);//Update Attribute Sets: zcZCc
                }                
            });
        }
        
    }); 
    //-------------delete  attribute-sets----------
    $("#delete_button").click(function(){
        <?php 
        $attributeSetId_r=$_REQUEST['id'];
        $atr_group=AttributeGroups::find()->where(['attributeSetId' => $attributeSetId_r])->all();
        foreach ($atr_group as $index => $file) 
        {?>  
     
        
            check = $("#ch_<?= $file['id'] ?>").prop("checked");
            //alert($("#ch_<?= $file['id'] ?>").val());
            if(check) 
                {
                    var atr_group_id=<?= $file['id'] ?>;
                    var atr_set_id=<?= $attributeSetId_r ?>;
                    //alert(atr_group_id);alert(atr_set_id);
                    //alert("Checkbox is checked.");
                $.ajax({
                    type: "POST",                
                    url: "<?=Yii::$app->urlManager->createUrl(['attribute-sets/deleteattributeset'])?>",                
                    data: "&atr_set_id=" + atr_set_id + "&atr_group_id=" + atr_group_id ,                   
                    dataType: "html", 
                    success: function (data) {
                     //alert(data);                    
                    }
                });

                } 
        <?php } ?>       
    });
});
</script> 
