<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AttributeSets */

$this->title = 'Update Attribute Sets: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Attribute Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="attribute-sets-update">
    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>
</div>
