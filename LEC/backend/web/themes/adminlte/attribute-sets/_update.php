<?php

use yii\helpers\Html;


?>
<div class="attribute-sets-form">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>