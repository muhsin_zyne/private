<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AttributeSets;


/* @var $this yii\web\View */
/* @var $model common\models\AttributeSets */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attribute-sets-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 45]) ?>
    <?php
    $sets=AttributeSets::find()->all();

    $listData=ArrayHelper::map($sets,'id','title');
    echo $form->field($model, 'basedOnSet')->dropDownList($listData, ['prompt'=>'Select...']);
	?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
