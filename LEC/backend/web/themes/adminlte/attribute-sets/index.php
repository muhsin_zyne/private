<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attribute Sets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-sets-index">

    <div class="box">
    <div class="box-body">

    <p class="manage_attri">
        <?= Html::a('<i class="fa fa-plus"></i>Create', ['create'], ['class' => 'btn btn-success ajax-update', 'data-update' => 'attribute-sets','data-title' => 'Create Attribute Set']) ?>
    </p>
<?php \yii\widgets\Pjax::begin(['id' => 'attribute-sets']); ?>
    <?= \app\components\AdminGridView::widget([
        'id' => 'attribute-sets',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'basedOnSet',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                'update' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                },
                'delete' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Delete" data-pjax="0"><span class="fa fa-trash-o"></span></a>' ;
                },
                ]
            ],
        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
</div>
</div>
