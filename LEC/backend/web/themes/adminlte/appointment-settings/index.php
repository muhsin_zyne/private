<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use backend\components\Helper;
use backend\assets\AppAsset;
use yii\grid\GridView;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use common\models\User;
use common\models\Stores;
use common\models\Appointments;
use common\models\AppointmentDefaultSettings;


$this->title = 'Default Settings';
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="consumer-promotions-create">
	<div class="box">
		<div class="box-body">
        	<div class="bk-head">
        		<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','class'=>'default-setting']]); ?>
        		<div class="booking-calendar-save">
        			<div class="booking-save">
						<div class="form-group">
							<?= Html::submitButton('<i class="fa fa-save"></i> Save Changes', ['class' => 'btn btn-success' ]) ?>
						</div>
					</div>
					<!-- <div class="booking-calendar">Action
	        			<select name="settings[action]" class="default-booking-status">
	        				<option value="available" selected="selected">Available</option>
	        				<option value="unavailable">Unavailable</option>
	        			</select>
        			</div> -->	
				</div>
				<div class="clearfix"></div>
				<span style="color:#b8c7ce;font-size:14px; margin-bottom:5px; font-style:italic;display:inline-block;">You can manage your default availability for design consultations here.</span>
                <div class="clearfix"></div>	
        		<div class="box box-solid box-default">
		            <div class="box-header">
		              	<div class="pull-right box-tools">
		                	<button type="button" class="btn btn-box-tool appo-default-btn" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
		                  <i class="fa fa-minus"></i></button>
		              	</div>
		             	<h3 class="box-title">Sunday</h3>
		            </div>
		           	<div class="box-body">
		            	<?php foreach ($timeSlots as $key => $slot) { 
		            		if(isset($defaultSetting->sun)){
		            			$sundaySlots =  json_decode($defaultSetting->sun,true);
		            		}
		            		else{
		            			$sundaySlots = $defaultSetting['sun'];
		            		}
		            	?>
		            		<div class="timing-check-div">
		            			<input type="hidden" name="settings[timeslots][sun][<?=$key?>]">
		            			<input type="checkbox" name="settings[timeslots][sun][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $sundaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
							</div>
		            	<?php } ?>
		            		</div>
            	</div>
            	<div class="box box-default box-solid collapsed-box">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Monday</h3>
						<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
		              	</div>
		            </div>
		            <div class="box-body">
		              	<?php foreach ($timeSlots as $key => $slot) {  
							if(isset($defaultSetting->mon)){
		            			$mondaySlots =  json_decode($defaultSetting->mon,true);
		            		}
		            		else{
		            			$mondaySlots = $defaultSetting['mon'];
		            		}
		              	?>
		            		<div class="timing-check-div">
		            			<input type="hidden" name="settings[timeslots][mon][<?=$key?>]">
		            			<input type="checkbox" name="settings[timeslots][mon][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $mondaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
		            			</div>
		            	<?php } ?>
		            </div>
		        </div>

		        <div class="box box-default box-solid collapsed-box">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Tuesday</h3>
						<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
		              	</div>
		            </div>
		            <div class="box-body">
		              	<?php foreach ($timeSlots as $key => $slot) {  
		              		if(isset($defaultSetting->tue)){
		            			$tuesdaySlots =  json_decode($defaultSetting->tue,true);
		            		}
		            		else{
		            			$tuesdaySlots = $defaultSetting['tue'];
		            		}
		              	?>
		              		<div class="timing-check-div">
		              			<input type="hidden" name="settings[timeslots][tue][<?=$key?>]">
		              			<input type="checkbox" name="settings[timeslots][tue][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $tuesdaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
		              		</div>
		            	<?php } ?>
		            </div>
		        </div>

		        <div class="box box-default box-solid collapsed-box">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Wednesday</h3>
						<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
		              	</div>
		            </div>
		            <div class="box-body">
		              <?php foreach ($timeSlots as $key => $slot) {  
		              		if(isset($defaultSetting->wed)){
		            			$wednesdaySlots =  json_decode($defaultSetting->wed,true);
		            		}
		            		else{
		            			$wednesdaySlots = $defaultSetting['wed'];
		            		}
		              	?>
		              		<div class="timing-check-div">
		              			<input type="hidden" name="settings[timeslots][wed][<?=$key?>]">
		              			<input type="checkbox" name="settings[timeslots][wed][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $wednesdaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
		              		</div>
		            	<?php } ?>
		            </div>
		        </div>

		        <div class="box box-default box-solid collapsed-box">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Thursday</h3>
						<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
		              	</div>
		            </div>
		            <div class="box-body">
		               <?php foreach ($timeSlots as $key => $slot) {  
		              		if(isset($defaultSetting->thu)){
		            			$thursdaySlots =  json_decode($defaultSetting->thu,true);
		            		}
		            		else{
		            			$thursdaySlots = $defaultSetting['thu'];
		            		}
		              	?>
		              		<div class="timing-check-div">
		              			<input type="hidden" name="settings[timeslots][thu][<?=$key?>]">
		              			<input type="checkbox" name="settings[timeslots][thu][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $thursdaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
		              		</div>
		            	<?php } ?>
		            </div>
		        </div>

		        <div class="box box-default box-solid collapsed-box">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Friday</h3>
						<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
		              	</div>
		            </div>
		            <div class="box-body">
		               <?php foreach ($timeSlots as $key => $slot) {  
		              		if(isset($defaultSetting->fri)){
		            			$fridaySlots =  json_decode($defaultSetting->fri,true);
		            		}
		            		else{
		            			$fridaySlots = $defaultSetting['fri'];
		            		}
		              	?>
		              		<div class="timing-check-div">
		              			<input type="hidden" name="settings[timeslots][fri][<?=$key?>]">
		              			<input type="checkbox" name="settings[timeslots][fri][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $fridaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
		              		</div>
		            	<?php } ?>
		            </div>
		        </div>

		        <div class="box box-default box-solid collapsed-box">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Saturday</h3>
						<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
		              	</div>
		            </div>
		            <div class="box-body">
		               <?php foreach ($timeSlots as $key => $slot) {  
		              		if(isset($defaultSetting->sat)){
		            			$saturdaySlots =  json_decode($defaultSetting->sat,true);
		            		}
		            		else{
		            			$saturdaySlots = $defaultSetting['sat'];
		            		}
		              	?>
		              		<div class="timing-check-div">
		              			<input type="hidden" name="settings[timeslots][sat][<?=$key?>]">
		              			<input type="checkbox" name="settings[timeslots][sat][<?=$key?>]" value="<?=$slot?>" <?=(in_array($slot, $saturdaySlots["workingHours"])) ? "checked" : "" ?>><?=$slot?>
		              		</div>
		            	<?php } ?>
		            </div>
		        </div>
		        <?php ActiveForm::end(); ?>
			</div>
        </div>
    </div>
</div>        		