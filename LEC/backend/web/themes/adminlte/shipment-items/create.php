<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShipmentItems */

$this->title = 'Create Shipment Items';
$this->params['breadcrumbs'][] = ['label' => 'Shipment Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipment-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
