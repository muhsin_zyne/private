<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShipmentItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipment-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shipmentId')->textInput() ?>

    <?= $form->field($model, 'orderItemId')->textInput() ?>

    <?= $form->field($model, 'quantityShipped')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
