<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShipmentItems */

$this->title = 'Update Shipment Items: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Shipment Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shipment-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
