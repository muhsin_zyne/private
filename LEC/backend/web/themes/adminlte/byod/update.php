<?php
	use yii\helpers\Html;
	$this->title = 'Update BYOD: ' . ' ' . $byod->studentCode;
	$this->params['breadcrumbs'][] = ['label' => 'BYOD', 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $byod->studentCode, 'url' => ['view', 'id' => $byod->id]];
	$this->params['breadcrumbs'][] = 'Update';
?>

<div class="byod-update">
	<?= $this->render('_form', compact('byod','byodProducts')) ?>
</div>