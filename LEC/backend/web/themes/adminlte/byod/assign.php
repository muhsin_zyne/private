<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\Helper;

$this->title = 'Assign Product';
$this->params['breadcrumbs'][] = ['label' => 'BYOD', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
	<div class="box-body">
		<div class="add_tray_products byod_assign_mainclass">
        	<div class="row">
        		<div class="col-md-12">
        			<?php $form = ActiveForm::begin(['id' => 'byod-assign-form','options'=>['enctype'=>'multipart/form-data']]); ?>
                	<div class="search sm-100">
                		<label class="kv-heading-container" for="products-size">Search</label>
                		<input type="text" class="product_search form-control" autocomplete="off" value="Enter SKU or Product Title" onblur="if(this.value=='')this.value='Enter SKU or Product Title';" onfocus="if(this.value=='Enter SKU or Product Title')this.value='';">
                		<span style="font-style:italic">Start typing to get autocomple results</span>
                		<div class="product_box">
		                    <div class="product_list"></div>
		                    <span style="font-style:italic;font-size:12px;">Tick the items and click the 'Add to List' button below</span>
                            <div class="form-group" style="margin-bottom:30px">
                                <?= Html::a('Add to List','javascript:;',$options = ['class'=>'btn btn-primary add_pdt']) ?>
                            </div>
							<div class="tray_products_table" style="display:none">
				        		<div class="table-responsive">
				            		<table border="1" cellspacing="0" cellpadding="0" class="table sm-table">
				                		<thead>
				                    		<tr class="headings">
				                    			<th style="width: 3%;"></th>
						                        <th style="width: 20%;">Name</th>
						                        <th style="width: 12%;">SKU</th>
						                        <th style="width: 14%;">Original sell price</th>
						                        <th style="width: 14%;">BYOD price (Incl GST)</th>
				                                <th style="width: 14%;">BYOD price (Excl GST)</th>
						                        <th style="width: 5%;">Remove</th>
				                    		</tr>
				                    	</thead>
				                    	<tbody>
				                    	</tbody>
				                    </table>
				                </div>
				            </div>        	
						</div>
		            </div>
		            <div class="search sm-100">
		            	<label class="kv-heading-container assign-title" for="products-size">Available BYOD List</label>
		                <div class="byod_list">
		                	<?php foreach ($byodlist as $byod) { ?>
		                		<div class="byod_item"><input type="checkbox" name="Byod[id][<?=$byod->id?>]" value="<?=$byod->id?>" class="selected"><?=$byod->studentCode?></div>
		                	<?php } ?>
		                </div>
		            </div>    
		            </div>
		            <div class="form-group" style="margin-top:20px;margin-bottom:30px">
		                <?= Html::submitButton('Assign', ['class' => 'btn btn-success byod-assign-button']) ?>
		            </div> 
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>        			
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		addedproducts = [];
	    listedproducts = [];
		$(".product_search").on('keyup',function(e){
            $('.add_pdt').show();
			$('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });
            
            $(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });
			var keyword = $('.product_search').val();
			$.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl(['byod/getproducts'])?>",    
                type: 'POST',
                data: {keyword: keyword},
                success: function(data)
                    {
                        $(".product_item").empty("");
						if(jQuery.trim($(".product_search")).length > 0) {   
                            $.each($.parseJSON(data), function(key, item) { 
								if($.inArray(key, listedproducts) == -1) {
									if ($.inArray(key, addedproducts) == -1) {    
                                    	$('.product_list').append('<div class="product_item"><input type="checkbox" name="Products[]" value="'+item['id']+'" class="product_checkbox" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'">'+item['name']+' - '+item['sku']+'</div>');
                                    }    
                                }    
                            }); 
                        }   
                    }
            });

		});
		$('#w1-tree').height('150');
		$('.add_pdt').on('click',function(){ 
			$(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });

            $('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });

            checked_pdts = [];

            $('.product_checkbox:checked').each(function(){
                checked_pdts.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price')});
            }); 

            $(".tray_products_table").css('display','block');

            for( var i = 0, xlength = checked_pdts.length; i < xlength; i++) { 
                if ($.inArray(checked_pdts[i].idss, listedproducts) == -1) {
					if ($.inArray(checked_pdts[i].idss, addedproducts) == -1) {
						$(".tray_products_table table tbody").append('<tr data-productId="'+checked_pdts[i].idss+'"><td><input type="checkbox" name="Products['+checked_pdts[i].idss+']" class="selected" value="'+checked_pdts[i].idss+'" checked></td><td>'+checked_pdts[i].names+'</td><td>'+checked_pdts[i].sku+'</td><td><input type="text" name="Products['+checked_pdts[i].idss+'][original_sell_price]" class="offer_price" value="'+checked_pdts[i].price+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][offerPrice]" class="offer_price portal_price" autocomplete="off"></td><td><input type="text" name="exclPrice" class="exclgst" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr>');
                    }    
                }    
            }

            $('.product_list').empty();
            $('.product_search').val('');
            $('.add_pdt').hide(); 
		});	

		$('body').on('change','.portal_price',function(){
        	var incprice = $(this).val();
        	var gstpercentage = <?=\common\models\Configuration::findSetting('gst_percentage',0); ?> ;
        	var excprice = (incprice*100)/(100+gstpercentage);
        	$(this).parent().next().find('.exclgst').val(excprice.toFixed(2));
    	});

    	$(".tray_products_table").on("click",".remove", function(e){  
        	$(this).closest('tr').remove();    
    	});

	});
</script>	