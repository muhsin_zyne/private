<?php
use common\models\Products;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\User;
use backend\components\Helper;
use common\models\OrderMeta;

ini_set('max_execution_time', 10000);
ini_set('memory_limit', '5000M');

$output	= "";
$connection = Yii::$app->getDb();

//var_dump($byodId);die();

$command = $connection->createCommand("SELECT ClientPortal.id,ClientPortal.organisation_name,ClientPortal.studentCode,ClientPortal.schoolCode,ClientPortal.address,ClientPortal.city,ClientPortal.state,ClientPortal.postcode,ClientPortal.validFrom,ClientPortal.expiresOn,oi.orderId from ClientPortal JOIN Orders o ON o.portalId = ClientPortal.id JOIN ClientPortalProducts cpp ON cpp.portalId = ClientPortal.id JOIN OrderItems oi ON oi.orderId = o.id  where o.portalId = '$byodId' and o.type='byod' GROUP BY ClientPortal.id");

$result = $command->queryAll();

$ordersCommand = $connection->createCommand("SELECT Orders.id,Orders.orderDate,Orders.grandTotal from Orders WHERE Orders.type='byod' AND Orders.portalId = '$byodId' GROUP BY Orders.id");

$orders = $ordersCommand->queryAll();

$orderItemsCommand = $connection->createCommand("SELECT Orders.id,Orders.orderDate,Orders.grandTotal,AttributeValues.value,Products.sku,OrderItems.price,OrderItems.quantity FROM OrderItems 
	INNER JOIN Orders ON Orders.id=OrderItems.orderId AND Orders.type='byod'
	INNER JOIN Products ON Products.id=OrderItems.productId 
	INNER JOIN Attributes ON Attributes.code='name'
	INNER JOIN AttributeValues ON AttributeValues.productId=OrderItems.productId
	WHERE AttributeValues.attributeId=Attributes.id AND Orders.type='byod' AND Orders.portalId = '$byodId'
    GROUP BY OrderItems.id");

$orderItems = $orderItemsCommand->queryAll();

$byodHeader=array("Organisation Name","Address","Student Code", "School Code", "Valid From", "Expired On");
$orderItemsHeader = array("Order Id","Order Date","Student Name","Parents Name","Order Total","Product Name","SKU","quantity","Special Price(excl. GST)");

foreach ($byodHeader as  $value) { 
 	$heading	=	$value;
	$output		.= '"'.$heading.'",';
}

$output .="\n";

$f=0;

foreach($result as $key=>$value)
	{ 
		if($value['orderId'] != $f) {
			$output .='"'.$value['organisation_name'].'","'.$value['address'].','.$value['city'].','.$value['state'].','.$value['postcode'].'","'.$value['studentCode'].'","'.$value['schoolCode'].'","'.Helper::date($value['validFrom']).'","'.Helper::date($value['expiresOn']).'"';
				$output .="\n";
		}
		$output .="\n"; 

		
	}


foreach ($orderItemsHeader as  $value) { 
 	$heading	=	$value;
	$output		.= '"'.$heading.'",';
}

$output .="\n";

foreach ($orders as $order) {
	$output .= '"'.$order['id'].'","'.Helper::date($order['orderDate']).'","'.OrderMeta::findValue('byod_studentName', $order['id']).'","'.OrderMeta::findValue('byod_parentName', $order['id']).'","'.$order['grandTotal'].'","","","",""';
	$output .="\n";
	foreach ($orderItems as $item) {
		if($order['id'] == $item['id']){
			$output .='"","","","","","'.$item['value'].'","'.$item['sku'].'","'.$item['quantity'].'","'.$item['price'].'"';
			$output .="\n";
		}	
	}
	$output .="\n";
}

$filename ='Byod #'.$byodId.' Orders CSV_'.date("Y-m-d").'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
echo $output;
exit;
	

?>