<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\components\Helper;



/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotions */

$this->title = 'BYOD Code: '.$byod->studentCode;
$this->params['breadcrumbs'][] = ['label' => 'BYOD', 'url' => ['index']];
$this->params['breadcrumbs'][] = $byod->studentCode;

?>
<div class="box">
<div class="box-body">
<div class="consumer-promotions-view">

    <h1><?php // Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $byod->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $byod->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?=Html::a('<i class="fa fa-download"></i> Download Report',['report','id'=>$byod->id],['class'=>'btn btn-primary'])?>
        <?=Html::a('<i class="fa fa-files-o"></i> Duplicate BYOD',['create','duplicateFrom'=>$byod->id],['class'=>'btn btn-primary'])?>

   	</p>

    <?= DetailView::widget([
        'model' => $byod,
        'attributes' => [
            [   
                'attribute' => 'studentCode',
                'label'=>'Student Code',
                'value' =>  $byod->studentCode,
            ],
            [   
                'attribute' => 'schoolCode',
                'label'=>'School Code',
                'value' =>  $byod->schoolCode,
            ],
            [   
                'attribute' => 'shipment_type',
                'label'=>'Shipment Type',
                //'value' =>  ($byod->shipment_type == "instore-pickup") ? "Instore Pickup" : (($byod->shipment_type == "delivery-program") ? "Delivery Program" : "N/A") : "N/A" ,
                'value' => ($byod->shipment_type == "instore-pickup") ? "Instore Pickup" : "Delivery Program" ,
            ],
            [   
                'attribute' => 'payment_type',
                'label'=>'Payment Type',
                //'value' =>  $byod->payment_type,
                'value' => ($byod->payment_type == "online-payment") ? "Online Payment" :  "No Online Payment"  ,
            ],
            [   
                'attribute' => 'validFrom',
                'label'=> 'Valid From',
                'value' =>  Helper::date($byod->validFrom),
            ],
            [   
                'attribute' => 'expiresOn',
                'label'=> 'Expires On',
                'value' =>  Helper::date($byod->expiresOn),
            ],
            [
            	'label' => 'Secure URL for Students',
            	'value' => $store->siteUrl.'/byod/authenticate?token='.$byod->studentAuthCode,
            ],
            [
                'label' => 'Secure URL for School',
                'value' => $store->siteUrl.'/byod/authenticate?token='.$byod->schoolAuthCode,
            ]
        ],
    ]) ?>

    <?=GridView::widget([
        'dataProvider' => $byodProducts,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'product.name',
            'product.sku',
            'product.price',
            [
                'attribute' => 'offerPrice',
                'label' => 'Offer Price (Incl GST)',
            ],
            /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            ],*/
        ],
    ]);?>

    <?php //var_dump($dataProvider);die(); ?>
</div>
</div>
</div>