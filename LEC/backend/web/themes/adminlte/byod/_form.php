<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use kartik\date\DatePicker;
	use backend\components\Helper;
	use backend\assets\AppAsset;
	use yii\web\UploadedFile;
	use kartik\widgets\FileInput;
	use yii\grid\GridView;
	use common\models\ClientPortal;
	use common\models\ClientPortalProducts;
?>


<?php 
    //$storeId = (Yii::$app->user->identity->roleId==3) ? Yii::$app->user->store->id : "0";
    //var_dump(\common\models\Configuration::findSetting('gst_percentage',0)); 
?>

<div class="error-msg"> </div>
<div class="box">
  	<div class="box-body">
  		<div class="byod-form">
			<?php $form = ActiveForm::begin(['id' => 'byod-form','options'=>['enctype'=>'multipart/form-data'],'enableAjaxValidation' => true,'enableClientValidation' => true]); 
				$option =[];
		        if(!$byod->isNewRecord) {
		            $option = ['disabled'=>'disabled'];
		        }
                else{
                    $option = ['value'=>''];
                } 
			?>
			<?=$form->field($byod, 'studentCode')->textInput($option) ?>
            <?=$form->field($byod, 'schoolCode')->textInput($option) ?>
			<?php //if($byod->isNewRecord) { ?>
		        <!-- <button type="button" class="btn btn-success generate">Generate BYOD Code</button>
		        <br /><br /> -->
		    <?php //} ?>

		    <?= $form->field($byod, 'shipment_type')->dropDownList((['instore-pickup'=>'In Store Pick Up','delivery-program'=>'Collection/Delivery Program']),['prompt'=>'Select...'])->label('Shipment Type'); ?>

		    <?= $form->field($byod, 'payment_type')->dropDownList((['online-payment'=>'Online Payment','no-online-payment'=>'No Online Payment']),['prompt'=>'Select...'])->label('Payment Type'); ?>
		    <?=DatePicker::widget([
				'form' => $form,
				'model' => $byod,
				'attribute' => 'validFrom',
				'pluginOptions' => [
					'format' => 'dd-MM-yyyy',
                    'startDate' => '+0d'
				],
			]);?>
			<?=DatePicker::widget([
				'form' => $form,
				'model' => $byod,
				'attribute' => 'expiresOn',
				'pluginOptions' => [
					'format' => 'dd-MM-yyyy',
                    'startDate' => '+0d'
				],
			]);?>



            <div class="school-del-class" style="display:none">
            <?=DatePicker::widget([
                'form' => $form,
                'model' => $byod,
                //'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'attribute' => 'schoolDeliverydate',
                'pluginOptions' => [
                    'format' => 'dd-MM-yyyy',
                    'startDate' => '+0d',
                    /*'beforeShowDay' => new \yii\web\JsExpression("function(date) {
                        entDate = $('#clientportal-expireson').val();
                        var exDate = new Date(entDate);
                        if(exDate >= date){
                            return {classes: 'disabled', tooltip: 'Title'};
                        }
                    }"),  
                    'defaultViewDate' => new \yii\web\JsExpression("function(date) {

                    }"),  */  
                ],
            ]);?>

            <!-- <input type="text" class="to span2" value="" id="to"> -->

            </div>
			<?= $form->field($byod, 'usage_count')->textInput(['maxlength' => 45])->label('Maximum No of Uses') ?>
			<div class="shipment1_fields">
				<?= $form->field($byod, 'name')->textInput(['maxlength' => 100])->label('Name')->label('Contact Name') ?>
				<?= $form->field($byod, 'email')->textInput(['maxlength' => 100])->label('Email')->label('Contact Email') ?>	
			</div>
			<div class="shipment2_fields">
				<?= $form->field($byod, 'organisation_name')->textInput()->label('Name of School') ?>
				
				<div class="brand-image" style="margin-bottom:10px;">
                    <?php
                        if(!empty($byod->organisation_logopath)){
                            $pluginOptions = ['initialPreview' => [Html::img(Yii::$app->params["rootUrl"].$byod->organisation_logopath)],
                                                'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false
                                            ];
                        }
                        else{
                            $pluginOptions = ['overwriteInitial'=>true,'showRemove' => false,'showUpload' => false
                                            ];
                        }
                        echo '<label class="control-label">School Logo (Logo size should be 260px X 120px (Width X Height))</label>';
                        echo FileInput::widget([
                            'model' => $byod,
                            'attribute' => 'organisation_logopath',
                            'pluginOptions' => $pluginOptions 
                        ]);
                    ?>
                </div> 
				
				<?= $form->field($byod, 'address')->textarea()->label('Address'); ?>
				<?= $form->field($byod, 'city')->textInput()->label('City') ?>	
				<?=$form->field($byod, 'state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])->label('State/Province')?>
				<?= $form->field($byod, 'postcode')->textInput(['maxlength' => 45])->label('Zip/Postal Code') ?>	
			</div>
			<?= $form->field($byod, 'phone')->textInput(['maxlength' => 100])->label('Phone (No dash or space please)') ?>

			<label class="control-label">Add Products</label>
 			<div class="add_tray_products">
        		<div class="row">
        			<div class="col-md-12">
                		<div class="search sm-100">
                			<label class="kv-heading-container" for="products-size">Search</label>
                			<?php // Html::textInput('search','',$options=['class'=>'product_search form-control','placeholder'=>'Enter SKU or name of a product','autocomplete'=>'off']) ?>
                            <input type="text" class="product_search form-control" autocomplete="off" value="Enter SKU or Product Title" onblur="if(this.value=='')this.value='Enter SKU or Product Title';" onfocus="if(this.value=='Enter SKU or Product Title')this.value='';" onkeypress="if(event.keyCode=='13')return anythingValidate(document.frm_any_request)">
                    		<span style="font-style:italic">Start typing to get autocomple results</span>
                    		<div class="product_box">
		                        <div class="product_list"></div>  
                                <span style="font-style:italic;font-size:12px;">Tick the items and click the 'Add to List' button below</span>  
		                        <div class="form-group" style="margin-top:20px;margin-bottom:30px">
		                            <?= Html::a('Add to List','javascript:;',$options = ['class'=>'btn btn-primary add_pdt']) ?>
		                        </div>
		                    </div>
                		</div>
                	</div>	
        		</div>
        		<div class="clear"></div>
        	</div>

        	<div class="tray_products_table">
        		<div class="table-responsive">
            		<table border="1" cellspacing="0" cellpadding="0" class="table sm-table">
                		<thead>
                    		<tr class="headings">
                    			<th></th>
		                        <th>Name</th>
		                        <th>SKU</th>
		                        <th>Original sell price</th>
		                        <th>BYOD price(Incl GST)</th>
                                <th>BYOD price(Excl GST)</th>
		                        <th>Remove</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		<?php  //var_dump($byod->clientPortalProducts);die();
                        		//if(!$byod->isNewRecord) {
                                if(!empty($byod->clientPortalProducts)) {
                                    $gstPercentage = \common\models\Configuration::findSetting('gst_percentage',0);    
                            		foreach ($byod->clientPortalProducts as $product) { 
                                    if(!is_null($product->product)){
                    		?>
		                        <tr data-productId=<?=$product['id']?>>
		                            <td><input type="checkbox" checked name="Products[id][<?=$product['id']?>]" class="selected" value="<?=$product->product->id?>"></td>
		                            <td><?=$product->product->name?></td>
		                            <td><?=$product->product->sku?></td>
		                            <td><?=$product->product->price?></td>
		                            <td><input type="text" name="Products[<?=$product->product->id?>][offerPrice]" class="offer_price portal_price" autocomplete="off" value="<?=$product['offerPrice']?>"></td>
                                    <td><input type="text" name="GST" class="exclgst" autocomplete="off" value="<?=Helper::getPriceExclGST($product['offerPrice'],$gstPercentage)?>"></td>
		                            <td><a href="#" class="remove" title="Delete">X</td>
		                        </tr>
                    		<?php } } } //} ?>
                    	</tbody>
                    </table>
                </div>
            </div>    

            <div class="deleted_items"></div>    		
			<div class="form-group">
                <?= Html::submitButton($byod->isNewRecord ? 'Create' : 'Update', ['class' => $byod->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
		</div>	 
  	</div>
</div> 		

<script type="text/javascript">
function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var j=0; j<3; j++){
    for( var i=0; i < 5; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    if(j<2)
    text = text +"-";
    }
    return text;
}

/*$('#clientportal-shipment_type').change(function(){
	var selected = $('#clientportal-shipment_type').val();
	if(selected == "instore-pickup"){
		$('.shipment2_fields').hide();
		$('.shipment1_fields').show();
	}
	else{
		$('.shipment1_fields').hide();
		$('.shipment2_fields').show();
	}
});*/

$(document).ready(function(){
    $('.generate').click(function(){
    	$('#clientportal-code').val(makeid());
	});

	/*var selected = $('#clientportal-shipment_type').val();
	if(selected == "instore-pickup"){
		$('.shipment2_fields').hide();
	}
	else{
		$('.shipment1_fields').hide();
	}*/

    $('body').on('change','#clientportal-shipment_type',function(){
        var shipment_type = $('#clientportal-shipment_type').val();
        var payment_type = $('#clientportal-payment_type').val();

        if(shipment_type == "delivery-program"){
            $('.school-del-class').css('display','block');
        }
        else{
            $('.school-del-class').css('display','none');
        }
    });

    $('body').on('change','#clientportal-payment_type',function(){
        var shipment_type = $('#clientportal-shipment_type').val();
        var payment_type = $('#clientportal-payment_type').val();

        if(shipment_type == "delivery-program"){
            $('.school-del-class').css('display','block');
        }
        else{
            $('.school-del-class').css('display','none');
        }
    });      
});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		addedproducts = [];
	    listedproducts = [];
		$(".product_search").on('keyup',function(e){
			$('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });
            
            $(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });
			var keyword = $('.product_search').val();
			$.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl(['byod/getproducts'])?>",    
                type: 'POST',
                data: {keyword: keyword},
                success: function(data)
                    {
                        $(".product_item").empty("");
						if(jQuery.trim($(".product_search")).length > 0) {   
                            $.each($.parseJSON(data), function(key, item) { 
								if($.inArray(key, listedproducts) == -1) {
									if ($.inArray(key, addedproducts) == -1) {    
                                    	$('.product_list').append('<div class="product_item"><input type="checkbox" name="Products[]" value="'+item['id']+'" class="product_checkbox" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'">'+item['name']+' - '+item['sku']+'</div>');
                                    }    
                                }    
                            }); 
                        }   
                    }
            });

		});
		$('#w1-tree').height('150');
		$('.add_pdt').on('click',function(){ 
			$(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });

            $('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });

            checked_pdts = [];

            $('.product_checkbox:checked').each(function(){
                checked_pdts.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price')});
            }); 

            $(".tray_products_table").css('display','block');

            for( var i = 0, xlength = checked_pdts.length; i < xlength; i++) { 
                if ($.inArray(checked_pdts[i].idss, listedproducts) == -1) {
					if ($.inArray(checked_pdts[i].idss, addedproducts) == -1) {
						$(".tray_products_table table tbody").append('<tr data-productId="'+checked_pdts[i].idss+'"><td><input type="checkbox" name="Products[id]['+checked_pdts[i].idss+']" class="selected" value="'+checked_pdts[i].idss+'" checked></td><td>'+checked_pdts[i].names+'</td><td>'+checked_pdts[i].sku+'</td><td><input type="text" name="Products['+checked_pdts[i].idss+'][original_sell_price]" class="offer_price" value="'+checked_pdts[i].price+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][offerPrice]" class="offer_price portal_price" autocomplete="off"></td><td><input type="text" name="exclPrice" class="exclgst" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products['+checked_pdts[i].idss+'][newitem]" id="data-new" value="1">');
                    }    
                }    
            }

            $('.product_list').empty();
            $('.product_search').val(''); 
		});	
	}); 

	<?php if(empty($byod->clientPortalProducts)) {  ?>
		$(".tray_products_table").css('display','none');
	<?php } ?>		

	$(".tray_products_table").on("click",".remove", function(e){  
        var trid = $(this).closest('tr').attr('data-productId');  
		if(trid) {
            jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedItems][]" id="deletedItems" value="'+trid+'"/>');
        }
        $(this).closest('tr').remove();    
    });

    $('body').on('change','.portal_price',function(){
        var incprice = $(this).val();
        var gstpercentage = <?=\common\models\Configuration::findSetting('gst_percentage',0); ?> ;
        var excprice = (incprice*100)/(100+gstpercentage);
        $(this).parent().next().find('.exclgst').val(excprice.toFixed(2));
    });

    /*$('body').on('click', '.portal_price', function(){    
        var qty = $(this).prev().val();
        if(qty == ""){
          $(this).prev().addClass('error_box');
          alert('Please enter quantity required');
          return false;
        }
        if($('.chkbx:checked').length < 1 && $('.chkbx').length > 0){
            alert('Please tick atleast one product to order');
            return false;
        }  

    });*/

</script>

<script type="text/javascript">

    /*$('form').submit(function(e) { 
        //var qty = $('.portal_price').val();
        var isValid = true;
        $( ".portal_price" ).each(function() {
            if ($.trim($(this).val()) == '') {
                isValid = false;
                $(this).css({
                    "border": "1px solid red !important;",
                    "background": "#FFCECE",
                });
                $(this).addClass('error_box');
            }

            else {
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        //console.log(valid);
        if(isValid){
            //alert('values are entered');
        }
        else{
            //alert('pls enter value');
            //$('.error_box').parent().append('<p style="color:red">Please Enter a valid offer Price</p>');
            e.preventDefault();

        }
    });*/
    
    /*$(document).ready(function(){
        $('form').submit(function(e) {
            if ($('.portal_price').val()) {
                console.log($(this));
            }
            else{
                $('.portal_price').addClass('errorclass');
                myFunc();
                e.preventDefault();
            }
        });
        
        function myFunc() {
            $('.errorclass').append('<p style="color:red">Please Enter a valid offer Price</p>');
            alert('Please Enter a valid offer Price');
            
        }  
    });  */

    $(document).ready(function(){
        $('form').submit(function(e) {
            var isValid = true;
            $( ".portal_price" ).each(function() {
                if($(this).val()){
                    if($(this).hasClass('error_box'))
                        $(this).removeClass('error_box');
                }
                else{
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red !important;",
                        "background": "#FFCECE",
                    });
                    $(this).addClass('error_box');
                    $(this).attr("placeholder", "Enter your BYOD price");
                }
            });
            if(isValid == false){ 
                //myFunc();
                e.preventDefault();
            }
        });  

        function myFunc() {
            $('.error_box').parent().append('<p style="color:red">Please Enter a valid offer Price</p>');
            alert('empty');
        }  


        // var _startDate = new Date();
        // var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000));
        // $('#to').datepicker({
        //     startDate: _endDate,
        // }); 

        

          
    });  

</script>