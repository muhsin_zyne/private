<?php
use yii\helpers\Html;

$this->title = 'Create BYOD';
$this->params['breadcrumbs'][] = ['label' => 'BYOD', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
<div class="box-body">

<div class="conferences-create">

    <?= $this->render('_form',compact('byod')) ?>
</div>

</div>
</div>