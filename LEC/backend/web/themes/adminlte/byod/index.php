<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\Helper;
use yii\helpers\Url;

$this->title = 'BYOD';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
	<div class="box-body">
		<div class="conferences-index byod-index">
				
			<p class="create_pdt">
				<?= Html::a('<i class="fa fa-plus"></i> Assign Product', ['assign'], ['class' => 'btn btn-success']) ?>
			</p>
			<p class="create_pdt">
				<?= Html::a('<i class="fa fa-plus"></i> Create BYOD', ['create'], ['class' => 'btn btn-success']) ?>
			</p>

			<?php
                            $form = \kartik\form\ActiveForm::begin(['id' => 'pricerules-form']);
                            \kartik\form\ActiveForm::end();
                        ?>

			<?= \app\components\AdminGridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
					'studentCode',
					'schoolCode',
		            [
		                'attribute'=>'validFrom',
		                'label'=>'Start Date',
		                'value' => function($model,$attribute){
		                  return Helper::date($model->validFrom);
		                },
		                 	'filter' => \kartik\field\FieldRange::widget([
                                'form' => $form,
                                'model' => $searchModel,
                                'template' => '{widget}{error}',
                                'attribute1' => 'validFrom_start',
                                'attribute2' => 'validFrom_end',
                                'type' => \kartik\field\FieldRange::INPUT_DATE,
                            ])
		            ],
		            [
		                'attribute'=>'expiresOn',
		                'label'=>'End Date',
		                'value' => function($model,$attribute){
		                  return Helper::date($model->expiresOn);
		                },
		                	'filter' => \kartik\field\FieldRange::widget([
                                'form' => $form,
                                'model' => $searchModel,
                                'template' => '{widget}{error}',
                                'attribute1' => 'expiresOn_start',
                                'attribute2' => 'expiresOn_end',
                                'type' => \kartik\field\FieldRange::INPUT_DATE,
                            ])
		            ],
		            [
		            	'label' => 'Shipment Type',
		            	'attribute'=>'shipment_type',
		            	'value' => function($model,$attribute){
		                  return ucwords($model->shipment_type);
		                },
		                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'shipment_type', ['instore-pickup' => 'Instore Pickup', 'delivery-program' => 'Delivery Program',],
		                    ['class'=>'form-control','prompt' => '']),
		            ],
		            [
		            	'label' => 'Payment Type',
		            	'attribute'=>'payment_type',
		            	'value' => function($model,$attribute){
		                  return ucwords($model->payment_type);
		                },
		                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'payment_type', ['online-payment' => 'Online Payment', 'no-online-payment' => 'No Online Payment',],
		                    ['class'=>'form-control','prompt' => '']),
		           	],
		            //'type',
		            //'usage_count',
		            [
						'class' => 'yii\grid\ActionColumn',
						'template' => '{view}{update}{delete}{duplicate}',
		                'buttons' => [
							'view' => function ($url, $model, $key) {
								return '<a href="'.$url.'" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>';
							},
							'update' => function ($url, $model, $key) {
								return '<a href="'.$url.'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
							},
							'delete' => function ($url, $model, $key) {
								
								//$text = ($model->hasOrders()) ? "" : "no orders";
								/*return Html::a('<span class="fa fa-trash"></span>', $url, ['class'=>'disabled-btn'],['data-confirm' => 'Are you sure you want to delete this item?', 'data-method' =>'POST']); */
								$action = ($model->hasOrders()) ?  Html::a('<span class="fa fa-trash"></span>','' ,["data-confirm" => "Sorry, you coludn't remove this BYOD. Orders are present by using this BYOD "]) :  Html::a('<span class="fa fa-trash"></span>', $url,['data-confirm' => 'Are you sure you want to delete this item?', 'data-method' =>'POST']);
								return $action;

							},
							'duplicate' => function ($url, $model, $key){
								return '<a href="'.Url::to(['byod/create','duplicateFrom'=>$model->id]).'" title="Duplicate" data-pjax="0"><span class="fa fa-files-o"></span></a>';
							}	
						],
					],
        		],
    		]); 
		?>
		</div>
	</div>
</div>