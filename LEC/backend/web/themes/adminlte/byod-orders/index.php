<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use kartik\export\ExportMenu;
use common\models\User;
use common\models\OrderMeta;

$this->title = 'BYOD Orders';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
	<div class="box-body">
		<div class="orders-index">
		    <p class="create_pdt">
		     <?php //Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['byod/export'], Yii::$app->request->queryParams)), ['class'=>'buttons-update btn btn-success enabled']); ?>
		    </p>

		    <?php
		        $form = \kartik\form\ActiveForm::begin(['id' => 'b2borders-form']);
		        \kartik\form\ActiveForm::end();
    		?>
    		<?php \yii\widgets\Pjax::begin(['id' => 'recent-products',]); ?>
    		<?=\app\components\AdminGridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'rowOptions'   => function ($model, $key, $index, $grid) {
		            return ['data-id' => $model->id];
		        },
		        'options'=>['class'=>'box-body table-responsive no-padding grid-view'],
		        'showHeader'=>true,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
					'id',
					[
		                'label' => 'Order Date',
		                'attribute' => 'id',
		                'value' => function($model){ return \backend\components\Helper::date($model->orderDate); },
		                'filter' => \kartik\field\FieldRange::widget([
		                        'form' => $form,
		                        'model' => $searchModel,
		                        'template' => '{widget}{error}',
		                        'attribute1' => 'orderDate_start',
		                        'attribute2' => 'orderDate_end',
		                        'type' => \kartik\field\FieldRange::INPUT_DATE,
		                ]),
		                'headerOptions' => ['class' => 'date-range']
		            ],
		            [
		            	'label' => 'Student Code',
		            	'attribute' => 'portal.studentCode',
		            ],
		            [
		            	'label' => 'School Code',
		            	'attribute' => 'portal.schoolCode',
		            ],
		            [
		                'label' => 'Billing Address',
		                'format' => 'html',
		                'attribute' => 'billingAddressText',
		                'value' => 'billingAddressText',
		               
		            ],
		            [
		            	'label' => 'Organisation Name',
		            	'attribute' => 'portal.organisation_name',
		            ],
		            /*[
		            	'label' => 'Parent Name',
		            	'value' => function($model) {
		            		return (!is_null($parentName = OrderMeta::findValue('byod_parentName', $model->id)) ? $parentName : "") ;
		            	},
		            ],*/
		            [
		            	'label' => 'Shipping Method',
		            	'attribute' => 'portal.shipment_type',
		            	'value' => function($model, $attribute){ 
		                    return ucwords($model->portal->shipment_type);
		                },
		                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['instore-pickup' => 'Instore Pickup', 'delivery-program' => 'Delivery Program',],
		                    ['class'=>'form-control','prompt' => '']),
		            ],
		           	[
		                'label' => 'Grand Total',
		                'attribute' => 'grandTotal',
		                'format' => 'html',
		                'value' => function ($model) {
		                    return Helper::money($model->grandTotal);
		                },
		            ],
		            [
		                'label' => 'Status',
		                'attribute' => 'status',
		                'value' => function($model, $attribute){ 
		                    return ucwords($model->status);
		                },
		                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['in-process' => 'In Process', 'partially-ready-for-collection' => 'Partially Ready For Collection','fully-ready-for-collection' => 'Fully Ready For Collection', 'partially-collected' => 'Partially Collected','fully-collected' => 'Fully Collected', 'partially-refunded' => 'Partially Refunded', 'fully-refunded' => 'Fully Refunded',],
		                    ['class'=>'form-control','prompt' => '']),
		            ],
		            [
		                'label'=> 'View',
		                'format' => 'html',
		                'value' => function($model){
		                    return Html::a('View',['byod-orders/view','id' => $model->id]);
		                }
		            ],
				],
			]); ?>

        </div>
    </div>
</div>
   