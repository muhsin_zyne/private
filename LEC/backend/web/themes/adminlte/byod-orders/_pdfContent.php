<?php 
use yii\helpers\Html;
use frontend\components\Helper;
?>

<!DOCTYPE html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="margin:0;padding:0;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <div style="width:936px;margin:0 auto;">
    	<!--top-header-->
        <div style="float:left;width:100%;padding:10px 0;text-align:center;box-sizing:border-box;">
            <a href=""><?=$order->store->logo?></a>
            <h1 style="margin:10px 0 8px;font-size:16px;color:#5e5e5e;font-weight:bold;">BYOD Invoice Summary</h1>
            <p style="margin:6px 0 0px;font-size:13px;">Invoice No:<?=$invoice->id?></p>
        </div>
        <!--top-header-->
        <!--content-sec-->
        <div style="float:left;width:100%;padding:10px 0;box-sizing:border-box;">
            <!--box1-->
            <div style="float:left;width:48%;padding:0 10px 0 0;box-sizing:border-box;">
                <!--inbox-->
                <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;height:155px;box-sizing:border-box;">
                    <h4 style="margin:0;margin-bottom:10px;font-size:14px;color:#535353;font-weight:bold;">
                    Customer Details:
                    </h4>
                    <div style="display:inline-block;color:#535353!important;"><?=$order->deliveryBillingInfo?></div>
                </div>
                <!--inbox-->
            </div>
            <!--box1-->
            <!--box1-->
            <div style="float:left;width:48%;padding:0 10px 0 0;box-sizing:border-box;">
                <!--inbox-->
                <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;height:155px;box-sizing:border-box;">
                    <h4 style="margin:0;margin-bottom:10px;font-size:14px;color:#535353;font-weight:bold;">
                    School Address:
                    </h4>
                    <?=$portal->schoolAddress?>
                </div>
                <!--inbox-->
            </div>
            <!--box1-->

            <div style="background:#fafafa;width:93%;border:1px solid #eeeeee;box-sizing:border-box;padding:15px;float:left;margin-top:10px;">
            	<div style="color:#535353;width:23%;float:left;text-align:center;">Order ID:<span style="font-weight:bold"> #<?=$order->id?></span></div>
            	<div style="color:#535353;width:30%;float:left;text-align:center;">BYOD Code:<span style="font-weight:bold"> <?=$portal->schoolCode?></span></div>
            	<div style="color:#535353;width:40%;float:left;text-align:center;">Order Date:<span style="font-weight:bold"> <?=$order->orderPlacedDate?></span></div>
            </div>

            <!--box1-->

            <div style="padding:15px;float:left;width:93%;margin-top:10px;background:#fafafa;border:1px solid #eeeeee;box-sizing:border-box;">
                
                <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;border: 1px solid #ddd;">
                    <thead>
                        <tr>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">#</th>
                            <th style="text-align:left;padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">Item</th>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">Qty</th>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">Discount</th>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">Gift Wrapping</th>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">Item Total</th>
                        </tr>
                    </thead>
                    <tbody valign="top" style="background:#fff;">
                        <?=$order->portalOrderItemsShortGrid?>
                    </tbody>
                </table>
            </div>  
            <!-- Box 1 End -->
            <div style="padding:15px;float:left;width:93%;margin-top:10px;box-sizing:border-box;text-align:right;">
                <div style="margin-bottom:10px;">Item Subtotal: <?=$order->orderItemTotalFormatted?></div>
                <div style="margin-bottom:10px;">Gift Wrapping: <?=$order->giftWrapTotalFormatted?></div>
                <div style="margin-bottom:10px;">Discount Applied: <?=$order->formatedOrderDiscountAmount?></div>
                <div style="margin-bottom:10px;background-color:#fafafa;padding:5px;"><span style="font-weight:bold;">Order Total: </span> <?=$order->grandTotalFormatted?></div>
                <div><?=$order->paymentDetails?></div>
            </div>
        </div>   
        
    </div>
</body> 