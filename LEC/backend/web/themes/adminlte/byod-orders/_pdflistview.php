<?php 
use yii\helpers\Html;
use frontend\components\Helper;


$thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();

$wrapAmount = !empty($model->giftWrap) ? $model->giftWrapAmount : 0;

?>

<tr style="background:#fff;">
    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;">1</td>
    <td style="text-align:left;padding:8px;border-right:1px solid #ddd;border-bottom:1px solid #ddd;">
        <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;">
            <tbody>
                <tr>
                    <td width="72" style="padding:5px;border:1px solid #ccc;text-align:center;box-sizing:border-box;">
                        <img src="<?=$thumbImagePath?>" alt="items" style="max-width:60px;">
                    </td>
                    <td style="font-size:12px;padding-left:10px;box-sizing:border-box;">
                        <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;">
                            <tr>
                                <td style="padding:0 0 4px;font-size:12px;"><?=$model->product->name?></td>
                            </tr>
                            <tr>
                                <td style="padding:0 0 4px;color:#e31837;font-size:12px;"><?=Helper::money($model->price)?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;"><?=$model->quantity?></td>
    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;"><?=Helper::money($model->discount)?></td>
    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;"><?=Helper::money($model->giftWrapAmount*$model->quantity)?></td>
    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;"><?=Helper::money($model->itemSubTotal)?></td>
</tr>                            