<?php

use yii\helpers\Html;
use backend\components\Helper;
use common\models\User;
use yii\web\Session;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>



<?php 
	if($user->roleId != "3") { 
		$session = Yii::$app->session;
		//var_dump($session['storeId']);
		$session['storeId'] = $storeId;
?>
			
<?php } 
 	
$this->title = 'Social Media Manager';
$this->params['breadcrumbs'][] = $this->title;
 	 
?>
<div class="box">
<div class="box-body">
<div class="conferences-index">

    <?php if($user->roleId == "1") { ?>
    	<?=Html::dropDownList('listname', $selected=isset($session['storeId']) ? $session['storeId'] : "", ArrayHelper::map($stores,'id','title'),['class' => 'store-dropdown']); ?>
    <?php } ?>	

    	<div class="row">
			<div class="col-xs-12">
				<?php $form = ActiveForm::begin(); ?>

					<?= Html::label('Facebook', 'facebook', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][facebook]', $facebook, ['class' =>'form-group form-control']) ?>

					<?= Html::label('Twitter', 'twitter', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][twitter]', $twitter, ['class' =>'form-group form-control']) ?>

					<?= Html::label('Linkedin', 'linkedin', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][linkedin]', $linkedin, ['class' =>'form-group form-control']) ?>

					<?= Html::label('Instagram', 'instagram', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][instagram]', $instagram, ['class' =>'form-group form-control']) ?>


					<?= Html::label('Google+', 'google', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][google]', $google, ['class' =>'form-group form-control']) ?>

					<?= Html::label('YouTube', 'youTube', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][youTube]', $youTube, ['class' =>'form-group form-control']) ?>

					<?= Html::label('Pinterest', 'pinterest', ['class' => 'control-label']) ?>
					<?= Html::input('text', 'Configuration[key][pinterest]', $pinterest, ['class' =>'form-group form-control']) ?>

					<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>


				<?php ActiveForm::end(); ?>
			</div>
      </div>
      </div>
		</div>	

</div>  

<script type="text/javascript">
	$(document).ready(function() {
		$('.store-dropdown').change(function(){ 
			if(confirm("are you sure")){
				storeId = $(this).val();
				$.ajax({
		   		  url:"<?php echo Yii::$app->urlManager->createUrl(['users/setstore'])?>",	
		   		  data: {storeId: storeId}, 
		          success: function(data)
		                {
		                 	location.reload();
						}
				});
			}
			else{
				return false;
			}	
		});	
			//}
	});	

</script>