<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Storebanners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="storebanners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'storeId')->textInput() ?>

    <?= $form->field($model, 'bannerId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
