<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Storebanners */

$this->title = 'Create Storebanners';
$this->params['breadcrumbs'][] = ['label' => 'Storebanners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="storebanners-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
