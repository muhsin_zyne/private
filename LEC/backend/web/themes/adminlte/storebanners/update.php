<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Storebanners */

$this->title = 'Update Storebanners: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Storebanners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="storebanners-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
