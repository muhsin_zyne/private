<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Brands;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $supplier common\models\Suppliers */

$this->title = $supplier->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $supplier->firstname;
?>
<div class="suppliers-view">
<div class="box">
<div class="box-body">
    <p>
        <?= Html::a('Update', ['update', 'id' => $supplier->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $supplier->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $supplier,
        'attributes' => [
            [
                'attribute' => 'firstname',
                'label' => 'Supplier Name'
            ],
            'email',
        ],
    ]) ?>
    <h2>Brands associated with <?=$supplier->firstname?></h2>
    <?php $form = ActiveForm::begin(); ?>
    <?= Html::checkBoxList("Brands", ArrayHelper::map($supplier->brands, 'id', 'id'), ArrayHelper::map(Brands::find()->all(), 'id', 'title'), ['unselect' => 'no-suppliers-selected']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php $form = ActiveForm::end(); ?>
    </div>
    </div>
</div>