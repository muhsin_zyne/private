<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Suppliers */

$this->title = 'Create Suppliers';
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suppliers-create">
  <div class="box">
  <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    
    </div>

</div>
