<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Suppliers */

$this->title = 'Update Suppliers: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="suppliers-update">

    <div class="box">
    <div class="box-body">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    
    </div>
    </div>

</div>
