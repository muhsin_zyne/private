<?php



use yii\helpers\Html;

use yii\grid\GridView;



/* @var $this yii\web\View */

/* @var $searchModel common\models\search\SupplierSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Suppliers';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="suppliers-index">
<div class="box">
  <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if($adminUser->roleId == "1"){ ?>
    <p class="create_pdt">
      <?= Html::a('<i class="fa fa-plus"></i> Create Suppliers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php } ?>
    <?php if($adminUser->roleId == "3") { ?>
    <p class="submit_enabled">
      <?= Html::button('Update',['class'=>'btn btn-primary enabled']); ?>
    </p>
    <?php } ?>
    <?= \app\components\AdminGridView::widget([

        'dataProvider' => $dataProvider,

        'filterModel' => $searchModel,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],

            [

                'attribute' => 'firstname',

                'label' => 'Supplier Name'

            ],

            'email:email',

            ($adminUser->roleId == "3")? [

                            'label' => 'Enabled',

                            'format' => 'raw',

                            'value' => function ($model) use($enabledSuppliers){

                                return '<input type="checkbox" class="enabled_checkbox" value="'.$model->id.'" name="StoreSuppliers[enabled]" '.(in_array($model->id, \yii\helpers\ArrayHelper::map($enabledSuppliers, 'id', 'supplierUid')) ? "checked" : "not").'  >';

                            },

                        ] : [

                'class' => 'yii\grid\ActionColumn',

                'template' => '{update}',

                'buttons' => [

					// 'view' => function($url, $model){

     //                    return '<a href="'.\yii\helpers\Url::to(['suppliers/update', 'id' => $model->id]).'" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>';

     //                },
					
                    'update' => function($url, $model){

                        return '<a href="'.\yii\helpers\Url::to(['suppliers/update', 'id' => $model->id]).'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';

                    },

                    // 'delete' => function($url, $model){

                    //     return '<a href="'.\yii\helpers\Url::to(['suppliers/delete', 'id' => $model->id]).'" title="Delete" data-pjax="0"><span class="fa fa-trash"></span></a>';

                    // }

                ]

            ], 

        ],

    ]); ?>
  </div>
</div>
<script type="text/javascript">

$(document).ready(function() {



    var enabledSuppliers = [];

    $('.enabled_checkbox:checked').each(function(){

            enabledSuppliers.push(this.value);

        });


    $("body").on("click", ".enabled", function(e){

        var currentEnabledSuppliers = [];

        $('.enabled_checkbox:checked').each(function(){

            currentEnabledSuppliers.push(this.value);

        });
        //console.log('enabledBrands:'+enabledBrands);

        //console.log('currentEnabledBrands:'+currentEnabledBrands);

        

        $.ajax({

            url:'<?=\yii\helpers\Url::to(["suppliers/enable"])?>',

            type: 'POST',

            data: {currentEnabledSuppliers : currentEnabledSuppliers, enabledSuppliers : enabledSuppliers},

            success: function(data)

                {

                    

                      

                    /*console.log(data);

                    $(".error-msg").append('<div class="alert alert-success"><i class="icon fa fa-tick"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Product Disabled</div>');*/

                }

        });

    });     

}); 

</script> 
