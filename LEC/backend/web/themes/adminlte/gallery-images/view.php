<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Galleryimages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Gallery-images', 'url' => ['index']];
?>
<div class="galleryimages-view">
  <div class="box">
  <div class="box-body">
   

    <p >
        <?= Html::a('Back', ['index', 'id' => $_REQUEST['galleryId']], ['class' => 'btn btn-primary']) ?>
        
        
    </p>
   <div class="row">
   
     <div class="col-sm-3">
       <img src ='<?=Yii::$app->params["rootUrl"]?>/store/gallery/images/<?=$model->title?>' class="img-responsive">
     </div>
     <div class="col-sm-9 img-desc">
       <div class="img-head">Image Descrption</div>
       <?=$model->description?>
     </div>
   </div>
   
    
    </div>
    </div>
</div>
