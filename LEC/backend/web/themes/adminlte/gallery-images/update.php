<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Galleryimages */

$this->title = 'Update Gallery Images: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Gallery-images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gallery-images-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
