<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\GalleryImages;
use common\models\Gallery;

use yii\web\UploadedFile;
use yii\helpers\Json;
use kartik\widgets\ActiveForm; // or yii\widgets\ActiveForm
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Galleryimages */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gallery Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galleryimages-index">
    <?= Html::a('Back', ['/gallery/index'], ['class'=>'btn btn-primary']) ?>
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i>Upload image</h3>
        </div>  
        <div class="box-body">
            <div class="galleryimages-form">
                <?php 
                $model = new GalleryImages();
                $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data'] ]);
                // echo $form->field($model, 'image[]')->widget(FileInput::classname(), ['options'=>['accept'=>'image/*','multiple'=>true],
                // 'pluginOptions' => ['uploadUrl' => Url::to(['/gallery-images/create','id' => $galleryid]),]]);
                // //echo $form->field($model, 'title[]')->fileInput(['multiple' => true, 'accept' => 'image/*']);
               echo FileInput::widget(['model' => $model,'attribute' => 'title', 'options' => ['multiple' => true,'maxSize'=>2*1024*1024],
                'pluginOptions' => ['uploadUrl' => Url::to(['/gallery-images/create','id' => $galleryid]),] ]);
                ?>
               
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <?php $gallery_name= Gallery::find()->where( ['id' => $galleryid] )->one();?>
    <h2>Images In <?= $gallery_name['title']?></h2>
    <h5>Drag and drop the images to sort the order. It will automatically saved once you drop the item.</h5>    
    <h5>The maximum file size allowed is 5 MB. This is to ensure optimum performance of your website's gallery</h5>
    <ul id="sortable" class="row sm-img-container">          
        <?php                      
        $gallery_image_all= GalleryImages::find()->where( ['galleryId' => $galleryid] )->orderBy('position')->all(); 
        if(empty($gallery_image_all))
        { ?>
           <h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No Images Found </h5>
        <?php }
        else
        {           
            foreach ($gallery_image_all as $index => $galleryimage) 
            {
            ?>
                <li class="ui-state-default col-md-3" id="<?=$galleryimage['id']?>">
                    <div class="box">
                        <div class="gl-img-row">
                            <a href=""><img src ='<?=Yii::$app->params["rootUrl"]?>/store/gallery/images/<?=$galleryimage['title']?>' ></a>
                        </div>
                        <div class="btn-row-sm">
                            <?= Html::a('Delete', ['delete', 'id' => $galleryimage['id'],'galleryId' => $galleryid], 
                            ['class' => 'btn btn-info btn-sm','data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?>
                            <button id="<?=$galleryimage['id']?>" class="btn btn-info btn-sm btn1" >Description</button>
                        </div>
                           
                        <div class="btn-row-sm txt-btn">
                            &nbsp;<?php   echo substr($galleryimage['description'],0,20);?>
                                                  
                            <?php 
                            $more=strlen($galleryimage['description']);
                            if($more>20)
                            {
                                echo Html::a('More...', ['view', 'id' => $galleryimage['id'],'galleryId' => $galleryid]);
                            }
                            ?>
                            
                        </div>
                    </div>
                </li>  
            <?php  } ?> 
        <?php  } ?> 
    </ul>
         
</div>    

<script>
 $('body').on('click', '.btn1', function () {
        var dec = prompt("Enter Image Description", "");
        var image_id=this.id; 
        var gallery_id=<?= $galleryid?>;   
        if (dec != null) {
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['gallery-images/update'])?>",
                data: "&dec=" + dec + "&image_id=" + image_id + "&gallery_id=" + gallery_id,
                //dataType: "json",
                dataType: "html", 
                success: function (data) {
                //alert(data);   
                //$("#sortable").html(data);                
                }                
            });
        }
      });
$(document).ready(function(){
    $("#sortable" ).sortable({                  
        update: function (event, ui) { 
            var list =  $(this).sortable("toArray").join(",");            
            $.ajax({
                type: "POST",                
                url: "<?=Yii::$app->urlManager->createUrl(['gallery-images/sortable'])?>",                
                data: "&list=" + list ,                   
                dataType: "html", 
                success: function (data) {
                    //alert(data); 
                        
                }
            });
        }
    });
});
function refreshImages() { }
</script>
