<?php
/* @var $this yii\web\View */
use backend\components\Helper;
$this->title = "Reports - Invoices";
if(!$invoices instanceof \yii\data\ArrayDataProvider)
    echo('<p>No Data to Display</p>');
else{
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Filter</h3>
        </div>
        <div class="box-body">
        <div class="row">
        <?php $form = \kartik\form\ActiveForm::begin(['id' => 'products-form', 'options' => ['enctype'=>'multipart/form-data']]); ?>
        <div class="col-xs-9">
			<?=\kartik\field\FieldRange::widget([
                'form' => $form,
                'model' => $report,
                'label' => 'Enter start and end date',
                'attribute1' => 'start_date',
                'attribute2' => 'end_date',
                'type' => \kartik\field\FieldRange::INPUT_DATE,
            ]);?>
        </div>
        <div class="col-xs-3 sm-pd">
			<?= \yii\helpers\Html::button('<i class="fa fa-file-text-o"></i> Generate Report', ['class' => 'btn btn-primary report-generate']) ?>
            <?= \yii\helpers\Html::button('<i class="fa fa-upload"></i> Export Report', ['class' => 'btn btn-success report-export']) ?>
            <?php \kartik\form\ActiveForm::end(); ?>
        </div>
        </div>
        
        
<p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $invoices,
        'summary' => '',
        'columns' => [
        	[
               'label'=>'Date',
                'value'=>'period',
            ],
        	[
	        	'label' => 'Number of Orders',
	        	'value' => 'orderCount',
        	],
        	[
        		'label' => 'Number of Invoiced Orders',
        		'value' => 'invoicedOrdersCount'
        	],
        	[
        		'label' => 'Total Invoiced',
        		'value' => function($row){ return Helper::money($row['invoiced']); },
        		'format' => 'raw'
        	]
        ],
    ]); ?>
</p>
<?php } ?>
</div>
</div>
        </div><!-- /.box-body -->
</div>

