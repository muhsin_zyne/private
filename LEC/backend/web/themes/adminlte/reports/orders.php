<?php
/* @var $this yii\web\View */
use backend\components\Helper;
$this->title = "Reports - Orders";
if(!$orders instanceof \yii\data\ArrayDataProvider)
    echo('<p>No Data to Display</p>');
else{
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Filter</h3>
        </div>
        <div class="box-body">
        
        <div class="row">
        <?php $form = \kartik\form\ActiveForm::begin(['id' => 'products-form', 'options' => ['enctype'=>'multipart/form-data']]); ?>
          <div class="col-xs-8">
         <?=\kartik\field\FieldRange::widget([
            'form' => $form,
            'model' => $report,
            'label' => 'Enter start and end date',
            'attribute1' => 'start_date',
            'attribute2' => 'end_date',
            'type' => \kartik\field\FieldRange::INPUT_DATE,
        ]);?>
          </div>
          <div class="col-xs-4 sm-pd">
			<?= \yii\helpers\Html::button('<i class="fa fa-file-text-o"></i> Generate Report', ['class' => 'btn btn-primary report-generate']) ?>
            <?= \yii\helpers\Html::button('<i class="fa fa-upload"></i> Export Report', ['class' => 'btn btn-success report-export']) ?>
           </div>
           <?php \kartik\form\ActiveForm::end(); ?>
        </div>
       

        <!-- <div class="info-text" style="font-style:italic">Export feature coming soon..</div> -->

        
    
<p>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $orders,
        'summary' => '',
        'columns' => [
            [
        	   'label'=>'Date',
                'value'=>'period',
            ],    
           [
	        	'label' => 'Orders',
	        	'value' => 'totalCount',
        	],
        	[
        		'label' => 'Sales Items',
        		'value' => 'salesItemsCount'
        	],
        	[
        		'label' => 'Sales Total',
        		'value' => function($row){ return Helper::money($row['salesTotal']); },
        		'format' => 'raw'
        	],
        	// [
        	// 	'label' => 'Invoices',
        	// 	'value' => function($row){ return Helper::money($row['invoiced']); },
        	// 	'format' => 'raw'
        	// ],
        	[
        		'label' => 'Refunded',
        		'value' => function($row){ return Helper::money($row['invoiced']); },
        		'format' => 'raw'
        	],
        	[
        		'label' => 'Shipped',
        		'value' => function($row){ return Helper::money($row['shipped']); },
        		'format' => 'raw'
        	],
        	[
        		'label' => 'Discount',
        		'value' => function($row){ return Helper::money($row['discount']); },
        		'format' => 'raw'
        	]
        ],
    ]); ?>
</p>

</div><!-- /.box-body -->

</div>    

<?php } ?>
