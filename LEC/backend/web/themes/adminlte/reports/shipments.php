<?php

/* @var $this yii\web\View */

use backend\components\Helper;

$this->title = "Reports - Shipments";

if(!$shipments instanceof \yii\data\ArrayDataProvider)

    echo('<p>No Data to Display</p>');

else{

?>

<div class="box">

    <div class="box-header with-border">

        <h3 class="box-title">Filter</h3>

        </div>

        <div class="box-body">
          <div class="row">
            <?php $form = \kartik\form\ActiveForm::begin(['id' => 'products-form', 'options' => ['enctype'=>'multipart/form-data']]); ?>
         <div class="col-xs-8"> 
        <?=\kartik\field\FieldRange::widget([

            'form' => $form,

            'model' => $report,

            'label' => 'Enter start and end date',

            'attribute1' => 'start_date',

            'attribute2' => 'end_date',

            'type' => \kartik\field\FieldRange::INPUT_DATE,

        ]);?>
        </div>
        <div class="col-xs-4 sm-pd">

        <?= \yii\helpers\Html::button('<i class="fa fa-file-text-o"></i> Generate Report', ['class' => 'btn btn-primary report-generate']) ?>

        <?= \yii\helpers\Html::button('<i class="fa fa-upload"></i> Export Report', ['class' => 'btn btn-success report-export']) ?>

        <?php \kartik\form\ActiveForm::end(); ?>
        </div>
        </div>



<p>

    <?= \yii\grid\GridView::widget([

        'dataProvider' => $shipments,

        'summary' => '',

        'columns' => [

                [

                    'label'=>'Date',

                    'value'=>'period',

                ],

                [

                    'label' => 'Carrier(s)',

                    'value' => 'periodCarriers'

                ],

                [

                    'label' => 'Number of Orders',

                    'value' => 'shipmentCount',

                ],

                [

                    'label' => 'Total Sales Shipping',

                    'value' => function($row){ return Helper::money($row['totalSalesShipping']); },

                    'format' => 'html'

                ],

            ],

    ]); ?>

</p>

<?php } ?>

</div>
        </div><!-- /.box-body -->

</div>

</div>

