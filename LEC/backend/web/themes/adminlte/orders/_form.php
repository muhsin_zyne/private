<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'storeId')->textInput() ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'b2c' => 'B2c', 'b2b' => 'B2b', 'conference' => 'Conference', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'customerId')->textInput() ?>

    <?= $form->field($model, 'billingAddressId')->textInput() ?>

    <?= $form->field($model, 'shippingAddressId')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_firstname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_lastname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_company')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_street')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'shipping_city')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_state')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'shipping_postcode')->textInput() ?>

    <?= $form->field($model, 'shipping_country')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_phone')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shipping_fax')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_firstname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_lastname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_company')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_street')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'billing_city')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_state')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_postcode')->textInput() ?>

    <?= $form->field($model, 'billing_country')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_phone')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'billing_fax')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'shippingMethod')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'discountAmount')->textInput(['maxlength' => 16]) ?>

    <?= $form->field($model, 'discountDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'subTotal')->textInput(['maxlength' => 16]) ?>

    <?= $form->field($model, 'shippingAmount')->textInput(['maxlength' => 16]) ?>

    <?= $form->field($model, 'grandTotal')->textInput(['maxlength' => 16]) ?>

    <?= $form->field($model, 'customerNotes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'orderDate')->textInput() ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
