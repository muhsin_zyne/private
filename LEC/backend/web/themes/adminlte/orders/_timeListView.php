<?php //var_dump($model->b2bTradingHoursFormatted)
 $h=$model->b2bTradingHoursFormatted;
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="text-align:center;font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;font-size:14px;">
    <thead style="background:#5b5b5b;color:#fff;font-size:15px;">
      <tr>
        <th style="padding:7px 3px;font-weight:normal;">MON</th>
        <th style="padding:7px 3px;font-weight:normal;">TUE</th>
        <th style="padding:7px 3px;font-weight:normal;">WED</th>
        <th style="padding:7px 3px;font-weight:normal;">THU</th>
        <th style="padding:7px 3px;font-weight:normal;">FRI</th>
        <th style="padding:7px 3px;font-weight:normal;">SAT</th>
        <th style="padding:7px 5px;font-weight:normal;background:#5b5b5b;">SUN</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['monday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['monday']['to']?>
        </td>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['tuesday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['tuesday']['to']?>
        </td>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['wednesday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['wednesday']['to']?>
        </td>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['thursday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['thursday']['to']?>
        </td>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['friday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['friday']['to']?>
        </td>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['saturday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['saturday']['to']?>
        </td>
        <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;background:#f6f6f6;">
            <?= $h['0']['sunday']['from']?><br>
            <img src="<?=Yii::$app->params["rootUrl"]?>/store/site/email/to.png"><br>
            <?= $h['0']['sunday']['to']?>
        </td>
      </tr>
    </tbody>
</table>
                  
                  

