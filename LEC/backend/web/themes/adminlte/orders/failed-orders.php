<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use common\models\Stores;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Failed Transaction Orders';
$this->params['breadcrumbs'][] = $this->title;
$stores=Stores::find()->where(['isVirtual'=>0])->all();
$listData=ArrayHelper::map($stores,'id','title');
$user = User::findOne(Yii::$app->user->id);
//var_dump($user->roleId);die;
?>
<div class="box">
<div class="box-body">
<div class="orders-index sm-tb">
    <?php
        $form = \kartik\form\ActiveForm::begin(['id' => 'b2borders-form']);
        \kartik\form\ActiveForm::end();
    ?>
    <?php \yii\widgets\Pjax::begin(['id' => 'recent-products',]); ?>
    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'   => function ($model, $key, $index, $grid) {
            return ['data-id' => $model->id];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'storeName',
            
            [   
                'label' => 'Store Name',
                'attribute' => 'storeId',
                'visible'=>($user->roleId==1)?true:false,
                'value' => function($model, $attribute){ 
                    return $model->store->title;
                },
                
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'storeId', $listData,
                    ['class'=>'form-control','prompt' => '']),
            ],
            // [   
            //     'label' => 'Store Name',
            //     //'attribute' => 'storeId',
            //     'visible'=>($user->roleId!=1)?true:false,
            //     'value' => function($model, $attribute){ 
            //         return $model->store->title;
            //     },
            // ],
            // 'orderDate',
            // [
            //      'label' => 'Order Date',
            //      'attribute' => 'orderDate',
            //      'format' => 'html',
            //      'value' => function ($model) {
            //          return Helper::date($model->orderDate, "d/m/Y, h:i A");
            //      },
            //      'filter'=>\kartik\date\DatePicker::widget([
            //         'model' => $searchModel,
            //         'size' => 'xs',
            //         'attribute' => 'orderDate',
            //         'pluginOptions' => [
            //             'format' => 'yyyy-mm-dd',
            //             'autoclose' => true,
            //         ],
            //     ]),                 
            //  ],
            [
                'label' => 'Order Date',
                'attribute' => 'id',
                'value' => function($model){ return \backend\components\Helper::date($model->orderDate); },
                'filter' => \kartik\field\FieldRange::widget([
                        'form' => $form,
                        'model' => $searchModel,
                        'template' => '{widget}{error}',
                        'attribute1' => 'orderDate_start',
                        'attribute2' => 'orderDate_end',
                        'type' => \kartik\field\FieldRange::INPUT_DATE,
                ]),
                'headerOptions' => ['class' => 'date-range']
            ],
            //'billingAddressText',
            [
                'label' => 'Billing Address',
                'format' => 'html',
                'attribute' => 'billingAddressText',
                'value' => 'billingAddressText',
               
            ],
            [
                'label' => 'Shipping Address',
                'format' => 'html',
                'attribute' => 'orderDeliveredAddress',
                'value' => 'orderDeliveredAddress',
               
            ],
            //'shippingAddressText',
            [
                'label' => 'Grand Total',
                'attribute' => 'grandTotal',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::money($model->grandTotal);
                },
            ],
            [
                'label' => 'Status',
                'attribute' => 'status',
                'value' => function($model, $attribute){ 
                    return ucwords($model->status);
                },
                /*'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['pending' => 'Pending', 'processing' => 'Processing',
                    'complete' => 'Complete', 'closed' => 'Closed','hold' => 'Hold', 'cancelled' => 'Cancelled', 'payment pending' => 'Payment Pending',],
                    ['class'=>'form-control','prompt' => '']),*/
            ],
            [
                'label'=> 'View',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('View',['orders/view','id' => $model->id]);
                }
            ],
            /*[
                'label'=> 'Refund',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('Refund',['orders/refund','id' => $model->id]);
                }
            ],*/
        ],
    ]); ?>
    <?php  \yii\widgets\Pjax::end();  ?> 
<?php
    /*$this->registerJs("
    $('td').click(function (e) {
        var id = $(this).closest('tr').data('id');
        if(e.target == this)
            location.href = '" . Url::to(['orders/view']) . "?id=' + id;
        });
    ");*/
?>
</div>
</div>
</div>
