<?php
	use yii\helpers\Html;
  	use frontend\components\Helper;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();
?>

<tr>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$index +1?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;">
    <img src="<?=$thumbImagePath?>" width="66px"></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
      <?=Helper::stripText($model->product->name,15) ?><br>
        <span style="color:#e31837;"><?=Helper::money($model->price)?></span>
    </td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$model->quantity?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=Helper::money($model->discount)?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;">
    <?=Helper::money($model->giftWrapAmount*$model->quantity)?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;">
    <?=Helper::money($model->itemSubTotal)?></td>
</tr>

