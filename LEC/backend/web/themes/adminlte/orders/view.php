<?php
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;
use common\models\OrderComment;
use common\models\B2bAddresses;
use common\models\SalesComments;
use common\models\OrderItems;
use dosamigos\ckeditor\CKEditor;
$this->title = 'Order #'.$model->orderId.' | '. $model->orderPlacedDate;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-xs-12"> 
        <div class="ordr-sts">Order Status : <span><?= $model->formattedStatus ?></span></div>
        <div style="float:right; margin-bottom:15px;">        
            <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/index'], ['class' => 'btn btn-default back-btn ']) ?>
            <?php if($model->status!="failed"){ ?>
                <?= Html::a('<i class="fa fa-envelope"></i> Resend Order Confirmation', ['sendmail','id'=>$model->id],['class' => 'btn btn-primary hold','data' => ['confirm' => 'Are you sure you want to resend the order confirmation email to customer?','method' => 'post',]]) ?> 
                <?php if($model->hasOrderCollection()){ ?>
                    <?= Html::a('<i class="fa fa-shopping-basket"></i> Order Collection', ['delivery/create','orderid'=>$model->id], ['class' => 'btn btn-success hold']) ?> 
                <?php } ?>  
            <?php } ?>
        </div>
    </div>
</div>

    
<div class="box">
    <div class="categories-index">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs sm-orders ">
                <li class="active"><a data-toggle="tab" href="#information"> <i class="glyph-icon flaticon-icon-1176"></i>Information</a></li>
                <li id="m_title" ><a data-toggle="tab" href="#invoices"><i class="glyph-icon flaticon-file"></i>Copy of Invoice</a></li>
                <li id="m_title" ><a data-toggle="tab" href="#send-message"><i class="glyph-icon flaticon-multimedia"></i>Send Message</a></li>
                <li id="m_title" ><a data-toggle="tab" href="#history"><i class="glyph-icon flaticon-clock"></i>History</a></li>
                <li class="m_title"><a data-toggle="tab" href="#deliveries"><i class="glyph-icon flaticon-approve-invoice"></i>Collection Advice (<?=$model->collectionCount?>)</a></li>
                <?php if($model->hasCreditMemo()){?>
                <li id="m_title" ><a data-toggle="tab" href="#credit_memos"><i class="glyph-icon flaticon-icon-1176"></i>Credit Memos</a></li>
                <?php } ?>               
            </ul>
        </div>
    </div>

    <div class="box-body">
    <div class="row">
    <div class="col-xs-12">
    <div class="tab-content responsive">
        <div class="tab-pane active" id="information">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-user"></i> 
                            <h3 class="box-title">Order Detail</h3>
                        </div>
                        <div class="box-body">
                            <p>Order Number: <?= $model->orderId ?> </p>
                            <p>Order Date: <?= $model->orderPlacedDate ?></p>
                            <p>Order Total: <?= Helper::money($model->grandTotal) ?> </p>
                        </div><!-- /.box-body --> 
                    </div><!-- /.box --> 
                </div>

                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-user"></i> 
                            <h3 class="box-title">Customer Details</h3>
                        </div>                      
                        <div class="box-body">
                            <div class="col-md-6">
                                <p class="linead"> <?= $model->billingAddressText ?></p>
                            </div>
                            <div class="col-md-6 cntct-icon">
                                <div class="inner-title">
                                    Contact Preference
                                </div>
                                <p> <i class="glyph-icon flaticon-multimedia"></i> <?= $model->customer->email ?> </p>
                                <p> <i class="glyph-icon flaticon-phone-call"></i> <?php echo  isset($model->billingAddressId)?$model->billingAddress->telephone:$model->billing_phone ?>  </p>
                            </div>
                        </div><!-- /.box-body -->                      
                    </div><!-- /.box -->
                </div>
            </div>                       

            <div class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-usd"></i>
                            <h3 class="box-title">Payment Information</h3>
                        </div>
                        <div class="box-body">
                            <?= $model->paymentInformation?>
                        </div><!-- /.box-body --> 
                    </div><!-- /.box --> 
                </div>
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
                            <h3 class="box-title">Collect From Store</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <p class="lineads"> <?= $model->orderDeliveredAddress ?></p>
                        </div><!-- /.box-body --> 
                    </div><!-- /.box --> 
                </div>                 
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
                            <h3 class="box-title">Item/s to Process (Confirm Availability)</h3>
                            <a href="#" class="view-order">View the entire order</a>
                        </div>
                        <div class="box-body">
        
                            <?php 
                             $form = ActiveForm::begin(['action' =>['orders/update','id'=> $model->id], 'id' => 'order-form','class'=>'bootstrap-modal']);
                            ?>
                            <?= GridView::widget([    
                                'id' => 'order-items', 
                                'summary' => false,   
                                'dataProvider' => $dataProvider,
                                'emptyText' => 'No More Items to Process',
                                'columns' => [    
                                    ['class' => 'yii\grid\SerialColumn'], 
                                    //'id',    
                                    [    
                                        'label'=> 'Item Orderd',    
                                        'attribute' => 'id', 
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            return $model->productDetails;               
                                        },
                                    ],    
                                    [    
                                        'label'=> 'SKU',    
                                        'attribute' => 'productId',    
                                        'value' => 'sku'    
                                    ], 
                                    
                                    [    
                                        'label' => 'Price',    
                                        'attribute' => 'id',    
                                        'format' => 'html',    
                                        'value' => function ($model) {    
                                            return Helper::money($model->price);    
                                        },    
                                    ],
                                    [    
                                        'label' => 'Qty To Orderd',    
                                        'attribute' => 'quantity',    
                                        'format' => 'html',    
                                        'value' => function ($model) {    
                                            return $model->quantity;    
                                        },    
                                    ],
                                    [    
                                        'label' => 'Qty Available',    
                                        //'attribute' => 'qtyReadyForDelivery',    
                                        'format' => 'raw',    
                                        'value' => function ($model) {    
                                            return $model->qtyDelivery.
                                            '<input type="hidden" class="qta" id="q'.$model->id.'" value="'.$model->qtyAvailable.'">';    
                                        },    
                                    ], 
                                    [    
                                        'label' => 'Ready',    
                                        //'attribute' => 'qtyReadyForDelivery',    
                                        'format' => 'raw',    
                                        'value' => function ($model) {
                                            $status= $model->order->CreditMemoCheck?'':'disabled';
                                            return '<input type="checkbox" name="chk_'.$model->id.'" '.$status.' class="ready" id="'.$model->id.'" >';    
                                        },    
                                    ],
                                ],

                            ]); ?>
                           
                            <?php ActiveForm::end(); ?> 
                            <p class="bottom-p">Update Client : By clicking on the appropriate tile below you will be able to update the customer with the status of their order.  </p>
                        </div><!-- /.box-body --> 
                        
                    </div>
                </div>
            </div>
            <?php echo $tid='<span id="pc"></span>'; ?>

            <?php if($model->status!="failed"){ ?>
                <div class="row control-btns">
                    <div class="col-md-3"> <!-- bootstrap-modal -->
                         <?= Html::a('<i class="glyph-icon flaticon-verified-text-paper"></i> <span>Ready for </br>Collection</span>', ['orders/update','id'=>$model->id,'type'=>'fc'], ['class' => 'bootstrap-modal btn btn-default','id'=>'full-collection']) ?>
                    </div>
                     <div class="col-md-3">
                         <?= Html::a('<i class="glyph-icon flaticon-check-1"></i><span>Partially Ready </br>for Collection</span>', ['orders/update','id'=>$model->id,], ['class' => 'bootstrap-modal btn btn-default back-btn ','id'=>'partially-collection']) ?>
                    </div>
                     <div class="col-md-3">
                         <a id="order-product" class="btn btn-default back-btn" ><i class="glyph-icon flaticon-technology"></i><span>Need to </br>order product</span></a>
                    </div>
                    <div class="col-md-3">
                        <?php 
                        $user = User::findOne(Yii::$app->user->id);
                        if($model->CreditMemoCheck && $user->roleId==3) { ?>
                        <?= Html::a('<i class="glyph-icon flaticon-money"></i><span>Refund </br>Order</span>', ['credit-memos/create','orderid'=>$model->id], ['class' => 'bootstrap-modal btn btn-default back-btn','data-parameters'=>'{"noFlash":true}','id'=>'refund-order']) ?>
                        <?php }
                        else { ?> 
                        <?= Html::a('<i class="glyph-icon flaticon-money"></i><span>Refund </br>Order</span>', ['credit-memos/create','orderid'=>$model->id], ['class' => 'bootstrap-modal btn btn-default back-btn','id'=>'refund-order','disabled'=>'disabled']) ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            
        </div>
        <!--  invoices -->              
        <div class="tab-pane" id="invoices">
            <div class="row">
                <div class="col-md-12">
                    <div class="print-pdf">
                    <?php if(isset($invoice)){ ?>
                    <?=$invoice->printInvoice()?>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
         <!--  send message -->
        <div class="tab-pane" id="send-message">
            <div class="order-comment-form">
                <h3 class="msg-head">Please type below to send a message to <span><?=$model->customer->fullName?></span></h3>
                <?php $form = ActiveForm::begin(['action' =>  Url::to(['sales-comments/create'])]); 
                    $model_oc= new SalesComments();                                
                    $model_oc->typeId=$model->id; 
                    $model_oc->type='order';
                    $model_oc->orderId=$model->id;
                    $model_oc->notify=1; ?>
                    <?= $form->field($model_oc, 'type')->hiddenInput()->label(false)  ; ?>
                    <?= $form->field($model_oc, 'typeId')->hiddenInput()->label(false)  ; ?>
                    <?= $form->field($model_oc, 'orderId')->hiddenInput()->label(false)  ; ?>
                    <?= $form->field($model_oc, 'comment')->widget(CKEditor::className(),[ 
                        'id'=>'cmscontent',
                        'preset' => 'basic', 
                        'clientOptions' =>[
                            'language' => 'en', 
                            'allowedContent' => true,
                            //'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['cms/image-upload']),
                            //'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['cms/image-browse']),
                            ]
                    ]) ?> 
                    <!--<?= $form->field($model_oc, 'comment')->textarea(['rows' => 6])->label('Update your customer with order status') ?>-->
                    <?= $form->field($model_oc, 'notify')->checkbox(); ?>
                    <div class="form-group">
                        <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-primary']) ?>
                    </div>                          
                <?php ActiveForm::end(); ?>
                <h3 class="p-m-head">Previous Messages </h3>
                <?= ListView::widget([    
                    'dataProvider' => $dataProvider_salescomments,    
                    //'filterModel' => $searchModel_ordercomment,    
                    'itemView' => '_listview',
                    'summary'=>'',
                    'layout' => "{items}",    
                ]); ?>                            
            </div>
        </div>
         <!--  history -->
        <div class="tab-pane" id="history">
            <ul class="timeline">
            <?php 
            $previousDate = "";
            if(empty($model->activity)){
                echo "No activity to show.";
            }
            foreach($model->activity as $action){ ?>

                <?php if($previousDate != Helper::localDate($action->date, 'Y-m-d')){ 
                        $previousDate = Helper::localDate($action->date, 'Y-m-d');
                    ?>
                    <!-- timeline time label -->
                        <li class="time-label">
                            <span class="bg-red">
                                <?=Helper::localDate($action->date, 'd M. Y')?>
                            </span>
                        </li>
                    <!-- /.timeline-label -->
                    <?php } ?>
                <!-- timeline item -->
                <li>
                    <!-- timeline icon -->
                    <i class="fa <?=$action->iconClassNames[$action->type]?> <?=$action->iconBackgrounds[$action->type]?>"></i>
                    <div class="timeline-item">
                        <!-- <span class="time"><i class="fa fa-clock-o"></i> </span> -->
                        <div class="timeline-body">
                            <?=$action->description;?>&nbsp;<i class="time-item"><?=Helper::localDate($action->date, 'd-M-Y \a\t h:i a')." AEST."?></i>
                        </div>
                    </div>
                </li>
                <!-- END timeline item -->
                <?php } ?>
            </ul>
        </div>
        <!--  credit memos -->
  
        <div class="tab-pane" id="credit_memos">
            <?= GridView::widget([
                'id' => 'recent-products',
                'dataProvider' => $dataProvider_creditmemos,
                'columns' => [    
                    ['class' => 'yii\grid\SerialColumn'], 
                    //'id',    
                    
                    [        
                        'label'=> 'Created Date',        
                        'attribute' => 'createdDate',        
                        //'format' => 'html',        
                        'value' => 'creditmemoPlacedDate'        
                    ],
                    
                    [        
                        'label'=> 'Status',        
                        'attribute' => 'status',        
                        'format' => 'html',        
                        'value' => function($model){        
                            return ucfirst($model->status);        
                        }        
                    ],
                    [        
                        'label'=> 'Refunded',        
                        'attribute' => 'id',        
                        'format' => 'html',        
                        'value' => function($model){        
                            return Helper::money($model->grandTotal);        
                        }        
                    ],    
                    [        
                        'label'=> 'View',        
                        'format' => 'html',        
                        'value' => function($model){        
                            return Html::a('View',['credit-memos/view','id' => $model->id]);        
                        }        
                    ],    
                ],    
            ]); ?>
        </div>
  
        <!-- shipment  -->
  
        <!--<div class="tab-pane" id="shipments">
        <?php /* GridView::widget([

                'dataProvider' => $dataProvider_shipment,

                //'filterModel' => $searchModel_shipment,

                'columns' => [

                    ['class' => 'yii\grid\SerialColumn'],



                    'id',

                    [

                        'label'=> 'Ship to Address',

                        'format' => 'html',

                        'attribute' => 'orderId',

                        'value' => 'order.ShippingAddressText'

                    ],

                    [

                        'label'=> 'Date Shipped',

                        'attribute' => 'id',

                        'value' => 'shipmentPlacedDate'

                    ],

                    //'createdDate',

                    //'orderItemCount',

                    [

                        'label'=> 'Total Qty',

                        'attribute' => 'id',

                        'value' => 'orderItemCount'

                    ],

                    [

                        'label'=> 'View',

                        'format' => 'html',

                        'value' => function($model){

                            return Html::a('View',['shipments/view','id' => $model->id]);

                        }

                    ],

                ],

            ]); */ ?>
        </div> -->

        <!-- Deliveries  -->
  
        <div class="tab-pane" id="deliveries">


            <div class="row">
                <?php 
                    if(count($deliveries) > 0) {
                        foreach ($deliveries as $delivery) { 
                ?>
                    <div class="col-md-6">
                        <div class="delivery-box">
                            <div class="box-header with-border"><i class="fa fa-file-pdf-o"></i>
                                <h3 class="box-title">A Collection Advice was generated on <?=$delivery->deliveryPlacedDate?></h3><br>
                                <div class="box-body">
                                    <a href="<?=Url::to(['delivery/pdf','id'=>$delivery->id])?>" target="_blank">Click here </a>to download PDF
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } }
                else { ?>
                    <div class="no-content">
                        <i class="glyph-icon flaticon-cross"></i>
                        <div class="collection-head">No Collection Advice available</div>
                        <div class="collection-subhead">Please process order to generate</div>
                        </div>
                <?php } ?>    
            </div>    
                    

           <?php /* GridView::widget([
            'id' => 'recent-products',    
            'dataProvider' => $dataProvider_deliveries,    
            //'filterModel' => $searchModel_shipment,    
            'columns' => [    
                ['class' => 'yii\grid\SerialColumn'],
                //'id',    
                
                [    
                    'label'=> 'Collection Date',
                    'attribute' => 'deliveryPlacedDate',    
                    'value' => 'deliveryPlacedDate'    
                ],                               
                [    
                    'label'=> 'Verified By',
                    'attribute' => 'verifiedBy',    
                    //'value' => 'deliveryPlacedDate'    
                ],
                'collectedBy',
                'idProofNumber',
                'phone',
                'deliveryItemCount',
                
                //'deliveryPlacedDate',
                [    
                    'label'=> 'View',    
                    'format' => 'html',    
                    'value' => function($model){    
                        return Html::a('View',['delivery/view','id' => $model->id]);    
                    }    
                ],    
            ],

        ]); */ ?>



        </div>
        <!-- comment_history  -->
        </div>
    </div><!--tab-content responsive -->    
    </div><!--col-xs-12-->
    </div><!--row-->
    </div><!--box-body -->
</div>
<script type="text/javascript">
    $(document).ready(function(){ 
        var url= '/orders/update?id='+<?=$model->id?>;  
        //console.log(url) ;        
        // disable buttons  
        $("#full-collection").attr("disabled", true);
        $("#partially-collection").attr("disabled", true);
        //$("#order-product").attr("disabled", true);
        //$("#refund-order").attr("disabled", true); 

        $('.ready').on('ifChanged', function(e){  // check box change function  
            //alert('check box change');
            $('#partially-collection').attr("href", url + "&" + $('#order-form').serialize());            
            if($(this).is(':checked')){ 
                if($('.ready:checked').length == $('.ready').length){
                    var sel_status=0;  
                    $(".select-qrfd").each(function(){ 
                        var id=this.id;
                        var qtyReadyForDelivery=$("#q"+id).val();
                        var qtySelected=$("#"+id+" option:selected").val();
                        if(qtyReadyForDelivery!=qtySelected){
                            sel_status=1;
                        }  
                    });
                    if(sel_status==0){
                        $("#full-collection").attr("disabled", false);
                        $("#partially-collection").attr("disabled", true);                         
                    } else if($("#"+this.id).val()!=0){
                        $("#partially-collection").attr("disabled", false);
                    }else{
                        $("#full-collection").attr("disabled", true);
                    }
                } else{
                    var id=this.id;
                    var currentSelectedQty=$("#"+id+" option:selected").val();
                    if(currentSelectedQty!=0){
                        $("#partially-collection").attr("disabled", false);
                    } else {
                        $(".select-qrfd").each(function(){ 
                            var currentSelectedQty=0;
                            var id=this.id;
                            var qtySelected=$("#"+id+" option:selected").val();
                            if(qtySelected!=0){
                               currentSelectedQty=1; 
                            }  
                        });
                        if(currentSelectedQty==1){
                           $("#partially-collection").attr("disabled", false); 
                        }  else {
                            $("#partially-collection").attr("disabled", true); 
                        }

                    }
                    $("#full-collection").attr("disabled", true);
                }
            } else{ 
                if($('.ready:checked').length == 0){
                    $("#partially-collection").attr("disabled", true);
                } else{
                    $("#partially-collection").attr("disabled", false);
                }       
                $("#full-collection").attr("disabled", true);
            }                  
        });
        
        $('select').on('change', function() { // select box change function 
            $('#partially-collection').attr("href", url + "&" + $('#order-form').serialize()); 
            var sid=this.id;
            var currentSelectedQty=$("#"+sid+" option:selected").val(); 
            if(currentSelectedQty!=0){
                $("#partially-collection").attr("disabled", false);
                var sel_status=0;
                $(".select-qrfd").each(function(){ 
                    var id=this.id;
                    var qtyReadyForDelivery=$("#q"+id).val();
                    var qtySelected=$("#"+id+" option:selected").val();
                    if(qtyReadyForDelivery!=qtySelected){
                        sel_status=1;
                    } 
                });

                if($('.ready:checked').length == $('.ready').length && sel_status!=1 ){
                    $("#partially-collection").attr("disabled", true);
                    $("#full-collection").attr("disabled", false);
                }else if($('.ready:checked').length == 0){
                    $("#partially-collection").attr("disabled", true);
                    $("#full-collection").attr("disabled", true);
                } else {
                    $("#partially-collection").attr("disabled", false);
                    $("#full-collection").attr("disabled", true);
                }

            }
            else{ 
                var sel_status=0;
                $(".select-qrfd").each(function(){ 
                    var id=this.id;
                    var qtySelected=$("#"+id+" option:selected").val();
                    if(qtySelected!=0){
                        sel_status=1;
                    } 
                });
                if($('.ready:checked').length==1 && sel_status==0){
                    $("#full-collection").attr("disabled", true);
                    $("#partially-collection").attr("disabled", true);
                } else if($('.ready:checked').length >= 1 && sel_status==1 ){
                    $("#full-collection").attr("disabled", true);
                    $("#partially-collection").attr("disabled", false);
                } else if($('.ready:checked').length == 0 && sel_status==1 ) {
                    $("#full-collection").attr("disabled", true);
                    $("#partially-collection").attr("disabled", true);
                } else{
                    $("#partially-collection").attr("disabled", true);
                }

                
            }
           
        });
        <?php  if($model->status=="fully-ready-for-collection" || $model->status=="fully-collected" || $model->status=="fully-refunded" ){ ?>
            $("#order-product").attr("disabled", true);
        <?php } ?>
    }); 
</script>

<script type="text/javascript">
    $(document).ready(function(){ 
        $('#order-product').click(function(){
            $('.nav-tabs a[href="#send-message"]').tab('show');
        });

        //---------------------  need to order product ------------------------------------
        $('.ready').on('ifChanged', function(e){
            var sel_status=0;
            $(".select-qrfd").each(function(){ 
                var id=this.id;
                var qtyReadyForDelivery=$("#q"+id).val();
                var qtySelected=$("#"+id+" option:selected").val();
                if(qtyReadyForDelivery!=qtySelected){
                    sel_status=1;
                } 
            });
            if($('.ready:checked').length == $('.ready').length && sel_status!=1){
               $("#order-product").attr("disabled", true);
            } else {
                $("#order-product").attr("disabled", false);
            }
        });
        $('select').on('change', function() {
            var sel_status=0;
            $(".select-qrfd").each(function(){ 
                var id=this.id;
                var qtyReadyForDelivery=$("#q"+id).val();
                var qtySelected=$("#"+id+" option:selected").val();
                if(qtyReadyForDelivery!=qtySelected){
                    sel_status=1;
                } 
            });
            if($('.ready:checked').length == $('.ready').length && sel_status!=1){
               $("#order-product").attr("disabled", true);
            } else {
                $("#order-product").attr("disabled", false);
            }
        });
        //---------------------  need to order product  end------------------------------------
        $('.view-order').click(function(){
            $('a[href="#invoices"]').click();
        });
    }); 
</script>