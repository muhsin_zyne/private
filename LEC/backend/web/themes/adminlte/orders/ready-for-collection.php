<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use frontend\components\Helper;
use common\models\SalesComments;
$this->title = 'Ready For Collection';
?>

<div class="modal-dialog modal-lg quick-view-wrapper portal-wrapper" role="document">
   <div class="box order-popup-bg">
        <div class="box-body">        
            <?php $form = ActiveForm::begin(['id'=>'client_form','enableAjaxValidation' => false,'enableClientValidation' => true, 'validateOnType' => true, 'validationDelay' => '0']); 
            $model_oc= new SalesComments();  ?>
            <h3 class="poporder-head"><?= $this->title?></h3>
            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
             <?= $form->field($model, 'id')->hiddenInput()->label(false)  ; ?>
             <div class="row">
            <div class="col-xs-12 popup-table ">                   
                <?= GridView::widget([    
                    'id' => 'order-items',    
                    'dataProvider' => $dataProvider,
					'summary' => false,
                    'columns' => [    
                        ['class' => 'yii\grid\SerialColumn'], 
                        //'id',    
                        [    
                            'label'=> 'Item Orderd',    
                            'attribute' => 'id', 
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->productDetails;
                            },
                        ],    
                        [    
                            'label'=> 'SKU',    
                            'attribute' => 'productId',    
                            'value' => 'sku'    
                        ], 
                        
                        [    
                            'label' => 'Price',    
                            'attribute' => 'id',    
                            'format' => 'html',    
                            'value' => function ($model) {    
                                return Helper::money($model->price);    
                            },    
                        ],
                        [    
                            'label' => 'Qty To Orderd',    
                            'attribute' => 'quantity',    
                            'format' => 'html',    
                            'value' => function ($model) {    
                                return $model->quantity;    
                            },    
                        ],
                        [    
                            'label' => 'Qty Ready For Collection',    
                            'attribute' => 'qtyReadyForDelivery',    
                            'format' => 'raw',  
                            'visible'=>($type!='fc')?true:false,  
                            'value' => function ($model) { 
                                return '<span id="qa_'.$model->id.'" class="qtyavailable"> </span>'.
                                '<input type="hidden" class="" id="oi'.$model->id.'" name="OrderItems['.$model->id.'][qrfd]" value="'.$model->qtyAvailable.'">'; 
                            },    
                        ], 
                        [    
                            'label' => 'Qty Ready For Collection',    
                            'attribute' => 'qtyReadyForDelivery',    
                            'format' => 'raw',  
                            'visible'=>($type=='fc')?true:false,  
                            'value' => function ($model) { 
                                return '<span class="qtyavailable">'.$model->qtyavailable.
                                '</span><input type="hidden" class="" id="oi'.$model->id.'" name="OrderItems['.$model->id.'][qrfd]" value="'.$model->qtyAvailable.'">'; 
                            },    
                        ],                          
                        
                       
                    ],

                ]); ?> 
            </div>
            </div>
            
            <div class="row pop-btm">
            
            <div class="col-xs-9">
                <?= $form->field($model_oc, 'readyComment')->textarea(array('placeholder' => 'If you would like to send a personalised message to the customer please type here'))->label(false) ?>
            </div>
            <div class="col-xs-3">
                <?php $model_oc->notify=1;?>
                <?= $form->field($model_oc, 'notify')->checkbox(); ?>
                <?= Html::submitButton('SUBMIT', ['class' => 'btn pdt-cart pop-submit sm-btn']) ?>
            </div>
            </div>
            <?php ActiveForm::end(); ?>
            
        </div>       
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){ 
       <?php foreach ($oi_qrfd as $key => $value) {
            if($key!=0) { ?>
                var id=<?=$key?>;
                var value=<?=$value?>;
                $('#qa_'+id).text(value);
                 $('#oi'+id).val(value);
            <?php } 
        }?>
		
         $("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            });
    });
	
	
</script>