<?php
	use yii\helpers\Html;
  	use frontend\components\Helper;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();
?>

<tr>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$index +1?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;"><img src="<?=$thumbImagePath?>" width="66px"></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
     <?=Helper::stripText($model->product->name,15) ?><br>
        <span style="color:#e31837;"><?=Helper::money($model->price)?></span>
    </td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$model->quantity?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#5e5e5e;line-height:21px;font-size:14px;">
          <?=$model->qtyStatus; ?>
        </table>
    </td>
  </tr>

