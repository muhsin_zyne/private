<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AttributeSets;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Json;
use frontend\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<?php  //var_dump($orderItem->product->brand->supplier->email);die(); ?>

<?php  

    $orderItem->message = 'Dear '.$orderItem->product->supplierName.', <br/><br/>

An online sale has been made on our website for which we do not currently have stock on hand. So that we may fulfil our delivery committment to our customer, we request the following item(s) be picked and shipped to us as  soon as possible. <br/><br/>

Order No: # '.$orderItem->orderId.'<br/>
Date: '.$orderItem->order->orderDate.'<br/>
Delivery Window to Customer: 5 - 7 Days. <br/><br/>

<b>Product Details</b> <br/>

Product Name: '.$orderItem->product->name.' <br/>
Cost Price: '.Helper::money($orderItem->product->cost_price).' <br/>
SKU: '.$orderItem->product->sku.' <br/>
Qty: '.$orderItem->quantity.' <br/><br/>


<b>Ship to</b>  <br/>

'.Yii::$app->user->identity->store->title.'<br/>
Member: '.Yii::$app->user->identity->jenumber.'<br/>
'.Yii::$app->user->identity->defaultB2bAddress->shippingAddress.'  <br/>

P: '.Yii::$app->user->identity->store->phone.' <br/>
E: '.Yii::$app->user->identity->store->email.' <br/><br/>


In case of any issues/concerns, or if you require further information, please contact us by phone on '.Yii::$app->user->identity->store->phone.' as soon as possible.


<br/><br/>

Thank you! <br/>';
?>



<div class="product-settings-form order_from_supplier">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
    	<div class="col-xs-6">
      		<?= $form->field($orderItem, 'storeMailId')->textInput(['readonly' => true, 'value' => Yii::$app->user->identity->email])->label('From') ?>
      	</div>

    	<div class="col-xs-6">
    		<?= $form->field($orderItem, 'supplierMailId')->textInput(['readonly' => true, 'value' => $orderItem->product->brand->supplier->email])->label('To (Supplier)') ?>
      	</div>
    </div>  
    	<?php /*$form->field($orderItem, 'message')->textArea(['rows'=>'4'])->label('Message')*/  ?>

    	<?=$form->field($orderItem, 'message')->widget(CKEditor::className(), ['id'=>'messagecontent', 'clientOptions' =>['language' => 'en' ]])?>

    	<input type="hidden" name="OrderItems[orderItemId]" value="<?=$orderItem->id?>">

	    <div class="form-group modal-footer">
	        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
	    </div>
      
	<?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
  $(".btn-success").click(function(){ 
    $('#cke_1_top').css('display','none');
  });  
</script>

    


