<?php
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;

$this->title = 'Order from Supplier';
$this->params['breadcrumbs'][] = $this->title;
//$model->product->brand->supplier->phone

?>

<style type="text/css">
    .ui-dialog{ box-shadow: 0 0 25px rgba(0, 0, 0, 0.3);}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="sm-box-container">
            <div class="box box-default">
                <div class="box-header with-border">
                    <!-- <i class="fa fa-bullhorn"></i><h3 class="box-title">Items Ordered</h3> -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= GridView::widget([
                        'id' => 'order-items',
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'label'=> 'Order Id',
                                'attribute' => 'orderId',
                                'value' => 'orderId'
                            ],
                            [
                                'label'=> 'Order Date/ Time',
                                'attribute' => 'order.orderDate',
                                'value' => 'order.orderDate'
                            ],
                            // [
                            //     'label'=> 'Customer Info',
                            //     'format' => 'html',
                            //     'attribute' => 'order.billingAddressText',
                            //     'value' => 'order.billingAddressText'
                            // ],
                            [
                                'label'=> 'Customer Info',
                                'format' => 'html',
                                'attribute' => 'order.shippingAddressText',
                                'value' => 'order.shippingAddressText'
                            ],
                            [
                                'label'=> 'Item Orderd',
                                'attribute' => 'id',
                                'value' => 'product.name'
                            ],
                            //'productId',
                            [
                                'label'=> 'SKU',
                                'attribute' => 'productId',
                                'value' => 'product.sku'
                            ],
                            [
                                'label' => 'Qty',
                                'attribute' => 'quantity',
                                'value' => 'quantity'
                            ],
                           [
                            //'label' => 'Order Item',
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{link}',
                                'buttons' => [         
                                    'link' => function ($url, $model) {
                                        $user= User::findOne(Yii::$app->user->id);
                                        $token= md5($user->email.':'.$user->password_hash);
                                        return "<a href=".  Yii::$app->params['b2bUrl'] . '/b2b/default/login-and-forward?uid='.Yii::$app->user->id.'&pid='.$model['productId'].'&authString='.$token." data-width='750px' data-title='Order from Supplier' target='_blank'><button class='btn btn-success'>Order</button></a>";
                                    },
                                ]    
                            ],
                        ],

                    ]); ?>

                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>