<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AttributeGroups;
use dosamigos\multiselect\MultiSelect;
use backend\components\Helper;
use \kartik\file\FileInput;
use yii\grid\GridView;
use common\models\AttributeOptions;
use common\models\Attributes;




/* @var $this yii\web\View */
/* @var $model common\models\Attributes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-index">
    <div class="nav-tabs-custom">                                
        <ul class="nav nav-tabs pull-right">
            <li class="active"><a data-toggle="tab" href="#attributes">Product Attribute</a></li>            
            <li id="m_title" ><a data-toggle="tab" href="#manage_title">Manage Titles (Size, Color, etc.)</a></li>           
        </ul>
    </div> 
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="tab-content responsive">
                    <div class="tab-pane active" id="attributes">
                        <div class="attributes-form" >             
                            <?= $form->field($model, 'title')->textInput(['maxlength' => 45]) ?>
                            <?= $form->field($model, 'code')->textInput() ?>
                            <?= $form->field($model, 'scope')->dropDownList(['global' => 'Global','store' => 'Store' ]); ?>
                            <?= $form->field($model, 'field')->dropDownList(['text' => 'Text Field', 'textarea' => 'Text Area','date' => 'Date', 'boolean' => 'Boolean', 
                             'multiselect' => 'Multiple Select','dropdown' => 'Dropdown', 'price' => 'Price']); ?>
                            <?php 
                            if ($this->context->action->id == 'update') {
                                $att= Attributes::find($model->id)->where(['id' => $model->id])->one();
                                if($att['applyToSimple']=='0' || $att['applyToGrouped']=='0' || $att['applyToConfigurable']=='0' || $att['applyToVirtual']=='0' || $att['applyToBundle']=='0' || $att['applyToDownloadable']=='0')
                                {
                                    $model->attributeGroupId='2';
                                }
                            }
                            ?>
                            <?= $form->field($model, 'attributeGroupId')->dropDownList(['1' => 'All Product Types', '2' => 'Selected Product Types']); ?>  
                            <div id="spt">
                                <?php
                                $attr_selected[]=0;
                                if ($this->context->action->id == 'update') {
                                    $att= Attributes::find($model->id)->where(['id' => $model->id])->one();                        
                                    //print_r($att);
                                    if($att['applyToSimple']=='1') { $attr_selected[]='Simple';}
                                    if($att['applyToGrouped']=='1') { $attr_selected[]='Grouped';}
                                    if($att['applyToConfigurable']=='1') { $attr_selected[]='Configurable';}
                                    if($att['applyToVirtual']=='1') { $attr_selected[]='Virtual';}
                                    if($att['applyToBundle']=='1') { $attr_selected[]='Bundle';}
                                    if($att['applyToDownloadable']=='1') { $attr_selected[]='Downloadable';}
                                     //print_r($ptr); 
                                    
                                    $data = array('Simple' => 'Simple','Grouped' => 'Grouped','Configurable' => 'Configurable','Virtual' => 'Virtual','Bundle' => 'Bundle','Downloadable' => 'Downloadable',); 
                                    $htmlOptions = array('multiple' => 'true');
                                    echo $form->field($model,'productTypeApplicableUpdate')->listBox($data, $htmlOptions);
                                }?> 

                                <?php 
                                if ($this->context->action->id == 'create') {
                                    $data = array('Simple' => 'Simple','Grouped' => 'Grouped','Configurable' => 'Configurable','Virtual' => 'Virtual','Bundle' => 'Bundle','Downloadable' => 'Downloadable',); 
                                    $htmlOptions = array('multiple' => 'true');
                                    echo $form->field($model,'productTypeApplicable')->listBox($data, $htmlOptions);
                                }
                                ?>                  
                            </div>    
                            <?= $form->field($model, 'required')->dropDownList(['0' => 'No', '1' => 'Yes']); ?>
                            <?= $form->field($model, 'validation')->dropDownList([ 'decimal' => 'Decimal Number','integer' => 'Integer Number', 
                            'email' => 'Email','url' => 'URL','letters' => 'Letters(a-z)(A-Z) or Numbers(0-9)'],['prompt'=>'Select']); ?>
            
                            <div id="drop_conf">
                            <?= $form->field($model, 'configurable')->dropDownList(['0' => 'No', '1' => 'yes']); ?>
                            </div>

                            <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="manage_title">
                        <div class="attributes-index">
                            <table  width="90%" border="1"><tr> <td align="right"><input type="button" value="Add Row" onclick="addRow('dataTable')"></td></tr></table> 
                                <?php $model1= new AttributeOptions();?>
                                <?php if ($this->context->action->id == 'update') { ?>
                                    <?php
                                    $att_options = AttributeOptions::find()->where(['attributeId' => $model->id])->all();
                                    foreach ($att_options as $index => $att_options_id) {?>
                                        <table id="dataTable1" width="90%" border="1">
                                            <tr id="<?=$att_options_id['id']?>"><td width="90%">                    
                                            <?= $form->field($model1, 'value['.$att_options_id['id'].']')->textInput(['readonly' => !$model->isNewRecord,'value'=>$att_options_id['value']])->label(false) ?></td>
                                            <td width="10%"><span class="man-title" id="<?=$att_options_id['id']?>"><i class="fa fa-close"></i></span>                            
                                            </td></tr>
                                        </table>
                                    <?php }
                                } ?>
                                <table id="dataTable" width="90%" border="1">
                                    <tr><td><?= $form->field($model1, 'value[]')->textInput(['maxlength' => 45])->label(false) ?></td></tr>
                                </table>                                                            
                                <div class="form-group">
                                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script language="javascript">
    function addRow(tableID){
        var table=document.getElementById(tableID);
        var rowCount=table.rows.length;
        var row=table.insertRow(rowCount);
        var colCount=table.rows[0].cells.length;
        for(var i=0;i<colCount;i++){
            var newcell=row.insertCell(i);
            newcell.innerHTML=table.rows[0].cells[i].innerHTML;
            switch(newcell.childNodes[0].type){
                case"text":newcell.childNodes[0].value="";break;
                case"checkbox":newcell.childNodes[0].checked=false;break;
                case"select-one":newcell.childNodes[0].selectedIndex=0;break;
            }
        }
    }

    $(document).ready(function(){        
        $("#attributes-producttypeapplicableupdate option").each(function(){            
            <?php foreach ($attr_selected as $index => $attr_sel)  {?>
                if ($(this).text() == "<?=$attr_sel?>")
                $(this).attr("selected","selected");
            <?php } ?> 
        });
        if ($('#attributes-field').val() == 'dropdown' || $('#attributes-field').val() == 'multiselect'){
            $("#m_title").show();    
            $("#drop_conf").show();
            $("#manage_title").show();                    
        }
        else
            {
            $("#m_title").hide();   
            $("#drop_conf").hide();
            $("#manage_title").hide();
        }
        //alert($("#attributes-attributegroupid").val());
        if ($('#attributes-attributegroupid').val() == '2'){
            $("#spt").show();
        }
        else {
            $("#spt").hide();
        }
    
        $('#attributes-attributegroupid').on('change', function() {
            if ( this.value == '2'){
                $("#spt").show();
            }
            else{
                $("#spt").hide();
            }
        });    
        $('#attributes-field').on('change', function() { 
            if ( this.value == 'dropdown' || this.value == 'multiselect'){
                $("#m_title").show();    
                $("#drop_conf").show();
                $("#manage_title").show();
            }
            else {
                $("#m_title").hide();    
                $("#drop_conf").hide();
                $("#manage_title").hide();
            }
        }); 
    });

</script>
<script>//---aaron 
    $(document).ready(function(){
        $(".man-title").click(function(){
            if(confirm("Are you sure you want to delete this item?..")){
                $.ajax({
                    type: "POST",
                    url: "<?=Yii::$app->urlManager->createUrl(['attributes/deleteattributeoption'])?>",             
                    data: "&id=" + this.id ,
                    dataType: "html", 
                    success: function (data) {                         
                        $("#"+data).hide(); 
                        $("#attributeoptions-value-"+data).val('');                        
                    }                 
                }); 
            }
            else{
                return false;
            }            
        });
    });
</script>