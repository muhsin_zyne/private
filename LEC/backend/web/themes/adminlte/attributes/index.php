<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Attributes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">

<div class="attributes-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="create_attri ">
        <?= Html::a('<i class="fa fa-plus"></i>Create Attributes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php \yii\widgets\Pjax::begin(array('id' => 'attributes')); ?>
    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            
            'code',
            'scope',
            'field',
             //'applyToType',
            'required', 
            'validation',
            'configurable',

            // 'applyToSimple',
            //'applyToBundle',
            //'applyToGrouped',
            //'applyToConfigurable',
            //'applyToVirtual',
            //'applyToDownloadable',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttons' => [
                'update' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                },
                'delete' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Delete" data-pjax="0" data-confirm ="Are you sure you want to delete this item?",><span class="fa fa-trash-o"></span></a>' ;
                },
                ]
            ],
        ],
    ]); ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>
</div>
</div>
