<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GiftRegistry */


$this->title = 'Update Gift Registry: ' . ' ' . $model->brideName . ' '. $model->groomName;
$this->params['breadcrumbs'][] = ['label' => 'Gift Registries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gift-registry-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
