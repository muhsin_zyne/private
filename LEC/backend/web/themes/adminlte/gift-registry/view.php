<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotions */

$this->title = $model->brideName. ' '.$model->groomName;
$this->params['breadcrumbs'][] = ['label' => 'Gift Registries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-body">
        <div class="consumer-promotions-view">

            <h1><?php // Html::encode($this->title)  ?></h1>

          
            <div class="box">
                <div class="box-body">

                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'brideName',
                             'groomName',
                             'contact',
                             'email:email',
                           // 'phone',
                            
                            [
                                'label'=>'Phone',
                                'attribute' => 'phone',
                            ],
                            'address:ntext',
                            'city',
                            'state',
                            'postalCode',
                            'blurb:ntext',
                            [
                                'attribute' => 'weddingDate',
                                'value' => Helper::date($model->weddingDate),
                            ],
                            [
                                'attribute' => 'activeFrom',
                                'value' => Helper::date($model->activeFrom),
                            ],
                            [
                                'attribute' => 'expiresOn',
                                'value' => Helper::date($model->expiresOn),
                            ],
                           
                            [
                                'attribute' => 'photo',
                                'value' => Yii::$app->params["rootUrl"] . $model->image,
                                'format' => ['image', ['width' => '100', 'height' => '100']],
                            ],
                            'urlKey',
                            //'catalog:ntext',
                        ],
                    ])
                    ?>

                    <?php //var_dump($dataProvider);die(); ?>

                    <?=
                    GridView::widget([
                        'dataProvider' => $giftpdts,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'product.name',
                            'product.sku',
                            [
                                'label' => 'PRODUCT PRICE',
                                'attribute' => 'product.price',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return Helper::money($model->product->price);
                                },
                            ],
                            //'product.price',
                            [
                                'label' => 'PRODUCT COST PRICE',
                                'attribute' => 'product.cost_price',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return Helper::money($model->product->cost_price);
                                },
                            ],
                            //'product.cost_price',
//                            [
//                                'label' => 'OFFER SELL PRICE',
//                                'attribute' => 'offerSellPrice',
//                                'format' => 'html',
//                                'value' => function ($model) {
//                                    return Helper::money($model->offerSellPrice);
//                                },
//                            ],
//                            //'offerSellPrice',
//                            [
//                                'label' => 'OFFER COST PRICE',
//                                'attribute' => 'offerCostPrice',
//                                'format' => 'html',
//                                'value' => function ($model) {
//                                    return Helper::money($model->offerCostPrice);
//                                },
//                            ],
                            //'offerCostPrice',
                          //  'pageId',
                        /* [
                          'class' => 'yii\grid\ActionColumn',
                          'template' => '{delete}',
                          ], */
                        ],
                    ]);
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>

