


<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\Helper;

/* @var $this yii\web\View */

/* @var $searchModel common\models\ConsumerPromotionsSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Gift Registries';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="gift-registries-index">



    <div class="box">

        <div class="box-body">

<?php // echo $this->render('_search', ['model' => $searchModel]);  ?>



            <p class="create_pdt">

           <?= Html::a('<i class="fa fa-plus"></i> Create Gift Registry', ['create'], ['class' => 'btn btn-success']) ?>

            </p>
<?=
\app\components\AdminGridView::widget([

    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [

        ['class' => 'yii\grid\SerialColumn'],
        //'id',
        'brideName',
        'groomName',
        //'contact',
        //'email:email',
        [

            'attribute' => 'weddingDate',
            //'label' => 'Start Date',
            'value' => function($model, $attribute) {

                return Helper::date($model->weddingDate);
            }
        ],
        [

            'attribute' => 'fromDate',
           // 'label' => 'Start Date',
            'value' => function($model, $attribute) {

                return Helper::date($model->activeFrom);
            }
        ],
//        [
//
//            'attribute' => 'toDate',
//           // 'label' => 'End Date',
//            'value' => function($model, $attribute) {
//
//                return Helper::date($model->expiresOn);
//            }
//        ],
        // 'fromDate',
        //'toDate',
        // 'createdOn',
        // 'consumerStartDate',
        // 'costEndDate',
        // 'image',
        // 'catalog:ntext',
        // 'reminderDate',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}{delete}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return '<a href="' . $url . '" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>';
                },
                'update' => function ($url, $model, $key) {
                    return '<a href="' . $url . '" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                },
                'delete' => function ($url, $model, $key) {
                    return '<a href="' . $url . '" title="Delete" data-pjax="0" data-confirm ="Are you sure you want to delete this item?"><span class="fa fa-trash-o"></span></a>';
                },
            ],
        ],
    ],
]);
?>

        </div>

    </div>



</div>


