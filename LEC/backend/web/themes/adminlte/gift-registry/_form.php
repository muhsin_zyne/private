<?php     

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\TreeView;

/* @var $this yii\web\View */
/* @var $model common\models\GiftRegistry */
/* @var $form yii\widgets\ActiveForm */
$gift_products= common\models\GiftRegistryProducts::find()->where(['giftRegistryId'=>$model->id])->all();

?>
<link rel='stylesheet' href='/css/easyTree.css' />
<link rel='stylesheet' href='/css/style_treeview.css' />
<div class="box">
<div class="box-body">

<div class="gift-registry-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'brideName')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'groomName')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'contact')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput() ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'city')->textInput() ?>

    <?= $form->field($model, 'state')->dropDownList(['QLD'=>'QLD','NSW/ACT'=>'NSW/ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--']); ?>

    <?= $form->field($model, 'postalCode')->textInput() ?>

    <?= $form->field($model, 'blurb')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'storeId')->hiddenInput(['value'=> Yii::$app->params['storeId']])->label(false); ?>

    
   
    
    <?= $form->field($model, 'weddingDate')->widget(\kartik\date\DatePicker::classname(), [
             'options' => ['accept'=>'image/*'],
            'pluginOptions' => [
                 'allowedFileExtensions'=>['jpg', 'gif', 'png', 'bmp'],
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy',   
           // 'startDate' => date("Y-m-d H:i:s"),    
        ]
    ]);
    
    ?>
    
    <?=
    $form->field($model, 'activeFrom')->widget(\kartik\date\DatePicker::classname(), [
         'options' => ['accept'=>'image/*'],
        'pluginOptions' => [
             'allowedFileExtensions'=>['jpg', 'gif', 'png', 'bmp'],
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            //'startDate' => date("Y-m-d H:i:s"),
        ]
    ]);
    ?>
    
    
    <?= $form->field($model, 'expiresOn')->widget(\kartik\date\DatePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            //'startDate' => date("Y-m-d H:i:s"),
        ]
    ]);
    ?>
    
    <?= $form->field($model, 'urlKey')->textInput() ?>

    
    <div class="brand-image" style="margin-bottom:10px;">
       
         <?php
             //var_dump(Yii::$app->params["rootUrl"]);die;
        if (!empty($model->image) && !empty($model->id)) {
            $pluginOptions = ['initialPreview' => [Html::img(Yii::$app->params["rootUrl"] . $model->image)],
                'overwriteInitial' => true, 'showRemove' => false, 'showUpload' => false,
               
            ];
        } else {
            $pluginOptions = [
                'overwriteInitial' => true, 'showRemove' => false, 'showUpload' => false,
               
            ];
        }
        //echo '<label class="control-label">Logo</label>';
//        echo kartik\widgets\FileInput::widget([
//            'model' => $model,
//            'attribute' => 'image',
//            'pluginOptions' => $pluginOptions
//        ]);
        echo $form->field($model, 'image')->widget(kartik\widgets\FileInput::classname(), [
            'pluginOptions' => $pluginOptions
        ]);
        ?>

    </div>
    <label class="control-label">Add Products</label>
    <div class="search sm-100">
        <label class="kv-heading-container" for="products-size">Search</label>
        <?= Html::textInput('search', '', $options = ['class' => 'product_search form-control', 'style' => '', 'placeholder' => 'Enter SKU or name of a product', 'autocomplete' => 'off']) ?>
        <span>When you type the letter, result will get displyed below.</span><div id="product-load" style="display:none;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
        <div class="product_box">
            <div class="product_list"></div>    
            <div class="form-group" style="margin-top:20px;margin-bottom:30px">
                <?= Html::a('Add to List', 'javascript:;', $options = ['class' => 'btn btn-primary add_pdt']) ?>
            </div>
        </div>
    </div>
    


    <div class="tray_products_table">
        <div class="table-responsive">
            <table border="1" cellspacing="0" cellpadding="0" class="table sm-table">
                <thead>
                    <tr class="headings">
                        <th></th>
                        <th>Name</th>
                        <th>SKU</th>
                        <th>EZ Code</th>
                        <th>Original sell price</th>
<!--                        <th>Original cost price</th>-->
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!$model->isNewRecord) {    //var_dump($promotionProducts);die();
                        if (!empty($gift_products)) {
                            foreach ($gift_products as $product) { 

//var_dump($product['offerSellPrice']);die();
                                ?>
                                <tr data-productId=<?= $product['id'] ?>>
                                    <td><input type="checkbox" checked name="Products[id][<?= $product['id'] ?>]" class="selected" value="<?= $product['id'] ?>"></td>
                                    <td><?= $product->product->name ?></td>
                                    <td><?= $product->product->sku ?></td>
                                    <td><?= $product->product->ezcode ?></td>
                                    <td><?= $product->product->price ?></td>
<!--                                    <td><?= $product->product->cost_price ?></td>-->
                                    
                                    <td><a href="#" class="remove" title="Delete">X</td>
                                </tr>
                            <?php }
                        }
                    } ?>
                </tbody>    
            </table>  
        </div>  
    </div> 

<div class="deleted_items"></div>
<br/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>    

<script type="text/javascript">
    $(document).ready(function() {

        addedproducts = [];
        listedproducts = [];

        
//        $(".product_search").on('keyup',function(e){
//
//            $('.selected:checked').each(function(){
//                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
//                    addedproducts.push($(this).attr('value'));
//                }    
//            });
//            
//            $(".tray_products_table table tbody tr").each(function(){ 
//                if($(this).attr('data-productId')) {
//                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 
//                        listedproducts.push($(this).attr('data-productId')); 
//                    }
//                }        
//            });
//
//            var keyword = $('.product_search').val();
//            $.ajax({
//                    url:"<?php echo Yii::$app->urlManager->createUrl(['gift-registry/getproducts']) ?>",    
//                    type: 'POST',
//                    data: {keyword: keyword},
//                    dataType: 'json',
//                    beforeSend: function() {
//                        $("#product-load").show();
//                    },
//                    success: function(data)
//                        {
//                            $(".product_item").empty("");
//                            $("#product-load").hide();
//                            if(jQuery.trim($(".product_search")).length > 0) 
//                            {   
//                                $.each(data, function(key, item) { 
//
//                                    if ($.inArray(key, listedproducts) == -1) {
//
//                                        if ($.inArray(key, addedproducts) == -1) {    
//                                    
//                                            $('.product_list').append('<div class="product_item"><input type="checkbox" id="'+item['id']+'" name="Products[]" value="'+item['id']+'" class="product_checkbox chk-box" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'" data-costprice="'+item['cost_price']+'"><label for="'+item['id']+'"></label><span>'+item['name']+'</span></div>');
//                                        }    
//                                    }    
//                                }); 
//                            }   
//                        }
//            });
//        });
        
        $(".product_search").autocomplete( {
            source: function( request, response ) { 
                $('.selected:checked').each(function(){
                    if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                        addedproducts.push($(this).attr('value'));
                    }    
                });

                $(".tray_products_table table tbody tr").each(function(){ 
                    if($(this).attr('data-productId')) {
                        if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 
                            listedproducts.push($(this).attr('data-productId')); 
                        }
                    }        
                });
                
                // New request 300ms after key stroke
                var $this = $(this);
                var $element = $(this.element);
                var previous_request = $element.data( "jqXHR" );
                if( previous_request ) {
                    // a previous request has been made.
                    // though we don't know if it's concluded
                    // we can try and kill it in case it hasn't
                    previous_request.abort();
                }
                
                 $element.data( "jqXHR",$.ajax({
                    url:"<?php echo Yii::$app->urlManager->createUrl(['gift-registry/getproducts']) ?>",    
                    type: 'GET',
                    data: {keyword: request.term},
                    dataType: 'json',
                    beforeSend: function() {
                        $("#product-load").show();
                    },
                     success: function(data)
                        {
                            $(".product_item").empty("");
                            $("#product-load").hide();
                            if(jQuery.trim($(".product_search")).length > 0) 
                            {   
                                $.each(data, function(key, item) { 

                                    if ($.inArray(key, listedproducts) == -1) {

                                        if ($.inArray(key, addedproducts) == -1) {    
                                    
                                            $('.product_list').append('<div class="product_item"><input type="checkbox" id="'+item['id']+'" name="Products[]" value="'+item['id']+'" class="product_checkbox chk-box" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'" data-costprice="'+item['cost_price']+'"><label for="'+item['id']+'"></label><span>'+item['name']+'</span></div>');
                                        }    
                                    }    
                                }); 
                            }   
                        }
                }));
            },
           // minLength: 3,
            json: true,
        });

        $('#w1-tree').height('150');

        $('.add_pdt').on('click',function(){ 

            $(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });

            $('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });

            checked_pdts = [];

            $('.product_checkbox:checked').each(function(){
                        checked_pdts.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price'), 'cost_price' : $(this).attr('data-costprice')});
            }); 

            $(".tray_products_table").css('display','block');

            for( var i = 0, xlength = checked_pdts.length; i < xlength; i++)
                { 
                    if ($.inArray(checked_pdts[i].idss, listedproducts) == -1) {

                        if ($.inArray(checked_pdts[i].idss, addedproducts) == -1) {

                            $(".tray_products_table table tbody").append('<tr data-productId="'+checked_pdts[i].idss+'"><td><input type="checkbox" name="Products[id]['+checked_pdts[i].idss+']" value="'+checked_pdts[i].idss+'" id="av'+checked_pdts[i].idss+'" class="chk-box"><label for="av'+checked_pdts[i].idss+'"></label></td><td>'+checked_pdts[i].names+'</td><td>'+checked_pdts[i].sku+'</td><td>###</td><td>'+checked_pdts[i].price+'</td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products[' + checked_pdts[i].idss + '][newitem]" id="data-new" value="1">');
                    }
                }
            }
        });

    });
</script>

<script type="text/javascript">
    $(".tray_products_table").on("click", ".remove", function (e) {

        var trid = $(this).closest('tr').attr('data-productid');
        //console.log(trid); 
        if (trid) {
            jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedItems][]" id="deletedItems" value="' + trid + '"/>');
        }
        $(this).closest('tr').remove();
    });
</script>

<script type="text/javascript">
<?php if ($model->isNewRecord) { ?>
        /*  if(!empty($promotion->products)) {*/
        $(".tray_products_table").css('display', 'none');
        //$(".tray_products_table").css('display','block');

<?php } /* foreach ($promotion->promotionProducts as $promotionProduct)  { ?>

  $(".tray_products_table table tbody").append('<tr data-productId="<?=$promotionProduct->id?>"><td><input type="checkbox" checked name="Products[id][<?=$promotionProduct->product->id?>]" class="selected" value="<?=$promotionProduct->product->id?>"></td><td><?=$promotionProduct->product->name?></td><td><?=$promotionProduct->product->sku?></td><td>###</td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][original_sell_price]" class="cost_price_promotions" value="<?=$promotionProduct->product->price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][original_cost_price]" class="cost_price_promotions" value="<?=$promotionProduct->product->cost_price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][offerSellPrice]" value="<?=$promotionProduct->offerSellPrice?>" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][offerCostPrice]" value="<?=$promotionProduct->offerCostPrice?>" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][pageId]" class="cost_price_promotions" value="<?=$promotionProduct->pageId?>" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td>');


  }
  }
  } */
?>
</script>

<script type="text/javascript">
    $(document).ready(function () {



        $('.load_pdt').on('click', function () {

            $('.selected:checked').each(function () {
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }
            });

            $(".tray_products_table table tbody tr").each(function () {
                if ($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                        listedproducts.push($(this).attr('data-productId'));
                    }
                }
            });

            categoryIds = Array();

            if ($("#w1").val() != "") {

                categoryIds.push($("#w1").val());

                $.ajax({
                    url: "<?php echo Yii::$app->urlManager->createUrl(['consumer-promotions/getcategoryproducts']) ?>",
                    type: 'POST',
                    data: {categoryIds: categoryIds},
                    success: function (data)
                    {

                        $(".tray_products_table").css('display', 'block');

<?php
if ($model->isNewRecord) {  //die('Update');
    if (empty($model->products)) {
        ?>

                                $(".tray_products_table table tbody").empty();

    <?php }
} ?>

                        $.each($.parseJSON(data), function (key, item) {
                            if ($.inArray(key, listedproducts) == -1) {
                                if ($.inArray(key, addedproducts) == -1) {

                                    $(".tray_products_table table tbody").append('<tr data-productId="' + item['id'] + '"><td><input type="checkbox" name="Products[id][' + item['id'] + ']" class="selected" value="' + item['id'] + '"></td><td>' + item['name'] + '</td><td>' + item['sku'] + '</td><td>###</td><td><input type="text" name="Products[' + item['id'] + '][original_sell_price]" class="cost_price_promotions" value="' + item['price'] + '" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[' + item['id'] + '][offerSellPrice]" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[' + item['id'] + '][offerCostPrice]" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[' + item['id'] + '][pageId]" class="cost_price_promotions" value="1" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr></tr><input type="hidden" name="Products[' + item['id'] + '][newitem]" id="data-new" value="1">');
                                }
                            }
                        });
                    }
                });
            } else {

                alert('Please select a category');

                $(".tray_products_table").css('display', 'block');

<?php
if (!$model->isNewRecord) {  //die('Update');
    if (!empty($model->products)) {
        ?>

                        $(".tray_products_table table tbody").empty();

                        $(".tray_products_table table tbody").append('<tr><td> No items found </td> </tr>');

    <?php }
} ?>


            }

        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.field-giftregistry-image label').append('<span style="color:#b8c7ce;font-size:12px; margin-left:15px; font-style:italic;">Ideal dimension is 224px x 224px. Any square dimension will work fine.</span>');
        /* $(".promotions").on('submit',(function(e) {
         var reg=/(.csv)$/;
         if (!reg.test($(".csv-file-upload").val())) {
         alert('Invalid File Type');
         return false;
         }
         else{
         var files = this.files[0];
         var formData = new FormData();
         // data.append('input_file_name', $('your_file_input_selector').prop('files')[0]);
         formData.append('formData', files);
         console.log(formData);
         $.ajax({
         url: '<?php echo Yii::$app->request->baseUrl; ?>/index.php?r=consumerpromotions/test',  //Server script to process data
         type: 'POST',
         data: formData,
         dataType : 'json',  
         contentType: false,
         processData: false,
         success: function(html){
         alert(html);
         }
         });
         }
         }));*/

        //console.log('before:'+listedproducts);

        //console.log('before:'+addedproducts);

        $(".promotions").on('submit', (function (e) {
            e.preventDefault();
            $(".upload-msg").text('Loading...');

            $(".tray_products_table table tbody tr").each(function () {
                if ($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                        listedproducts.push($(this).attr('data-productId'));
                    }
                }
            });

            $('.selected:checked').each(function () {
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }
            });

            console.log('after:' + listedproducts);

            console.log('after:' + addedproducts);


            $.ajax({
                url: "<?php echo Yii::$app->urlManager->createUrl(['gift-registry/getproducts']) ?>",
                type: "POST",
                data: new FormData($(this)[0]),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                        $("#product-load").show();
                    },
                success: function (data)
                {

                    $(".tray_products_table").css('display', 'block');
                    $("#product-load").hide();
                    $.each($.parseJSON(data), function (key, item) {
                        if (item.length != 0) {
                            if (key == "notfound") {
                                for (var i = 0, xlength = item.length; i < xlength; i++)
                                {
                                    $(".error-msg").append('<div class="alert alert-danger alert-dismissable"><i class="icon fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Not found this products with sku :' + item[i]['sku'] + ' </div>');
                                    //console.log(item);
                                }
                            } else {
                                for (var i = 0, xlength = item.length; i < xlength; i++)
                                {
                                    if ($.inArray(item[i]['id'], listedproducts) == -1) {

                                        //console.log('listedproducts : '+listedproducts);
                                        //console.log('addedproducts : '+addedproducts);

                                        if ($.inArray(item[i]['id'], addedproducts) == -1) {

                                            $(".tray_products_table table tbody").append('<tr data-productId="' + item[i]['id'] + '"><td><input type="checkbox" checked name="Products[id][' + item[i]['id'] + ']" class="selected" value="' + item[i]['id'] + '"></td><td>' + item[i]['name'] + '</td><td>' + item[i]['sku'] + '</td><td>###</td><td><input type="text" name="Products[' + item[i]['id'] + '][original_sell_price]" class="cost_price_promotions" value="' + item[i]['price'] + '" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[' + item[i]['id'] + '][original_cost_price]" class="cost_price_promotions" value="' + item[i]['costprice'] + '" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[' + item[i]['id'] + '][offerSellPrice]" value="' + item[i]['offerSellPrice'] + '" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[' + item[i]['id'] + '][offerCostPrice]" value="' + item[i]['offerCostPrice'] + '" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[' + item[i]['id'] + '][pageId]" class="cost_price_promotions" value="' + item[i]['pageId'] + '" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products[' + item[i]['id'] + '][newitem]" id="data-new" value="1">');
                                        }
                                    }
                                }
                            }

                        }
                    });

                }
                //return true;
            });
        }));

    });
</script>        


