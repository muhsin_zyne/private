<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GiftRegistry */

$this->title = 'Create Gift Registry';
$this->params['breadcrumbs'][] = ['label' => 'Gift Registries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-registry-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
