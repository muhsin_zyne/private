<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DownloadPages */

$this->title = 'Create Downloads';
$this->params['breadcrumbs'][] = ['label' => 'Downloads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="download-pages-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
