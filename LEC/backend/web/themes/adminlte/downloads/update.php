<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DownloadPages */

$this->title = 'Update Downloads: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Downloads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="download-pages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
