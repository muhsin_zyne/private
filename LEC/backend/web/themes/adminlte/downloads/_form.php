<?php


use yii\helpers\Html;
use yii\helpers\Url;
//use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\Stores;
/* @var $this yii\web\View */
/* @var $model common\models\DownloadPages */
/* @
var $form yii\widgets\ActiveForm */
?>
<?php $user = User::findOne(Yii::$app->user->id);?>
<div class="box">
    <div class="box-body">
        <div class="download-pages-form">

            <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']] ); ?>
            <?php $model->storeId=Yii::$app->params["storeId"]; ?>
            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
            <?php if($model->isNewRecord){
                    echo $form->field($model, 'filePath')->widget(FileInput::classname(), [
                        'options'=>[],
                        'pluginOptions' => [ 
                            'showPreview' => true,'showCaption' => true,'showRemove' => false,'showUpload' => false,
                        ]
                    ]);
                } else {
                    echo FileInput::widget([
                    'model' => $model,
                    'attribute' => 'filePath',
                    //'disabled' => true,
                    'pluginOptions' => [
                    'initialPreview'=>[Html::img(Yii::$app->params["rootUrl"].$model->filePreview)],'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false] ]); 
                }
            ?>
            <?php if($user->roleId == 1) {
                $stores=Stores::find()->where(['isVirtual'=>0])->all();
                $listData=ArrayHelper::map($stores,'id','title');
                echo $form->field($model, 'storeId')->dropDownList($listData);
            } else {  
                echo $form->field($model, 'storeId')->hiddenInput()->label(false);
            } ?>
            

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            
            <?= $form->field($model, 'status')->dropDownList([ '1' => 'Enable', '0' => 'Disable']); ?>
            
            <?= $form->field($model, 'position')->hiddenInput(['value'=>0])->label(false); ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
