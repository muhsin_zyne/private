<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DownloadPages */

$this->title = 'Downloads View : '.$model->title;
$this->params['breadcrumbs'][] = ['label' => 'Downloads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="box">
    <div class="box-body">
        <div class="download-pages-view">
            <!--<p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>-->

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //'id',
                    'title',
                    [
                        'label' => 'File',
                        'attribute'=>'filePath',
                        'value'=>Yii::$app->params["rootUrl"].$model->filePreview,
                        'format' => ['image',['width'=>'400']],
                    ],
                    
                    //'filePath:ntext',
                    [
                        'label' => 'Store Name',
                        'value' => $model->store->title,
                    ],
                    //'store.title',                    
                    'description:ntext',
                    //'status',
                    [
                        'label' => 'Status',
                        'value' => ($model->status==1) ? 'Enabled': 'Disabled',
                    ],
                    //'value' => $model->status == 1 ? 'yes' : 'no',
                    //'position',
                    [
                        'label' => 'Date Updated',
                        'value' => $model->formatedDateUpdated,
                    ],
                    //'formatedDateUpdated',
                ],
            ]) ?>
        </div>
    </div>
</div>
