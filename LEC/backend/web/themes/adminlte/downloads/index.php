<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Stores;
use common\models\User;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DownloadPagesSaerch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Downloads';
$this->params['breadcrumbs'][] = $this->title;
$stores=Stores::find()->where(['isVirtual'=>0])->all();
$listData=ArrayHelper::map($stores,'id','title');
$user = User::findOne(Yii::$app->user->id);
?>
<div class="box">
    <div class="box-body">
        <div class="download-pages-index">
            <p class="pull-right">
                <?= Html::a('Create Downloads', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php \yii\widgets\Pjax::begin(['id' => 'download-pages']); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'id'=>'download-pages',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'title',
                    //'filePath',
                    //'storeId',
                    [   
                        'label' => 'Description',
                        'attribute' => 'description',
                        'value'=>function($model, $attribute){ return (strlen($model->description)<550)?$model->description:substr($model->description, 0, 550).'......';}, 
                    ],
                    [   
                        'label' => 'Store Name',
                        'attribute' => 'storeId',
                        'visible'=>($user->roleId==1)?true:false,
                        'value' => function($model, $attribute){ return $model->store->title;}, 
                        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'storeId', $listData,
                            ['class'=>'form-control','prompt' => '']),
                    ],
                    // 'status',
                    'status' => [
                        'label' => 'Status',
                        'value' => function($model, $attribute){ return $model->status=="1"? "Enabled" : "Disabled"; },
                        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Disabled', '1' => 'Enabled'],['class'=>'form-control','prompt' => '']),
                    ],
                    // 'position',
                    // 'formatedDateUpdated',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
