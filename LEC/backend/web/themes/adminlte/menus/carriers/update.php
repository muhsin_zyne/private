<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Carriers */

$this->title = 'Update Carriers: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Carriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carriers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
