<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Carriers */

$this->title = 'Create Carriers';
$this->params['breadcrumbs'][] = ['label' => 'Carriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carriers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
