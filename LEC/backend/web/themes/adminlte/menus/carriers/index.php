<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CarriersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Carriers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carriers-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Carriers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'trackingUrl:ntext',
            'createdDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
