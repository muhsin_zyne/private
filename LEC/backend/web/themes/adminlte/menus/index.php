<?php

use yii\helpers\Html;
use common\models\Stores;
use common\models\Menus;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;

$this->title =ucfirst($_REQUEST['type']).' Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
    $model= new Menus;
    $user = User::findOne(Yii::$app->user->id);   
    $model->storeId=$storeid;
    $model->type=$_REQUEST['type'];   
?>

<div class="box">
    <div class="box-body">
        <div class="menu-top-area row" >       
            <div class="col-md-12 sm-grp">
                <?php $form = ActiveForm::begin();
                echo  $form->field($model, 'type')->hiddenInput()->label(false) ;
                if($user->roleId == 1) {
                    $stores=Stores::find()->where(['isVirtual'=>0])->all();
                    $listData=ArrayHelper::map($stores,'id','title');
                    echo $form->field($model, 'storeId')->dropDownList($listData,['prompt'=>'Default']);
                } else {
                    echo  $form->field($model, 'storeId')->hiddenInput()->label(false) ;
                    echo '<b> Store Name : '.$user->store->title;
                }
                ?>
                <img src="/images/loading.gif" id="loading-img" style="display:none"/ >
                <input type="button" value="Save changes" class="btn btn-primary update-btn" id="update_button">
                <?php 
                //echo  Html::submitButton('Save Changes',['id'=>'update_button','class'=>'btn btn-primary update-btn']);
                ActiveForm::end(); ?>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <menu id="nestable-menu">
                            <button type="button" data-action="expand-all" class="btn btn-primary"><i class="fa fa-expand"></i> Expand All</button>
                            <button type="button" data-action="collapse-all" class="btn btn-primary"><i class="fa fa-compress"></i> Collapse All</button>
                            <span style="float: right;">
                               <?= Html::a('<i class="fa fa-plus"></i> Create Menu', ['create','type'=>$model->type], ['class' => 'btn btn-success bootstrap-modal','data-title' => 'Create Menu','data-complete' => 'location.reload();']) ?>
                            </span>
                        </menu>

                    </div>
                </div>
            </div>
        </div>
    
        <div class="row" id="row_main">
            <div class="tab-content responsive">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header"> <h3 class="box-title">Assigned Menu List</h3>
                            <div class="dd box-body" id="nestable">                               
                               <?php  if(count($menuall_position)!=0){
                                    echo $model->getMenuList($menuall_position); 
                                } else {
                                    echo ' <div class="dd-empty"></div>';
                                    echo $model->getMenuList($menuall_position);
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header"><h3 class="box-title">Unassigned Menu List</h3>
                            <div class="dd box-body" id="nestable2">
                                <?php  if(count($menuall_unAssigned)!=0){
                                    echo $model->getMenuList($menuall_unAssigned); 
                                } else {
                                    echo ' <div class="dd-empty"></div>';
                                    echo $model->getMenuList($menuall_unAssigned);
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <textarea id="nestable-output" style="display:none;"></textarea> 
        <textarea id="nestable2-output" style="display:none;"></textarea>
    </div>
</div>

<link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/adminlte/css/nestedSortable.css" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/adminlte/js/AdminLTE/jquery.nestable.js"></script>

<script>
    $(document).ready(function() {
        var updateOutput = function(e){
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // activate Nestable for list 2
    $('#nestable2').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));
    $('.dd').nestable('collapseAll');
    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
            //action ='collapse-all';
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
    //------------------------------------------update menus------------------------------
    $("#update_button").click(function(){
        //alert('hiii');
        $('#loading-img').show();
        var store_id;
        var assigned_menu=$("#nestable-output").val();
        var unassigned_menu=$("#nestable2-output").val();
        var type="<?=$model->type?>";        
        if($("#menus-storeid").val()==''){        
            store_id=0;
        } else {
            store_id=$("#menus-storeid").val();
        }
        //alert(store_id);
        $.ajax({
            type: "POST",
            url: "<?=Yii::$app->urlManager->createUrl(['menus/update'])?>",
            data: "&store_id=" + store_id + "&type=" + type + "&assigned_menu=" + assigned_menu +"&unassigned_menu=" + unassigned_menu,
            dataType: "html", 
            success: function (data) {
                $('#loading-img').hide();
                alert(data);
            }
        });        
    });
    
    //------------------------------------------store select------------------------------
    $("#menus-storeid").change(function(){

        //alert($("#menus-storeid").val());
        var store_id=$("#menus-storeid").val();
        var type="<?=$model->type?>";
        $.ajax({
            type: "POST",
            url: "<?=Yii::$app->urlManager->createUrl(['menus/storemenu'])?>",
            data: "&store_id=" + store_id + "&type=" + type ,
            dataType: "html", 
            success: function (data) {
                //alert(data);
                $("#row_main").html(data);
                var updateOutput = function(e)
                {
                    var list   = e.length ? e : $(e.target),
                    output = list.data('output');
                    if (window.JSON) {
                        output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                        } else {
                        output.val('JSON browser support required for this demo.');
                    }
                };
                $('#nestable').nestable({
                    group: 1
                })
                .on('change', updateOutput);
                $('#nestable2').nestable({
                group: 1
                })
                .on('change', updateOutput);
                updateOutput($('#nestable').data('output', $('#nestable-output')));
                updateOutput($('#nestable2').data('output', $('#nestable2-output')));
                $('.dd').nestable('collapseAll');
                $('#nestable-menu').on('click', function(e)
                {
                    var target = $(e.target),
                        action = target.data('action');
                        //action ='collapse-all';
                    if (action === 'expand-all') {
                        $('.dd').nestable('expandAll');
                    }
                    if (action === 'collapse-all') {
                        $('.dd').nestable('collapseAll');
                    }
                });
            }
        });
    }); 
});
</script>
