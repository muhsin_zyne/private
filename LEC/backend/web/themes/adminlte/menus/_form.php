<?php

use yii\helpers\Html;
use common\models\Stores;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
$this->title = $model->isNewRecord ? 'Create Menu' : 'Update Menu: '.$model->title;
?>
<?php  $user = User::findOne(Yii::$app->user->id); ?>
<div class="menus-form">
    <div class="modal-dialog modal-lg quick-view-wrapper portal-wrapper menu-bm-popup" role="document" data-backdrop="true">
        <div class="box order-popup-bg">
            <div class="box-body">  
                <h3 class="poporder-head"><?= $this->title?></h3>
                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <?php $form = ActiveForm::begin(['id'=>'menu-create','enableAjaxValidation' => false,'enableClientValidation' => true, 'validateOnType' => true, 'validationDelay' => '0']); 
                    $model->unAssigned=1; $model->type=$type ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'path')->textarea()->label('Path <span class="menu-tool"><a href="#" data-toggle="tooltip" data-placement="right" title="Please add .html towards the end of the link! Eg: If your page\'s URL is about-us, please add about-us.html here."><i class="fa fa-question-circle" aria-hidden="true"></i></a></span>') ?> 
                <div id="div-htmlattr">
                    <?= $form->field($model, 'htmlAttributes')->textarea() ?>  
                </div>
                <?php if($user->roleId == 1) {
                    $stores=Stores::find()->where(['isVirtual'=>0])->all();
                    $listData=ArrayHelper::map($stores,'id','title');
                    echo $form->field($model, 'storeId')->dropDownList($listData);
                } else { ?>
                    <?php $model->storeId=Yii::$app->params['storeId'];  ?>
                    <?= $form->field($model, 'storeId')->hiddenInput()->label(false) ?>
                <?php } ?>               
                <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
                <?= $form->field($model, 'unAssigned')->hiddenInput()->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success refresh' : 'btn btn-primary refresh']) ?>                   
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
