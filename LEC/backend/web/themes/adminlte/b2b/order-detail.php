<?php

use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;
use common\models\OrderComment;
use common\models\SalesComments;

$this->title = 'Order #'.$model->orderId.' | '. $model->orderPlacedDate;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
  <div class="col-sm-12">
    <div class="btn-right-sm">
    <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['b2b/orders'], ['class' => 'btn btn-default ']) ?>
    </div>
    </div>
</div>

<div class="categories-index">
</div> 
  <div class="row">
    <div class="col-xs-12">
        <div class="tab-content responsive">
            <div class="tab-pane active" id="information">
            <div class="box">
             <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                  <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-file-text-o"></i><h3 class="box-title">Order # <?= $model->orderId ?> (the order confirmation email was sent)</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body span-sm">
                            <p><span>Order Date</span> : <?= $model->orderPlacedDate ?></p>
                            <p><span>Order Status</span> : <?= $model->status ?></p>
                            <p><span>Purchased From</span> : <?= $model->storeName ?></p>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    </div>
                    
               
                <div class="col-md-6">
                 <div class="box box-default">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i><h3 class="box-title">Account Information</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body span-sm">
                        <p><span>Customer Name</span> : <?= ucfirst($model->customer->firstname).' '.ucfirst($model->customer->lastname) ?></p>
                        <p><span>Email</span> : <?= $model->customer->email ?></p>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                </div>
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-shopping-basket"></i><h3 class="box-title">Items Ordered</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                    <?= GridView::widget([
                    'id' => 'order-items',
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        
                        //'id',
                        [
                            'label'=> 'Item Orderd',
                            'attribute' => 'id',
                            'value' => 'product.name'
                            
                        ],
                        [
                            'label' => 'Option(s)',
                            'value' => 'superAttributeValuesText',
                            'format' => 'html'
                        ],
                        //'productId',
                        [
                            'label'=> 'SKU',
                            'attribute' => 'productId',
                            'value' => 'sku'
                        ],
                        'status',
                        [
                            'label' => 'To Store',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return ($model->b2baddress)? $model->b2baddress->storeName : "N/A";
                            },
                        ],
                        [
                            'label' => 'Price',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money($model->price);
                            },
                        ],
                        
                        [
                            'label' => 'Qty',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->qtyStatus;
                            },
                        ],
                        [
                            'label' => 'Sub Total',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money($model->price * $model->quantity);
                            },
                        ],
                        [
                            'label' => 'Discount Amount',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money($model->discount);
                            },
                        ],
                        [
                            'label' => 'Row Total',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money(($model->price * $model->quantity)- $model->discount);
                            },
                        ],
                    ],

                ]); ?>
                    </div><!-- /.box-body -->
                    
                    
                
                
            </div>
            
              
              <div class="box box-default">
                        <div class="box-header with-border">
                            <i class="fa fa-book"></i><h3 class="box-title">Order Totals</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body sm-min-row" >
                            <p><span class="one-span">Grand Total</span> : <?= Helper::money($model->grandTotal) ?></p>
                        </div><!-- /.box-body -->
                    </div>
              
                </div>
                
                
                
                
            </div>
            
            </div>
            
            
            </div>
                
                
            
            
            
            
            </div>
            </div>
            </div>
            </div>

<?php $order_status= $model->status; ?>
<script type="text/javascript">
    $(document).ready(function(){
        var status = '<?php echo $order_status;?>';

        if(status=='hold')
        {
            $(".hold").hide();
            $(".div-unhold").show();
        }
        else
        {
           //$(".hold").hide();
            $(".div-unhold").hide();
        }
        $('#unhold').click(function(){
            var status='pending';
            var order_id='<?php echo $model->id;?>';
            //alert(order_id);
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['orders/changestatus'])?>",
                data: "&status=" + status + "&order_id=" + order_id,
                dataType: "html", 
                success: function (data) {
                    //alert(data);                
                }                
            });
        });
        $('#hold').click(function(){
            var status='hold';
            var order_id='<?php echo $model->id;?>';
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['orders/changestatus'])?>",
                data: "&status=" + status + "&order_id=" + order_id,
                dataType: "html", 
                success: function (data) {
                    //alert(data);                
                }                
            });

        });

      
    }); 
</script>