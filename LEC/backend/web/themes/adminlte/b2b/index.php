<?php



use yii\helpers\Html;

use yii\grid\GridView;



/* @var $this yii\web\View */

/* @var $searchModel common\models\search\SupplierSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'B2B Dashboard';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
<div class="box-body">

<div class="b2b-index">



   

    <?php // echo $this->render('_search', ['model' => $searchModel]); 

            error_reporting(-1);

    ini_set('display_errors', 'On');

    ?>



    <?= GridView::widget([

        'dataProvider' => $stores,

        //'filterModel' => $searchModel,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],

            'title',

            'email:email',

            'billingAddress',

            'shippingAddress',

            [

                'header' => 'Suppliers',

                'value' => function($store){ return yii\helpers\Html::a('B2C', \Yii::$app->urlManager->createUrl(['b2b/suppliers', 'id' => $store->id])) ." | ". yii\helpers\Html::a('B2B', \Yii::$app->urlManager->createUrl(['b2b/suppliers', 'id' => $store->id, 'type' => 'b2b'])); },

                'format' => 'html'

            ],

            [

                'header' => 'Brands',

                'value' => function($store){ return yii\helpers\Html::a('B2C Assigned', \Yii::$app->urlManager->createUrl(['b2b/brands', 'id' => $store->id])). " | ".yii\helpers\Html::a('B2C Selectable', \Yii::$app->urlManager->createUrl(['b2b/allowedbrands', 'id' => $store->id]))." | ". yii\helpers\Html::a('B2B', \Yii::$app->urlManager->createUrl(['b2b/brands', 'id' => $store->id, 'type' => 'b2b'])); },

                'format' => 'html'

            ],

            [

                'header' => 'Products',

                'value' => function($store){ return yii\helpers\Html::a('B2C', \Yii::$app->urlManager->createUrl(['b2b/products', 'id' => $store->id])) ." | ". yii\helpers\Html::a('B2B', \Yii::$app->urlManager->createUrl(['b2b/products', 'id' => $store->id, 'type' => 'b2b'])); },

                'format' => 'html'

            ],

            //['class' => 'yii\grid\ActionColumn'],

        ],

    ]); ?>



</div>
</div>
</div>

