<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use kartik\export\ExportMenu;
//use kartik\grid\GridView; 
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'B2B Orders';
$this->params['breadcrumbs'][] = $this->title;



//die();
?>

<div class="row">
  <div class="col-sm-12">
    <div class="btn-right-sm">
        <?php // Html::a('Export as CSV', ['b2b/export'], ['class' => 'btn btn-success ']) ?>
       
    </div>
  </div>
</div>        

<div class="box">
<div class="box-body">
<div class="orders-index">
  <p class="create_pdt">
     <?=Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['b2b/export'], Yii::$app->request->queryParams)), ['class'=>'buttons-update btn btn-success enabled']); ?>
  </p>

    <?php /*GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'storeName',
            [
                'label' => 'Order Date',
                'attribute' => 'id',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::date($model->orderDate, "l jS \of F Y h:i:s A");
                },
            ],
            [
                'label' => 'Name',
                'attribute' => 'customer.fullName',
            ],
            [
                'label' => 'JE Number',
                'attribute' => 'customer.jenumber',
            ],
            [
                'label' => 'Grand Total',
                'attribute' => 'id',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::money($model->grandTotal);
                },
            ],
             'status',
            [
                'label'=> 'View',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('View',['b2b/orderdetail','id' => $model->id]);
                }
            ],
        ],
    ]);*/ ?>

    <?php
        $form = \kartik\form\ActiveForm::begin(['id' => 'b2borders-form']);
        \kartik\form\ActiveForm::end();
    ?>

    
    <?=\app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'   => function ($model, $key, $index, $grid) {
            return ['data-id' => $model->id];
        },
        //'summary'=>'',
        'options'=>['class'=>'box-body table-responsive no-padding grid-view'],
        'showHeader'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'storeName',
            /*[
                'label' => 'Order Date',
                'attribute' => 'id',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::date($model->orderDate, "l jS \of F Y h:i:s A");
                },
            ],*/


            [
                'label' => 'Order Date',
                'attribute' => 'id',
                'value' => function($model){ return \backend\components\Helper::date($model->orderDate); },
                'filter' => \kartik\field\FieldRange::widget([
                        'form' => $form,
                        'model' => $searchModel,
                        'template' => '{widget}{error}',
                        'attribute1' => 'orderDate_start',
                        'attribute2' => 'orderDate_end',
                        'type' => \kartik\field\FieldRange::INPUT_DATE,
                ]),
                'headerOptions' => ['class' => 'date-range']
            ],


            [
                'label' => 'Name',
                'attribute' => 'customer.fullName',
            ],
            [
                'label' => 'JE Number',
                'attribute' => 'customer.jenumber',
            ],
            /* 'billingAddressText',
             'shippingAddressText',*/

            [
                'label' => 'Grand Total',
                'attribute' => 'grandTotal',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::money($model->grandTotal);
                },
            ],
             'b2bStatus',
            [
                'label'=> 'View',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('View',['b2b/orderdetail','id' => $model->id]);
                }
            ],
        ],
    ]); ?>
    

</div>
</div>
</div>
