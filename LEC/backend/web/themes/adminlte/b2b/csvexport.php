<?php

use common\models\Products;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Orders;
use common\models\OrderItems;

if(isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"]))
	$start_date = date(date('Y-m-d 00:00:00', strtotime(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_start"])));
else
	$start_date = "";

if(isset(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"]))
	$end_date = date(date('Y-m-d 00:00:00', strtotime(Yii::$app->request->queryParams["OrdersSearch"]["orderDate_end"])));
else
	$end_date = "";

$items = ArrayHelper::map(OrderItems::find()->join('JOIN','Orders as o','orderId=o.id')->where('o.type="b2b" AND o.orderDate BETWEEN "'.$start_date.'" AND "'.$end_date.'"')->all(),'id','self');

$items = ArrayHelper::map(OrderItems::find()->join('JOIN','Orders as o','orderId=o.id')->where('o.type="b2b"')->all(),'id','self');


$output	= "";

$connection = Yii::$app->getDb();
$command = $connection->createCommand("SELECT user.jenumber,OrderItems.id,OrderItems.orderId,Orders.storeId, Orders.orderDate,Orders.customerNotes,Orders.grandTotal,B2bAddresses.storeName,AttributeValues.value,Products.sku,Products.typeId,OrderItems.price,OrderItems.conferenceId,OrderItems.quantity,OrderItems.comment, supplier.firstname FROM OrderItems 
        INNER JOIN Orders ON Orders.id=OrderItems.orderId AND Orders.type='b2b'
        INNER JOIN B2bAddresses ON B2bAddresses.id=OrderItems.storeAddressId 
        INNER JOIN Products ON Products.id=OrderItems.productId 
        INNER JOIN Attributes ON Attributes.code='name'
        INNER JOIN Users as supplier ON supplier.id= OrderItems.supplierUid
        INNER JOIN Users as user ON user.id= Orders.customerId 
        INNER JOIN AttributeValues ON AttributeValues.productId=OrderItems.productId 
        WHERE AttributeValues.attributeId=Attributes.id AND Orders.type='b2b' AND Orders.orderDate BETWEEN '$start_date' AND '$end_date'
        ORDER BY OrderItems.OrderId
    ");


/*$command = $connection->createCommand("SELECT user.jenumber,OrderItems.id,OrderItems.orderId,Orders.storeId, Orders.orderDate,Orders.customerNotes,Orders.grandTotal,B2bAddresses.storeName,AttributeValues.value,Products.sku,Products.typeId,OrderItems.price,OrderItems.conferenceId,OrderItems.quantity,OrderItems.comment, supplier.firstname FROM OrderItems 
        INNER JOIN Orders ON Orders.id=OrderItems.orderId AND Orders.type='b2b'
        INNER JOIN B2bAddresses ON B2bAddresses.id=OrderItems.storeAddressId 
        INNER JOIN Products ON Products.id=OrderItems.productId 
        INNER JOIN Attributes ON Attributes.code='name'
        INNER JOIN Users as supplier ON supplier.id= OrderItems.supplierUid
        INNER JOIN Users as user ON user.id= Orders.customerId 
        INNER JOIN AttributeValues ON AttributeValues.productId=OrderItems.productId 
        WHERE AttributeValues.attributeId=Attributes.id AND Orders.type='b2b'
        ORDER BY OrderItems.OrderId
    ");*/


$result = $command->queryAll();
$heder=array("Order Id", "Date", "Store Name","JE Number","Product Name", "SKU", "Price", "Quantity","Options","Supplier Name","Conference Product","Order Item Comment","Order Total","Order Comment");
foreach ($heder as  $value) {
 	$heading	=	$value;
	$output		.= '"'.$heading.'",';
}
$output .="\n";

$f=0;

foreach($result as $key=>$value)
	{ 
		if($value['orderId'] != $f)
			{
				if($value['conferenceId'] != NULL)
					$conference = "Yes"; 
				else
					$conference = "No";

				$output .='"'.$value['orderId'].'","'.$value['orderDate'].'","'.$value['storeName'].'","'.$value['jenumber'].'","","","","","","","","'.$value['grandTotal'].'","'.$value['customerNotes'].'"';
				$output .="\n";
				$output .= '"","","","","'.$value['value'].'","'.$items[$value['id']]->sku.'","'.$value['price'].'","'.$value['quantity'].'","'.strip_tags($items[$value['id']]->superAttributeValuesText).'","'.$value['firstname'].'","'.$conference.'","'.$value['comment'].'",""';
				$f=$value['orderId'];
			}

			else{
				$output .= '"","","","","'.$value['value'].'","'.$items[$value['id']]->sku.'","'.$value['price'].'","'.$value['quantity'].'","'.strip_tags($items[$value['id']]->superAttributeValuesText).'","'.$value['firstname'].'","'.$conference.'","'.$value['comment'].'",""';
			}
			$output .="\n";
	}		

$filename ='Order_'.date("Y-m-d").'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
echo $output;
exit;
	
?>
