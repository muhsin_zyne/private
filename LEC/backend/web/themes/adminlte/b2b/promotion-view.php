<?php
use common\models\User;
//use common\models\B2bAddresses;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\Helper;
use kartik\date\DatePicker;
use common\models\StorePromotions;
use yii\grid\GridView;
use yii\widgets\DetailView;

$this->title = $promotion->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
  <div class="col-sm-12">
    <div class="btn-right-sm">
    </div>
  </div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body">
				<div class="date_offer">
					<div class="offer">
						<b><span>Global Promotion Period:</span></b> <span><?=Helper::date($promotion->fromDate);?></span> <b>to</b> <span><?=Helper::date($promotion->toDate);?></span>
					</div>
					<div class="offer">
						<b><span>Indent Period:</span></b> <span><?=Helper::date($promotion->fromDate);?></span> <b>to</b> <span><?=Helper::date($promotion->costEndDate);?></span>
					</div>
					<div class="clear"></div>
				</div>

				<p>Leading Edge Jewellers has created a promotion, selling some of the products at a discounted price to boost sales. You may accept or reject this by clicking on the buttons. You can see the list of all products below.</p>
				<p>If you decide to Accept this promotion, these products will have the revised price from <?=Helper::date($promotion->fromDate);?>. At 12:01 am <?=Helper::date($promotion->toDate);?>, the price will revert back to normal.</p>
				<div class="promotions-select">
				<?= GridView::widget([
				    'dataProvider' => $dataProvider,
				    'columns' => ['pageId',['attribute'=>'product.name','label'=>'Item(s)'],['attribute'=>'product.ezcode','label'=>'EZCode'],'product.sku',['attribute'=>'product.price','label'=>'Original Selling Price'],'offerSellPrice'],
				]); ?>
				</div>
		    <div class="cost_html">
		    	<div class="inst_text">
					<b>Please set the selling period. (<span style="font-size: 11px;">required</span>)</b>
				</div>	
				<?php $form = ActiveForm::begin(['action'=>Url::to(['b2b/accept','id'=>$promotion->id])]); ?>
				<!-- <div class="from_field">
					<label>From Date</label>
					<input type="text" name="StorePromotion[startDate]" id="datestart" class="hasDatepicker startdate">
				</div>
				<div class="from_field">
					<label>End Date</label>
					<input type="text" name="StorePromotion[endDate]" id="dateend" class="hasDatepicker enddate">
				</div> -->

				<?=DatePicker::widget([
			        'form' => $form,
			        'model' => $promotion,
			        'attribute' => 'fromDate',
			        'value' => date('d-M-Y', strtotime('+2 days')),
			        'pluginOptions' => [
			        	'format' => 'yyyy-mm-dd',
			        ],
			 	]);
			    ?>

			    <?=DatePicker::widget([
			        'form' => $form,
			        'model' => $promotion,
			        'attribute' => 'toDate',
			        'pluginOptions' => [
			        	'format' => 'yyyy-mm-dd',
			        ],
			    ]);
			    ?>

				<div class="clear"></div>
			</div>
			<div class="btnarea">
				<input type="checkbox" class="agree" id="agree" name="StorePromotion[agree]"> <span> I agree to the terms and conditions above.</span>
				<input type="submit" value="I Accept This Promotion" name="accept" class="button accept btn btn-success ">
				<a href="<?=\yii\helpers\Url::to(['b2b/promotions']) ?>">
					<input type="button" value="Not Interested In This Promotion" name="reject" class="button reject btn btn-primary">
				</a>	
				<div class="clear"></div>
			</div>

			<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>	

<script type="text/javascript">
    $(document).ready(function () {
        $('#datestart').datepicker({
            format: "dd/mm/yyyy"
        }); 

    });
</script>


<script type="text/javascript">
    $(document).ready(function () {
		$('#dateend').datepicker({
            format: "dd/mm/yyyy",
            startDate: '2015-06-01',
        });  
    });
</script>    		

<script type="text/javascript">
	$(document).ready(function () {
		$('.accept').on("click", function(){   
			if($('.agree').prop( "checked" )){//return true;
				//alert('checked');
				return true;
			}
			else{ 
				alert('Please agree terms and conditions');
				return false;
			}
		});	
	});	
</script>