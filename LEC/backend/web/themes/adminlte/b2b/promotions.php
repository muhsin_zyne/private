<?php
use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use backend\components\Helper;


$this->title = 'Promotions';
$this->params['breadcrumbs'][] = $this->title;
$this->params['tooltip'] = "Here you shall find the list of promotions which you can opt-in.";
?>

<div class="box">
<div class="box-body">
<div class="orders-index">

    <?= \app\components\AdminGridView::widget([
    			'dataProvider' => $dataProvider,
    			'class'=>'data-table',
        		'columns' => [
        				'title',
        				[
        				'attribute'=>'fromDate',
        				'label'=>'Start Date',
        				'value' => function($model,$attribute){
        					return Helper::date($model->fromDate);
        				}
        				],
        				[
        				'attribute'=>'toDate',
        				'label'=>'End Date',
        				'value' => function($model,$attribute){
        					return Helper::date($model->toDate);
        				}
        				],
        				'shortTag',
        		['class' => 'yii\grid\ActionColumn','template' => '{view}']]
    		]); ?>

</div>
</div>
</div>
