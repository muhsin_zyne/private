<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Products;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $supplier common\models\Suppliers */

$this->title = strtoupper($type)." products of {$store->title}";
$this->params['breadcrumbs'][] = $store->title;
$this->params['breadcrumbs'][] = "Products";
?>

<?php 
$b2cProducts = array_values(ArrayHelper::map($store->products, 'id', 'id'));
$searchModel = new \common\models\search\ProductsSearch();
$dataProvider = $searchModel->search(Yii::$app->request->post());
//var_dump($searchModel->attributes); die;
$tickedProducts = ArrayHelper::map(($type=="b2c")? $store->products : array_merge($store->products, $store->b2bProducts), 'id', 'id');?>

<div class="box">
  <div class="box-body pg-float">
<?php $form = ActiveForm::begin(['method' => 'POST']);

 \yii\widgets\Pjax::begin(array('id' => 'store-products')); ?>
<div class="suppliers-view">
    <?= \app\components\AdminGridView::widget([
        'id' => 'store-products',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => array_merge([
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'Products',
                'contentOptions' => ['class' => 'check'],
                'checkboxOptions' => function($model, $key, $index, $column) use($tickedProducts, $b2cProducts, $type){
                                        if(in_array($model->id, $tickedProducts))
                                            return (in_array($model->id, $b2cProducts) && $type == "b2b")? ['value' => $model->id, 'uncheck' => $model->id."_unchecked", 'checked' => 'checked', 'disabled' => 'disabled'] : ['value' => $model->id, 'uncheck' => $model->id."_unchecked", 'checked' => 'checked'];
                                        else
                                            return ['value' => $model->id, 'uncheck' => $model->id."_unchecked"];
                                    }
            ],
            'name',
            'sku',
        ]),
    ]); ?>
</div>
<?php \yii\widgets\Pjax::end(); ?>
<div class="form-group ss">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
<?php $form = ActiveForm::end(); ?>
</div>
</div>
