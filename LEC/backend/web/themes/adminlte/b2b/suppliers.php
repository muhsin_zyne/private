<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Suppliers;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $supplier common\models\Suppliers */

$this->title = strtoupper($type)." suppliers of {$store->title}";
$this->params['breadcrumbs'][] = $store->title;
$this->params['breadcrumbs'][] = "Brands";
?>
<div class="box">
  <div class="box-body">
<?php $form = ActiveForm::begin(); 
	$b2cSuppliers = array_values(ArrayHelper::map($store->suppliers, 'id', 'id'));
?>
<div class="suppliers-view">
    <?= Html::checkBoxList("Suppliers", 
    	ArrayHelper::map(($type=="b2c")? $store->suppliers : array_merge($store->suppliers, $store->b2bSuppliers), 'id', 'id'), 
    	ArrayHelper::map(Suppliers::find()->andWhere("roleId='2'")->all(), 'id', 'firstname'), 
    	[
    		'unselect' => 'no-suppliers-selected',
    		'item' => function ($index, $label, $name, $checked, $value) use($b2cSuppliers, $type){ 
    				  	  return "<label><input type=\"checkbox\" name=\"Suppliers[]\" value=\"$value\" ".((in_array($value, $b2cSuppliers) && $type=="b2b")? "disabled=\"disabled\"" : "")." ".(($checked)? "checked=\"checked\"" : "")."><span>$label</span></label>"; 
    				  }
    	]
    	) ?>
</div>
<div class="form-group supplr">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
<?php $form = ActiveForm::end(); ?>
</div>
</div>