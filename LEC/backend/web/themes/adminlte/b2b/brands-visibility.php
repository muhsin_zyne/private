<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Brands;
use common\models\StoreBrandsVisibility;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $supplier common\models\Suppliers */

$this->title = strtoupper($type)." brands selectable for {$store->title}";
$this->params['breadcrumbs'][] = $store->title;
$this->params['breadcrumbs'][] = "Brands";
$visibleBrands = StoreBrandsVisibility::find()->where(['storeId'=>$id])->all();
?>
<?php $form = ActiveForm::begin(); 
	$b2cBrands = array_values(ArrayHelper::map($store->brands, 'id', 'id'));
    //var_dump($b2cBrands); die;
?>
<div class="box box-default">
<div class="box-body">
<div class="suppliers-view">

    <?= Html::checkBoxList("Brands",
    	ArrayHelper::map($visibleBrands,'id','brandId') + $b2cBrands, 
    	ArrayHelper::map(Brands::find()->all(), 'id', 'title'),
    	[
    		'item' => function ($index, $label, $name, $checked, $value) use($b2cBrands, $type){ 
    				  	  return "<div class='col-md-3 col-sm-6 brnd-check'><label><input type=\"checkbox\" name=\"Brands[]\" value=\"$value\" ".((in_array($value, $b2cBrands))? "disabled=\"disabled\"" : "")." ".(($checked)? "checked=\"checked\"" : "").">$label</label></div>"; 
    				  }
    	]
    	) ?>
 </div>

<div class="form-group brnd-btm">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
 </div>
</div>
<?php $form = ActiveForm::end(); ?>