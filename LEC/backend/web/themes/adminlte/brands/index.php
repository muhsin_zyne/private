<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brands';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">
<div class="brands-index"> 

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php if($adminUser->roleId == "1"){ ?>
        <p class="create_pdt">
            <?= Html::a('<i class="fa fa-plus"></i> Create Brands', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>
    <?php if($adminUser->roleId == "3") { ?>
        <p class="submit_enabled">
        <?= Html::button('Update',['class'=>'buttons-update btn btn-primary enabled']); ?>
        </p>
    <?php } ?>
    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            // [
            //     'label' => 'Status',
            //     'attribute' => 'enabled',
            //     'value' => function($model){
            //         return ($model->enabled == "1")? "Enabled" : "Disabled";
            //     }
            // ]
            [
                'attribute' => 'path',
                'format' => 'html',
                'label' => 'Logo',
                'value' => function ($data) {
                    return Html::img($data['logoUrl'],
                    ['width' => '120px']);
                },
            ],
            ($adminUser->roleId == "3")? [
                            'label' => 'Enabled',
                            'format' => 'raw',
                            'value' => function ($model) use($enabledBrands){
                                return '<input type="checkbox" class="enabled_checkbox chk-box" value="'.$model->id.'" name="StoreBrands[enabled]" '.(in_array($model->id, \yii\helpers\ArrayHelper::map($enabledBrands, 'id', 'brandId')) ? "checked" : "not").'  >';
                            },
                        ] :   [
				 'class' => 'yii\grid\ActionColumn',
				 'template' => '{view}{update}',
				 'buttons' => [
				 	'view' => function($url, $model){
							return '<a href="'.\yii\helpers\Url::to(['brands/view', 'id' => $model->id]).'" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>' ;
					},
					'update' => function($url, $model){
							return '<a href="'.\yii\helpers\Url::to(['brands/update', 'id' => $model->id]).'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>' ;
					},
					// 'delete' => function($url, $model){
					// 		return '<a href="'.\yii\helpers\Url::to(['brands/delete', 'id' => $model->id]).'" title="Delete" data-pjax="0"><span class="fa fa-trash"></span></a>';
					// }

                ]
			],
        ],
    ]); ?>

</div>
<?php if($adminUser->roleId == "3") { ?>
<p style="color:#a8a8a8;font-style:italic; /*margin-top: -15px;*/">Please save changes (if you have made any) by clicking the 'Update' button before navigating to the next page.</p>
<?php } ?>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {

    var enabledBrands = [];
    $('.enabled_checkbox:checked').each(function(){
            enabledBrands.push(this.value);
        });

    $("body").on("click", ".enabled", function(e){
        var currentEnabledBrands = [];
        $('.enabled_checkbox:checked').each(function(){
            currentEnabledBrands.push(this.value);
        });
        
        //console.log('enabledBrands:'+enabledBrands);
        //console.log('currentEnabledBrands:'+currentEnabledBrands);

        $.ajax({
            url:'<?=\yii\helpers\Url::to(["brands/enable"])?>',
            type: 'POST',
            data: {currentEnabledBrands : currentEnabledBrands, enabledBrands : enabledBrands},
            success: function(data)
                {
                    alert('Saved Successfully!');
                    enabledProducts = [];
                    $('.enabled_checkbox:checked').each(function(){ console.log(this.value);
                        enabledProducts.push(this.value);
                    });
                    /*console.log(data);
                    $(".error-msg").append('<div class="alert alert-success"><i class="icon fa fa-tick"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Product Disabled</div>');*/
                }
        });
    });     
}); 
</script>
