<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $brand common\models\Brands */

$this->title = 'Update Brands: ' . ' ' . $brand->title;
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $brand->title, 'url' => ['view', 'id' => $brand->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="brands-update">

    <?= $this->render('_form', compact('brand')) ?>

</div>
