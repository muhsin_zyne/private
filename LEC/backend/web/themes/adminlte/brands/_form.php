<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Suppliers;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use common\models\CmsImages;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model common\models\Brands */
/* @var $form yii\widgets\ActiveForm */

$user = User::findOne(Yii::$app->user->id);
if($user->roleId == 1)
    $storeId = 0;
else
    $storeId = $user->store->id;

?>
<div class="box">
<div class="categories-index">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs ">
            <li class="active"><a data-toggle="tab" href="#details">Brand Details</a></li>
            <li><a data-toggle="tab" href="#landing">Landing Page Content</a></li>
        </ul>
    </div>
</div>    

<div class="row">
    <div class="col-xs-12">
        
            <div class="box-body">
                <div class="brands-form">
                    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data'], 'enableAjaxValidation' => true]); ?>
                        <div class="tab-content responsive">
                            <div class="tab-pane active" id="details">
                                <?= $form->field($brand, 'title')->textInput(['maxlength' => 45]) ?>
                                <?= ($brand->isNewRecord)? $form->field($brand, 'suppliers')->dropDownlist(ArrayHelper::map(Suppliers::find()->all(), 'id', 'title'), ['multiple'=>'multiple']) : ''?>
                                <div class="brand-image" style="margin-bottom:10px;">
                                    <?php
                                        if(!empty($brand->path)){
                                            $pluginOptions = ['initialPreview' => [Html::img(Yii::$app->params["rootUrl"].$brand->path)],
                                                'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false
                                            ];
                                        }
                                        else{
                                             $pluginOptions = [
                                                'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false
                                            ];
                                        }
                                        echo '<label class="control-label">Logo</label>';
                                        echo FileInput::widget([
                                            'model' => $brand,
                                            'attribute' => 'path',
                                            'pluginOptions' => $pluginOptions 
                                        ]);
                                    ?>
                                </div>
                                <?= $form->field($brand, 'urlKey')->textInput(['maxlength' => 255]) ?>
                                <?= $form->field($brand, 'enabled')->dropDownlist(['1' => 'Enabled', '0' => 'Disabled'])->label('Status') ?> 
                                <div class="form-group">
                                    <?= Html::submitButton($brand->isNewRecord ? 'Create' : 'Update', ['class' => $brand->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                </div> 
                            </div>
                            <div class="tab-pane" id="landing">
                                <?= $form->field($brand, 'resolvedContent')->widget(CKEditor::className(), 
                                    [
                                        'id'=>'cmscontent',
                                        'preset' => 'full', 
                                        'clientOptions' =>[
                                            'language' => 'en', 
                                            'allowedContent' => true,
                                            'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['brands/image-upload']),
                                            'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['brands/image-browse']),
                                            ]
                                    ])->label('Page Content') ?>   
                                
                                <div class="form-group">
                                    <?= Html::submitButton($brand->isNewRecord ? 'Create' : 'Update', ['class' => $brand->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                </div>

                                

                            </div>
                        </div>    
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #feedback { font-size: 1.4em; }
    #selectable .ui-selecting { background:#ccc ; padding:5px; }
	
    #selectable .ui-selected { background: #ccc ; color: white; padding:5px; }
	
	.ui-selected .ui-selected{ padding:0 !important;}
    #selectable { list-style-type: none; margin: 0; padding: 0; width: 650px; }
    #selectable li { margin: 3px; padding: 1px; float: left; width: 150px; height: 120px; font-size: 4em; text-align: center; }
</style>    

<script>             
    var last_selected_id;
    function selectImages() {
        $("#selectable").selectable({
            selecting: function(event, ui) {        
                last_selected_id = ui.selecting.id;  
            } 
        }); 
    }
    function refreshImages() { 
        var d=1;
        $.ajax({
            type: "POST",
            url: "<?=Yii::$app->urlManager->createUrl(['brands/getimages'])?>", 
            data: "&d=" + d ,
            dataType: "html", 
            success: function (data) {
                $("#images_db").html(data);
                selectImages();              
            }                 
        });           
    }

    $(document).ready(function(){
        
        $("#image_button").click(function(){
            $("#dialog" ).dialog({
                height: 500,
                width: 750,
                modal: true,
                resizable: true,
                dialogClass: 'no-close success-dialog'
            });
        });      
        selectImages(); 
        $("#insert_button").click(function(){ 
            var imageid=last_selected_id;
            console.log(imageid);
            $("li#"+imageid+" img").attr('src');
            var a=$("li#"+imageid+" img").attr('src');
            CKEDITOR.instances['brands-resolvedcontent'].insertHtml('<img src="'+a+'">');     
            $('#dialog').dialog('close');  
                      
        });
    });
</script>
