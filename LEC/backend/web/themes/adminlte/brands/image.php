<!DOCTYPE html><head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/browser-popup.css">
<body>
<div class="fixed-div">
    <div class="container">
        <div class="fixed-div-con">
            <h1 class="">Previously Uploaded Images  </h1>  
            <input type="button" value="Insert" id="insert" class="btn btn-success">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div id="images_db">
            <ol id="popup-selectable">
                <?php foreach ($cmsImages as $key => $cmsImage){ ?>                    
                    <li class="ui-state-default col-xs-3" id="<?=Yii::$app->params["rootUrl"]?>/store/cms/images/<?=$cmsImage['title']?>">
                    	<div class="popup-selectable-img">
                        <img src ='<?=Yii::$app->params["rootUrl"]?>/store/cms/images/<?=$cmsImage['title']?>' width="140">
                        </div>
                    </li>                    
                <?php  } ?>
            </ol>          
        </div>
    </div>
</div>  

<script>
    $(document).ready(function(){
        $("#popup-selectable").selectable({
            stop: function(event, ui) { 
                last_selected_url = $('li.ui-selected').attr("id");
                //console.log(last_selected_id);                
                $("#insert").click(function(){
                    window.opener.CKEDITOR.tools.callFunction(1, last_selected_url , '');
                    window.close();
                });                                         
            }
        });    
    });
</script>

<script src="/js/jquery.hc-sticky.min.js"></script>
<script>
jQuery(document).ready(function($){
  $('.fixed-div').hcSticky();
});
</script>
</body>
</html>