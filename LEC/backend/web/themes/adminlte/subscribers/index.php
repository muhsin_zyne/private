<?php



use yii\helpers\Html;

use yii\grid\GridView;

use common\models\Stores;

use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Subscribers';

$this->params['breadcrumbs'][] = $this->title;

$this->params['tooltip'] = "This is the list of users who have subscribed via the initial popup";
$stores=Stores::find()->where(['isVirtual'=>0])->all();
$listData=ArrayHelper::map($stores,'id','title');
?>



    <!-- <p>

        <?= Html::a('Create Subscribers', ['create'], ['class' => 'btn btn-success']) ?>

    </p> -->

    <div class="box">

    <div class="box-body">

    

        <!-- <h3 class="box-title"><?= Html::encode($this->title) ?></h3> -->

<!--         <div class="box-tools">

            <div class="input-group">

                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search">

                <div class="input-group-btn">

                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>

                </div>

            </div>

        </div> -->

        <p class="submit_enabled create_pdt">

        <?= Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['subscribers/export'], Yii::$app->request->queryParams)), ['class'=>'btn buttons-update btn-success enabled']); ?>

        </p>


    <?php

        $form = \kartik\form\ActiveForm::begin(['id' => 'subscribers-form']);

        \kartik\form\ActiveForm::end();

    ?>

    <?=

    \app\components\AdminGridView::widget([

        'dataProvider' => $dataProvider,

        'filterModel' => $searchModel,

        //'summary'=>'',

        'options'=>['class'=>'box-body table-responsive no-padding subcr grid-view'],

        'showHeader'=>true,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],

            //'firstname',

            //'lastname',

            'email:email',

            
            [   
                'label' => 'Store Name',
                'attribute' => 'storeId',
                 'visible' => Yii::$app->user->identity->roleId != 3,
                'value' => function($model, $attribute){ 
                    return $model->store->title;
                },
                
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'storeId', $listData,
                    ['class'=>'form-control','prompt' => '']),
            ],
           
            [

                'attribute' => 'dateAdded',

                'value' => function($model){ return \backend\components\Helper::date($model->dateAdded); },

                'filter' => \kartik\field\FieldRange::widget([

                        'form' => $form,

                        'model' => $searchModel,

                        'template' => '{widget}{error}',

                        'attribute1' => 'createdDate_start',

                        'attribute2' => 'createdDate_end',

                        'type' => \kartik\field\FieldRange::INPUT_DATE,

                ])

            ],

            [

                'class' => 'yii\grid\ActionColumn',

                'template' => '{delete}',

                'buttons' => [

                    'delete' => function ($url, $model, $key) {

                        return '<a href="'.$url.'" title="Delete" data-pjax="0" data-confirm = "Are you sure you want to delete this item?"><span class="fa fa-trash-o"></span></a>' ;

                    },

                ]

            ]

        ],

    ]); ?>

    </div>

    </div>

