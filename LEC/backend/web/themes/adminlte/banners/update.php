<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */

$this->title = 'Update Banners: ' . ' ' . $model->type;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="banner-button">
	<a href="<?=\yii\helpers\Url::to(['banners/index']) ?>"><button name="back_button" class="btn btn-primary banner-back">Back</button></a>
</div>

<div class="banners-update">
	<?php /* $this->render('_updateform', ['model' => $model])*/ ?>
	<?=$this->render('_updateform',compact('model','type','listData'))?>
</div>
