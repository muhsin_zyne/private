<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Banners;
use yii\helpers\Url;
use common\models\StoreBanners;
use common\models\Configuration;
use common\models\User;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\Query;
?>
<?php //var_dump($type);die;// heading section
if($type=='b2c-main'){ $heading='B2C Main';$config_key='mainbanner_resolution';}
else if($type=='b2c-mini'){ $heading='B2C Mini'; $config_key='minibanner_resolution';}
else if($type=='b2c-side') { $heading='B2C Side'; $config_key='sidebanner_resolution';}
else if($type=='b2c-strip') {$heading='B2c Strip';$config_key='stripbanner_resolution';}
else if($type=='b2c-mobile'){$heading='B2C Mobile'; $config_key='mobilebanner_resolution';}
else if($type=='b2b-main')
    $heading='B2B Main';
else if($type=='b2b-mini')
   $heading='B2B Mini';
else if($type=='b2b-side')
    $heading='B2B Side';
else
    die('Sorry....Please check your Url.....');

$this->title = $heading.' Banners';//($id=='b2b')?'Banners':'B2C Banners';

$this->params['breadcrumbs'][] = $this->title;

if($id == "b2c"){
    $this->params['tooltip'] = "You can manage the banners to be displayed on your website here.";
}

$user = User::findOne(Yii::$app->user->id);
$store_id=($user->roleId==1)?'0':$user->store->id;
if($id=='b2c'){
    $banner_conf[Configuration::findSetting($config_key, $store_id)]=Configuration::findSetting($config_key, $store_id);
}
$banner_resolution=($user->roleId==1)? Yii::$app->params["$type"] :$banner_conf;
$model = new Banners;

?>
<div class="row">
    <!--  only in side banner  -->
    <?php if($user->roleId == 3 && $type=='b2c-side'){ ?>
        <div class="col-xs-12">
            <div class="box box-default color-palette-box">
                <div id="configuration" class="box-body">
                    <?php $form_conf = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']] ); ?>
                    <?php 
                        $conf=Configuration::find()->where(['storeId' =>$store_id ,'key' => 'is_sidebanner_facebook'])->one();
                        if($conf['value']=='1')
                        {  $model->isFacebook=1;}
                        else 
                        {  $model->isFacebook=0;}
                    ?>
                    <?= $form_conf->field($model, 'isFacebook')->dropDownList([ '0' => 'Banner', '1' => 'Facebook']); ?>
                    <input type="button" id="btn_conf" value="Save" class="btn btn-primary">
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
         </div>
     <?php } ?>
    <!--  only in side banner  -->
    <?php if($user->roleId == 3 && $type=='b2c-side'){ ?>
        <div class="col-xs-12" id="div_facebook">
            <div class="box box-default color-palette-box">
                <div class="box-body" >
                  <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-tag"></i> Set Facebook URL</h3>
                  </div>
                  <?php $form_face = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']] );?>
                  <?= $form_face->field($model, 'isFacebook')->hiddenInput( ['value' => 1])->label(false); ?>
                  <?php $facebook_id= Banners::find()->where( ['isFacebook' => 1,'storeId' => $store_id] )->one(); ?>
                  <?= $form_face->field($model, 'facebook_link')->textInput(['value' => $facebook_id['path']]) ?>
                  <?= Html::submitButton('Set', array('id' => 'face','class' => 'btn btn-primary')) ?>
                  <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    <?php } ?>


    <div class="col-xs-12" id="s_banner">
        <div class="box">            
            <div class="banners-form">
                <div class="color-palette-box">                                 
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-image-o"></i> Add <?=$heading?> Banner</h3>
                    </div>

                    <div class="box-body">
                        <?php 
                        $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','id'=>'main-banner-form']] );                        
                        $model->url_type=2;$model->type=$type;
                        ?>
                        <?= $form->field($model, 'main_image')->widget(FileInput::classname(), ['options'=>['accept'=>'image/*'],
                            'pluginOptions' => ['showPreview' => true,'showCaption' => true,'showRemove' => false,'showUpload' => false ]]); 

                            if($user->roleId == 3) {
                                echo $form->field($model, 'type')->hiddenInput( ['value' => $type])->label(false);
                                echo $form->field($model, 'storeId')->hiddenInput( ['value' => $store_id])->label(false);
                                //$main_b_category=Configuration::find()->where(['key'=>'mainbanner_resolution','storeId'=>Yii::$app->params['storeId']])->all();
                                //$listData=ArrayHelper::map($main_b_category,'value','value');
                                echo $form->field($model, 'category')->dropDownList($banner_conf);
                            }
                            else if($user->roleId == 1) {                                  
                                echo $form->field($model, 'type')->hiddenInput()->label(false);
                                echo $form->field($model, 'storeId')->hiddenInput( ['value' => 0])->label(false); 
                                if($id=='b2b')
                                    echo $form->field($model, 'category')->dropDownList($banner_resolution); 
                                else
                                    echo $form->field($model, 'category')->dropDownList($banner_resolution); 
                            }                           
                        ?>
                        <?= $form->field($model, 'url_type')->dropDownList(['2'=>'Web URL','1'=>'Youtube URL'],['class' => 'url-type']) ?>
                        <!--<?= $form->field($model, 'url_type')->radioList(array('1'=>'Youtube URL','2'=>' Web URL'))->label('URL Type'); ?>-->
                        <?= $form->field($model, 'link')->textInput(array('placeholder' => 'Youtube URL Example: https://www.youtube.com/watch?v=WW5zQMo5d6g  |  Web URL Example: www.mywebsite.com.au/page')) ?>
                        <span class="targets">
                        <?= $form->field($model, 'target')->dropDownList(['_sef'=>'Open in current tab','_blank'=>'Open in new tab']) ?>
                        </span>
                        <?= Html::submitButton('Create', array('id' => 'main','class' => 'btn btn-primary')) ?>
                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div> 
</div>
<!--  Only for main banners site admin -->
<?php if($user->roleId == 3 && $type=='b2c-main') { ?>
    <?php  
        $storeBanners=StoreBanners::find()->where(['storeId'=>$store_id])->orderBy(['position'=>SORT_ASC])->all();          
    ?>
    <div class="selctedbanners-bg">
        <div id="selctedbanners" class="box box-default color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-file-image-o"></i> Selected Banners</h3>
                <p class="drag-text">(Drag and drop banners to set default sequence.)</p>  
            </div>
            <div class="box-body">
                <div class="row">
                    <ul id="sortable">
                        <?php foreach ($storeBanners as $key => $storeBanner) {
                            $selected_banner=Banners::findOne($storeBanner->bannerId);
                            if($selected_banner->type=='b2c-main'){?>
                                <li id="<?=$selected_banner['id']?>"> 
                                <img src ='<?=Yii::$app->params["rootUrl"].$selected_banner['path']?>' class="img-sm" align="middle" > 
                                </li>
                            <?php }?>    
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>    
<?php }?>

<!--    banners shows     -->

<div class="box-body" id="banners_image">
    <?php if($user->roleId == 3 || $id=='b2b'){   ?>
        <button  class="btn btn-primary btn-sm save-float" id="main_save"  style="float: right;">Save</button>
    <?php } ?> 
    <?php foreach ($banner_resolution as $key => $main_banner_category) { ?>
        <div class="row">
        <div class="col-xs-12"><h3 class="av-float">List of <?=$heading?> Banners - <span> <?= $main_banner_category?></span></h3></div>
        <?php foreach ($banners_all as $index => $banner) { //var_dump($banner);die;
            
            if($main_banner_category==$banner->x.'px'.' X '.$banner->y.'px')
            { ?>
                <div class="col-md-4">
                    <div class="box box-solid">
                        <div class="box-body sm-checkox">
                            <?php if($user->roleId == 3) {  // site admin
                                if($banner['storeId']!=0){ ?>
                                    <u> <?= Html::a('<i class="fa fa-pencil"></i> Edit', ['/banners/update','id'=>$banner['id'],'type'=>$type]) ?></u> 
                                    <u> <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $banner['id']], 
                                        ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?></u>
                                <?php } else{   ?>                
                                    <u> <?= Html::a('<i class="fa fa-pencil"></i> Edit', ['/banners/update','id'=>$banner['id'],'id'=>$banner['id'],'type'=>$type]) ?></u>
                                <?php }
                            }
                            if($user->roleId == 1) { // master admin?>
                                <u> <?= Html::a('<i class="fa fa-pencil"></i> Edit', ['/banners/update','id'=>$banner['id'],'id'=>$banner['id'],'type'=>$type]) ?> </u> 
                                <u><?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $banner['id']], 
                                        ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?></u>
                            <?php } ?>
                            <?php if($user->roleId == 3){ //site admin
                                if(!in_array($banner['id'], $store)) 
                                { 
                                    echo $form->field($model, 'main_check['.$banner['id'].']')->checkbox(array('label'=>'')); 
                                }
                                else 
                                { 
                                    $selected_banners[] = $banner;
                                    $model->main_check[$banner['id']] = true;
                                    echo $form->field($model, 'main_check['.$banner['id'].']')->checkbox(array('label'=>'')); 
                                }
                            } ?> 

                            <?php if($id=='b2b'){   // only for b2b users                          
                                $b2b_banners_all=yii\helpers\ArrayHelper::getColumn(StoreBanners::find()->where(['storeId'=>-1])->all(), 'bannerId');
                                if(!in_array($banner['id'], $b2b_banners_all)) { 
                                    echo $form->field($model, 'main_check['.$banner['id'].']')->checkbox(array('label'=>'')); 
                                }
                                else 
                                { $model->main_check[$banner['id']] = true;
                                    echo $form->field($model, 'main_check['.$banner['id'].']')->checkbox(array('label'=>''));
                                }
                            }?>  

                            <img src ='<?=Yii::$app->params["rootUrl"].$banner['path']?>' class="img-sm" align="middle" >
                
                        </div>
                    </div>
                </div>
            <?php } //if  ?>
        <?php }  ?>
        </div>
    <?php } ?>
</div>

<script type="text/javascript">    
    $(document).ready(function(){
        /* side banner facbook and banner div hide and show */
        <?php $conf=Configuration::find()->where(['storeId' =>$store_id ,'key' => 'is_sidebanner_facebook'])->one(); 
        //var_dump($conf->value);die;
        if($conf['value']=='1' && $user->roleId == 3 && $type=='b2c-side') { ?>         
            $("#s_banner").hide();
            $("#banners_image").hide();
        <?php  }  else { ?>
           $("#div_facebook").hide();
        <?php }?>

        $('#btn_conf').click(function(){   //------save to config table for facebook/banner configration
            var fac=$('#banners-isfacebook').val();
            var store_id=<?= $store_id ?>;
            //alert(store_id);
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['banners/configuration'])?>",
                data: "&fac=" + fac + "&store_id=" + store_id,
                dataType: "html", 
                success: function (data) {
                    alert(data);                
                }                
            });
        });
        /*  facebook\banner  div on change hide and show */
        $("#banners-isfacebook").change(function(){
           var fac=$('#banners-isfacebook').val();
           //alert(fac);
            if($("#banners-isfacebook").val()=='1')
            {
                $("#s_banner").hide();
                $("#div_facebook").show();
                $("#banners_image").hide();
            }
            else
            {
                $("#s_banner").show();
                $("#div_facebook").hide();
                $("#banners_image").show();
            }
        });

        $('#main_save').click(function(){ //b2c  banner save to StoreBanners            
            var site=[];
            var i=0;
            var type="<?=$type?>";
            if(type=='b2c-strip' && $(":checkbox:checked").length >1){  //------------check banner length          
                alert('Please choose only one strip banner!.. ');return false;
            } else if(type=='b2c-side' && $(":checkbox:checked").length >1){
                alert('Please choose only one side banner!.. ');return false;
            } else if(type=='b2c-mini' && $(":checkbox:checked").length >2){
                alert('Please choose only two mini banner!.. ');return false;
            }
            <?php foreach ($banners_all as $index => $banner) { ?> 
                var chk =<?= $banner['id']?>;
                if($("#banners-main_check-"+chk).is(":checked"))
                {
                    site[i]=chk;i++;
                }
            <?php }  ?>
            /*if(i>6)
                alert('Sorry you can select a maximum of 6 banners only!');return false;
            if(i<1)
                alert('Sorry you can please select a minimum of 1 banners!');return false;
            */
            $.ajax({
                type: "POST",
                url: "<?=Yii::$app->urlManager->createUrl(['banners/assignbanners'])?>",
                data: "&site=" + site + "&type=" + type ,
                dataType: "html", 
                success: function (data) {
                //alert(data);                
                }                
            });
        });
    });
</script>
<style> /* sreedev  banner drag and drop */
  #sortable { list-style-type: none; margin: 0; padding: 0; margin-left: 10px; }
  #sortable li { margin: 3px 3px 3px 0; padding: 5px; display: inline; font-size: 4em; text-align: center; }
  #sortable li img{width:170px;}
</style>
<script>
    $(function() {
        $( "#sortable" ).sortable({
        // axis:'x',
            stop:function(event, ui){
                var data = $(this).sortable('toArray');
                console.log(data);
                $.ajax({
                    data: {banners:data,store_id:<?=$store_id?>},
                    type:'POST',
                    url:"<?=Yii::$app->urlManager->createUrl(['banners/sortbanners'])?>",
                })
            }
        });
        $( "#sortable" ).disableSelection();
    });
 </script>