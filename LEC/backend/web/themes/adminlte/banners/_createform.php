<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Banners;
use yii\helpers\Url;
//use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Banners */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php
$this->title = 'B2C Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">
    <div class="nav-tabs-custom">                                
        <ul class="nav nav-tabs pull-right">
            <li class="active"><a data-toggle="tab" href="#main-banner">Main Banner</a></li>
            <li><a data-toggle="tab" href="#mini-banner">Mini Banner</a></li>
            <li><a data-toggle="tab" href="#side-banner">Side Banner</a></li>            
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="tab-content responsive">
        <?php if(Yii::$app->session->getFlash('error'))
          { ?>
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Error!</h4>                          
            <?= Yii::$app->session->getFlash('error'); ?>       
          </div> 
        <?php } ?>
            <div class="tab-pane active" id="main-banner">
                <div class="banners-form">                  
                    <div class="box box-default color-palette-box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Add Main Banner</h3>
                        </div>
                        
                        <div class="box-body">
                            <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']] ); 
                            $model = new Banners;
                            $model->type='b2c-main';?>
                            <?= $form->field($model, 'type')->hiddenInput()->label(false); ?> 
                            <?= $form->field($model, 'main_image')->widget(FileInput::classname(), ['options'=>['accept'=>'image/*'],
                            'pluginOptions' => ['showPreview' => true,'showCaption' => true,'showRemove' => false,'showUpload' => false ]]); ?>

                            <?= $form->field($model, 'category')->dropDownList([ '1000X400' => '1000px X 400px']); ?>
                            <?= $form->field($model, 'url_type')->radioList(array('1'=>'Youtube ID','2'=>' URL')); ?>
                            <?= $form->field($model, 'link')->textInput() ?>
                            <?= Html::a('Create1', ['create'], ['class'=>'btn btn-info btn-sm']) ?>                            
                                     <?= Html::submitButton('Create', array('id' => 'main','class' => 'btn btn-primary')) ?>
                            
                        <?php ActiveForm::end(); ?>
                        </div><!-- /.row -->            
                    </div><!-- /.box -->                    
                </div>
                <h3><strong> List of Main Banners - 1000px X 400px </strong></h3>
                <?php 
                $main_banner_all=Banners::find()->where( ['type' => 'b2c-main'] )->all();
                //print_r($main_banner);
                foreach ($main_banner_all as $index => $main_banner) 
                {
                ?>                          
                    <div class="col-md-6">
                        <div class="box box-solid">                             
                            <div class="box-body">
                                <img src ='<?=Yii::$app->params["rootUrl"].$main_banner['path']?>' height="200" width="500" align="middle" >
                                <u><?= Html::a('Edit', ['/banners/update','id'=>$main_banner['id']]) ?></u>
                                <u><?= Html::a('Delete', ['delete', 'id' => $main_banner['id']], 
                                    ['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?></u>                                
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                <?php
                }
                ?>            
            </div>
            
            <div class="tab-pane" id="mini-banner">
                <div class="banners-form">
                    <div class="box box-default color-palette-box">
                         <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Add Mini Banner</h3>
                        </div>
                           <div class="box-body">
                      <?php $form_mini = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']] ); 
                          $model = new Banners;$model->type='b2c-mini';?>
                          <?= $form_mini->field($model, 'type')->hiddenInput()->label(false); ?>   
                            <?= $form_mini->field($model, 'mini_image')->widget(FileInput::classname(), ['options'=>['accept'=>'image/*'],
                             'pluginOptions' => ['showPreview' => true,'showCaption' => true,'showRemove' => false,'showUpload' => false ]]); ?>
                            <?= $form_mini->field($model, 'category')->dropDownList([ '312X158' => '312px X 158px']); ?>
                            <?= $form_mini->field($model, 'url_type')->radioList(array('1'=>'Youtube ID','2'=>' URL')); ?>
                            <?= $form_mini->field($model, 'link')->textInput() ?>
                            <?= Html::submitButton('Create', array('id' => 'mini','class' => 'btn btn-primary')) ?>
                        <?php ActiveForm::end(); ?>
                    </div><!-- /.row -->            
                    </div>
                </div>      
                <h3><strong> List of Mini Banners - 312px X 158px </strong></h3>
                
                <?php 
                    $mini_banner_all=Banners::find()->where( ['type' => 'b2c-mini'] )->all();
                    foreach ($mini_banner_all as $index => $mini_banner) 
                    {
                    ?>                        
                        <div class="col-md-4">
                            <div class="box box-solid">                
                                <div class="box-body">
                                    <img src ='<?=Yii::$app->params["rootUrl"].$mini_banner['path']?>' height="200" width="500" align="middle" >
                                    <u><?= Html::a('Edit', ['/banners/update']) ?></u>
                                    <u><?= Html::a('Delete', ['/controller/action']) ?></u>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    <?php
                    }
                ?>
               
            </div>

            <div class="tab-pane" id="side-banner">             
                <div class="banners-form">
                    <div class="box box-default color-palette-box">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-tag"></i> Add Side Banner</h3>
                        </div>
                        <div class="box-body">
                            <?php $form_side = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']] ); 
                            $model = new Banners;$model->type='b2c-side';?>
                            <?= $form_side->field($model, 'type')->hiddenInput()->label(false); ?>   
                            <?= $form_side->field($model, 'side_image')->widget(FileInput::classname(), ['options'=>['accept'=>'image/*'],
                             'pluginOptions' => ['showPreview' => true,'showCaption' => true,'showRemove' => false,'showUpload' => false ]]); ?>
                            <?= $form_side->field($model, 'category')->dropDownList([ '285X346' => '285px X 346px']); ?>
                            <?= $form_side->field($model, 'url_type')->radioList(array('1'=>'Youtube ID','2'=>' URL')); ?>
                            <?= $form_side->field($model, 'link')->textInput() ?>
                            <?= Html::submitButton('Create', array('id' => 'side','class' => 'btn btn-primary')) ?>
                        <?php ActiveForm::end(); ?>
                        </div><!-- /.row -->            
                    </div>
                </div>                            
                <h3><strong> List of Side Banners - 285px X 346px</strong></h3>
                <?php 
                $side_banner_all=Banners::find()->where( ['type' => 'b2c-side'] )->all();
                foreach ($side_banner_all as $index => $side_banner) 
                {
                ?>
                <div class="col-md-3">
                    <div class="box box-solid">                
                        <div class="box-body">
                            <img src ='<?=Yii::$app->params["rootUrl"].$side_banner['path']?>' height="200" width="500" align="middle" >
                            <u><?= Html::a('Edit', ['/banner/update']) ?></u>
                            <u><?= Html::a('Delete', ['/controller/action']) ?></u>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
                <?php } ?>    
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">    
    function refreshImages() { }
</script>
