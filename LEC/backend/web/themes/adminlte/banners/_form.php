<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">
	<div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-tag"></i> Update Main Banner</h3>
        </div>
        <?php if(Yii::$app->session->getFlash('error'))
        { ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban"></i> Error!</h4>                    			
                <?= Yii::$app->session->getFlash('error'); ?>       
            </div> 
        <?php } ?>
        <div class="box-body">
        	<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
            <?php  
            Yii::$app->params['uploadPath'] = Yii::$app->basePath."/.."; 
               	echo FileInput::widget([
        		'model' => $model,
        		'attribute' => 'path',
        		'pluginOptions' => [
            	'initialPreview'=>[Html::img(Yii::$app->params["rootUrl"].$model->path)],'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false] ]); 
            ?>    
                
    		<?= $form->field($model, 'category')->dropDownList([ '1000X400' => '1000px X 400px']); ?>
          	<?php   
            if(isset($model->html)) 
            { 
                $model->url_type=1;
                $model->link=$model->html; 
            } 
            else
            { 
                $model->url_type=2;
                $model->link=$model->url; 
            }  ?> 
            <?php $model->demo_path=$model->path;  ?>
          	<?=$form->field($model, 'demo_path')->hiddenInput()->label(false); ?>
            <?=$form->field($model, 'type')->hiddenInput()->label(false); ?>
    		<?= $form->field($model, 'url_type')->radioList(array('1'=>'Youtube ID','2'=>' URL')); ?>
    		<?= $form->field($model, 'link')->textInput() ?>
   

    		<div class="form-group">
        		<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    			</div>

    		<?php ActiveForm::end(); ?>
    	</div>
    </div>
</div>

    

</div>
<script type="text/javascript">
    
    function refreshImages() { }

</script>
