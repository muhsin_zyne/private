<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConsumerPromotionProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consumer Promotion Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-promotion-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Consumer Promotion Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'promotionId',
            'productId',
            'offerSellPrice',
            'offerCostPrice',
            // 'pageId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
