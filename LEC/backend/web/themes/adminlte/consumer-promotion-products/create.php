<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotionProducts */

$this->title = 'Create Consumer Promotion Products';
$this->params['breadcrumbs'][] = ['label' => 'Consumer Promotion Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-promotion-products-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
