<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotionProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consumer-promotion-products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'promotionId')->textInput() ?>

    <?= $form->field($model, 'productId')->textInput() ?>

    <?= $form->field($model, 'offerSellPrice')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'offerCostPrice')->textInput(['maxlength' => 6]) ?>

    <?= $form->field($model, 'pageId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
