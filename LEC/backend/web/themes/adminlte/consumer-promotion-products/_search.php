<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotionProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consumer-promotion-products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'promotionId') ?>

    <?= $form->field($model, 'productId') ?>

    <?= $form->field($model, 'offerSellPrice') ?>

    <?= $form->field($model, 'offerCostPrice') ?>

    <?php // echo $form->field($model, 'pageId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
