<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotionProducts */

$this->title = 'Update Consumer Promotion Products: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Consumer Promotion Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="consumer-promotion-products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
