<?php
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\helpers\Url;
use frontend\components\Helper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CmsPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Content Blocks';
$this->params['breadcrumbs'][] = $this->title;
$this->params['tooltip'] = "Please manage your content pages here.";
$stores=common\models\Stores::find()->where(['isVirtual'=>0])->all();
$listData=ArrayHelper::map($stores,'id','title');
$user = User::findOne(Yii::$app->user->id);
?>
<div class="box">
    <div class="box-body">
        <div class="cms-pages-index">
            <p class="pull-right create_pdt">
                <?= Html::a('<i class="fa fa-plus"></i> Create Category Content', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php \yii\widgets\Pjax::begin(['id' => 'category-content',]); ?>

                <?= \app\components\AdminGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',
                        //'type',
                        //'categories.title',
                        [
                            'label' => 'Category Name',
                            'format' => 'html',
                            'attribute' => 'categories.title',
                            'value' => 'categories.title',
                           
                        ],
                        [        
                            'attribute' => 'type',
                            'value' => 'type',
                            'visible' => false,
                        ],

                        [
                            'header' => 'Stores',
                            'visible'=>($user->roleId==1)?true:false,
                            'value' => function($model, $attribute){ return implode(", ", \yii\helpers\ArrayHelper::map($model->stores, 'id', 'title')); },

                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'storeId', $listData,['class'=>'form-control','prompt' => '']),                  

                        ],

                        'status' => [
                            'value' => function($model, $attribute){ return $model->status=="1"? "Enabled" : "Disabled"; },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Disabled', '1' => 'Enabled'],['class'=>'form-control','prompt' => '']),
                        ],

                        //['class' => 'yii\grid\ActionColumn'],
                        ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],

                    ],

                ]); ?>
            <?php  \yii\widgets\Pjax::end();  ?>     
        </div>
    </div>
</div>

