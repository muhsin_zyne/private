<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CmsBlocks */

$this->title = 'Update Category Content Blocks: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cms Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cms-blocks-update">
<?= $this->render('_form', compact('model','storeselected')) ?>

    

</div>
