<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CmsBlocks */

$this->title = 'Create Category Content Blocks';
$this->params['breadcrumbs'][] = ['label' => 'Cms Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cms-blocks-create">

    
	<?= $this->render('_form', compact('model')) ?>
    

</div>
