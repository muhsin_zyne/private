<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Stores;
use common\models\User;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use \kartik\widgets\FileInput;
use yii\helpers\Url;
use common\models\CmsBlockImages;
use common\models\StoreCmsBlocks;
use backend\components\TreeView ;
use common\models\Categories;
/* @var $this yii\web\View */
/* @var $model common\models\CmsblockPages */
/* @var $form yii\widgets\ActiveForm */

$user = User::findOne(Yii::$app->user->id);
if($user->roleId == 1)
    $storeId = 0;
else
    $storeId = $user->store->id;
?>

<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel='stylesheet' href='/css/easyTree.css' />
<link rel='stylesheet' href='/css/style_treeview.css' />
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
               
                <?= $form->field($model, 'slug')->hiddenInput(['value'=>rand(1,10000)])->label(false) ?> 

                <?= $form->field($model, 'type')->hiddenInput(['value'=>'category_content'])->label(false) ?>   

                <?php 
                    //$categoriespos=Categories::find()->orderBy('parent')->all();
                    //$listData=ArrayHelper::map($categories,'id','title');
                    //echo $form->field($model, 'categoryId')->dropDownList($listData,['prompt'=>'Please Select...'])->label('Category Name');
                ?>
                <div class="cat-dp">
                 <i class="fa fa-angle-down"></i>
                <b>Select Categories</b>
                <?php $catname=(isset($model->categoryId))?$model->categories->title:NULL;?>
                <input type="text" class="btn-group form-control" name="" id="cattext" placeholder="----Please Select-----"  autocomplete="off" value="<?=$catname?>"></input>
                 <input type="hidden" value="<?=$model->categoryId?>" name="CmsBlocks[categoryId]" id="cmsblocks-categoryid">
                    <div  id="catlist">                   
                    <?php  
                    if(!$model->isNewRecord) {
                        $categoryIds=array($model->categoryId);
                        if(!is_null($categoryIds))  {   ?>
                            <input type="hidden" id="<?=implode(', ', array_keys($categoryIds))?>"  value='<?=implode(', ', array_keys($categoryIds))?>' name="kv-products" class="form-control kv-products hide" id="w0">
                            <?php foreach ($categoryIds as $categoryId ) {   //var_dump($categoryId);die;?> 
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        var key = <?=$categoryId; ?>;                                       
                                        $( ".selectcat" ).each(function() {
                                            if($(this).val() == key){ 
                                                $(this).attr('checked','true');
                                                //alert($(this).val());
                                                $('.kv-products').val($(this).val());
                                            }
                                        });
                                    });
                                </script>
                            <?php 
                            }
                        }
                    }
                    else
                    { ?>
                        <input type="hidden" value="" name="kv-products" class="form-control kv-products hide">
                    <?php } ?>
                    <?=TreeView::widget()?>
                    </ul>
                </div>
                </div>
                <?php
                //die;
                    $data = ArrayHelper::map(Stores::find()->where(['isVirtual'=>0])->all(), 'id', 'title'); 
                    //var_dump($data);die;

                    if($user->roleId == 1) { 
                        $store_selected[]=0;                            
                        if ($this->context->action->id == 'update') 
                        { 
                            $storePages= StoreCmsBlocks::find()->where(['blockId'=>$model->id])->all();
                            foreach ($storePages as $key => $storePage) {
                                $store_selected[]=$storePage->store->title;
                            }                                
                            
                            $htmlOptions = array('multiple' => 'true');
                            echo $form->field($model,'selected_store')->listBox($data, $htmlOptions);
                        }
                        if ($this->context->action->id == 'create') 
                        {
                            //$data = ArrayHelper::map(Stores::find()->all(), 'id', 'title'); 
                            $htmlOptions = array('multiple' => 'true');
                            echo $form->field($model,'selected_store')->listBox($data, $htmlOptions);
                        }
                        //echo Html::activeDropDownList($model, 'storeId',ArrayHelper::map(Stores::find()->all(), 'id', 'title')); 
                    }   
                    else 
                    {
                        echo '<input type="hidden" id="cmsblocks-selected_store" name="CmsBlocks[selected_store][]" value="'.$storeId.'">';
                    }
                    //var_dump($store_selected);die; 
                ?>
                
                
                
                <?= $form->field($model, 'content')->widget(CKEditor::className(), 
                    [
                        'id'=>'cmscontent',
                        'preset' => 'full', 
                        'clientOptions' =>[
                            'language' => 'en', 
                            'allowedContent' => true,
                            'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['category-blocks/image-upload']),
                            'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['category-blocks/image-browse']),
                            ]
                    ]) ?>   
                <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']); ?>


                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>               
                <?php  ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<script>
  $(document).ready(function(){ 
        <?php if($user->roleId == 1){ ?>
            $("#cmsblocks-selected_store option").each(function(){            
                <?php foreach ($store_selected as  $store_sel)             
                { ?>            
                    //var a= <?= $store_sel ?>;                
                    if ($(this).text() == "<?=$store_sel?>")
                    {  
                        $(this).attr("selected","selected");
                    }                
                <?php } ?>
            });
        <?php } ?>
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dd').nestable('collapseAll');
        //$('.dd').nestable('expandAll');
        $('#catlist').hide();  
        $("#cattext").click(function(){ 
            $('#catlist').toggle();
        });              
    });
</script>
<script type="text/javascript">
    /*$(document).ready(function(){  //limit chk checked one
        $('.selectcat').on('ifChanged', function(e){
            if($(this).is(':checked')){                
                $("div").removeClass("checked");
                $(this).parents('div').addClass("checked");                
                //$('input[type=checkbox]').attr('disabled',true);
                //$(this).attr('disabled',false);
                //var id=$('.selectcat:checked').val();
                var id=this.value;
                $.ajax({
                    type: "POST",
                    url: "<?=Yii::$app->urlManager->createUrl(['category-blocks/categoryname'])?>",             
                    data: "&id=" + id ,
                    dataType: "html", 
                    success: function (data) {
                        $('#cattext').val(data);                                   
                    }                 
                });  
                //$('#cmsblocks-categoryid').val($('.selectcat:checked').val());
                $('#cmsblocks-categoryid').val(id);
                $('#catlist').hide();
            }
            else{
                $('input[type=checkbox]').attr('disabled',false);
                var catid=null;
                $('#cattext').val(catid); 
                $('#cmsblocks-categoryid').val(catid);
            }            
        });        
    });*/
</script>
<script type="text/javascript">
    $(document).ready(function(){  //limit chk checked one
         $(function(){
            $('.selectcat').on('change', function() { 
                if($(this).is(':checked')) {
                    var id=this.value;                    
                    $("input:checkbox").attr("checked", false);                    
                    $(this).prop('checked','checked');
                     $.ajax({
                        type: "POST",
                        url: "<?=Yii::$app->urlManager->createUrl(['category-blocks/categoryname'])?>",             
                        data: "&id=" + id ,
                        dataType: "html", 
                        success: function (data) {
                            $('#cattext').val(data);                                   
                        }                 
                    });  
                    //$('#cmsblocks-categoryid').val($('.selectcat:checked').val());
                    $('#cmsblocks-categoryid').val(id);
                    $('#catlist').hide();
                  }
                  else{
                    $('input[type=checkbox]').attr('disabled',false);
                    var catid=null;
                    $('#cattext').val(catid); 
                    $('#cmsblocks-categoryid').val(catid);
                }    
        
            });
        }); 
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){  // ***aaron***  23/06/2016      
        $('.dd3-content').click(function(){  //alert($(this).children("i"));        
            $(this).children("i").toggleClass('fa-plus fa-minus');
            $(this).children("i").parent().next().toggle();
        });
        $('.dd3-content .fa-plus, .dd3-content .fa-minus').click(function(){
            $(this).toggleClass('fa-plus fa-minus');           
            $(this).parent().next().toggle();
        });
    });    
</script>
<script type="text/javascript">
    var mouse_is_inside = false;  // dropdown outside click close the div @ aaron 15/07/2016
    $(document).ready(function()
    {
        $('#catlist').hover(function(){ 
            mouse_is_inside=true; 
        }, function(){ 
            mouse_is_inside=false; 
        });

        $("body").mouseup(function(){ 
            if(! mouse_is_inside) $('#catlist').hide();
        });
    });
</script>