<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ConferenceTrays */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conference-trays-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($tray, 'title')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($tray, 'image')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($tray, 'conferenceId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($tray->isNewRecord ? 'Create' : 'Update', ['class' => $tray->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
