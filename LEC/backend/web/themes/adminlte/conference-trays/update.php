<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ConferenceTrays */

$this->title = 'Update Conference Trays: ' . ' ' . $tray->title;
$this->params['breadcrumbs'][] = ['label' => 'Conference Trays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $tray->title, 'url' => ['view', 'id' => $tray->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conference-trays-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'tray' => $tray,
    ]) ?>

</div>
