<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;
use yii\grid\GridView;
use kartik\tree\TreeView;
use common\models\Categories;
use common\models\ProductCategories;
use backend\components\Helper;
use \kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\ConferenceTrays */
/* @var $form yii\widgets\ActiveForm */
?>

<?php //var_dump($tray->trayProducts);die(); ?> 

<div class="conference-trays-form">

    
    <?php /*$form = ActiveForm::begin(['method'=>'post','action'=>['conferencetrays/test'],]);*/ ?> 

    <?php $form = ActiveForm::begin(); ?>

   	
	<label class="control-label" for="products-size">Title</label>
	<?= Html::activeInput('text',$tray, 'title',$options=['maxlength'=>45,'class'=>'form-control']) ?>

    <label class="control-label" for="products-size">Tray Id</label>
	<?= Html::activeInput('text',$tray, 'trayId',$options=['maxlength'=>45,'class'=>'form-control']) ?>

  	<label class="control-label" for="products-size">Image</label>
	<?=Html::activeInput('file',$tray, 'image',$options=['class'=>'form-control']) ?> 

 

    <div class="add_tray_products" style="width:100%">
		<div class="categories">
				<?php  	/*echo \kartik\tree\TreeViewInput::widget([ 
						   	'query' => \common\models\Categories::find()->addOrderBy('root, lft'),
						    'headingOptions'=>['label'=>'Categories'],
						    'name' => 'kv-product', 
						   	'asDropdown' => false, 
						    'multiple' => true, 
						    'fontAwesome' => true, 
						    'rootOptions' => [
						    'label'=>'<i class="fa fa-tree"></i>', 
						    'class'=>'text-success'
						    ],
						]);*/
				?>

				<?= Html::textInput('category','',$options=['class'=>'category_search','style'=>'','placeholder'=>'Enter Category','autocomplete'=>'off']) ?>
				<span>When you type the letter, result will get displyed below.</span>	
				<div class="product_box">
					<div class="category_product"></div>
					<?= Html::a('Load products','javascript:;',$options = ['class'=>'btn btn-primary load_pdt']) ?>
				</div>	
		</div>
		<div class="search">
			<label class="kv-heading-container" for="products-size">Search</label>
			<?= Html::textInput('search','',$options=['class'=>'product_search','style'=>'','placeholder'=>'Enter SKU or name of a product','autocomplete'=>'off']) ?>
			<span>When you type the letter, result will get displyed below.</span>
			<div class="product_box">
				<div class="product_list"></div>	
				<div class="form-group">
	        		<?= Html::a('add to list','javascript:;',$options = ['class'=>'btn btn-primary add_pdt']) ?>
	    		</div>
	    	</div>
	    </div>

	    <div class="clear"></div>

	</div>

	<div class="clear"></div>

	<div class="deleted_items"></div>

	<div class="tray_products_table" style="display:none">
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
	  		<thead>
		        <tr class="headings">
		        	<th></th>
		            <th>Name</th>
		            <th>SKU</th>
		            <th>Original cost price</th>
		            <th>Offer cost price</th>
		            <th>Remove</th>
		        </tr>
	    	</thead>
	    	<tbody>
	    	</tbody>	
	    </table>	
	</div>

	<div class="form-group" style="margin-top:20px;margin-bottom:30px">
    <?= Html::submitButton($tray->isNewRecord ? 'Submit' : 'Update', ['class' => $tray->isNewRecord ? 'btn btn-success' : 'btn btn-primary','data-before-submit'=>"setConfig();"]) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
$(document).ready(function() {

	addedproducts = [];
    listedproducts = [];
    

	$(".product_search").on('keyup',function(e){
		
		$('.selected:checked').each(function(){
            if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                addedproducts.push($(this).attr('value'));
            }    
        });
        
        $(".tray_products_table table tbody tr").each(function(){ 
            if($(this).attr('data-productId')) {
                if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                    listedproducts.push($(this).attr('data-productId')); 
                }
            }        
        });

		var keyword = $('.product_search').val();
		$.ajax({
		   	url:"<?php echo Yii::$app->urlManager->createUrl(['conference-trays/getproducts'])?>",	
			type: 'POST',
	        data: {keyword: keyword},
		    success: function(data)
		        {
		            $(".product_item").empty("");
		            if(jQuery.trim($(".product_search")).length > 0){ 	
		            	$.each($.parseJSON(data), function(key, item) {
							if ($.inArray(key, listedproducts) == -1) {
								if ($.inArray(key, addedproducts) == -1) {
									$('.product_list').append('<div class="product_item"><input type="checkbox" name="Products_added[]" value="'+item['id']+'" class="product_checkbox" data-price="'+item['price']+'" data-costprice="'+item['cost_price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'">'+item['name']+'</div>');
			                	}
			                }
			                	
			            });	
					}	
				}
		});
	});

	$(".category_search").on('keyup',function(e){

		$('.selected:checked').each(function(){
            if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                addedproducts.push($(this).attr('value'));
            }    
        });
        
        $(".tray_products_table table tbody tr").each(function(){ 
            if($(this).attr('data-productId')) {
                if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                    listedproducts.push($(this).attr('data-productId')); 
                }
            }        
        });

        var keyword = $('.category_search').val();
		$.ajax({
		   	url:"<?php echo Yii::$app->urlManager->createUrl(['conference-trays/getcategories'])?>",	
		   	type: 'POST',
	        data: {keyword: keyword},
		    success: function(data)
		        {
		            $(".product_item").empty("");
		            if(jQuery.trim($(".product_search")).length > 0){ 	
		            	$.each($.parseJSON(data), function(key, item) {
							if ($.inArray(key, listedproducts) == -1) {
								if ($.inArray(key, addedproducts) == -1) {
									$('.category_product').append('<div class="product_item"><input type="checkbox" name="Products_added[]" value="'+item['id']+'" class="category_checkbox">'+item['name']+'</div>');
			                	}
			                }
			                	
			            });	
					}	
				}
		});

	});	


	$('#w1-tree').height('150');
	
	$('.add_pdt').on('click',function(){ 
		values = [];
		$('.product_checkbox:checked').each(function(){
			values.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price'),'costprice' : $(this).attr('data-costprice')});
     	});	
		$(".tray_products_table").css('display','block');
		for( var i = 0, xlength = values.length; i < xlength; i++)
			{ 
				$(".tray_products_table table tbody").append('<tr data-productId="'+values[i].idss+'"><td><input type="checkbox" value="'+values[i].idss+'" name="Products[id]['+values[i].idss+']" class="selected"></td><td>'+values[i].names+'<input type="hidden" name="Products['+values[i].idss+'][name]" value="'+values[i].names+'"></td><td><input type="hidden" name="Products['+values[i].idss+'][sku]" value="'+values[i].sku+'">'+values[i].sku+'</td><td><input type="text" name="Products['+values[i].idss+'][original_cost_price]" class="cost_price" value="'+values[i].costprice+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+values[i].idss+'][offerCostPrice]" class="cost_price" autocomplete="off"></td><td><a href="#" class="remove">X</td></tr><input type="hidden" name="Products['+values[i].idss+'][newitem]" id="data-new" value="1">');
				}	
		});

	<?php 
        if(!$tray->isNewRecord) { //var_dump($tray->trayProducts);die();
            if(!empty($tray->trayProducts)) { 
    ?>
        $(".tray_products_table").css('display','block');

    <?php  foreach ($tray->trayProducts as $trayProduct)  { //var_dump($trayProduct);die(); ?>

                $(".tray_products_table table tbody").append('<tr data-productId="<?=$trayProduct->id?>"><td><input type="checkbox" checked value="<?=$trayProduct->product->id?>" name="Products[id][<?=$trayProduct->product->id?>]" class="selected"></td><td><?=$trayProduct->product->name?></td><td><?=$trayProduct->product->sku?></td><td><input type="text" name="Products[<?=$trayProduct->product->id?>][original_cost_price]" class="cost_price" value="<?=$trayProduct->product->cost_price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" value="<?=$trayProduct->offerCostPrice?>" name="Products[<?=$trayProduct->product->id?>][offerCostPrice]" class="cost_price" autocomplete="off"></td><td><a href="#" class="remove">X</td></tr>');

    <?php           
            } 
            }
            }
    ?>    



		$('.load_pdt').on('click',function(){ 

			$('.selected:checked').each(function(){
            	if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                	addedproducts.push($(this).attr('value'));
            	}    
        	});
        
        	$(".tray_products_table table tbody tr").each(function(){ 
            	if($(this).attr('data-productId')) {
                	if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                    	listedproducts.push($(this).attr('data-productId')); 
                	}
            	}        
       	 	});

       	 	categoryIds = [];


			$('.category_checkbox:checked').each(function(){
				categoryIds.push($(this).val());
     		});	

       	 	

       	 	if($(".category_search").val() != "") {

				//categoryIds.push($("#w1").val());
			
				$.ajax({
			   		url:"<?php echo Yii::$app->urlManager->createUrl(['conference-trays/getcategoryproducts'])?>",	
			    	type: 'POST',
		        	data: {categoryIds: categoryIds},
		        	success: function(data)
			        	{
			            	$(".tray_products_table").css('display','block');
			            	$.each($.parseJSON(data), function(key, item) {
			            		if ($.inArray(key, listedproducts) == -1) {
	                            	if ($.inArray(key, addedproducts) == -1) {
										$(".tray_products_table table tbody").append('<tr data-productId="'+item['id']+'"><td><input type="checkbox" value="'+item['id']+'" name="Products[id]['+item['id']+']" class="selected form-control" style="width:15px"></td><td>'+item['name']+'</td><td>'+item['sku']+'</td><td><input type="text" name="Products['+item['id']+'][original_cost_price]" class="cost_price" value="'+item['cost_price']+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+item['id']+'][offerCostPrice]" class="cost_price form-control" autocomplete="off"></td><td><a href="#" class="remove">X</td></tr><input type="hidden" name="Products['+item['id']+'][newitem]" id="data-new" value="1">');

									}
								}	
							});	
						}
				});	
			}

			else{

                alert('Please select a category');    

                $(".tray_products_table").css('display','block');

                $(".tray_products_table table tbody").empty();

                $(".tray_products_table table tbody").append('<tr><td> No items found </td> </tr>');
			}

		});
});		
</script>

<script type="text/javascript">
	$(".tray_products_table").on("click",".remove", function(e){  

		var trid = $(this).closest('tr').attr('data-productid');  
		if(trid) {
            jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedItems][]" id="deletedItems" value="'+trid+'"/>');
        }

		$(this).closest('tr').remove();
	});	
</script>














