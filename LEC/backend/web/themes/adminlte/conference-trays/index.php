<?php



use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConferencesTraysSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conference Trays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conference-trays-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Conference Trays', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \app\components\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'image',
            'conferenceId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
