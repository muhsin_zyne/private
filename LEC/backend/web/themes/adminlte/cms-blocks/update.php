<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CmsblockPages */

$this->title = 'Update Cms Blocks : ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Cmsblocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cmsblock-pages-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
