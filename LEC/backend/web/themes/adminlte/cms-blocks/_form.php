<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Stores;
use common\models\User;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use \kartik\widgets\FileInput;
use yii\helpers\Url;
use common\models\CmsBlockImages;
use common\models\StoreCmsBlocks;

/* @var $this yii\web\View */
/* @var $model common\models\CmsblockPages */
/* @var $form yii\widgets\ActiveForm */

$user = User::findOne(Yii::$app->user->id);
if($user->roleId == 1)
    $storeId = 0;
else
    $storeId = $user->store->id;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
               
                <?= $form->field($model, 'slug')->textInput(['readonly' => !$model->isNewRecord]) ?> 
                <?php 
                    $data = ArrayHelper::map(Stores::find()->where(['isVirtual'=>0])->all(), 'id', 'title'); 
                    //var_dump($data);die;
                    if($user->roleId == 1) { 
                        $store_selected[]=0;                            
                        if ($this->context->action->id == 'update') 
                        { 
                            $storePages= StoreCmsBlocks::find()->where(['blockId'=>$model->id])->all();
                            foreach ($storePages as $key => $storePage) {
                                $store_selected[]=$storePage->store->title;
                            }                                
                            
                            $htmlOptions = array('multiple' => 'true');
                            echo $form->field($model,'selected_store')->listBox($data, $htmlOptions);
                        }
                        if ($this->context->action->id == 'create') 
                        {
                            //$data = ArrayHelper::map(Stores::find()->all(), 'id', 'title'); 
                            $htmlOptions = array('multiple' => 'true');
                            echo $form->field($model,'selected_store')->listBox($data, $htmlOptions);
                        }
                        //echo Html::activeDropDownList($model, 'storeId',ArrayHelper::map(Stores::find()->all(), 'id', 'title')); 
                    }   
                    else 
                    {
                        echo '<input type="hidden" id="cmsblocks-selected_store" name="CmsBlocks[selected_store][]" value="'.$storeId.'">';
                    }
                    //var_dump($store_selected);die; 
                ?>
                <?= $form->field($model, 'content')->widget(CKEditor::className(), 
                    [
                        'id'=>'cmscontent',
                        'preset' => 'full', 
                        'clientOptions' =>[
                            'language' => 'en', 
                            'allowedContent' => true,
                            'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['cms-blocks/image-upload']),
                            'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['cms-blocks/image-browse']),
                            ]
                    ]) ?> 
                  
                <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']); ?>


                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<script>       
    $(document).ready(function(){ 
        <?php if($user->roleId == 1){ ?>        
            $("#cmsblocks-selected_store option").each(function(){            
                <?php foreach ($store_selected as  $store_sel) { ?>
                    //var a= <?= $store_sel ?>;                
                    if ($(this).text() == "<?=$store_sel?>")
                    {  
                        $(this).attr("selected","selected");
                    }                
                <?php } ?>
            });
        <?php } ?>        
    });    
</script>

