<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\helpers\Url;
use frontend\components\Helper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CmsblockPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'CMS Blocks';
$this->params['breadcrumbs'][] = $this->title;
$this->params['tooltip'] = "Please manage your content block  here.";

$stores=common\models\Stores::find()->where(['isVirtual'=>0])->all();
$listData=ArrayHelper::map($stores,'id','title');
$user = User::findOne(Yii::$app->user->id);
?>
<div class="box">
    <div class="box-body">
        <div class="cms-pages-index">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p class="pull-right create_pdt">
                <?= Html::a('<i class="fa fa-plus"></i> Create Block', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php \yii\widgets\Pjax::begin(['id' => 'cms-block',]); ?>
            <?= \app\components\AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'title',
                    //'content:ntext',
                    //'storeId',
                    'slug',
                    //'type',
                    [

                        'header' => 'Stores',
                        'visible'=>($user->roleId==1)?true:false,
                        'value' => function($model, $attribute){ return implode(", ", \yii\helpers\ArrayHelper::map($model->stores, 'id', 'title')); },

                        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'storeId', $listData,['class'=>'form-control','prompt' => '']),                   

                    ],
                    
                    'status' => [
                        'value' => function($model, $attribute){ return $model->status=="1"? "Enabled" : "Disabled"; },
                        'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Disabled', '1' => 'Enabled'],['class'=>'form-control','prompt' => '']),
                    ],
                    // 'dateUpdated',
                    // 'createdBy',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update}{delete}',
                        'buttons' => [
                            'update' => function($url, $model){
                                return '<a href="'.\yii\helpers\Url::to(['cms-blocks/update', 'id' => $model->id]).'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                            },
                            'delete' => function($url, $model){
                                return '<a href="'.\yii\helpers\Url::to(['cms-blocks/delete', 'id' => $model->id]).'" title="Delete" data-pjax="0"><span class="fa fa-trash"></span></a>';
                            }
                        ]
                    ],
                ],
            ]); ?>
            <?php  \yii\widgets\Pjax::end();  ?>  
        </div>
    </div>
</div>
