<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CmsblockPages */

$this->title = 'Create  Content blocks';
$this->params['breadcrumbs'][] = ['label' => 'Content Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cmsblock-pages-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
