<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InvoiceItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoice Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-items-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Invoice Items', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'invoiceId',
            'orderItemId',
            'quantityInvoiced',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
