<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CmsPages */

$this->title = 'Update CMS Page: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'CMS Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cms-pages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
