<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use \kartik\widgets\FileInput;
use common\models\Stores;
use common\models\User;
use common\models\CmsImages;
use common\models\StorePages;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Json;
/* @var $this yii\web\View */
/* @var $model common\models\CmsPages */
/* @var $form yii\widgets\ActiveForm */

$user = User::findOne(Yii::$app->user->id);
if($user->roleId == 1)
    $storeId = 0;
else
    $storeId = $user->store->id;

?>


<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="categories-index">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="toggle-mytab">
                        <li class="active"><a data-toggle="tab" href="#content">Content</a></li>
                        <li ><a data-toggle="tab" href="#cms-pages">Page Information</a></li>
                    </ul>
                    <?php $form = ActiveForm::begin(['id' => 'cms-form', 'enableAjaxValidation' => true]); ?>
                    <div class="form-group create-subminbutton">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
                    </div> 
                    <div class="box-body">
                        <div class="tab-content responsive">
                            <div class="tab-pane active" id="content">
                                <div class="content-index">
                                    <?= $form->field($model, 'contentHeading')->textInput(['maxlength' => 255]) ?>
                                    <?= $form->field($model, 'resolvedContent')->widget(CKEditor::className(), 
                                        [
                                            'id'=>'cmscontent',
                                            'preset' => 'full', 
                                            'clientOptions' =>[
                                                'language' => 'en', 
                                                'allowedContent' => true,
                                                'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['cms/image-upload']),
                                                'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['cms/image-browse']),
                                                ]
                                        ]) ?>        
                                </div>                            
                            </div>
                            <div class="tab-pane" id="cms-pages">
                                <div class="cms-pages-form">
                                    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
                                    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

                                    <?php 
                                    $data = ArrayHelper::map(Stores::find()->where(['isVirtual'=>0])->all(), 'id', 'title'); 
                                    if($user->roleId == 1) { 
                                        $store_selected[]=0;                            
                                        if ($this->context->action->id == 'update') 
                                        { 
                                            $storePages= StorePages::find()->where(['cmspageId'=>$model->id])->all();
                                            foreach ($storePages as $key => $storePage) {
                                                if(isset($storePage->store)){
                                                    $store_selected[]=$storePage->store->title;
                                                }
                                            }                                
                                            
                                            $htmlOptions = array('multiple' => 'true');
                                            echo $form->field($model,'selected_store')->listBox($data, $htmlOptions);
                                        }
                                        if ($this->context->action->id == 'create') 
                                        {
                                            //$data = ArrayHelper::map(Stores::find()->all(), 'id', 'title'); 
                                            $htmlOptions = array('multiple' => 'true');
                                            echo $form->field($model,'selected_store')->listBox($data, $htmlOptions);
                                        }
                                        //echo Html::activeDropDownList($model, 'storeId',ArrayHelper::map(Stores::find()->all(), 'id', 'title')); 
                                    }   
                                    else 
                                    {
                                        echo '<input type="hidden" id="cmspages-selected_store" name="CmsPages[selected_store][]" value="'.$storeId.'">';
                                    }
                                    //var_dump($store_selected);die; 
                                    ?>
                                    <?= $form->field($model, 'metaTitle')->textInput(['maxlength' => 255]) ?>

                                    <?= $form->field($model, 'metaKeywords')->textarea() ?>

                                    <?= $form->field($model, 'metaDescription')->textarea() ?>

                                    <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']); ?>

                                    <?= $form->field($model, 'dateUpdated')->hiddenInput()->label(false) ?>

                                    <div class="form-group">
                                        <!--Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary'])--> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <button type="button" id="backbutton" class="btn btn-primary">Back</button>
                        <button type="button" id="nextbutton" class="btn btn-primary">Next</button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?> 
            </div>
        </div>       
    </div>
</div>

  
<script>
    $(document).ready(function()
    {       
        <?php if($user->roleId == 1)
        { ?>
            $("#cmspages-selected_store option").each(function(){            
                <?php foreach ($store_selected as  $store_sel)             
                { ?>            
                    //var a= <?= $store_sel ?>;                
                    if ($(this).text() == "<?=$store_sel?>")
                    {  
                        $(this).attr("selected","selected");
                    }                
                <?php } ?>
            });
        <?php } ?>        
    });
    
</script>
