<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\GiftVouchers;
use frontend\components\Helper;
/* @var $this yii\web\View */
/* @var $model common\models\GiftVouchers */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Redeem';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php 

$user = User::findOne(Yii::$app->user->id);
$giftvouchers=GiftVouchers::findOne($id);
?>

<div class="box">
    <div class="box-body">
        <div class="gift-vouchers-form">
            <?php $form = ActiveForm::begin(); ?>   
            <?php $model->voucherId=$id;$model->balance=$giftvouchers->balance;?>
            <?=$form->field($model, 'amountRedeemed')->textInput(['maxlength' => 255])->label('Amount Redeemed (Max : '.Helper::money($model->balance).' )')  ?>
            <?= \kartik\date\DatePicker::widget([
                'form' => $form,
                'model' => $model,
                'attribute' => 'dateRedeemed',
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'autoclose' => true,
                    'startDate'=>date("Y-m-d H:i:s"),
                ],
            ]); ?>

            <?=$form->field($model, 'voucherId')->hiddenInput()->label(false) ?> 
            <?=$form->field($model, 'balance')->hiddenInput()->label(false) ?> 

            <?=$form->field($model, 'redemptionComment')->textArea() ?> 
            
            <?=$form->field($model, 'redemptionAuthorizedBy')->textArea() ?> 


            <div class="form-group">  
                <?= Html::submitButton('Create', ['class' => 'btn btn-success' ]) ?>            

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>


