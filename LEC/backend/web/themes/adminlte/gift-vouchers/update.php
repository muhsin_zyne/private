<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GiftVouchers */

$this->title = 'Update Gift Vouchers: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gift Vouchers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gift-vouchers-update"> 
    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
