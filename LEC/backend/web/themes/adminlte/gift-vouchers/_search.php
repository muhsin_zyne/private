<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\GiftVouchers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gift-vouchers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'initialAmount') ?>

    <?= $form->field($model, 'balance') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'creatorId') ?>

    <?php // echo $form->field($model, 'orderId') ?>

    <?php // echo $form->field($model, 'recipientName') ?>

    <?php // echo $form->field($model, 'recipientEmail') ?>

    <?php // echo $form->field($model, 'recipientMessage') ?>

    <?php // echo $form->field($model, 'createdDate') ?>

    <?php // echo $form->field($model, 'expiredDate') ?>

    <?php // echo $form->field($model, 'lastUsedDate') ?>

    <?php // echo $form->field($model, 'storeId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
