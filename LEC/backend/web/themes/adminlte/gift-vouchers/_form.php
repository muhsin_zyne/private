<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\Stores;
use frontend\components\Helper;
/* @var $this yii\web\View */
/* @var $model common\models\GiftVouchers */
/* @var $form yii\widgets\ActiveForm */
?>

<?php 

$user = User::findOne(Yii::$app->user->id);
?>
<div class="row">
    <div class="col-xs-12">
        <div style="float:right; margin-bottom:15px;">
            <?= Html::a('<i class="fa fa-usd"></i> Redeem', ['gift-vouchers/redeem' ,'id'=>$model->id], ['class' => 'btn btn-primary ', 'data-title' => 'Create Redee']) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs ">
                    <li class="active"><a data-toggle="tab" href="#voucher-details">Voucher Details </a></li>
                    <?php if(!$model->isNewRecord) {?>
                    <li><a data-toggle="tab" href="#usage-history">Usage History</a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="box-body"> 
                <div class="tab-content responsive">
                    <div class="gift-vouchers-form tab-pane active" id="voucher-details">

                        <?php $form = ActiveForm::begin(); 
                            $disabled =[];
                            if(!$model->isNewRecord) {
                                $disabled = ['disabled'=>'disabled'];
                            }    
                        ?>

                        <?=$form->field($model, 'code')->textInput($disabled) ?>

                        <?php if($model->isNewRecord) { ?>
                            <button type="button" class="btn btn-success generate">Generate Voucher Code</button>
                            <br /><br />
                        <?php } ?>    

                        <?= $form->field($model, 'balance')->textInput(['readonly' => !$model->isNewRecord]) ?>

                        <?php if(!$model->isNewRecord) { ?>

                            <?=$form->field($model, 'initialAmount')->hiddenInput()->label(false)?>


                            <?php } ?>


                        <?= $form->field($model, 'status')->dropDownList([ 'pending' => 'Pending', 'active' => 'Active', 'disabled' => 'Disabled', 'used' => 'Used', 'expired' => 'Expired', ]) ?>

                        <?= \kartik\date\DatePicker::widget([
                            'form' => $form,
                            'model' => $model,
                            'attribute' => 'expiredDate',
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'autoclose' => true,
                                'startDate'=>date("Y-m-d H:i:s"),
                            ],
                        ]); ?>

                        <?php if($user->roleId != "3") { ?>

                        <?= $form->field($model, 'storeId')->dropDownList(ArrayHelper::map(\common\models\Stores::find()->all(), 'id', 'title'))->label('Store') ?>

                        <?php } elseif ($user->roleId == "3") { ?>
                           <?= $form->field($model, 'storeId')->dropDownList(ArrayHelper::map(\common\models\Stores::find()->andWhere("(id = '".$user->store->id."')")->all(), 'id', 'title'))->label('Store') ?>
                       <?php } ?>


                        <?=$form->field($model, 'recipientName')->textInput(['maxlength' => 255]) ?>
                        <?=$form->field($model, 'recipientEmail')->textInput(['placeholder'=>'Your/Recipient’s Email'])->label('Email Voucher to') ?>
                        <?=$form->field($model, 'recipientMessage')->textArea()->label('Message') ?> 


                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create and Email' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name' => $model->isNewRecord ? 'create' : 'update']) ?>
                            <?php if(!$model->isNewRecord) { ?>

                            <?= Html::submitButton('Update and Email', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name' => 'update_email']) ?>

                            <?php } ?>

                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>  
                     <?php if(!$model->isNewRecord) {?>
                    <div class="tab-pane" id="usage-history">
                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => $dataProvider,                           
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                //'id',
                                'voucherId',
                                [
                                    'label' => 'Voucher Code',
                                    'attribute' => 'id',
                                    'value' => function($model){ return $model->giftVouchers->code; }, 
                                ],
                                //'userId',
                                //'orderId',
                                [
                                    'label'=> 'Order #',
                                    'format' => 'html',
                                    'value' => function($model){
                                        return $model->orderIdFormated;
                                    }
                                ],
                                [
                                    'label' => 'Date Used',
                                    'attribute' => 'id',
                                    'value' => function($model){ return Helper::date($model->dateUsed); }, 
                                ],
                                //'dateUsed',
                                [
                                    'label' => 'Amount Redeemed',
                                    'attribute' => 'amountRedeemed',
                                    'format' => 'html',
                                    'value' => function ($model) {
                                        return Helper::money($model->amountRedeemed);
                                    },
                                ],
                                //'dateRedeemedFormated',
                                [
                                    'label' => 'Date Redeemed',
                                    'attribute' => 'id',
                                    'format' => 'html',
                                    'value' => function($model){ return $model->dateRedeemed; }, 
                                ],
                                //'dateRedeemed',
                                'redemptionComment:ntext',
                                'redemptionAuthorizedBy:ntext',                                
                            ],
                        ]); ?>

                    </div>  
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var j=0; j<3; j++){
    for( var i=0; i < 5; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    if(j<2)
    text = text +"-";
    }
    return text;
}
$(document).ready(function(){
    $('.generate').click(function(){
    	$('#giftvouchers-code').val(makeid());
	})
});
</script>
