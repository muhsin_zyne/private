<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GiftVouchers */

$this->title = 'Create Gift Voucher';
$this->params['breadcrumbs'][] = ['label' => 'Gift Vouchers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-vouchers-create">
    <div class="box">
      <div  class="box-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    
    </div>
    </div>

</div>
