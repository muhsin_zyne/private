<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Users;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\GiftVouchers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gift Vouchers';
$this->params['breadcrumbs'][] = $this->title;
$this->params['tooltip'] = "This is the list of Gift vouchers that have been purchased on your website and also created by you for your customers.";
?>
<div class="gift-vouchers-index">

   <div class="box">
   <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="create_pdt">
        <?= Html::a('<i class="fa fa-plus"></i> Create Gift Vouchers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code:ntext',
            'initialAmount'=>[
                'attribute' => 'initialAmount',
                'format' => 'html',
                'value' => function($model){
                    return \backend\components\Helper::money($model->initialAmount);
                }
            ],
            'balance'=>[
                'attribute' => 'balance',
                'format' => 'html',
                'value' => function($model){
                    return \backend\components\Helper::money($model->balance);
                }
            ],
            'status',
            // 'creatorId',
            // 'orderId',
            // 'recipientName',
            // 'recipientEmail:email',
            // 'recipientMessage:ntext',
            // 'createdDate',
            // 'expiredDate',
            // 'lastUsedDate',
            // 'storeId',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                'update' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                },
                'delete' => function ($url, $model, $key) {
                    return ($model->storeId == Yii::$app->params['storeId'] || Yii::$app->user->identity->roleId == 1)? '<a href="'.$url.'" title="Delete" data-pjax="0"><span class="fa fa-trash"></span></a>' : '';
                },
                ]
            ],
        ],
    ]); ?>

</div>
</div>
</div>
