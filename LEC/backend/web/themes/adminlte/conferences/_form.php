<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use backend\components\Helper;
use backend\assets\AppAsset;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use yii\grid\GridView;
use kartik\tree\TreeViewInput;
use common\models\Categories;
use common\models\ProductCategories;

/* @var $this yii\web\View */
/* @var $model common\models\Conferences */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="error-msg"> </div>

<div class="box">
  <div class="box-body">
  
 

<div class="conferences-form">

    <?php $form = ActiveForm::begin(['id' => 'conference-details']); ?>

    <?= $form->field($conference, 'title')->textInput(['maxlength' => 45]) ?>

    <div class="deleted_products"></div>
    <div class="deleted_trays"></div>

    

    <?=DatePicker::widget([
			'form' => $form,
			'model' => $conference,
			'attribute' => 'fromDate',
			'pluginOptions' => [
			'format' => 'dd-mm-yyyy',
			],
		]);
		?>

	<?=DatePicker::widget([
			'form' => $form,
			'model' => $conference,
			'attribute' => 'toDate',
			'pluginOptions' => [
			'format' => 'dd-mm-yyyy',
			],
		]);
		?>	
	
	<?php //Yii::$app->params['uploadPath'] = Yii::$app->basePath."/.."; 
        echo FileInput::widget([
        'model' => $conference,
        'attribute' => 'image',
        'pluginOptions' => [
        'initialPreview'=>[Html::img(Yii::$app->params["rootUrl"].$conference->image)],'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false] ]); 
    ?>

	<?= $form->field($conference, 'status')
        ->dropDownList(
            (['0'=>'Not Active','1'=>'Active']),           // Flat array ('id'=>'label')
            ['prompt'=>'']    // options
        ); ?>

    <div class="form-group addnew_tray">
    	<span>Listing Trays</span>
    	
    	<a data-title="Edit Tray" data-width="900px" href="<?=\yii\helpers\Url::to(['conference-trays/create'])?>" class="btn btn-success add_tray ajax-update" data-before-submit="setTray();"><i class="fa fa-plus"></i> Add New Tray</a>

    	<div class="tray_items">	

    	<?php 

    		if(!$conference->isNewRecord) { 
            	if(!empty($conferenceTrays)) {


    		foreach ($conferenceTrays as $tray) {   


    		//var_dump($tray->trayProducts);die();

    		?>

    	

    	<div class="tray_products">

    		<div class="tray_details">
    			<input type="hidden" name="Products[trays][<?=$tray->id?>][id]" value="<?=$tray->id?>">
				<div class="tray_attri">Tray Name</div>
                <div class="attri_details"><input type="text" value="<?=$tray->title?>" id="title" name="Products[trays][<?=$tray->id?>][title]"></div>
 
                <div class="tray_attri">Tray Id</div>
                <div class="attri_details"><input type="text" value="<?=$tray->trayId?>" id="title" name="Products[trays][<?=$tray->id?>][trayId]">

	    	</div>	

	    </div>

	    	<div class="remove_tray" tray-id="<?=$tray->id?>">

	    		<a href="#" class="removetray">Remove Tray</a>

	    	</div>

	    	<!-- <a data-title="Edit Tray" data-width="900px" href="<?php /*\yii\helpers\Url::to(['conferencetrays/update', 'id'=>$tray->id])*/?>" class="btn btn-primary edit_tray">Update tray</a> -->

	    	<div class="tray_attri">Product(s)</div>
	    	<div class="attri_details">

	    	<?php //var_dump($tray->trayProducts);die(); ?>
	    		

		    	<table class="traypdt_table">
		    		<thead>
						<tr>
							<th>Name</th>
							<th>SKU</th>
							<th>Original cost price</th>
							<th>Offer cost price</th>
							<th>Remove</th>
						</tr>
					</thead>
					<tbody> 
						<?php foreach ($tray->trayProducts as $id => $product) { //var_dump($tray->trayProducts);die();?>
						<tr id="<?=$product->id?>">
							<input type="hidden" name="Products[trays][<?=$id?>][trayProduct][$product->id][id]" value="<?=$product->id?>">
							<td><?=$product->product->name?></td>
							<td><?=$product->product->sku?></td>
							<td><?=$product->product->cost_price?></td>
							<td><input type="text" value="<?=$product->offerCostPrice?>" name="Products[trays][<?=$id?>][trayProduct][<?=$product->id?>][offerCostPrice]"></td>
							<td><a href="#" class="delete">X</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>

    	<div class="clear"></div>

    	</div>

    	<?php } } } ?>
    </div>	

    </div>

    <div class="form-group">
        <?= Html::submitButton($conference->isNewRecord ? 'Create' : 'Update', ['class' => $conference->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="autofile-upload btm-upld">
        <?php $form1 = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','class'=>'trays']]); ?>
        <label class="control-label"><i class="fa fa-paperclip"></i> Upload Document</label>
        <?=Html::input('file', 'csv', '', ['class' =>'csv-file-upload']) ?>
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        <?php //ActiveForm::end(); ?>
	</div>


	 </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {

$(".trays").on('submit',(function(e) {
        e.preventDefault();
        $(".upload-msg").text('Loading...');    

        $(".tray_products_table table tbody tr").each(function(){ 
            if($(this).attr('data-productId')) {
                if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                    listedproducts.push($(this).attr('data-productId')); 
                }
            }        
        });

        $('.selected:checked').each(function(){
            if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                addedproducts.push($(this).attr('value'));
            }    
        });


        $.ajax({
            url: "<?=Yii::$app->urlManager->createUrl(['conference-trays/test'])?>", 
			type: "POST",            
            data: new FormData($(this)[0]), 
            contentType: false,       
            cache: false,             
            processData:false,        
            success: function(data)   
            {

            	$('.selected:checked').each(function(){
		            if ($.inArray($(this).attr('value'), addedproducts) == -1) {
		                addedproducts.push($(this).attr('value'));
		            }    
	        	});
	        
		        $(".tray_products_table table tbody tr").each(function(){ 
		            if($(this).attr('data-productId')) {
		                if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
		                    listedproducts.push($(this).attr('data-productId')); 
		                }
		            }        
		        });

		        var currkey = $('.tray_products:last').attr('data-id');
                if(typeof(currkey)  === "undefined"){ 
                    ids=1;
                }
                else{ 
                    ids = parseInt(currkey)+1;
                }

		        $.each($.parseJSON(data), function(status, trays) { 
                    if(trays.length != 0){
                        if(status == "notfound"){
                            for( var i = 0, xlength = trays.length; i < xlength; i++)
                            {  
                                $(".error-msg").append('<div class="alert alert-danger alert-dismissable"><i class="icon fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Not found this products with sku :'+trays[i]['sku']+' </div>');
                            }    
                        }
                        else {   
                        		$.each(trays, function(key, tray) { 
	                            	if(tray.length != 0){
	                            		$('.tray_items').append('<div class="tray_products" data-id="'+ids+'"><div class="tray_details"><div class="tray_attri">Tray Name</div><div class="attri_details"><input type="text" value="'+(tray['name']).replace('Tray:',"")+'" name="Products[trays]['+key+'][title]"></div><div class="tray_attri">Tray Image</div><div class="attri_details"><input type="file" name="Products[trays]['+key+'][image]"></div><div class="tray_attri">Tray Id</div><div class="attri_details"><input type="text" value="'+tray['trayId']+'" name="Products[trays]['+key+'][trayId]"></div></div><table id="'+key+'" class="traypdt_table"><thead><tr><th>Name</th><th>SKU</th><th>Original cost price</th><th>Offer cost price</th><th>Remove</th></tr></thead><tbody></tbody></table></div><div class="clear"></div></div>');
											$.each(tray['products'], function(keyval1, products) { //console.log(products);
											$('.tray_products[data-id="'+ids+'"] table[id="'+key+'"] tbody').append('<tr id="'+products['id']+'"><td>'+products['name']+'</td><td>'+products['sku']+'</td><td>'+products['costprice']+'</td><td><input type="text" name="Products[trays]['+key+'][trayProduct]['+products['id']+'][offerCostPrice]" value="'+products['offerCostPrice']+'"></td><td><a href="#" class="delete">X</a></td></tr>');	
										});
									}
								});	
						}
					}	
            	});
            }		
        });
	}));
});

</script>

<script type="text/javascript">
function setTray(){
    //$('#conference-details').append($('.conference-trays-form .form-control').attr("type", "hidden"));
    $.each($(".selected:not(:checked)"), function(i, val){
        $(this).closest('tr').remove();
    });
    var formData = '';
	$('.conference-trays-form form').find('input, textarea, select').each(function(i, field) {
    	formData += field.name+'|'+field.value+'~';
	});

	var currkey = $('.tray_products:last').attr('data-id');
 
    if(typeof(currkey)  === "undefined"){ 
        id=1;
    }
    else{ 
        id = parseInt(currkey)+1;
    }
	ids = Array();

	$('.addnew_tray .tray_items').append('<div class="tray_products" data-id="'+id+'"><div class="tray_details"><div class="tray_attri">Tray Name</div><div class="attri_details"><input type="text" value="" id="title" name="Products[trays]['+id+'][title]"></div><div class="tray_attri">Tray Image</div><div class="attri_details"><input type="file" name="Products[trays]['+id+'][image]" id="image"></div><div class="tray_attri">Tray Id</div><div class="attri_details"><input type="text" value="" name="Products[trays]['+id+'][trayId]" id="trayid"></div><table id="1" class="traypdt_table"><thead><tr><th>Name</th><th>SKU</th><th>Original cost price</th><th>Offer cost price</th><th>Remove</th></tr></thead><tbody></tbody></table></div></div><div class="clear"></div>'); 


		$.each(formData.split("~"), function(key,value){
		var arr = value.split('|');	

			if(arr[0] == "ConferenceTrays[title]"){
				$('.tray_products[data-id="'+id+'"] .tray_details .attri_details #title').val(arr[1]);
			}
			else if(arr[0] == "ConferenceTrays[trayId]"){ 
                $('.tray_products[data-id="'+id+'"] .tray_details .attri_details #trayid').val(arr[1]);
            }
            else if(arr[0] == "ConferenceTrays[image]"){
                //$('.tray_items .tray_details .attri_details #image').val(arr[1]);
            }
			else{
				if(arr[0].indexOf("Products[id]") >= 0){
					if ($.inArray(arr[1], ids) == -1)
                    {
                        ids.push(arr[1]);
                    }
				}	
			}

			//console.log(ids);
			$.each(ids, function(i, val){ 
				
					/*$('.tray_details table tbody').append('<tr><td class="pdtname"></td><td class="sku"></td><td class="original_cost_price"></td><td class="offercostprice"></td></tr>');*/

					if(arr[0] == "Products[id]["+ids[i]+"]"){

						//console.log(arr[0]);

						$('.tray_products[data-id="'+id+'"] .tray_details table tbody').append('<tr id="'+ids[i]+'"><td class="pdtname"></td><td class="sku"></td><td class="original_cost_price"></td><td class="offercostprice"></td><td>X</td></tr>');
					}

					if(arr[0] == "Products["+ids[i]+"][name]"){ 
						$('.tray_products[data-id="'+id+'"] .tray_details table tbody tr[id="'+ids[i]+'"] .pdtname').html(arr[1]);
					}
					if(arr[0] == "Products["+ids[i]+"][sku]"){
						$('.tray_products[data-id="'+id+'"] .tray_details table tbody tr[id="'+ids[i]+'"] .sku').html(arr[1]);
					}
					if(arr[0] == "Products["+ids[i]+"][original_cost_price]"){
						$('.tray_products[data-id="'+id+'"] .tray_details table tbody tr[id="'+ids[i]+'"] .original_cost_price').html(arr[1]);
					}
					if(arr[0] == "Products["+ids[i]+"][offerCostPrice]"){
						$('.tray_products[data-id="'+id+'"] .tray_details table tbody tr[id="'+ids[i]+'"] .offercostprice').append('<input type="text" value="'+arr[1]+'" name="Products[trays]['+id+'][trayProduct]['+ids[i]+'][offerCostPrice]">');
					}
				//}	
			});	
		});



	/*$.each(formData.split("~"), function(key,value){
		var arr = value.split('|');	

			if(arr[0] == "ConferenceTrays[title]"){
				$('.tray_items .tray_details').append('<div class="tray_attri">Tray Name</div><div class="attri_details"><input type="text" value="" name="Products[trays]['+key+'][title]"></div>');
			}
			if(arr[0] == "ConferenceTrays[image]"){
				$('.tray_items .tray_details').append('<div class="tray_attri">Tray Image</div><div class="attri_details"><input type="file" name="Products[trays]['+key+'][image]"></div>');
			}
			if(arr[0] == "ConferenceTrays[trayId]"){
				$('.tray_items .tray_details').append('<div class="tray_attri">Tray Id</div><div class="attri_details"><input type="text" value="'+arr[1]+'" name="Products[trays]['+key+'][trayId]"></div>');
			}
			else{
				if(arr[0].indexOf("Products[id]") >= 0){
					ids.push(arr[1]);
				}	
			}

			$.each(ids, function(i, val){ 
				if(arr[0] == "Products["+ids[i]+"][name]"){ 
					$('.tray_details table tbody').append('<tr><td>'+arr[1]+'</td>');
				}
				if(arr[0] == "Products["+ids[i]+"][sku]"){
					$('.tray_details table tbody tr').append('<td>'+arr[1]+'</td>');
				}
				if(arr[0] == "Products["+ids[i]+"][original_cost_price]"){
					$('.tray_details table tbody tr').append('<td>'+arr[1]+'</td>');	
				}
				if(arr[0] == "Products["+ids[i]+"][offerCostPrice]"){
					$('.tray_details table tbody tr').append('<td><input type="text" value="'+arr[1]+'" name="Products[trays]['+ids[i]+'][offerCostPrice]"></td><td><a href="#" class="delete">X</a></td></tr>');
					//$('.tray_details table tbody tr').append('<td>X</td></tr>');	
				}
			});	
	});	*/

	

    $("#ajax-update").dialog("close");
    throw new Error('That\'s all we know');
}
$(document).ready(function() {

	$(".tray_items").on("click",".delete", function(e){ 
		var tableid = $(this).closest('div table').attr('id');
		var trid = $(this).closest('tr').attr('id');
		$('.deleted_products').append('<input type="hidden" name="Products[deletedProducts][]" id="deletedProducts" value="'+trid+'"/>');
		$(this).closest('tr').remove();
		var trlen = $('.bundle_items_table table[id="'+tableid+'"]  tbody tr').length;
		if(trlen == 0) { $('.bundle_items_table table[id="'+tableid+'"]').css('display', 'none'); }
    });	
});
</script>

<script type="text/javascript">
$(document).ready(function() {

	$(".tray_products").on("click",".removetray", function(e){
		var tableid = $(this).closest('div table').attr('id');
		var trayId = $(this).closest('.remove_tray').attr('tray-id');
		$('.deleted_trays').append('<input type="hidden" name="Products[deletedTrays][]" id="deletedTrays" value="'+trayId+'"/>');
		$(this).closest('.tray_products').remove();
	});	
});
</script>


<?php
	/*if(isset($_POST['serialize'])){  die('set');
		parse_str($_POST['serialize'], $searcharray);
		print_r($searcharray);
	}*/	
?>