<?php

use yii\helpers\Html;

$this->title = 'Create Conference';
$this->params['breadcrumbs'][] = ['label' => 'Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\Conferences */

?>
<div class="box">
<div class="box-body">

<div class="conferences-create">

    <?= $this->render('_form',compact('conference')) ?>
</div>

</div>
</div>
