<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\components\Helper;

/* @var $this yii\web\View */
/* @var $model common\models\Conferences */

$this->title = $conference->title;
$this->params['breadcrumbs'][] = ['label' => 'Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conferences-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $conference->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $conference->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $conference,
        'attributes' => [
            'id',
            'title',
            [   
                'attribute' => 'fromDate',
                'value' =>  Helper::date($conference->fromDate),
            ],
            [   
                'attribute' => 'toDate',
                'value' =>  Helper::date($conference->toDate),
            ],
            //'fromDate',
            //'toDate',
            'status',
            'image',
        ],
    ]) ?>

</div>
