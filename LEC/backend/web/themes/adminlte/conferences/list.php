<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\Helper;
//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConferencesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conferences';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">
<div class="conferences-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute'=>'fromDate',
                'label'=>'Start Date',
                'value' => function($model,$attribute){
                  return Helper::date($model->fromDate);
                }
            ],
            [
                'attribute'=>'toDate',
                'label'=>'End Date',
                'value' => function($model,$attribute){
                  return Helper::date($model->toDate);
                }
            ],
            //'fromDate',
            //'toDate',
            'status',
            // 'image',

            ['class' => 'yii\grid\ActionColumn',
            	'template' => '{orders}',
            	'buttons' =>[
            		'orders' => function ($url, $model) {
            			return Html::a('<span class="glyphicon glyphicon-info-sign"></span>', $url, [
                        	'title' => Yii::t('app', 'Info'),
            			]);
            		}	
				] 
            ],
        ],
    ]); ?>

</div>
</div>
</div>
