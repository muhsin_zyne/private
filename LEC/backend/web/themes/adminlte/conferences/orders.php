<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ConferencesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders: '.$conference->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">
<div class="conferences-orders-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $orderItems,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order.orderDate',
            'orderId',
            'b2baddress.storeName',
            'product.name',
            'product.supplierName',
            'product.sku',
            'product.ezcode',
            'product.price',
            'quantity',
            'price',
        ],
    ]); ?>

</div>
</div>
</div>
