<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Conferences */

$this->title = 'Update Conference: ' . ' ' . $conference->title;
$this->params['breadcrumbs'][] = ['label' => 'Conferences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $conference->title, 'url' => ['view', 'id' => $conference->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conferences-update">

    <?= $this->render('_form', compact('conference','conferenceTrays','trayproducts')) ?>

</div>
