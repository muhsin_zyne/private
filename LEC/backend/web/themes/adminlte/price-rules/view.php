<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\components\Helper;
use common\models\Stores;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\GiftVouchers */

$this->title = 'Price Rule #'.$rule->id;
$this->params['breadcrumbs'][] = ['label' => 'Price Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-vouchers-view">

    <!-- <p> -->
    <div class="row">
        <div class="col-xs-12"> 
            <div style="float:right; margin-bottom:15px;">
                <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['price-rules/index'], ['class' => 'btn btn-default back-btn ']) ?>
                <?= Html::a('Update', ['update', 'id' => $rule->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $rule->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
    </div>            
    <!-- </p> -->

    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box box-default">
                                <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
                                    <h3 class="box-title">Price Rule # <?= $rule->id ?></h3>
                                </div>
                              <!-- /.box-header -->
                                <div class="box-body">
                                    <p>Created Date :
                                      <?= Helper::date($rule->fromDate) ?>
                                    </p>
                                    <p>Expired Date :
                                      <?= Helper::date($rule->toDate) ?>
                                    </p>
                                    <p>Status :
                                      <?= ucfirst($rule->status) ?>
                                    </p>
                                    <p>Initial Amount :
                                      <?= Helper::money($rule->discount) ?>
                                    </p>
                                    <p>Coupon Usage :
                                      <?=$rule->couponUsage?>
                                    </p>
                                    <p>Maximum Coupon Usage :
                                      <?=$rule->maxCouponUsage ?>
                                    </p>
                                    <?php  
                                        //$store= Stores::findOne($rule->createdBy);
                                    ?>

                                    <p>Created By :
                                        <?php   if($rule->createdBy == 0)
                                                    echo "Master Admin";
                                                else{
                                                    $user = User::findOne($rule->createdBy);
                                                    echo $user->store->title;
                                                }
                                        ?>
                                    </p>
                                    <p>Type :
                                      <?= $rule->type ?>
                                    </p>

                                </div>
                              <!-- /.box-body --> 
                            </div>
                        </div>
                       <!--  <div class="col-md-6">
                            <div class="box box-default">
                                <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
                                    <h3 class="box-title">Usage Details</h3>
                                </div>
                                <div class="box-body">
                                    <p>Balance: <?php // Helper::money($rule->balance)?></p>
                                    <p>Last Used date: <?php //Helper::date($rule->lastUsedDate)?></p>
                                    <p>Order Id: <?php //$rule->orderId?></p>
                                </div>
                                
                            </div>
                        </div> -->
                       <!--  <div class="col-md-6">
                            <div class="box box-default">
                                <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
                                    <h3 class="box-title">Reciepient Details</h3>
                                </div>
                                <div class="box-body">
                                    <p>Name: <?php //$rule->recipientName?></p>
                                    <p>Email Id: <?php //$rule->recipientEmail?></p>
                                    <p>Message: <?php //$rule->recipientMessage?></p>
                                </div>
                                
                            </div>
                        </div> -->    
                    </div>        

                </div>
            </div>
        </div>
    </div>                

</div>
