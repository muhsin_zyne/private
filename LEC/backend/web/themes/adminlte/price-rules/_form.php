<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\Helper;
use yii\helpers\ArrayHelper;
use common\models\Stores;
use kartik\date\DatePicker;
use common\models\Attributes;
use yii\helpers\Url;
use common\models\User;
use common\models\Brands;
use backend\components\TreeView;
//use common\models\Stores;

/* @var $this yii\web\View */

/* @var $priceRule app\models\PriceRules */

/* @var $form yii\widgets\ActiveForm */

?>


<?php 
	$user = User::findOne(Yii::$app->user->id);
?>


<link rel='stylesheet' href='/css/easyTree.css' />
<link rel='stylesheet' href='/css/style_treeview.css' />

<?php 
	$conditions = unserialize($priceRule->conditions);
	$attributes = ArrayHelper::map(Attributes::find()->all(), 'code', 'self');
	if($user->roleId == "1") {
		$brands = Brands::find()->where(['isVirtual'=>'0','enabled'=>'1'])->all();
		//var_dump(count($brands));die();
	}
	elseif ($user->roleId == "3") {
		$brands = Brands::find()->join('JOIN','StoreBrands sb','Brands.id=sb.brandId')->where('Brands.isVirtual=0 and Brands.enabled=1 and sb.storeId='.Yii::$app->user->identity->store->id.' and sb.type="b2c" and sb.enabled=1')->all();
		//var_dump(count($brands));die();
	}	
?>

<div class="rules-index">
	<div class="nav-tabs-custom">                       
		<ul class="nav nav-tabs pull-right" id="toggle-mytab">
			<li class="active"><a data-toggle="tab" href="#info">Rule Information</a></li>
			<li id="m_title" ><a data-toggle="tab" href="#conditions">Conditions</a></li>
		</ul>
		<div class="box-body">

<div class="row">
	<div class="col-xs-12">
		<div class="attribute-sets-form">
			<?php $form = ActiveForm::begin(['id' => 'pricerules-form', 'enableAjaxValidation' => true]); ?>
		<div class="form-group create-subminbutton">
			<?= Html::submitButton($priceRule->isNewRecord ? 'Create' : 'Update', ['class' => $priceRule->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
		</div>
			<div class="tab-content responsive">
				<div class="tab-pane active" id="info">
					<div class="info">
						<?= $form->field($priceRule, 'title')->textInput(); ?>
						
						<?= $form->field($priceRule, 'code')->textInput(); ?>
						<!-- <div class="form-group"><button type="button" class="btn btn-primary generate">Generate Coupon Code</button></div> -->
						<?= $form->field($priceRule, 'maxCouponUsage')->textInput(); ?>
						<?= $form->field($priceRule, 'maxCouponUsagePerUser')->textInput(); ?>
						<?= $form->field($priceRule, 'status')->dropDownList(['Active'=>'Active','Pending'=>'Pending','Disabled'=>'Disabled','Used'=>'Used','Expired'=>'Expired']); ?>
						<?= DatePicker::widget([
							'form' => $form,
							'model' => $priceRule,
							'attribute' => 'fromDate',
							'pluginOptions' => [
								'format' => 'dd-mm-yyyy',
							],
						]); ?>

						<?= DatePicker::widget([
							'form' => $form,
							'model' => $priceRule,
							'attribute' => 'toDate',
							'pluginOptions' => [
								'format' => 'dd-mm-yyyy',
							],
						]);

						$selected = [];
						if(!$priceRule->isNewRecord){
							foreach($priceRule->stores as $store){
								$selected[$store->id] = ['selected' => 'selected'];
							}
						}
						?>

							<?php /* $form->field($priceRule, 'stores')->dropDownList(ArrayHelper::map(Stores::find()->all(), 'id', 'title'), ['multiple' => 'multiple', 'options' => $selected]);*/ ?>
						<?php if($user->roleId != "3") { ?>
						<?= $form->field($priceRule, 'stores')->dropDownList(ArrayHelper::map(Stores::find()->all(), 'id', 'title'), ['multiple' => 'multiple', 'options' => $selected]); ?>

						<?php } elseif ($user->roleId == "3") { ?>
						<?= $form->field($priceRule, 'stores')->hiddenInput(['value' => Yii::$app->user->identity->store->id, 'name' => 'PriceRules[stores][]'])->label('');?>
						<?php } ?>

						<div class="form-group">
							<!--Html::submitButton($priceRule->isNewRecord ? 'Create' : 'Update', ['class' => $priceRule->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) -->
						</div>
					</div>
				</div>
				<div class="tab-pane" id="conditions">
					<div class="conditions">

							<?= $form->field($priceRule, 'type')->dropDownList(['percent-cart' => 'Percent of Cart Amount', 'fixed-cart' => 'Fixed Amount from Cart', 'percent-product' => "Percent of Product's Price", 'fixed-product' => "Fixed Amount from Product's Price",'free-shipping' => 'Free Shipping']); ?>

					        <?= $form->field($priceRule, 'discount')->textInput(); ?>

					        <div class="form-group sm-cntrl">
                            <label>Apply the rule only to cart items matching</label>
                              <select name="PriceRules[aggregator]" class="form-control ">
                                <option <?=!empty($conditions)? ($conditions[0]['aggregator']=="&&"? "selected" : "") : ""?> value="&&">all</option>
                                <option <?=!empty($conditions)? ($conditions[0]['aggregator']=="||"? "selected" : "") : ""?> value="||">any</option>
                              </select><span>of the following conditions</span>
                             </div>

					        <div class="condition-category">
						        <div class="form-group">
						        	<input type="button" id="add-condition" class="btn btn-success" value="Add Condition"/>
						        	<?php

						        		if(!empty($conditions[0]['conditions'])){

						        			foreach($conditions[0]['conditions'] as $i => $condition){

						        				$attr = explode(".", $condition['attribute']);
						        				//var_dump($attr);die();

						        				echo "<div class='condition sm-rule-row'>";

						        				

						        				 echo Html::dropDownlist("PriceRules[conditions][$i][attribute]", $condition['attribute'], array_merge(['cart.cost' =>'Cart Total', 'position.quantity'=>'Quantity in Cart', 'position.price'=>'Price in Cart', 'position.cost'=>'Row Total in Cart', 'product.category' => 'Category','product.brandid'=>'Brand'], ArrayHelper::map(Attributes::find()->select("*, CONCAT('product.', `title`) as attr")->where(['field'=>'dropdown', 'system'=>'0'])->all(), 'attrCode', 'title')), [ 'id' => 'attributes-dd', 'prompt' => '--Select--', 'style' => 'pointer-events: none; cursor: default;']);

						        				

						        				 echo Html::dropDownList("PriceRules[conditions][$i][operator]", $condition['operator'], array_flip(["is"=>"==", "is not"=>"!=", "equals or greater than"=>">=", "equals or less than"=>"<=", "greater than"=>">", "less than"=>"<"]), ["style" => "width: 260px;"]);

						        				

						        				 if(isset($attributes[$attr[1]])){

							        			 	if($attributes[$attr[1]]->field == "dropdown" || $attributes[$attr[1]]->field == "multiselect"){

							        			 		echo Html::dropDownList("PriceRules[conditions][$i][value]", $condition['value'], ArrayHelper::map($attributes[$attr[1]]->options, 'value', 'value'), ["style" => "width: 260px;"]);

							        			 	}else{

							        			 		echo Html::textInput("PriceRules[conditions][$i][value]", $condition['value'], ["style" => "width: 260px;"]);

							        			 	}

							        			 }

							        			 else{

							        			 	echo Html::textInput("PriceRules[conditions][$i][value]", $condition['value'], ["style" => "width: 260px;","class"=>"value_field"]);

							        			 }

							        			

							        			 echo '<span class="remove-condition">&nbsp;<a href="javascript:;">X</a></span>';

							        			echo "</div>";

						        			}


						        		}

						        	?>

						    	</div>

						    	<div class="row categories-row">
							    	<div class="category-block col-md-4">
										<?=TreeView::widget()?>
									</div>
								</div>	

						    	<div class="brands-block col-md-4" style="display:none">
									<div class="brands-coupon-list" style="max-height:300px;border:1px solid #ccc;">
										<?php  foreach ($brands as $brand) { ?>
											<div class="coupon-brand-title">
												<input type="checkbox" name="brandid" value="<?=$brand->id?>" class="coupon-brand"><?=$brand->title?>
											</div>		
										<?php } ?>
									</div>
								</div>
							</div>	

					        <div class="form-group">
								<!--Html::submitButton($priceRule->isNewRecord ? 'Create' : 'Update', ['class' => $priceRule->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) -->
							</div>

				    </div>
				</div>
                <button type="button" id="backbutton" class="btn btn-primary">Back</button>
                        <button type="button" id="nextbutton" class="btn btn-primary">Next</button>
			</div>

			<?php ActiveForm::end(); ?>

			<?=Html::dropDownlist('PriceRules[][Attributes]', [], array_merge(['cart.cost' =>'Cart Total',/* 'position.quantity'=>'Quantity in Cart', 'position.price'=>'Price in Cart', 'position.cost'=>'Row Total in Cart', */'product.category' => 'Category','product.brandId' => 'Brand'], []/*ArrayHelper::map(Attributes::find()->select("*, CONCAT('product.', `title`) as attr")->where(['field'=>'dropdown', 'system'=>'0'])->all(), 'attrCode', 'title')*/), [ 'id' => 'attributes-dd', 'prompt' => '--Select--', 'class' => 'form-control sm-drop primary'])?>

		</div>

	</div>

</div>

</div>
	</div>
</div>

<script type="text/javascript">
	function makeid(){
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for( var i=0; i < 5; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	}
	
	$(document).ready(function(){

		var operators_dd = $('<?=trim(preg_replace("/\s+/", " ", Html::dropDownList("PriceRules[][operator]", [], array_flip(["is"=>"==", "is not"=>"!=", "equals or greater than"=>">=", "equals or less than"=>"<=", "greater than"=>">", "less than"=>"<"]), ["style" => "width: 260px;","class"=>"pricerule_operator form-control"])))?>');
		$('#attributes-dd.primary').hide();

		var i = $('.conditions .condition').length;

		$('#add-condition').click(function(){

			$(this).hide();
			$(this).parent().append("<div class='condition sm-rule-row'></div>");
			$(this).parent().find('.condition').last().append($('#attributes-dd.primary').clone().attr("name", 'PriceRules[conditions]['+i+'][attribute]').attr("data-ruleid", i).removeClass("primary").removeAttr("style"));
			//$(this).parent().find('#attributes-dd.primary').show();
			i++;

		})

		$('body').parent().on('change', '#attributes-dd', function(){
			var dd = this;
			var value = $(this).val();

			$.ajax({
				url: '<?=Url::to(["price-rules/getoptions"])?>',
				data: {code: $(dd).val(), ruleid: $(dd).attr("data-ruleid")},
				method: 'GET',
			}).success(function(data){
				$(dd).attr("style", "pointer-events: none; cursor: default;");
				var $operators_dd = $(operators_dd).clone();
				if(!data.indexOf('<select') || $(dd).val() == "product.category" || $(dd).val() == "product.brandId"){
					$operators_dd.find('option[value!="=="][value!="!="]').remove();
					//$operators_dd.find('option[value!="!="]').remove();
				}
				$(dd).parent().append($operators_dd.attr("name", 'PriceRules[conditions]['+$(dd).attr("data-ruleid")+'][operator]')).append(data);
				$(dd).parent().append('<span class="remove-condition">&nbsp;<a href="javascript:;">X</a></span>');
				$('#add-condition').show();
			})

			if(value == "product.category"){
				$('.category-block').css('display','block');
			}
			if(value == "product.brandId"){
				$('.brands-block').css('display','block');
			}
		})

		$('body').parent().on('click', '.remove-condition', function(){ console.log($(this).parent());
			$(this).parent().remove();
		});
		$('.generate').click(function(){
			$('#pricerules-code').val(makeid());
		})
	})

</script>



<script type="text/javascript">

		if($('.value_field').length == 0){
			var ids = [];
		}
		else{
			var ids = [];
			ids.push($('.value_field').val());
		}	

		$(document).ready(function(){
			/*$('body').on('click', '.kv-tree-input-widget', function() {
				val = $('input[name=kv-product]').val();
				$('.categoryids').val(val);
			});	*/

			$('input').on('ifChanged', function(){
				if($(this).prop('checked')){ //alert('checked');
					ids.push($(this).val());
					$('.brandids').val(ids);
				}
				else{
					pos = ids.indexOf($(this).val());
					ids.splice(pos,1);
					$('.brandids').val(ids);
				}
			});
			$('body').on('click', '#add-condition', function() { 
				$('.category-block').css('display','none');
				$('.brands-block').css('display','none');
			});

			$('body').on('click', '.categoryids', function() { 
				$('.brands-block').css('display','none');
				$('.category-block').css('display','block');
			});


			/*$('body').on('click', '.brandids', function() { 
				$('.category-block').css('display','none');
				$('.brands-block').css('display','block');
			});	*/

		catids = new Array();



		/*	var condition = $('#attributes-dd').val();
			if(condition == "product.category"){
				//$('.value_field').addClass('categoryids');
			}
			else if(condition == "product.brandid"){
				//$('.value_field').addClass('brandids');
			}
		})*/

		$('.selectcat').change(function(){ 

			if($(this).prop('checked')){ 
				catids.push($(this).val());
     			$('.categoryids').val(catids);
  			}

   			else{
   				pos = ids.indexOf($(this).val());
   				catids.splice(pos,1);
   				$('.categoryids').val(catids);
   			}

		});	
		
		/*$('input').on('ifChanged', function(){

			if($(this).prop('checked')){ //alert('checked');
				ids.push($(this).val());
				$('.brandids').val(ids);
			}
			else{
				pos = ids.indexOf($(this).val());
				ids.splice(pos,1);
				$('.brandids').val(ids);
			}

		});*/


		$('body').on('click', '#add-condition', function() { 
			$('.category-block').css('display','none');
			$('.brands-block').css('display','none');

		});

		

		$('body').on('click', '.categoryids', function() { 

			$('.brands-block').css('display','none');

     		$('.category-block').css('display','block');

		});



		$('body').on('click', '.brandids', function() { 

			$('.category-block').css('display','none');

     		$('.brands-block').css('display','block');

		});	



		var condition = $('#attributes-dd').val();



		if(condition == "product.category"){

			//$('.value_field').addClass('categoryids');

		}

		else if(condition == "product.brandid"){

			//$('.value_field').addClass('brandids');

		}



	})


</script>
