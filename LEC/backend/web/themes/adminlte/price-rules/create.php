<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PriceRules */

$this->title = 'Create Price Rule';
$this->params['breadcrumbs'][] = ['label' => 'Price Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">

<div class="products-create">

	<?= $this->render('_form', compact('priceRule')) ?>

</div>
</div>
</div>
