<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\PriceRules;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Price Rules';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs pull-right">
            <li class="active"><a data-toggle="tab" href="#activecoupon">Active Coupons</a></li>
            <li><a data-toggle="tab" href="#expirecoupon">Expired Coupons</a></li>
        </ul>
    </div>

    <div class="box-body">
        <div class="products-index">
            <div class="tab-content responsive">
                <div class="tab-pane active" id="activecoupon">
                    <p class="create_pdt">
                        <?= Html::a('<i class="fa fa-tag"></i> Create Price Rule', ['create'], ['class' => 'btn btn-success']); ?>
                    </p>

                        <?php
                            $form = \kartik\form\ActiveForm::begin(['id' => 'pricerules-form']);
                            \kartik\form\ActiveForm::end();
                        ?>
                            
                            <?php \yii\widgets\Pjax::begin(['id' => 'recent-products',]); ?>
                            <?= \app\components\AdminGridView::widget([
                                'filterModel' => $activeSearchModel,
                                'dataProvider' => $activeDataProvider,
                                'rowOptions'   => function ($model, $key, $index, $grid) {
                                    return ['data-id' => $model->id];
                                },
                                'options'=>['class'=>'box-body table-responsive no-padding grid-view'],
                                'showHeader'=>true,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'title',
                                    //'description',
                                    'code',
                                    'couponUsage',
                                    'maxCouponUsage',
                                        [
                                           /* 'attribute' => 'fromDate',
                                            'value' => function($model, $attribute){
                                            return \backend\components\Helper::date($model->fromDate);
                                            }*/
                                            'attribute' => 'fromDate',
                                            'value' => function($model){ return \backend\components\Helper::date($model->fromDate); },
                                            'filter' => \kartik\field\FieldRange::widget([
                                                    'form' => $form,
                                                    'model' => $activeSearchModel,
                                                    'template' => '{widget}{error}',
                                                    'attribute1' => 'created_start',
                                                    'attribute2' => 'created_end',
                                                    'type' => \kartik\field\FieldRange::INPUT_DATE,
                                            ])
                                        ],
                                        [
                                           /* 'attribute' => 'toDate',
                                            'value' => function($model, $attribute){
                                                return \backend\components\Helper::date($model->toDate);
                                            }*/

                                            'attribute' => 'toDate',
                                            'value' => function($model){ return \backend\components\Helper::date($model->toDate); },
                                            'filter' => \kartik\field\FieldRange::widget([
                                                    'form' => $form,
                                                    'model' => $activeSearchModel,
                                                    'template' => '{widget}{error}',
                                                    'attribute1' => 'expired_start',
                                                    'attribute2' => 'expired_end',
                                                    'type' => \kartik\field\FieldRange::INPUT_DATE,
                                            ])
                                        ],

                                	'type',
                                    'discount'=>[
                                         'format' => 'html',
                                        'value' => function($model){
                                            return ($model->type == "fixed-cart" || $model->type == "fixed-product" || $model->type == "free-shipping")? \backend\components\Helper::money($model->discount) : $model->discount."%";
                                        }
                                    ],
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{view}{update}{delete}',
                                        'buttons' => [
                                            'view' => function($url, $model){
                                                return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.\yii\helpers\Url::to(['price-rules/view', 'id' => $model->id]).'" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>' : '';
                                            },
                                            'update' => function($url, $model){
                                                return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.\yii\helpers\Url::to(['price-rules/update', 'id' => $model->id]).'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>' : '';
                                            },
                                            'delete' => function($url, $model){
                                                return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.\yii\helpers\Url::to(['price-rules/delete', 'id' => $model->id]).'" title="Update" data-pjax="0" data-confirm ="Are you sure you want to delete this item?"><span class="fa fa-trash"></span></a>' : '';
                                            }
                                        ]
                                    ],
                                ]
                            ]); ?>
                             <?php  \yii\widgets\Pjax::end();  ?> 
                        <?php //\yii\widgets\Pjax::end(); ?>

                </div>

                <div class="tab-pane" id="expirecoupon">

                    <?php
                            $form = \kartik\form\ActiveForm::begin(['id' => 'pricerules-form']);
                            \kartik\form\ActiveForm::end();
                        ?>
                            
                            <?php \yii\widgets\Pjax::begin(['id' => 'recent-products',]); ?>
                            <?= \app\components\AdminGridView::widget([
                                'filterModel' => $expiredSearchModel,
                                'dataProvider' => $expiredDataProvider,
                                'rowOptions'   => function ($model, $key, $index, $grid) {
                                    return ['data-id' => $model->id];
                                },
                                'options'=>['class'=>'box-body table-responsive no-padding grid-view'],
                                'showHeader'=>true,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute'=>'title',
                                        'filter'=> Html::textInput("PriceRulesExpiredSearch[title]","",["class"=>"form-control"]),
                                    ],
                                    [
                                        'attribute'=>'description',
                                        'filter'=> Html::textInput("PriceRulesExpiredSearch[description]","",["class"=>"form-control"]),
                                    ],
                                    [
                                        'attribute'=>'code',
                                        'filter'=> Html::textInput("PriceRulesExpiredSearch[code]","",["class"=>"form-control"]),
                                    ],
                                    'couponUsage',
                                    'maxCouponUsage',
                                   /* [
                                        'attribute'=>'couponUsage',
                                        'filter'=> Html::textInput("PriceRulesExpiredSearch[couponUsage]","",["class"=>"form-control"]),
                                    ],
                                    [
                                        'attribute'=>'maxCouponUsage',
                                        'filter'=> Html::textInput("PriceRulesExpiredSearch[maxCouponUsage]","",["class"=>"form-control"]),
                                    ],*/
                                    [
                                        'attribute' => 'fromDate',
                                        'value' => function($model){ return \backend\components\Helper::date($model->fromDate); },
                                        'filter' => \kartik\field\FieldRange::widget([
                                            //'form' => $form,
                                            'model' => $expiredSearchModel,
                                            'template' => '{widget}{error}',
                                            //'attribute1' => 'created_start',
                                           // 'attribute2' => 'created_end',
                                            'type' => \kartik\field\FieldRange::INPUT_DATE,
                                            'name1' => 'PriceRulesExpiredSearch[created_start]',
                                            'name2' => 'PriceRulesExpiredSearch[created_end]',
                                        ])
                                    ],
                                    [
                                          
                                        'attribute' => 'toDate',
                                        'value' => function($model){ return \backend\components\Helper::date($model->toDate); },
                                        'filter' => \kartik\field\FieldRange::widget([
                                            //'form' => $form,
                                            'model' => $expiredSearchModel,
                                            'template' => '{widget}{error}',
                                            //'attribute1' => 'expexpired_start',
                                            //'attribute2' => 'expexpired_end',
                                            'type' => \kartik\field\FieldRange::INPUT_DATE,
                                            'name1' => 'PriceRulesExpiredSearch[expired_start]',
                                            'name2' => 'PriceRulesExpiredSearch[expired_end]',
                                        ])
                                    ],

                                    //'type',
                                    [
                                        'attribute'=>'type',
                                        'filter'=> Html::textInput("PriceRulesExpiredSearch[type]","",["class"=>"form-control"]),
                                    ],
                                    'discount'=>[
                                        'format' => 'html',
                                        //'filter' => Html::textInput("PriceRulesExpiredSearch[discount]","",["class"=>"form-control"]),
                                        'value' => function($model){
                                            return \backend\components\Helper::money($model->discount);
                                        }

                                    ],
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{view}{update}{delete}',
                                        'buttons' => [
                                            'view' => function($url, $model){
                                                return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.\yii\helpers\Url::to(['price-rules/view', 'id' => $model->id]).'" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>' : '';
                                            },
                                            'update' => function($url, $model){
                                                return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.\yii\helpers\Url::to(['price-rules/update', 'id' => $model->id]).'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>' : '';
                                            },
                                            'delete' => function($url, $model){
                                                return ($model->createdBy == Yii::$app->user->id || Yii::$app->user->identity->roleId == 1)? '<a href="'.\yii\helpers\Url::to(['price-rules/delete', 'id' => $model->id]).'" title="Update" data-pjax="0" data-confirm ="Are you sure you want to delete this item?"><span class="fa fa-trash"></span></a>' : '';
                                            }
                                        ]
                                    ],
                                ]
                            ]); ?>
                             <?php  \yii\widgets\Pjax::end();  ?> 
                        <?php //\yii\widgets\Pjax::end(); ?>

                </div>

                       
            </div>
        </div>
    </div>
</div>