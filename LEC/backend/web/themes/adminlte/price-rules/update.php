<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $priceRule app\models\PriceRules */

$this->title = 'Update Price Rule: ' . ' ' . $priceRule->title;
$this->params['breadcrumbs'][] = ['label' => 'PriceRules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $priceRule->title, 'url' => ['view', 'id' => $priceRule->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box">
<div class="price-rules-update products-create">

    <?= $this->render('_form', compact('priceRule')) ?>

</div>
</div>