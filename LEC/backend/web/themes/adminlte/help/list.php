<?php
use yii\helpers\Html;
use common\models\HelpCategories;
use common\models\HelpItems;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<div class="menus-index">
    <div class="box">
    	<div class="box-body">
    		<div class="menu-top-area row">
    			<div class="col-md-12">
		            <div class="row">
		                <div class="col-md-6">
		                    <menu id="nestable-menu">
		                        <button type="button" data-action="expand-all" class="btn btn-primary"><i class="fa fa-expand"></i> Expand All</button>
		                        <button type="button" data-action="collapse-all" class="btn btn-primary"><i class="fa fa-compress"></i> Collapse All</button>
                                <span style="float: right;">
                                    <?= Html::a('<i class="fa fa-plus"></i> Create Help Item', ['create'], ['class' => 'btn btn-success']) ?>
                                </span>
		                    </menu>
						</div>
		            </div>
        		</div>
    		</div>

    		<div class="row" id="row_main">
        		<div class="tab-content responsive">
            		<div class="col-md-6">
                		<div class="box box-primary">
                    		<div class="box-header"> <h3 class="box-title">Assigned Help Items</h3>
                        		<div class="dd box-body" id="nestable">
                            		<ol class="dd-list">
                            			<?php foreach ($helpCategories as $category) { 
                            				$catItems = HelpItems::find()->where(['enabled'=>1,'categoryId'=>$category->id])->orderBy('position')->all();
                            			?>
                            				<li class="dd-item dd3-item" data-id="<?=$category->id?>">
                            					<div class="dd-handle dd3-handle"></div>
                                            	<div class="dd3-content"><?=$category->title?></div>
                                                <ol class="dd-list">
                                                    <?php foreach ($catItems as $item) { ?>
                                                        <li class="dd-item dd3-item" data-id="<?=$item->id?>">
                                                            <div class="dd-handle dd3-handle"></div>
                                                            <div class="dd3-content"><?=$item->title?>
                                                                <u class="menu-icon-set">
                                                                    <?= Html::a('<i class="fa fa-pencil"></i> Edit', ['/help/update','id'=>$item->id]) ?>
                                                                </u>
                                                                <u class="menu-icon-set">
                                                                    <?= Html::a('<i class="fa fa-trash"></i> Delete', ['/help/delete', 'id' => $item->id],['data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?>
                                                                </u> 
                                                            </div>    
                                                        </li>    
                                                    <?php } ?>
                                                </ol>  
                                            </li>	
                            			<?php } ?>
                            		</ol>
                                    <textarea id="nestable-output" class="nestable-output" name="Categories[order]" style="display:none"></textarea>
                            	</div>
                            </div>
                       	</div>
                    </div>

                    <div class="col-md-6">
                		<div class="box box-primary">
                    		<div class="box-header"><h3 class="box-title">Unassigned Help Items</h3>
                        		<div class="dd box-body" id="nestable2">
                            		<ol class="dd-list">

                            		</ol>
                            	</div>
                            </div>
                        </div>
                    </div>        		
                </div>
            </div>                		
    	</div>
    </div>
</div>

<link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/adminlte/css/nestedSortable.css" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/adminlte/js/AdminLTE/jquery.nestable.js"></script>  

<script>

$(document).ready(function()
{
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // activate Nestable for list 2
    $('#nestable2').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));
    $('.dd').nestable('collapseAll');
    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
            //action ='collapse-all';
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
});
</script>  
<script type="text/javascript">
    $('.dd').on('change', function() { 
        setTimeout(function(){ 
            var treeOrder = $('.nestable-output').val();
            $.ajax({
                url: "<?=Url::to(['help/changeorder'])?>", 
                dataType: 'json',
                type : 'post',  
                data: {treeOrder: treeOrder}, 
                success: function(data){
                    alert('Order saved Sucessfully.');                                   
                }
            });
        }, 1000);     
    });
</script>   		     