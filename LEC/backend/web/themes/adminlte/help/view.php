<?php
use yii\helpers\Html;
use common\models\HelpCategories;
use common\models\HelpItems;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

	<!-- help-wrapper-bg -->
<div class="box">
	<div class="box-body">

    <section class="help-wrapper-bg">
        
        <!-- <div class="banner">
          <div class="container">
          	<div class="banner-content">
                <div class="help-search">
                  <input type="search" placeholder="How can we help?"/>
                  <a href="#" class="fa fa-search"></a>
                  <div class="clearfix"></div>
                </div>
            </div>
          </div>
        </div> -->
        
        <!-- <div class="container"> -->
              <div class="help-outer">
                <div class="help-search-list-head">
                <a href="<?=Url::to(['help/index'])?>"><i class="fa fa-reply"></i>Back to Home</a>
                <div class="clearfix"></div>
              </div>
              
              <div class="help-search-list-cnt">
                
                <div class="content-details">
                 	<h3><?=$helpItem->title?></h3>
                 	<?=$helpItem->content?>
                </div>
                
                <div class="clearfix"></div>
                <div class="hs-footer">
                  <!-- <div class="hs-inner pull-left">
                    <span>Need more detail?</span>
                    <?php // if(!is_null($helpItem->videoPath)){ ?>
                    	<a href="#" data-toggle="modal" data-target="#watchvideo">Watch Video</a>
                    <?php //} ?>	
                    <a href="<?php //Url::to(['help/screenshot','id'=>$helpItem->id])?>" class="cht-btn" target="_blank">View Screnshots</a>
                  </div> -->
                  <div class="hs-right">If this is not the answer you are looking for <a href="<?=Url::to(['site/contact'])?>"><i>Raise a Support Request</a></i></div>
                </div>
              </div>
            </div>
        <!--</div> 
        /container -->
        
    </section>
    <!-- /help-wrapper-bg -->

    <!-- Modal -->
    <?php //$videoLink = isset($helpItem->videoPath) ? $helpItem->videoPath :  "https://player.vimeo.com/video/137464081" ?>
    <div class="modal fade" id="watchvideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog help-vdo" role="document">
        <button type="button" class="close vdo-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!-- <iframe src="<?php //$videoLink?>" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
        <?php if(!is_null($helpItem->videoPath)){ ?>
          <?=$helpItem->videoPath?>
        <?php } ?>
      </div>
    </div>
        
    
    <!-- <div class="modal fade" id="#watchvideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div> -->
    </div>
   </div> 