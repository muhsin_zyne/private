<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\HelpCategories;
use common\models\HelpItems;
use dosamigos\ckeditor\CKEditor;
use \kartik\widgets\FileInput;
use yii\helpers\Json;

$this->title = 'Create Help Item';
$this->params['breadcrumbs'][] = ['label' => 'Help', 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="cms-pages-create">
	<div class="row">
		<div class="box">
			<div class="box-body">	
				<?php $form = ActiveForm::begin(['id'=>'item-create']); ?>
					<div class="form-group create-subminbutton">
                      	<?= Html::submitButton($helpItem->isNewRecord ? 'Create' : 'Update', ['class' => $helpItem->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
                    </div> 
					<?= $form->field($helpItem, 'title')->textInput(['maxlength' => 255]) ?>
					<?=$form->field($helpItem, 'categoryId')->dropDownList(ArrayHelper::map(HelpCategories::find()->all(), 'id', 'title'))?>
					<?= $form->field($helpItem, 'content')->widget(CKEditor::className(),
						[
							'id'=>'cmscontent',
							'preset' => 'full', 
							'clientOptions' =>[
								'language' => 'en', 
								'allowedContent' => true,                                                
								'filebrowserUploadUrl' =>Yii::$app->urlManager->createUrl(['help/image-upload']),
                                'filebrowserBrowseUrl'=>Yii::$app->urlManager->createUrl(['help/image-browse']), 
                            ]
                        ]) ?>
					<?= $form->field($helpItem, 'screenshots')->widget(CKEditor::className(),['id'=>'cmscontent','preset' => 'full', 'clientOptions' =>['language' => 'en', 'allowedContent' => true ]]) ?>
					<?= $form->field($helpItem, 'videoPath')->textInput(['maxlength' => 255]) ?>
					<?= $form->field($helpItem, 'enabled')->dropDownList(["1"=>"Yes","0"=>"No"]); ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>			
</div>