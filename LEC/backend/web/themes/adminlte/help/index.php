<?php
use yii\helpers\Html;
use common\models\HelpCategories;
use common\models\HelpItems;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>


<!-- help-wrapper-bg  -->
    <section class="help-wrapper-bg ">
    	
         <!--  <div class="container">
          	<div class="banner-content">
                <div class="help-search">
                  <!-- <input type="search" placeholder="How can we help?"/>
                  <a href="#" class="fa fa-search"></a> 
                  <form class="navbar-form pull-left" role="search" id="search_mini_form" action="/help/search" method="get">
                    <input type="text" class="form-control" placeholder="How can we help?" value="" name="q" id="searchname" required>
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </form>
                  <div class="clearfix"></div>
                </div>
            </div>
          </div> -->
        
        <div class="box box-body">
            <div class="help-outer">
            <?php foreach ($helpCategories as $category) { 
            	$helpItems = HelpItems::find()->where(['enabled'=>1,'categoryId'=>$category->id])->all();
            ?>
            	<div class="h-head">
                	<i class="fa fa-question"></i>
                	<span><?=$category->title?></span>
              	</div>
              	<div class="h-cnt">
              		<ul>
              		<?php 
              			if(count($helpItems) >= 1){   
              				foreach ($helpItems as $item) {
              		?>
                		<li><a href="<?=Url::to(['help/view','id'=>$item->id])?>"><?=$item->title?></a></li>
	                <?php } }  ?>
	           		</ul>
	            </div>
            <?php } ?>  	
            </div>
        </div>
        <!--/container -->
        
    </section>
    <!-- /help-wrapper-bg  -->