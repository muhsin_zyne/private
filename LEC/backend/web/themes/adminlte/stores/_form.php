<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Stores */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
  <div class="box-body">
  
  

<div class="stores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'abn')->textInput() ?>

    <?= $form->field($model, 'billingAddress')->textArea(['rows' => 6]) ?>

    <?= $form->field($model, 'shippingAddress')->textArea(['rows' => 6]) ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']); ?>

    <?= $form->field($model, 'adminId')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput() ?>

    <?= $form->field($model, 'logoPath')->textInput(array('placeholder' => 'Logo Path Example: /store/site/logo/logo-name.jpg')) ?>

    <?= $form->field($model, 'siteUrl')->textInput(array('placeholder' => 'Website URL Example: http://www.mywebsite.com.au')) ?>
   
    <?= $form->field($model, 'isVirtual')->dropDownList(['0' => 'B2C and B2B Store', '1' => 'B2B Store Only']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    </div>
</div> 

</div>
