

<?php



use yii\helpers\Html;

use yii\grid\GridView;



/* @var $this yii\web\View */

/* @var $searchModel common\models\search\StoresSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Stores';

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="box">
<div class="box-body">
<div class="shipments-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p class="pull-right">
        <?= Html::a('<i class="fa fa-plus"></i>  Create Stores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([

        'dataProvider' => $dataProvider,

        'filterModel' => $searchModel,

        'columns' => [

           // ['class' => 'yii\grid\SerialColumn'],



            'id',

            'title',

            'abn',

            'billingAddress:ntext',

            'shippingAddress:ntext',

            'email:email',

            

           // 'status',

            // 'adminId',

             'phone',

            // 'logoPath:ntext',

            // 'siteUrl:ntext',

            // 'facebookUrl:ntext',

            // 'twitterUrl:ntext',

            // 'instagramUrl:ntext',

            // 'isVirtual',



            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update} {delete}',
                'buttons' => [
                'view' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="View" data-pjax="0"><span class="fa fa-eye"></span></a>';
                },
                'update' => function ($url, $model, $key) {
                    return '<a href="'.$url.'" title="Update" data-pjax="0"><span class="fa fa-pencil"></span></a>';
                },
                'delete' => function ($url, $model, $key) {
                    return ($model->id == Yii::$app->params['storeId'] || Yii::$app->user->identity->roleId == 1)? '<a href="'.$url.'" title="Delete" data-pjax="0"><span class="fa fa-trash"></span></a>' : '';
                },
                ]
            ],

        ],

    ]); ?>




</div>

</div>
</div>



