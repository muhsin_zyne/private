<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Stores */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stores-view">
<div class="box">
  <div class="box-body">
  
  
    

    <p class="pull-right">
        <?= Html::a('<i class="fa fa-angle-left"></i>Back', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('<i class="fa fa-save"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>        
        <?= Html::a('<i class="fa fa-trash"></i>  Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'abn',
            'billingAddress:ntext',
            'shippingAddress:ntext',
            'email:email',
            'status',
            'adminId',
            'phone',
            'logoPath:ntext',
            'siteUrl:ntext',
            //'facebookUrl:ntext',
            //'twitterUrl:ntext',
            //'instagramUrl:ntext',
            'isVirtual',
        ],
    ]) ?>

</div>

</div>
</div>
