<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\StoresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stores-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'abn') ?>

    <?= $form->field($model, 'billingAddress') ?>

    <?= $form->field($model, 'shippingAddress') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'adminId') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'logoPath') ?>

    <?php // echo $form->field($model, 'siteUrl') ?>

    <?php // echo $form->field($model, 'facebookUrl') ?>

    <?php // echo $form->field($model, 'twitterUrl') ?>

    <?php // echo $form->field($model, 'instagramUrl') ?>

    <?php // echo $form->field($model, 'isVirtual') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
