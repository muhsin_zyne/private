<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotions */

$this->title = 'Create Consumer Promotions';
$this->params['breadcrumbs'][] = ['label' => 'Consumer Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-promotions-create">

    <div class="box">
    
    <div class="box-body">

    <?= $this->render('_form',compact('promotion','stores')) ?>
    
    </div>
    
    </div>

</div>
