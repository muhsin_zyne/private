<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotions */

$this->title = 'Update Consumer Promotions: ' . ' ' . $promotion->title;
$this->params['breadcrumbs'][] = ['label' => 'Consumer Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $promotion->title, 'url' => ['view', 'id' => $promotion->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="consumer-promotions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('promotion','stores','promotionProducts')) ?>

</div>
