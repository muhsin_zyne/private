<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;

use kartik\date\DatePicker;

use backend\components\Helper;

use backend\assets\AppAsset;

use yii\grid\GridView;

use common\models\User;

//use kartik\tree\TreeViewInput;

use common\models\Categories;

use common\models\ProductCategories;

use common\models\ConsumerPromotionProducts;

use yii\web\UploadedFile;

use kartik\widgets\FileInput;

use yii\helpers\Url;

use backend\components\TreeView;

use yii\helpers\ArrayHelper;



$user = User::findOne(Yii::$app->user->id);





/* @var $this yii\web\View */

/* @var $model common\models\ConsumerPromotions */

/* @var $form yii\widgets\ActiveForm */

?>



<link rel='stylesheet' href='/css/easyTree.css' />

<link rel='stylesheet' href='/css/style_treeview.css' />



<div class="error-msg"> </div>



<div class="consumer-promotions-form">



    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','class'=>'promotions-fields']]); ?>



    <?= $form->field($promotion, 'title')->textInput(['maxlength' => 45]) ?>



    <?= $form->field($promotion, 'shortTag')->textInput(['maxlength' => 45]) ?>



    <?=DatePicker::widget([

        'form' => $form,

        'model' => $promotion,

        'attribute' => 'fromDate',

        'pluginOptions' => [

        'format' => 'dd-MM-yyyy',

        'autoclose' => true,

        ],

    ]);

    ?>



    <?=DatePicker::widget([

        'form' => $form,

        'model' => $promotion,

        'attribute' => 'costEndDate',

        'pluginOptions' => [

        'format' => 'dd-MM-yyyy',

        'autoclose' => true,

        ],

    ]);

    ?>



    <?=DatePicker::widget([

        'form' => $form,

        'model' => $promotion,

        'attribute' => 'consumerStartDate',

        'pluginOptions' => [

        'format' => 'dd-MM-yyyy',

        'autoclose' => true,

        ],

    ]);

    ?>



    <?=DatePicker::widget([

        'form' => $form,

        'model' => $promotion,

        'attribute' => 'toDate',

        'pluginOptions' => [

        'format' => 'dd-MM-yyyy',

        'autoclose' => true,

        ],

    ]);

    ?>  



    <?= $form->field($promotion, 'image')->fileInput() ?>



    <?= $form->field($promotion, 'catalog')->textarea(['rows' => 6])->label('Catalogue and Embed Code') ?>



    <?php  

        $selected = [];

        if(!$promotion->isNewRecord){

            foreach($promotion->stores as $store){ //var_dump($store->storeId);die();

                $selected[$store->storeId] = ['Selected' => 'selected'];

            }

        }



        //var_dump($selected);die();

    ?>



    <?php if($user->roleId != "3") { ?>

        <?= $form->field($promotion, 'stores')->dropDownList(ArrayHelper::map($stores, 'id', 'title'), ['multiple' => 'multiple', 'options' => $selected])->label('Stores') ?>

    <?php } ?>    



    <?php /*DatePicker::widget([

        'form' => $form,

        'model' => $promotion,

        'attribute' => 'reminderDate',

        'pluginOptions' => [

        'format' => 'dd-MM-yyyy',

        'autoclose' => true,

        ],

    ]);*/

    ?> 



    <div class="add_tray_products" style="width:100%">

        <div class="row">

            <div class="col-md-6">

                <div class="categories sm-100">

                        <?=TreeView::widget()?> 

                        <?= Html::a('Load products','javascript:;',$options = ['class'=>'btn btn-primary load_pdt']) ?>

                </div>

            </div>

            <div class="col-md-6">

                <div class="search sm-100">

                    <label class="kv-heading-container" for="products-size">Search</label>

                    <?= Html::textInput('search','',$options=['class'=>'product_search form-control','style'=>'','placeholder'=>'Enter SKU or name of a product','autocomplete'=>'off']) ?>

                    <span>When you type the letter, result will get displyed below.</span>

                    <div class="product_box">

                        <div class="product_list"></div>  

                        <span style="font-style:italic;font-size:12px;">Tick the items and click the 'Add to list' button below</span>  

                        <div class="form-group" style="margin-top:20px;margin-bottom:30px">

                            <?= Html::a('Add to list','javascript:;',$options = ['class'=>'btn btn-primary add_pdt']) ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <br/>

        <div class="clear"></div>

    </div>



    <div class="tray_products_table">

        <div class="table-responsive">

            <table border="1" cellspacing="0" cellpadding="0" class="table sm-table">

                <thead>

                    <tr class="headings">

                        <th style="width:3%;"></th>

                        <th style="width:22%;">Name</th>

                        <th style="width:12%;">SKU</th>

                        <!-- <th>EZ Code</th> -->

                        <th style="width:10%;">Original sell price</th>

                        <th style="width:10%;">Original cost price</th>

                        <th style="width:10%;">Catalogue Sell Price</th>

                        <th style="width:10%;">Catalogue Cost Price</th>

                        <th style="width:10%;">Page ID</th>

                        <th style="width:8%;">Remove</th>

                    </tr>

                </thead>

                <tbody>

                    <?php  

                        if(!$promotion->isNewRecord) {    //var_dump($promotionProducts);die();

                            if(!empty($promotionProducts)) {

                                foreach ($promotionProducts as $product) { //var_dump($product['offerSellPrice']);die();

                                    if(!is_null($product->product)){ 

                    ?>

                        <tr data-productId=<?=$product['id']?>>

                            <td><input type="checkbox" checked name="Products[id][<?=$product['id']?>]" class="selected" value="<?=$product['id']?>"></td>

                            <td><?=$product->product->name?></td>

                            <td><?=$product->product->sku?></td>

                            <!-- <td><?php //$product->product->ezcode?></td> -->

                            <td><?=$product->product->price?></td>

                            <td><?=$product->product->cost_price?></td>

                            <td><input type="text" name="Products[<?=$product['id']?>][offerSellPrice]" class="cost_price_promotions" autocomplete="off" value="<?=$product['offerSellPrice']?>"></td>

                            <td><input type="text" name="Products[<?=$product['id']?>][offerCostPrice]" class="cost_price_promotions" autocomplete="off" value="<?=$product['offerCostPrice']?>"></td>

                            <td><input type="text" name="Products[<?=$product['id']?>][pageId]" class="cost_price_promotions no-err" value="<?=$product['pageId']?>" autocomplete="off"></td>

                            <td><a href="#" class="remove" title="Delete">X</td>

                        </tr>

                    <?php } } } } ?>

                </tbody>    

            </table>  

        </div>  

    </div> 

    <input type="hidden" name="selected_categories" class="category-ids"> 

    <div class="form-group">

        <?= Html::submitButton($promotion->isNewRecord ? 'Create' : 'Update', ['class' => $promotion->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    </div>



    <?php ActiveForm::end(); ?>

    

    <div class="autofile-upload btm-upld">

        <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data','class'=>'promotions']]); ?>

        <label class="control-label">Import CSV (Optional)</label>

        <?=Html::input('file', 'csv', '', ['class' =>'csv-file-upload']) ?>

        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>

    </div>



    <div class="clear"></div>

    <div class="deleted_items"></div>



</div>



<script type="text/javascript">

    $(document).ready(function() {



        $(".product_search").on('input',function(e){

            $('.product_list').append('<div class="wait-gif" style="width:100%;text-align:center;" ><img src="/images/wait.gif" height="170"></div>');

        }); 



        addedproducts = [];

        listedproducts = [];

        var typingTimer;

        

        $(".product_search").on('keyup',function(e){

            $('.product_list').empty("");

            $('.product_list').append('<div class="wait-gif" style="width:100%;text-align:center;" ><img src="/images/wait.gif" height="170"></div>');

            clearTimeout(typingTimer);

            $('.selected:checked').each(function(){

                if ($.inArray($(this).attr('value'), addedproducts) == -1) {

                    addedproducts.push($(this).attr('value'));

                }    

            });

            

            $(".tray_products_table table tbody tr").each(function(){ 

                if($(this).attr('data-productId')) {

                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 

                        listedproducts.push($(this).attr('data-productId')); 

                    }

                }        

            });





            typingTimer = setTimeout(function(){

            var keyword = $('.product_search').val();

            if(keyword.length >= 2)

            {

                $.ajax({

                    url:"<?php echo Yii::$app->urlManager->createUrl(['consumer-promotions/getproducts'])?>",    

                    type: 'POST',

                    data: {keyword: keyword},

                    success: function(data)

                        {

                            $('.product_list').empty("");

                            $(".product_item").empty("");

                            if(jQuery.trim($(".product_search")).length > 0) {   

                                $.each($.parseJSON(data), function(key, item) { 

                                    if ($.inArray(key, listedproducts) == -1) {

                                        if ($.inArray(key, addedproducts) == -1) {    

                                            $('.product_list').append('<div class="product_item"><input type="checkbox" name="Products[]" value="'+item['id']+'" class="product_checkbox" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'" data-costprice="'+item['cost_price']+'"><label style="padding:5px;">'+item['name']+'</label></div>');

                                        }    

                                    }    

                                }); 

                            }   

                        }

                });

            }

            if(keyword.length == 0){

                    $('.product_list').empty("");

            }

            },1000);    

        });



        $('#w1-tree').height('150');



        $('.add_pdt').on('click',function(){ 



            $(".tray_products_table table tbody tr").each(function(){ 

                if($(this).attr('data-productId')) {

                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {

                        listedproducts.push($(this).attr('data-productId')); 

                    }

                }        

            });



            $('.selected:checked').each(function(){

                if ($.inArray($(this).attr('value'), addedproducts) == -1) {

                    addedproducts.push($(this).attr('value'));

                }    

            });



            checked_pdts = [];



            $('.product_checkbox:checked').each(function(){

                        checked_pdts.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price'), 'cost_price' : $(this).attr('data-costprice')});

            }); 



            $(".tray_products_table").css('display','block');



            for( var i = 0, xlength = checked_pdts.length; i < xlength; i++)

                { 

                    if ($.inArray(checked_pdts[i].idss, listedproducts) == -1) {



                        if ($.inArray(checked_pdts[i].idss, addedproducts) == -1) {



                            $(".tray_products_table table tbody").append('<tr data-productId="'+checked_pdts[i].idss+'"><td><input type="checkbox" name="Products[id]['+checked_pdts[i].idss+']" class="selected" value="'+checked_pdts[i].idss+'" checked></td><td>'+checked_pdts[i].names+'</td><td>'+checked_pdts[i].sku+'</td><td><input type="text" name="Products['+checked_pdts[i].idss+'][original_sell_price]" class="cost_price_promotions" value="'+checked_pdts[i].price+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][original_cost_price]" class="cost_price_promotions" value="'+checked_pdts[i].cost_price+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][offerSellPrice]" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][offerCostPrice]" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][pageId]" class="cost_price_promotions no-err" value="" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products['+checked_pdts[i].idss+'][newitem]" id="data-new" value="1">');

                        }    

                    }    

                }   

        });



    });     

</script>



<script type="text/javascript">

    $(".tray_products_table").on("click",".remove", function(e){  

            

        var trid = $(this).closest('tr').attr('data-consumerproductId');  



        //console.log(trid); 

        if(trid) {

            jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedItems][]" id="deletedItems" value="'+trid+'"/>');

        }

        $(this).closest('tr').remove();    

    }); 

</script>



<script type="text/javascript">

    <?php 

        if($promotion->isNewRecord) {    ?>

          /*  if(!empty($promotion->products)) {*/

            $(".tray_products_table").css('display','none');

        //$(".tray_products_table").css('display','block');



    <?php } /* foreach ($promotion->promotionProducts as $promotionProduct)  { ?>



                $(".tray_products_table table tbody").append('<tr data-productId="<?=$promotionProduct->id?>"><td><input type="checkbox" checked name="Products[id][<?=$promotionProduct->product->id?>]" class="selected" value="<?=$promotionProduct->product->id?>"></td><td><?=$promotionProduct->product->name?></td><td><?=$promotionProduct->product->sku?></td><td>###</td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][original_sell_price]" class="cost_price_promotions" value="<?=$promotionProduct->product->price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][original_cost_price]" class="cost_price_promotions" value="<?=$promotionProduct->product->cost_price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][offerSellPrice]" value="<?=$promotionProduct->offerSellPrice?>" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][offerCostPrice]" value="<?=$promotionProduct->offerCostPrice?>" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][pageId]" class="cost_price_promotions" value="<?=$promotionProduct->pageId?>" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td>');  



               

            } 

            }

            }*/

    ?>    

</script>



<script type="text/javascript">



    $(document).ready(function() {



        addedproducts = [];

        listedproducts = [];



        $('.load_pdt').on('click',function(){ 



            $('.selected:checked').each(function(){

                if ($.inArray($(this).attr('value'), addedproducts) == -1) {

                    addedproducts.push($(this).attr('value'));

                }    

            });

            

            $(".tray_products_table table tbody tr").each(function(){ 

                if($(this).attr('data-productId')) {

                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {

                        listedproducts.push($(this).attr('data-productId')); 

                    }

                }        

            });



            /*categoryIds = Array();



            $('.selectcat:checked').each(function(){

                if ($.inArray($(this).attr('value'), categoryIds) == -1) {

                    categoryIds.push($(this).attr('value'));

                }    

            });*/



            if($(".selectcat:checked").val() != "") {



                categoryIds = $('.category-ids').val();



                $.ajax({

                    url: "<?php echo Yii::$app->urlManager->createUrl(['consumer-promotions/getcategoryproducts'])?>",    

                    type: 'POST',

                    data: {categoryIds: categoryIds},

                    success: function(data)     

                        {

                                    

                            $(".tray_products_table").css('display','block');



                            <?php 

                                if($promotion->isNewRecord) {  //die('Update');

                                    if(empty($promotion->products)) {

                            ?>



                            $(".tray_products_table table tbody").empty();



                            <?php } } ?>



                            $.each($.parseJSON(data), function(key, item) {

                            if ($.inArray(key, listedproducts) == -1) {

                                if ($.inArray(key, addedproducts) == -1) {



                                        $(".tray_products_table table tbody").append('<tr data-productId="'+item['id']+'"><td><input type="checkbox" name="Products[id]['+item['id']+']" class="selected" value="'+item['id']+'" checked></td><td>'+item['name']+'</td><td>'+item['sku']+'</td><td><input type="text" name="Products['+item['id']+'][original_sell_price]" class="cost_price_promotions" value="'+item['price']+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+item['id']+'][original_cost_price]" class="cost_price_promotions" value="'+item['cost_price']+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+item['id']+'][offerSellPrice]" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products['+item['id']+'][offerCostPrice]" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products['+item['id']+'][pageId]" class="cost_price_promotions no-err" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr></tr><input type="hidden" name="Products['+item['id']+'][newitem]" id="data-new" value="1">');

                                    }    

                                }    

                            }); 

                        }

                }); 

            }



            else{



                alert('Please select a category');    



                $(".tray_products_table").css('display','block');



                <?php 

                    if(!$promotion->isNewRecord) {  //die('Update');

                        if(!empty($promotion->products)) {

                ?>



                $(".tray_products_table table tbody").empty();



                $(".tray_products_table table tbody").append('<tr><td> No items found </td> </tr>');



                <?php } } ?>





            }



        });



        ids =Array();



        $('.selectcat').change(function(){

            if($(this).prop('checked')){

                ids.push($(this).val());

                $('.category-ids').val(ids);

            }



            else{

                pos = ids.indexOf($(this).val());

                ids.splice(pos,1);

                $('.category-ids').val(ids);

            }

        });

    });

</script>



<script type="text/javascript">

    $(document).ready(function() {

       /* $(".promotions").on('submit',(function(e) {

            var reg=/(.csv)$/;

            if (!reg.test($(".csv-file-upload").val())) {

                alert('Invalid File Type');

                return false;

            }

            else{

                var files = this.files[0];

                var formData = new FormData();

               // data.append('input_file_name', $('your_file_input_selector').prop('files')[0]);

                formData.append('formData', files);

                console.log(formData);

                $.ajax({

                    url: '<?php echo Yii::$app->request->baseUrl;?>/index.php?r=consumerpromotions/test',  //Server script to process data

                    type: 'POST',

                    data: formData,

                    dataType : 'json',  

                    contentType: false,

                    processData: false,

                    success: function(html){

                        alert(html);

                    }

                });

            }

        }));*/



        //console.log('before:'+listedproducts);



        //console.log('before:'+addedproducts);



        $(".promotions").on('submit',(function(e) {

        e.preventDefault();

        $(".upload-msg").text('Loading...');    



        $(".tray_products_table table tbody tr").each(function(){ 

            if($(this).attr('data-productId')) {

                if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {

                    listedproducts.push($(this).attr('data-productId')); 

                }

            }        

        });



        $('.selected:checked').each(function(){

            if ($.inArray($(this).attr('value'), addedproducts) == -1) {

                addedproducts.push($(this).attr('value'));

            }    

        });



        console.log('after:'+listedproducts);



        console.log('after:'+addedproducts);





        $.ajax({

            url: "<?php echo Yii::$app->urlManager->createUrl(['consumer-promotions/test'])?>",       

            type: "POST",            

            data: new FormData($(this)[0]), 

            contentType: false,       

            cache: false,             

            processData:false,        

            success: function(data)   

            {

                

            $(".tray_products_table").css('display','block');



                $.each($.parseJSON(data), function(key, item) {

                    if(item.length != 0){

                        if(key == "notfound"){ 

                            for( var i = 0, xlength = item.length; i < xlength; i++)

                            {  

                                $(".error-msg").append('<div class="alert alert-danger alert-dismissable"><i class="icon fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Not found this products with sku :'+item[i]['sku']+' </div>');

                                //console.log(item);

                            }    

                        }

                        else{

                            for( var i = 0, xlength = item.length; i < xlength; i++)

                            {

                                if ($.inArray(item[i]['id'], listedproducts) == -1) {  



                                   //console.log('listedproducts : '+listedproducts);

                                    //console.log('addedproducts : '+addedproducts);



                                    if ($.inArray(item[i]['id'], addedproducts) == -1) {



                                        $(".tray_products_table table tbody").append('<tr data-productId="'+item[i]['id']+'"><td><input type="checkbox" checked name="Products[id]['+item[i]['id']+']" class="selected" value="'+item[i]['id']+'"></td><td>'+item[i]['name']+'</td><td>'+item[i]['sku']+'</td><td><input type="text" name="Products['+item[i]['id']+'][original_sell_price]" class="cost_price_promotions" value="'+item[i]['price']+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+item[i]['id']+'][original_cost_price]" class="cost_price_promotions" value="'+item[i]['costprice']+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+item[i]['id']+'][offerSellPrice]" value="'+item[i]['offerSellPrice']+'" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products['+item[i]['id']+'][offerCostPrice]" value="'+item[i]['offerCostPrice']+'" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products['+item[i]['id']+'][pageId]" class="cost_price_promotions no-err" value="'+item[i]['pageId']+'" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products['+item[i]['id']+'][newitem]" id="data-new" value="1">');

                                    }    

                                }    

                            }    

                        }

                

                    }   

                });    



            }

            //return true;

        });

    }));



    });

    

        $('form').submit(function(e) {

            var isValid = true;

            $( ".cost_price_promotions" ).each(function() {

                if(!$(this).hasClass('no-err')) {

                    if($(this).val()) {

                        if($(this).hasClass('error_box'))

                            $(this).removeClass('error_box');

                    }

                    else{

                        isValid = false;

                        $(this).css({

                            "border": "1px solid red !important;",

                            "background": "#FFCECE",

                        });

                        $(this).addClass('error_box');

                        $(this).attr("placeholder", "This field can not be blank!");

                    }

                }    

            });

            if(isValid == false){ 

                //myFunc();

                e.preventDefault();

            }

        }); 



</script>        





