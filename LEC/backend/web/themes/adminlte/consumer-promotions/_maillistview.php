<?php
	use yii\helpers\Html;
  	use frontend\components\Helper;
 $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage();
?>

<tr>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$index +1?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
        <img src="<?=$thumbImagePath?>" alt="items" width="66px">
    </td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
        <?=Helper::stripText($model->product->name,15) ?><br>
        <span style="text-decoration:line-through;color:#999999;"><?=Helper::money($model->product->price)?></span>
    </td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;"><?=$model->product->sku ?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=Helper::money($model->offerSellPrice)?></td>
</tr>

