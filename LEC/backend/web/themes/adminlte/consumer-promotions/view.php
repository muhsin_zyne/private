<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\components\Helper;



/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotions */

$this->title = $promotion->title;
$this->params['breadcrumbs'][] = ['label' => 'Consumer Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-promotions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $promotion->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $promotion->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?php if($promotion->status =="not-published") { ?>
            <?= Html::a('Publish', ['publish', 'id' => $promotion->id], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $promotion,
        'attributes' => [
            'id',
            'title',
            'shortTag',
            [   
                'attribute' => 'fromDate',
                'value' =>  Helper::date($promotion->fromDate),
            ],
            [   
                'attribute' => 'costEndDate',
                'value' =>  Helper::date($promotion->costEndDate),
            ],
            [   
                'attribute' => 'consumerStartDate',
                'value' =>  Helper::date($promotion->consumerStartDate),
            ],
            [   
                'attribute' => 'toDate',
                'value' =>  Helper::date($promotion->toDate),
            ],
            /*[   
                'attribute' => 'createdOn',
                'value' =>  Helper::date($promotion->createdOn),
            ],*/
            //'image',
            'catalog:ntext',
            /*[   
                'attribute' => 'reminderDate',
                'value' =>  Helper::date($promotion->reminderDate),
            ],*/
        ],
    ]) ?>

    <?php //var_dump($dataProvider);die(); ?>

    <?=GridView::widget([
        'dataProvider' => $promotionpdts,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'product.name',
            'product.sku',
            'product.price',
            'product.cost_price',
            'offerSellPrice',
            'offerCostPrice',
            'pageId',
            /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            ],*/
        ],
]); 
?>

</div>
