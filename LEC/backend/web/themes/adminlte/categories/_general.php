<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
//use kartik\tree\TreeView;
use common\models\Categories;
use common\models\ProductCategories;
use backend\components\TreeView;
use yii\helpers\Url;
//use kartik\tree\models\Tree;
//use kartik\tree\controllers\NodeController;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/adminlte/css/nestedSortable.css" />
<div class="categories-index">
<div class="box">

  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
      <li class="active"><a data-toggle="tab" href="#general">General</a></li>
      
      <!-- <li><a data-toggle="tab" href="#custom_design">Custom Design</a></li> -->
      
      <li><a data-toggle="tab" href="#category_products">Category Products</a></li>
    </ul>
  </div>
  <div class="box-body">

<div class="row">
  <div class="col-xs-12">
    
      
        <div class="tab-content responsive">
          <div class="tab-pane active" id="general">
            <?php /*echo \kartik\tree\TreeView::widget([

 'query' => \Common\models\Categories::find()->addOrderBy('root, lft'),

 //'nodeView' => '@kvtree/views/_form',

 'headingOptions' => ['label' => 'Categories'],

 'rootOptions' => ['label'=>'<span class="text-primary">Root</span>'],
  </div>
</div>
</div>
<script type="text/javascript">

 'fontAwesome' => true,

 'isAdmin' => true,

 'displayValue' => 19,

 'softDelete' => true,

 'cacheSettings' => ['enableCache' => true],



// //'iconEditSettings' => ['show' => 'text','type' => TreeView::ICON_CSS,'listData' => []]

 ]);*/

?>
            <div class="col-sm-4 tree-section">
              <?php //TreeView::widget(['modelClass'=>'\common\models\Categories']);  ?>
              <?=TreeView::widget(['readOnly' => false])?>
            </div>
            <div class="col-md-8">
              <?php if(!$model->isNewRecord) { ?>
              <div class="delete_catbutton" style="float:right; margin-bottom:10px;"> 
                <!-- <a href="javascript: void(0);" class="btn btn-primary add_category">Add Category</a> -->
                <a href="javascript: void(0);" class="btn btn-primary add_subcategory">Add Sub Category</a> 
                <a href="javascript: void(0);" class="btn btn-primary delete_category">Delete Category</a> 
              </div>
              <?php }  ?>
              <?php $action = ($model->isNewRecord) ? Url::to(['categories/create']) : Url::to(['categories/update','selected'=>$model->id])   ?>
              <?php $form = ActiveForm::begin(['id' => 'category-details','action' => $action, 'enableAjaxValidation' => true]); ?>
              <?php $id = isset($model->id) ? $model->id : " "; ?>
              <span>ID:<?=$id?></span>
              <?= $form->field($model, 'title')->textInput(['maxlength' => 45]) ?>
              <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
              <?= $form->field($model, 'pageTitle')->textInput(['maxlength' => 45]) ?>
              <?= $form->field($model, 'metaKeywords')->textarea(['rows' => 3]) ?>
              <?= $form->field($model, 'metaDescription')->textarea(['rows' => 3]) ?>
              <?= $form->field($model, 'urlKey')->textInput(['maxlength' => 45]) ?>
              <?= $form->field($model, 'disabled')->dropDownList([ '0' => 'Yes','1' => 'No']) ?>
              <input type="hidden" name="selected" class="selected" value="">
              <textarea id="nestable-output" class="nestable-output" name="Categories[order]" style="display:none"></textarea>

              <?php if(!$model->isNewRecord) { ?>
                  <input type="hidden" name="Categories[parent]" class="parent" value="<?=$model->parent?>">
              <?php } else { ?>
                  <input type="hidden" name="Categories[parent]" class="parent" value="">
              <?php } ?>
              <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
              </div>
              <?php ActiveForm::end(); ?>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="tab-pane" id="category_products">
            <?=GridView::widget([

        'dataProvider' => $dataProvider,

        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'product.name','label'=>'Item(s)'],

            ['attribute'=>'product.sku','label'=>'SKU'],

            [   

                'class' => 'yii\grid\ActionColumn',

                'template' => '{update}',

                'buttons' => [

                    'update' => function ($url, $model, $key) {

                       return '<a href="/products/update?id='.$model->productId.'" title="Update Product" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>';

                    },

                ]

            ],

        ],

    ]); 

?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">

    $(".delete_category").click(function(e){
        selected = $('.selected').val();
        if(confirm("Are you sure?")){
          $.ajax({
            url:'<?php echo Yii::$app->request->baseUrl;?>/categories/delete',
            data: {selected: selected}, 
            success: function(data)
              {
                  //location.reload();
              }
          });
        }

        else{
          return false;
        }    
    });

    $(".tree_keyword").keydown(function() {
        var keyword = $(this).val();
        $.ajax({
              url:'<?php echo Yii::$app->request->baseUrl;?>/categories/search',   
              data: {keyword: keyword}, 
              success: function(data)
                {
                    var catwithkey = [];
                    $($.parseJSON(data)).map(function () {
                        catwithkey.push(this.id);
                    });
                    $( ".dd-item" ).removeClass('hai');
                    $( ".dd-item" ).each(function( index ) {
                    });
                }
        });
    });   

    $('.category-text').click(function(e){ 
        var selected = $(this).closest('li').attr('data-id');
        // var parent = $(this).closest('li').parent().parent().attr('data-id');
        // console.log(parent);
        // $('.parent').val(parent);
        $('.selected').val(selected);
        $.ajax({
            
            url:'<?php echo Yii::$app->request->baseUrl;?>/categories/update',   
            data: {selected: selected}, 
            success: function(data)
              {
                $('.content').html(data);
                $('.selected').val(selected);
                $( ".dd-item" ).each(function( index ) {
                  if($(this).attr('data-id') == selected){
                    $(this).find('.dd3-content > span').addClass('selected-cat');
                    $(this).parent().show();
                    // $(this).parent().parent().find('.dd3-content fa').removeClass('fa-plus');
                    // $(this).parent().parent().find('.dd3-content fa').addClass('fa-minus');
                  }
                }); 
              }
        });
    });
    
    $('.add_category').click(function(){
        $.ajax({
            url:'<?php echo Yii::$app->request->baseUrl;?>/categories/create',
            success: function(data)
                {
                  $('.content').html(data);
                }
        });
    })

     $('.add_subcategory').click(function(){
        parent = $('.selected').val();
        $.ajax({
            url:'<?php echo Yii::$app->request->baseUrl;?>/categories/create',
            success: function(data)
                {
                  $('.content').html(data);
                  $('.parent').val(parent);
                }
        });
    })
</script> 

<script type="text/javascript">
    /*$(document).ready(function() {
        $('#category-details').submit(function(e) {
            data = $('#category-details').serialize();
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '<?php echo ($model->isNewRecord) ? Yii::$app->urlManager->createUrl(["categories/create"]) : Yii::$app->urlManager->createUrl(["categories/update","id"=>$model->id])?>',
                data: data,
                success: function(){ console.log(data);
                    
                    return false;
                }
            });
        });
    });*/ 
</script>
