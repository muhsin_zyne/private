<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Categories */

$this->title = 'Create Categories';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-page">

    <h1><?php // Html::encode($this->title) ?></h1>

    <?= $this->render('_general', compact('model','dataProvider')) ?>

</div>
