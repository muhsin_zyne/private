<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use kartik\widgets\TimePicker;

?>


<div class="box">
    <div class="box-body">
        <div class="b2b-addresses-form">
        <?php  ?>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'ownerId')->dropDownList($listData,['prompt'=>'Select...']);?>

            <?= $form->field($model, 'storeName')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'streetAddress')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'state')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'country')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'postCode')->textInput() ?>

            <?= $form->field($model, 'phone')->textInput() ?>

            <?= $form->field($model, 'latitude')->textInput() ?>

            <?= $form->field($model, 'longitude')->textInput() ?>

            <?php if($model->isNewRecord){
                $model->mondayFrom='08:00 AM'; $model->mondayTo='5:00 PM';
                $model->tuesdayFrom='08:00 AM'; $model->tuesdayTo='5:00 PM';
                $model->wednesdayFrom='08:00 AM'; $model->wednesdayTo='5:00 PM';
                $model->thursdayFrom='08:00 AM'; $model->thursdayTo='5:00 PM';
                $model->fridayFrom='08:00 AM'; $model->fridayTo='5:00 PM';
                $model->saturdayFrom='08:00 AM'; $model->saturdayTo='5:00 PM';
                $model->sundayFrom='00:00 AM'; $model->sundayTo='00:00 PM';
            } else {
                //var_dump($model->b2bTradingHoursFormatted);die;
                $model->mondayFrom=isset($model->b2bTradingHoursFormatted['0']['monday']['from']) ? $model->b2bTradingHoursFormatted['0']['monday']['from'] :'';
                $model->mondayTo=isset($model->b2bTradingHoursFormatted['0']['monday']['to']) ? $model->b2bTradingHoursFormatted['0']['monday']['to'] :'';
                $model->tuesdayFrom=isset($model->b2bTradingHoursFormatted['0']['tuesday']['from']) ? $model->b2bTradingHoursFormatted['0']['tuesday']['from'] : '' ;
                $model->tuesdayTo=isset($model->b2bTradingHoursFormatted['0']['tuesday']['to']) ? $model->b2bTradingHoursFormatted['0']['tuesday']['to'] : '';
                $model->wednesdayFrom=isset($model->b2bTradingHoursFormatted['0']['wednesday']['from']) ? $model->b2bTradingHoursFormatted['0']['wednesday']['from'] : ''; 
                $model->wednesdayTo=isset($model->b2bTradingHoursFormatted['0']['wednesday']['to']) ? $model->b2bTradingHoursFormatted['0']['wednesday']['to'] : '';
                $model->thursdayFrom=isset($model->b2bTradingHoursFormatted['0']['thursday']['from']) ? $model->b2bTradingHoursFormatted['0']['thursday']['from'] : ''; 
                $model->thursdayTo=isset($model->b2bTradingHoursFormatted['0']['thursday']['to']) ? $model->b2bTradingHoursFormatted['0']['thursday']['to'] : '';
                $model->fridayFrom=isset($model->b2bTradingHoursFormatted['0']['friday']['from']) ? $model->b2bTradingHoursFormatted['0']['friday']['from']: ''; 
                $model->fridayTo=isset($model->b2bTradingHoursFormatted['0']['friday']['to']) ? $model->b2bTradingHoursFormatted['0']['friday']['to'] : '';
                $model->saturdayFrom=isset($model->b2bTradingHoursFormatted['0']['saturday']['from']) ? $model->b2bTradingHoursFormatted['0']['saturday']['from'] : ''; 
                $model->saturdayTo=isset($model->b2bTradingHoursFormatted['0']['saturday']['to']) ? $model->b2bTradingHoursFormatted['0']['saturday']['to'] :'';
                $model->sundayFrom=isset($model->b2bTradingHoursFormatted['0']['sunday']['from']) ? $model->b2bTradingHoursFormatted['0']['sunday']['from'] : ''; 
                $model->sundayTo=isset($model->b2bTradingHoursFormatted['0']['sunday']['to']) ? $model->b2bTradingHoursFormatted['0']['sunday']['to'] : '';
            } ?>
            <table>
                <tr>
                    <td><span class="f-chk-mon"><?= $form->field($model, 'mondayFrom')->widget(TimePicker::classname(), []);?></span></td> 
                    <td><span class="f-chk-tue"><?= $form->field($model, 'tuesdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
                    <td><span class="f-chk-wed"><?= $form->field($model, 'wednesdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
                    <td><span class="f-chk-thu"><?= $form->field($model, 'thursdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
                    <td><span class="f-chk-fri"><?= $form->field($model, 'fridayFrom')->widget(TimePicker::classname(), []);?></span></td> 
                    <td><span class="f-chk-sat"><?= $form->field($model, 'saturdayFrom')->widget(TimePicker::classname(), []);?></span></td> 
                    <td><span class="f-chk-sun"><?= $form->field($model, 'sundayFrom')->widget(TimePicker::classname(), []);?></span></td>                   
                </tr>
                <tr>
                    <td align="center">to</td>
                    <td align="center">to</td>
                    <td align="center">to</td>
                    <td align="center">to</td>
                    <td align="center">to</td>
                    <td align="center">to</td>
                    <td align="center">to</td>
                </tr>
                <tr>                  
                    <td><span class="f-chk-mon"><?= $form->field($model, 'mondayTo')->widget(TimePicker::classname(), []);?></span></td>
                    <td><span class="f-chk-tue"><?= $form->field($model, 'tuesdayTo')->widget(TimePicker::classname(), []);?></span></td>
                    <td><span class="f-chk-wed"><?= $form->field($model, 'wednesdayTo')->widget(TimePicker::classname(), []);?></span></td>
                    <td><span class="f-chk-thu"><?= $form->field($model, 'thursdayTo')->widget(TimePicker::classname(), []);?></span></td>
                    <td><span class="f-chk-fri"><?= $form->field($model, 'fridayTo')->widget(TimePicker::classname(), []);?></span></td>
                    <td><span class="f-chk-sat"><?= $form->field($model, 'saturdayTo')->widget(TimePicker::classname(), []);?></span></td>
                    <td><span class="f-chk-sun" ><?= $form->field($model, 'sundayTo')->widget(TimePicker::classname(), []);?></span></td>                    
                </tr>
                <tr>
                    <td><input type="checkbox" name="B2bAddresses[chkMon]" class="chk-closed" id="chk-mon"><b>Monday Closed</b></td>
                    <td><input type="checkbox" name="B2bAddresses[chkTue]" class="chk-closed" id="chk-tue"><b>Tuesday Closed</b></td>
                    <td><input type="checkbox" name="B2bAddresses[chkWed]" class="chk-closed" id="chk-wed"><b>Wednesday Closed</b></td>
                    <td><input type="checkbox" name="B2bAddresses[chkThu]" class="chk-closed" id="chk-thu"><b>Thursday Closed</b></td>
                    <td><input type="checkbox" name="B2bAddresses[chkFri]" class="chk-closed" id="chk-fri"><b>Friday Closed</b></td>
                    <td><input type="checkbox" name="B2bAddresses[chkSat]" class="chk-closed" id="chk-sat"><b>Saturday Closed</b></td>
                    <td><input type="checkbox" name="B2bAddresses[chkSun]" class="chk-closed" id="chk-sun"><b>Sunday Closed</b></td>
                </tr>
            </table>
            
            <?= $form->field($model, 'defaultAddress')->dropDownList(['0' => 'No', '1' => 'Yes']); ?>
           
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){ 
        <?php if(!$model->isNewRecord){            
            if($model->mondayFrom=='closed'){ ?>
                $('#chk-mon').click(); $('.f-chk-mon').hide();
            <?php } ?>
            <?php if($model->tuesdayFrom=='closed') {?>
                $('#chk-tue').click(); $('.f-chk-tue').hide();
            <?php } ?>
            <?php if($model->wednesdayFrom=='closed') {?>
                $('#chk-wed').click(); $('.f-chk-wed').hide();
            <?php } ?>
            <?php if($model->thursdayFrom=='closed') {?>
                $('#chk-thu').click(); $('.f-chk-thu').hide();
            <?php } ?>
            <?php if($model->fridayFrom=='closed') { ?>
                $('#chk-fri').click(); $('.f-chk-fri').hide();
            <?php } ?>
            <?php if($model->saturdayFrom=='closed') { ?>
                $('#chk-sat').click(); $('.f-chk-sat').hide();
            <?php }?>
            <?php if($model->sundayFrom=='closed') { ?>
                $('#chk-sun').click(); $('.f-chk-sun').hide();
        <?php } } ?>
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){ 
        $('.chk-closed').on('ifChanged', function(e){ 
            if($(this).is(':checked')){
                $('.f-'+this.id).hide();
            } else {
                $('.f-'+this.id).show();
            }
        });
    });
</script>