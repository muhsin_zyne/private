<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\B2bAddresses */

$this->title = 'Update B2b Addresses: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'B2b Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="b2b-addresses-update">


    <?= $this->render('_form', [
        'model' => $model,'listData'=>$listData,
    ]) ?>

</div>
