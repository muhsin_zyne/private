<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Search\B2bAddressesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'B2b Addresses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="b2b-addresses-index">
            <p>
                <?= Html::a('Create B2b Addresses', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'ownerId',
                    'storeName',
                    'streetAddress:ntext',
                    //'city',
                    // 'state',
                    // 'country',
                    // 'postCode',
                     'phone',
                    // 'latitude:ntext',
                    // 'longitude:ntext',
                    // 'tradingHours:ntext',
                     'defaultAddress',

                    [
                        'class' => 'yii\grid\ActionColumn' ,
                        'template' => '{download} {view} {update}{delete}',
                        'buttons'=>['label'=>'Actions'], 
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
