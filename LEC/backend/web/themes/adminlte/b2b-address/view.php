<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\Growl;
/* @var $this yii\web\View */
/* @var $model common\models\B2bAddresses */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'B2b Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="box">
    <div class="box-body">
        <div class="b2b-addresses-view">
            <p align="right"> 
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['index',], ['class' => 'btn btn-default']) ?>
            </p>
            
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'ownerId',
                    'storeName',
                    'streetAddress:ntext',
                    'city',
                    'state',
                    'country',
                    'postCode',
                    'phone',
                    'latitude:ntext',
                    'longitude:ntext',
                    'tradingHours:ntext',
                    'defaultAddress',
                ],
            ]) ?>

        </div>
    </div>
</div>
