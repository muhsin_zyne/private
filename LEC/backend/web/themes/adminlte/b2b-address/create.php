<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\B2bAddresses */

$this->title = 'Create B2b Addresses';
$this->params['breadcrumbs'][] = ['label' => 'B2b Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="b2b-addresses-create">
    <?= $this->render('_form', [
        'model' => $model,'listData'=>$listData,
    ]) ?>

</div>
