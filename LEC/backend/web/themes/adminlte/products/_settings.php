<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AttributeSets;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($product, 'attributeSetId')->dropDownList(ArrayHelper::map(AttributeSets::find()->where('id NOT IN (25, 20)')->all(), 'id', 'title')) ?>

    <?= $form->field($product, 'typeId')->dropDownList([
    	'simple'=>'Simple Product',
    	'configurable'=>'Configurable Product',
    //	'bundle'=>'Bundle Product',
        'gift-voucher'=>'Gift Voucher'
    	]) ?>

    <div class="form-group modal-footer">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#products-typeid').change(function(){
        if($(this).val()!="configurable")
            $(".product-settings-form form").wl_Form("set","ajax",false);
        else
            $(".product-settings-form form").wl_Form("set","ajax",true);
    });
})
</script>
