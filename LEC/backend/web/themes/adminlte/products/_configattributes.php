<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AttributeSets;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
$superAttributes = $product->findSuperAttributes();
if(empty($superAttributes)){
    echo "<div class='warning'>This attribute set does not have attributes which we can use for configurable product</div>"; die;
}
?>

<div class="product-settings-form">

    <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
    <?= Html::activeHiddenInput($product, 'typeId') ?>
    <?= Html::activeHiddenInput($product, 'attributeSetId') ?>
    
    <?= Html::checkBoxList('Products[ProductSuperAttributes]', null, ArrayHelper::map($superAttributes, 'id', 'title'))?>
    <div class="form-group">
        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
