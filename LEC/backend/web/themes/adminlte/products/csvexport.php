<?php

use common\models\Products;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\User;

ini_set('max_execution_time', 10000);
ini_set('memory_limit', '5000M');

$output	= "";
$connection = Yii::$app->getDb();

$command = $connection->createCommand("SELECT Products.id,Products.sku FROM Products WHERE Products.visibility='everywhere' AND Products.typeId='simple' and Products.status=1 and Products.createdBy=0 GROUP BY Products.id");

$header=array("name", "sku", "price","cost_price","image url");

foreach ($header as  $value) {
 	$heading	=	$value;
	$output		.= '"'.$heading.'",';
}
$output .="\n";

$result = $command->queryAll();

$f=0;
$count = 1;

foreach($result as $key=>$value){

	$product = Products::find()->where(['id'=>$value['id']])->one();
	$output .= '"'.str_replace('"'," ",$product->name).'","'.$value['sku'].'","'.$product->price.'","'.$product->cost_price.'","https://root.lec.inte.com.au/store/products/images/base/'.$product->baseImage.'"';

	$count = $count+1;
	$output .="\n";
}	

$filename ='LEC Products_'.date("Y-m-d").'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
echo $output;
exit;

?>


<?php
/*use common\models\Products;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\User;

ini_set('max_execution_time', 10000);
ini_set('memory_limit', '5000M');

$output	= "";
$connection = Yii::$app->getDb();

$exclusions = ["'name'","'cost_price'","'price'","'pricetype'","'special_price'","'special_from_date'","'special_to_date'","'description'","'short_description'"];

$command = $connection->createCommand("SELECT Products.id,Products.sku,Products.status,Products.brandId,b.title as brandTitle,supplier.firstname as suppliername FROM Products JOIN Brands b ON Products.brandId=b.id JOIN BrandSuppliers bs ON bs.brandId = b.id JOIN Users supplier ON supplier.id = bs.supplierUid WHERE Products.visibility='everywhere' AND Products.typeId='simple' GROUP BY Products.id");

$attrCommand = $connection->createCommand("SELECT Attributes.title,Attributes.code FROM Attributes JOIN AttributeValues av ON av.attributeId = Attributes.id WHERE Attributes.code not in (".implode(",", $exclusions).") GROUP BY Attributes.title");

$attrText = $attrCommand->queryAll();

//$titleAttr = []; 
$codeAttr = [];

foreach ($attrText as $key => $value) { 
	//$titleAttr[] = $value['code'];
	$codeAttr[] = $value['code'];
}

$heder=array("attribute_set","product_type","category", "name", "sku", "price","thumbnail_image","small_image","base_image","brand","supplier","description");

$fullArray = array_merge($heder,$codeAttr);

foreach ($fullArray as  $value) {
 	$heading	=	$value;
	$output		.= '"'.$heading.'",';
}
$output .="\n";

$result = $command->queryAll();

$f=0;
$count = 1;

foreach($result as $key=>$value){
	if($count <=750){
		if($value['status'] == 0){
			$status = "Disabled";
		}
		else{
			$status = "Enabled";
		}

		$product = Products::find()->where(['id'=>$value['id']])->one();

		$output .= '"'.$product->attributeSet->title.'","'.$product->typeId.'","'.implode(',',ArrayHelper::map($product->categories,'id','categoryId')).'","'.$product->name.'","'.$value['sku'].'","'.$product->price.'","'.$product->thumbnailImage.'","'.$product->smallImage.'","'.$product->baseImage.'","'.$product->brandName.'",';

		if(isset($value['suppliername'])){
			$output .= '"'.trim($value['suppliername']).'",';
		}
		else{
			$output .="".",";
		}
		if($product->description != ""){
			$output .='"'.str_replace(array("\n", "\r", "\r\n", "\n\r",","), " ", $product->description).'",';
		}
		else{
			 $output .="".",";
		}

		foreach ($codeAttr as $attr) { 
			if($product->$attr != ""){
				if(isJson($product->$attr)){
					$output .= '"'.str_replace('","',",",str_replace(array('["','"]'),"",$product->$attr)).'",';
				}
				else{  
					$output .='"'.str_replace(array("\n", "\r", "\r\n", "\n\r"), " ", $product->$attr).'",';
				}	
			}
			else{
				$output .="".",";
			}	
		}

		$count = $count+1;
		$output .="\n";
	}	
}	


function isJson($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}

$filename ='LEC Products_'.date("Y-m-d").'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
echo $output;
exit;

*/

/*use common\models\Products;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\User;

$output	= "";
$connection = Yii::$app->getDb();



$exclusions = ["'country_of_manufacture'","'name'","'cost_price'","'price'","'occasions'","'pricetype'","'special_price'","'special_from_date'","'special_to_date'"];

$attrCommand = $connection->createCommand("SELECT Attributes.title,Attributes.code FROM Attributes JOIN AttributeValues av ON av.attributeId = Attributes.id WHERE Attributes.code not in (".implode(",", $exclusions).") GROUP BY Attributes.title");

// $attrCommand = $connection->createCommand("SELECT a.title,a.code FROM Products JOIN AttributeValues av ON av.productId = Products.id JOIN Attributes a ON a.id=av.attributeId WHERE Products.visibility='everywhere' AND Products.typeId='simple'  AND a.code not in (".implode(",", $exclusions).") GROUP BY a.title");

//Sample pdt export 

//$command = $connection->createCommand("SELECT Products.id,Products.sku,Products.status,Products.brandId,b.title as brandTitle,supplier.firstname as suppliername FROM Products JOIN Brands b ON Products.brandId=b.id JOIN BrandSuppliers bs ON bs.brandId = b.id JOIN Users supplier ON supplier.id = bs.supplierUid WHERE Products.visibility='everywhere' AND Products.createdBy!=6 AND Products.typeId='simple' GROUP BY Products.id LIMIT 200");

//Configurable Pdt Export: visibility everywhere

$command = $connection->createCommand("SELECT Products.id,Products.sku,Products.status,Products.brandId,b.title as brandTitle,supplier.firstname as suppliername FROM Products JOIN Brands b ON Products.brandId=b.id JOIN BrandSuppliers bs ON bs.brandId = b.id JOIN Users supplier ON supplier.id = bs.supplierUid WHERE Products.visibility='everywhere' AND Products.createdBy!=6 AND Products.typeId='configurable' GROUP BY Products.id LIMIT 200");

//Configurable Pdt Export: not-visible-individually


//$command = $connection->createCommand("SELECT Products.id,Products.sku,Products.status,Products.brandId,b.title as brandTitle,supplier.firstname as suppliername FROM Products JOIN Brands b ON Products.brandId=b.id JOIN BrandSuppliers bs ON bs.brandId = b.id JOIN Users supplier ON supplier.id = bs.supplierUid WHERE Products.visibility='not-visible-individually' AND Products.createdBy!=6 GROUP BY Products.id LIMIT 200");



$attrText = $attrCommand->queryAll();

$titleAttr = []; 
$codeAttr = [];
foreach ($attrText as $key => $value) { 
	$titleAttr[] = $value['code'];
	$codeAttr[] = $value['code'];
}

$heder=array("attribute_set","product_type","category", "name", "sku", "price","thumbnail_image","small_image","base_image","brand","supplier","configurable_attributes","parent","sub_products");

$fullArray = array_merge($heder,$titleAttr);

foreach ($fullArray as  $value) {
 	$heading	=	$value;
	$output		.= '"'.$heading.'",';
}
$output .="\n";

$result = $command->queryAll();

$f=0;
$count = 1;

foreach($result as $key=>$value){
	if($count <=200){
		if($value['status'] == 0){
			$status = "Disabled";
		}
		else{
			$status = "Enabled";
		}

		$product = Products::find()->where(['id'=>$value['id']])->one();

		$output .= '"'.$product->attributeSet->title.'","'.$product->typeId.'","'.implode(',',ArrayHelper::map($product->categories,'id','categoryId')).'","'.$product->name.'","'.$value['sku'].'","'.$product->price.'","'.$product->thumbnailImage.'","'.$product->smallImage.'","'.$product->baseImage.'","'.$product->brandName.'",';

		if(isset($value['suppliername'])){
			$output .= '"'.trim($value['suppliername']).'",';
		}
		else{
			$output .="".",";
		}

		if($product->superAttributes){ 
			$output .= '"'.implode(",", ArrayHelper::map($product->superAttributes, 'id', 'attr.code')).'",';
		}
		else{
			$output .="".",";
		}

		if($product->parent){ 
			$output .= '"'.$product->parent->sku.'",';
		}
		else{
			$output .="".",";
		}

		if($product->linkedProducts){ 

			$childProducts = $product->linkedProducts;
			$childPdts = [];
			foreach ($childProducts as $value) {
				$pdt = Products::find()->where(['id'=>$value['productId']])->one();
				$childPdts[] = $pdt->sku;
			}

			$output .= '"'.implode(',',$childPdts).'",';
		}
		else{
			$output .="".",";
		}
		
		foreach ($codeAttr as $attr) { 
			if($product->$attr != ""){
				$output .='"'.$product->$attr.'",';
			}
			else{
				$output .="".",";
			}	
		}

		$count = $count+1;

		$output .="\n";
		
	}	
}	

$filename ='Products_'.date("Y-m-d").'.csv';
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
echo $output;
exit;*/

?>
