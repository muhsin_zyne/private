<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = 'Create Products';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">

    <?php $cats =''; ?>

	<?= $this->render('_form', compact('product', 'groups','cats' , 'availableChildProducts','dataProvider','searchModel','stores','storeProducts','productImages','product_meta','categoryIds')) ?>

</div>
