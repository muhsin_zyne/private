<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\Helper;
use \kartik\file\FileInput;
use common\models\Categories;
use common\models\ProductCategories;
use common\models\Attributes;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $product app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>


<?php  die('image.php');
$form = ActiveForm::begin([
    'options'=>['enctype'=>'multipart/form-data'] // important
]);
//echo $form->field($product, 'filename');

?>

						    <?php	echo '<label class="control-label">Add Attachments</label>';
								echo FileInput::widget([
							    'model' => $product,
							    'attribute' => 'images',
							    'options' => ['multiple' => true],
							    'pluginOptions' => [
						        'uploadUrl' => Url::to(['/products/test']),
						        'uploadExtraData' => [
						            'album_id' => 20,
						            'cat_id' => 'Nature'
						        ],
						        'maxFileCount' => 10
						    ]
							]); ?>
							
							
	
<?php
		echo Html::submitButton($product->isNewRecord ? 'Upload' : 'Update', [
    'class'=>$product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
);
ActiveForm::end();

?>