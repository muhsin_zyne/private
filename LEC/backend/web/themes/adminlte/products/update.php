<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $product app\models\Products */

$this->title = 'Update Products: ' . ' ' . $product->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $product->name, 'url' => ['view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="text-center"><img src="/images/loading.gif" id="loading-img" style="display:none"/ ></div>

<div class="products-update">

    <?php   
		$selectedCategories = implode(",", array_keys($categoryIds));
    ?>

    <?= $this->render('_form', compact('product', 'groups', 'categoryIds', 'selectedCategories' , 'availableChildProducts','stores','storeProducts','productImages','thumbImageUrl','product_meta')) ?>

</div>
