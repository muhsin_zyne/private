<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Products;
use common\models\User;
use yii\helpers\ArrayHelper;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
$products = new Products;
$user = User::findOne(Yii::$app->user->id);

$class = ($user->roleId == 3) ? "update-specialclass" : " ";

?>

<!-- <div class="row">
    <div class="col-sm-12">
        <div class="btn-right-sm">
            <?php //Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['site/export'], Yii::$app->request->queryParams)), ['class'=>'buttons-update btn btn-success enabled']); ?>
        </div>
    </div>
</div>  -->

<div class="text-center"><img src="/images/loading.gif" id="loading-img" style="display:none"/ ></div>
    <div class="box">
        <div class="box-body <?=$class?>">
            <div class="products-index">
                <div class="error-msg"> </div>
      
                    <p class="create_pdt">
                        <?= Html::a('<i class="fa fa-plus"></i>Create Product', ['create'], ['class' => 'btn btn-success ajax-update create_button', 'data-title' => 'Create Product Settings', 'data-parameters' => '{"noFlash":true}', 'data-ajax-loaded'=>'$(".product-settings-form form").wl_Form("set","ajax",false);','data-complete' =>  'myTitle = "Select Configurable Attributes";
                            $dialog.html(jqXHR.responseText);
                            $dialog.dialog({
                                width: myWidth,
                                title: myTitle,
                                resizable: false,
                                modal: true,
                                close: function(event, id){
                                    $(this).dialog("destroy");
                                }
                            }).dialog("open");'
                        ]); ?>
                    </p>
                    <?php \yii\widgets\Pjax::begin(array('id' => 'attributes')); ?>
                    <?php if($user->roleId == "3") { ?>
                        <p class="submit_enabled">
                            <?= Html::button('<i class="fa fa-save"></i> Update',['class'=>'buttons-update btn btn-success enabled']); ?>
                        </p>
                    <?php } ?>
                    <?= \app\components\AdminGridView::widget([
                        'id' => 'products',
                        'filterModel' => $searchModel, 
                        'dataProvider' => $dataProvider,
                        'columns' => $user->roleId != "3" ? $products->getColumns() : array_merge($products->getColumns(),[
                            [

                                'label' => 'Enabled',
                                'format' => 'raw',
                                'value' => function ($model) use($enabledProducts){ 
                                    return '<input type="checkbox" class="enabled_checkbox" value="'.$model->id.'" name="StoreProducts[enabled]" '.(in_array($model->id, ArrayHelper::map($enabledProducts, 'id', 'productId')) ? "checked" : "not").'  >';

                                },
                            ],
                            [

                                /*'label' => 'Featured',
                                'format' => 'raw',
                                'value' => function ($model) use($featured){ 
                                    return Html::checkbox("StoreProducts[featured]",in_array($model->id, $featured) ? true : false, $options = ['value'=>$model->id,'class'=>"featured_checkbox",'uncheck'=>$model->id]);

                                },*/

                                'label' => 'Featured',
                                'format' => 'raw',
                                'filter' => Html::checkbox("ProductsSearch[featured]",isset($_GET['ProductsSearch']['featured']) ? ($_GET['ProductsSearch']['featured'] ==1) ?  true : false : false , $options = ['id'=>'filter_check','class'=>"filter_checkbox",'uncheck'=>"0"]),
                                'value'=> function ($model) use($featured){ 
                                    return Html::checkbox("StoreProducts[featured]",in_array($model->id, $featured) ? true : false, $options = ['value'=>$model->id,'class'=>"featured_checkbox",'uncheck'=>$model->id]);
                                },    

                            ],
                            [
                            	'label' => 'Star Rating',
                            	'format'=>'raw',
                            	'value'=>function($model){
                            		return StarRating::widget([ 
                            			'name' => 'rating_2',
										'value' => $model->rating,
										'pluginOptions' => [
											'readonly' => true,
											'showClear' => false,
											'showCaption' => false,
										],
                            		]);
                            	},
                            ],
                        ])
                    ]); ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                    <div class="test_div"></div>
            </div>
        </div>
    </div>
<script type="text/javascript">

$(document).ready(function() {
    var enabledProducts = [];
    var featuredProducts = [];
    
    $('.enabled_checkbox:checked').each(function(){
        enabledProducts.push(this.value);
    });

    $('.featured_checkbox:checked').each(function(){
        featuredProducts.push(this.value);
    });

    $('body').on('beforeFilter', '#attributes', function(e){
        if($('#filter_check').parent('.icheckbox_minimal').attr("aria-checked") == "false"){
            $('#products form[data-pjax] input[type="hidden"][name="ProductsSearch[featured]"]').remove();
        }
    });

    $('body').on('ifClicked', '#filter_check', function(e){
        setTimeout(function(){$('input[name="ProductsSearch[id]"]').change();}, 500);
    });

    $('#attributes').on('pjax:end', function(){ 
        enabledProducts = []; //test
        featuredProducts = [];
        $('.enabled_checkbox:checked').each(function(){ //console.log(this.value);
            enabledProducts.push(this.value);
        });
        $('.featured_checkbox:checked').each(function(){
            featuredProducts.push(this.value);
        });
    });

    $("body").on("click", ".enabled", function(e){
        $('#loading-img').show();
        var currentEnabledPdts = [];
        var currentFeaturedPdts = [];
        $('.enabled_checkbox:checked').each(function(){
            currentEnabledPdts.push(this.value);
        });

        $('.featured_checkbox:checked').each(function(){
            currentFeaturedPdts.push(this.value);
        });

        $.ajax({

            url:'<?=\yii\helpers\Url::to(["products/enable"])?>',
            type: 'POST',
            data: {currentEnabledPdts : currentEnabledPdts, enabledProducts : enabledProducts, currentFeaturedPdts : currentFeaturedPdts, featuredProducts : featuredProducts},
            success: function(data)
            {
                  $('#loading-img').hide();
                    alert('Saved Successfully!');

                    /*console.log(data);
                    $(".error-msg").append('<div class="alert alert-success"><i class="icon fa fa-tick"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Product Disabled</div>');*/
            }

        });
    }); 

    /*setTimeout(function(){
        $('.filter_checkbox').iCheck('destroy');

    }, 2000);	*/

    
});	

</script>

<script type="text/javascript">

$(document).on("pjax:complete", function(event) { 
    $('.products-index').iCheck({checkboxClass: 'icheckbox_minimal',radioClass: 'iradio_minimal'});
});    

/* $("input[name='ProductsSearch[brandName]']").on("keyup",function(){
        var keyword = this.value;
        var inputbox = this;
        $.ajax({
            url:"<?php echo Yii::$app->urlManager->createUrl(['brands/list'])?>",
            type: 'GET',
            data: {keyword: keyword}, 
            success: function(data)
                { 
                    $(".brands-list").remove();
                    if(jQuery.trim($(this).length > 0)) {
                        $.each($.parseJSON(data), function(key, item) {
                            $(inputbox).parent().append('<div class="brands-list" style="background-color:#fff;min-height:10px;color:#000;border-bottom:#ccc"><div class="brandtext"><a href="#" style="cursor: pointer;">'+item['title']+'</a></div></div>');
                        });    
                    }
                }    
        });            
    });

    $(document).on('click', '.brandtext', function(){ 
        $("input[name='ProductsSearch[brandName]']").val($(this).text());
    });*/




</script>    
        
