<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\Helper;
use kartik\file\FileInput;
use yii\web\UploadedFile;
use yii\grid\GridView;
use kartik\tree\TreeViewInput;
use common\models\Categories;
use common\models\ProductCategories;
use common\models\Attributes;
use common\models\Products;
use common\models\Brands;
use common\models\User;
use common\models\ProductInventory;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\AttributeOptionPrices;
use yii\web\Session;
use backend\components\TreeView;
use wbraganca\dynamicform\DynamicFormWidget;


$products = new Products;
$relatedProducts= common\models\RelatedProducts::find()->join('JOIN','Products as p','p.id = RelatedProducts.relatedProductId')->join('JOIN','StoreProducts as sp','sp.productId = RelatedProducts.relatedProductId')->where(['RelatedProducts.productId'=>$product->id,'RelatedProducts.storeId'=>Yii::$app->params['storeId'],'sp.storeId'=>Yii::$app->params['storeId'],'p.status'=>1,'sp.enabled'=>1])->all();
//print_r($related_products);
//exit;


/* @var $this yii\web\View */
/* @var $product app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<link rel='stylesheet' href='/css/easyTree.css' />
<link rel='stylesheet' href='/css/style_treeview.css' />

<?php 
	$user = User::findOne(Yii::$app->user->id);
	$session = Yii::$app->session;
?>

<?php 	if($user->roleId != "3") { 
		$session = Yii::$app->session;
		if(!$product->isNewRecord) { ?>
			<?= Html::dropDownList('listname', $selected=isset($session['storeId']) ? $session['storeId'] : "", ['0'=>'default value'] + ArrayHelper::map($stores,'id','title'),['class' => 'store-dropdown']); ?>
<?php if($product->createdBy != '0'){ ?>
                                        <span style="color:#b8c7ce;font-size:12px;margin-left:15px;font-style:italic;font-weight: bold;">This product was added by <?=$product->creator->store->title?>. Please ensure that the store is selected.</span>
                        <?php } ?>
<?php } } ?>


<div class="row categories-row">
	<div class="col-xs-12">

		<div class="nav-tabs-custom box">
		    <ul class="nav nav-tabs" id="toggle-mytab">
		    	<?php foreach($groups as $k => $group){ ?>
		        <li <?=($k==0)? 'class="active"' : ''?>>
		        	<a data-toggle="tab" href="#<?=Helper::slugify($group->title)?>"><?=$group->title?></a>
		        </li>
		        <?php } ?>
		        <li><a data-toggle="tab" href="#images">Images</a></li>
		        <!-- <li><a data-toggle="tab" href="#inventory">Inventory</a></li> -->
		        <?php 
		        	if($user->roleId != "3") { ?>
		        		<li><a data-toggle="tab" href="#stores">Stores</a></li>
		        <?php } ?>		
		        <li><a data-toggle="tab" href="#categories">Categories</a></li>
                        <?php if($user->roleId != "1") { ?>
                        <li><a data-toggle="tab" href="#related-products">Related Products</a></li>
                        <?php } ?>
		        <?php if($product->typeId=="configurable"){ ?>
		        	<li><a data-toggle="tab" href="#associated">Associated Products</a></li>
		        <?php }elseif($product->typeId=="bundle"){ ?>
		        	<li><a data-toggle="tab" href="#items">Bundle Items</a></li>
		        <?php } ?>
		    </ul>

		    <?php $form = ActiveForm::begin(['id' => 'products-form', 'options' => ['enctype'=>'multipart/form-data'], 'enableAjaxValidation' => true, 'validationUrl' => (($product->isNewRecord || $product->duplicate)? '?validate' : '#'), 'action' => (isset($_GET['copyFrom'])? Url::to(['products/create']) : "#")]); ?>

		    	<div class="form-group create-subminbutton">
		    		
		    		<?php 
		    		if(!$product->isNewRecord && Yii::$app->user->identity->roleId == "3" && strtotime($storeProducts->disableDate) >= strtotime(date('Y-m-d'))){
		    			echo Html::a('Refresh', Url::to(['products/refreshdisabledate', 'id' => $product->id]), 
		    				[
		    				'class' => 'btn btn-default ', 
		    				'title' => 'Click here to get 30 days more validity',
		    				'data-productid' => $product->id
		    				]
		    				);
		    		}
	    			?>
		    		
		    		<?=(!$product->isNewRecord? Html::a('Discontinue', Url::to(['products/discontinue', 'id' => $product->id]), ['class' => 'btn btn-warning']) : "") ?>
					
					<?=Html::submitButton($product->isNewRecord || $product->duplicate? '<i class="fa fa-save"></i> Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>

				<?php if(!$product->isNewRecord && Yii::$app->user->identity->roleId == "3" && strtotime($storeProducts->disableDate) >= strtotime(date('Y-m-d'))){ ?>
				<div class="text-right " style="padding-right: 10px;">
				  <?php echo 'Auto Disable Date : '.date('d-M-Y',strtotime($storeProducts->disableDate)) ?>
				</div>
				<?php } ?>

		    	<div class="box-body">
				<div class="tab-content">
					<?= Html::hiddenInput('createForm', 'true') ?>
					<?= Html::activeHiddenInput($product, 'typeId') ?>
				    <?= Html::activeHiddenInput($product, 'attributeSetId') ?>
					<?php foreach($groups as $k => $group){ ?>
						<div class="tab-pane<?=($k==0)? ' active' : ''?>" id="<?=Helper::slugify($group->title)?>">
						    <?= $product->renderAttributes($group, $form);
									if($group->title=="General"){ ?>
						    				<!--<span class="place-holder">-->
						    				<?= $form->field($product, 'urlKey')->textInput(array('placeholder' => 'Eg: For mywebsite.com/laptop.html, just enter laptop'))->label('URL Key') ?>
						    				<!--</span>-->
						    			 	
										<?php 	
												if(Yii::$app->user->identity->roleId == "1"){
													echo $form->field($product, 'status')->dropDownList(['1' => 'Enabled', '0' => 'Disabled']);
												}

						    			 		if(Yii::$app->user->identity->roleId == "1" || $product->createdBy == Yii::$app->user->id || $product->isNewRecord){
						    			 			echo $form->field($product, 'visibility')->dropDownList(['everywhere' => 'Everywhere', 'not-visible-individually' => 'Not Visible Individually']); 
						    			 		}  
						    			 		// 	$suppliers = \common\models\Suppliers::find()->all();
						    			 		// elseif(Yii::$app->user->identity->roleId == "3"){
						    			 		//   	$suppliers = Yii::$app->user->identity->store->selectableSuppliers;
						    			 		// }
						    			 	?>
						    			 	<?php //echo $form->field($product, 'supplier')->dropDownList(ArrayHelper::map($suppliers, 'id', 'fullName')) ?>
						    			 	<?php //echo $form->field($product, 'brandId')->dropDownList([], ['prompt' => '']) ?>
									<?php }
										?>
                                                                        <?php if($group->title=="Description"){ ?>
                                                                                <div class="panel panel-default">

                                                                                            

                                                                                            <div class="panel-body des-panel-body">
                                                                                                <?php
                                                                                                DynamicFormWidget::begin([
                                                                                                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                                                                                                    'widgetBody' => '.container-items', // required: css class selector
                                                                                                    'widgetItem' => '.item', // required: css class
                                                                                                    'limit' => 50, // the maximum times, an element can be cloned (default 999)
                                                                                                    'min' => 0, // 0 or 1 (default 1)
                                                                                                    'insertButton' => '.add-item,.add-item1', // css class
                                                                                                    'deleteButton' => '.remove-item', // css class
                                                                                                    'model' => $product_meta[0],
                                                                                                    'formId' => 'products-form',
                                                                                                    'formFields' => [
                                                                                                        'full_name',
                                                                                                        'address_line1',
                                                                                                        'address_line2',
                                                                                                        'city',
                                                                                                        'state',
                                                                                                        'postal_code',
                                                                                                    ],
                                                                                                ]);
                                                                                                ?>
                                                                                            <div class="panel-heading">
                                                                                                <h4><i class="glyphicon glyphicon-sort-by-attributes"></i> Detail Page Attributes</h4>
                                                                                            	<button type="button" class="add-item btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Add</button>
                                                                                            </div>		
                                                                                                
                                                                                                <div class="dynamictable-bg">
                                                                                                <table class="table table-striped table-bordered container-items"><!-- widgetContainer -->
                                                                                                    <tbody>

                                                                                                    <?php foreach ($product_meta as $i => $meta): ?>
                                                                                                        
                                                                                                                
                                                                                                                	
                                                                                                                            <tr class="item">
                                                                                                                            	<?php
																																	// necessary for update action.
																																	if (!$meta->isNewRecord) {
																																		echo Html::activeHiddenInput($meta, "[{$i}]id");
																																	}
																																	?>
                                                                                                                                <td>
                                                                                                                                     <?= $form->field($meta, "[{$i}]key")->textInput(['maxlength' => true,'placeholder'=>'Key'])->label(false); ?>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <?= $form->field($meta, "[{$i}]value")->textInput(['maxlength' => true,'placeholder'=>'Value'])->label(false); ?>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                <button type="button" class="add-item1 btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                                                                                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        
                                                                                                                
                                                                                                <?php endforeach; ?>
                                                                                                </tbody>
                                                                                              </table>
                                                                                              </div> 
                                                                                        <?php DynamicFormWidget::end(); ?>
                                                                                            </div>
                                                                                        </div>
                                                                            <?php } ?>
							<!-- <div class="form-group"> -->
						        <?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						    <!-- </div> -->
					  	</div>
					  	<?php } ?>
					  	<div class="tab-pane" id="images">
					  	<div class="deleted_images"></div>
						  	<div class="input_fields_wrap" id="input_fields">
								<div class="add_prod_images">	
									<?=Html::activeInput('file',$product, 'images',$options = ['class'=>'file','id'=>'file-1','multiple'=>'true','data-overwrite-initial'=>'false','data-min-file-count'=>'1']) ?>
								</div>   
							</div>
							<!-- <div class="form-group"> -->
							  	<?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

							<!-- </div> -->
					  	</div>
						<!--<div class="tab-pane" id="inventory">
						
						    <?=$form->field($product->inventory, 'manageStock')->dropDownList(['1' => 'Yes', '0' => 'No']) ?>
						    <?=$form->field($product->inventory, 'quantity')->textInput() ?>
						    <?=$form->field($product->inventory, 'minQuantity')->textInput() ?>
						    <?=$form->field($product->inventory, 'isInStock')->dropDownList(['1' => 'In Stock', '0' => 'Out of Stock']) ?>

							 <div class="form-group">
						        <?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						    </div> 
					  	</div> -->
					  	<?php
					  	$user = User::findOne(Yii::$app->user->id);
            			if($user->roleId != "3") { ?>
					  	<div class="tab-pane" id="stores">
					  		<div class="select_all">
					  			<input type="checkbox" name="selectall" class="selectall_check"> Select All
					  		</div>
						    <?php foreach ($stores as $store) { ?>
						    	<div class="check-container"><input type="checkbox" class="stores_checkbox" name="Products[stores][]" <?php if(!$product->isNewRecord) { if(in_array($store->id, $storeProducts)) {  echo "checked"; } }?> value="<?=$store->id?>"><?=$store->title?></div>
							<?php } ?>
							<!-- <div class="form-group">  -->
						        <?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						    <!-- </div> -->
					  	</div>
					  	<?php } ?>
						<div class="tab-pane" id="categories">
							
							<?php  
								if(!$product->isNewRecord) {  
					      			if(!is_null($categoryIds))  {   ?>
					      			 <input type="hidden"  value='<?=implode(',', array_keys($categoryIds))?>' name="kv-products" class="form-control kv-products hide" id="w0">
					      			 <input type="hidden" class="value_field" value="<?=implode(',', array_keys($categoryIds))?>">
					      	<?php			
					      			foreach ($categoryIds as $categoryId => $storeId) {  
					      					
					      	?>				<script type="text/javascript">
					      						$(document).ready(function() {
													var key = <?=$categoryId; ?>;
													$( ".selectcat" ).each(function() {
														if($(this).val() == key){ 
															$(this).attr('checked','true');
														}
													});
												});
											</script>

					      	<?php			
					      		if($user->roleId == 3){
					      			if($storeId == 0) {
							?>				
					      					
									<script type="text/javascript">
										$(document).ready(function() {
											var key = <?=$categoryId; ?>;
											$( ".kv-node-key" ).each(function() {
												if($(this).text() == key){
													$(this).parent().parent().parent().addClass('kv-disabled');
												}
											});
										});
									</script>


					      	<?php
					      					}
					      				}
					      				}
					      			}
					      		}
							else{	

							?>
								<input type="hidden" value="" name="kv-products" class="form-control kv-products hide">
							<?php } ?>	


							<?=TreeView::widget()?>

						
						<!-- <div class="form-group"> 
						        <?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						</div>--></div>
                                                
                                                <div class="tab-pane" id="related-products">
                                                    <label class="control-label">Add Products</label>
                                                    <div class="search sm-100">
                                                        <label class="kv-heading-container" for="products-size">Search</label>
                                                        <?= Html::textInput('search', '', $options = ['class' => 'product_search form-control', 'style' => '', 'placeholder' => 'Enter SKU or name of a product', 'autocomplete' => 'off']) ?>
                                                        <span>When you type the letter, result will get displyed below.</span><div id="product-load" style="display:none;"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                                                        <div class="product_box">
                                                            <div class="product_list"></div>    
                                                            <div class="form-group" style="margin-top:20px;margin-bottom:30px">
                                                                <?= Html::a('Add to List', 'javascript:;', $options = ['class' => 'btn btn-primary add_pdt']) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="tray_products_table">
                                                        <div class="table-responsive">
                                                            <table border="1" cellspacing="0" cellpadding="0" class="table sm-table">
                                                                <thead>
                                                                    <tr class="headings">
                                                                        <th></th>
                                                                        <th>Name</th>
                                                                        <th>SKU</th>
                                                                        <th>EZ Code</th>
                                                                        <th>Original sell price</th>
                                                <!--                        <th>Original cost price</th>-->
                                                                        <th>Remove</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
//                                                                                                                        print_r($product->relatedProducts);
//                                                                                                                        exit;
                                                                    if (!$product->isNewRecord) {    //var_dump($relatedProducts);die();
                                                                        if (!empty($relatedProducts)) {
                                                                            foreach ($relatedProducts as $related_product) {
                                                                                if(!empty($related_product->relatedProductId)) {    
//var_dump($product['offerSellPrice']);die();
                                                                                ?>
                                                                                <tr data-productId=<?= $related_product['id'] ?>>
                                                                                    <td><input type="checkbox" checked name="Products[rel_product_id][<?= $related_product['relatedProductId'] ?>]" class="selected" value="<?= $related_product['relatedProductId'] ?>"></td>
                                                                                    <td><?= $related_product->product->name ?></td>
                                                                                    <td><?= $related_product->product->sku ?></td>
                                                                                    <td><?= $related_product->product->ezcode ?></td>
                                                                                    <td><?= $related_product->product->price ?></td>
                                                                    

                                                                                    <td><a href="#" class="remove" title="Delete">X</td>
                                                                                </tr>
                                                                            <?php
                                                                            }
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>    
                                                            </table>  
                                                        </div>  
                                                    </div> 

                                                    <div class="deleted_items"></div>
                                                    <br/>
    
                                                </div>
                                                
                                                
					  	<?php if($product->typeId=="configurable"){ ?>
						  	<div class="tab-pane" id="associated">
						    	<div class="col-md-12">
		                            <div class="box box-solid box-primary">
		                                <div class="box-header">
		                                    <h3 class="box-title">Super Product Attributes Configuration</h3>
		                                </div>
		                                <div class="box-body" style="display: block;">
		                                   <ul id="attributes-sortable">
		                                   	<?php 
		                                   		$data = [];
		                                   		$superAttrColumns = [];
		                                   		foreach($_REQUEST['Products']['ProductSuperAttributes'] as $attrid){
		                                   			$attr = Attributes::findOne($attrid); 
		                                   			if(!$attr) {break;}
		                                   			$superAttrColumns[] = [
											        	'contentOptions' => function($model, $key, $index, $column) use($attr, $product){
											        							if($model->{$attr->code}==""){
											        								die($model->id." - ".$attr->code);
											        							}
											        							$optionId = $model->getOptionId($attr->id, $model->{$attr->code});
											        							$priceAttr = [];
											        							if($optionPrice = AttributeOptionPrices::find()->where(['attributeId' => $attr->id, 'productId' => $product->id, 'optionId' => $optionId])->one()){
											        								$priceAttr = ['data-price' => $optionPrice->price, 'data-type' => $optionPrice->type];
											        							} //var_dump($optionPrice); die;
											        	 						return array_merge([
											        	 							'data-optionid' => $optionId,
											        	 							'class' => 'super-attribute',
											        	 							'data-attrid' => $attr->id,
											        	 						], $priceAttr);
											        						},
											        	'attribute' => $attr->code,
											        	'filter' => Html::textInput("ProductsSearch[{$attr->code}]", isset($_GET['ProductsSearch'][$attr->code])? $_GET['ProductsSearch'][$attr->code] : "", ['class' => 'form-control'])
											        ];
		                                   		?>
											    <li data-attrid="<?=$attr->id?>" class="ui-state-default"><div class="attribute-name-container left"><i class="fa fa-arrows"></i><?=$attr->title?></div>
														<ul class="attribute-values">
															<?php
																if(!$product->isNewRecord){
																	$attributeOptionPrices = AttributeOptionPrices::find()->where(['productId' => $product->id, 'attributeId' => $attrid])->orderBy('attributeId')->all();
																	foreach($attributeOptionPrices as $aop){ ?>
																		<li data-optionid="<?=$aop->optionId?>"><div class="option-details">Option: <strong><?=$aop->option->value?></strong></div><div style="display:none" class="option-details">Price:    <input name="Products[ProductSuperAttributes][<?=$aop->attributeId?>][<?=$aop->optionId?>][price]" type="text" value="<?=($aop->price != ""? $aop->price : '0.00')?>"></div><div style="display:none" class="option-details">&nbsp;<select name="Products[ProductSuperAttributes][<?=$aop->attributeId?>][<?=$aop->optionId?>][type]"><option <?=$aop->type == "fixed"? "selected='selected'" : ""?> value="fixed">Fixed</option><option <?=$aop->type == "percentage"? "selected='selected'" : ""?> value="percentage">Percentage</option></select></div></li>
																 <? }
																}
															?>
														</ul>	    		
											    </li>

											   <?php } ?>
											</ul>
		                                </div><!-- /.box-body -->
		                            </div><!-- /.box -->
		                        </div>

								<div class="form-group">
									<?= Html::hiddenInput('Products[ProductLinkages]', "", ['id' => 'product-linkages']);?>
									<?= Html::hiddenInput('Products[ProductSuperAttributes][ids]', json_encode($_REQUEST['Products']['ProductSuperAttributes']));?>
							        <?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
							    </div>
						  	</div>
					  	<?php }elseif($product->typeId == "bundle"){ ?>
					  		<div class="tab-pane" id="items">
					  			<div class="col-md-12">
		                            <div class="box box-solid box-primary">
		                                <div class="box-header">
		                                    <h3 class="box-title">Shipment</h3>
		                                </div>
		                                <div class="box-body" style="display: block;">
		                                   Ship Bundle Items 
		                                   <select name="shippingMethod" class="form-control select-attri" id="shippingMethod" style="width:300px !important">
												<option value="1">Separately</option>
												<option value="0" selected="selected">Together</option>
											</select>
		                                </div><!-- /.box-body -->

		                              

		                            </div><!-- /.box -->
		                        </div>

		                        <div class="bundle_items_list_all" style="display:none">
		                        	<div class="box box-solid box-primary">
		                        		<div class="box-header">
		                                    <h3 class="box-title">Please Select Products to Add</h3>
		                                    <input type="button" value="Add Selected Product(s) to Option" title="Add Selected Product(s) to Option" name="add_selected_items" id="add_selected_items" class="add_selected_items">
		                                </div>
		                            </div>    
								</div>
									<div class="col-md-12">
		                            <div class="box box-solid box-primary box-items">
		                                <div class="box-header">
		                                    <h3 class="box-title">Bundle Items</h3>
		                                    <input type="button" value="Add New Option" name="add_new_option" id="add_new_option" class="add_new_option">
		                                </div>

		                                <?php if(!$product->isNewRecord) { ?>

		                                <div class="deleted_items"></div>

		                                <?php foreach ($product->bundles as $bundle) { ?>

		                                	<div class="bundle_items_all" style="display: block;">
		                                	<div class="bundle_items_options">
		                                		
		                                		<ul id="bundle-items-options_list" class="" data-bundleid="<?=$bundle->id?>">
		                                			<li>DefaultTitle
		                                				<input type="hidden" name="Products[Bundles][<?=$bundle->id?>][bundle_id]" id="bundle_id" value="<?=$bundle->id?>">
		                                				<input type="hidden" name="Products[Bundles][<?=$bundle->id?>][productId]" id="bundle_id" value="<?=$bundle->productId?>">
		                                				<input type="text" value="<?=$bundle->title?>" id="bundle_options_title" name="Products[Bundles][<?=$bundle->id?>][title]" class="input-text required-entrybundle_options_title">
		                                				<input type="button" value="Delete Option" name="delete_items" id="delete_items" class="delete_items">
		                                				
													</li>
		                                			<li>
		                                				<table>
		                                					<thead>
		                                						<tr>
		                                							<th>Input Type</th>
		                                							<th>Is Required</th>
		                                							<th>Position</th>
		                                						</tr>
		                                					</thead>
		                                					<tbody>
		                                						<tr>
		                                						<td>
		                                							<select id="bundle_option_type" class="select-product-option-type" title="" name="Products[Bundles][<?=$bundle->id?>][inputType]">
		                                								<option value="select" <?php if($bundle->inputType == "select"): ?> selected="selected"<?php endif; ?>>Drop-down</option>
		                                								<option value="radio" <?php if($bundle->inputType == "radio"): ?> selected="selected"<?php endif; ?>>Radio Buttons</option>
		                                								<option value="checkbox" <?php if($bundle->inputType == "checkbox"): ?> selected="selected"<?php endif; ?>>Checkbox</option>
		                                								<option value="multi" <?php if($bundle->inputType == "multi"): ?> selected="selected"<?php endif; ?> >Multiple Select</option>
		                                							</select>
		                                						</td>
		                                						<td>
		                                							<select id="bundle_option_required" class="select" title="" name="Products[Bundles][<?=$bundle->id?>][isRequired]">
		                                								<option value="1" <?php if($bundle->isRequired == "1"): ?> selected="selected"<?php endif; ?> >Yes</option>
		                                								<option value="0" <?php if($bundle->isRequired == "0"): ?> selected="selected"<?php endif; ?> >No
		                                								</option>
		                                							</select>
		                                						</td>
		                                						<td>
		                                						   <input class="input-text" type="text" value="<?=$bundle->position?>" name="Products[Bundles][<?=$bundle->id?>][position]">
		                                							<input type="button" value="Add Selection" name="add_selection" id="add_selection" class="add_selection">
		                                						</td>
		                                						</tr>
		                                					</tbody>
		                                				</table>
		                                			</li>
		                                		</ul>



		                                		<div class="option-body">
		                                			<div class="bundle_items_table">
		                                				<table id="<?=$bundle->id?>">
		                                					<thead>
		                                						<tr>
		                                							<th>Name</th>
		                                							<th class="price" style="display:none">Price</th>
		                                							<th class="price_type" style="display:none">Price Type</th>
		                                							<th class="type-price">Default Qty</th>
		                                							<th class="type-uqty qty-box">User Defined Qty</th>
		                                							<th class="type-order">Position</th>
		                                							<th style="width:1px">Default</th>
		                                							<th class="last"> </th>
		                                						</tr>
		                                					</thead>
		                                					<tbody>

		                                					<?php $i=0; ?>

		                                						 <?php foreach ($bundle->bundleitems as $item) { ?>

		                                						 <?php if($item->price != NULL) {   ?>

		                                						 	<script type="text/javascript">

		                                						 	$(document).ready(function(){

		                                						 	$("#products-priceType option[value='0']").attr("selected", null);
		                                						 	$("#products-priceType option[value='1']").attr("selected", "selected");

		                                						 	var pricetype = $('#products-priceType').find(":selected").text();
																	if(pricetype == "fixed") {
					   													
					   													$('.price').css('display','');
       																	$('.bundlePdt_price').css('display','');
       																	$('.price_type').css('display','');
       																	$('.bundlePdt_price_type').css('display','');
					   												}

																	});


		                                						 	</script>

		                                						<?php	} ?>

		                                						 	<tr data-bundleItemid="<?=$item->id?>">
		                                						 		<td class="bundlePdt_name"><?=$item->product->name ?><br/>sku
		                                						 			<input type="hidden" id="bundleItemId" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][bundleItemId]" value="<?=$item->id ?>">
		                                						 			
		                                						 			<input type="hidden" id="bundleId" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][bundleId]" value="<?=$item->bundleId ?>">

		                                						 			
		                                						 			<input type="hidden" id="productId" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][productId]" value="<?=$item->productId ?>">
		                                						 		</td>
		                                						 		
		                                						 		<td class="bundlePdt_price" style="display:none">
		                                						 			<input type="text" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][price]" value="<?=$item->price ?>" id="price">
		                                						 		</td>
		                                						 		
		                                						 		<td class="bundlePdt_price_type" style="display:none">
		                                						 			<select id="priceType" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][priceType]">
		                                						 				<option value="fixed" <?php if($item->priceType == "Fixed"): ?> selected="selected"<?php endif; ?>> Fixed </option>
		                                						 				<option value="percent" <?php if($item->priceType == "Percent"): ?> selected="selected"<?php endif; ?>> Percent </option>
		                                						 			</select>
		                                						 		</td>

		                                						 		<td class="bundlePdt_defaultqty">
		                                						 			<input type="text" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][defaultQty]" value="<?=$item->defaultQty ?>" id="default_qty">
		                                						 		</td>

		                                						 		<td class="bundlePdt_user_definedqty">
		                                						 			<select id="user_defined_qty" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][isUserDefinedQuantity]">
		                                						 				<option value="1" <?php if($item->isUserDefinedQuantity == "1"): ?> selected="selected"<?php endif; ?>> Yes </option>
		                                						 				<option value="0" <?php if($item->isUserDefinedQuantity == "0"): ?> selected="selected"<?php endif; ?>> No </option>
		                                						 			</select>
		                                						 		</td>

		                                						 		<td class="position">
		                                						 			<input type="text" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][position]" value="<?=$item->position ?>" id="position">
		                                						 		</td>

		                                						 		<td class="default_radio">
		                                						 			<!-- <input type="radio" class="default" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][default]" value="1" id="default" <?php if($item->default == "1"): ?> checked <?php endif; ?>> -->

		                                						 			<input type="radio" class="default" name="Products[Bundles][<?=$bundle->id?>][BundleItems][<?=$item->id?>][default]" value="1" id="default" <?php if($item->default == "1"): ?> checked <?php endif; ?>>
		                                						 		</td>
		                                						 		<td><a href="#" class="deletes">X</a></td>
																	</tr>
																<?php } ?>
		                                					</tbody>
		                                					
		                                				</table>
		                                				
		                                			</div>
		                                		</div>

		                                		<?php if(!$product->isNewRecord) { ?>

		                                		<div class="select_products_add" style="display:none">
		                                			<div class="box box-solid box-primary">
		                                				<div class="box-header">
		                                					<h3 class="box-title">Please Select Products to Add</h3>
		                                					<input type="button" value="Add Selected Product(s) to Option" title="Add Selected Product(s) to Option" name="add_selected_items" id="<?=$bundle->id?>" class="add_selected_items">
		                                				</div>
		                                			</div>
		                                		</div>

		                                		<?php } else { ?>


		                                		<div class="select_products_add" style="display:none">
		                                			<div class="box box-solid box-primary">
		                                				<div class="box-header">
		                                					<h3 class="box-title">Please Select Products to Add</h3>
		                                					<input type="button" value="Add Selected Product(s) to Option" title="Add Selected Product(s) to Option" name="add_selected_items" id="'+bundleCount+'" class="add_selected_items">
		                                				</div>
		                                			</div>
		                                		</div>

		                                		<?php } ?>
		                                		
		                                	</div>
		                                	</div>
		                                <?php } }?>

		                                </div>

		                                <?php if($product->isNewRecord) { ?>

		                               <div class="bundle_items_all" style="display: block;">

		                               </div>	

		                               <?php } ?>

		                           </div><!-- /.box -->
		                        
		                        	<!-- <div class="form-group"> -->
						        <?php // Html::submitButton($product->isNewRecord ? 'Create' : 'Update', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
						        <!-- </div> -->
						    </div>
					  		</div>
					  	<?php } ?>
					  	<div class="next-prev-bg">
					  		<button type="button" id="backbutton" class="btn btn-primary">Back</button>
					  		<button type="button" id="nextbutton" class="btn btn-primary">Next</button>
					  	</div>
      			</div>
			</div>
				<?php ActiveForm::end(); 
					if($product->typeId == "configurable"){ ?>
				<div class="child-products-outer">
				<?php \yii\widgets\Pjax::begin([
		                        	'id' => 'child-products',
		                        	]); ?>

                        <?= GridView::widget([
                        	'id' => 'child-products',
					        'dataProvider' => $availableChildProducts,
					        'filterModel' => new \common\models\search\ProductsSearch,
					        'columns' => array_merge([
					        	[
					        		'header' => false,
					        		'format' => 'raw',
					        		'contentOptions' => ['class' => 'check', 'name' => 'Products[ProductLinkages]'],
					        		'value'=> function ($model){ 
	                                    return Html::checkbox("Products[ProductLinkages]",false, $options = ['value'=>$model->id,'class'=>"linkage",'uncheck'=>$model->id]);
	                                },  
					        		'filter' => Html::checkbox("ProductsSearch[id]", (isset($_GET['ProductsSearch']['id']) && $_GET['ProductsSearch']['id'] != "")? true : false, ['id' => 'filter-linkages', 'value' => implode(",", ArrayHelper::map($product->linkedProducts, 'productId', 'productId')), 'class' => 'form-control', 'uncheck' => ""])
					        	],
						        [
						        	'attribute' => 'sku',
						        	'filter' => Html::textInput('ProductsSearch[sku]', isset($_GET['ProductsSearch']['sku'])? $_GET['ProductsSearch']['sku'] : "", ['class' => 'form-control'])
						        //'price',
					        	]], $superAttrColumns),
					    ]); ?>

				<?php \yii\widgets\Pjax::end(); ?>
			</div>
			<?php } ?>
		</div>
</div>
</div>


<script type="text/javascript">
		//$(".select_all").on("click",".iCheck-helper", function(e){	alert('hai');
			//$('.select_all .iCheck-helper').click(function(){ alert('hai');
			/*if(this.checked){ alert('hai');
				$('.stores_checkbox').attr('checked',true);
				$('.icheckbox_minimal').addClass('checked'); 
			}
			else{
				$('#stores .stores_checkbox').attr('checked',false);
				$('.icheckbox_minimal').removeClass('checked'); 
			}	*/
	// $('.icheckbox_minimal .iCheck-helper').live('click',function(){ alert('hai') });	
	

	
</script>	

<script type="text/javascript">
$(document).ready(function(){
	$('#child-products').hide();
	$('ul.nav-tabs li').click(function(){
		if($(this).find('a[href="#associated"]').length > 0){ 
			$('#child-products').show(); 
		}else{ 
			$('#child-products').hide(); 
		}  
	});
	$('body').on('beforeFilter', '#child-products', function(e){
		if($('#filter-linkages').parent('.icheckbox_minimal').attr("aria-checked") == "false"){
			$('#child-products form[data-pjax] input[type="hidden"][name="ProductsSearch[id]"]').remove();
		}
	})
	$('body').on('ifClicked', '#filter-linkages', function(e){
		setTimeout(function(){$('input[name="ProductsSearch[sku]"]').change();}, 500);
	});
	// $('body').on('ifChecked', '#filter-linkages', function(e){
	// 	$('.selected-linkages').val($(this).val());
	// 	$('input[name="ProductsSearch[sku]"]').change();
	// });
	// $('body').on('ifChecked', '#filter-linkages', function(e){
	// 	$('.selected-linkages').val($(this).val());
	// 	$('input[name="ProductsSearch[sku]"]').change();
	// })
	var linkages = Array();
	var selectedChildren = Array();
	var linkageOptions = Array();
	$(document).on("pjax:complete", function(event) { 
		console.log(linkages);
        $("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        $('input[type="checkbox"][name="ProductsSearch[id]"]').val(linkages.join(','));
        $('#child-products input[type=\"checkbox\"]:not(#filter-linkages)').each(function(){
        	if($.inArray($(this).val(), linkages) != -1){
        		if(!$(this).is(':checked'))
        			$(this).parent('.icheckbox_minimal').click();
        	}
        });

        <?php
			if(!$product->isNewRecord){
				foreach($product->linkedProducts as $link){ //var_dump($product->superAttributes); die;
					foreach($product->superAttributes as $att){ ?>
						if(typeof linkageOptions[<?=$link->product->id?>] == "undefined")
							linkageOptions[<?=$link->product->id?>] = Array();
						linkageOptions[<?=$link->product->id?>][<?=$att->attr->id?>] = '<?=$link->product->{$att->attr->code};?>';
			<?php	}
				}
			} 
		?>
		console.log('Linkage Options - ');
		console.log(linkageOptions);
		// Max: Loops through each child-product row and checks if it's selectable with the current available attribute option prices.
	    $('#child-products input[type=\"checkbox\"]:not(#filter-linkages,:checked,:disabled)').each(function(){ 


	    	var thisCheckBox = this;
	    	var disableRow = false;

	    	$.each(linkageOptions, function(value){
	    		$(thisCheckBox).parents('tr').find('.super-attribute').each(function(){
	    			if($(this).text() == value[$(this).attr('data-attrid')]){
	    				disableRow = true;
			    	}else{
			    		disableRow = false;
			    		return false;
			    	}
	    		});

	    		if(disableRow)
	    			return false;
	    	});

	    	if(disableRow){
		    	$(this).attr("disabled", "disabled");
				$(this).parents('tr').find('td').each(function(){
					$(this).css('background', '#f5d6c7');
				})
			}
	    })
    });

	<?php
		if(!$product->isNewRecord){
			foreach($product->linkedProducts as $prod){ ?>
				// MAX: Ticks the child products if it's a configurable product's update
				linkages.push('<?=$prod->productId?>');
				// setTimeout(function(){
				// 	var $checkbox = $('#child-products input[type=\"checkbox\"][value=\"<?=$prod->productId?>\"]:not(.select-on-check-all)');
				// 	//console.log($checkbox);
				// 	$checkbox.parent('.icheckbox_minimal').iCheck('check');
				// 	var $checkedTr = $checkbox.parent().parent().parent();
				// 	$checkedTr.find('.super-attribute').each(function(){
				// 		$('ul#attributes-sortable input[name="Products[ProductSuperAttributes]['+$(this).attr("data-attrid")+']['+$(this).attr("data-optionid")+'][price]"]').val($(this).attr("data-price"));
				// 		$('ul#attributes-sortable select[name="Products[ProductSuperAttributes]['+$(this).attr("data-attrid")+']['+$(this).attr("data-optionid")+'][type]"]').val($(this).attr("data-type"));
				// 	})
				// }, 1000);
			<?php }
		}
	?>

	<?php if($product->typeId=="configurable"){ ?>
		$.pjax.reload('#child-products');
	<?php } ?>	

	// $('.form-group').removeClass("has-error");
	// $('.help-block').remove();
	$('#products-supplier').change(function(){
		$.ajax({
   		    url:'<?=Url::to(["products/getsupplierbrands"])?>',
   		    data: {supplierUid: $(this).val()},
            success: function(data){
            	$('#products-brandid').html('');
                var options = $($.parseJSON(data)).map(function () {
				    //console.log(this);
				    $('#products-brandid').append('<option value="'+this.id+'">'+this.title+'</option');
				});
				<?php if(!$product->isNewRecord){ ?>
					$('#products-brandid').val(<?=$product->brandId?>);
				<?php } ?>
			}
		});
	});
	<?php if(!$product->isNewRecord){ ?>
		$('#products-supplier').val(<?=$product->supplierId?>);
	<?php } ?>
	$('#products-supplier').change();

	$('#attributes-sortable').sortable();
	// $('body').on('ifChanged', '#child-products input[type="checkbox"]:not(.select-on-check-all)', function(){ alert(linkages.join(','));
	// 	$('#product-linkages').attr("value", linkages.join(','));
	// })
    $('body').on('ifChecked', '#child-products input[type="checkbox"]:not(#filter-linkages)', function(){
    	if($.inArray($(this).val(), linkages) == -1){
    		linkages.push($(this).val());
    	}
    	$('input[type="checkbox"][name="ProductsSearch[id]"]').val(linkages.join(','));
    	var checkbox = this;
    	var checkedTr = $(this).parent().parent().parent();
    	var options = Array();

    	$(this).parent().parent().parent().find('.super-attribute').each(function(){  	
    		options.push($(this).attr("data-optionid"));
    		if(typeof linkageOptions[$(this).parents('tr').find('input[type="checkbox"].linkage').val()] == 'undefined')
    			linkageOptions[$(this).parents('tr').find('input[type="checkbox"].linkage').val()] = Array();
    		linkageOptions[$(this).parents('tr').find('input[type="checkbox"].linkage').val()][$(this).attr("data-attrid")] = $(this).text();
    		if($('#attributes-sortable .attribute-values li[data-optionid="'+$(this).attr("data-optionid")+'"]').length<1){
    			$('#attributes-sortable').find('[data-attrid="'+$(this).attr('data-attrid')+'"] .attribute-values').append('<li data-optionid="'+$(this).attr("data-optionid")+'"><div class="option-details">Option: <strong>'+$(this).html()+'</strong></div><div style="display:none;" class="option-details">Price:    <input name="Products[ProductSuperAttributes]['+$(this).attr("data-attrid")+']['+$(this).attr("data-optionid")+'][price]" type="text" value="0.00"></div><div style="display:none;" class="option-details">&nbsp;<select name="Products[ProductSuperAttributes]['+$(this).attr("data-attrid")+']['+$(this).attr("data-optionid")+'][type]"><option value="fixed">Fixed</option><option value="percentage">Percentage</option></select></div></li>');
    		}
    	})
		var disable = false;
		$('#child-products tr').each(function(){
				var tr = this;
				$.each(options, function(index, value){
					if($(tr).find('td[data-optionid="'+value+'"]').length > 0)
						disable = true;
					else{
						disable = false;
						return false;
					}
				})
			if(disable){
				$(this).find('input[type="checkbox"]').not(checkbox).attr("disabled", "disabled");
				$(this).not(checkedTr).find('td').css('background', '#f5d6c7');
			}
		})
		$('#product-linkages').attr("value", linkages.join(','));
    })
    $('body').on('ifUnchecked', '#child-products input[type="checkbox"]:not(#filter-linkages)', function(){
    	var uncheckedI = linkages.indexOf($(this).val());
		if(uncheckedI != -1)
			linkages.splice(uncheckedI, 1);
		$('input[type="checkbox"][name="ProductsSearch[id]"]').val(linkages.join(','));
    	var options = Array();
    	$(this).parent().parent().parent().find('.super-attribute').each(function(){
    		options.push($(this).attr("data-optionid"));
    		var remove = false;
    		var optionid = $(this).attr("data-optionid");
    		if($('#child-products input[type="checkbox"]:checked').length<1)
    			remove = true;
	    	$('#child-products input[type="checkbox"]:checked').each(function(){
	    		if($(this).parent().parent().parent().find('.super-attribute[data-optionid="'+optionid+'"]').length<1)
	    			remove = true;
	    		else{
	    			remove = false;
	    			return false;
	    		}
	    	})
	    	if(remove)
	    		$('#attributes-sortable').find('[data-attrid="'+$(this).attr('data-attrid')+'"] .attribute-values li[data-optionid="'+ $(this).attr("data-optionid") +'"]').remove();
	    });
	    var enable = false;
    	$('#child-products tr').each(function(){
			var tr = this;
			$.each(options, function(index, value){
				if($(tr).find('td[data-optionid="'+value+'"]').length > 0)
					enable = true;
				else{
					enable = false;
					return false;
				}
			})
			if(enable){
				$(this).find('input[type="checkbox"]').removeAttr("disabled");
				$(this).find('td').css('background', 'none');
			}
		})
		$('#product-linkages').attr("value", linkages.join(','));
    })
})
</script>

<!-- <script src="jquery-1.9.1.min.js"></script> -->

<script type="text/javascript">
	$(document).ready(function() {


		var bundleCount=0;

		var maxvalue = 0;

		var leng = 0;

		var values = Array();

   		$(".add_new_option").click(function(e){ 
   			
   			e.preventDefault();

   			maxvalue = 0;

   			<?php if(!$product->isNewRecord) { ?>

   				cnt = parseInt($('.bundle_items_options ul:last').attr('data-bundleid'));

   				if(isNaN(cnt)) {
					
					var cnt = 900;
				}

   				bundleCount = cnt+1;

   			<?php } ?>	

   			var pricetype = $('#products-pricetype').find(":selected").text(); 

   			//alert(pricetype);

					    if(pricetype == "fixed") { //alert('fixed');

							$(".box-items").append('<div class="bundle_items_all"><div class="bundle_items_options"><ul id="bundle-items-options_list" class="" data-bundleid="'+bundleCount+'"><li>DefaultTitle<input type="text" value="" id="bundle_options_title" name="Products[Bundles]['+bundleCount+'][title]" class="input-text required-entrybundle_options_title"><input type="button" value="Delete Option" name="delete_items" id="delete_items" class="delete_items"></li><li><table><thead><tr><th>Input Type</th><th>Is Required</th><th>Position</th></tr></thead><tbody><tr><td><select id="bundle_option_type" class="select-product-option-type" title="" name="Products[Bundles]['+bundleCount+'][inputType]"><option value="select">Drop-down</option><option value="radio">Radio Buttons</option><option value="checkbox">Checkbox</option><option value="multi">Multiple Select</option></select></td><td><select id="bundle_option_required" class="select" title="" name="Products[Bundles]['+bundleCount+'][isRequired]"><option value="1">Yes</option><option value="0">No</option></select></td><td><input class="input-text" type="text" value="" name="Products[Bundles]['+bundleCount+'][position]"><input type="button" value="Add Selection" name="add_selection" id="add_selection" class="add_selection"></td></tr></tbody></table></li><input type="hidden" name="Products[Bundles]['+bundleCount+'][newBundle]" id="newBundle" value="1"></ul><div class="option-body"><div class="bundle_items_table" style="display:none"><table id="'+bundleCount+'"><thead><tr><th>Name</th><th class="price">Price</th><th class="price_type">Price Type</th><th class="type-price">Default Qty</th><th class="type-uqty qty-box">User Defined Qty</th><th class="type-order">Position</th><th style="width:1px">Default</th><th class="last"> </th></tr></thead><tbody></tbody></table></div></div><div class="select_products_add" style="display:none"><div class="box box-solid box-primary"><div class="box-header"><h3 class="box-title">Please Select Products to Add</h3><input type="button" value="Add Selected Product(s) to Option" title="Add Selected Product(s) to Option" name="add_selected_items" id="'+bundleCount+'" class="add_selected_items"></div></div></div></div></div></div>');
					    }

					    else {

   			$(".box-items").append('<div class="bundle_items_all"><div class="bundle_items_options"><ul id="bundle-items-options_list" class="" data-bundleid="'+bundleCount+'"><li>DefaultTitle<input type="text" value="" id="bundle_options_title" name="Products[Bundles]['+bundleCount+'][title]" class="input-text required-entrybundle_options_title"><input type="button" value="Delete Option" name="delete_items" id="delete_items" class="delete_items"></li><li><table><thead><tr><th>Input Type</th><th>Is Required</th><th>Position</th></tr></thead><tbody><tr><td><select id="bundle_option_type" class="select-product-option-type" title="" name="Products[Bundles]['+bundleCount+'][inputType]"><option value="select">Drop-down</option><option value="radio">Radio Buttons</option><option value="checkbox">Checkbox</option><option value="multi">Multiple Select</option></select></td><td><select id="bundle_option_required" class="select" title="" name="Products[Bundles]['+bundleCount+'][isRequired]"><option value="1">Yes</option><option value="0">No</option></select></td><td><input class="input-text" type="text" value="" name="Products[Bundles]['+bundleCount+'][position]"><input type="button" value="Add Selection" name="add_selection" id="add_selection" class="add_selection"></td></tr></tbody></table></li><input type="hidden" name="Products[Bundles]['+bundleCount+'][newBundle]" id="newBundle" value="1"></ul><div class="option-body"><div class="bundle_items_table" style="display:none"><table id="'+bundleCount+'"><thead><tr><th>Name</th><th class="price" style="display:none">Price</th><th class="price_type" style="display:none">Price Type</th><th class="type-price">Default Qty</th><th class="type-uqty qty-box">User Defined Qty</th><th class="type-order">Position</th><th style="width:1px">Default</th><th class="last"> </th></tr></thead><tbody></tbody></table></div></div><div class="select_products_add" style="display:none"><div class="box box-solid box-primary"><div class="box-header"><h3 class="box-title">Please Select Products to Add</h3><input type="button" value="Add Selected Product(s) to Option" title="Add Selected Product(s) to Option" name="add_selected_items" id="'+bundleCount+'" class="add_selected_items"></div></div></div></div></div></div>');

   		}

			
		bundleCount = bundleCount+1;
		});	

		$(".box-items").on("click",".add_selection", function(e){ 
			var button = this;

			$(this).closest('div').find(".select_products_add").css('display','block');

			// $(this).closest('div').find(".select_products_add").css('display','block');
			$.ajax({
		   		  url:'<?php echo Yii::$app->request->baseUrl;?>/index.php?r=ajax/getbundleitems',	
		          success: function(data)
		                {
		                 	
		                 	$(button).closest('div').append(data);

						}
			});

		});	
	
		$(".box-items").on("click",".delete_items", function(e){  
			<?php if(!$product->isNewRecord) { ?>
				e.preventDefault(); 

				var bundleid = $(this).closest('ul').attr('data-bundleid');

				jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedBundles][]" id="deletedBundles" value="'+bundleid+'"/>');

				$(this).closest('div').parent().remove();

			<?php } else {?>	
	        e.preventDefault(); 
	        $(this).closest('div').remove();
	        <?php } ?>
     	});

     	$(".box-items").on("click",".add_selected_items", function(e){
			
			e.preventDefault();

			var button_id = $(this).attr('id');

		   <?php if(!$product->isNewRecord) { ?>

		   		//console.log($(this).parent().parent().parent().prev('div').find('table tbody tr:last').attr('data-bundleitemid'));

		   		vals = parseInt($(this).parent().parent().parent().prev('div').find('table tbody tr:last').attr('data-bundleitemid'));

		   		//console.log(vals);

		   		if(isNaN(vals)) {
					
					var vals = 900;
				}

		   		maxvalue = vals+1;



		   	<?php } ?>

			$(this).closest('div').parent().parent().css('display','none');

				values = [];
     		
				$('.bundlePdtId:checked').each(function(){
						values.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'), 'sku' : 'SKU'});
     			});	

     			$(this).closest('div').parent().parent().next().remove();

     			//console.log(values);

				for( var i = 0, xlength = values.length; i < xlength; i++)
					{  
					    id = values[i].idss;	
					    name = values[i].names;
					    sku =  values[i].sku;

					    //console.log(id);

					    $('.bundle_items_table').css('display','block');

					    $('.bundle_items_table table[id="'+button_id+'"]').css('display','');

					    var pricetype = $('#products-pricetype').find(":selected").text();
					    
					    //alert(pricetype);

					    if(pricetype == "fixed") {
					    	$('.bundle_items_table table[id="'+button_id+'"]  tbody').append('<tr <?php if(!$product->isNewRecord) { ?>data-bundleitemid='+maxvalue+'<?php } ?>><td class="bundlePdt_name">'+values[i].names+'<br/>'+sku+'<input type="hidden" id="pdtid" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][productId]" value="'+id+'"></td><td class="bundlePdt_price"><input type="text" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][price]" value="0.00" id="price"></td><td class="bundlePdt_price_type"><select id="priceType"  name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][priceType]"><option value="Fixed" selected=""> Fixed </option><option value="Percent"> Percent </option></select></td><td class="bundlePdt_defaultqty"><input type="text" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][defaultQty]" value="1" id="default_qty" ></td><td class="bundlePdt_user_definedqty"><select id="user_defined_qty" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][isUserDefinedQuantity]"><option value="1"> Yes </option><option value="0"> No </option></select></td><td class="position"><input type="text" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][position]" value="" id="position"></td><td class="default_radio"><input type="radio" class="default" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][default]" value="1" id="default"></td><td><a href="#" class="delete">X</a></td><input type="hidden" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][newItem]" id="data-new" value="1"></tr>');
						}

					    else {

					    $('.bundle_items_table table[id="'+button_id+'"]  tbody').append('<tr <?php if(!$product->isNewRecord) { ?>data-bundleitemid='+maxvalue+'<?php } ?>><td class="bundlePdt_name">'+values[i].names+'<br/>'+sku+'<input type="hidden" id="pdtid" data-new="1" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][productId]" value="'+id+'"></td><td class="bundlePdt_price" style="display:none"><input type="text" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][price]" value="0.00" id="price" data-new="1"></td><td class="bundlePdt_price_type" style="display:none"><select id="priceType" data-new="1" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][priceType]"><option value="Fixed" selected=""> Fixed </option><option value="Percent"> Percent </option></select></td><td class="bundlePdt_defaultqty"><input type="text" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][defaultQty]" data-new="1" value="1" id="default_qty"></td><td class="bundlePdt_user_definedqty"><select id="user_defined_qty" data-new="1" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][isUserDefinedQuantity]"><option value="1"> Yes </option><option value="0"> No </option></select></td><td class="position"><input type="text" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][position]" value="" data-new="1" id="position"></td><td class="default_radio"><input type="radio" class="default" data-new="1" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][default]" value="1" id="default"></td><td><a href="#" class="delete">X</a></td><input type="hidden" name="Products[Bundles]['+button_id+'][BundleItems]['+maxvalue+'][newItem]" id="data-new" value="1"></tr>');
					}

						//console.log('maxvalue:'+maxvalue);

						//console.log('i:'+i);

						if ( maxvalue == 0 && i==0 ) {maxvalue = maxvalue+1;}

						if(maxvalue <= i && i != 0 ) {
					    	maxvalue = i+1;	
					    }

					    //if(maxvalue ==1 && i==0) {maxvalue = maxvalue+1;}

					    var len = $('.bundle_items_table table[id="'+button_id+'"]  tbody tr').length;

					     if(maxvalue >= xlength)
					    {
					    	
					    	

					    	if(maxvalue == len && maxvalue != xlength) {maxvalue = len+1;}

					    	else if(maxvalue > len) {maxvalue = maxvalue+1;}

					    	else if(maxvalue ==1 && i==0) {  maxvalue = maxvalue+1; }

					    	else { maxvalue = len; }
					    	
					    }

					    
					   // console.log('after:maxvalue:'+maxvalue);

						//console.log('after:i:'+i);

					}

					$(".bundle_items_table").on("click",".default", function(){   
	  			     	// var tableid = $(this).closest('table').attr('id');
	  			     	
	  			     	// $('.bundle_items_table table[id="'+tableid+'"]  tbody tr td .default').prop('checked', false);
	  			     	// $(this).prop('checked', true);
   			  		});

					
 			});

     	$(".box-items").on("click",".delete", function(e){  
			
			e.preventDefault();

			var tableid = $(this).closest('div table').attr('id');

			$(this).closest('tr').remove();

			var trlen = $('.bundle_items_table table[id="'+tableid+'"]  tbody tr').length;
			
			if(trlen == 0) { $('.bundle_items_table table[id="'+tableid+'"]').css('display', 'none'); }

			values = [];

			$('.bundlePdtId:checked').each(function(){
						values.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name')});
     		});	
	    });

     	$(".box-items").on("click",".deletes", function(e){ 

     		var tableid = $(this).closest('div table').attr('id');

     		var trid = $(this).closest('tr').attr('data-bundleitemid');

     		jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedBundleItems][]" id="deletedBundleItems" value="'+trid+'"/>');

     		$(this).closest('tr').remove();

     		var trlen = $('.bundle_items_table table[id="'+tableid+'"]  tbody tr').length;

			if(trlen == 0) { $('.bundle_items_table table[id="'+tableid+'"]').css('display', 'none'); }
     	
     	});	


});	
</script>

<script type="text/javascript">

	$('#products-pricetype').change(function(){ 
       		var type = $(this).find(":selected").text();
       		if(type == "fixed")
       			{   //alert('fixed');
       				$('.price').css('display','');
       				$('.bundlePdt_price').css('display','');
       				$('.price_type').css('display','');
       				$('.bundlePdt_price_type').css('display','');
       			}
       		if(type == "dynamic")
       			{
       				$('.price').css('display','none');
       				$('.bundlePdt_price').css('display','none');
       				$('.price_type').css('display','none');
       				$('.bundlePdt_price_type').css('display','none');
       			}	
    	});

</script>

<script type="text/javascript">
	function randomString(len, charSet) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
    	var randomPoz = Math.floor(Math.random() * charSet.length);
    	randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}
</script>

<script>

	var randomValue = randomString(10);
	var url = "<?=\yii\helpers\Url::to(['/product-images/upload'])?>";
	var key = 0;

	$("#file-1").fileinput({ 
		uploadUrl: url+'?rand='+randomValue, 
   		allowedFileExtensions : ['jpg', 'png','gif'],
        overwriteInitial: false,
        uploadAsync: true,
        maxFileSize: 20000,
        maxFilesNum: 3,
        'showPreview' : true,
        allowedFileTypes: ['image'],
        slugCallback: function(filename) { 
        	filename = randomValue+'_'+filename;
        	/*$('.tab-content').find('input').iCheck({radioClass: 'iradio_minimal checked'});
        	$('.tab-content').find('input').iCheck({checkBoxClass: 'icheckbox_minimal'});*/

        	//$('.tab-content').iCheck({checkboxClass: 'icheckbox_minimal',radioClass: 'iradio_minimal'});

        	$("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
        		checkboxClass: 'icheckbox_minimal',
        		radioClass: 'iradio_minimal'
    		});

        	<?php if(!$product->isNewRecord) {  ?>
        		len = $('.file-preview-frame').length;
        		if(len != 1 && len != 0){
        			key = $('.base_image:last').val();
        			$('.file-preview-frame').attr('data-fileindex',(parseInt(key)+1));
        		}
        		else{ console.log('len:'+len);
        			$('.file-preview-frame').attr('data-fileindex',len);
        		}	
    		<?php } ?>
            return filename.replace(')', ')').replace(']', '_');
        }
	});


//});
	
</script> 

<script type="text/javascript">
	$(document).ready(function() {
		<?php if($user->roleId == "3" && $product->createdBy != Yii::$app->user->id) { ?>
			$(".products-update :input").prop("disabled", true);
			$("#inventory :input").prop("disabled", false);
			$("#categories :input").prop("disabled", false);
                        $("#meta-information :input").prop("disabled",false);
			$('#related-products input').prop("disabled",false); 
			<?php
			foreach(Yii::$app->controller->b2cAdminEditableAttrs as $attr){ ?>
				$('#products-<?=$attr?>').prop("disabled", false);
			<?php } ?>
			//$("#prices :input").prop("disabled", false);
			$(".btn-primary").prop("disabled", false);
			$('#child-products :input').prop("disabled", false);
		<?php } ?>	
	});	
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.store-dropdown').change(function(){
			if(confirm("Please confirm site switching. All data that hasn't been saved will be lost.")){
				storeId = $(this).val();
				$.ajax({
		   		  url:'<?php echo Yii::$app->request->baseUrl;?>/users/setstore',	
		   		  data: {storeId: storeId}, 
		          success: function(data)
		                {
		                 	location.reload();
						}
				});
			}
			else{
				return false;
			}	
		});	
			//}
	});	
</script>	       

<script type="text/javascript">
	$(document).ready(function() {

		var key = 0;
		<?php 
			if(!$product->isNewRecord) { 
				if(isset($productImages)) { 
					foreach($productImages as $image) {
		?>
		var imageurl = "<?=Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."thumbnail"."/".Products::getImagePathCaseCorrected($image['path'])?>";
		
		$('.file-drop-zone table tbody').append('<tr data-imageid="<?=$image["id"]?>" data-imagename="<?=$image["path"]?>"><td><img src="'+imageurl+'"></td><td><input class="simple" type="text" name="Products[images][old]['+key+'][sort_order]" id="sort_order" class="form-control" value="<?=$image["sortOrder"]?>"></td><td><input class="simple" type="radio" name="Products[images][base_image]" class="base_image" value="'+key+'" <?=(($image["isBase"]=="1") ? "checked" : "")?>></td><td><input class="simple" type="radio" name="Products[images][small_image]" class="small_image" value="'+key+'" <?=(($image["isSmall"]=="1") ? "checked" : "")?>></td><td><input class="simple" type="radio" name="Products[images][thumbnail]" class="thumbnail" value="'+key+'" <?=(($image["isThumbnail"]=="1") ? "checked" : "")?>></td><td><input class="simple" type="checkbox" name="Products[images][old]['+key+'][exclude]" id="exclude" value="<?=(($image["exclude"]=="1") ? "1" : "0")?>" <?=(($image["exclude"]=="1") ? "checked" : "")?>></td><td><a href="#" class="remove">X</a></td><input type="hidden" class="image-old" name="Products[images][old]['+key+'][imageId]" value="<?=$image["id"]?>"></tr>');

		key = key+1;
		
		<?php }  } } ?>	

	});	
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var count = 0;
		var pendingUpload = false;
		$('#products-form').on('beforeSubmit', function() {
		  	$.each( $(".file-preview-frame"), function( key, value ) {
		  		console.log($(this).attr('data-fileindex'));
  				if($(this).attr('data-fileindex') != "-1"){
  					pendingUpload = true;
  					return;
  					// $('.kv-fileinput-upload').click();
  					// return;
  				}
  				else{
  					pendingUpload =false;
  				}
			});
			if(pendingUpload){
				$('a[href="#images"]').click();
				if(confirm('You have one or more images pending to be uploaded. Do you want to continue without saving them?')){
					$.each( $(".file-preview-frame"), function( key, value ) {
  						if($(this).attr('data-fileindex') != "-1"){
  							$(this).remove();
  						}
					});
					return true;
				}else{
					return false;
				}
			}
		});
	});
</script>		


<script type="text/javascript">
	$(document).ready(function() {
		$('div#general .form-group').not('.field-products-name,.field-products-sku,.field-products-supplier,.field-products-brandid').addClass('col-xs-6');
		var key=1;
		$(".file-preview").on("click",".remove", function(e){
			<?php if(!$product->isNewRecord) { ?>
				e.preventDefault();
				var imageid = $(this).closest('tr').attr('data-imageid');
				var imagename = $(this).closest('tr').attr('data-imagename');
				$('.deleted_images').append('<input type="hidden" name="Products[deletedImages]['+key+'][id]" id="deletedImages" value="'+imageid+'"/><input type="hidden" name="Products[deletedImages]['+key+'][name]" id="deletedImages" value="'+imagename+'"/>');	
				$(this).closest('tr').remove();
				key = key+1;
			<?php } else {?>	
				e.preventDefault(); 
				$(this).closest('tr').remove();
			<?php } ?>
		});	
		$('option[checked]').attr("selected", "selected").removeAttr("checked");

		<?php if(Yii::$app->user->identity->roleId == "3" && $product->createdBy == '0' && !$product->isNewRecord){ ?>
			$('.field-products-price').append('<a class="btn btn-primary reset-price" style="margin-bottom: 0px; float: right; top: -33px; position: relative;" href="/products/reset-price?id=<?=$product->id?>">Reset</a>');	
		<?php } ?>

		$('.field-products-cost_price label').append(' (ex GST)');

		$('.field-products-special_price label').append(' (inc GST)<span style="color:#b8c7ce;font-size:12px; margin-left:15px; font-style:italic;">NB: Modifying the special price may affect the off catalogue price.</span>');

		$('.field-products-urlkey label').append(' <span style="color:#b8c7ce;font-size:12px; margin-left:15px; font-style:italic;">Use lowercase. No special characters and spaces.</span>');

		$('.field-products-meta_title label').append('<span style="color:#b8c7ce;font-size:12px; margin-left:15px; font-style:italic;">Note: If you haven\'t added a meta element, the system will automatically take your "Website Name: Product Title" as the meta.</span>');

	        $('.tab-pane#images').prepend('<span style="color:#b8c7ce;font-size:12px; margin-left:15px; font-style:italic; font-weight:bold;">Note: Click the \'Browse\' button to select the images. Click the \'Upload\' button for uploading them to the server.</span>');

		$('select[multiple]').each(function(){
            $(this).multiselect({
                checkboxName: $(this).attr("name")
            });
        });

        $( ".easy-tree > ol.dd-list > li > ol").each( function(){
   	 		$($(this).find($('.selectcat').is(':checked'))).each( function(){
   	 			console.log($this);
   	 		});
		});
	});

	$('.selectall_check').on('ifChanged', function(){
  		if(this.checked){ 
				$('.stores_checkbox').attr('checked',true);
				$('.icheckbox_minimal').addClass('checked'); 
			}
			else{
				$('#stores .stores_checkbox').attr('checked',false);
				$('.icheckbox_minimal').removeClass('checked'); 
			}	
	});
</script>

<script type="text/javascript">

		if($('.value_field').length == 0){
			var ids = [];
		}
		else{
			var ids = [];
			ids = $('.value_field').val().split(",");
		}

	/*$('ol.dd-list > li input').on('ifChanged', function(){ 

			if($(this).prop('checked')){ //alert('checked');
     			ids.push($(this).val());
  				$('.kv-products').val(ids);
  			}

   			else{
   				pos = ids.indexOf($(this).val());
   				ids.splice(pos,1);
   				$('.kv-products').val(ids);
   			}

   			
		});
	});*/

$('.selectcat').change(function(){
	if($(this).prop('checked')){
     	ids.push($(this).val());
  		$('.kv-products').val(ids);
  	}

   	else{
   		pos = ids.indexOf($(this).val());
   		ids.splice(pos,1);
   		$('.kv-products').val(ids);
   	}
});




</script>

<script type="text/javascript">
    $(document).ready(function(){
        addedproducts = [];
        listedproducts = [];

        $(".product_search").on('input',function(e){
        	$('.product_list').empty("");
            $('.product_list').append('<div class="wait-gif" style="width:100%;text-align:center;" ><img src="/images/wait.gif" height="200"></div>');
        });

        $(".product_search").autocomplete( {
        	minLength: 2,
        	delay: 1000,
            source: function( request, response ) { 
                $('.selected:checked').each(function(){
                    if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                        addedproducts.push($(this).attr('value'));
                    }    
                });

                $(".tray_products_table table tbody tr").each(function(){ 
                    if($(this).attr('data-productId')) {
                        if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 
                            listedproducts.push($(this).attr('data-productId')); 
                        }
                    }        
                });

                // New request 300ms after key stroke
                var $this = $(this);
                var $element = $(this.element);
                var previous_request = $element.data( "jqXHR" );
                if( previous_request ) {
                    // a previous request has been made.
                    // though we don't know if it's concluded
                    // we can try and kill it in case it hasn't
                    previous_request.abort();
                }

                 $element.data( "jqXHR",$.ajax({
                    url:"<?php echo Yii::$app->urlManager->createUrl(['gift-registry/getproducts']) ?>",    
                    type: 'GET',
                    data: {keyword: request.term},
                    dataType: 'json',
                    beforeSend: function() {
                        //$("#product-load").show();
                    },
                     success: function(data)
                        {
                            $(".product_item").empty("");
                            $('.product_list').empty("");
                            //$("#product-load").hide();
                            if(jQuery.trim($(".product_search")).length > 0) 
                            {   
                                $.each(data, function(key, item) { 

                                    if ($.inArray(key, listedproducts) == -1) {

                                        if ($.inArray(key, addedproducts) == -1) {    

                                            $('.product_list').append('<div class="product_item"><input type="checkbox" id="'+item['id']+'" name="Products[]" value="'+item['id']+'" class="product_checkbox chk-box" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'" data-costprice="'+item['cost_price']+'"><label for="'+item['id']+'"></label><span>'+item['name']+'</span></div>');
                                        }    
                                    }    
                                }); 
                            }   
                        }
                }));
            },
           // minLength: 3,
            json: true,
        });
        
        $('#w1-tree').height('150');
        
        $('.add_pdt').on('click',function(){ 
        
            $(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) {
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });

            $('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });

            checked_pdts = [];

            $('.product_checkbox:checked').each(function(){
                        checked_pdts.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price'), 'cost_price' : $(this).attr('data-costprice')});
            }); 

            $(".tray_products_table").css('display','block');

            for( var i = 0, xlength = checked_pdts.length; i < xlength; i++)
            { 
                if ($.inArray(checked_pdts[i].idss, listedproducts) == -1) {

                    if ($.inArray(checked_pdts[i].idss, addedproducts) == -1) {

                        $(".tray_products_table table tbody").append('<tr data-productId="'+checked_pdts[i].idss+'"><td><input type="checkbox" name="Products[rel_product_id]['+checked_pdts[i].idss+']" value="'+checked_pdts[i].idss+'" id="av'+checked_pdts[i].idss+'" class="chk-box"><label for="av'+checked_pdts[i].idss+'"></label></td><td>'+checked_pdts[i].names+'</td><td>'+checked_pdts[i].sku+'</td><td>###</td><td>'+checked_pdts[i].price+'</td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products[' + checked_pdts[i].idss + '][newitem]" id="data-new" value="1">');
                    }
                }
            }
        });
    });
    
</script>

<script type="text/javascript">
    $(".tray_products_table").on("click", ".remove", function (e) {

        var trid = $(this).closest('tr').attr('data-productid');
        //console.log(trid); 
        if (trid) {
            jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedItems][]" id="deletedItems" value="' + trid + '"/>');
        }
        $(this).closest('tr').remove();
    });
</script>

<script type="text/javascript">
<?php if ($product->isNewRecord) { ?>
        /*  if(!empty($promotion->products)) {*/
        $(".tray_products_table").css('display', 'none');
        //$(".tray_products_table").css('display','block');

<?php } /* foreach ($promotion->promotionProducts as $promotionProduct)  { ?>

  $(".tray_products_table table tbody").append('<tr data-productId="<?=$promotionProduct->id?>"><td><input type="checkbox" checked name="Products[id][<?=$promotionProduct->product->id?>]" class="selected" value="<?=$promotionProduct->product->id?>"></td><td><?=$promotionProduct->product->name?></td><td><?=$promotionProduct->product->sku?></td><td>###</td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][original_sell_price]" class="cost_price_promotions" value="<?=$promotionProduct->product->price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][original_cost_price]" class="cost_price_promotions" value="<?=$promotionProduct->product->cost_price?>" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][offerSellPrice]" value="<?=$promotionProduct->offerSellPrice?>" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][offerCostPrice]" value="<?=$promotionProduct->offerCostPrice?>" class="cost_price_promotions" autocomplete="off"></td><td><input type="text" name="Products[<?=$promotionProduct->product->id?>][pageId]" class="cost_price_promotions" value="<?=$promotionProduct->pageId?>" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td>');


  }
  }
  } */
?>
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#related-products > td input').on('ifChanged', function(){ 
			//alert('clicked');
			/*if($(this).prop('checked')){ //alert('checked');
	     		ids.push($(this).val());
	  			$('.kv-products').val(ids);
	  		}
			else{
	   			pos = ids.indexOf($(this).val());
	   			ids.splice(pos,1);
	   			$('.kv-products').val(ids);
	   		}*/

	   	});
	});
</script>
