<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use yii\helpers\ArrayHelper;
use common\models\Orders;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">
<div class="invoices-index">

    <?php \yii\widgets\Pjax::begin(['id' => 'invoices-index',]); ?>

    <!--<p>
        <?= Html::a('Create Invoices', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'invoicePlacedDate',
            //'createdDate',
            [
                'label' => 'INVOICE DATE',
                'attribute' => 'createdDate',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::date($model->createdDate);
                },
            ],
            'orderId',
            
            //'orderDate',
            //'orderBillToAddress',
            
            
            [
                'label' => 'Order Date',
                'attribute' => 'orderDate',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::date($model->order->orderDate);
                },
            ],

            [
                'label'=> 'Bill to Address',
                'format' => 'html',
                'attribute' => 'orderBillToAddress',
                'value' => 'order.billingAddressText'
            ],
            
            [
                'label' => 'Status',
                'attribute' => 'status',
                'value' => function($model, $attribute){ 
                    return ucfirst($model->status);
                },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['pending' => 'pending', 'paid' => 'paid', 'cancelled' => 'cancelled',],                   
                    ['class'=>'form-control','prompt' => '']),
            ],
            [
                'label' => 'Amount',
                'attribute' => 'grandTotal',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::money($model->grandTotal);
                },
            ],
            //'amountFormated',
            [
                'label'=> 'View',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('View',['invoices/view','id' => $model->id]);
                }
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php  \yii\widgets\Pjax::end();  ?> 

    <?php
    $this->registerJs("
    $('td').click(function (e) {
        var id = $(this).closest('tr').data('key');
        if(e.target == this)
            location.href = '" . Url::to(['invoices/view']) . "?id=' + id;
        });
    ");
?>

</div>
</div>
</div>
