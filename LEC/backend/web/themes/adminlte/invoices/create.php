<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Invoices */

$this->title = 'New Invoice for Order #'.$model->order->orderId;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-create">


    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider'=>$dataProvider,

    ]) ?>

</div>
