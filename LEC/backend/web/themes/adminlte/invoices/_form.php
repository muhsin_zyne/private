<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\components\Helper;
use yii\widgets\ListView;
use yii\helpers\url;
use yii\grid\GridView;
use common\models\SalesComments;
use common\models\Shipments;
use common\models\OrderItems;
use common\models\Orders;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Invoices */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
  <div class="col-xs-12">
    <div style="float:right; margin-bottom:15px;">
            <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/view' ,'id'=>$model->order->id], ['class' => 'btn btn-default back-btn']) ?>
        </div>
    </div>
</div>
<div class="invoices-form">
<div class="box">
  <div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                     <i class="fa fa-file-text-o"></i><h3 class="box-title">Order # <?= $model->order->orderId ?> (Order confirmation email was sent)</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p>Order Date : <?= $model->order->orderPlacedDate ?></p>
                    <p>Order Status : <u><b><?= ucfirst($model->order->status) ?></b></u></p>
                    <p>Purchased From : <?= $model->order->storeName ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i><h3 class="box-title">Account Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p> Customer Name : <?= ucfirst($model->order->customer->firstname).' '.ucfirst($model->order->customer->lastname) ?></p>
                    <p> Email : <?= $model->order->customer->email ?></p>
                   
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-home"></i><h3 class="box-title">Billing Address</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p><?= $model->order->billingAddressText ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-building-o"></i><h3 class="box-title">Collect From Store</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p><?= $model->order->orderDeliveredAddress ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                  <i class="fa fa-usd"></i><h3 class="box-title">Payment Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                            
                            <?= $model->order->paymentDetails?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                 <i class="fa fa-truck"></i><h3 class="box-title">Shipping & Handling Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>
                    <?php 
                
                    $model->shipping=(isset($model->order->shippingAmount))? $model->order->shippingAmount : 0;
                    //echo $form->field($model, 'shipping')->textInput();
                    if(isset($model->order->shippingMethodId)){ ?>
                        <?= $model->order->shippingMethod->title ?> | Total Shipping Charges: <?= Helper::money($model->order->shippingAmount) ?> <br/>
                    <?php }
                    else
                    {?>
                       N/A | Total Shipping Charges: <?= Helper::money(0) ?> <br/>
                    <?php }  ?>
                   
                   
                    Create Shipment
                    <input type="checkBox" class="enabled_checkbox"  id="invoices-shipment_chk"  name="Invoices[shipment_chk]">

                        <div class="shipping-form" id="shipping-form">
                            <h3>Create Shipment</h3>
                            <div class="inv-ship">  
                                <?php $Carriers=common\models\Carriers::find()->all(); 
                                $listData=ArrayHelper::map($Carriers,'id','title'); ?>  
                                <?= $form->field($model, 'shipment_carrier')->dropDownList($listData, ['prompt'=>'Select Carrier...']);?>
                                <?= $form->field($model, 'shipment_title')->textInput() ?>
                                <?= $form->field($model, 'shipment_trackingNumber')->textInput() ?> 
                                <p>Email Copy of Shipment <input type="checkBox" class="enabled_checkbox" id="invoices-ship_chkmail" name="Invoices[ship_chkmail]" checked></p>
                            </div>
                        </div>  
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-shopping-basket"></i><h3 class="box-title">Items Invoiced</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                 <?= GridView::widget([    
                    'id' => 'order-items',    
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'columns' => [    
                        ['class' => 'yii\grid\SerialColumn'], 
                        //'id',    
                        [    
                            'label'=> 'Item Orderd',    
                            'attribute' => 'id', 
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->giftWrap)  ? 
                                    '<div class="tb-img">'.Html::img(Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage()).'</div>'.
                                        ($model->product->name.'<br/><div class="tb-price">'.Helper::money($model->price).'</div><span class="label label-success">$'.round($model->giftWrapAmount).' Gift Wrapping Included</span>'.($model->superAttributeValuesText!='N/A'?' '.$model->superAttributeValuesText:'')) : 
                                    '<div class="tb-img">'.Html::img(Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage(),['width' => '30px']).'</div><br/>'.$model->product->name.' '.($model->superAttributeValuesText!='N/A'?$model->superAttributeValuesText:''.'<br/><div class="tb-price">'.Helper::money($model->price).'</div>');
                            },
                        ],    
                        [    
                            'label'=> 'SKU',    
                            'attribute' => 'productId',    
                            'value' => 'sku'    
                        ], 
                        
                        [    
                            'label' => 'Price',    
                            'attribute' => 'id',    
                            'format' => 'html',    
                            'value' => function ($model) {    
                                return Helper::money($model->price);    
                            },    
                        ],
                        [    
                            'label' => 'Qty To Fulfill',    
                            'attribute' => 'quantity',    
                            'format' => 'html',    
                            'value' => function ($model) {    
                                return $model->quantity;    
                            },    
                        ],
                        [    
                            'label' => 'Qty Ready For Collection',    
                            'attribute' => 'qtyReadyForDelivery',    
                            'format' => 'raw',  
                            'visible'=>($type!='fc')?true:false,  
                            'value' => function ($model) { 
                                return '<span id="qa_'.$model->id.'" class="qtyavailable"> </span>'.
                                '<input type="hidden" class="" id="oi'.$model->id.'" name="OrderItems['.$model->id.'][qrfd]" value="'.$model->qtyAvailable.'">'; 
                            },    
                        ], 
                        [    
                            'label' => 'Qty Ready For Collection',    
                            'attribute' => 'qtyReadyForDelivery',    
                            'format' => 'raw',  
                            'visible'=>($type=='fc')?true:false,  
                            'value' => function ($model) { 
                                return $model->qtyavailable.
                                '<input type="hidden" class="" id="oi'.$model->id.'" name="OrderItems['.$model->id.'][qrfd]" value="'.$model->qtyAvailable.'">'; 
                            },    
                        ],                          
                        
                       
                    ],

                ]); ?>
                </div><!-- /.box-body -->
                <div class="clearfix"></div>
                <div style="float:right; margin:15px 0;"><input type="button" class="btn btn-success" id="update_qty" value="Update Qty's"></div>
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <table width="100%">
                    <tr>
                        <td align="center">Paid Amount <br/><?= Helper::money($model->order->grandTotal) ?></td>
                    
                        <td align="center">Refund Amount<br/><?= Helper::money($model->order->totalRefunded) ?></td>
                    
                        <td align="center" >Shipping Amount<br/><?= Helper::money($model->order->shippingAmount) ?></span></td>                        

                        <td align="center">Coupon Discount <br/><?= Helper::money($model->order->couponDiscount) ?></td>
                    
                        <td align="center">Order Grand Total<br/><?= Helper::money($model->order->grandTotal) ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                  <i class="fa fa-comments-o"></i><h3 class="box-title">Invoice Comments</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="order-comment-form">
                        <?php 
                        $model->orderId=$model->order->id;
                        //$model->shipping=$model->order->shippingAmount; ?>
                        <?= $form->field($model, 'orderId')->hiddenInput()->label(false)  ; ?>
                        <?= $form->field($model, 'shipping')->hiddenInput()->label(false)  ; ?>
                        <?= $form->field($model, 'comments')->textarea(['rows' => 4]) ?>
                            
                    </div> 
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                  <i class="fa fa-book"></i><h3 class="box-title">Invoice Totals</h3>
                </div><!-- /.box-header -->
                <div class="box-body" >
                    <p>Subtotal    : AU$<span id="subtotal"> </span> </p>
                    <input type="hidden" name="Invoices[subTotal]" id="hsubtotal" value="">
                    <p>Discount Amount : <?= Helper::money($model->order->orderDiscountAmount)?> </p> 
                    <input type="hidden" name="discount" id="ida" value="<?=$model->order->orderDiscountAmount?>"> 
                    <p>Grand Total  : AU$<span id="grandtotal"> </span> </p>
                    <input type="hidden" name="Invoices[grandTotal]" id="hgrandtotal" value="">    
                    <p>Email Copy of Invoice <input type="checkBox" class="enabled_checkbox" id="invoices-chkmail" name="Invoices[chkmail]"></p>
                    <?php //echo $form->field($model, 'chkmail')->checkBox(['label' => '...', 'uncheck' => null, 'selected' => true]); ?>
          
                    <?= Html::submitButton('Submit Invoice', ['class' => 'btn btn-success','id'=>'sub_inv' ]) ?>
                    <?php ActiveForm::end(); ?>
                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div>
    </div>
</div>
</div>
</div>


<script>
$(document).ready(function(){
    // $('#shipping-form').hide();
    // $('#123').click(function(){
    //    if($(this).prop("checked") == true)
    //     {  $('#shipping-form').show();  }
    //     else
    //     {  $('#shipping-form').hide(); } 
    // });
    var sum = 0;
    var shipping_amound=<?= $model->shipping ?>;
    $(".hrt").each(function() {
        var id=$(this).attr('id'); 
        var oi_id=id.slice(2);
        var tqty=$('#'+oi_id).val(); 
        var st=$('#'+oi_id).val();
        var price=$('#p'+oi_id).val();
        var disc_amt=$('#da'+oi_id).val();
        var gw_amt=$('#gw'+oi_id).val();
        //alert(disc_amt);
        //var shipping_amound=$('#shipping_amound').val();
        $('#stu'+id).val(price*tqty);
        var value = $(this).val();
       
        //alert(id);
        $('#stu'+id).text(price*tqty);
        $('#rtu'+id).text(price*tqty-disc_amt);
        //alert(value);
        if(!isNaN(price) && price.length != 0) {
            sum += parseFloat(price*tqty);
        }
    });
    var total_discamt=$('#ida').val();
    var g_total=sum+shipping_amound-total_discamt;
    //alert(g_total);
    $('#subtotal').text(sum);
    $('#hsubtotal').val(sum);
    $('#grandtotal').text(g_total);
    $('#hgrandtotal').val(g_total);

    $("#update_qty").prop('disabled', true);   
    $(".qti").change(function(){               // --------- qnty onchange 
        $("#update_qty").prop('disabled', false);
        $("#sub_inv").prop('disabled', true);
        var qty = $( this ).val();
        var oi_id=$(this).attr('id');
        var hqty=$('#hqti'+oi_id).val();
        var org_qty = hqty.split(',');
        if(org_qty['1']<qty)
        {
            alert('Invalid Qty to Invoiced!');
            $(this).val(org_qty['1']);
            qty=org_qty['1'];
        }
        var price=$('#p'+oi_id).val();
        var disamound=$('#da'+oi_id).val();
        var subtotal=qty*price;
        
        $('#hqti'+oi_id).val(oi_id+','+qty);
        $('#st'+oi_id).val(subtotal);
        $('#rt'+oi_id).val(subtotal);
        $('#rturt'+oi_id).text(subtotal);
        $('#sturt'+oi_id).text(subtotal);
        //alert($('#rturt'+oi_id).val());
    });  
    $("#update_qty").click(function(){
        $("#update_qty").prop('disabled', true);
        $("#sub_inv").prop('disabled', false);
        var sum = 0;
        $(".hrt").each(function() {
            var value = $(this).val();
            var id=$(this).attr('id');
            var st=$('#'+id).val();
            $('#stu'+id).text(st);
            $('#rtu'+id).text(st);
        if(!isNaN(value) && value.length != 0) {
            sum += parseFloat(value+shipping_amound);
        }
    });
    $('#subtotal').text(sum);
    $('#hsubtotal').val(sum);
    $('#grandtotal').text(sum);
    $('#hgrandtotal').val(sum);

    });
});

</script>
<script type="text/javascript">  // shipment form  
    $(document).ready(function(){
        $('#shipping-form').hide();
        $('#invoices-shipment_chk').change(function(){
            if (this.checked) { 
                $('#shipping-form').show();
            }
            else {
                $('#shipping-form').hide();
            }
        });
        $('#invoices-shipment_carrier').on('change',function(){
            $.ajax({
                type: "GET",
                url: "<?=Yii::$app->urlManager->createUrl(['shipments/carrier'])?>",
                data: {id: $(this).val()},
                dataType: "html", 
                success: function (data) {
                    //alert(data);  
                    $('#invoices-shipment_trackingnumber').val(data);              
                }                
            });
        });    
    });
</script>