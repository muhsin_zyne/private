<?php



use yii\helpers\Html;

use yii\widgets\DetailView;

use yii\grid\GridView;

use frontend\components\Helper;

use yii\widgets\ActiveForm;

use yii\widgets\ListView;

use yii\helpers\url;

use common\models\SalesComments;

if($model->notify==1)

{

    $message='(the invoice email was sent)';

}

else

{

    $message='(the invoice email was not sent)';

}

$this->title = 'Invoice #'.$model->invoiceId.'|'.$model->status.'|'.$model->invoicePlacedDate.' '.$message;

?>

<div class="row">
  <div class="col-md-12">
  <div style="float:right; margin-bottom:15px;">
    <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/view' ,'id'=>$model->order->id], ['class' => 'btn btn-default ']) ?>
    <?= Html::a('<i class="fa fa-envelope"></i> Resend Invoice', ['sendmail','id'=>$model->id],['class' => 'btn btn-primary hold',
    'data' => ['confirm' => 'Are you sure you want to send invoice email to customer?','method' => 'post',]]) ?>
  </div>
  </div>
</div>

<div class="box box-default">
<div class="box-body">
<div class="invoices-view">
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
          <h3 class="box-title">Order #
            <?= $model->order->orderId ?>
            (Order confirmation email was sent)</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <p>Order Date :
            <?= $model->order->orderPlacedDate ?>
          </p>
          <p>Order Status :
            <b><u><?= ucfirst($model->order->status) ?></u></b>
          </p>
          <p>Purchased From :
            <?= $model->order->storeName ?>
          </p>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-user"></i>
          <h3 class="box-title">Account Information</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <p> Customer Name :
            <?= ucfirst($model->order->customer->firstname).' '.ucfirst($model->order->customer->lastname) ?>
          </p>
          <p> Email :
            <?= $model->order->customer->email ?>
          </p>
          <p> Phone :
              <?= $model->order->billing_phone ?>
          </p>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-home"></i>
          <h3 class="box-title">Billing Address</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <p>
            <?= $model->order->billingAddressText ?>
          </p>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
          <h3 class="box-title">Collect From Store</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <p>
            <?= $model->order->orderDeliveredAddress ?>
          </p>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-usd"></i>
          <h3 class="box-title">Payment Information</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <?= $model->order->paymentDetails?>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-truck"></i>
          <h3 class="box-title">Shipping & Handling Information</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <?php 

                

                    $model->shipping=(isset($model->order->shippingAmount))? $model->order->shippingAmount : 0;

                    //echo $form->field($model, 'shipping')->textInput();

                  if(isset($model->order->shippingMethodId)){ ?>
          <p>  Shipment Type :          
          <?= $model->order->shippingMethod->title ?>
          </p>
          <p> Total Shipping Charges:
          <?= Helper::money($model->order->shippingAmount) ?>
          </p>
          <?php }

                    else

                    {?>
          <p>  Shipment Type :  N/A</p> 
          <p> Total Shipping Charges:
          <?= Helper::money(0) ?>
          </p>
          <?php }  ?>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
          <h3 class="box-title">Items Invoiced</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          <?= GridView::widget([

                    'id' => 'order-items',

                    'dataProvider' => $dataProvider,

                    //'filterModel' => $searchModel,

                    'columns' => [

                        ['class' => 'yii\grid\SerialColumn'],

                        

                        //'id',
                        [
                            'label'=> 'Item Orderd',
                            'attribute' => 'id', 
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->orderItem->giftWrap)?($model->orderItem->product->name.' <br/><span class="label label-success">$'.round($model->orderItem->giftWrapAmount).' Gift Wrapping Included</span>'.($model->orderItem->superAttributeValuesText!='N/A'?' '.$model->orderItem->superAttributeValuesText:'')):$model->orderItem->product->name.' '.($model->orderItem->superAttributeValuesText!='N/A'?$model->orderItem->superAttributeValuesText:'');
                            },
                        ],

                        /*[

                            'label'=> 'Product Name',

                            'attribute' => 'orderId',

                            'value' => 'orderItem.product.name'

                        ],*/

                        [

                            'label'=> 'SKU',

                            'attribute' => 'productId',

                            'value' => 'orderItem.sku'

                        ],

                        [

                            'label' => 'Price',

                            'attribute' => 'id',

                            'format' => 'html',

                            'value' => function ($model) {

                                return Helper::money($model->orderItem->price);

                            },

                        ],
                        [

                            'label'=> 'Qty Invoiced',

                            'attribute' => 'quantityInvoiced',

                            'value' => 'quantityInvoiced'

                        ],

                        //'quantityInvoiced',

                        [

                            'label' => 'Sub Total',

                            'attribute' => 'id',

                            'format' => 'html',

                            'value' => function ($model) {

                                return Helper::money($model->orderItem->price * $model->quantityInvoiced);

                            },

                        ],

                        [
                            'label' => 'Gift Wrapping',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {

                                return Helper::money($model->orderItem->giftWrapAmount* $model->orderItem->quantity);
                            },
                        ],

                        [

                            'label' => 'Discount Amount',

                            'attribute' => 'id',

                            'format' => 'html',

                            'value' => function ($model) {

                                return Helper::money($model->orderItem->discount);

                            },

                        ],

                        [

                            'label' => 'Row Total',

                            'attribute' => 'id',

                            'format' => 'html',

                            'value' => function ($model) {

                                return Helper::money(($model->orderItem->price* $model->quantityInvoiced)-$model->orderItem->discount+($model->orderItem->giftWrapAmount*$model->quantityInvoiced));

                            },

                        ],

                    ],



                ]); ?>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-comments-o"></i>
          <h3 class="box-title">Invoice Comments History</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body">
          
          <div class="order-comment-form">
            <?php $form = ActiveForm::begin(['action' =>  Url::to(['sales-comments/create'])]); 

                            $model_sc= new SalesComments(); 

                            $model_sc->typeId=$model->id; $model_sc->type='invoice';$model_sc->orderId=$model->order->id; ?>
            <?= $form->field($model_sc, 'type')->hiddenInput()->label(false)  ; ?>
            <?= $form->field($model_sc, 'typeId')->hiddenInput()->label(false)  ; ?>
            <?= $form->field($model_sc, 'orderId')->hiddenInput()->label(false)  ; ?>
            <?= $form->field($model_sc, 'comment')->textarea(['rows' => 4])->label('Add Invoice Comments'); ?> 
            <?= $form->field($model_sc, 'notify')->checkbox(); ?>
            <div class="form-group">
              <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <?= ListView::widget([

                            'dataProvider' => $dataProvider_salescomments,

                            //'filterModel' => $searchModel_ordercomment,

                            'itemView' => '_listview',
                            'summary'=>'',

                        ]); ?>
          </div>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
    <div class="col-md-6">
      <div class="box box-default">
        <div class="box-header with-border"><i class="fa fa-book"></i>
          <h3 class="box-title">Invoice Totals</h3>
        </div>
        <!-- /.box-header -->
        
        <div class="box-body" >
          <p>Subtotal    :
            <?= Helper::money($model->subTotal) ?>
          </p>
          <p>Discount Amount  :
            <?= $model->order->formatedOrderDiscountAmount?>
          </p>
          <p>Grand Total    :
            <?= Helper::money($model->grandTotal) ?>
          </p>
        </div>
        <!-- /.box-body --> 
        
      </div>
      <!-- /.box --> 
      
    </div>
  </div>
</div>
</div>
</div>

