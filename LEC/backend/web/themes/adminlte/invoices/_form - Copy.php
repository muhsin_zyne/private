<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Invoices */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="invoices-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'orderId')->textInput() ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'subTotal')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'shipping')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'grandTotal')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'createdDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
