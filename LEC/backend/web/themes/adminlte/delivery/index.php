<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\components\Helper;
use common\models\Orders;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DeliveriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="deliveries-index"> 
            <?php \yii\widgets\Pjax::begin(['id' => 'delivery-index',]); ?>
            <?= \app\components\AdminGridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'id' => 'delivery-index',
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],                   
                    [    
                        'label'=> 'Delivery #',    
                        //'format' => 'html',    
                        'attribute' => 'id',    
                        'value' => 'id'    
                    ], 
                    
                    [
                        'label' => 'Delivery Date',
                        'attribute' => 'deliveryDate',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Helper::date($model->deliveryDate);
                        },
                    ],
                    [    
                        'label'=> 'Order #',    
                        //'format' => 'html',    
                        'attribute' => 'orderId',    
                        'value' => 'orderId'    
                    ], 
                    
                    [
                        'label' => 'Order Date',
                        'attribute' => 'orderDate',
                        'format' => 'html',
                        'value' => function ($model) {
                            return Helper::date($model->order->orderDate);
                        },
                    ],
                    [
                        'label' => 'Delivery Address',
                        'attribute' => 'order.orderDeliveredAddress',
                        'format' => 'html',
                        'value' => 'order.orderDeliveredAddress',
                    ],
                    [
                        'label'=> 'View',
                        'format' => 'html',
                        'value' => function($model){
                            return Html::a('View',['delivery/view','id' => $model->id]);
                        }
                    ],

                    
                ],
            ]); ?>
            <?php  \yii\widgets\Pjax::end();  ?> 
        </div>
    </div>

</div>
