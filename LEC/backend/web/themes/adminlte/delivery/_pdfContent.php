<?php
use yii\helpers\Html;
use frontend\components\Helper;


//var_dump(ucwords(str_replace("-", " ", $delivery->paymentType)));die();

?>

<!DOCTYPE html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="margin:0;padding:0;font-family:Arial, Helvetica, sans-serif;font-size:13px;color:#3a3138;">
    <div style="width:936px;margin:0 auto;">

        <!--top-header-->
        <div style="float:left;width:100%;padding:10px 0;text-align:center;box-sizing:border-box;">
            <a href=""><?=$delivery->order->store->logo?></a>
            <h1 style="margin:10px 0 8px;font-size:25px;">In-Store Collection</h1>
            <p style="margin:6px 0 0px;font-size:12px;"><?=$delivery->order->storeAddressFormatted?>. Ph: <?=$delivery->order->store->phone?></p>
        </div>
        <!--top-header-->

        <!--content-sec-->
        <div style="float:left;width:100%;padding:10px 0;box-sizing:border-box;">
            <!--box1-->
            <div style="float:left;width:48%;padding:0 10px 0 0;box-sizing:border-box;">
                <!--inbox-->
                <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;height:155px;box-sizing:border-box;">
                    <h4 style="margin:0;margin-bottom:10px;font-size:16px;">
                    <img  alt="" src="/images/ico1.jpg">
                     Order #<?=$delivery->order->id?>
                    </h4>
                    <p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Order Date: <?=$delivery->order->orderPlacedDate?></p>
                    <p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Order Staus: <b><?=ucwords(str_replace("-", " ", $delivery->order->status))?></p>
                    <p style="margin:0;color:#8c8c7b;margin-bottom:10px;">Purchased From: <?=$delivery->order->storeName?></p>
                    <?php if($delivery->order->type == "byod") { ?>
                    <p style="margin:0;color:#8c8c7b;">BYOD Code: <?=$delivery->order->portal->studentCode?></p>
                    <?php } ?>
                </div>
                <!--inbox-->
            </div>
            <!--box1-->
            <!--box1-->
            <div style="float:left;width:48%;padding:0 10px 0 0;box-sizing:border-box;">
                <!--inbox-->
                <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;height:155px;box-sizing:border-box;">
                    <h4 style="margin:0;margin-bottom:10px;font-size:16px;">
                    <img  alt="" src="/images/ico2.jpg">
                     Customer Detail
                    </h4>
                    <!-- <p style="margin:0;margin-bottom:10px;color:#8c8c7b;"><?php //$delivery->order->billing_firstname?></p>
                    <p style="margin:0;margin-bottom:10px;color:#8c8c7b;"><?php //$delivery->order->billing_street?></p>
                    <p style="margin:0;color:#8c8c7b;margin-bottom:10px;"><?php //$delivery->order->billing_city?>, <?php //$delivery->order->billing_state?>, <?php //$delivery->order->billing_postcode?></p>
                    <p style="margin:0;color:#8c8c7b;">E: <?php //$delivery->order->email?> | P: <?php //$delivery->order->billing_phone?> </p> -->
                    <?=$delivery->order->deliveryBillingInfo?>
                </div>
                <!--inbox-->
            </div>
            <!--box1-->

            <div style="padding:15px;float:left;width:93%;margin-top:10px;background:#fafafa;border:1px solid #eeeeee;box-sizing:border-box;">
                <h4 style="margin:0;margin-bottom:10px;font-size:16px;">
                    <img  alt="" src="/images/ico4.jpg">
                    Item/s Collected
                </h4>
                <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;border: 1px solid #ddd;">
                    <thead>
                        <tr>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">#</th>
                            <th style="text-align:left;padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">ITEM</th>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">SKU</th>
                            <th style="padding:8px;border-right:1px solid #ddd;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">Price</th>
                            <th style="padding:8px;background:#5b5b5b;color:#fff;text-transform:uppercase;font-size:12px;">QTY Collected</th>
                        </tr>
                    </thead>
                    <tbody valign="top" style="background:#fff;">
                        <?=$delivery->pdfShortGrid?>
                    </tbody>
                </table>
            </div>  

            <!--bottom-cont-->
            <div style="float:left;width:100%;padding:10px 0;box-sizing:border-box;">
                <!--box1-->
                <div style="float:left;width:48%;padding:0 10px 0 0;box-sizing:border-box;">
                    <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;box-sizing:border-box;">
                        <h4 style="margin:0;margin-bottom:10px;font-size:16px;"><img src="/images/ico5.jpg"> Payment Information</h4>
                        <?php 
                        if(isset($delivery->order->portal)){
                            if($delivery->order->portal->payment_type == "no-online-payment" && $delivery->order->portal->shipment_type == "instore-pickup"){
                                    echo '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Payment Status: '.$delivery->order->paymentStatus."</p>";

                                    echo '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Payment Type: '.ucwords(str_replace("-", " ", $delivery->paymentType))."</p>";

                                    echo '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Amount Recieved in this Collection: '.Helper::money($delivery->amount)."</p>";

                                    echo '<p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Total Amount Recieved: '.Helper::money($delivery->order->amountPaid)."</p>";

                            } 
                        } else  {     ?> 
                                <?=$delivery->order->deliveryPaymentInformation?>
                        <?php } ?>
                    </div>
                    
                    <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;position:relative;margin-top:10px;height:78px;text-align:center;box-sizing:border-box;">
                        <div style="position:absolute;bottom:5px;width:100%;">Customer Signature</div>
                    </div>
                </div>
                <!--box1-->
                
                <!--box2-->
                <div  style="float:left;width:48%;padding:0 10px 0 0;box-sizing:border-box;">
                    <div style="background:#fafafa;border:1px solid #eeeeee;float:left;width:100%;padding:15px;height:250px;box-sizing:border-box;">
                        <h4 style="margin:0;margin-bottom:10px;font-size:16px;"><img src="/images/ico6.jpg">  Collection Information</h4>
                        <p style="margin:25px 0 10px;margin-bottom:10px;color:#8c8c7b;">Date Collected: <?=$delivery->deliveryPlacedDate?></p>
                        <p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Phone Number: <?=$delivery->phone?></p>
                        <p style="margin:0;margin-bottom:10px;color:#8c8c7b;">License Number: <?=$delivery->idProofNumber?></p>
                        <p style="margin:0;margin-bottom:10px;color:#8c8c7b;">Verified By: <?=$delivery->verifiedBy?></p>
                        <p style="margin:0;color:#8c8c7b;">Comments: <?=$delivery->comments?></p>
                    </div>
                </div>
                <!--box2-->
            </div>
            <!--bottom-cont-->

            <!--thankyou-bg-->
            <div style="float:left;width:100%;text-align:center;padding:50px 0 0;box-sizing:border-box;">
                -- Thank You --
            </div>
            <!--thankyou-bg-->

        </div>    
    </div>
</body>   
