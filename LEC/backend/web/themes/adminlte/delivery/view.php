<?php



use yii\helpers\Html;

use yii\widgets\DetailView;

use yii\grid\GridView;

use frontend\components\Helper;

use yii\widgets\ActiveForm;

use yii\widgets\ListView;

use yii\helpers\url;

use common\models\SalesComments;

use common\models\Shipments;

use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */

/* @var $model common\models\Shipments */

if($model->notify==1)
{
    $message='(the collection email was sent)';
}
else
{
    $message='(the collection email was not sent)';
}
$this->title = 'Collection #'.$model->deliveryId.' | '.$model->deliveryPlacedDate.$message;

?>
<div class="row">
    <div class="col-md-12">
        <div style="float:right; margin-bottom:15px;">
            <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/view' ,'id'=>$model->order->id], ['class' => 'btn btn-default ']) ?>
            <?= Html::a('<i class="fa fa-envelope"></i> Resend Collection Email', ['sendmail','id'=>$model->id],['class' => 'btn btn-primary hold',
            'data' => ['confirm' => 'Are you sure you want to send collection email to customer?','method' => 'post',]]) ?>
        </div>
    </div>
</div>
<div class="box box-default">
    <div class="box-body">
        <div class="shipments-view">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
                            <h3 class="box-title">Order #
                            <?= $model->order->orderId ?>
                            (Order confirmation email was sent)</h3>
                        </div>
                        <!-- /.box-header -->
                
                        <div class="box-body">
                          <p>Order Date :
                            <?= $model->order->orderPlacedDate ?>
                          </p>
                          <p>Order Status :
                            <b><?= ucfirst($model->order->status) ?></b>
                          </p>
                          <p>Purchased From :
                            <?= $model->order->storeName ?>
                          </p>
                        </div>
                        <!-- /.box-body -->             
                    </div>
                    <!-- /.box -->          
                </div>
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-header with-border"><i class="fa fa-user"></i>
                            <h3 class="box-title">Account Information</h3>
                        </div>
                        <!-- /.box-header -->
                
                        <div class="box-body">
                          <p> Customer Name :
                            <?= ucfirst($model->order->customer->firstname).' '.ucfirst($model->order->customer->lastname) ?>
                          </p>
                          <p> Email :
                            <?= $model->order->customer->email ?>
                          </p>
                        </div>
                    <!-- /.box-body -->             
                </div>
                <!-- /.box -->           
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-home"></i>
              <h3 class="box-title">Billing Address</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <p>
                <?= $model->order->billingAddressText ?>
              </p>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
            </div>
            <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
              <h3 class="box-title">Collect From Store</h3>
            </div>
            <!-- /.box-header -->
            
            <div class="box-body">
              <p>
                 <?= $model->order->orderDeliveredAddress ?>
              </p>
            </div>
            <!-- /.box-body --> 
            
          </div>
          <!-- /.box --> 
          
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border"><i class="fa fa-usd"></i>
                    <h3 class="box-title">Payment Information</h3>
                    </div>
                    <!-- /.box-header -->
            
                    <div class="box-body">
                      <?= $model->order->paymentInformation?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->           
            </div>
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border"><i class="fa fa-truck"></i>
                      <h3 class="box-title">Collection Information</h3>
                    </div>
                
                    <div class="box-body"> 
                        <p>Date Collected : <?= $model->deliveryPlacedDate?></p>
                        <p>License Number : <?= isset($model->idProofNumber)?$model->idProofNumber:'Not set'?></p>
                        <p>Verified By : <?= isset($model->verifiedBy)?$model->verifiedBy : 'Not set'?></p>
                        <p>Comments : <?= isset($model->comments)? $model->comments : 'Not set' ?></p>
                    </div>
                
                </div>
                <!-- /.box --> 
              
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>
                      <h3 class="box-title">Order Summary</h3>
                    </div>
                    <!-- /.box-header -->
            
                    <div class="box-body">
                      <?= GridView::widget([
            
                                'dataProvider' => $dataProvider,
            
                                //'filterModel' => $searchModel,
            
                                'columns' => [
            
                                ['class' => 'yii\grid\SerialColumn'],
            
            
            
                                //'id',
            
                                /*[
            
                                    'label'=> 'Product Name',
            
                                    'attribute' => 'orderId',
            
                                    'value' => 'orderItem.product.name'
            
                                ],*/
                                [
                                    'label'=> 'Item Orderd',
                                    'attribute' => 'id', 
                                    'format' => 'html',
                                    'value' => function ($model) {
                                        return !empty($model->orderItem->giftWrap)?($model->orderItem->product->name.' <br/><span class="label label-success">$'.round($model->orderItem->giftWrapAmount).' Gift Wrapping Included</span>'.($model->orderItem->superAttributeValuesText!='N/A'?' '.$model->orderItem->superAttributeValuesText:'')):$model->orderItem->product->name.' '.($model->orderItem->superAttributeValuesText!='N/A'?$model->orderItem->superAttributeValuesText:'');
                                    },
                                ],
                                /*[
            
                                    'label' => 'Option(s)',

                                    'value' => 'orderItem.superAttributeValuesText',

                                    'format' => 'html'

                                ],*/
            
                                [
            
                                    'label'=> 'SKU',
            
                                    'attribute' => 'productId',
            
                                    'value' => 'orderItem.sku'
            
                                ],
            
                                'quantityDelivered',
            
                                ],
            
                            ]); ?>
                    </div>
                    <!-- /.box-body -->             
                </div>
                <!-- /.box -->           
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border"><i class="fa fa-home"></i>
                         <h3 class="box-title"> Add Collection Comments</h3>
                    </div>   
                    <div class="box-body">
                        <div class="order-comment-form">
                            <?php $form = ActiveForm::begin(['action' =>  Url::to(['sales-comments/create'])]);     
                                $model_dc= new SalesComments();     
                                $model_dc->typeId=$model->id; $model_dc->type='delivery';$model_dc->orderId=$model->order->id; ?>
                            <?= $form->field($model_dc, 'type')->hiddenInput()->label(false)  ; ?>
                            <?= $form->field($model_dc, 'typeId')->hiddenInput()->label(false)  ; ?>
                            <?= $form->field($model_dc, 'orderId')->hiddenInput()->label(false)  ; ?>
                            <?= $form->field($model_dc, 'comment')->textarea(['rows' => 4])-> label('Add Delivery Comments'); ?>
                            <?= $form->field($model_dc, 'notify')->checkbox(); ?>
                            <div class="form-group">
                                <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-success']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
               
                        </div>
                    </div>                             
                </div>                 
            </div>
            <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border"><i class="fa fa-building-o"></i>
                     <h3 class="box-title">Collection Comments History</h3>
                    </div>
                    <div class="box-body">
                       <?= ListView::widget([    
                            'dataProvider' => $dataProvider_salescomments,
                            'itemView' => '_listview',
                            'summary'  =>'',    
                        ]); ?>
                    </div>           
                </div>          
            </div>
        </div>
    </div>
</div>
    
