<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\components\Helper;
use yii\widgets\ListView;
use yii\helpers\url;
use yii\grid\GridView;
use common\models\SalesComments;
use common\models\OrderItems;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Deliveries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deliveries-form">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="ordr-sts">Order Status : <?= $model->order->formattedStatus?>
            </span></div>
            <div style="float:right; margin-bottom:15px;">
                <?php
                    if($model->order->type=="byod")
                        $action = 'byod-orders/view';
                    else
                        $action = 'orders/view';

                ?>        
                <?= Html::a('<i class="fa fa-angle-left"></i> Back', [$action ,'id'=>$model->order->id], ['class' => 'btn btn-default']) ?>
                <?php if($model->order->status!="failed"){ ?>
                    <?= Html::a('<i class="fa fa-envelope"></i> Resend Order Confirmation', ['sendmail','id'=>$model->order->id],['class' => 'btn btn-primary hold','data' => ['confirm' => 'Are you sure you want to resend the order confirmation email to customer?','method' => 'post',]]) ?> 
                      
                <?php } ?> 
            </div>
        </div>
    </div>
    
    <div class="box">
        <div class="box-body">
             <div class="row">
                <div class="col-md-12 form-table">
                    <?php $form = ActiveForm::begin(); ?>    
                        <?= GridView::widget([    
                            'id' => 'order-items',    
                            'dataProvider' => $dataProvider,
                            'summary' => false,
                            'columns' => [    
                                ['class' => 'yii\grid\SerialColumn'], 
                                //'id',    
                                [    
                                    'label'=> 'Item Orderd',    
                                    'attribute' => 'id', 
                                    'format' => 'html',
                                    'value' => function ($model) {
                                        return $model->productDetails;
                                    },
                                ],    
                                [    
                                    'label'=> 'SKU',    
                                    'attribute' => 'productId',    
                                    'value' => 'sku'    
                                ], 
                                
                                [    
                                    'label' => 'Price',    
                                    'attribute' => 'id',    
                                    'format' => 'html',    
                                    'value' => function ($model) {    
                                        return Helper::money($model->itemSubTotal);    
                                    },    
                                ],
                                [    
                                    'label' => 'Qty To Fulfill',    
                                    'attribute' => 'quantity',    
                                    'format' => 'html',    
                                    'value' => function ($model) {    
                                        return $model->quantity;    
                                    },    
                                ],
                                [           
                                    'label' => 'Qty Ready For Collection',         
                                    'format' => 'raw', 
                                    'value' => function ($model) { //var_dump(ArrayHelper::map($enabledProducts, 'id', 'productId'));die();            
                                        return '<span class="qtyavailable">'.$model->qtyToDelivered.'</span><input type="hidden" id="qts'.$model->id.'" name="DeliveryItems[orderItemId][]" value="'.$model->id.','.$model->qtyToDelivered.'">' ;
                                                
                                    }, 
                                ],
                                                  
                                
                               
                            ],

                        ]); ?>
                        
                    <!-- /.box -->  
                    
                    <div class="cl-cnt">                    
                            <div class="shipping-form" id="shipping-form">
                                                           
                                    <div class="row">
                                      <div class="col-sm-6">
                                        <?php echo '<label class="control-label"> Date Collected </label>'; ?>
                                                <?=  DatePicker::widget([
                                                    'name' => 'deliveryDate',
                                                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                                    'value' => date("d-M-Y"),
                                                    'pluginOptions' => [
                                                        'autoclose'=>true,
                                                        'format' => 'dd-M-yyyy'
                                                    ]
                                                ]);?>
                                      </div>
                                      <div class="col-sm-6">
                                        <?= $form->field($model, 'verifiedBy')->textInput()->label('Verified By (Staff)') ?>
                                      </div>
                                    </div>
                                    <div class="row">
                                      
                                      
                                      
                                      <div class="col-sm-6">
                                        <div class="row">
                                         <div class="col-sm-12">
                                        <?= $form->field($model, 'collectedBy')->textInput() ?>
                                      </div>
                                          <div class="col-sm-6">
                                            
                                            <?= $form->field($model, 'idProofNumber')->textInput()->label('License Number/Photo ID') ?>
                                          </div>
                                          <div class="col-sm-6">
                                            <?= $form->field($model, 'phone')->textInput() ?>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="col-sm-6 txtx">
                                        <?= $form->field($model, 'comments')->textarea() ?>
                                      </div>

                                    <?php if($model->order->type == "byod") {  ?>
                                        <?php if($model->order->portal->shipment_type == "instore-pickup" && $model->order->portal->payment_type == "no-online-payment") { ?>
                                            <!--<div class="row">-->
                                                <div class="col-sm-6">
                                                    <?= $form->field($model, 'amount')->textInput() ?>
                                                </div>
                                                <div class="col-sm-6">
                                                    <?= $form->field($model, 'paymentType')->dropDownList(['cash' => 'Cash', 'credit-card' => 'Credit Card', 'debit-card' => 'Debit Card', 'cheque' => 'Cheque']) ?>
                                                </div>
                                            <!--</div>-->
                                    <?php } } ?>  
                                </div>
                                    
                            </div>
                        </div>
                        
                        <div class="form-btm-btns">  
                        <!--<p class="ship-check">
                            <input type="checkBox" class="enabled_checkbox" id="delivery-chkmail" name="Delivery[chkmail]" checked>Email Collection Advice
                        </p>-->
                        
                          <?= Html::submitButton('Generate collection advice', ['class' => 'btn btn-primary generate-coln']) ?>
                        
                        <?php ActiveForm::end(); ?>
                    </div>
                                   
                </div>
            </div>
            
            
            
            
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $(".qts").change(function(){
        var text = $( this ).val();
        var oi_id=$(this).attr('id');
        $('#qts'+oi_id).val(oi_id+','+text);
        //alert($('#qts'+oi_id).val());
    });     
});
</script>

<script type="text/javascript">
    $(document).ready(function(){
        <?php if($model->order->type == "byod") {  ?>
        <?php if($model->order->portal->payment_type == "no-online-payment" && $model->order->portal->shipment_type == "instore-pickup") { ?>
            var totalRecieved = <?=$model->order->amountPaid?>;
            $('.generate-coln').on("click", function (e) {
                var amountEntered = $('#deliveries-amount').val();
                if(confirm("Are you sure that you want to generate the collection advice? Payment received is AU$"+amountEntered)){
                    return true;
                }
                else{
                    return false;
                }
            });
        <?php } ?>
    <?php } ?>
    });
</script>