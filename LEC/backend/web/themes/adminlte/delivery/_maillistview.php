<?php
    use yii\helpers\Html;
    use frontend\components\Helper;
    $thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->orderItem->product->getThumbnailImage();
?>
<tr>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$index +1?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;">
            <img src="<?=$thumbImagePath?>" width="66px"></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;">
      <?=Helper::stripText($model->orderItem->product->name,15) ?><br/>
    </td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$model->orderItem->product->sku?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$model->orderItem->quantity?></td>
    <td style="padding:5px 3px;border-bottom:1px solid #eeeeee;text-align:center;"><?=$model->quantityDelivered?></td>
</tr>
                                  