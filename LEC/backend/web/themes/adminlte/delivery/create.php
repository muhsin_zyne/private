<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Deliveries */

$this->title = 'Create Deliveries';
$this->params['breadcrumbs'][] = ['label' => 'Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deliveries-create">

 

  <?= $this->render('_form', [
        'model' => $model,
        'dataProvider'=>$dataProvider,
    ]) ?>

</div>
