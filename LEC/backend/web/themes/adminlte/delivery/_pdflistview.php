<?php 
use yii\helpers\Html;
use frontend\components\Helper;

$thumbImagePath = Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->orderItem->product->getThumbnailImage();

$wrapAmount = !empty($model->orderItem->giftWrap) ? $model->orderItem->giftWrapAmount : 0;

?>

<tr style="background:#fff;">
                    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;">1</td>
                    <td style="text-align:left;padding:8px;border-right:1px solid #ddd;border-bottom:1px solid #ddd;">                  
                        <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;">
                            <tbody>
                                <tr>
                                    <td width="72" style="padding:5px;border:1px solid #ccc;text-align:center;box-sizing:border-box;">
                                        <img src="<?=$thumbImagePath?>" alt="items" style="max-width:60px;">
                                    </td>
                                    <td style="font-size:12px;padding-left:10px;box-sizing:border-box;">
                                        <table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;max-width:100%;border-spacing:0;border-collapse:collapse;">
                                            <tr>
                                                <td style="padding:0 0 4px;font-size:12px;"><?=$model->orderItem->product->name?></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:0 0 4px;color:#e31837;font-size:12px;"><?=Helper::money($model->orderItem->product->price)?></td>
                                            </tr>
                                            <?php if(!empty($model->orderItem->giftWrap)) { ?> 
                                            <tr>
                                                <!-- <td style="background:#2e823e;padding:3px 7px;color:#fff;float:left;border-radius:3px;box-sizing:border-box;">Gift Wrapping: <?php //Helper::money(round($wrapAmount))?>
                                                <!--</td> -->
                                                <td style="color:#2e823e;float:left;">
                                                    Gift Wrapping: <?=Helper::money(round($wrapAmount))?>
                                                </td>
                                            </tr>
                                            <?php } ?> 
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;"><?=$model->orderItem->product->sku?></td>
                    <td style="padding:8px;text-align:center;border-right:1px solid #ddd;border-bottom:1px solid #ddd;font-size:12px;"><?=Helper::money($model->orderItem->product->price + $wrapAmount)?></td>
                    <td style="padding:8px;text-align:center;border-bottom:1px solid #ddd;">
                        <table width="43" border="0" cellspacing="0" cellpadding="0" align="center" style="border-spacing:0;border-collapse:collapse;">
                            <tr>
                                <td valign="middle" height="23" style="padding-top:3px;border:1px solid #4dba8d;width:43px;height:23px;box-sizing:border-box;">
                                    <?=$model->quantityDelivered?> <img src="/images/tick2.jpg" style="float:right;margin:3px 5px 0 0;">
                                </td>
                            </tr>
                        </table>
                    </td>
                  </tr>