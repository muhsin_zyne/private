<?php

use backend\assets\AppAsset;

use yii\helpers\Html;

use yii\bootstrap\Nav;

use yii\bootstrap\NavBar;

use yii\widgets\Breadcrumbs;

use kartik\sidenav\SideNav;

use backend\components\AdminNavBar;



/* @var $this \yii\web\View */

/* @var $content string */



AppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>

    <meta charset="<?= Yii::$app->charset ?>"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- bootstrap 3.2.0 -->

    <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->

    <!-- Font Awesome Icons -->

    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Ionicons -->

    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->

    <!-- <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" /> -->

    <link href="/css/bootstrap-tour-standalone.min.css" rel="stylesheet" type="text/css" />

    <link href="/css/help-screenshot.css" rel="stylesheet" type="text/css" />

    <!-- local css -->

    <link href="/css/style.css" rel="stylesheet" type="text/css" />

    <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> 

    <link rel="stylesheet" href="/css/jquery.gritter.css" />

    <link rel="stylesheet" href="/css/fileinput.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

    <!--<link rel="stylesheet" href="/css/fileinput.min.css" />
    <link rel="stylesheet" href="/css/easyTree.css" />
    <link rel="stylesheet" href="/css/style_treeview.css" />-->

    <!-- <link rel="stylesheet" href="css/kv-tree.css" /> 



    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->

    <script src="/js/jquery-1.9.1.min.js"></script>

    <!--<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script src="/adminlte/js/responsive-tabs.js"></script>-->

    <script src="/js/bootstrap-tour-standalone.min.js"></script>

    <script src="/js/ajax-update.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>

    <script src="/js/wl_Form.js"></script>

    <script src="/js/functions.js"></script>

    <script src="/js/jquery.gritter.js"></script>

    <script src="/js/jquery.slimscroll.js"></script>

    <script src="/js/fileinput.js"></script>

    <script src="/js/scripts.js"></script>

    <script src="/js/jquery.nestable.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-modal.js"></script>

    <!-- <script type="text/javascript" src="/js/bootstrap-modal.js"></script> -->

    <!--<script src="js/kv-tree.js"></script>-->

    <?= Html::csrfMetaTags() ?>

    <title>LEC Website Admin Panel - <?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

</head>

<body class="skin-blue fixed">

    <?php $this->beginBody() ?>

            <?php

            AdminNavBar::begin([

                'brandLabel' => 'My Company',

                'brandUrl' => Yii::$app->homeUrl,

                'options' => [

                    'class' => 'navbar-inverse navbar-fixed-top navbar-static-top',

                ],

                'userName'=>Yii::$app->user->identity->fullName

            ]);

            $menuItems = [

                ['label' => 'Home', 'url' => ['/site/index']],

            ];

            if (Yii::$app->user->isGuest) {

                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];

            } else {

                $menuItems[] = [

                    'label' => 'Logout (' . Yii::$app->user->identity->email . ')',

                    'url' => ['/site/logout'],

                    'linkOptions' => ['data-method' => 'post']

                ];



            }

            // echo Nav::widget([

            //     'options' => ['class' => 'navbar-nav navbar-right'],

            //     'items' => $menuItems,

            // ]);

            AdminNavBar::end();

        ?>

    <div class="wrapper  row-offcanvas row-offcanvas-left">

        <aside class="left-side sidebar-offcanvas" style="min-height: 2250px;">

                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">

                    <i class="fa fa-bars"></i>

                </a> 

                <!-- sidebar: style can be found in sidebar.less -->

                <section class="sidebar">

                    <!-- Sidebar user panel -->

                    <div class="user-panel">

                        <div class="pull-left image">

                            <!--<img src="/images/avatar5.png" class="img-circle" alt="User Image">--> 

                        </div>

                        <div class="pull-left info">

                             <p>Hello, <?=Yii::$app->user->identity->fullName?></p>  







                            <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->

                        </div>

                    </div>

                    <!-- search form -->

                    <!-- <form action="#" method="get" class="sidebar-form">

                        <div class="input-group">

                            <input type="text" name="q" class="form-control" placeholder="Search...">

                            <span class="input-group-btn">

                                <button type="submit" name="seach" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>

                            </span>

                        </div>

                    </form> -->

                    <!-- /.search form -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->

                    <ul class="sidebar-menu">

                        <li>

                            <?php  

                                echo SideNav::widget([

                                'type' => SideNav::TYPE_DEFAULT,

                                'iconPrefix' => 'fa fa-',

                                'indItem' => '',

                                'indMenuOpen' => '<i class="fa fa-angle-down"></i>',

                                'indMenuClose' => '<i class="fa fa-angle-double-right"></i>',

                                'items' => Yii::$app->controller->mainMenu,

                                ]); 

                            ?>

                        </li>

                    </ul>

                </section>

                <!-- /.sidebar -->

            </aside>

        <aside class="right-side">

            <section class="content-header">

                <h1><span><?=$this->title?></span>

                <?php if (isset($this->params['tooltip'])) {  ?>

                    <i class="fa fa-question-circle question" data-toggle="tooltip" data-placement="right" title="<?=$this->params['tooltip']?>"></i><div class="clearfix"></div>

                <?php } ?>

                <div class="clearfix"></div>

                </h1>

                <?= Breadcrumbs::widget([

                'tag' => 'ol',

                'options' => ['class' => 'breadcrumb'],

                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],

            ]) ?>
            
            <div class="clearfix"></div>

            </section>
            
            <section class="content">
                <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
            <?php if(Yii::$app->session->getFlash('error'))

              { ?>

              <div class="alert alert-danger alert-dismissable">

                <i class="icon fa fa-ban"></i>

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>                         

                <?= Yii::$app->session->getFlash('error'); ?>       

              </div> 

            <?php } ?>

            <?php if(Yii::$app->session->getFlash('success'))

              { ?>

              <div class="alert alert-success alert-dismissable">

                <i class="icon fa fa-check"></i>

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>

                <?= Yii::$app->session->getFlash('success'); ?>       

              </div> 

            <?php } ?>

            <?= $content ?>

            </section>

        </aside>

    </div>


    <!--<script src="/js/datepicker-kv.js"></script>-->

    <!-- <footer class="footer">

        <div class="container">

        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>

        </div>

    </footer> -->



    <?php $this->endBody() ?>

</body>

</html>

<?php $this->endPage() ?>

