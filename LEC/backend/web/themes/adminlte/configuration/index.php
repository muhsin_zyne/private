<?php



use yii\helpers\Html;

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;

use common\models\AttributeGroups;

use dosamigos\multiselect\MultiSelect;

use backend\components\Helper;

use \kartik\file\FileInput;

use yii\grid\GridView;

use common\models\AttributeOptions;

use common\models\Attributes;

use common\models\User;





$user = User::findOne(Yii::$app->user->id);

//var_dump($user->store->id);die();



/* @var $this yii\web\View */

/* @var $model common\models\Attributes */

/* @var $form yii\widgets\ActiveForm */

$this->title = "Configuration";
$this->params['breadcrumbs'][] = 'Configuration';

$session = Yii::$app->session;

$stores = \common\models\Stores::find()->where(['isVirtual' => '0'])->all();

$storeId = isset($session['storeId'])? $session['storeId'] : 0;

?>


<div class="row">
    <div class="col-xs-12"> 
        <div class="box-body">
            <?= ((Yii::$app->user->identity->roleId == "1")? Html::dropDownList('listname', isset($session['storeId']) ? $session['storeId'] : "",

            [0=>'Default Values'] + ArrayHelper::map($stores,'id','title'),['class' => 'store-dropdown']) : ""); 

            //var_dump(ArrayHelper::map($stores,'id','title'));die;

            ?>
        </div>
    </div>
</div>
   
<div class="box">
<div class="categories-index">

    <div class="nav-tabs-custom">             

        <ul class="nav nav-tabs pull-right">

            <li class="active"><a data-toggle="tab" href="#store-settings">Store Settings</a></li>

            <li id="m_title"><a data-toggle="tab" href="#store-emails">Store Email Addresses</a></li>
            <?php if(Yii::$app->user->identity->roleId == "1"){ ?>
                <li id="m_title" ><a data-toggle="tab" href="#mail-settings">Mail Sending Settings</a></li>
    
                <li id="m_title" ><a data-toggle="tab" href="#banner-resolutions">Store Banner Resolutions</a></li>
	    <?php } ?>
            <li id="m_title" ><a data-toggle="tab" href="#meta-description">Meta Descriptions</a></li>
            
            <li id="m_title" ><a data-toggle="tab" href="#gift-wrapping">Gift Wrapping</a></li>

            <li id="g_title" ><a data-toggle="tab" href="#gatc">Google Analytics </a></li>

        </ul>
        
    </div>  

</div>

<div class="row">

    <div class="col-xs-12">

      

        <div class="box-body">

            <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::to(['configuration/update'])]); ?>            

                <div class="tab-content responsive">

                    <div class="tab-pane active" id="store-settings">
                        <div class="tab-form">
                            <?=$form->field($config, 'gst_percentage')->label('GST Percentage') ?>
                            <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>    

                    <div class="tab-pane" id="store-emails">

                        <div class="tab-form" >

                            <?=$form->field($config, 'general_sender_name')?>

                            <?=$form->field($config, 'general_sender_email')?>

                            <?=$form->field($config, 'order_confirmation_recipients')->textArea(['rows'=>2,'cols'=>5])?>

                            <?=$form->field($config, 'contact_form_recipients')->textArea(['rows'=>2,'cols'=>5])?>

                            <div class="form-group">

                             <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

                            </div>

                        </div>

                    </div>



                    <div class="tab-pane" id="mail-settings">

                        <div class="tab-form">

                            <?=$form->field($config, 'smtp_host')?>

                            <?=$form->field($config, 'smtp_username')?>

                            <?=$form->field($config, 'smtp_password')?>

                            <?=$form->field($config, 'smtp_port')?>

                            <?=$form->field($config, 'smtp_encryption')->dropDownList(['tls' => 'TLS'])?>

                            <div class="form-group">

                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

                            </div>

                        </div>

                    </div>

                    

                    <div class="tab-pane" id="banner-resolutions">

                        <div class="tab-form">

                            <?=$form->field($config, 'mainbanner_resolution')->dropDownList([
                                '1920px X 655px'=>'1920px X 655px',                                
                                '824px X 440px'=>'824px X 440px',
                                '856px X 406px'=>'856px X 406px'
                                ],['prompt' => '--Select--'])->label('Main Banner Resolution')?>                            

                            <?=$form->field($config, 'minibanner_resolution')->dropDownList(['325px X 210px'=>'325px X 210px'],['prompt' => '--Select--'])->label('Mini Banner Resolution')?>

                            <?=$form->field($config, 'sidebanner_resolution')->dropDownList(['320px X 395px'=>'320px X 395px'],['prompt' => '--Select--'])->label('Side Banner Resolution')?>

                            <?=$form->field($config, 'stripbanner_resolution')->dropDownList(['1169px X 143px'=>'1169px X 143px'],['prompt' => '--Select--'])->label('Strip Banner Resolution')?>

                            <?=$form->field($config, 'mobilebanner_resolution')->dropDownList(['650px X 325px' => '650px X 325px'],['prompt' => '--Select--'])->label('Mobile Banner Resolution')?>

                            <div class="form-group">

                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

                            </div>                

                        </div>

                       

                    </div>

                    <div class="tab-pane" id="meta-description">

                        <div class="tab-form">

                            <?=$form->field($config, 'meta_title')?>

                            <?=$form->field($config, 'meta_keywords')?>

                            <?=$form->field($config, 'meta_description')?>                            

                            <div class="form-group">

                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

                            </div>

                        </div>

                    </div>
                     <div class="tab-pane" id="gift-wrapping">

                        <div class="tab-form">

                             <?= $form->field($config, 'gift_wrapping')->dropDownList(['no' => 'No Gift Wrapping Facility', '0' => 'Free Gift Wrapping', '5' => 'Gift Wrapping $5', '10' => 'Gift Wrapping $10']); ?>
                            <div class="form-group">

                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

                            </div>

                        </div>

                    </div> 
                    <div class="tab-pane" id="gatc">

                        <div class="tab-form">

                            <?=$form->field($config, 'google_analytics')?>
                            

                            <div class="form-group">

                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>

                            </div>

                        </div>

                    </div>

                    <?php if (Yii::$app->user->identity->roleId == "1") { ?>

                    <input type="hidden" class="storeid" name="ConfigurationForm[storeId]" value="">

                    <?php } elseif (Yii::$app->user->identity->roleId == "3") { ?>

                        <input type="hidden" class="storeid" name="ConfigurationForm[storeId]" value="<?=$user->store->id?>">

                    <?php } ?>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>

    </div>

</div>



<script language="javascript">

    $(document).ready(function(){

        $('.store-dropdown').change(function(){

            if(confirm("Please confirm site switching. All data that hasn't been saved will be lost.")){

                storeId = $(this).val();

                $.ajax({

                  url:'<?php echo Yii::$app->request->baseUrl;?>/users/setstore',   

                  data: {storeId: storeId}, 

                  success: function(data)

                        {

                            location.reload();

                        }

                });

            }

            else{

                return false;

            }   

        });



        $('.storeid').val($('.store-dropdown').val());

    });

</script>
