<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use frontend\components\Helper;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\helpers\url;
use common\models\SalesComments;
/* @var $this yii\web\View */
/* @var $model common\models\CreditMemos */
if($model->notify==1)
{
    $message='(the credit memo email was sent)';
}
else
{
    $message='(the credit memo email was not sent)';
}
$this->title = 'Credit Memo #'.$model->creditmemoId.'|'.$model->status.'|'.$model->creditmemoPlacedDate.$message;
?>
<div class="credit-memos-view">

    <div class="row">
    <div class="col-md-12">
        <div style="float:right; margin-bottom:15px;">
            <?= Html::a('<i class="fa fa-angle-left"></i> Back', ['orders/view' ,'id'=>$model->order->id], ['class' => 'btn btn-default ']) ?>
            <?= Html::a('<i class="fa fa-envelope"></i> Resend credit-memo mail', ['sendmail','id'=>$model->id],['class' => 'btn btn-primary hold',
            'data' => ['confirm' => 'Are you sure you want to send credit-memo email to customer?','method' => 'post',]]) ?>
        </div>
    </div>
</div>
<div class="box box-default">
    <div class="box-body">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                     <i class="fa fa-bullhorn"></i><h3 class="box-title">Order # <?= $model->order->orderId ?> (Order confirmation email was sent)</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p>Order Date : <?= $model->order->orderPlacedDate ?></p>
                    <p>Order Status : <b><u><?= $model->order->status ?></u></b></p>
                    <p>Purchased From : <?= $model->order->storeName ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Account Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p> Customer Name : <?= ucfirst($model->order->customer->firstname).' '.ucfirst($model->order->customer->lastname) ?></p>
                    <p> Email : <?= $model->order->customer->email ?></p>
                    <p> Phone : <?= $model->order->billing_phone ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Billing Address</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p><?= $model->order->billingAddressText ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Collect From Store</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <p><?= $model->order->orderDeliveredAddress ?></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Payment Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= $model->order->paymentDetails?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Shipping & Handling Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                     Total Shipping Charges: 0
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Items Refunded</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'label'=> 'Product Name',
                            'attribute' => 'id',
                            'value' => 'orderItem.product.name'
                        ],
                        [
                            'label'=> 'SKU',
                            'attribute' => 'id',
                            'value' => 'orderItem.product.sku'
                        ],
                        [
                            'label' => 'Price',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money($model->orderItem->price);
                            },
                        ],
                    
                        [
                            'label' => ' Qty Refunded',
                            'attribute' => 'quantityRefunded',
                        ],
                        [
                            'label' => 'Sub Total',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money(($model->orderItem->price) * $model->quantityRefunded);
                            },
                        ],
                        [
                            'label' => 'Discount Amount',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money($model->orderItem->discount);
                            },
                        ],
                        [
                            'label' => 'Gift Wrapping',
                            'attribute' => 'id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Helper::money($model->orderItem->giftWrapAmount* $model->quantityRefunded);
                            },
                        ],
                        [
                            'label' => 'Row Total',
                            'attribute' => 'id',
                            'format' => 'html',
                            'value' => function ($model) {
                                return Helper::money(($model->orderItem->price * $model->quantityRefunded)-$model->orderItem->discount+($model->orderItem->giftWrapAmount* $model->quantityRefunded));
                            },
                        ],

                    //['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Credit Memo History</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <h3>Add Credit Memo Comments</h3>
                    <div class="order-comment-form">
                        <?php $form = ActiveForm::begin(['action' =>  Url::to(['sales-comments/create'])]); 
                            $model_cmc= new SalesComments(); 
                            $model_cmc->typeId=$model->id; $model_cmc->type='creditmemo';$model_cmc->orderId=$model->order->id; ?>
                            <?= $form->field($model_cmc, 'type')->hiddenInput()->label(false)  ; ?>
                            <?= $form->field($model_cmc, 'typeId')->hiddenInput()->label(false)  ; ?>
                            <?= $form->field($model_cmc, 'orderId')->hiddenInput()->label(false)  ; ?>
                            <?= $form->field($model_cmc, 'comment')->textarea(['rows' => 4]) ?>
                            <div class="form-group">
                                <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-success']) ?>
                            </div>
                            <?= $form->field($model_cmc, 'notify')->checkbox(array('label'=>'Notify Customer by Email')); ?>
                        <?php ActiveForm::end(); ?>
                        <?= ListView::widget([
                            'dataProvider' => $dataProvider_salescomments,
                            //'filterModel' => $searchModel_ordercomment,
                            'itemView' => '_listview',
                            'summary'=>'',
                        ]); ?>
                    </div> 
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i><h3 class="box-title">Credit Memo Totals</h3>
                </div><!-- /.box-header -->
                <div class="box-body" >
                    <!-- <p>Subtotal    : <b><?= Helper::money($model->subTotal) ?></b></p>  -->                   
                    <p>Refunded Total    : <b><?= Helper::money($model->grandTotal) ?></b></p>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
