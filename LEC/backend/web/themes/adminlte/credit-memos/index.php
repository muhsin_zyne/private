<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CreditMemosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Credit Memos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
<div class="box-body">

<div class="credit-memos-index">
     <?php \yii\widgets\Pjax::begin(['id' => 'credit-memos-index',]); ?>
    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'createdDate',
            // [
            //     'label'=> 'Created At',
            //     'attribute' => 'id',
            //     'value' => 'creditmemoPlacedDate'
            // ],
            //'creditmemoPlacedDate',
            'orderId',
            [
                'label'=> 'Order Date',
                'attribute' => 'orderDate',
                'value' => 'order.orderDate'
            ],
            //'order.orderPlacedDate',
            [
                'label'=> 'Bill To Address',
                'format' => 'html',
                'attribute' => 'orderBillToAddress',
                'value' => 'order.billingAddressText',
            ],
            //'order.billingAddressText',
            //'comments:ntext',
            //'subTotal',
            //'shipping',
            // 'grandTotal',
            //'status',
            [
                'label' => 'Status',
                'attribute' => 'status',
                'value' => function($model, $attribute){ 
                    return $model->status;
                },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['pending' => 'pending', 'refunded' => 'refunded', 'cancelled' => 'cancelled',],                   
                    ['class'=>'form-control','prompt' => '']),
            ],
            [
                'label' => 'Amount',
                'attribute' => 'grandTotal',
                'format' => 'html',
                'value' => function ($model) {
                    return Helper::money($model->grandTotal);
                },
            ],
             
            // 'createdDate',

            [
                'label'=> 'View',
                'format' => 'html',
                'value' => function($model){
                    return Html::a('View',['credit-memos/view','id' => $model->id]);
                }
            ],
        ],
    ]); ?>
    <?php  \yii\widgets\Pjax::end();  ?> 
</div>
</div>
</div> 


