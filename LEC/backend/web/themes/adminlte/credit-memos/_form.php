<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\components\Helper;
use yii\widgets\ListView;
use yii\helpers\url;
use yii\grid\GridView;
use common\models\CreditMemos;
use common\models\Shipments;
use common\models\OrderItems;

$this->title = 'Refund';
?>

<div class="modal-dialog modal-lg quick-view-wrapper portal-wrapper refund-pop" role="document">
   <div class="box order-popup-bg">
        <div class="box-body">        
            <?php $form = ActiveForm::begin(['id'=>'client_form','enableAjaxValidation' => false,'enableClientValidation' => true, 'validateOnType' => true, 'validationDelay' => '0']); 
            $model_r= new CreditMemos();
            $model_r->orderId= $model->orderId; 
            $model_r->grandTotal= number_format((float)$model->RefundGrandTotal, 2, '.', ''); 
            
            ?>
            <h3 class="poporder-head"><?= $this->title?></h3>
            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
             <?= $form->field($model, 'id')->hiddenInput()->label(false)  ; ?>
             <div class="row">
            <div class="col-xs-12 popup-table refund-table">                   
                <?= GridView::widget([    
                    'id' => 'order-items',    
                    'dataProvider' => $dataProvider,
                    'summary' => false,
                    'columns' => [    
                        ['class' => 'yii\grid\SerialColumn'], 
                        //'id',    
                        [    
                            'label'=> 'Item Orderd',    
                            'attribute' => 'id', 
                            'format' => 'html',
                            'value' => function ($model) {
                                return !empty($model->giftWrap)  ? 
                                    '<div class="tb-img">'.Html::img(Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage()).'</div>'.
                                        ($model->product->name.'<br/><div class="tb-price">'.Helper::money($model->price).'</div><span class="label label-success">$'.round($model->giftWrapAmount).' Gift Wrapping Included</span>'.($model->superAttributeValuesText!='N/A'?' '.$model->superAttributeValuesText:'')) : 
                                    '<div class="tb-img">'.Html::img(Yii::$app->params["rootUrl"].Yii::$app->params["productImagePath"]."/thumbnail/".$model->product->getThumbnailImage()).'</div>'.$model->product->name.' '.($model->superAttributeValuesText!='N/A'?$model->superAttributeValuesText:''.'<br/><div class="tb-price">'.Helper::money($model->price).'</div>');
                            },
                        ],    
                        [    
                            'label'=> 'SKU',  
                            'value' => 'sku'    
                        ], 
                        [
                            'label' => 'Discount',                            
                            'format' => 'raw',
                            'value' => function ($model) {
                                 return Helper::money($model->discount).
                                //return '$AU<span id="sda'.$model->id.'" value="'.$model->discount.'"></span>'.
                                '<input type="hidden" id="da'.$model->id.'" value="'.$model->discountAmount.'">';
                            },
                        ],
                        [
                            'label' => 'Gift Wrapping',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Helper::money($model->giftWrapAmount* $model->qtyToCreditMemo).
                                '<input type="hidden" id="gw'.$model->id.'" value="'.$model->giftWrapAmount* $model->qtyToCreditMemo.'">';
                            },
                        ],
                        [    
                            'label' => 'Item Total', 
                            'format' => 'html',    
                            'value' => function ($model) {    
                                return Helper::money($model->itemSubTotal);    
                            },    
                        ],
                        [
                            'label' => 'Refund Amount',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return '<input type="number" class="refund" name="refund['.$model->id.']" value="'.$model->RefundAmountItem.'" >';
                                
                                
                            },
                        ],
                        [    
                            'label' => 'Qty',  
                            'format' => 'raw',    
                            'value' => function ($model) {    
                                return $model->quantity;
                            },    
                        ], 
                                                
                        
                       
                    ],

                ]); ?> 
            </div>
            </div>
            
            <div class="row pop-btm">            
                <div class="col-xs-6">
                    <?= $form->field($model_r, 'comments')->textarea(array('placeholder' => 'If you would like to send a personalised message to the customer please type here'))->label(false) ?>
                </div>
                <div class="col-xs-3">
                    <?= $form->field($model_r, 'grandTotal')->textInput(['readonly' =>true])->label('Enter Refund Amount') ?>
                    <?= $form->field($model_r, 'notify')->checkbox(); ?>
                    <?= Html::submitButton('Process Refund', ['class' => 'btn pdt-cart pop-submit sm-btn refund-btn', 'id'=>'refund-btn']) ?>
                </div>
                <div class="col-xs-3 price-block">
                   <p>Item Subtotal: <span><?= Helper::money($model->order->subTotal)?></span></p>                
                   <p>Discount Applied: <span><?= Helper::money($model->order->shippingAmount)?></span></p>
                   <p>Order Total: <span><?= Helper::money($model->order->grandTotal)?></span></p>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            
        </div>       
    </div>
</div>


<script>
    $(document).ready(function(){
        //$("#refund-btn").attr("disabled", true);
    });
     $('.refund').keyup(function () {
        var sum=0; 
        $('input[name^="refund"]').each(function() {
            sum+=Number($(this).val());
        });
        $("#creditmemos-grandtotal").val(sum);
    });
//	$("input[type='checkbox']:not(.simple), input[type='radio']:not(.simple)").iCheck({
//                checkboxClass: 'icheckbox_minimal',
//                radioClass: 'iradio_minimal'
//            });
</script>