<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CreditMemos */

$this->title ='New CreditMemos for Order#'.$model->order->orderId;
$this->params['breadcrumbs'][] = ['label' => 'Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-memos-create">

   
    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider'=>$dataProvider,
    ]) ?>

</div>
