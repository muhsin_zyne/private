<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Gallery;
use yii\helpers\ArrayHelper;
use common\models\Stores;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Gallery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gallery';
$this->params['breadcrumbs'][] = $this->title;
$this->params['tooltip'] = "Please manage the image gallery of your website here.";
?>
<div class="gallery-index">
    <?php $form = ActiveForm::begin();
    $user = User::findOne(Yii::$app->user->id);
    $model = new Gallery();
    if($user->roleId == 1)
    {
        $storeId = (isset($_REQUEST['sid'])==NULL ? 1 : $_REQUEST['sid'] );
    }
    else
    {
        $storeId = $user->store->id; 
    }
    $model->storeId=$storeId;
    //echo $storeId; ?>
    <div class="gallery-form" >
        <div class="box box-default color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Add a New Category</h3>
            </div>                  
            <div class="box-body"> 
                <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
                <input type="button" value="create" id="create_button" class="btn btn-primary">
            </div>
        </div>
    </div>
    
    
    <?php
        $sets=Stores::find()->where(['isVirtual'=>0])->all();    
        $listData=ArrayHelper::map($sets,'id','title');
        if($user->roleId == 1){
           
            echo '<div class="box-body"><div class="box box-default color-palette-box"><div class="box-body">';
            echo $form->field($model, 'storeId')->dropDownList($listData);
            echo '</div></div></div>';
        }
        else{
            $storeId = $user->store->id;
            echo '<input type="hidden" id="gallery-storeid" value="'.$storeId.'">';
        }    
    ?>
    <?php ActiveForm::end(); ?>   
</div>
<div class="box">
    <div id="catageries_list">
        <div class="box-header">
            <h3 class="box-title">List of Categories</h3><br/>
            <p class="sm-full">Drag and drop the title to sort the order. It will automatically saved once you drop the item.<br>
            Categories that do not contain images will not display at the front end.</p>
        </div>
        <div class="box-body">
                <div class="cf nestable-lists">
                    <div class="dd" id="nestable">
                        <ul id="sortable">
                        <?php 
                            if($user->roleId == 1)
                                $gallery_storeid=Gallery::find()->where(['storeId' => $storeId])->orderBy('position')->all();
                            else
                                $gallery_storeid=Gallery::find()->where(['storeId' => $storeId])->orderBy('position')->all();
                                //var_dump($gallery_storeid);die;
                                if(empty($gallery_storeid))
                                {
                                    echo 'No Category Found';
                                }
                                else
                                {
                                    
                                    foreach ($gallery_storeid as $index => $item) 
                                    { ?>                                             
                                    <li class="ui-state-default" id="<?=$item['id']?>">
                                        <div class="dd-handle">
                                           <span><?=$item['title']?></span>
                                            <div style="float:right;">               
                                                <?= Html::a('<i class="fa fa-plus"></i> Add / Edit Images', ['gallery-images/index','id' => $item['id']], ['class'=>'btn btn-primary']) ?>
                                                <?= Html::a('<i class="fa fa-save"></i> Update', ['update','id' => $item['id']], ['class'=>'btn btn-success']) ?>
                                                <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $item['id']],['class' => 'btn btn-danger','data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post',],]) ?>
                                            </div>
                                        </div>
                                    </li>  
                                    <?php   }  ?>
                                <?php   }  ?>
                        </ul>
                    </div>       
                </div>
            </div>
        </div>
    </div>

<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 99%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 50px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }
  </style>


<script >
    $(document).ready(function(){  
        //alert('hii');
        $("#sortable" ).sortable({                  
            update: function (event, ui) { 
                var list =  $(this).sortable("toArray").join(",");                
                $.ajax({
                    type: "POST",                
                    url: "<?=Yii::$app->urlManager->createUrl(['gallery/sortable'])?>",                
                    data: "&list=" + list ,                   
                    dataType: "html", 
                    success: function (data) {
                        //alert(data); 
                                  
                    }
                });
            }
        });

       $('#gallery-storeid').on('change', function() 
        { 
            var store_id=$("#gallery-storeid").val();
            $.ajax({
                type: "POST",                
                url: "<?=Yii::$app->urlManager->createUrl(['gallery/view'])?>",                
                data: "&store_id=" + store_id ,                   
                dataType: "html", 
                success: function (data) {
                //alert(data); 
                $("#catageries_list").html(data);                   
                }
            }); 
        });
        $('#create_button').on('click', function() 
        { 
            //alert("hai");exit;
            var store_id=$("#gallery-storeid").val();
            var title=$("#gallery-title").val();
            if(title!='')
            { 
                $.ajax({
                type: "POST",                
                url: "<?=Yii::$app->urlManager->createUrl(['gallery/create'])?>",                
                data: "&store_id=" + store_id + "&title=" + title ,                   
                dataType: "html", 
                success: function (data) {
                    //alert(data); 
                    //$("#catageries_list").html(data);                   
                    }
                });     
            }
            
        });       
    });

</script>