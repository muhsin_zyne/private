<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
$this->params['tooltip'] = "This is the list of customers who have purchased on your website.";
?>

<div class="box">

    <div class="box-body">
        <!-- <div class="smrw sm-pdt-one"> -->   
       <p>  Following is the list of your customers. To see your subscribers <a href="https://talkbox.impactapp.com.au/" target="_blank"> click here </a> </p>
            <p class="submit_enabled create_pdt">
                <?= Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['users/export'], Yii::$app->request->queryParams)), ['class'=>'btn buttons-update btn-success enabled']); ?>
            </p>
        <!-- </div> -->   

<?php
    $form = \kartik\form\ActiveForm::begin(['id' => 'costumers-form']);
    \kartik\form\ActiveForm::end();
?>

<?=\app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options'=>['class'=>'box-body table-responsive no-padding subcr grid-view'],
        'showHeader'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'firstname',    
            'lastname',
            [
                'label' => 'Type',
                'attribute' => 'role.role',
            ],
            'email:email',
            [
                'label' => 'Store',
                'attribute' => 'customerStore.title',
                'visible' => Yii::$app->user->identity->roleId != 3
            ],
            [
                'attribute' => 'status',
                'value' => function($model){ return $model->status=="1"? 'Active' : 'Inactive'; }
            ],
            [
                'attribute' => 'created_at',
                'label'=> 'Created Date',
                'value' => function($model){ return \backend\components\Helper::date($model->created_at); },
                'filter' => \kartik\field\FieldRange::widget([
                        'form' => $form,
                        'model' => $searchModel,
                        'template' => '{widget}{error}',
                        'attribute1' => 'createdDate_start',
                        'attribute2' => 'createdDate_end',
                        'type' => \kartik\field\FieldRange::INPUT_DATE,
                ])
            ],
        ],
    ]);        
?>        


</div>
</div>