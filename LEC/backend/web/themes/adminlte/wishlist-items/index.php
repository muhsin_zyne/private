<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Stores;
use common\models\WishlistItems;
 use frontend\components\Helper;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wishlist Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wishlist-items-index">
    
    <?= \app\components\AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            
            [
                'label'=> 'PRODUCT',
                'attribute' => 'productId',
                'format' => 'html',
                'value' => function($model){
                // $url = \Yii::$app->request->BaseUrl."/images/STR1P_1.png"; 
                //return Html::img($url,['alt'=>'yii']).'<br/><span align="center">'.$model->product->name.'<span><br/><b><font size="4">'.Helper::money($model->product->price).'</font></b>';
                return $model->product->name.'<span><br/><b><font size="4">'.Helper::money($model->product->price).'</font></b>';
                }
            ],
            //'attributeSetName',
            //'productName',
            // [
            //     'label'=> 'User Name',
            //     'attribute' => 'createdDate',
            //     'format' => 'html',
            //     'value' => function($model){
            //         return  $model->user->getFullName();
            //         }
            // ],
            // [
            //     'label'=> 'Store',
            //     'attribute' => 'storeId',
            //     'format' => 'html',
            //     'value' => function($model){
            //         return  $model->store->title;
            //         }
            // ],
            'storeName',
            'fullName',
            //'createdDate',
           'createdDate:datetime',
            
            //'wliDate',
            // [
            //     'label'=> 'ADDED ON',
            //     'attribute' => 'createdDate',
            //     'format' => 'html',
            //     'value' => function($model){
            //         return  Helper::date($model->createdDate, " F jS, Y ");
            //         }
            // ],
             'comments:ntext',

            
        ],
    ]); ?>

</div>
<script >       
    $(document).ready(function(){ 
        $("#wishlistitems-storeid").change(function(){
           var store_id=$("#wishlistitems-storeid").val();
            $.ajax({
                type: "POST",                
                url: "<?=Yii::$app->urlManager->createUrl(['wishlistitems/index'])?>",                
                data: "&store_id=" + store_id ,                   
                dataType: "html", 
                success: function (data) {
                alert(data); 
                //$("#catageries_list").html(data);                   
                }
            }); 
        });
        
    });
</script>
