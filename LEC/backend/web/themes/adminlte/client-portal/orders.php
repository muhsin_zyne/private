<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\components\Helper;
use kartik\export\ExportMenu;
use common\models\User;

$this->title = 'Client Portal Orders';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">
	<div class="box-body">
		<div class="orders-index">
		    <p class="create_pdt">
		     <?php //Html::a('<i class="fa fa-upload"></i> Export as CSV', \yii\helpers\Url::to(array_merge(['byod/export'], Yii::$app->request->queryParams)), ['class'=>'buttons-update btn btn-success enabled']); ?>
		    </p>

		    <?php
		        $form = \kartik\form\ActiveForm::begin(['id' => 'clientportalorders-form']);
		        \kartik\form\ActiveForm::end();
    		?>
    		<?php \yii\widgets\Pjax::begin(['id' => 'recent-products',]); ?>
    		<?=\app\components\AdminGridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'rowOptions'   => function ($model, $key, $index, $grid) {
		            return ['data-id' => $model->id];
		        },
		        'options'=>['class'=>'box-body table-responsive no-padding grid-view'],
		        'showHeader'=>true,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
					'id',
					[
		                'label' => 'Order Date',
		                'attribute' => 'id',
		                'value' => function($model){ return \backend\components\Helper::date($model->orderDate); },
		                'filter' => \kartik\field\FieldRange::widget([
		                        'form' => $form,
		                        'model' => $searchModel,
		                        'template' => '{widget}{error}',
		                        'attribute1' => 'orderDate_start',
		                        'attribute2' => 'orderDate_end',
		                        'type' => \kartik\field\FieldRange::INPUT_DATE,
		                ]),
		                'headerOptions' => ['class' => 'date-range']
		            ],
		            [
		            	'label' => 'Code',
		            	'attribute' => 'portal.studentCode',
		            ],
		            [
		            	'label' => 'Name',
		            	'attribute' => 'portal.name',
		            ],
		            [
		            	'label' => 'Email',
		            	'attribute' => 'portal.email',
		            ],
		            [
		            	'label' => 'Billing Address',
		            	'attribute' => 'billingAddressText',
		            ],
		            [
		            	'label' => 'Shipping Method',
		            	'attribute' => 'portal.shipment_type',
		            ],
		            [
		                'label' => 'Grand Total',
		                'attribute' => 'grandTotal',
		                'format' => 'html',
		                'value' => function ($model) {
		                    return Helper::money($model->grandTotal);
		                },
		            ],
		            [
		                'label' => 'Status',
		                'attribute' => 'status',
		                'value' => function($model, $attribute){ 
		                    return ucwords($model->status);
		                },
		                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['pending' => 'Pending', 'processing' => 'Processing',
		                    'complete' => 'Complete', 'closed' => 'Closed','hold' => 'Hold', 'cancelled' => 'Cancelled', 'payment pending' => 'Payment Pending',],
		                    ['class'=>'form-control','prompt' => '']),
		            ],
		            [
		                'label'=> 'View',
		                'format' => 'html',
		                'value' => function($model){
		                    return Html::a('View',['client-portal/order-detail','id' => $model->id]);
		                }
		            ],
				],
			]); ?>

        </div><!-- /.box-body -->
    </div>
</div>
   