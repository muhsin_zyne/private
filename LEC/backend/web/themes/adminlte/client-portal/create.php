<?php
use yii\helpers\Html;

$this->title = 'Create Client Portal';
$this->params['breadcrumbs'][] = ['label' => 'Client Portal', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
<div class="box-body">

<div class="conferences-create">

    <?= $this->render('_form',compact('clientPortal','clientPortalProducts','stores')) ?>
</div>

</div>
</div>