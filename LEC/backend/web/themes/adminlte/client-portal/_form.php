<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use kartik\date\DatePicker;
	use backend\components\Helper;
	use backend\assets\AppAsset;
	use yii\web\UploadedFile;
	use kartik\widgets\FileInput;
	use yii\grid\GridView;
	use common\models\ClientPortal;
	use common\models\ClientPortalProducts;
    use common\models\User;
    use yii\helpers\ArrayHelper;
?>

<?php
    $user = User::findOne(Yii::$app->user->id);
?>

<div class="error-msg"> </div>
<div class="box">
  	<div class="box-body">
  		<div class="portal-form">
			<?php $form = ActiveForm::begin(['id' => 'clientportal-form','options'=>['enctype'=>'multipart/form-data']]); 
				$disabled =['style'=>'width:500px !important;'];
		        if(!$clientPortal->isNewRecord) {
		            $disabled = ['disabled'=>'disabled','style'=>'width:500px !important;'];
		        } 
			?>

            <?php   
                if($user->roleId != "3") { 
            ?>        
            <label class="control-label">Store</label><br/>        
            <?=Html::dropDownList('listname', $selected=isset($clientPortal->storeId) ? $clientPortal->storeId : "", ArrayHelper::map($stores,'id','title'),['class' => 'store-dropdown']); ?>
            <?php    }    ?>
			<?=$form->field($clientPortal, 'studentCode')->textInput($disabled)->label('Client Code') ?>
			<?php //if($byod->isNewRecord) { ?>
		        <!-- <button type="button" class="btn btn-success generate">Generate BYOD Code</button>
		        <br /><br /> -->
		    <?php //} ?>
            <?= $form->field($clientPortal, 'quote')->checkbox()->label('');?>

            <div class="clearfix"></div>

		    <?= $form->field($clientPortal, 'shipment_type')->dropDownList((['instore-pickup'=>'In Store Pick Up']))->label('Shipment Type'); ?>

		    <?= $form->field($clientPortal, 'payment_type')->dropDownList((['online-payment'=>'Online Payment','no-online-payment'=>'No Online Payment']),['prompt'=>'Select...'])->label('Payment Type'); ?>
		    <?=DatePicker::widget([
				'form' => $form,
				'model' => $clientPortal,
				'attribute' => 'validFrom',
				'pluginOptions' => [
					'format' => 'dd-mm-yyyy',
                    'startDate' => '+0d'
				],
			]);?>
			<?=DatePicker::widget([
				'form' => $form,
				'model' => $clientPortal,
				'attribute' => 'expiresOn',
				'pluginOptions' => [
					'format' => 'dd-mm-yyyy',
                    'startDate' => '+0d'
				],
			]);?>
			<?= $form->field($clientPortal, 'usage_count')->textInput(['maxlength' => 45])->label('Maximum No of Uses') ?>
			<!-- <div class="shipment1_fields"> -->
				<?= $form->field($clientPortal, 'name')->textInput(['maxlength' => 100])->label('Name')->label('Contact Name') ?>
				<?= $form->field($clientPortal, 'email')->textInput(['maxlength' => 100])->label('Email')->label('Contact Email') ?>	
			<!-- </div> -->
			<div class="shipment2_fields">
				<?= $form->field($clientPortal, 'organisation_name')->textInput()->label('Organisation Name') ?>
				
				<div class="brand-image" style="margin-bottom:10px;">
                    <?php
                        if(!empty($clientPortal->organisation_logopath)){
                            $pluginOptions = ['initialPreview' => [Html::img(Yii::$app->params["rootUrl"].$clientPortal->organisation_logopath)],
                                                'overwriteInitial'=>true,'showRemove' => false,'showUpload' => false
                                            ];
                        }
                        else{
                            $pluginOptions = ['overwriteInitial'=>true,'showRemove' => false,'showUpload' => false
                                            ];
                        }
                        echo '<label class="control-label">Organisation Logo (Logo size should be 260px X 120px (Width X Height))</label>';
                        echo FileInput::widget([
                            'model' => $clientPortal,
                            'attribute' => 'organisation_logopath',
                            'pluginOptions' => $pluginOptions 
                        ]);
                    ?>
                </div> 
				
				<?= $form->field($clientPortal, 'address')->textarea()->label('Address'); ?>
				<?= $form->field($clientPortal, 'city')->textInput()->label('City') ?>	
				<?=$form->field($clientPortal, 'state')->dropDownList(['QLD'=>'QLD','NSW'=>'NSW','ACT'=>'ACT','VIC'=>'VIC','TAS'=>'TAS','SA'=>'SA','WA'=>'WA','NT'=>'NT'], ['prompt'=>'--Select--'])->label('State/Province')?>
				<?= $form->field($clientPortal, 'postcode')->textInput(['maxlength' => 45])->label('Zip/Postal Code') ?>	
			</div>
			<?= $form->field($clientPortal, 'phone')->textInput(['maxlength' => 100])->label('Phone (No dash or space please)') ?>

			<label class="control-label">Add Products</label>
 			<div class="add_tray_products">
        		<div class="row">
        			<div class="col-md-12">
                		<div class="search sm-100">
                			<label class="kv-heading-container" for="products-size">Search</label>
                			<?php // Html::textInput('search','',$options=['class'=>'product_search form-control','placeholder'=>'Enter SKU or name of a product','autocomplete'=>'off']) ?>
                            <input type="text" class="product_search form-control" autocomplete="off" value="Enter SKU or Product Title" onblur="if(this.value=='')this.value='Enter SKU or Product Title';" onfocus="if(this.value=='Enter SKU or Product Title')this.value='';" onkeypress="if(event.keyCode=='13')return anythingValidate(document.frm_any_request)">
                    		<span style="font-style:italic">Start typing to get autocomplete results</span>
                    		<div class="product_box">
		                        <div class="product_list"></div>    
                                <span style="font-style:italic;font-size:12px;">Tick the items and click the 'Add to List' button below</span> 
		                        <div class="form-group" style="margin-top:20px;margin-bottom:30px">
		                            <?= Html::a('Add to List','javascript:;',$options = ['class'=>'btn btn-primary add_pdt']) ?>
		                        </div>
		                    </div>
                		</div>
                	</div>	
        		</div>
        		<div class="clear"></div>
        	</div>

        	<div class="tray_products_table">
        		<div class="table-responsive">
            		<table border="1" cellspacing="0" cellpadding="0" class="table sm-table">
                		<thead>
                    		<tr class="headings">
                    			<th style="width: 100px;"></th>
		                        <th>Name</th>
		                        <th>SKU</th>
		                        <th>Original sell price</th>
		                        <th>Client portal price (Incl GST)</th>
		                        <th>Remove</th>
                    		</tr>
                    	</thead>
                    	<tbody>
                    		<?php  
                        		if(!$clientPortal->isNewRecord) {    
                            		if(!empty($clientPortalProducts)) {
                                		foreach ($clientPortalProducts as $product) { //var_dump($product['offerSellPrice']);die();
                                       
                                        /*else
                                            var_dump($product->product->name);die();*/

                                       if(!is_null($product->product)){    

                    		?>
		                        <tr data-id=<?=$product->id?> data-productId=<?=$product->product->id?>>
		                            <td><input type="checkbox" checked name="Products[id][<?=$product['id']?>]" class="selected" value="<?=$product['id']?>"></td>
		                            <td><?=$product->product->name?></td>
		                            <td><?=$product->product->sku?></td>
		                            <td><?=$product->product->price?></td>
		                            <td><input type="text" name="Products[<?=$product['id']?>][offerPrice]" class="offer_price portal_price" autocomplete="off" value="<?=$product['offerPrice']?>"></td>
		                            <td><a href="#" class="remove" title="Delete">X</td>
		                        </tr>
                    		<?php } } } }  ?>
                    	</tbody>
                    </table>
                </div>
            </div>    

            <div class="deleted_items"></div>    		
			<div class="form-group">
                <?= Html::submitButton($clientPortal->isNewRecord ? 'Create' : 'Update', ['class' => $clientPortal->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?php if(!$clientPortal->isNewRecord) { ?>
                    <?= Html::submitButton('Resend Quote', ['class' => $clientPortal->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name' => 'update_email']) ?>
                <?php } ?>
            </div>
            <?php ActiveForm::end(); ?>
		</div>	 
  	</div>
</div> 		

<script type="text/javascript">
function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var j=0; j<3; j++){
    for( var i=0; i < 5; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    if(j<2)
    text = text +"-";
    }
    return text;
}

$('#clientportal-shipment_type').change(function(){
	var selected = $('#clientportal-shipment_type').val();
	if(selected == "instore-pickup"){
		$('.shipment2_fields').hide();
		//$('.shipment1_fields').show();
	}
	else{
		//$('.shipment1_fields').hide();
		$('.shipment2_fields').show();
	}
});

$(document).ready(function(){
    $('.generate').click(function(){
    	$('#clientportal-code').val(makeid());
	});

	/*var selected = $('#clientportal-shipment_type').val();
	if(selected == "instore-pickup"){
		$('.shipment2_fields').hide();
	}
	else{
		$('.shipment1_fields').hide();
	}*/
});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		addedproducts = [];
	    listedproducts = [];
		$(".product_search").on('keyup',function(e){
			$('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });
            
            $(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) == -1) { 
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });
			var keyword = $('.product_search').val();
			$.ajax({
                url:"<?php echo Yii::$app->urlManager->createUrl(['client-portal/getproducts'])?>",    
                type: 'POST',
                data: {keyword: keyword},
                success: function(data)
                    {
                        $(".product_item").empty("");
						if(jQuery.trim($(".product_search")).length > 0) {   
                            $.each($.parseJSON(data), function(key, item) { 
								if($.inArray(key, listedproducts) == -1) {
									if ($.inArray(key, addedproducts) == -1) {    
                                    	$('.product_list').append('<div class="product_item"><input type="checkbox" name="Products[]" value="'+item['id']+'" class="product_checkbox" data-price="'+item['price']+'" data-name="'+item['name']+'" data-sku="'+item['sku']+'">'+item['name']+' - '+item['sku']+'</div>');
                                    }    
                                }    
                            }); 
                        }   
                    }
            });

		});
		$('#w1-tree').height('150');
		$('.add_pdt').on('click',function(){
			$(".tray_products_table table tbody tr").each(function(){ 
                if($(this).attr('data-productId')) {
                    if ($.inArray($(this).attr('data-productId'), listedproducts) != -1) {
                        listedproducts.push($(this).attr('data-productId')); 
                    }
                }        
            });

            $('.selected:checked').each(function(){
                if ($.inArray($(this).attr('value'), addedproducts) == -1) {
                    addedproducts.push($(this).attr('value'));
                }    
            });

            checked_pdts = [];

            $('.product_checkbox:checked').each(function(){
                checked_pdts.push({'idss' : $(this).attr('value'), 'names' : $(this).attr('data-name'),'sku' : $(this).attr('data-sku'),'price' : $(this).attr('data-price')});
            }); 

            $(".tray_products_table").css('display','block');

            for( var i = 0, xlength = checked_pdts.length; i < xlength; i++) { 
                if ($.inArray(checked_pdts[i].idss, listedproducts) == -1) {
					if ($.inArray(checked_pdts[i].idss, addedproducts) == -1) {
						$(".tray_products_table table tbody").append('<tr data-productId="'+checked_pdts[i].idss+'"><td><input type="checkbox" name="Products[id]['+checked_pdts[i].idss+']" class="selected" value="'+checked_pdts[i].idss+'" checked></td><td>'+checked_pdts[i].names+'</td><td>'+checked_pdts[i].sku+'</td><td><input type="text" name="Products['+checked_pdts[i].idss+'][original_sell_price]" class="offer_price portal_price" value="'+checked_pdts[i].price+'" autocomplete="off" disabled="disabled"></td><td><input type="text" name="Products['+checked_pdts[i].idss+'][offerPrice]" class="offer_price portal_price" autocomplete="off"></td><td><a href="#" class="remove" title="Delete">X</td></tr><input type="hidden" name="Products['+checked_pdts[i].idss+'][newitem]" id="data-new" value="1">');
                    }    
                }    
            } 
		});	
	}); 

	<?php if($clientPortal->isNewRecord) { ?>
		$(".tray_products_table").css('display','none');
	<?php } ?>		

	$(".tray_products_table").on("click",".remove", function(e){  
        <?php if(!$clientPortal->isNewRecord) { ?>
            var trid = $(this).closest('tr').attr('data-id');  
		    if(trid) {
                jQuery('.deleted_items').append('<input type="hidden" name="Products[deletedItems][]" id="deletedItems" value="'+trid+'"/>');
            }
        <?php } ?>    
        $(this).closest('tr').remove();    
    }); 

    $('form').submit(function(e) { 
        //var qty = $('.portal_price').val();
        $( ".portal_price" ).each(function( index ) {
          //console.log($(this).val());
            var price = $(this).val();
            if(price == ""){
                $(this).addClass('error_box');
                $(this).css('border-color','red');
                //alert('Please enter offer price');
                e.preventDefault();
            }
            if(price<0){
                $(this).addClass('error_box');
                $(this).css('border-color','red');
                //alert('Please enter a valid offer price');
                e.preventDefault();
            }
        });
       // e.preventDefault();
    });
</script>
