<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\components\Helper;



/* @var $this yii\web\View */
/* @var $model common\models\ConsumerPromotions */

$this->title = 'Client Portal Code: '.$clientPortal->studentCode;
$this->params['breadcrumbs'][] = ['label' => 'Client Portal', 'url' => ['index']];
$this->params['breadcrumbs'][] = $clientPortal->studentCode;
?>
<div class="box">
<div class="box-body">
<div class="consumer-promotions-view">

    <h1><?php // Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $clientPortal->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $clientPortal->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Resend Email', ['send-notification', 'id' => $clientPortal->id], ['class' => 'btn btn-primary']) ?>

   	</p>

    <?= DetailView::widget([
        'model' => $clientPortal,
        'attributes' => [
             [   
                'attribute' => 'studentCode',
                'label'=>'Code',
                'value' =>  $clientPortal->studentCode,
            ],
            [   
                'attribute' => 'shipment_type',
                'label'=>'Shipment Type',
                //'value' =>  ($byod->shipment_type == "instore-pickup") ? "Instore Pickup" : (($byod->shipment_type == "delivery-program") ? "Delivery Program" : "N/A") : "N/A" ,
                'value' => ($clientPortal->shipment_type == "instore-pickup") ? "Instore Pickup" : "Delivery Program" ,
            ],
            [   
                'attribute' => 'payment_type',
                'label'=>'Payment Type',
                //'value' =>  $byod->payment_type,
                'value' => ($clientPortal->payment_type == "online-payment") ? "Online Payment" : "No Online Payment",
            ],
            [   
                'attribute' => 'validFrom',
                'label'=> 'Valid From',
                'value' =>  Helper::date($clientPortal->validFrom),
            ],
            [   
                'attribute' => 'expiresOn',
                'label'=> 'Expires On',
                'value' =>  Helper::date($clientPortal->expiresOn),
            ],
            [
            	'label' => 'Secure URL',
            	'value' => $store->siteUrl.'/client-portal/authenticate?token='.$clientPortal->studentAuthCode,
            ]
        ],
    ]) ?>

    <?=GridView::widget([
        'dataProvider' => $clientPortalProducts,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'product.name',
            'product.sku',
            'product.price',
            [
                'attribute' => 'offerPrice',
                'label' => 'Client portal price (Incl GST)',
            ],
            /*[
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            ],*/
        ],
]); 
?>

    <?php //var_dump($dataProvider);die(); ?>
</div>
</div>
</div>