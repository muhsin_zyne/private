<?php
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\DetailView;
use common\models\User;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use frontend\components\Helper;
use common\models\OrderComment;
use common\models\B2bAddresses;
use common\models\SalesComments;
use common\models\OrderMeta;



$this->title = 'Client Portal Order #'.$model->orderId.' | Client Portal Code : '.$model->portal->studentCode;
$this->params['breadcrumbs'][] = $this->title;

?>

    

    
    <div class="box">
      <div class="categories-index">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs ">
            <li class="active"><a data-toggle="tab" href="#information">Information</a></li>
            <li id="m_title" ><a data-toggle="tab" href="#invoices">Invoices</a></li>
            <li id="m_title" ><a data-toggle="tab" href="#credit_memos">Credit Memos</a></li>
            <li class="m_title"><a data-toggle="tab" href="#shipments">Shipments</a></li>
            <li class="m_title"><a data-toggle="tab" href="#deliveries">Deliveries</a></li>
            <!--<li class="m_title"><a data-toggle="tab" href="#comment_history">Comments History</a></li>-->
          </ul>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="tab-content responsive">
              <div class="tab-pane active" id="information">
                <div class="row">
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-file-text-o"></i>
                        <h3 class="box-title">Order #
                          <?= $model->orderId ?>
                          (the order confirmation email was sent)</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <p>Order Date :
                          <?= $model->orderPlacedDate ?>
                        </p>
                        <p>Order Status :
                          <?= ucfirst($model->status) ?>
                        </p>
                        <p>Purchased From :
                          <?= $model->storeName ?>
                        </p>
                      </div>
                      <!-- /.box-body --> 
                    </div>
                    <!-- /.box --> 
                  </div>
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-user"></i> 
                        <h3 class="box-title">Account Information</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <p> Customer Name :
                          <?= ucfirst($model->customer->firstname).' '.ucfirst($model->customer->lastname) ?>
                        </p>
                        <p> Email :
                          <?= $model->customer->email ?>
                        </p>
                      </div>
                      <!-- /.box-body --> 
                    </div>
                    <!-- /.box --> 
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-home"></i>
                        <h3 class="box-title">Billing Address</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <p>
                          <?= $model->billingAddressText ?>
                        </p>
                      </div>
                      <!-- /.box-body --> 
                    </div>
                    <!-- /.box --> 
                  </div>
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-building-o"></i>
                        <h3 class="box-title">Shipping Address</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <?php if($model->portal->shipment_type == "delivery-program") { ?>
                            <p>
                              <?= $model->portal->organisation_name ?><?php echo ", "; ?>
                              <?= $model->portal->address ?><?php echo ", "; ?>
                              <?= $model->portal->city ?><?php echo ", "; ?>
                              <?= $model->portal->postcode ?>
                            </p>
                        <?php } else { ?>
                        <p>
                          <?= $model->orderDeliveredAddress ?>
                        </p>
                        <?php } ?>
                      </div>
                      <!-- /.box-body --> 
                    </div>
                    <!-- /.box --> 
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-usd"></i>
                        <h3 class="box-title">Payment Information</h3>
                      </div>
                      <!-- /.box-header -->
                      
                      <div class="box-body"> 
                        <span style="color:red">
                          <?php if($model->status=='payment-pending')
                            {
                               echo '<i class="fa fa-warning"></i>'; echo ' Payment Not Received!.';echo '<br>';
                            }?>
                        </span>

                        <!-- <p> by cards or by PayPal account</p> -->
                        <?php 
                            if ($model->paymentMethod == "Braintree") {
                                if (is_null($model->cardNumber)) {
                                    echo "Payment Method: Paypal <br/>";
                                }
                                else{
                                    echo "Payment Method: By Card <br/>";
                                    echo "Card Number(Last 4 digits): ".$model->cardNumber;echo "<br/>";
                                }
                                echo "Transaction Id: ".$model->transactionId;echo "<br/>";
                                //echo $model->vouchers;

                                if(!is_null($model->vouchers))
                                {
                                  $giftamount=array_sum(array_values(json_decode($model->vouchers, true)));
                                  echo 'Gift Voucher : '.Helper::money($giftamount).' <br/>';
                                }
                            } 
                            elseif ($model->paymentMethod == "Gift Voucher") {
                                echo "Payment Method: Gift Voucher <br/>";
                            }
                            else
                            {?>
                             <span style="color:red">
                              <?php 
                              if($model->status!='payment-pending')
                                echo 'Set as paid by Admin!.'; ?>
                              </span>
                            <?php }
                        ?>
                      </div>
                      <!-- /.box-body --> 
                      
                    </div>
                    <!-- /.box --> 
                    
                  </div>
                  <!-- <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-truck"></i>
                        <h3 class="box-title">Shipping & Handling Information</h3>
                      </div>
                      x
                      
                      <div class="box-body">
                        <?php /*
                                if(isset($model->shippingMethodId)){*/ ?>
                        <?php // $model->shippingMethod->title ?>
                        | Total Shipping Charges:
                        <?php // Helper::money($model->shippingAmount) ?>
                        <br/>
                        <?php /* }
                                else
                                {*/ ?>
                        N/A | Total Shipping Charges:
                        <?php // Helper::money(0) ?>
                        <br/>
                        <?php  /*}  */ ?>
                      </div>
                      <!-- /.box-body --> 
                    <!--</div>
                     /.box  
                  </div> -->

                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-money"></i>
                        <h3 class="box-title">Client Portal Information</h3>
                      </div>
                      <!-- /.box-header -->
                      
                      <div class="box-body">
                        <?php echo  "Code :" .$model->portal->studentCode ?> <br/>
                        <?php echo  "Shipment Type :" .$model->portal->shipment_type ?><br/>
                        <?php echo "Payment Type :" .$model->portal->payment_type ?><br/>
                      </div>
                      <!-- /.box-body --> 
                    </div>
                    <!-- /.box --> 
                  </div>

                  

                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-shopping-basket"></i>

                        <h3 class="box-title">Items Ordered</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <?= GridView::widget([
    
                                        'id' => 'order-items',
    
                                        'dataProvider' => $dataProvider,
    
                                        //'filterModel' => $searchModel,
    
                                        'columns' => [
    
                                        ['class' => 'yii\grid\SerialColumn'],
    
                            
    
                                            //'id',
    
                                            [
    
                                                'label'=> 'Item Orderd',
    
                                                'attribute' => 'id',
    
                                                'value' => 'product.name'
    
                                            ],
    
                                            [
    
                                                'label' => 'Option(s)',
    
                                                'value' => 'superAttributeValuesText',
    
                                                'format' => 'html'
    
                                            ],
    
                                            //'productId',
    
                                            [
    
                                                'label'=> 'SKU',
    
                                                'attribute' => 'productId',
    
                                                'value' => 'sku'
    
                                            ],
    
                                            //'status',
                                            [
    
                                                'label' => 'Status',
    
                                                'attribute' => 'status',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return ($model->order->status=='payment-pending')?'Payment-Pending':ucfirst($model->status);
                                                    
    
                                                },
    
                                            ],
                                            [
    
                                                'label' => 'Price',
    
                                                'attribute' => 'id',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return Helper::money($model->price);
    
                                                },
    
                                            ],
    
                            
    
                                            [
    
                                                'label' => 'Qty',
    
                                                'attribute' => 'id',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return $model->qtyStatus;

    
                                                },
    
                                            ],
    
                                            [
    
                                                'label' => 'Sub Total',
    
                                                'attribute' => 'id',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return Helper::money($model->price * $model->quantity);
    
                                                },
    
                                            ],
    
                                            [
    
                                                'label' => 'Discount ',
    
                                                'attribute' => 'id',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return Helper::money($model->discount);
    
                                                },
    
                                            ],
    
                                            [
    
                                                'label' => 'Row Total',
    
                                                'attribute' => 'id',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return Helper::money(($model->price * $model->quantity)- $model->discount);
    
                                                },
    
                                            ],
    
                                            //'comment'
                                            [
    
                                                'label' => 'Comment',
    
                                                'attribute' => 'comment',
    
                                                'format' => 'html',
    
                                                'value' => function ($model) {
    
                                                    return (isset($model->comment))?$model->comment:'';
    
                                                },
    
                                            ],
                                        ],
    
                                    ]); ?>
                      </div>
                      <!-- /.box-body --> 
                      
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-comments-o"></i>
                        <h3 class="box-title">Comments History</h3>
                      </div>
                      <!-- /.box-header -->
                      
                      <div class="box-body">
                        
                        <div class="order-comment-form">
                          <?php $form = ActiveForm::begin(['action' =>  Url::to(['sales-comments/create'])]); 
    
                                        $model_oc= new SalesComments(); 
    
                                       $model_oc->typeId=$model->id; $model_oc->type='order';$model_oc->orderId=$model->id; ?>
                          <?= $form->field($model_oc, 'type')->hiddenInput()->label(false)  ; ?>
                          <?= $form->field($model_oc, 'typeId')->hiddenInput()->label(false)  ; ?>
                          <?= $form->field($model_oc, 'orderId')->hiddenInput()->label(false)  ; ?>
                          <?= $form->field($model_oc, 'comment')->textarea(['rows' => 6])->label('Add Order Comments') ?>
                          <?= $form->field($model_oc, 'notify')->checkbox(); ?>
                          <div class="form-group">
                            <?= Html::submitButton('Submit Comment', ['class' => 'btn btn-primary']) ?>
                          </div>
                          
                          <!--<?= $form->field($model_oc, 'notify')->checkbox(array('label'=>'Notify Customer by Email')); ?>-->
                          
                          <?php ActiveForm::end(); ?>
                          <?= ListView::widget([
    
                                            'dataProvider' => $dataProvider_salescomments,
    
                                            //'filterModel' => $searchModel_ordercomment,
    
                                            'itemView' => '_listview',
    
                                            ]); ?>
                        </div>
                      </div>
                      <!-- /.box-body --> 
                      
                    </div>
                    <!-- /.box --> 
                    
                  </div>
                  <div class="col-md-6">
                    <div class="box box-default">
                      <div class="box-header with-border"><i class="fa fa-book"></i>
                        <h3 class="box-title">Order Totals</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body" >
                        <p>Subtotal    :
                          <?= Helper::money($model->subTotal) ?>
                        </p>
                        <p>Shipping & Handling :
                          <?= Helper::money($model->shippingAmount) ?>
                        </p>
                        <p>Coupon Discount :
                          <?= $model->formatedOrderDiscountAmount ?>
                        </p>
                        <p>Grand Total :
                          <?= Helper::money($model->grandTotal) ?>
                        </p>
                        <p>Total Paid  :
                          <?= Helper::money($model->grandTotal) ?>
                        </p>
                        <p>Total Refunded :
                          <?= Helper::money($model->totalRefunded) ?>
                        </p>
                        <p>Total Due   : AU$0.00</p>
                      </div>
                      <!-- /.box-body --> 
                    </div>
                    <!-- /.box --> 
                  </div>
                </div>
              </div>
              <!--  invoices -->
              
              <div class="tab-pane" id="invoices">
                <?= GridView::widget([
    
                        'dataProvider' => $dataProvider_invoices,
    
                        //'filterModel' => $searchModel_invoices,
    
                        'columns' => [
    
                            ['class' => 'yii\grid\SerialColumn'],
    
    
    
                            'id',
    
                            [
    
                                'label'=> 'Bill to Address',
    
                                'format' => 'html',
    
                                'attribute' => 'orderId',
    
                                'value' => 'order.billingAddressText'
    
                            ],
    
                            [
    
                                'label'=> 'Invoice Date',
    
                                'attribute' => 'id',
    
                                'value' => 'invoicePlacedDate'
    
                            ],
    
                            //'createdDate',
    
                            //'status',
    
                            //'subTotal',
    
                            //'shipping',
                            [
    
                                'label'=> 'Status',
    
                                'attribute' => 'status',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return ucfirst($model->status);
    
                                }
    
                            ],
                            [
    
                                'label'=> 'Amount',
    
                                'attribute' => 'id',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return Helper::money($model->grandTotal);
    
                                }
    
                            ],
    
                            //'grandTotal',
    
                            [
    
                                'label'=> 'View',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return Html::a('View',['invoices/view','id' => $model->id]);
    
                                }
    
                            ],
    
                        ],
    
                    ]); ?>
              </div>
              
              <!--  credit memos -->
              
              <div class="tab-pane" id="credit_memos">
                <?= GridView::widget([
    
                        'dataProvider' => $dataProvider_creditmemos,
    
                        //'filterModel' => $searchModel_creditmemos,
    
                        'columns' => [
    
                            ['class' => 'yii\grid\SerialColumn'],
    
    
    
                            'id',
    
                            [
    
                                'label'=> 'Bill to Address',
    
                                'attribute' => 'orderId',
    
                                'format' => 'html',
    
                                'value' => 'order.billingAddressText'
    
                            ],
                            [
    
                                'label'=> 'Created Date',
    
                                'attribute' => 'createdDate',
    
                                //'format' => 'html',
    
                                'value' => 'creditmemoPlacedDate'
    
                            ],
    
    
                            //'creditmemoPlacedDate',
    
                            //'status',
                            [
    
                                'label'=> 'Status',
    
                                'attribute' => 'status',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return ucfirst($model->status);
    
                                }
    
                            ],
                            [
    
                                'label'=> 'Refunded',
    
                                'attribute' => 'id',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return Helper::money($model->grandTotal);
    
                                }
    
                            ],
    
                            //'refundedDate',
    
                            
    
                            [
    
                                'label'=> 'View',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return Html::a('View',['credit-memos/view','id' => $model->id]);
    
                                }
    
                            ],
    
                        ],
    
                    ]); ?>
              </div>
              
              <!-- shipment  -->
              
              <div class="tab-pane" id="shipments">
                <?= GridView::widget([
    
                        'dataProvider' => $dataProvider_shipment,
    
                        //'filterModel' => $searchModel_shipment,
    
                        'columns' => [
    
                            ['class' => 'yii\grid\SerialColumn'],
    
    
    
                            'id',
    
                            [
    
                                'label'=> 'Ship to Address',
    
                                'format' => 'html',
    
                                'attribute' => 'orderId',
    
                                'value' => 'order.ShippingAddressText'
    
                            ],
    
                            [
    
                                'label'=> 'Date Shipped',
    
                                'attribute' => 'id',
    
                                'value' => 'shipmentPlacedDate'
    
                            ],
    
                            //'createdDate',
    
                            //'orderItemCount',
    
                            [
    
                                'label'=> 'Total Qty',
    
                                'attribute' => 'id',
    
                                'value' => 'orderItemCount'
    
                            ],
    
                            [
    
                                'label'=> 'View',
    
                                'format' => 'html',
    
                                'value' => function($model){
    
                                    return Html::a('View',['shipments/view','id' => $model->id]);
    
                                }
    
                            ],
    
                        ],
    
                    ]); ?>
              </div>
              <!-- Deliveries  -->
              
              <div class="tab-pane" id="deliveries">
                <?= GridView::widget([    
                        'dataProvider' => $dataProvider_deliveries,    
                        //'filterModel' => $searchModel_shipment,    
                        'columns' => [    
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',    
                            [    
                                'label'=> 'Delivered  Address',    
                                'format' => 'html',    
                                'attribute' => 'orderId',    
                                'value' => 'order.orderDeliveredAddress'    
                            ], 
                            'deliveryPlacedDate',
                            [    
                                'label'=> 'View',    
                                'format' => 'html',    
                                'value' => function($model){    
                                    return Html::a('View',['delivery/view','id' => $model->id]);    
                                }    
                            ],    
                        ],
    
                    ]); ?>
              </div>
              <!-- comment_history  -->
              
              <div class="tab-pane" id="comment_history">
               <!-- <?= ListView::widget([
    
                        'dataProvider' => $dataProvider_salescomments_all,
    
                        //'filterModel' => $searchModel_ordercomment,
    
                        'itemView' => '_listview',
    
                    ]); ?>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php $order_status= $model->status; ?>
<script type="text/javascript">

    $(document).ready(function(){

        //alert('hiii');
        var invoice_status= <?= $model->invoiceStatus?>;
        $("#invoice").hide();
        if(invoice_status>0)
        {
            $("#invoice").show();
        }
        var status = '<?php echo $order_status;?>';
        if(status=='hold')

        {

            $(".hold").hide();

            $(".div-unhold").show();

        }

        else

        {

           //$(".hold").hide();

            $(".div-unhold").hide();

        }

        $('#hold').click(function(){

            //alert('hiii');

            var status='hold';

            var order_id='<?php echo $model->id;?>';

            //alert(order_id);

            $.ajax({

                type: "POST",

                url: "<?=Yii::$app->urlManager->createUrl(['orders/changestatus'])?>",

                data: "&status=" + status + "&order_id=" + order_id,

                dataType: "html", 

                success: function (data) {

                    //alert(data);                

                }                

            });

        });

        $('#unhold').click(function(){

            //alert('hiii');

            var status='processing';

            var order_id='<?php echo $model->id;?>';

            //alert(order_id);

            $.ajax({

                type: "POST",

                url: "<?=Yii::$app->urlManager->createUrl(['orders/changestatus'])?>",

                data: "&status=" + status + "&order_id=" + order_id,

                dataType: "html", 

                success: function (data) {

                    //alert(data);                

                }                

            });

        });

        $('#pending_payment').click(function(){

            var status='pending';

            var order_id='<?php echo $model->id;?>';

            $.ajax({

                type: "POST",

                url: "<?=Yii::$app->urlManager->createUrl(['orders/changestatus'])?>",

                data: "&status=" + status + "&order_id=" + order_id,

                dataType: "html", 

                success: function (data) {

                }                

            });

        });



      

    }); 



</script>