<?php
	use yii\helpers\Html;
	$this->title = 'Update Client Portal: ' . ' ' . $clientPortal->studentCode;
	$this->params['breadcrumbs'][] = ['label' => 'Client Portal', 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $clientPortal->studentCode, 'url' => ['view', 'id' => $clientPortal->id]];
	$this->params['breadcrumbs'][] = 'Update';
?>

<div class="byod-update">
	<?= $this->render('_form', compact('clientPortal','clientPortalProducts','stores')) ?>
</div>