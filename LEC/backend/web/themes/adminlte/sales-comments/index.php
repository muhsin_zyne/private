<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SalesCommentsPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sales Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-comments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sales Comments', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'typeId',
            'comment:ntext',
            'createdDate',
            // 'notifiedDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
