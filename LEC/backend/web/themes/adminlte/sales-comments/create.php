<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SalesComments */

$this->title = 'Create Sales Comments';
$this->params['breadcrumbs'][] = ['label' => 'Sales Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-comments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
